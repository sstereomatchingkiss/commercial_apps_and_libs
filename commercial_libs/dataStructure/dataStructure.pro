#-------------------------------------------------
#
# Project created by QtCreator 2013-01-10T17:18:29
#
#-------------------------------------------------

QT     += core testlib
CONFIG += console testlib
CONFIG += warn_on

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = dataStructure
TEMPLATE = app

win32{
QMAKE_CXXFLAGS += -std=c++0x
QMAKE_CXXFLAGS += -Wall
}
mac{
LIBS += -stdlib=libc++

QMAKE_CXXFLAGS += -stdlib=libc++
QMAKE_CXXFLAGS += -std=c++11
QMAKE_CXXFLAGS += -mmacosx-version-min=10.7
QMAKE_LFLAGS += -mmacosx-version-min=10.7
}

SOURCES +=

HEADERS  += \
    genericCache.hpp
