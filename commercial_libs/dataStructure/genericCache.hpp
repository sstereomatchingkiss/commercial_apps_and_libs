#ifndef GENERICCACHE_HPP
#define GENERICCACHE_HPP

#include <vector>

/*
 * a cache which support undo ability
 */
template<typename T>
class genericCache
{
public:
    explicit genericCache(int cache_max = 2) :
        cache_counter_(0), cache_max_(cache_max), data_(cache_max_) {}
    genericCache(genericCache const&) = delete;
    genericCache& operator=(genericCache const&) = delete;

    int get_counter() const { return cache_counter_; }
    T& get_data()
    {
        return cache_counter_ != 0 ? data_[cache_counter_ - 1] : data_[cache_counter_];
    }
    int get_maximum() const { return cache_max_; }

    int is_undoable() const { return cache_counter_ > 0; }

    void set_data(T const &data);
    void set_maximum(int max) { cache_max_ = max; data_.resize(cache_max_); }

    void undo()
    {
        if(cache_counter_ != 0){
            --cache_counter_;
        }
    }

    T const& undo_then_get();

private:
    int cache_counter_;
    int cache_max_;

    std::vector<T> data_;
};

template<typename T>
void genericCache<T>::set_data(T const &data)
{
    if(cache_counter_ == cache_max_){
        for(int i = 1; i != cache_max_; ++i){
            data_[i - 1] = std::move(data_[i]);
        }
        data_[cache_counter_ - 1] = data;
    }else{
        data_[cache_counter_++] = data;
    }
}

template<typename T>
T const& genericCache<T>::undo_then_get()
{
    undo();

    return get_data();
}

#endif // GENERICCACHE_HPP
