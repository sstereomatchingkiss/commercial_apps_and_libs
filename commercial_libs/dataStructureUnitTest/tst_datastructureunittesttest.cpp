#include <QDebug>
#include <QDir>
#include <QImage>
#include <QString>
#include <QtTest>

#include "genericCache.hpp"

class DataStructureUnitTestTest : public QObject
{
    Q_OBJECT
    
public:
    DataStructureUnitTestTest();
    
private Q_SLOTS:
    void testCase1();
};

DataStructureUnitTestTest::DataStructureUnitTestTest()
{
}

void DataStructureUnitTestTest::testCase1()
{    
    QImage img[3];
    img[0].load(":/00.png");
    img[1].load(":/01.png");
    img[2].load(":/02.png");

    QCOMPARE(img[0].isNull(), false);
    QCOMPARE(img[1].isNull(), false);
    QCOMPARE(img[2].isNull(), false);

    genericCache<QImage> cache(2);
    cache.set_data(img[0]);
    QCOMPARE(img[0] == cache.get_data(), true);

    cache.set_data(img[1]);
    QCOMPARE(img[1] == cache.get_data(), true);

    cache.set_data(img[2]);
    QCOMPARE(img[2] == cache.get_data(), true);
    QCOMPARE(img[1] == cache.undo_then_get(), true);
}

QTEST_APPLESS_MAIN(DataStructureUnitTestTest)

#include "tst_datastructureunittesttest.moc"
