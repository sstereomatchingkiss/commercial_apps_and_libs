#-------------------------------------------------
#
# Project created by QtCreator 2013-02-09T11:30:58
#
#-------------------------------------------------

QT       += gui testlib

TARGET = tst_datastructureunittesttest
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

win32{
QMAKE_CXXFLAGS += -std=c++0x
QMAKE_CXXFLAGS += -Wall
}
mac{
LIBS += -stdlib=libc++

QMAKE_CXXFLAGS += -stdlib=libc++
QMAKE_CXXFLAGS += -std=c++11
QMAKE_CXXFLAGS += -mmacosx-version-min=10.7
QMAKE_LFLAGS += -mmacosx-version-min=10.7
}

INCLUDEPATH += ../dataStructure


SOURCES += tst_datastructureunittesttest.cpp
DEFINES += SRCDIR=\\\"$$PWD/\\\"

HEADERS += \
    ../dataStructure/genericCache.hpp

RESOURCES += \
    icons.qrc
