#include <QCoreApplication>
#include <QString>
#include <QStringList>
#include <QtTest>

#include <stringHelper/qstringAlgo.hpp>

class StringHelperUnitTestTest : public QObject
{
    Q_OBJECT

public:
    StringHelperUnitTestTest();

private Q_SLOTS:
    void test_create_serial_name_single() const;
    void test_create_serial_name() const;
    void test_number_mapper() const;
    void test_number_mapper_str_list() const;
    void test_qstring_position_surround_with() const;
    void test_split_str_by_wrap_around() const;
    void test_words_to_numbers() const;

private:
    QStringList create_numbers(int size) const; //design for test_number_mapper and test_number_mapper_str_list
};

StringHelperUnitTestTest::StringHelperUnitTestTest()
{    
}

void StringHelperUnitTestTest::test_create_serial_name_single() const
{    
    QString const name = "melon";

    QString result = SCode::create_serial_name_single(name, ".png", 0, 3);
    QCOMPARE(result, QString("melon000.png"));

    result = SCode::create_serial_name_single(name, ".png", 0, 4);
    QCOMPARE(result, QString("melon0000.png"));

    result = SCode::create_serial_name_single(name, ".png", 12345, 4);
    QCOMPARE(result, QString("melon12345.png"));
}

void StringHelperUnitTestTest::test_create_serial_name() const
{
    QString const name = "melon";
    QStringList const result = SCode::create_serial_name(name, ".png", 0, 10);
    QStringList test_result = {"000", "001", "002", "003", "004", "005", "006", "007", "008", "009"};
    for(QString &data : test_result){
        data.prepend("melon");
        data.append(".png");
    }
    for(int i = 0; i != result.size(); ++i){
        QCOMPARE(result[i], test_result[i] );
        QCOMPARE(result[i], test_result[i]);
    }
}

void StringHelperUnitTestTest::test_qstring_position_surround_with() const
{
    QString const name = "iiiia href = lllll]]]]";
    auto const index = SCode::qstring_position_surround_with(name, "iiii", "]]]]");

    QCOMPARE(index, std::make_pair(4, 14) );
    QCOMPARE(name.mid(index.first, index.second), QString("a href = lllll") );

    QTextCodec *codec = QTextCodec::codecForName("UTF-8");
    QTextCodec::setCodecForLocale(codec);

    QString const utf8Name = "[<a href=\"index.php?res=553215\">返信</a>]";
    auto const index2 = SCode::qstring_position_surround_with(utf8Name, "\"", "\"");

    QCOMPARE(index2, std::make_pair(10, 20) );
    QCOMPARE(utf8Name.mid(index2.first, index2.second), QString("index.php?res=553215") );
}

void StringHelperUnitTestTest::test_number_mapper() const
{
    int const size = 100;
    int const length = QString::number(size).length();
    QStringList results = create_numbers(size);
    for(int i = 0; i != size; ++i){
        QCOMPARE(SCode::number_mapper(i, length), results[i]);
    }
}

void StringHelperUnitTestTest::test_number_mapper_str_list() const
{
    int const maximum = 100;
    QStringList const data = SCode::number_mapper(0, maximum, 3);
    QStringList const results = create_numbers(maximum);

    for(int i = 0; i != maximum; ++i){
        QCOMPARE(data[i], results[i]);
    }
}

void StringHelperUnitTestTest::test_split_str_by_wrap_around() const
{
    QString const text[] = {"cow_#_boy_##_girl_###_human####", "lll ### ### ###", "$&*###\\-+##"};
    QStringList split_results[] =
    {
        QStringList() <<"cow_"<<"#"<<"_boy_"<<"##"<<"_girl_"<<"###"<<"_human"<<"####",
        QStringList() <<"lll "<<"###"<<" "<<"###"<<" "<<"###",
        QStringList() <<"$&*"<<"###"<<"\\-+"<<"##"
    };

    for(int i = 0; i != sizeof(text) / sizeof(QString); ++i){
        QCOMPARE(SCode::split_str_by_wrap_around('#', text[i]), split_results[i]);
    }
}

void StringHelperUnitTestTest::test_words_to_numbers() const
{
    QString const text[]    = {"cow_#_boy_##_girl_###_human####", "lll ### ### ###", "$&*###\\-+##"};
    QString const results[] = {"cow_0_boy_00_girl_000_human0000", "lll 000 000 000", "$&*000\\-+00"};
    for(size_t i = 0; i != sizeof(text)/ sizeof(QString); ++i){
        auto const output = SCode::words_to_number(QRegularExpression("#+"), SCode::split_str_by_wrap_around('#', text[i]), 0);
        auto const output2 = SCode::words_to_number(QRegularExpression("(#+)"), SCode::split_str_by_wrap_around('#', text[i]), 0);
        QCOMPARE(results[i], output);
        QCOMPARE(results[i], output2);
    }
}

/**********************************************************
 ****************** implementation ************************
 **********************************************************/

QStringList StringHelperUnitTestTest::create_numbers(int size) const
{
    QStringList results;
    for(int i = 0; i != size; ++i){
        results << QString::number(i);
    }

    int const length = QString::number(size).length();
    for(int i = 0; i != size; ++i){
        for(int j = results[i].length(); j < length; ++j)
          results[i].prepend("0");
    }

    return results;
}

QTEST_APPLESS_MAIN(StringHelperUnitTestTest)

#include "tst_StringHelperUnitTestTest.moc"
