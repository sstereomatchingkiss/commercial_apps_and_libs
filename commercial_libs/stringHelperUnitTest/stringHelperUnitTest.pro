#-------------------------------------------------
#
# Project created by QtCreator 2014-04-19T02:31:47
#
#-------------------------------------------------

QT       += testlib

QT       -= gui

TARGET = tst_StringHelperUnitTestTest
CONFIG   += console
CONFIG   -= app_bundle
CONFIG   += c++11

TEMPLATE = app

INCLUDEPATH += ../


SOURCES += tst_StringHelperUnitTestTest.cpp \
    ../stringHelper/qstringAlgo.cpp
DEFINES += SRCDIR=\\\"$$PWD/\\\"

HEADERS += \
    ../stringHelper/qstringAlgo.hpp
