#-------------------------------------------------
#
# Project created by QtCreator 2013-02-11T12:49:28
#
#-------------------------------------------------

QT       -= gui

TARGET = QtHelper
TEMPLATE = lib
CONFIG += staticlib

win32{
QMAKE_CXXFLAGS += -std=c++0x
}
mac{
LIBS += -stdlib=libc++

QMAKE_CXXFLAGS += -stdlib=libc++
QMAKE_CXXFLAGS += -std=c++11
QMAKE_CXXFLAGS += -mmacosx-version-min=10.7
QMAKE_LFLAGS += -mmacosx-version-min=10.7
}

QMAKE_CXXFLAGS += -Woverloaded-virtual

SOURCES +=

HEADERS += \
    QtConnection.hpp
unix:!symbian {
    maemo5 {
        target.path = /opt/usr/lib
    } else {
        target.path = /usr/lib
    }
    INSTALLS += target
}
