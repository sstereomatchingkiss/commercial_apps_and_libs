#ifndef QTCONNECTION_HPP
#define QTCONNECTION_HPP

#include <QObject>

template<typename T, typename Func1 ,typename U, typename Func2>
inline void set_connect(T ptr1, Func1 func1, U ptr2, Func2 func2, bool is_connect)
{
    if(is_connect){
        QObject::connect(ptr1, func1, ptr2, func2);
    }else{
        QObject::disconnect(ptr1, func1, ptr2, func2);
    }
}

#endif // QTCONNECTION_HPP
