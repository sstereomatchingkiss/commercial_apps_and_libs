uniform sampler2D source;

uniform mediump float intensity;
uniform float imageHeight;
uniform float imageWidth;

uniform lowp float qt_Opacity;
varying highp vec2 qt_TexCoord0;

const mediump vec3 W = vec3(0.2125, 0.7154, 0.0721);

/*void rgb2yuv(vec3 rgb, out vec3 yuv)
{
    yuv.r = rgb.r * 0.299 + rgb.g * 0.587 + rgb.b * 0.114;
    yuv.g = rgb.r * -0.14713 + rgb.g * -0.28886 + rgb.b * 0.436;
    yuv.b = rgb.r * 0.615 + rgb.g * -0.51499 + rgb.b * -0.10001;
}

void yuv2rgb(vec3 yuv, out vec3 rgb)
{
    rgb.r = yuv.r + 1.13983 * yuv.b;
    rgb.g = yuv.r - 0.39465 * yuv.g  -0.58060 * yuv.b;
    rgb.b = yuv.r + 2.03211 * yuv.g;
}*/

void main()
{
    mediump vec3 textureColor = texture2D(source, qt_TexCoord0).rgb;

    mediump vec2 stp0 = vec2(1.0 / imageWidth, 0.0);
    mediump vec2 st0p = vec2(0.0, 1.0 / imageHeight);
    mediump vec2 stpp = vec2(1.0 / imageWidth, 1.0 / imageHeight);
    mediump vec2 stpm = vec2(1.0 / imageWidth, -1.0 / imageHeight);

    mediump float im1m1 = dot( texture2D(source, qt_TexCoord0 - stpp).rgb, W);
    mediump float ip1p1 = dot( texture2D(source, qt_TexCoord0 + stpp).rgb, W);
    mediump float im1p1 = dot( texture2D(source, qt_TexCoord0 - stpm).rgb, W);
    mediump float ip1m1 = dot( texture2D(source, qt_TexCoord0 + stpm).rgb, W);
    mediump float im10 = dot( texture2D(source, qt_TexCoord0 - stp0).rgb, W);
    mediump float ip10 = dot( texture2D(source, qt_TexCoord0 + stp0).rgb, W);
    mediump float i0m1 = dot( texture2D(source, qt_TexCoord0 - st0p).rgb, W);
    mediump float i0p1 = dot( texture2D(source, qt_TexCoord0 + st0p).rgb, W);
    mediump float h = -im1p1 - 2.0 * i0p1 - ip1p1 + im1m1 + 2.0 * i0m1 + ip1m1;
    mediump float v = -im1m1 - 2.0 * im10 - im1p1 + ip1m1 + 2.0 * ip10 + ip1p1;

    mediump float mag = 1.0 - length(vec2(h, v));
    mediump vec3 target = vec3(mag);

    gl_FragColor = vec4(mix(textureColor, target, intensity), 1.0) * qt_Opacity;
}

/*void main()
{
    mediump vec3 textureColor = texture2D(source, qt_TexCoord0).rgb;

    mediump vec2 stp0 = vec2(1.0 / imageWidth, 0.0);
    mediump vec2 st0p = vec2(0.0, 1.0 / imageHeight);
    mediump vec2 stpp = vec2(1.0 / imageWidth, 1.0 / imageHeight);
    mediump vec2 stpm = vec2(1.0 / imageWidth, -1.0 / imageHeight);

    mediump float im1m1 = dot( texture2D(source, qt_TexCoord0 - stpp).rgb, W);
    mediump float ip1p1 = dot( texture2D(source, qt_TexCoord0 + stpp).rgb, W);
    mediump float im1p1 = dot( texture2D(source, qt_TexCoord0 - stpm).rgb, W);
    mediump float ip1m1 = dot( texture2D(source, qt_TexCoord0 + stpm).rgb, W);
    mediump float im10 = dot( texture2D(source, qt_TexCoord0 - stp0).rgb, W);
    mediump float ip10 = dot( texture2D(source, qt_TexCoord0 + stp0).rgb, W);
    mediump float i0m1 = dot( texture2D(source, qt_TexCoord0 - st0p).rgb, W);
    mediump float i0p1 = dot( texture2D(source, qt_TexCoord0 + st0p).rgb, W);
    mediump float h = -im1p1 - 2.0 * i0p1 - ip1p1 + im1m1 + 2.0 * i0m1 + ip1m1;
    mediump float v = -im1m1 - 2.0 * im10 - im1p1 + ip1m1 + 2.0 * ip10 + ip1p1;

    mediump float mag = 1.0 - length(vec2(h, v));
    mediump vec3 target = vec3(mag);

    gl_FragColor = vec4(mix(textureColor, target, intensity), 1.0) * qt_Opacity;
}

/*uniform float mixLevel;
uniform float resS;
uniform float resT;

uniform sampler2D source;
uniform lowp float qt_Opacity;
varying vec2 qt_TexCoord0;

void main()
{
    vec2 uv = qt_TexCoord0.xy;
    vec4 c = vec4(0.0);

    vec2 st = qt_TexCoord0.st;
    vec3 irgb = texture2D(source, st).rgb;
    vec2 stp0 = vec2(1.0 / resS, 0.0);
    vec2 st0p = vec2(0.0       , 1.0 / resT);
    vec2 stpp = vec2(1.0 / resS, 1.0 / resT);
    vec2 stpm = vec2(1.0 / resS, -1.0 / resT);
    const vec3 W = vec3(0.2125, 0.7154, 0.0721);
    float im1m1 = dot(texture2D(source, st-stpp).rgb, W);
    float ip1p1 = dot(texture2D(source, st+stpp).rgb, W);
    float im1p1 = dot(texture2D(source, st-stpm).rgb, W);
    float ip1m1 = dot(texture2D(source, st+stpm).rgb, W);
    float im10  = dot(texture2D(source, st-stp0).rgb, W);
    float ip10  = dot(texture2D(source, st+stp0).rgb, W);
    float i0m1  = dot(texture2D(source, st-st0p).rgb, W);
    float i0p1  = dot(texture2D(source, st+st0p).rgb, W);
    float h = -1.0*im1p1 - 2.0*i0p1 - 1.0*ip1p1 + 1.0*im1m1 + 2.0*i0m1 + 1.0*ip1m1;
    float v = -1.0*im1m1 - 2.0*im10 - 1.0*im1p1 + 1.0*ip1m1 + 2.0*ip10 + 1.0*ip1p1;
    float mag = 1.0 - length(vec2(h, v));
    vec3 target = vec3(mag, mag, mag);
    c = vec4(target, 1.0);

    gl_FragColor = qt_Opacity * c;
}*/
