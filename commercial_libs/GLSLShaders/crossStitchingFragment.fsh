uniform sampler2D source;
uniform float imageHeight;
uniform float imageWidth;
uniform float stitching_size;
uniform int invert;
uniform lowp float qt_Opacity;
varying vec2 qt_TexCoord0;

vec4 PostFX(vec2 uv)
{
  vec4 c = vec4(0.0);
  float size = stitching_size;
  vec2 cPos = uv * vec2(imageWidth, imageHeight);
  vec2 tlPos = floor(cPos / vec2(size, size));
  tlPos *= size;
  int remX = int(mod(cPos.x, size));
  int remY = int(mod(cPos.y, size));
  if (remX == 0 && remY == 0)
    tlPos = cPos;
  vec2 blPos = tlPos;
  blPos.y += (size - 1.0);
  if ((remX == remY) ||
     (((int(cPos.x) - int(blPos.x)) == (int(blPos.y) - int(cPos.y)))))
  {
    if (invert == 1)
      c = vec4(0.2, 0.15, 0.05, 1.0);
    else
      c = texture2D(source, tlPos * vec2(1.0/imageWidth, 1.0/imageHeight)) * 1.4;
  }
  else
  {
    if (invert == 1)
      c = texture2D(source, tlPos * vec2(1.0/imageWidth, 1.0/imageHeight)) * 1.4;
    else
      c = vec4(0.0, 0.0, 0.0, 1.0);
  }
  return c;
}

void main (void)
{
  vec2 uv = qt_TexCoord0.st;
  gl_FragColor = PostFX(uv) * qt_Opacity;
}
