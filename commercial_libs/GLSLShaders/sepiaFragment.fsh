// Based on http://kodemongki.blogspot.com/2011/06/kameraku-custom-shader-effects-example.html

uniform sampler2D source;
uniform lowp float qt_Opacity;
varying vec2 qt_TexCoord0;

void main()
{
    vec2 uv = qt_TexCoord0.xy;
    vec4 orig = texture2D(source, uv);
    vec3 col = orig.rgb;
    /*float y = 0.3 * col.r + 0.59 * col.g + 0.11 * col.b;

    gl_FragColor = qt_Opacity * vec4(y + 0.15, y + 0.07, y - 0.12, 1.0);*/

    col.b = col.b * 0.272 + col.g * 0.534 + col.r * 0.131;
    col.g = col.b * 0.349 + col.g * 0.686 + col.r * 0.168;
    col.r = col.b * 0.393 + col.g * 0.769 + col.r * 0.189;

    gl_FragColor = qt_Opacity * vec4(col, 1.0);

}
