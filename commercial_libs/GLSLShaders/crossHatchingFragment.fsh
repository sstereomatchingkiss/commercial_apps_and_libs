uniform sampler2D source;
uniform float hatch_y_offset; // 5.0
uniform float lum_threshold_1; // 1.0
uniform float lum_threshold_2; // 0.7
uniform float lum_threshold_3; // 0.5
uniform float lum_threshold_4; // 0.3

uniform lowp float qt_Opacity;
varying vec2 qt_TexCoord0;

void main()
{
    vec2 uv = qt_TexCoord0.xy;

    vec3 tc = vec3(1.0, 0.0, 0.0);

    float lum = length(texture2D(source, uv).rgb);
    tc = vec3(1.0, 1.0, 1.0);

    //vec3 uv = texture2D(source, uv).rgb;
    //float lum = uv.r * 0.2126 + uv.g * 0.7152 + uv.b * 0.0722

    if (lum < lum_threshold_1){
        if (mod(gl_FragCoord.x + gl_FragCoord.y, 10.0) == 0.0)
            tc = vec3(0.0, 0.0, 0.0);
    }

    if (lum < lum_threshold_2){
        if (mod(gl_FragCoord.x - gl_FragCoord.y, 10.0) == 0.0)
            tc = vec3(0.0, 0.0, 0.0);
    }

    if (lum < lum_threshold_3){
        if (mod(gl_FragCoord.x + gl_FragCoord.y - hatch_y_offset, 10.0) == 0.0)
            tc = vec3(0.0, 0.0, 0.0);
    }

    if (lum < lum_threshold_4){
        if (mod(gl_FragCoord.x - gl_FragCoord.y - hatch_y_offset, 10.0) == 0.0)
            tc = vec3(0.0, 0.0, 0.0);
    }

    gl_FragColor = vec4(tc, 1.0) * qt_Opacity;
}
