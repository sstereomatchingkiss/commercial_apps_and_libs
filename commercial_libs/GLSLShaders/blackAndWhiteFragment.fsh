// Based on http://kodemongki.blogspot.com/2011/06/kameraku-custom-shader-effects-example.html

uniform float threshold;

uniform sampler2D source;
uniform lowp float qt_Opacity;
varying vec2 qt_TexCoord0;

void main()
{    
    vec4 orig = texture2D(source, qt_TexCoord0);
    float y = 0.3 *orig.r + 0.59 * orig.g + 0.11 * orig.b;
    y = y < threshold ? 0.0 : 1.0;

    gl_FragColor = qt_Opacity * vec4(y, y, y, 1.0);
}
