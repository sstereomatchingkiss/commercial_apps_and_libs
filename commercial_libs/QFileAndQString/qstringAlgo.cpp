#include <algorithm>
#include <cstdlib>
#include <list>

#ifdef DEBUG_OK
#include <QDebug>
#endif

#include "qstringAlgo.hpp"

/**
 *@brief change the integer into serial number which the length do not less
 * than the number of space
 *
 * @param new_name : new name of the file(s)
 * @param suffix : like "txt", "jpg", "png" and so on
 * @param initial : initial value of the serial number
 * @param end : the biggest value of the serial number
 * @param space : the minimum space the serial number has to occupy
 * @param place_holder : the place holder insert in front of the serial number
 *
 * @return :
 * return the QStringList after mapping
 *
 * @example :
 *  QStringList result = create_serial_name("cow", ".jpg", 3, 0, 200);
 *  for(QString const &data : result)
 *  qDebug() << data; // "cow000.jpg, cow001.jpg, cow002.jpg, ..., cow199.jpg"
 */
QStringList create_serial_name(QString const &new_name, QString const &suffix, int initial, int end, int space, QChar place_holder)
{
    QStringList result;
    result.reserve(abs(end - initial)); //compiler do not support std::abs(int) yet

    for(int i = initial; i != end; ++i){
        result << (new_name + number_mapper(i, space, place_holder) + suffix);
    }

    return result;
}

/**
 * @brief get the past after positions of the first QString "start" and the distance between "start" and "end"
 *
 * example :
 *
 * QString const name = "iiiia href = lllll]]]]";
 * auto index = qstring_position_surround_with(name, "iiii", "]]]]");
 * //index.first = 4(position of a), index.second = 14(distance between "end" and "start")
 * //index.first + index.second = ']'(first position of "]]]]")
 */
std::pair<int, int> qstring_position_surround_with(QString const &target, QString const &start, QString const &end)
{
    int const start_position = target.indexOf(start) + start.size();
    int const end_position = target.indexOf(end, start_position);

    return std::make_pair(start_position, end_position - start_position);
}

/**
 * @brief change the integer into serial number which the length do not less
 * than the number of space
 *
 * @param value : value of the serial number
 * @param space : the minimum space of the serial number has to occupy
 * @param place_holder : the place holder insert in front of the serial number if the
 *  length of the number generated are smaller than the param "space"
 *
 * @return :
 * return the QString after mapping
 *
 * @example :
 *  QString result = number_mapper(3);
 *  qDebug() << result; // "000"
 */
QString number_mapper(int value, int space, QChar place_holder)
{
    QString number;
    number.setNum(value);
    auto const size = space - number.length();

    if(size > 0)
        number.prepend(QString(size, place_holder));

    return number;
}

/**
 * @brief change the integer into serial number which the length do not less
 * than the number of space
 *
 * @param initial : initial value of the serial number
 * @param end : the biggest value of the serial number
 * @param space : the minimum space of the serial number has to occupy
 * @param place_holder : the place holder insert in front of the serial number if the
 *  length of the number generated are smaller than the param "space"
 *
 * @return :
 *  return the QStringList after mapping
 *
 * @example :
 *  QStringList result = number_mapper(0, 200, 3);
 *  for(QString const &data : result)
 *  qDebug() << data; // "000, 001, 002, ..., 199"
 */
QStringList number_mapper(int initial, int end, int space, QChar place_holder)
{
    QStringList result;
    result.reserve(abs(end - initial)); //compiler do not support std::abs(int) yet
    for(int i = initial; i != end; ++i)
        result << number_mapper(i, space, place_holder);

    return result;
}

/**
 * @brief split the QString by a single wrap around
 *
 * ex :
 * QString data = "aaa-bbb-###-##, ##";
 * QStringList result = split_str_by_wrap_around('#', data);
 * //result = "aaa-bbb-", "###", "-", "##", " ", "##"
 *
 */
QStringList split_str_by_wrap_around(QChar wrap_around, QString const &contents)
{
    typedef std::pair<int, int> DInt; //double int type
    typedef std::vector<DInt> RType; //return type
    auto positions = get_positions<RType>(QRegularExpression(QStringLiteral("(") + wrap_around + QStringLiteral("+)")), contents);
    auto positions2 = get_positions<RType>(QRegularExpression("([^" + QString(wrap_around) + "]+)"), contents);
    //positions.merge(positions2, [](DInt const &one, DInt const &two){ return one.first < two.first; });

    positions.reserve(positions.size() + positions2.size());
    std::copy(std::begin(positions2), std::end(positions2), std::back_inserter(positions));
    std::sort(std::begin(positions), std::end(positions), [](DInt const &one, DInt const &two){ return one.first < two.first; });

    return split_str_by_positions(positions, contents);
    //return QStringList();
}

/**
 * @brief transform symbol to number, same as words_to_number but pass in a lot of QString by QStringList, the result
 * will be concatenate
 *
 * ex :
 * QStringList target = {"###", "_ppool_", "#####", "%^&*"};
 * QString result = words_to_number(QRegExp("#+"), target, 9999);
 * //result = "9999_ppool_09999%^&*"
 */
QString words_to_number(QRegExp const &symbol_to_number, QStringList const &target, int number, QChar place_holder)
{
    QString result;
    for(QString const &data : target){
        result += words_to_number(symbol_to_number, data, number, place_holder);
    }

    return result;
}

/**
 * @brief transform symbol to number, pass in a lot of QString by QStringList, the result
 * will be concatenate
 * @param symbol_to_number : symbol need to transform to number
 * @param target The string : need to transform to number
 * @param number The number : of the symbol need to transform to
 * @param place_holder : the place holder insert in front of the serial number if the
 *  length of the number generated are smaller than the param "space"
 *
 * @return
 *  The QString after conversion
 *
 * ex :
 * QStringList target = {"###", "_ppool_", "#####", "%^&*"};
 * QString result = words_to_number(QRegExp("(#+)"), target, 9999);
 * //result = "9999_ppool_09999%^&*"
 */
QString words_to_number(QRegularExpression const &symbol_to_number, QStringList const &target, int number, QChar place_holder)
{
    QString result;
    for(QString const &data : target){
        result += words_to_number(symbol_to_number, data, number, place_holder);
    }

    return result;
}
