#include <QStringList>

#include "fileAuxiliary.hpp"
#include "qstringAlgo.hpp"

namespace{

/*template<typename T>
QString generate_noduplicate_name_impl(QString const &file_path, QString const &file_name, T unaryFunctor)
{
    QDir const dir(file_path);
    size_t index = 0;
    QFileInfo const info(file_name);
    QString const temporary = info.completeBaseName() + "_";
    QString const suffix = "." + info.suffix();
    QString new_file_name = file_name;
    while(dir.exists(new_file_name)){
        new_file_name = temporary + unaryFunctor(index) + suffix;
        ++index;
    }

    return file_path + "/" + new_file_name;
}*/

}

/**
 * @brief get all of name of the files at the absolute path
 *
 * @param current_path : path of the current directory
 * @param filter : filtering options
 */
QStringList get_file_names(QString const &current_path, QDir::Filter filter)
{
    QDir dir(current_path);
    dir.setFilter(filter);
    auto const file_info = dir.entryInfoList();
    QStringList result;
    result.reserve(file_info.size());

    for(auto const &data : file_info){
        result << data.fileName();
    }

    return result;
}

QString generate_noduplicate_name(QString const &file_path, QString const &file_name)
{
    int hint = 0;
    return generate_noduplicate_name(file_path, file_name, hint);
}

/**
 * @brief generate non-duplicate file name if there exist the same file name
 *
 * @param file_path : the file path of the file
 * @param file_name : the duplicate(maybe) file name
 * @param hint: initial number of the file name, could reduce the times of iterative
 *  if assign it properly
 *
 * @return :
 *  if file_name = "abc.jpg" exist, the function will return file name as "abc_0.jpg"
 *  generally speaking, if "abc_n.jpg" exist, will return  "abc_(n + 1).jpg"
 */
QString generate_noduplicate_name(QString const &file_path, QString const &file_name, int &hint)
{    
    QDir const dir(file_path);
    int index = hint;
    QFileInfo const info(file_name);
    QString const temporary = info.completeBaseName() + "_";
    QString const suffix = "." + info.suffix();
    QString new_file_name = file_name;
    while(dir.exists(new_file_name)){
        new_file_name = temporary + QString::number(index) + suffix;
        ++index;
    }

    hint = index;

    return file_path + "/" + new_file_name;

    //return generate_noduplicate_name_impl(file_path, file_name, [](int index){return QString::number(index);});
}

QString generate_noduplicate_number_name(QString const &file_path, QString const &file_name, int space)
{
    int hint = 0;
    return generate_noduplicate_number_name(file_path, file_name, space, hint);
}

/**
 * @brief generate non-duplicate file name if there exist the same file name
 *
 * @param file_path : the file path of the file
 * @param file_name : the duplicate(maybe) file name
 * @param space : the minimum space of the serial number has to occupy
 * @param hint: initial number of the file name, could reduce the times of iterative
 *  if assign it properly
 *
 * @return :
 *  if file_name = "abc.jpg" exist, the function will return file name as "abc_0.jpg"
 *  generally speaking, if "abc_n.jpg" exist, will return  "abc_(n + 1).jpg"
 */
QString generate_noduplicate_number_name(QString const &file_path, QString const &file_name, int space, int &hint)
{
    QDir const dir(file_path);
    size_t index = hint;
    QFileInfo const info(file_name);
    QString const temporary = info.completeBaseName() + "_";
    QString const suffix = "." + info.suffix();
    QString new_file_name = temporary + number_mapper(index, space) + suffix;
    while(dir.exists(new_file_name)){
        new_file_name = temporary + number_mapper(index, space) + suffix;
        ++index;
    }

    hint = index;

    return file_path + "/" + new_file_name;

    //return generate_noduplicate_name_impl(file_path, file_name, [=](int index){return number_mapper(index, space);});
}
