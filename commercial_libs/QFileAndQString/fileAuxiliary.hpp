#ifndef FILE_AUXILIARY_HPP
#define FILE_AUXILIARY_HPP

#include <QDir>
#include <QFileInfo>
#include <QString>

class QStringList;

QStringList get_file_names(QString const &current_path, QDir::Filter filter = QDir::AllDirs);

QString generate_noduplicate_name(QString const &file_path, QString const &file_name);
QString generate_noduplicate_name(QString const &file_path, QString const &file_name, int &hint);
QString generate_noduplicate_number_name(QString const &file_path, QString const &file_name, int space);
QString generate_noduplicate_number_name(QString const &file_path, QString const &file_name, int space, int &hint);

/**
 *@brief replace the suffix of the file name
 *
 *@param fine_name : name of the file
 *@param suffix : suffix want to append on the file_name
 *
 *@return :
 * ex : replace_file_suffix("C:Qt/commercial/abcd.ggg.jpg", "bmp")
 * will return "C:Qt/commercial/abcd.ggg.bmp"
 */
inline QString replace_file_suffix(QString const &file_name, QString const &suffix)
{
    return file_name.left(file_name.size() - QFileInfo(file_name).suffix().size()) + suffix;
}

#endif // FILE_AUXILIARY_HPP
