TEMPLATE = app

#CONFIG += console
CONFIG += core
#DEFINES += DEBUG_OK

win32{
QMAKE_CXXFLAGS += -std=c++0x
}
mac{
LIBS += -stdlib=libc++

QMAKE_CXXFLAGS += -stdlib=libc++
QMAKE_CXXFLAGS += -std=c++11
QMAKE_CXXFLAGS += -mmacosx-version-min=10.7
QMAKE_LFLAGS += -mmacosx-version-min=10.7
}

QMAKE_CXXFLAGS += -Woverloaded-virtual

SOURCES += main.cpp \        
    qstringAlgo.cpp \
    fileAuxiliary.cpp

HEADERS += \    
    qstringAlgo.hpp \
    fileAuxiliary.hpp

FORMS +=

