#ifndef STRING_ALGO_HPP
#define STRING_ALGO_HPP

#include <vector>
#include <utility>

#include <QRegularExpression>
#include <QString>
#include <QStringList>

/**
 *
 * The functions with QRegExp are kept for backward compatibility, QRegularExpression is a
 * much more better solution the QRegExp
 *
 **/

QString number_mapper(int value, int space = 3, QChar place_holder = '0');

QStringList number_mapper(int initial, int end, int space, QChar place_holder = '0');

/**
 *@brief record all of the positions of "contents" which match the "rx"
 *
 * @param rx the symbols want to match(ex : (#+))
 * @param contents the contents want to match
 *
 * example :
 * QString contents = "lll ### ## ####";
 *
 * typedef std::pair<int, int> DInt; //first value is the position of the symbol, second value is "how many symbol"
 * auto const positions = get_positions<std::vector<DInt> >(QRegExp("#+"), contents);
 * //result are (4, 3), (8, 2), (11, 4)
 */
template<typename T = std::vector<std::pair<int, int> > >
T get_positions(QRegExp const &rx, QString const &contents)
{
    int pos = 0;
    T result;
    while((pos = rx.indexIn(contents, pos)) != -1){
        result.emplace_back(pos, rx.matchedLength());
        pos += rx.matchedLength();
    }

    return result;
}

/**
 * @brief overload version with QRegularExpression
 *
 * @param rx the symbols want to match(ex : (#+))
 * @param contents the contents want to match
 *
 * example :
 * QString contents = "lll ### ## ####";
 *
 * typedef std::pair<int, int> DInt; //first value is the position of the symbol, second value is "how many symbol"
 * auto const positions = get_positions<std::vector<DInt> >(QRegExp("(#+)"), contents);
 * //result are (4, 3), (8, 2), (11, 4)
 *
 * Cautions!!!
 * The param "rx" must wrapped by "()", this is the different with the old get_positions with QRegExp
 * In the Qt5 codes, this function should be considerered rather than the old one
 */
template<typename T = std::vector<std::pair<int, int> > >
T get_positions(QRegularExpression const &rx, QString const &contents)
{
    T result;
    QRegularExpressionMatchIterator i = rx.globalMatch(contents);
    while (i.hasNext()) {
        QRegularExpressionMatch const match = i.next();
        int const start_offset = match.capturedStart(1);
        int const end_offset = match.capturedEnd(1);
        result.emplace_back(start_offset, end_offset - start_offset);
    }

    return result;
}

/**
 *@brief change the integer into serial number which the length do not less
 * than the number of space
 *
 * @param new_name : new name of the file(s)
 * @param suffix : like "txt", "jpg", "png" and so on
 * @param number : number want to append on the new name
 * @param space : the minimum space the serial number has to occupy
 * @param place_holder : the place holder insert in front of the serial number
 *
 * @return :
 * return the QStringList after mapping
 *
 * @example :
 *  QString result = create_serial_name("cow", ".jpg", 3, 3);
 *  //result == "cow003.jpg"
 */
inline QString create_serial_name_single(QString const &new_name, QString const &suffix, int number, int space = 3, QChar place_holder = '0')
{
    return QString(new_name + number_mapper(number, space, place_holder) + suffix);
}

QStringList create_serial_name(QString const &new_name, QString const &suffix, int initial, int end, int space = 3, QChar place_holder = '0');

std::pair<int, int> qstring_position_surround_with(QString const &target, QString const &start, QString const &end);

/**
 * @split the string specified by the param position
 *
 * @param position combine by two params, first and second, first indicate
 *  position of the QChar, second indicate the length of the QChar
 * @param contents QString need to split
 */
template<typename T>
QStringList split_str_by_positions(T const &position, QString const &contents)
{
    QStringList results;
    for(std::pair<int, int> const &data : position){
        results << contents.mid(data.first, data.second);
    }

    return results;
}

QStringList split_str_by_wrap_around(QChar wrap_around, QString const &contents);

/**
 * @brief change the words which match the regular expression into number
 *
 * @param symbol_to_number : the symbol will be convert to "number"(ex : ###)
 * @target : the symbol need to transform to param number
 * @number : number what to transform to
 * @param place_holder : the place holder insert in front of the serial number if the
 *  length of the number generated are smaller than the param "target"
 *
 * @example :
 *  QString result = words_to_number(QRegExp("#+"), "###", 30);
 *  // result : 030
 *
 *  QString result2 = words_to_number(QRegExp("#+"), "###", 300);
 *  // result2 : 300
 */
inline QString words_to_number(QRegExp const &symbol_to_number, QString const &target, int number, QChar place_holder = '0')
{    
    if(!target.contains(symbol_to_number)) return target;

    return number_mapper(number, target.size(), place_holder);
}

/**
 * @brief change the words which match the regular expression into number
 *
 * @param symbol_to_number : the symbol will be convert to "number"(ex : ###)
 * @target : the symbol need to transform to param number
 * @number : number what to transform to
 * @param place_holder : the place holder insert in front of the serial number if the
 *  length of the number generated are smaller than the param "target"
 *
 * @example :
 *  QString result = words_to_number(QRegExp("(#+)"), "###", 30);
 *  // result : 030
 *
 *  QString result2 = words_to_number(QRegExp("(#+)"), "###", 300);
 *  // result2 : 300
 */
inline QString words_to_number(QRegularExpression const &symbol_to_number, QString const &target, int number, QChar place_holder = '0')
{
    if(!target.contains(symbol_to_number)) return target;

    return number_mapper(number, target.size(), place_holder);
}

QString words_to_number(QRegExp const &symbolToNumber, QStringList const &target, int number, QChar place_holder = '0');

QString words_to_number(QRegularExpression const &symbolToNumber, QStringList const &target, int number, QChar place_holder = '0');

#endif // STRING_ALGO_HPP
