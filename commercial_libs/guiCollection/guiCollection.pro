#-------------------------------------------------
#
# Project created by QtCreator 2012-08-28T11:03:34
#
#-------------------------------------------------

QT       += core gui


greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = guiCollection
TEMPLATE = app

CONFIG += console
DEFINES += DEBUG_OK

QMAKE_CXXFLAGS += -std=c++0x

INCLUDEPATH += ../timeConverter

SOURCES += main.cpp \
    autoUpdateWidget.cpp

HEADERS  += \
    autoUpdateWidget.hpp \
    sdiMainWindow.hpp
