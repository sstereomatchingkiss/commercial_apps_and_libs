#ifndef AUTOUPDATEWIDGET_HPP
#define AUTOUPDATEWIDGET_HPP

#include <QtCore/QTimer>

#include <QtGui/QWidget>

class QGroupBox;
class QLineEdit;
class QPushButton;

class autoUpdateWidget : public QWidget
{
    Q_OBJECT
public:
    explicit autoUpdateWidget(QWidget *parent = 0);

public:
    bool get_update_flag() const { return is_auto_update_; }

    void pause_count();

    void resume_count();

    void set_group_name(QString const &name);
    void set_interval(int interval) { interval_ = interval; }
    void set_update_frequency(int h = 0, int m = 10, int s = 0);

signals:    
    void remain_second_is_zero();

    void start_to_count();   
    
private slots:
    void start_count();
    void stop_count();

    void update_remain_time();

private:
    void create_connection();
    void create_layout();
    void create_widget();

    bool is_legal_range(int h, int m, int s);

    void stop_count_implement();

private:
    QGroupBox   *auto_update_group_box_;    

    int          interval_;
    bool         is_auto_update_;      

    int          remain_second_;
    QLineEdit   *remain_time_line_edit_;

    QPushButton *start_button_;
    QPushButton *stop_button_;

    QTimer       timer_;

    QLineEdit   *update_frequency_hour_;   
    QLineEdit   *update_frequency_min_;    
    QLineEdit   *update_frequency_sec_;
    
};

#endif // AUTOUPDATEWIDGET_HPP
