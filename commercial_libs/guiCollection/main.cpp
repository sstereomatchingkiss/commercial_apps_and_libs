#include <QApplication>

#include "autoUpdateWidget.hpp"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    autoUpdateWidget widget;
    widget.show();
    
    return a.exec();
}
