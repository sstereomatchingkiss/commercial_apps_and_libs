#ifndef SDIMAINWINDOW_HPP
#define SDIMAINWINDOW_HPP

#include <QtGui/QMainWindow>

/*
 * A simple gui class to show some sdi widget
 * I designed this class for showing debug message
 * from the start(create simple log)
 *
 * @param
 *  T : the widget you want to place as the central widget of the QMainWindow
 */
template<typename T>
class sdiMainWindow : public QMainWindow
{    
public:
    explicit sdiMainWindow(QWidget *parent = nullptr);
    ~sdiMainWindow();
    sdiMainWindow(sdiMainWindow const&) = delete;
    sdiMainWindow& operator=(sdiMainWindow const&) = delete;

    T &operator*()  const { return *central_widget_;}
    T *operator->() const { return central_widget_;}

private:
    T *central_widget_;
    bool parent_is_empty_;
    
};

template<typename T>
sdiMainWindow<T>::sdiMainWindow(QWidget *parent) :
    QMainWindow(parent), central_widget_(new T(this) ),
    parent_is_empty_(parent == nullptr ? true : false)
{
    setCentralWidget(central_widget_);
}

template<typename T>
sdiMainWindow<T>::~sdiMainWindow()
{
    if(parent_is_empty_){
        delete central_widget_;
        central_widget_ = 0;
    }
}

#endif // SDIMAINWINDOW_HPP
