#ifdef DEBUG_OK
#include <QtCore/QDebug>
#endif

#include <memory>

#include <QtCore/QString>

#include <QtGui/QFormLayout>
#include <QtGui/QGridLayout>
#include <QtGui/QGroupBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QIntValidator>
#include <QtGui/QLineEdit>
#include <QtGui/QMessageBox>
#include <QtGui/QPushButton>
#include <QtGui/QRegExpValidator>
#include <QtGui/QWidget>

#include "autoUpdateWidget.hpp"
#include "timeAuxiliary.hpp"

autoUpdateWidget::autoUpdateWidget(QWidget *parent) :
    QWidget(parent), interval_(1000), is_auto_update_(false), remain_second_(0)
{
    create_widget();
    create_layout();
    create_connection();

    timer_.setInterval(interval_);
}

void autoUpdateWidget::pause_count()
{
    timer_.stop();
}

/*
 * resume the counting job repeatedly if
 * is_auto_update_ is true
 */
void autoUpdateWidget::resume_count()
{
    if(remain_second_ > 0)
    {
      timer_.start(interval_);
      return;
    }

    if(is_auto_update_) start_count();
}

void autoUpdateWidget::set_group_name(QString const &name)
{
    auto_update_group_box_->setTitle(name);
}

void autoUpdateWidget::set_update_frequency(int h, int m, int s)
{
    if(!is_legal_range(h, m, s))
    {
        QMessageBox::warning(this, tr("out of range"), "biggest value is 99:59:59(hh:mm:ss)");
        return;
    }
    update_frequency_hour_->setText(QString::number(h));
    update_frequency_min_->setText(QString::number(m));
    update_frequency_sec_->setText(QString::number(s));
}

/**********************************************************
 ****************** implementation ************************
 **********************************************************/

void autoUpdateWidget::create_connection()
{
    connect(start_button_, SIGNAL(clicked()), this, SLOT(start_count()) );
    connect(stop_button_, SIGNAL(clicked()), this, SLOT(stop_count()) );
    connect(&timer_, SIGNAL(timeout()), this, SLOT(update_remain_time()) );
}

void autoUpdateWidget::create_layout()
{
    std::unique_ptr<QFormLayout> form_layout(new QFormLayout);
    form_layout->addRow(tr("Set hour"), update_frequency_hour_);
    form_layout->addRow(tr("Set minute"), update_frequency_min_);
    form_layout->addRow(tr("Set second"), update_frequency_sec_);
    form_layout->addRow(tr("Remain time(hh:mm:ss)"), remain_time_line_edit_);

    std::unique_ptr<QGridLayout> auto_update_layout(new QGridLayout);
    auto_update_layout->addLayout(form_layout.release(), 0, 0, 4, 2);
    auto_update_layout->addWidget(start_button_, 0, 2);
    auto_update_layout->addWidget(stop_button_, 1, 2);

    auto_update_group_box_->setLayout(auto_update_layout.release() );

    QHBoxLayout *layout = new QHBoxLayout(this);
    layout->addWidget(auto_update_group_box_);
    setLayout(layout);
}

void autoUpdateWidget::create_widget()
{
    auto_update_group_box_ = new QGroupBox(this);
    set_group_name("Set update frequency");

    remain_time_line_edit_ = new QLineEdit("00:00:00", this);
    remain_time_line_edit_->setReadOnly(true);

    start_button_ = new QPushButton("Start", this);
    stop_button_ = new QPushButton("Stop", this);

    update_frequency_hour_ = new QLineEdit(this);
    update_frequency_min_ = new QLineEdit(this);
    update_frequency_sec_ = new QLineEdit(this);
    update_frequency_hour_->setValidator(new QIntValidator(0, 99, this) );
    update_frequency_min_->setValidator(new QIntValidator(0, 59, this) );
    update_frequency_sec_->setValidator(new QIntValidator(0, 59, this) );   

    set_update_frequency(0, 10, 0);
}

bool autoUpdateWidget::is_legal_range(int h, int m, int s)
{
    if(h > 99 || h < 0 || m > 59 || m < 0 || s > 59 || s < 0) return false;

    return true;
}

void autoUpdateWidget::stop_count_implement()
{
    timer_.stop();
    remain_second_ = 0;
    remain_time_line_edit_->setText(second_to_hhmmss(remain_second_) );
}

void autoUpdateWidget::start_count()
{        
    is_auto_update_ = true;    
    remain_second_ = hhmmss_to_second(update_frequency_hour_->text().toInt(),
                                      update_frequency_min_->text().toInt(),
                                      update_frequency_sec_->text().toInt() );
    if(remain_second_ <= 0) return;
    remain_time_line_edit_->setText(second_to_hhmmss(remain_second_) );

    timer_.start(interval_);
    emit start_to_count();
}

void autoUpdateWidget::stop_count()
{
    is_auto_update_= false;
    stop_count_implement();
}

void autoUpdateWidget::update_remain_time()
{
    remain_second_ -= interval_ / 1000;

    if(remain_second_ >= 0){
      remain_time_line_edit_->setText(second_to_hhmmss(remain_second_) );     
    }

    if(remain_second_ < 0)
    {
        stop_count_implement();
        emit remain_second_is_zero();        
    }
}
