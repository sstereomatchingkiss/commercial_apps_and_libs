#ifndef STRING_ALGO_HPP
#define STRING_ALGO_HPP

#include <vector>
#include <utility>

#include <QRegularExpression>
#include <QString>
#include <QStringList>

/**
 *
 * The functions with QRegExp are kept for backward compatibility, QRegularExpression is a
 * much more better solution the QRegExp
 *
 **/

namespace SCode
{

QString number_mapper(int value, int space = 3, QChar place_holder = '0');

QStringList number_mapper(int initial, int end, int space, QChar place_holder = '0');

QString create_serial_name_single(QString const &new_name, QString const &suffix, int number, int space = 3, QChar place_holder = '0');

QStringList create_serial_name(QString const &new_name, QString const &suffix, int initial, int end, int space = 3, QChar place_holder = '0');

std::pair<int, int> qstring_position_surround_with(QString const &target, QString const &start, QString const &end);

QStringList split_str_by_wrap_around(QChar wrap_around, QString const &contents);

QString words_to_number(QRegularExpression const &symbol_to_number, QString const &target, int number, QChar place_holder = '0');

QString words_to_number(QRegularExpression const &symbolToNumber, QStringList const &target, int number, QChar place_holder = '0');

}

#endif // STRING_ALGO_HPP
