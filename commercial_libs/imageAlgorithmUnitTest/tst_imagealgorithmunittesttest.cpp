#include <numeric>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <QImage>
#include <QList>
#include <QtTest>

#include "basicImageAlgo.hpp"
#include "basicImageAlgoFunctor.hpp"
#include "imageResize/bicubic/bicubicInterpolation.hpp"
#include "utility/dimensionMapper.hpp"
#include "imageResize/hqx/hqxAlgo.hpp"
#include "imageResize/hqx/hqxInterpolationBase.hpp"
#include "imageAlgorithm.hpp"
#include "openCVToQt.hpp"
#include "utility/keepAspectRatio.hpp"
#include "imageResize/resizeAlgo.hpp"
#include "timeEstimate.hpp"
#include "variadicTemplateAlgo.hpp"


class ImageAlgorithmUnitTestTest : public QObject
{
    Q_OBJECT
    
public:
    ImageAlgorithmUnitTestTest();
    
private Q_SLOTS:
    void test_add_channel();
    void test_compare_channels();
    void test_copy_to_one_dim_array();
    void test_dimension_mapper();
    void test_for_each_channels();
    void test_for_each_channels_binary();
    void test_for_each_continuous_channels();
    void test_for_each_continuous_channel();
    void test_get_pointer();
    void test_index_helper();
    void test_process_three_channels();
    void test_process_four_channels();
    void test_region_algo_channel();
    void test_region_algo_channel2();
    void test_region_deviation();
    void test_region_mean();
    void test_region_size();
    //void test_rgb_white_balance();
    void test_staircase_interpolation();
    void test_staircase_percent();
    void test_transform_channel_binary();
    void test_transform_channel_unary();
    void test_transform_channels_binary();
    void test_transform_channels_unary();
    void test_transform_continuous_channels_unary();
    void test_transform_three_channels();

private:
    template<typename T>
    cv::Mat create_mat(int row, int col, int type)
    {
        cv::Mat result(row, col, type);
        OCV::initialize_mat<T>(result);

        return result;
    }

private :
    char const *name[2];
};

ImageAlgorithmUnitTestTest::ImageAlgorithmUnitTestTest() : QObject(0), name{":/icons/HappyFish.jpg", ":/icons/lena.png"}
{

}

void ImageAlgorithmUnitTestTest::test_add_channel()
{
    cv::Mat_<cv::Vec3b> src(3, 3, 100);
    cv::Mat temp = src;
    OCV::add_channel(temp, 255, 0);
    OCV::add_channel(temp, -255, 1);
    OCV::add_channel(temp, 3600, 2);

    for(int i = 0; i != src.rows; ++i){
        for(int j = 0; j != src.cols; ++j)
        {
            QCOMPARE(src(i, j)[0], uchar(255));
            QCOMPARE(src(i, j)[1], uchar(0));
            QCOMPARE(src(i, j)[2], uchar(255));
        }
    }
}

void ImageAlgorithmUnitTestTest::test_compare_channels()
{
    cv::Mat_<int> A(3, 3);
    OCV::initialize_mat(A);
    int index = 0;
    cv::Mat temp_A = A;
    bool result = OCV::compare_channels<int>(temp_A, [&](int a){ return a == index++;});
    QCOMPARE(true, result);

    cv::Mat_<int> B(3, 3);
    OCV::initialize_mat(B);
    cv::Mat temp_B = B;
    result = OCV::compare_channels<int>(temp_A, temp_B);
    QCOMPARE(true, result);
}

void ImageAlgorithmUnitTestTest::test_copy_to_one_dim_array()
{    

    cv::Mat_<int> A(3, 3);
    OCV::initialize_mat(A);
    std::vector<int> A_out = OCV::copy_to_one_dim_array(A);
    std::vector<int> A_result{0, 1, 2, 3, 4, 5, 6, 7, 8};

    for(size_t i = 0; i != A_out.size(); ++i){
        QCOMPARE(A_out[i], A_result[i]);
    }

    cv::Mat A2(3, 3, CV_8U);
    OCV::initialize_mat<uchar>(A2);
    std::vector<uchar> A2_out = OCV::copy_to_one_dim_array<uchar>(A2);
    std::vector<uchar> A2_result{0, 1, 2, 3, 4, 5, 6, 7, 8};
    //QCOMPARE(A2_out, A2_result);
    for(size_t i = 0; i != A2_out.size(); ++i){
        QCOMPARE(A2_out[i], A2_result[i]);
    }

    cv::Mat_<cv::Vec3i> B(3, 3);
    OCV::initialize_mat(B);
    std::vector<int> B_out = OCV::copy_to_one_dim_array_ch(B, 0);
    std::vector<int> B_result{0, 3, 6, 9, 12, 15, 18, 21, 24};
    //QCOMPARE(B_out, B_result);
    for(size_t i = 0; i != B_out.size(); ++i){
        QCOMPARE(B_out[i], B_result[i]);
    }

    B_out = OCV::copy_to_one_dim_array_ch(B, 1);
    B_result = std::vector<int>{1 , 4 , 7 , 10 , 13 , 16 , 19 , 22 , 25};
    //QCOMPARE(B_out, B_result);
    for(size_t i = 0; i != B_out.size(); ++i){
        QCOMPARE(B_out[i], B_result[i]);
    }

    B_out = OCV::copy_to_one_dim_array_ch(B, 2);
    B_result = std::vector<int>{2 , 5 , 8 , 11 , 14 , 17 , 20 , 23 , 26};
    //QCOMPARE(B_out, B_result);
    for(size_t i = 0; i != B_out.size(); ++i){
        QCOMPARE(B_out[i], B_result[i]);
    }
}

void ImageAlgorithmUnitTestTest::test_dimension_mapper()
{
    for(size_t k = 3; k != 5; ++k){
        size_t channel = k;
        int index = 0;
        size_t const row_and_col = 3;
        OCV::mapDimTwo2One mapper(channel, row_and_col * channel);
        for(size_t i = 0; i != row_and_col; ++i){
            for(size_t j = 0; j != row_and_col; ++j){
                for(size_t c = 0; c != channel; ++c){
                    QCOMPARE(mapper(i, j, c), index++);
                }
            }
        }
    }
}

void ImageAlgorithmUnitTestTest::test_for_each_channels()
{
    cv::Mat input = create_mat<int>(3, 5, CV_32SC3);
    OCV::for_each_channels_impl<int>(input, [](int &a){ ++a; });

    std::vector<int> results = OCV::copy_to_one_dim_array<int>(input);
    std::vector<int> expected(results.size());
    std::iota(std::begin(expected), std::end(expected), 1);
    for(size_t i = 0; i != input.total(); ++i){
        QCOMPARE(results[i], expected[i]);
    }

    OCV::for_each_channel<int>(input, 0, [](int &a){ a = 0;});
    std::vector<int> results_2 = OCV::copy_to_one_dim_array_ch<int>(input, 0);
    std::vector<int> expected_2(results_2.size(), 0);
    for(size_t i = 0; i != input.total(); ++i){
        QCOMPARE(results_2[i], expected_2[i]);
    }
}

void ImageAlgorithmUnitTestTest::test_for_each_channels_binary()
{
    cv::Mat one = create_mat<uchar>(3, 5, CV_8UC3);
    cv::Mat two = create_mat<uchar>(3, 5, CV_8UC3);

    bool same = true;
    OCV::for_each_channels<uchar>(one, two, [&](uchar a, uchar b)
    {
        if(a != b){
            same = false;
        }
    });

    QCOMPARE(same, true);
}

void ImageAlgorithmUnitTestTest::test_for_each_continuous_channels()
{
    cv::Mat input;
    input.create(5, 3, CV_32SC3);
    typedef int TYPE;
    OCV::initialize_mat<TYPE>(input);

    OCV::for_each_continuous_channels<TYPE>(input, [](TYPE &a){ ++a; });

    std::vector<TYPE> results = OCV::copy_to_one_dim_array<TYPE>(input);
    std::vector<TYPE> expected(results.size());
    std::iota(std::begin(expected), std::end(expected), 1);
    for(size_t i = 0; i != input.total(); ++i){
        QCOMPARE(results[i], expected[i]);
    }
}

void ImageAlgorithmUnitTestTest::test_for_each_continuous_channel()
{
    cv::Mat input;
    input.create(3, 2, CV_32SC3);
    typedef int TYPE;
    OCV::initialize_mat<TYPE>(input);

    OCV::for_each_continuous_channel<TYPE>(input, 0, [](TYPE &a){ ++a; });
    OCV::for_each_continuous_channel<TYPE>(input, 1, [](TYPE &a){ ++a; });
    OCV::for_each_continuous_channel<TYPE>(input, 2, [](TYPE &a){ ++a; });

    std::vector<TYPE> results = OCV::copy_to_one_dim_array<TYPE>(input);
    std::vector<TYPE> expected(results.size());
    std::iota(std::begin(expected), std::end(expected), 1);
    for(size_t i = 0; i != input.total(); ++i){
        QCOMPARE(results[i], expected[i]);
    }
}

void ImageAlgorithmUnitTestTest::test_get_pointer()
{
    cv::Mat one(5, 5, CV_32SC3);
    OCV::initialize_mat<int>(one);

    auto const *one_ptr = OCV::get_pointer<int>(one, 0, 4);
    QCOMPARE(*one_ptr, 12);

    one_ptr = OCV::get_pointer<int>(one, 1);
    QCOMPARE(*one_ptr, 15);

    one_ptr = OCV::get_pointer<int>(one, 0, 4, 1);
    QCOMPARE(*one_ptr, 13);
}

void ImageAlgorithmUnitTestTest::test_process_three_channels()
{
    cv::Mat input;
    input.create(4, 4, CV_32SC3);
    OCV::initialize_mat<int>(input);

    OCV::process_three_channels<int>(input, [](int &a, int &b, int &c)
    {
        ++a; ++b; ++c;
    });

    std::vector<int> results = OCV::copy_to_one_dim_array<int>(input);
    std::vector<int> expected(results.size());
    std::iota(std::begin(expected), std::end(expected), 1);
    for(size_t i = 0; i != input.total(); ++i){
        QCOMPARE(results[i], expected[i]);
    }
}

void ImageAlgorithmUnitTestTest::test_process_four_channels()
{
    cv::Mat input;
    input.create(4, 4, CV_32SC4);
    OCV::initialize_mat<int>(input);

    OCV::process_four_channels<int>(input, [](int &a, int &b, int &c, int &d)
    {
        ++a; ++b; ++c; ++d;
    });

    std::vector<int> results = OCV::copy_to_one_dim_array<int>(input);
    std::vector<int> expected(results.size());
    std::iota(std::begin(expected), std::end(expected), 1);
    for(size_t i = 0; i != input.total(); ++i){
        QCOMPARE(results[i], expected[i]);
    }
}

void ImageAlgorithmUnitTestTest::test_region_algo_channel()
{
    cv::Mat input(4, 4, CV_8U);
    OCV::initialize_mat<uchar>(input);
    std::vector<int> sums;
    try{
        OCV::region_algo_channel<uchar>(input, 0, 2, 2, [&](cv::Mat &img, int channel)
        {
            int sum = 0;
            OCV::for_each_channel<uchar>(img, channel, [&](uchar data)
            {
                sum += data;
            });
            sums.emplace_back(sum);
            //qDebug()<<sum;
        });

        std::vector<int> results{10, 18, 42, 50};
        for(size_t i = 0; i != results.size(); ++i){
            QCOMPARE(results[i], sums[i]);
        }
    }catch(std::exception const &ex){
        std::cerr<<"test_region_algo_channel error : "<<ex.what()<<std::endl;
    }
}

void ImageAlgorithmUnitTestTest::test_region_deviation()
{
    cv::Mat input(4, 4, CV_8U);
    OCV::initialize_mat<uchar>(input);
    std::vector<float> mean{2.5, 4.5, 10.5, 12.5};
    OCV::regionAvgAbsDiff<uchar> region_deviation(mean);

    OCV::region_algo_channel<uchar>(input, 0, 2, 2, std::ref(region_deviation));
    std::vector<float> result{2, 2, 2, 2};
    for(size_t i = 0; i != region_deviation.result_.size(); ++i){
        QCOMPARE(result[i], region_deviation.result_[i]);
    }
}

void ImageAlgorithmUnitTestTest::test_region_algo_channel2()
{
    cv::Mat input(4, 4, CV_8U);
    OCV::initialize_mat<uchar>(input);
    std::vector<int> sums;
    try{
        OCV::region_algo_channel<uchar>(input, 0, OCV::region_size(input, 2, 2), [&](cv::Mat &img, int channel)
        {
            int sum = 0;
            OCV::for_each_channel<uchar>(img, channel, [&](uchar data)
            {
                sum += data;
            });
            sums.emplace_back(sum);
            //qDebug()<<sum;
        });

        std::vector<int> results{10, 18, 42, 50};
        for(size_t i = 0; i != results.size(); ++i){
            QCOMPARE(results[i], sums[i]);
        }
    }catch(std::exception const &ex){
        std::cerr<<"test_region_algo_channel error : "<<ex.what()<<std::endl;
    }
}

void ImageAlgorithmUnitTestTest::test_region_mean()
{
    cv::Mat input(4, 6, CV_8UC3);
    int index = 0;
    auto func = [&](uchar &data){ data = index++;};
    OCV::for_each_channel<uchar>(input, 0, func); index = 0;
    OCV::for_each_channel<uchar>(input, 1, func); index = 0;
    OCV::for_each_channel<uchar>(input, 2, func); index = 0;

    {
        OCV::regionMean<uchar> meanFunctor;
        OCV::region_algo_channel<uchar>(input, 0, 2, 2, std::ref(meanFunctor));
        std::vector<float> result{3.5, 5.5, 7.5, 15.5, 17.5, 19.5};
        for(size_t i = 0; i != result.size(); ++i){
            QCOMPARE(result[i], meanFunctor.result_[i]);
        }
    }

    {
        OCV::regionMean<uchar> meanFunctor;
        OCV::region_algo_channel<uchar>(input, 1, 2, 2, std::ref(meanFunctor));
        std::vector<float> result{3.5, 5.5, 7.5, 15.5, 17.5, 19.5};
        for(size_t i = 0; i != result.size(); ++i){
            QCOMPARE(result[i], meanFunctor.result_[i]);
        }
    }

    {
        OCV::regionMean<uchar> meanFunctor;
        OCV::region_algo_channel<uchar>(input, 2, 2, 2, std::ref(meanFunctor));
        std::vector<float> result{3.5, 5.5, 7.5, 15.5, 17.5, 19.5};
        for(size_t i = 0; i != result.size(); ++i){
            QCOMPARE(result[i], meanFunctor.result_[i]);
        }
    }
}

void ImageAlgorithmUnitTestTest::test_region_size()
{
    cv::Mat input(534, 311, CV_8UC3);

    auto result = OCV::region_size(input, 100, 100);

    /*for(auto data : result.first){
        qDebug()<<"width = "<<data;
    }
    for(auto data : result.second){
        qDebug()<<"height = "<<data;
    }*/

    std::vector<int> real_width{100, 100, 111};
    std::vector<int> real_height{100, 100, 100, 100, 134};

    QCOMPARE(result.first.size(), real_height.size());
    QCOMPARE(result.second.size(), real_width.size());

    for(size_t i = 0; i != real_width.size(); ++i){
        QCOMPARE(result.second[i], real_width[i]);
    }
    for(size_t i = 0; i != real_height.size(); ++i){
        QCOMPARE(result.first[i], real_height[i]);
    }

    cv::Mat input2(100, 100, CV_8U);
    result = OCV::region_size(input2, 50, 50);
    std::vector<int> real_size(2, 50);
    for(size_t i = 0; i != real_size.size(); ++i){
        QCOMPARE(result.first[i], real_size[i]);
        QCOMPARE(result.second[i], real_size[i]);
    }
}

void ImageAlgorithmUnitTestTest::test_index_helper()
{
    OCV::indexHelper<cv::Vec3b> channel_3;

    for(int i = 0; i != 9; ++i){
        QCOMPARE(channel_3.get_index(i), i * 3);
    }

    OCV::indexHelper<cv::Vec4b> channel_4;

    for(int i = 0; i != 9; ++i){
        QCOMPARE(channel_4.get_index(i), i * 4);
    }
}

/*void ImageAlgorithmUnitTestTest::test_rgb_white_balance()
{
    try{
        cv::Mat_<cv::Vec3i> input(2, 2);
        OCV::initialize_mat(input);
        //Mat C = (Mat_<double>(3,3) << 0, -1, 0, -1, 5, -1, 0, -1, 0);
        std::cout<<input<<std::endl;
    }
    catch(std::exception const &ex){
        std::cout<<ex.what()<<std::endl;
    }
}*/

void ImageAlgorithmUnitTestTest::test_staircase_interpolation()
{
    auto const output = OCV::staircase_steps(2480, 1024, 20);
    QList<int> result;
    for(auto const data : output){
        result << data;
    }

    QList<int> expected{2372, 2270, 2171, 2077, 1987, 1901, 1819, 1740, 1665, 1593,
                        1524, 1458, 1395, 1335, 1277, 1222, 1169, 1118, 1070, 1024};

    QCOMPARE(result, expected);
}

void ImageAlgorithmUnitTestTest::test_staircase_percent()
{
    auto const output = OCV::staircase_percent(2480, 1024, 10);
    QList<int> result;
    for(auto const data : output){
        result << data;
    }
    QList<int> expected{2232, 2009, 1809, 1629, 1467, 1321, 1189, 1071, 1024};
    QCOMPARE(result, expected);

    auto const output2 = OCV::staircase_percent(1280, 2480, 10);
    QList<int> result2;
    for(auto const data : output2){
        result2 << data;
    }
    QList<int> expected2{1408, 1548, 1702, 1872, 2059, 2264, 2480};
    QCOMPARE(result2, expected2);
}

void ImageAlgorithmUnitTestTest::test_transform_channel_binary()
{
    typedef int Type;
    cv::Mat input_one = create_mat<Type>(2, 2, CV_32SC3);
    cv::Mat input_two = create_mat<Type>(2, 2, CV_32SC3);
    cv::Mat output = create_mat<Type>(2, 2, CV_32SC3);

    /**
     * input_one and input_two, output
     * 0, 1, 2, 3, 4, 5
     * 6, 7, 8, 9, 10, 11
     */
    OCV::transform_channel<Type>(input_one, input_two, output, 0, [](Type a, Type b)
    {
        return a + b;
    });

    std::vector<Type> result_one = OCV::copy_to_one_dim_array_ch<Type>(output, 0);
    std::vector<Type> expected_one{0, 6, 12, 18};
    for(size_t i = 0; i != result_one.size(); ++i){
        QCOMPARE(result_one[i], expected_one[i]);
    }

    OCV::transform_channel<Type>(input_one, input_two, input_two, 0, [](Type a, Type b)
    {
        return a + b;
    });

    std::vector<Type> result_two = OCV::copy_to_one_dim_array_ch<Type>(input_two, 1);
    std::vector<Type> expected_two{2, 8, 14, 20};
    for(size_t i = 0; i != result_one.size(); ++i){
        QCOMPARE(result_two[i], expected_two[i]);
    }
}

void ImageAlgorithmUnitTestTest::test_transform_channel_unary()
{
    cv::Mat img1(3, 3, CV_32SC3);
    typedef int Type;
    OCV::transform_channel<Type>(img1, 0, [](Type){ return 133;});
    OCV::transform_channel<Type>(img1, 1, [](Type){ return 134;});
    OCV::transform_channel<Type>(img1, 2, [](Type){ return 135;});

    std::vector<Type> expected;
    std::vector<Type> result;
    for(size_t i = 0; i != img1.total(); ++i){
        expected.push_back(133);
        expected.push_back(134);
        expected.push_back(135);
    }

    result = OCV::copy_to_one_dim_array<Type>(img1);
    for(size_t i = 0; i != expected.size(); ++i){
        QCOMPARE(expected[i], result[i]);
    }

    cv::Mat_<cv::Vec3i> img2 = img1;

    OCV::transform_channel(img2, 0, [](Type){ return 133;});
    OCV::transform_channel(img2, 1, [](Type){ return 134;});
    OCV::transform_channel(img2, 2, [](Type){ return 135;});

    result = OCV::copy_to_one_dim_array(img2);
    for(size_t i = 0; i != expected.size(); ++i){
        QCOMPARE(expected[i], result[i]);
    }
}

void ImageAlgorithmUnitTestTest::test_transform_channels_binary()
{
    typedef int Type;
    cv::Mat input_one = create_mat<Type>(2, 2, CV_32SC3);
    cv::Mat input_two = create_mat<Type>(2, 2, CV_32SC3);
    cv::Mat output    = create_mat<Type>(2, 2, CV_32SC3);

    /**
     * input_one and input_two, output
     * 0, 1, 2, 3, 4, 5
     * 6, 7, 8, 9, 10, 11
     */
    OCV::transform_channels<Type>(input_one, input_two, output, [](Type a, Type b)
    {
        return a + b;
    });

    std::vector<Type> result_one = OCV::copy_to_one_dim_array<Type>(output);
    std::vector<Type> expected_one{0,   2,  4,  6,  8, 10,
                                   12, 14, 16, 18, 20, 22};
    for(size_t i = 0; i != result_one.size(); ++i){
        QCOMPARE(result_one[i], expected_one[i]);
    }

}

void ImageAlgorithmUnitTestTest::test_transform_channels_unary()
{
    cv::Mat img1(3, 3, CV_32S);
    OCV::transform_channels<int>(img1, [](int){ return 133; });
    std::vector<int> result = OCV::copy_to_one_dim_array<int>(img1);
    std::vector<int> expected(result.size(), 133);
    for(size_t i = 0; i != result.size(); ++i){
        QCOMPARE(expected[i], result[i]);
    }

    cv::Mat img2(3, 3, CV_32S);
    OCV::transform_channels<int>(img1, img2, [](int a)
    {
        return a;
    });
    std::vector<int> result2 = OCV::copy_to_one_dim_array<int>(img2);
    for(size_t i = 0; i != result.size(); ++i){
        QCOMPARE(expected[i], result2[i]);
    }
}

void ImageAlgorithmUnitTestTest::test_transform_continuous_channels_unary()
{
    typedef int Type;

    cv::Mat input_one = create_mat<Type>(2, 2, CV_32SC3);
    cv::Mat input_two = create_mat<Type>(2, 2, CV_32SC3);
    cv::Mat output = create_mat<Type>(2, 2, CV_32SC3);

    QCOMPARE(input_one.isContinuous(), true);
    QCOMPARE(input_two.isContinuous(), true);
    QCOMPARE(output.isContinuous(), true);

    OCV::transform_continuous_channels<Type>(input_one, input_two, output, [](Type a, Type b)
    {
        return a + b;
    });

    std::vector<Type> result_one = OCV::copy_to_one_dim_array<Type>(output);
    std::vector<Type> expected_one{0,   2,  4,  6,  8, 10,
                                   12, 14, 16, 18, 20, 22};
    for(size_t i = 0; i != result_one.size(); ++i){
        QCOMPARE(result_one[i], expected_one[i]);
    }

    OCV::transform_continuous_channels<Type>(input_one, output, [](Type)
    {
        return 10;
    });

    std::vector<Type> result_two = OCV::copy_to_one_dim_array<Type>(output);
    std::vector<Type> expected_two(12, 10);
    for(size_t i = 0; i != result_two.size(); ++i){
        QCOMPARE(result_two[i], expected_two[i]);
    }
}

void ImageAlgorithmUnitTestTest::test_transform_three_channels()
{
    struct temp
    {
        temp(int value) : value_(value) {}

        int operator()(int data) { return data + value_; }

        int value_;
    };

    cv::Mat input = create_mat<int>(2, 2, CV_32SC3);

    OCV::transform_three_channels<int>(input, temp(0), temp(1), temp(2));
    std::vector<int> const results = OCV::copy_to_one_dim_array<int>(input);
    std::vector<int> const expected = {0, 2, 4,  3, 5, 7,
                                       6, 8, 10, 9, 11, 13};

    for(size_t i = 0; i != results.size(); ++i){
        QCOMPARE(results[i], expected[i]);
    }
}

QTEST_APPLESS_MAIN(ImageAlgorithmUnitTestTest)

#include "tst_imagealgorithmunittesttest.moc"
