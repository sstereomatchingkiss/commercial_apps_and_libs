#-------------------------------------------------
#
# Project created by QtCreator 2013-02-09T15:56:34
#
#-------------------------------------------------

QT       += core gui testlib

TARGET = tst_imagealgorithmunittesttest
CONFIG   += console
CONFIG   += c++11
CONFIG   -= app_bundle

TEMPLATE = app

win32{
INCLUDEPATH += ../../3rdLibs/openCV/OpenCV-2.4.5/build/include

LIBS += -L../../3rdLibs/openCV/OpenCV-2.4.5/build/x86/mingw/lib/ -lopencv_core245 -lopencv_highgui245 -lopencv_imgproc245 -lopencv_photo245
}
mac{
INCLUDEPATH += /usr/local/include

LIBS += -L/usr/local/lib/ -lopencv_core.2.4.5 -lopencv_highgui.2.4.5 -lopencv_imgproc.2.4.5 -lopencv_photo.2.4.5

#INCLUDEPATH += /opt/local/include

#LIBS += -L/opt/local/lib/ -lopencv_core.2.4.6  -lopencv_imgproc.2.4.6 -lopencv_highgui.2.4.6 -lopencv_photo.2.4.6
}

QMAKE_CXXFLAGS += -Woverloaded-virtual

INCLUDEPATH += ../../boost/boost_1_53_0
INCLUDEPATH += ../../commercial_libs/algorithms
INCLUDEPATH += ../../commercial_libs/debugHelper
INCLUDEPATH += ../../commercial_libs/imageAlgorithm
INCLUDEPATH += ../../commercial_libs/OpenCVAndQt

SOURCES += tst_imagealgorithmunittesttest.cpp \
    ../imageAlgorithm/imageAlgorithm.cpp \  
    ../imageAlgorithm/basicImageAlgo.cpp \
    ../imageAlgorithm/imageResize/resizeAlgo.cpp \
    ../imageAlgorithm/genericAlgo/genericTransform.cpp \
    ../imageAlgorithm/genericAlgo/genericRegion.cpp
DEFINES += SRCDIR=\\\"$$PWD/\\\"

HEADERS += \
    ../imageAlgorithm/waterMark.hpp \
    ../imageAlgorithm/imageAlgoUnitTest.hpp \
    ../imageAlgorithm/imageAlgorithm.hpp \
    ../imageAlgorithm/colorConversion.hpp \  
    ../imageAlgorithm/basicImageAlgo.hpp \
    ../imageAlgorithm/imageResize/resizeAlgo.hpp \
    ../imageAlgorithm/basicImageAlgoImpl.hpp \
    ../imageAlgorithm/basicImageAlgoFunctor.hpp \
    ../imageAlgorithm/utility/getDataHelper.hpp \
    ../imageAlgorithm/genericAlgo/genericForEach.hpp \
    ../imageAlgorithm/genericAlgo/genericTransform.hpp \
    ../imageAlgorithm/genericAlgo/genericRegion.hpp

RESOURCES += \
    icons.qrc
