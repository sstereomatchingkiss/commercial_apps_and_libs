#ifndef QIMAGEHELPER_HPP
#define QIMAGEHELPER_HPP


#include <QImage>

class QString;

QImage qimage_4ch_to_other(QImage &input, QImage::Format format = QImage::Format_RGB888);

QString get_qimage_format(QImage const &input);

#endif // QIMAGEHELPER_HPP
