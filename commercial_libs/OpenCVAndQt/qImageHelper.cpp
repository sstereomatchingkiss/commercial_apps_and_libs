#include <QImage>
#include <QString>

#include "qImageHelper.hpp"

/**
 * @brief transform 4 channels qimage to 3 channels qimage
 * @param input : input image
 * @param format : format want to convert to
 * @return the image after transform if input is a 4 channels image; else return the original image
 */
QImage qimage_4ch_to_other(QImage &input, QImage::Format format)
{
    switch(input.format()){
    case QImage::Format_RGB32:
    case QImage::Format_ARGB32:
    case QImage::Format_ARGB32_Premultiplied:
    case QImage::Format_ARGB8565_Premultiplied:
    case QImage::Format_ARGB6666_Premultiplied:
    case QImage::Format_ARGB8555_Premultiplied:
    case QImage::Format_ARGB4444_Premultiplied:
    case QImage::Format_RGB16:
    case QImage::Format_RGB444:
    {
        return input.convertToFormat(format);
    }
    default:
        return input;
    }
}

/**
 * @brief return the qimage format with human friendly string
 * @param input : input image
 * @return the format of the QImage
 */
QString get_qimage_format(QImage const &input)
{
    switch(input.format()){
    case QImage::Format_Invalid:{
        return "QImage::Format_Invalid";
    }
    case QImage::Format_Mono:{
        return "QImage::Format_Mono";
    }
    case QImage::Format_MonoLSB:{
        return "QImage::Format_MonoLSB";
    }
    case QImage::Format_Indexed8:{
        return "QImage::Format_Indexed8";
    }
    case QImage::Format_RGB32:{
        return "QImage::Format_RGB32";
    }
    case QImage::Format_ARGB32:{
        return "QImage::Format_ARGB32";
    }
    case QImage::Format_ARGB32_Premultiplied:{
        return "QImage::Format_ARGB32_Premultiplied";
    }
    case QImage::Format_RGB16:{
        return "QImage::Format_RGB16";
    }
    case QImage::Format_ARGB8565_Premultiplied:{
        return "QImage::Format_ARGB8565_Premultiplied";
    }
    case QImage::Format_RGB666:{
        return "QImage::Format_RGB666";
    }
    case QImage::Format_ARGB6666_Premultiplied:{
        return "QImage::Format_ARGB6666_Premultiplied";
    }
    case QImage::Format_RGB555:{
        return "QImage::Format_RGB555";
    }
    case QImage::Format_ARGB8555_Premultiplied:{
        return "QImage::Format_ARGB8555_Premultiplied";
    }
    case QImage::Format_RGB888:{
        return "QImage::Format_RGB888";
    }
    case QImage::Format_RGB444:{
        return "QImage::Format_RGB444";
    }
    case QImage::Format_ARGB4444_Premultiplied:{
        return "QImage::Format_ARGB4444_Premultiplied";
    }
    default:
        return QString("unknown format");
    }
}
