#include <vector>

#include <QString>
#include <QtTest>

#include "debugHelper.hpp"

class DebugHelperUnitTestTest : public QObject
{
    Q_OBJECT
    
public:
    DebugHelperUnitTestTest();
    
private Q_SLOTS:
    void test_write_and_read_data_to_file();

private:
    std::vector<float> data_;
};

DebugHelperUnitTestTest::DebugHelperUnitTestTest()
{
    for(size_t i = 0; i != data_.size(); ++i){
        data_.emplace_back(i);
    }
}

void DebugHelperUnitTestTest::test_write_and_read_data_to_file()
{
    std::string const file_name("debugHelper");
    write_data_to_file(data_, file_name);
    auto result = read_data_from_file<float>(file_name);
    for(size_t i = 0; i != result.size(); ++i){
        QCOMPARE(result[i], data_[i]);
    }

    write_data_to_file(std::begin(data_), std::end(data_), file_name);
    result = read_data_from_file<float>(file_name);
    for(size_t i = 0; i != result.size(); ++i){
        QCOMPARE(result[i], data_[i]);
    }
}

QTEST_APPLESS_MAIN(DebugHelperUnitTestTest)

#include "tst_debughelperunittesttest.moc"
