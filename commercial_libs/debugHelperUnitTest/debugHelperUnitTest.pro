#-------------------------------------------------
#
# Project created by QtCreator 2013-04-03T08:44:11
#
#-------------------------------------------------

QT       += testlib

QT       -= gui

TARGET = tst_debughelperunittesttest
CONFIG   += console
CONFIG   -= app_bundle

win32{
QMAKE_CXXFLAGS += -std=c++0x
}
mac{
LIBS += -stdlib=libc++

QMAKE_CXXFLAGS += -stdlib=libc++
QMAKE_CXXFLAGS += -std=c++11
QMAKE_CXXFLAGS += -mmacosx-version-min=10.7
QMAKE_LFLAGS += -mmacosx-version-min=10.7
}

INCLUDEPATH += ../debugHelper

TEMPLATE = app


SOURCES += tst_debughelperunittesttest.cpp
DEFINES += SRCDIR=\\\"$$PWD/\\\"

HEADERS += \
    ../debugHelper/debugHelper.hpp
