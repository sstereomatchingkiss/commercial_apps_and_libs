#ifndef KOMICAMODELFUNCTION_HPP
#define KOMICAMODELFUNCTION_HPP

#ifdef DEBUG_OK
#include <QDebug>
#endif

#include <algorithm>
#include <type_traits>

#include <QList>
#include <QString>
#include <QStringList>

#include <QIcon>
#include <QStandardItem>

/*
 * collect some functions to help model/view design
 */
class modelFunction
{
public:   
    /*
     * get string from QStandardItemModel
     */
    template<typename T>
    static QString const get_string(int row, int column, T *source, int role = Qt::DisplayRole)
    {
        if(source->hasIndex(row, column) )
          return source->item(row, column)->data(role).toString();

        return QString();
    }

    template<typename T>
    static QStringList const get_strings(int column, T *source, int role = Qt::DisplayRole);

    template<typename T>
    static void set_icon(int row, int column, QIcon const &icon, T *source)
    {
        set_icon(row, column, icon, "", source);
    }

    template<typename T>
    static void set_icon(int row, int column, QIcon const &icon, QString const &name, T *source)
    {
        source->setItem(row, column, new QStandardItem(icon, name) );
    }

    template<typename VariantType, typename T>
    static void set_data(int column, VariantType const &data, T *source, int role = Qt::DisplayRole);

    template<typename VariantType, typename T>
    static void set_datum(int row, int column, VariantType const &data, T *source, int role = Qt::DisplayRole);

    template<typename VariantType, typename T>
    static void sort_data(int column, bool flag, T *source, int role = Qt::DisplayRole);
};

/*
 * get the strings of specific column
 */
template<typename T>
QStringList const modelFunction::get_strings(int column, T *source, int role)
{
    int const row = source->rowCount();
    QStringList result;
    result.reserve(row);
    for(int i = 0; i != row; ++i)
       result << source->item(i, column)->data(role).toString();

    return result;
}

/*
 * set every kind of data supported by QStandardItemModel except of QIcon, this method
 * will replace the data if it already exist
 */
template<typename VariantType, typename T>
void modelFunction::set_data(int column, VariantType const &data, T *source, int role)
{
    int size = data.size();
    for(int row = 0; row != size; ++row)
        set_datum(row, column, data[row], source, role);
}

/*
 * set every kind of datum supported by QStandardItemModel except of QIcon
 */
template<typename VariantType, typename T>
void modelFunction::set_datum(int row, int column, VariantType const &data, T *source, int role)
{
    static_assert(!std::is_same<QIcon, VariantType>::value, "This function do not support QIcon");

    if(source->hasIndex(row, column, source->invisibleRootItem()->index() ) )
        source->setData(source->index(row, column), data, role);
    else
        source->setItem(row, column, new QStandardItem(data) );
}

/*
 *@brief sort the data of the model
 *
 *@param VariantType : indicate the type of the QVariant
 *@param column : column number of the model
 *@param flag : indicate it is ascending(false) or descending(true)
 *@param source : pointer point to QStandardItemModel and it subclass
 */
template<typename VariantType, typename T>
void modelFunction::sort_data(int column, bool flag, T *source, int role)
{
    typedef QList<QStandardItem*> Items;
    QList<Items> row_items;
    int const row = source->rowCount();
    for(int i = 0; i != row; ++i)
        row_items << source->takeRow(0);

    std::sort(std::begin(row_items), std::end(row_items), [&](Items const &one, Items const &two)->bool
    {
        int const num1 = one[column]->data(role).value<VariantType>();
        int const num2 = two[column]->data(role).value<VariantType>();

        return !flag ? num1 < num2 : num1 > num2;
    } );

    for(int i = 0; i != row; ++i) source->appendRow(row_items[i]);
}

#endif // KOMICAMODELFUNCTION_HPP
