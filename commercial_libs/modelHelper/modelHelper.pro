#-------------------------------------------------
#
# Project created by QtCreator 2012-11-10T18:58:39
#
#-------------------------------------------------

TARGET = modelHelper
TEMPLATE = lib

QT += core gui

CONFIG -= app_bundle

win32{
QMAKE_CXXFLAGS += -std=c++0x
QMAKE_CXXFLAGS += -Wall
}
mac{
LIBS += -stdlib=libc++

QMAKE_CXXFLAGS += -stdlib=libc++
QMAKE_CXXFLAGS += -std=c++11
QMAKE_CXXFLAGS += -mmacosx-version-min=10.7
QMAKE_LFLAGS += -mmacosx-version-min=10.7
}

SOURCES +=

HEADERS += \
    modelFunction.hpp
unix:!symbian {
    maemo5 {
        target.path = /opt/usr/lib
    } else {
        target.path = /usr/lib
    }
    INSTALLS += target
}
