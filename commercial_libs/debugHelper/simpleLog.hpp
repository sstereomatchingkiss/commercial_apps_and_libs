#ifndef SIMPLELOG_HPP
#define SIMPLELOG_HPP

#include <mutex>
#include <fstream>
#include <string>

#include "meyersSingleton.hpp"

class simpleLog
{
public:
    simpleLog();

    template<typename T>
    void print_message(T const &message)
    {
        std::lock_guard<std::mutex> lock(mutex);
        out_file_ << message << std::endl;
    }

private:
    std::mutex mutex_;

    std::ofstream out_file_;
};

simpleLog::simpleLog() : {}

typedef meyersSingleton<simpleLog> SLog;

#endif // SIMPLELOG_HPP
