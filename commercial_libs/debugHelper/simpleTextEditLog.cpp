#ifndef DISABLED_QT

#include <QMutexLocker>
#include <QPushButton>
#include <QString>
#include <QTextEdit>
#include <QVBoxLayout>

#include "simpleTextEditLog.hpp"

simpleTextEditLog::simpleTextEditLog(QWidget *parent) : QDialog(parent)
{
    button_close_ = new QPushButton(tr("Close"), this);

    text_edit_ = new QTextEdit(this);
    text_edit_->setReadOnly(true);

    auto layout = new QVBoxLayout(this);    
    layout->addWidget(text_edit_);
    layout->addWidget(button_close_);

    connect(button_close_, SIGNAL(clicked()), this, SLOT(hide()));
    connect(button_close_, SIGNAL(clicked()), this, SLOT(clean_up()));

    setWindowModality(Qt::ApplicationModal);
    setLayout(layout);
}

void simpleTextEditLog::clear_ths()
{
    QMutexLocker lock(&mutex_);
    text_edit_->clear();
}

void simpleTextEditLog::exec_ths()
{
    QMutexLocker lock(&mutex_);
    show();
}

void simpleTextEditLog::exec_then_clear_ths()
{
    QMutexLocker lock(&mutex_);
    show();
    text_edit_->clear();
}

/**
 * @brief thread safe append, ths means thread safe
 * @param text
 */
void simpleTextEditLog::set_plain_text_ths(QString const &text)
{
    QMutexLocker lock(&mutex_);
    text_edit_->setPlainText(text);
}

void simpleTextEditLog::set_plain_text_and_exec(QString const &text)
{
    QMutexLocker lock(&mutex_);
    text_edit_->setPlainText(text);
    show();
}

/**********************************************************
 ****************** implementation ************************
 **********************************************************/

void simpleTextEditLog::clean_up()
{
    text_edit_->clear();
}

#endif
