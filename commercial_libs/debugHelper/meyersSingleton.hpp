#ifndef MEYERSSINGLETON_HPP
#define MEYERSSINGLETON_HPP

template<typename T, size_t N = 0>
class meyersSingleton
{
public:
  static T& instance()
  {
    static T theSingleton;
    return theSingleton;
  }

  meyersSingleton() = delete;
  ~meyersSingleton() = delete;
  meyersSingleton(meyersSingleton const&) = delete;
  meyersSingleton& operator=(meyersSingleton const&) = delete;

};

#endif // MEYERSSINGLETON_HPP
