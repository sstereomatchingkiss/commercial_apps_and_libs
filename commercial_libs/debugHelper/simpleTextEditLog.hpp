#ifndef SIMPLETEXTEDITLOG_HPP
#define SIMPLETEXTEDITLOG_HPP

#ifndef DISABLED_QT

#include <QMutex>
#include <QDialog>

class QPushButton;
class QString;
class QTextEdit;

class simpleTextEditLog : public QDialog
{
    Q_OBJECT

public:
    simpleTextEditLog(QWidget *parent = nullptr);

    void clear_ths();

    void exec_ths();
    void exec_then_clear_ths();

    void set_plain_text_ths(QString const &text);
    void set_plain_text_and_exec(QString const &text);

private slots:
    void clean_up();

private:
    QPushButton *button_close_;

    QMutex       mutex_;

    QTextEdit   *text_edit_;
};

#endif

#endif // SIMPLETEXTEDITLOG_HPP
