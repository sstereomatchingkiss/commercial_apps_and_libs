#-------------------------------------------------
#
# Project created by QtCreator 2013-04-03T08:22:08
#
#-------------------------------------------------

QT       += concurrent core gui widgets

TARGET = debugHelper
TEMPLATE = lib
CONFIG += staticlib


win32{
QMAKE_CXXFLAGS += -std=c++0x
}
mac{
LIBS += -stdlib=libc++

QMAKE_CXXFLAGS += -stdlib=libc++
QMAKE_CXXFLAGS += -std=c++11
QMAKE_CXXFLAGS += -mmacosx-version-min=10.7
QMAKE_LFLAGS += -mmacosx-version-min=10.7
}

SOURCES += \
    simpleTextEditLog.cpp

HEADERS += \
    debugHelper.hpp \
    bugLocation.hpp \
    simpleLog.hpp \
    meyersSingleton.hpp \
    simpleTextEditLog.hpp
unix:!symbian {
    maemo5 {
        target.path = /opt/usr/lib
    } else {
        target.path = /usr/lib
    }
    INSTALLS += target
}
