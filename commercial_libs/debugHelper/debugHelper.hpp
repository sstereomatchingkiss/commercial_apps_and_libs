#ifndef DEBUGHELPER_HPP
#define DEBUGHELPER_HPP

#include <algorithm>
#include <fstream>
#include <iterator>
#include <string>
#include <vector>

#include "bugLocation.hpp"

/*
 *@brief write the element of data into the file with "file_name"
 *@param begin first position of the data
 *@param end   pass the end position of the data
 *@param file_name name of the file
 *@return true if success, else false
 */
template<typename T>
bool write_data_to_file(T begin, T end, std::string const &file_name)
{
    typedef typename std::iterator_traits<T>::value_type value_type;
    std::ofstream out(file_name.c_str());

    if(out){
        std::copy(begin, end, std::ostream_iterator<value_type>(out, "\n"));

        return true;
    }

    return false;
}

/*
 *@brief write the element of data into the file with "file_name"
 *@param data the data write into the file
 *@param file_name name of the file
 *@return true if success, else false
 */
template<typename T>
inline bool write_data_to_file(T const &data, std::string const &file_name)
{    
    return write_data_to_file(std::begin(data), std::end(data), file_name);
}

/*
 *@brief read the data of file_name
 *@param file_name name of the file
 *@return the data in the file
 */
template<typename T>
std::vector<T> const read_data_from_file(std::string const &file_name)
{
    std::ifstream in(file_name.c_str());
    std::vector<T> result;
    T data;
    if(in){
        while(in>>data){
            result.emplace_back(data);
        }
    }

    return result;
}

#endif // DEBUGHELPER_HPP
