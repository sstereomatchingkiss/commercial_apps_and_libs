uniform sampler2D source;
uniform float hatch_y_offset; // 5.0
uniform float lum_threshold_1; // 1.0
uniform float lum_threshold_2; // 0.7
uniform float lum_threshold_3; // 0.5
uniform float lum_threshold_4; // 0.3

uniform float imageHeight;
uniform float imageWidth;

uniform lowp float qt_Opacity;
varying vec2 qt_TexCoord0;

float lookup2(vec2 p, vec2 offset)
{
    vec3 c = texture2D(source,  p + offset).rgb;

    return dot(c, vec3(0.2126, 0.7152, 0.0722));
}

void main()
{
    vec2 uv = qt_TexCoord0.xy;

    vec3 tc = vec3(1.0, 0.0, 0.0);

    float lum = length(texture2D(source, uv).rgb);
    tc = vec3(1.0, 1.0, 1.0);

    if (lum < lum_threshold_1){
        if (mod(gl_FragCoord.x + gl_FragCoord.y, 10.0) == 0.0)
            tc = vec3(0.8);
    }

    if (lum < lum_threshold_2){
        if (mod(gl_FragCoord.x - gl_FragCoord.y, 10.0) == 0.0)
            tc = vec3(0.6);
    }

    if (lum < lum_threshold_3){
        if (mod(gl_FragCoord.x + gl_FragCoord.y - hatch_y_offset, 10.0) == 0.0)
            tc = vec3(0.3);
    }

    if (lum < lum_threshold_4){
        if (mod(gl_FragCoord.x - gl_FragCoord.y - hatch_y_offset, 10.0) == 0.0)
            tc = vec3(0.0, 0.0, 0.0);
    }

    // simple sobel edge detection,
    // borrowed and tweaked from jmk's "edge glow" filter, here:
    // https://www.shadertoy.com/view/Mdf3zr
    vec2 p = qt_TexCoord0.xy;
    vec2 leftDown = vec2(-1.0 / imageWidth, -1.0 / imageHeight);
    vec2 left = vec2(-1.0 / imageWidth, 0);
    vec2 leftUp = vec2(-1.0 / imageWidth, 1.0 / imageHeight);
    vec2 rightDown = -1.0 * leftUp;
    vec2 right = -1.0 * left;
    vec2 rightUp = -1.0 * leftDown;
    vec2 up = vec2(0.0, 1.0);
    vec2 down = vec2(0.0, -1.0);

    float gx = 0.0;
    gx += -1.0 * lookup2(p, leftDown); //left down
    gx += -2.0 * lookup2(p, left); //left
    gx += -1.0 * lookup2(p, leftUp); //left up
    gx +=  1.0 * lookup2(p,  rightDown); //right down
    gx +=  2.0 * lookup2(p,  right); //right
    gx +=  1.0 * lookup2(p,  rightUp); //right up

    float gy = 0.0;
    gy += -1.0 * lookup2(p, leftDown); //left down
    gy += -2.0 * lookup2(p,  down); //down
    gy += -1.0 * lookup2(p,  rightDown); //right down
    gy +=  1.0 * lookup2(p, leftUp); //left up
    gy +=  2.0 * lookup2(p,  up); //up
    gy +=  1.0 * lookup2(p,  rightUp); //right up

    // hack: use g^2 to conceal noise in the video
    float g = gx * gx + gy * gy;
    tc *= (1.0 - g);

    gl_FragColor = vec4(tc, 1.0) * qt_Opacity;
}
