// Based on http://blog.qt.digia.com/blog/2011/03/22/the-convenient-power-of-qml-scene-graph/

uniform float amplitude;
uniform float frequency;
uniform float time;

uniform sampler2D source;
uniform lowp float qt_Opacity;
varying vec2 qt_TexCoord0;

void main()
{
    vec2 uv = qt_TexCoord0.xy;
    vec2 tc = qt_TexCoord0;

    vec2 p = sin(time + frequency * qt_TexCoord0);
    tc += amplitude * vec2(p.y, -p.x);

    gl_FragColor = qt_Opacity * texture2D(source, tc);
}
