// Based on http://www.geeks3d.com/20091027/shader-library-posterization-post-processing-effect-glsl/

uniform float gamma;
uniform float numColors;

uniform sampler2D source;
uniform lowp float qt_Opacity;
varying vec2 qt_TexCoord0;

void main()
{
    vec2 uv = qt_TexCoord0.xy;

    vec3 x = texture2D(source, uv).rgb;
    x = pow(x, vec3(gamma, gamma, gamma));
    x = x * numColors;
    x = floor(x);
    x = x / numColors;

    gl_FragColor = qt_Opacity * vec4(pow(x, vec3(1.0/gamma)), 1.0);
}
