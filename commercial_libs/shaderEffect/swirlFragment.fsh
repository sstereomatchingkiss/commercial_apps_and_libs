uniform lowp float qt_Opacity;
varying highp vec2 qt_TexCoord0;

uniform sampler2D source;

uniform highp float angle;
uniform highp float centerX;
uniform highp float centerY;
uniform highp float radius;
uniform float time;

void main()
{
    vec2 center = vec2(centerX, centerY);
    highp vec2 textureCoordinateToUse = qt_TexCoord0;
    highp float dist = distance(center, qt_TexCoord0);
    textureCoordinateToUse -= center;
    if (dist < radius)
    {
        highp float percent = (radius - dist) / radius;
        highp float theta = time * percent * percent * angle * 8.0;
        highp float s = sin(theta);
        highp float c = cos(theta);
        textureCoordinateToUse = vec2(dot(textureCoordinateToUse, vec2(c, -s)), dot(textureCoordinateToUse, vec2(s, c)));
    }
    textureCoordinateToUse += center;

    gl_FragColor = texture2D(source, textureCoordinateToUse) * qt_Opacity;

}
