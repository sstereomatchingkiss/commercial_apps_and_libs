// Based on http://www.geeks3d.com/20101029/shader-library-pixelation-post-processing-effect-glsl/

uniform float granularity;
uniform float imageHeight;
uniform float imageWidth;

uniform sampler2D source;
uniform lowp float qt_Opacity;
varying vec2 qt_TexCoord0;

void main()
{
    //vec2 uv = qt_TexCoord0.xy;
    vec2 tc = qt_TexCoord0;
    if (granularity > 0.0){
        float dx = granularity / imageWidth;
        float dy = granularity / imageHeight;
        tc = vec2(dx * (floor(qt_TexCoord0.x / dx) + 0.5),
                  dy * (floor(qt_TexCoord0.y / dy) + 0.5));
    }
    gl_FragColor = qt_Opacity * texture2D(source, tc);
}
