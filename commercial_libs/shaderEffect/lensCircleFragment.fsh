uniform float lensRadiusX; // 0.45, 0.38
uniform float lensRadiusY; // 0.45, 0.38

uniform sampler2D source;
uniform lowp float qt_Opacity;
varying vec2 qt_TexCoord0;

void main()
{
    vec4 Color = texture2D(source, qt_TexCoord0.xy);
    float dist = distance(qt_TexCoord0.xy, vec2(0.5,0.5));
    Color.rgb *= smoothstep(lensRadiusX, lensRadiusY, dist);
    gl_FragColor = Color;
}
