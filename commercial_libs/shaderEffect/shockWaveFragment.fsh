// Based on http://www.geeks3d.com/20091116/shader-library-2d-shockwave-post-processing-filter-glsl/

uniform float centerX;
uniform float centerY;
uniform float time;
uniform float weight;

uniform sampler2D source;
uniform lowp float qt_Opacity;
varying vec2 qt_TexCoord0;

void main()
{    
    vec2 tc = qt_TexCoord0;
    vec2 center = vec2(centerX, centerY);
    const vec3 shock = vec3(9.0, 1.5, 0.1);

    float distance = distance(qt_TexCoord0, center);
    if ((distance <= (time + shock.z)) &&
            (distance >= (time - shock.z))) {
        float diff = (distance - time);
        float powDiff = 1.0 - pow(abs(diff*shock.x), shock.y*weight);
        float diffTime = diff  * powDiff;
        vec2 diffUV = normalize(qt_TexCoord0 - center);
        tc += (diffUV * diffTime);
    }

    gl_FragColor = qt_Opacity * texture2D(source, tc);
}
