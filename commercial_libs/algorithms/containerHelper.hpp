#ifndef CONTAINERHELPER_HPP
#define CONTAINERHELPER_HPP

/*
 * collect algorithms for manipulating containers like stl
 */

/*
 * release the container memory if the memory is bigger than maximum.
 *
 *@param :
 * data : input, must support typedef of "value_type", member function
 * "capacity()" and "swap", they have the same meaning as those stl
 * containers.
 *
 * maximum : if the memory of the container exceed maximum, it may release
 * the memory hold by the container
 *
 */
template<typename T>
void release_if_big_enought(T &data, size_t maximum)
{
    if(sizeof(typename T::value_type) * data.capacity() > maximum){
        T().swap(data);
    }
}

#endif // CONTAINERHELPER_HPP
