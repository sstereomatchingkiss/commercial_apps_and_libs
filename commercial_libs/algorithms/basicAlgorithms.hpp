#ifndef BASIC_ALGORITHMS_HPP
#define BASIC_ALGORITHMS_HPP

#include <algorithm>
#include <iterator>
#include <type_traits>

template<typename RandomItr>
inline void linear_insert(RandomItr first, RandomItr last);

template<typename T, typename RandomItr>
void unguarded_linear_insert(RandomItr last,
                             typename std::conditional
                             <
                               std::is_arithmetic<T>::value,
                               T,
                               T const&
                             >::type value);

/*
 * please refer to http://en.wikipedia.org/wiki/Insertion_sort
 */
template<typename RandomItr>
void insertion_sort(RandomItr first, RandomItr last)
{
    if(first == last) return;

    for(auto it = first + 1; it != last; ++it)
        linear_insert(first, it);
}

/*
 * "inner loop" of the insertion sort
 */
template<typename RandomItr>
inline void linear_insert(RandomItr first, RandomItr last)
{
    typedef typename std::iterator_traits<RandomItr>::value_type value_type;

    value_type const value = *last;
    //this could make sure the unguarded_linear_insert would not out of bound
    if(value < *first)
    {
        std::copy_backward(first, last, last + 1);
        *first = std::move(value);
    }
    else
        unguarded_linear_insert<value_type>(last, value);
}

/*
 * generally, we have to check two things,
 * first : it is an inversion pair or not
 * second : it is out of bound or not
 * but this function do not test the second case,
 * so we call it unguarded_linear_insert
 */
template<typename T, typename RandomItr>
void unguarded_linear_insert(RandomItr last,
                             typename std::conditional
                             <
                               std::is_arithmetic<T>::value,
                               T,
                               T const&
                             >::type value)
{
  RandomItr next = last;
  --next;

  while(value < *next)
  {
      *last = *next;
      last = next;
      --next;
  }
  *last = std::move(value);
}

#endif // BASIC_ALGORITHMS_HPP
