#ifndef METAPROGRAMMINGHELPER_HPP
#define METAPROGRAMMINGHELPER_HPP

template<int T>
struct int2Type
{
    enum{value = T};
};

template<typename T>
struct type2Type
{
    typedef T type;
};

#endif // METAPROGRAMMINGHELPER_HPP
