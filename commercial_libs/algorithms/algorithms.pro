TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG += warn_on
CONFIG += c++11

QT       += testlib

QT       -= gui

win32{
QMAKE_CXXFLAGS += -Wall
}
mac{
LIBS += -stdlib=libc++

}

SOURCES += main.cpp

HEADERS += \
    stlAlgoReimplement.hpp \
    typeTriatsHelp.hpp \
    metaProgrammingHelper.hpp \
    timeEstimate.hpp \
    extraAlgorithm.hpp \
    typeTraitsHelp.hpp \
    stlAlgoWrapper.hpp \
    stlAlgoReimplement.hpp \
    basicAlgorithms.hpp \
    variadicTemplateAlgo.hpp \
    timeElapsed.hpp

