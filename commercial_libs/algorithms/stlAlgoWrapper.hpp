#ifndef STL_ALGO_WRAPPER_HPP
#define STL_ALGO_WRAPPER_HPP

/*
 * wrapped the algorithms of stl, so we don't need to input so many iterator again and again
 */

#include <algorithm>
#include <iterator>

template<typename T, typename U>
inline void fill(T &data, U const &value)
{
    std::fill(std::begin(data), std::end(data), value);
}

/*
 * The data should compatible with std::max_element
 */
template<typename T>
inline auto max_element(T const &data)->decltype(std::begin(data))
{
    return std::max_element(std::begin(data), std::end(data));
}

/*
 * The data should compatible with std::max_element
 */
template<typename T, typename BiFunc>
inline auto max_element(T const &data, BiFunc func)->decltype(std::begin(data))
{
    return std::max_element(std::begin(data), std::end(data), func);
}

/*
 * The data should compatible with std::max_element
 */
template<typename T>
inline auto min_element(T const &data)->decltype(std::begin(data))
{
    return std::min_element(std::begin(data), std::end(data));
}

/*
 * The data should compatible with std::max_element
 */
template<typename T, typename BiFunc>
inline auto min_element(T const &data, BiFunc func)->decltype(std::begin(data))
{
    return std::min_element(std::begin(data), std::end(data), func);
}

/*
 * T and U must be able to cope up with std::set_difference, U need to support
 * member function push_back
 */
template<typename T, typename U>
inline auto set_difference(T const &first, T const &second, U &result)->decltype(std::end(result))
{
    return std::set_difference(std::begin(first), std::end(first),
                               std::begin(second), std::end(second),
                               std::back_inserter(result) );
}

/*
 * return the result directly and don't need to pass in output parameter
 *
 * T and U must be able to cope up with std::set_difference, U need to support
 * member function push_back.
 */
template<typename T, typename U>
inline U const set_difference(T const &first, U const &second)
{
    T result;

    std::set_difference(std::begin(first), std::end(first),
                        std::begin(second), std::end(second),
                        std::back_inserter(result) );

    return result;
}

/*
 * return the result directly
 * don't need to sort the parameters before pass in
 *
 * T and U must be able to cope up with std::sort, U need to support
 * member function push_back.
 */
template<typename T, typename U>
U set_difference_lazy(T &fir, U &sec)
{
    std::sort(std::begin(fir), std::end(fir));
    std::sort(std::begin(sec), std::end(sec));

    U results;
    std::set_difference(std::begin(fir), std::end(fir),
                        std::begin(sec), std::end(sec),
                        std::back_inserter(results));

    return results;
}

/*
 * the data should be compatible with std::sort
 */
template<typename T>
inline void sort(T &data)
{
    std::sort(std::begin(data), std::end(data));
}

/*
 * the data should be compatible with std::sort
 */
template<typename T, typename BiFunc>
inline void sort(T &data, BiFunc func)
{
    std::sort(std::begin(data), std::end(data), func);
}

/*
 * the data should be compatible with std::unique
 */
template<typename T>
inline auto unique(T &data)->decltype(std::end(data))
{
    return std::unique(std::begin(data), std::end(data));
}

/*
 * the data should be compatible with std::unique
 */
template<typename T, typename BiFunc>
inline auto unique(T &data, BiFunc func)->decltype(std::end(data))
{
    return std::unique(std::begin(data), std::end(data), func);
}

#endif // STL_ALGO_WRAPPER_HPP
