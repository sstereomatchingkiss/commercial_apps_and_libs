#ifndef STL_ALGORITHM_REIMPLEMENT_HPP
#define STL_ALGORITHM_REIMPLEMENT_HPP

#include <algorithm>
#include <type_traits>

#include "typeTraitsHelp.hpp"

/*
 * find out the median value and return it
 */
template<typename T>
inline typename std::conditional<std::is_scalar<T>::value, T, T const>::type
median(typename param_type<T>::type a,
       typename param_type<T>::type b,
       typename param_type<T>::type c)
{
    if(a < b)
    {
        if(b < c)
            return b;
        else if(a < c)
            return c;
        else
            return a;
    }
    else if(a < c)
        return a;
    else if(b < c)
        return c;
    else
        return b;
}

/*
 * partition the array, after partioning, every elements on the right sight of first point to
 * will >= pivot; every elements on the left sight of last point to will <= pivot.
 *
 * @return
 *  the position first point to
 */
template<typename T, typename RandomItr>
RandomItr unguarded_partition(RandomItr first, RandomItr last,
                              typename param_type<T>::type pivot)
{
    while(true)
    {
        while(*first < pivot) ++first;
        --last;
        while(*last > pivot) --last;

        if(first > last) return first;
        std::iter_swap(first, last);
        ++first;
    }
}

#endif // STL_ALGORITHM_REIMPLEMENT_HPP
