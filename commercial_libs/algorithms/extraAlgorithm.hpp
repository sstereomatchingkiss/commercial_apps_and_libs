#ifndef EXTRA_ALGORITHM_HPP
#define EXTRA_ALGORITHM_HPP

/*
 *collect some simple algorithms do not implement by STL
 */

#include <algorithm>
#include <cmath>
#include <iterator>
#include <tuple>
#include <type_traits>

#include "typeTraitsHelp.hpp"

namespace{

template<typename T, typename U>
inline constexpr typename std::common_type<T, U>::type max_is_arith(T a, U b)
{
    return a > b ? a : b;
}

}

template<typename T>
inline constexpr typename std::enable_if<std::is_floating_point<T>::value, bool>::type fuzzy_compare(T p1, T p2)
{
    return (std::abs(p1 - p2) <= 0.000000000001 * std::min(std::abs(p1), std::abs(p2)));
}

/*
 * find out the minimum value of the array which all of the value are
 * negative. This function will return non negative value immediately
 * if any.
 *
 *@param
 * first : Iterator pointing to the initial element.
 * last : Iterator pointing to the final element. This must be reachable from first.
 *
 *@return
 * smallest negative value or non-negative value
 */
template<typename ForwardItr>
ForwardItr minimum_negative(ForwardItr first, ForwardItr last)
{
    ForwardItr min_it = first;
    typename std::iterator_traits<ForwardItr>::value_type min_value = *first;
    for(auto it = first; it != last; ++it)
    {
      if(*it >= 0) return it;
      if(*it < min_value)
      {
        min_it = it;
        min_value = *it;
      }
    }

    return min_it;
}

/*
 * max function which support arbitraty number of arithmetic types
 */
template<typename T, typename U, typename ...Var>
constexpr typename std::common_type<T, U, Var...>::type max_is_arith(T a, U b, Var...args)
{
    static_assert(std::is_arithmetic<T>::value, "only support arithmetic types");
    static_assert(std::is_arithmetic<U>::value, "only support arithmetic types");

    return max_is_arith(a > b ? a : b, args...);
}

/*
 * find out the maximum sub array, the type of the threshold
 * should be same as the value_type of the iterator
 *
 *@param
 * first : Iterator pointing to the initial element.
 * last : Iterator pointing to the final element. This must be reachable from first.
 * threshold : the threshold to determine the sub array is valid or not
 *
 *@return
 * first position of the sub array, last position of the sub array,
 * the biggest summation of the sub array
 */
template<typename ForwardItr>
std::tuple<ForwardItr, ForwardItr, typename std::iterator_traits<ForwardItr>::value_type>
partial_maximum_subarray(ForwardItr first, ForwardItr last,
                         typename param_type<typename std::iterator_traits<ForwardItr>::value_type>::type threshold)
{
  typedef typename std::iterator_traits<ForwardItr>::value_type value_type;
  typedef typename param_type<typename std::iterator_traits<ForwardItr>::value_type>::type param_type;

  if(first == last) return std::make_tuple(first, last, threshold);

  auto const min_it = minimum_negative(first, last);

  if(*min_it < threshold) return std::make_tuple(min_it, min_it, *min_it);

  ForwardItr current_first = first; //begin position of the sub array
  ForwardItr max_first     = first;
  ForwardItr max_end       = first;
  value_type current_sum   = *first;
  value_type max_sum       = *first;
  while(first != last)
  {
      current_sum += *first;
      current_sum = std::max(threshold, current_sum);

      ++first;

      if(current_sum > max_sum)
      {
          max_first = current_first;
          max_end = first;
          max_sum = std::max(current_sum, max_sum);
      }

      if(current_sum == threshold)
      {
          current_first = first;
      }
  }

  return std::make_tuple(max_first, max_end, max_sum);
}


#endif // EXTRA_ALGORITHM_HPP
