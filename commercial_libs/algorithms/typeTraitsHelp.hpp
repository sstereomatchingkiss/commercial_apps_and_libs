#ifndef TYPE_TRIATS_HELP_HPP
#define TYPE_TRIATS_HELP_HPP

#include <type_traits>

template<typename T>
struct param_type
{
    typedef typename std::conditional<std::is_scalar<T>::value, T, T const&>::type type;
};

#endif // TYPE_TRIATS_HELP_HPP
