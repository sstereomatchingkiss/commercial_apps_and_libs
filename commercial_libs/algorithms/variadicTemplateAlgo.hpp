#ifndef VARIADICTEMPLATEALGO_HPP
#define VARIADICTEMPLATEALGO_HPP

//collect some algorithms to help variadic programming

template<typename ForwardIter>
inline void expand_transform_binary_func(ForwardIter, ForwardIter, ForwardIter)
{

}

template<typename ForwardIter, typename BinaryFunc, typename ...Arg>
void expand_transform_binary_func(ForwardIter begin1, ForwardIter begin2, ForwardIter dst, BinaryFunc func, Arg ...args)
{
    *dst = func(*begin1, *begin2);
    ++begin1; ++begin2; ++dst;
    expand_transform_binary_func(begin1, begin2, dst, args...);
}

template<typename ForwardIter>
inline void expand_transform_unary_func(ForwardIter)
{

}

template<typename ForwardIter, typename UnaryFunc, typename ...Arg>
void expand_transform_unary_func(ForwardIter begin, UnaryFunc func, Arg ...args)
{
    *begin = func(*begin); ++begin;
    expand_transform_unary_func(begin, args...);
}

template<typename ForwardIter>
inline void expand_unary_func(ForwardIter)
{

}

template<typename inputIter, typename UnaryFunc, typename ...Arg>
void expand_unary_func(inputIter begin, UnaryFunc func, Arg ...args)
{    
    func(*begin); ++begin;
    expand_unary_func(begin, args...);
}

#endif // VARIADICTEMPLATEALGO_HPP
