#include <QCoreApplication>
#include <QString>
#include <QStringList>
#include <QtTest>

#include "fileAuxiliary.hpp"
#include "qstringAlgo.hpp"

class QFileAndQStringUnitTestTest : public QObject
{
    Q_OBJECT
    
public:
    QFileAndQStringUnitTestTest();
    
private Q_SLOTS:
    void test_create_serial_name_single();
    void test_create_serial_name();    
    void test_generate_noduplicate_name();
    void test_generate_noduplicate_number_name();
    void test_get_positions();
    void test_number_mapper();
    void test_number_mapper_str_list();    
    void test_qstring_position_surround_with();
    void test_replace_file_suffix();
    void test_split_str_by_positions();
    void test_split_str_by_wrap_around();
    void test_words_to_numbers();

private:
    QStringList create_numbers(int size); //design for test_number_mapper and test_number_mapper_str_list
};

QFileAndQStringUnitTestTest::QFileAndQStringUnitTestTest()
{

}

void QFileAndQStringUnitTestTest::test_create_serial_name_single()
{
    QString const name = "melon";

    QString result = create_serial_name_single(name, ".png", 0, 3);
    QCOMPARE(result, QString("melon000.png"));

    result = create_serial_name_single(name, ".png", 0, 4);
    QCOMPARE(result, QString("melon0000.png"));

    result = create_serial_name_single(name, ".png", 12345, 4);
    QCOMPARE(result, QString("melon12345.png"));
}

void QFileAndQStringUnitTestTest::test_create_serial_name()
{
    QString const name = "melon";
    QStringList const result = create_serial_name(name, ".png", 0, 10);
    QStringList test_result = {"000", "001", "002", "003", "004", "005", "006", "007", "008", "009"};
    for(QString &data : test_result){
        data.prepend("melon");
        data.append(".png");
    }
    for(int i = 0; i != result.size(); ++i){
        QCOMPARE(result[i], test_result[i] );
        QCOMPARE(result[i], test_result[i]);
    }
}

void QFileAndQStringUnitTestTest::test_qstring_position_surround_with()
{
    QString const name = "iiiia href = lllll]]]]";
    auto const index = qstring_position_surround_with(name, "iiii", "]]]]");

    QCOMPARE(index, std::make_pair(4, 14) );
    QCOMPARE(name.mid(index.first, index.second), QString("a href = lllll") );

    QTextCodec *codec = QTextCodec::codecForName("UTF-8");
    QTextCodec::setCodecForLocale(codec);

    QString const utf8Name = "[<a href=\"index.php?res=553215\">返信</a>]";
    auto const index2 = qstring_position_surround_with(utf8Name, "\"", "\"");

    QCOMPARE(index2, std::make_pair(10, 20) );
    QCOMPARE(utf8Name.mid(index2.first, index2.second), QString("index.php?res=553215") );
}

void QFileAndQStringUnitTestTest::test_get_positions()
{
    QString const contents = "lll ### ## ####";

    typedef std::pair<int, int> DInt;
    typedef std::vector<DInt> RType;
    auto positions = get_positions<RType>(QRegExp("#+"), contents);
    RType const positions_result = { std::make_pair(4, 3), std::make_pair(8, 2), std::make_pair(11, 4) };
    QCOMPARE(positions, positions_result);

    auto const positions2 = get_positions<RType>(QRegExp("[^#]+"), contents);
    RType const positions_result2 = { std::make_pair(0, 4), std::make_pair(7, 1), std::make_pair(10, 1)};
    QCOMPARE(positions2, positions_result2);

    auto const positions3 = get_positions<RType>(QRegularExpression("(#+)"), contents);
    QCOMPARE(positions3, positions_result);
}

void QFileAndQStringUnitTestTest::test_number_mapper()
{
    int const size = 100;
    int const length = QString::number(size).length();
    QStringList results = create_numbers(size);
    for(int i = 0; i != size; ++i){
        QCOMPARE(number_mapper(i, length), results[i]);
    }
}

void QFileAndQStringUnitTestTest::test_number_mapper_str_list()
{
    int const maximum = 100;
    QStringList const data = number_mapper(0, maximum, 3);
    QStringList const results = create_numbers(maximum);

    for(int i = 0; i != maximum; ++i){
        QCOMPARE(data[i], results[i]);
    }
}

void QFileAndQStringUnitTestTest::test_generate_noduplicate_name()
{
    QStringList result;
    int hint = 0;
    result << generate_noduplicate_name(QDir::currentPath() + "/TestFiles", "HappyFish.jpg", hint);
    result << generate_noduplicate_name(QDir::currentPath() + "/TestFiles", "fish.jpg", hint);
    result << generate_noduplicate_name(QDir::currentPath() +  "/TestFiles", "2.1.01.tiff", hint);

    QCOMPARE(result[0], QDir::currentPath() + "/TestFiles/HappyFish_0.jpg");
    QCOMPARE(result[1], QDir::currentPath() + "/TestFiles/fish_1.jpg");
    QCOMPARE(result[2], QDir::currentPath() + "/TestFiles/2.1.01_0.tiff");
}

void QFileAndQStringUnitTestTest::test_generate_noduplicate_number_name()
{
    QStringList result;
    int hint = 0;
    result << generate_noduplicate_number_name(QDir::currentPath() + "/TestFiles", "HappyFish.jpg", 4, hint);
    result << generate_noduplicate_number_name(QDir::currentPath() + "/TestFiles", "fish.jpg", 4, hint);
    result << generate_noduplicate_number_name(QDir::currentPath() +  "/TestFiles", "2.1.01.tiff", 4, hint);

    QCOMPARE(result[0], QDir::currentPath() + "/TestFiles/HappyFish_0000.jpg");
    QCOMPARE(result[1], QDir::currentPath() + "/TestFiles/fish_0000.jpg");
    QCOMPARE(result[2], QDir::currentPath() + "/TestFiles/2.1.01_0000.tiff");
}

void QFileAndQStringUnitTestTest::test_replace_file_suffix()
{
    QStringList result;
    result << replace_file_suffix("abc.jpg", "bmp");
    result << replace_file_suffix("C:Qt/commercial/abcd.ggg.bmp", "jpg");

    QCOMPARE(result[0], QString("abc.bmp"));
    QCOMPARE(result[1], QString("C:Qt/commercial/abcd.ggg.jpg"));
}

void QFileAndQStringUnitTestTest::test_split_str_by_positions()
{
    QString const contents = "lll ### ## ####";

    typedef std::pair<int, int> DInt;
    typedef std::vector<DInt> RType;
    auto positions = get_positions<RType>(QRegExp("#+"), contents);
    auto positions2 = get_positions<RType>(QRegExp("[^#]+"), contents);

    positions.reserve(positions.size() + positions2.size());
    std::copy(std::begin(positions2), std::end(positions2), std::back_inserter(positions));
    //positions.merge(positions2, [](DInt &one, DInt &two){ return one.first < two.first; } );
    std::sort(std::begin(positions), std::end(positions), [](DInt const &one, DInt const &two){ return one.first < two.first; });

    QStringList results = split_str_by_positions(positions, contents);
    QStringList test_results = {"lll ", "###", " ", "##", " ", "####"};
    QCOMPARE(results, test_results);
}

void QFileAndQStringUnitTestTest::test_split_str_by_wrap_around()
{
    QString const text[] = {"cow_#_boy_##_girl_###_human####", "lll ### ### ###", "$&*###\\-+##"};
    QStringList split_results[] =
    {
        QStringList() <<"cow_"<<"#"<<"_boy_"<<"##"<<"_girl_"<<"###"<<"_human"<<"####",
        QStringList() <<"lll "<<"###"<<" "<<"###"<<" "<<"###",
        QStringList() <<"$&*"<<"###"<<"\\-+"<<"##"
    };

    for(int i = 0; i != sizeof(text) / sizeof(QString); ++i){
        QCOMPARE(split_str_by_wrap_around('#', text[i]), split_results[i]);
    }
}

void QFileAndQStringUnitTestTest::test_words_to_numbers()
{
    QString const text[]    = {"cow_#_boy_##_girl_###_human####", "lll ### ### ###", "$&*###\\-+##"};
    QString const results[] = {"cow_0_boy_00_girl_000_human0000", "lll 000 000 000", "$&*000\\-+00"};
    for(size_t i = 0; i != sizeof(text)/ sizeof(QString); ++i){
        auto const output = words_to_number(QRegExp("#+"), split_str_by_wrap_around('#', text[i]), 0);
        auto const output2 = words_to_number(QRegularExpression("(#+)"), split_str_by_wrap_around('#', text[i]), 0);
        QCOMPARE(results[i], output);
        QCOMPARE(results[i], output2);
    }
}

/**********************************************************
 ****************** implementation ************************
 **********************************************************/

QStringList QFileAndQStringUnitTestTest::create_numbers(int size)
{
    QStringList results;
    for(int i = 0; i != size; ++i){
        results << QString::number(i);
    }

    int const length = QString::number(size).length();
    for(int i = 0; i != size; ++i){
        for(int j = results[i].length(); j < length; ++j)
          results[i].prepend("0");
    }

    return results;
}

QTEST_APPLESS_MAIN(QFileAndQStringUnitTestTest)

#include "tst_qfileandqstringunittesttest.moc"
