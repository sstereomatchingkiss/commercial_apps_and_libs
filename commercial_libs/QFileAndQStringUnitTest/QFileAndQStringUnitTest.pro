#-------------------------------------------------
#
# Project created by QtCreator 2013-02-10T09:45:25
#
#-------------------------------------------------

QT       += testlib

QT       -= gui

TARGET = tst_qfileandqstringunittesttest
CONFIG   += console
CONFIG   += c++11
CONFIG   -= app_bundle

TEMPLATE = app

QMAKE_CXXFLAGS += -Woverloaded-virtual

INCLUDEPATH += ../QFileAndQString

SOURCES += tst_qfileandqstringunittesttest.cpp \
    ../QFileAndQString/qstringAlgo.cpp \
    ../QFileAndQString/fileAuxiliary.cpp
DEFINES += SRCDIR=\\\"$$PWD/\\\"

HEADERS += \
    ../QFileAndQString/qstringAlgo.hpp \
    ../QFileAndQString/fileAuxiliary.hpp

RESOURCES +=
