#include "widgetGlobalHelper.hpp"

void populate_menu_and_toolbar(QMenu *menu, QToolBar *toolbar, std::initializer_list<QAction*> actions)
{
    for(auto *action : actions) {
        if (!action) {
            menu->addSeparator();
            toolbar->addSeparator();
        }
        else {
            menu->addAction(action);
            toolbar->addAction(action);
        }
    }
}
