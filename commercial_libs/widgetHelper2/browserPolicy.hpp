#ifndef BROWSERPOLICY_HPP
#define BROWSERPOLICY_HPP

class imageViewer;
class QString;
class QTextBrowser;

class initializeTextBrowser
{
public:
    initializeTextBrowser(QString const &path, QString const &page, QString const &postfix);

    void initialize_browser(QTextBrowser *browser) const;

private:
    QString const &path_;
    QString const &page_;
    QString const &postfix_;
};

#endif // BROWSERPOLICY_HPP
