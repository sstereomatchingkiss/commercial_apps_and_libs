#include <QDir>
#include <QString>
#include <QTextBrowser>

#include "browserPolicy.hpp"
#include "imageViewer.hpp"

initializeTextBrowser::initializeTextBrowser(QString const &path, QString const &page, QString const &postfix) :
    path_(path), page_(page), postfix_(postfix)
{}

void initializeTextBrowser::initialize_browser(QTextBrowser *browser) const
{
    QDir dir(path_);
    if(dir.exists()){
        browser->setSearchPaths(QStringList() << (QDir::currentPath() + postfix_));
    }else{
        browser->setSearchPaths(QStringList() << (path_ + postfix_));
    }

    browser->setSource(page_);
}
