#ifndef BROWSERCREATOR_HPP
#define BROWSERCREATOR_HPP

#include <memory>

#include <QDir>
#include <QHBoxLayout>
#include <QString>
#include <QStringList>
#include <QWidget>

#include "browserPolicy.hpp"

/**
 * @brief create a browser like widget
 * @param T : type of the widget want to show
 * @param size_t : let user able to create different browser with the same type
 */
template<typename T, typename Policy = initializeTextBrowser, size_t Series = 0>
class browserCreator : public QWidget
{
public:
    explicit browserCreator(Policy const &policy, Qt::WindowModality modal_choice = Qt::NonModal, QWidget *parent = 0);
    ~browserCreator();

    T* get_browser() { return browser_; }

    static T* show_browser(Policy const &policy);
    static void set_max_num(size_t max_num) { max_num_ = max_num; }

private:
    T *browser_;

    static size_t current_num_; //current number of the display browser(default value is zero)

    static size_t max_num_; //determine how many help browser could exist in the same time, default value is 1
};

/**
 * @param path : path pf the helps
 * @param page : path of the page
 * @param Qt::WindowModality : choice of the modality
 * @param parent : parent widget
 */
template<typename T, typename Policy, size_t Series>
browserCreator<T, Policy, Series>::browserCreator(Policy const &policy, Qt::WindowModality modal_choice, QWidget *parent) :
    QWidget(parent)
{
    setAttribute(Qt::WA_DeleteOnClose);
    setWindowModality(modal_choice);    

    browser_ = new T(this);
    /*QDir dir(path);
    if(dir.exists()){
        browser_->setSearchPaths(QStringList() << (QDir::currentPath() + "/helps"));
    }else{
        browser_->setSearchPaths(QStringList() << (path + "/helps"));
    }

    browser_->setSource(page);*/
    policy.initialize_browser(browser_);

    setWindowTitle(tr("Help"));

    std::unique_ptr<QHBoxLayout> layout(new QHBoxLayout);
    layout->addWidget(browser_);
    setLayout(layout.release());
}

template<typename T, typename Policy, size_t Series>
browserCreator<T, Policy, Series>::~browserCreator()
{
    --current_num_;
}

/**
 * @brief create and show the browser
 * @param path : path pf the helps
 * @param page : path of the page
 *
 * @return return the address of the browser if the browser is created; else return nullptr
 */
template<typename T, typename Policy, size_t Series>
T* browserCreator<T, Policy, Series>::show_browser(Policy const &policy)
{
    if(current_num_ != max_num_){
        auto browser = new browserCreator(policy);
        browser->resize(640, 480);
        browser->show();
        ++current_num_;

        return browser->get_browser();
    }

    return nullptr;
}

template<typename T, typename Policy, size_t Series>
size_t browserCreator<T, Policy, Series>::current_num_ = 0;
template<typename T, typename Policy, size_t Series>
size_t browserCreator<T, Policy, Series>::max_num_ = 1;

#endif // BROWSERCREATOR_HPP
