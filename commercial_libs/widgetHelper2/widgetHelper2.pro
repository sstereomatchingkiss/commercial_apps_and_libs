#-------------------------------------------------
#
# Project created by QtCreator 2012-12-27T00:33:58
#
#-------------------------------------------------

QT += core gui widgets

TARGET = widgetHelper2
TEMPLATE = lib

win32{
QMAKE_CXXFLAGS += -std=c++0x
}
mac{
LIBS += -stdlib=libc++

QMAKE_CXXFLAGS += -stdlib=libc++
QMAKE_CXXFLAGS += -std=c++11
QMAKE_CXXFLAGS += -mmacosx-version-min=10.7
QMAKE_LFLAGS += -mmacosx-version-min=10.7
}

QMAKE_CXXFLAGS += -Woverloaded-virtual

SOURCES += \
    widgetGlobalHelper.cpp

HEADERS += \
    widgetGlobalHelper.hpp \
    blockSignal.hpp \
    browserCreator.hpp

unix:!symbian {
    maemo5 {
        target.path = /opt/usr/lib
    } else {
        target.path = /usr/lib
    }
    INSTALLS += target
}
