#ifndef WIDGETGLOBARHELPER_HPP
#define WIDGETGLOBARHELPER_HPP

#include <initializer_list>

#include <QAction>
#include <QMenu>
#include <QToolBar>

namespace{

template<typename T>
void polulate_impl(T *menu_or_toolbar, std::initializer_list<QAction*> actions)
{
    for(auto *action : actions) {
        if (!action) {
            menu_or_toolbar->addSeparator();
        }
        else {
            menu_or_toolbar->addAction(action);
        }
    }
}

}

inline void populate_menu(QMenu *menu, std::initializer_list<QAction*> actions)
{
    polulate_impl(menu, actions);
}

void populate_menu_and_toolbar(QMenu *menu, QToolBar *toolbar, std::initializer_list<QAction*> actions);

inline void populate_toolbar(QToolBar *toolbar, std::initializer_list<QAction*> actions)
{
    polulate_impl(toolbar, actions);
}

/*
 * set checkable of gui which support member functions "setCheckable"
 */
template<typename T>
void set_qcheckable(bool checkable, std::initializer_list<T> inputs)
{
    for(auto *data : inputs){
        data->setCheckable(checkable);
    }
}

inline void set_qcheckable(bool){}

template<typename T, typename...U>
void set_qcheckable(bool checkable, T *param, U...params)
{    
    param->setCheckable(checkable);
    set_qcheckable(checkable, params...);
}

/*
 * set the range of gui which support member functions "setRange"
 */
template<typename T>
void set_qrange(int min, int max, std::initializer_list<T> inputs)
{
    for(auto *data : inputs){
        data->setRange(min, max);
    }
}

inline void set_qrange(int, int){}

template<typename T, typename...U>
void set_qrange(int min, int max, T *param, U...params)
{    
    param->setRange(min, max);
    set_qrange(min, max, params...);
}

inline void set_qvalue(int)
{
}

template<typename T, typename...Args>
void set_qvalue(int value, T *param, Args...params)
{
    param->setValue(value);
    set_qvalue(value, params...);
}

inline void set_qvisible(bool){}

template<typename T, typename...U>
void set_qvisible(bool visible, T *param, U...params)
{
    param->setVisible(visible);
    set_qvisible(visible, params...);
}


#endif // WIDGETGLOBARHELPER_HPP
