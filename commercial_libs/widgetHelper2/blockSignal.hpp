#ifndef BLOCKSIGNAL_HPP
#define BLOCKSIGNAL_HPP

#include <initializer_list>

#include <QObject>

class blockQSignal
{
public :
    blockQSignal(QObject *data) : data_(data), old_state_(data_->blockSignals(true)) {  }
    blockQSignal(blockQSignal const &data) = delete;
    blockQSignal& operator=(blockQSignal const&) = delete;
    ~blockQSignal() { data_->blockSignals(old_state_); }

private :
    QObject *data_;
    bool     old_state_;
};

template<size_t N>
class blockQSignals
{
public :    
    blockQSignals(std::initializer_list<QObject*> list);
    template<typename T>
    blockQSignals(T *list[]);
    blockQSignals(blockQSignals const&) = delete;
    blockQSignals& operator=(blockQSignals const&) = delete;
    ~blockQSignals();

private :
    QObject *data_[N];
    bool    old_states_[N];
};

template<size_t N>
blockQSignals<N>::blockQSignals(std::initializer_list<QObject*> list)
{
    auto begin = list.begin();
    for(size_t i = 0; i != N; ++i){
        data_[i] = *begin;
        ++begin;
        old_states_[i] = data_[i]->blockSignals(true);
    }
}

template<size_t N>
template<typename T>
blockQSignals<N>::blockQSignals(T *list[])
{
    for(size_t i = 0; i != N; ++i){
        data_[i]       = list[i];
        old_states_[i] = data_[i]->blockSignals(true);
    }
}

template<size_t N>
blockQSignals<N>::~blockQSignals()
{
    for(size_t i = 0; i != N; ++i){
        data_[i]->blockSignals(old_states_[i]);
    }
}

#endif // BLOCKSIGNAL_HPP
