#ifndef IMAGEALGORITHM_HPP
#define IMAGEALGORITHM_HPP

#include <initializer_list>
#include <utility>
#include <vector>

#ifndef DISABLED_QT
#include <QtCore/QSize>
#endif

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <colorCorrection/colorSpaceEnum.hpp>
#include <histogram/calcHistogram.hpp>
#include <histogram/histogramEqualize.hpp>
#include <sharpen/basicSharpenAlgo.hpp>

#include "colorFlag.hpp"

namespace OCV
{

enum class colorFlag : size_t{
    BGR2SBGR,
    BGR2SGM, //BGR 2 sigmoidal
    SBGR2BGR,
    SGM2BGR //sigmoidal to BGR
};

class clahe
{
public:
    clahe();

    void run(cv::Mat &src, cv::Mat &dst, double clip_limit = 40.0, cv::Size const &grid_size = cv::Size(8, 8), OCV::colorSpace cl_space = OCV::colorSpace::YCrCb);

private:
    void helper(cv::Mat &inout, double clip_limit, cv::Size const &grid_size, int channel);

private:
    std::vector<cv::Mat> buffer_; //alleviate the cost of allocated

    cv::Ptr<cv::CLAHE> clahe_;

    cv::Mat temp_src_; //save the
};

inline void clahe_function(cv::Mat &src, cv::Mat &dst, double clip_limit = 40.0, cv::Size const &grid_size = cv::Size(8, 8), OCV::colorSpace cl_space = OCV::colorSpace::YCrCb)
{
    clahe().run(src, dst, clip_limit, grid_size, cl_space);
}

void cvt_to_gray(cv::Mat &src, cv::Mat &dst, bool copy = true, int dstCn=0, int flag = CV_BGR2GRAY);

void cvt_color(cv::Mat &src, cv::Mat &dst, colorFlag flags);

void gray_bgra_2_bgr(cv::Mat &input);

cv::Mat gray_bgra_2_bgr(cv::Mat const &input);

void gamma_correction_lazy(cv::Mat &src, cv::Mat &dst, float pow, OCV::colorSpace cl_space = OCV::colorSpace::Lab);

inline void mix_channels(cv::Mat const &src, cv::Mat &dst, std::initializer_list<int> from_to)
{
    //const_cast is not an ideal solution, should find a better way to finish the task    
    cv::mixChannels(&src, 1, &dst, 1, std::begin(from_to), from_to.size() / 2);
}

void rotate_image_90n(cv::Mat &src, cv::Mat &dst, int angle);

void rotate_image(cv::Mat &src, cv::Mat &dst, double angle);

std::vector<int> staircase_percent(double original, double target, double percent);

std::vector<cv::Size> staircase_percent(cv::Size_<double> const &original, cv::Size_<double> const &target, double percent);

std::vector<int> staircase_steps(double original, double target, int step);

std::vector<cv::Size> staircase_steps(cv::Size_<double> const &original, cv::Size_<double> const &target, int step);

#ifndef DISABLED_QT
inline std::vector<cv::Size> staircase_percent(QSize const &original, QSize const &target, double percent)
{
    return staircase_percent(cv::Size_<double>(original.width(), original.height()), cv::Size_<double>(target.width(), target.height()), percent);
}

inline std::vector<cv::Size> staircase_steps(QSize const &original, QSize const &target, int step)
{
    return staircase_steps(cv::Size_<double>(original.width(), original.height()), cv::Size_<double>(target.width(), target.height()), step);
}
#endif

}


#endif // IMAGEALGORITHM_HPP
