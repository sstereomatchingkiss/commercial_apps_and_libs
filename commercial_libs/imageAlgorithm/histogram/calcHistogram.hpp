#ifndef CALCHISTOGRAM_HPP
#define CALCHISTOGRAM_HPP

#include <vector>

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>

namespace OCV
{

/**
 * @brief encapsulate the api of cv::calcHist, because it is too complicated.
 * The cols of the input Mat should be one dimension
 */
class calcHistogram
{
public:
    calcHistogram();

    std::vector<cv::Mat> calc_hist(cv::Mat const &input);
    /*cv::MatND calc_hist(cv::Mat const &input, std::initializer_list<int> channels,
                        std::initializer_list<int> hist_sizes,
                        std::initializer_list<std::initializer_list<float>> hist_ranges);*/

    void calc_hist(cv::Mat const &input, std::vector<cv::Mat> &results);
    /*void calc_hist(cv::Mat const &input, cv::MatND &results, std::initializer_list<int> channels,
                   std::initializer_list<int> hist_sizes,
                   std::initializer_list<std::initializer_list<float>> hist_ranges);*/

    cv::Mat draw_hist(cv::Mat const &input, cv::Scalar const &scalar = cv::Scalar(255, 255, 255), int rows = 480, int cols = 640);
    cv::Mat draw_hist(std::vector<cv::Mat> const &input, std::vector<cv::Scalar> const &colors =
    {cv::Scalar(255, 0, 0), cv::Scalar(0, 255, 0), cv::Scalar(0, 0, 255)}, int rows = 480, int cols = 640);

    void set_accumulate(bool accumulate) { accumulate_ = accumulate; }
    void set_channel(int *channel) { channels_ = channel; }
    void set_dims(int dim) { dims_ = dim; }
    void set_hist_range(float hist_range_begin, float hist_range_end)
    {
        hist_range_begin_ = hist_range_begin;
        hist_range_end_ = hist_range_end;
    }
    void set_hist_size(int hist_size) { hist_size_ = hist_size; }
    void set_mask(cv::Mat &mask) { mask_ = mask;}
    void set_nimages(int nimages) { nimages_ = nimages; }
    void set_uniform(bool uniform) { uniform_ = uniform; }

private:
    cv::Mat draw_hist_impl(cv::Mat const &input, cv::Scalar const &scalar);

private:
    bool accumulate_;

    int *channels_;

    int dims_;

    cv::Mat hist_image_; //the histogram image will draw on this buffer
    cv::Mat hist_normalize_; //save the histogram affter normalize, needed for drawing histogram
    float hist_range_begin_;
    float hist_range_end_;
    int hist_size_;

    cv::Mat mask_;

    int nimages_;

    std::vector<cv::Mat> planes_;

    bool uniform_;
};

inline std::vector<cv::Mat> calc_histogram(cv::Mat const &input)
{
    return calcHistogram().calc_hist(input);
}

}

#endif // CALCHISTOGRAM_HPP
