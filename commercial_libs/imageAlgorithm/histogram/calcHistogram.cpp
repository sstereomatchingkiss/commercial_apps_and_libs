#include <bugLocation.hpp>

#include "calcHistogram.hpp"

namespace OCV
{

calcHistogram::calcHistogram() :
    accumulate_(false), channels_(nullptr), dims_(1),
    hist_range_begin_(0), hist_range_end_(256), hist_size_(256), nimages_(1), uniform_(true)
{}

/**
 * @brief calculate the histogram, default range of the histogram is within 0~255(all of the channels share the same range)
 * @param input :input image
 * @return the histograms of input
 */
std::vector<cv::Mat> calcHistogram::calc_hist(cv::Mat const &input)
{
    std::vector<cv::Mat> results;
    calc_hist(input, results);

    return results;
}

/**
 * @brief overload function, safe the histogram in result, could reduce memory reallocation
 * @param input  : input image
 * @param results : histogram after calculate
 */
void calcHistogram::calc_hist(cv::Mat const &input, std::vector<cv::Mat> &results)
{
    cv::split(input, planes_);
    /// Set the ranges ( for B,G,R) )
    float range[] = {hist_range_begin_, hist_range_end_};
    float const *hist_range = {range};

    size_t const size = planes_.size();
    if(results.size() < size){
        results.resize(size);
    }

    //Compute the histograms
    for(size_t i = 0; i != size; ++i){
        cv::calcHist( &planes_[i], nimages_, channels_, mask_, results[i], dims_, &hist_size_, &hist_range, uniform_, accumulate_);
    }
}

/**
 * @brief overload of draw_hist, this one only draw a single histogram
 */
cv::Mat calcHistogram::draw_hist(cv::Mat const &input, cv::Scalar const &scalar, int rows, int cols)
{
    hist_image_.create(rows, cols, CV_8UC3);
    hist_image_ = cv::Scalar( 0,0,0);

    return draw_hist_impl(input, scalar);
}

/**
 * @brief draw the input histogram on graph, default range is 0~255
 * @param input : input histogram
 * @param scalars : color of each histogram
 * @param rows: rows of the output
 * @param cols: cols of the output
 * @return the histogram image
 */
cv::Mat calcHistogram::draw_hist(std::vector<cv::Mat> const &input, std::vector<cv::Scalar> const &colors, int rows, int cols)
{
    if(input.size() != colors.size()){
        throw std::runtime_error(COMMON_DEBUG_MESSAGE + "input.size() should equal to scalars.size()");
    }

    hist_image_.create(rows, cols, CV_8UC3);
    hist_image_ = cv::Scalar( 0,0,0);
    for(size_t i = 0; i != input.size(); ++i){
        draw_hist_impl(input[i], colors[i]);
    }

    return hist_image_;
}

cv::Mat calcHistogram::draw_hist_impl(cv::Mat const &input, cv::Scalar const &scalar)
{
    int const bin_w = cvRound( static_cast<double>(hist_image_.rows/input.rows) );
    /// Normalize the result to [0, hist_normalize_.rows]
    cv::normalize(input, hist_normalize_, 0, hist_image_.rows, cv::NORM_MINMAX);
    /// Draw for each channel
    for( int i = 1; i != input.rows; ++i){
        cv::line(hist_image_,
                 cv::Point( bin_w * (i - 1), hist_image_.rows - cvRound(hist_normalize_.at<float>(i - 1)) ),
                 cv::Point( bin_w * (i), hist_image_.rows - cvRound(hist_normalize_.at<float>(i)) ),
                 scalar, 2, 8, 0);
    }

    return hist_image_;
}

}
