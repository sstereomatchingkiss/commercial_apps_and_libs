#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "histogramEqualize.hpp"

namespace
{

void histogram_equalize_helper(cv::Mat &inout, int channel)
{
    std::vector<cv::Mat> target_color_space;
    cv::split(inout, target_color_space);
    cv::equalizeHist(target_color_space[channel], target_color_space[channel]);
    cv::merge(target_color_space, inout);
}

}

namespace OCV
{

/**
 *@brief apply histogram equalization on src, if it is color image of
 * channel 3 or 4, this algorithm will transform the src to
 * YCrCb and apply histogram equalize on the Y channel and then
 * transform it back to BGR channel.Support CV_8U, CV_8UC3, CV_8UC4
 *
 *@param src : input image, the color space should be BGR or gray; src will not
 *be altered or reallocate memory if src and dst do not share the same resource
 *@param dst : output
 *@param cl_space : color space of histogram equalization will apply on,
 * YCrCb is the default color space and will be choose if the cl_space
 * are not one of "BGR, YCrCb, HLS".
 *
 *@return : return true if equalize success, else return false
 */
void histogram_equalize_tag(cv::Mat &src, cv::Mat &dst, BGR_TAG)
{    
    OCV::copy_if_not_same(src, dst);
    std::vector<cv::Mat> target_color_space;
    cv::split(dst, target_color_space);
    for(size_t i = 0; i != 3; ++i){
        cv::equalizeHist(target_color_space[i], target_color_space[i]);
    }
    cv::merge(target_color_space, dst);
}

void histogram_equalize_tag(cv::Mat &src, cv::Mat &dst, HLS_TAG)
{
    OCV::copy_if_not_same(src, dst);
    cv::cvtColor(src, dst, CV_BGR2HLS);
    histogram_equalize_helper(dst, 1);
    cv::cvtColor(dst, dst, CV_HLS2BGR);
}

void histogram_equalize_tag(cv::Mat &src, cv::Mat &dst, YCRCB_TAG)
{
    OCV::copy_if_not_same(src, dst);
    cv::cvtColor(src, dst, CV_BGR2YCrCb);
    histogram_equalize_helper(dst, 0);
    cv::cvtColor(dst, dst, CV_YCrCb2BGR);
}

/**
 *@brief apply histogram equalization of src, if it is color image of
 * channel 3 or 4, this algorithm will transform the src to
 * YCrCb and apply histogram equalize on the Y channel and then
 * transform it back to BGR channel.Support CV_8U, CV_8UC3, CV_8UC4
 *
 *@param src : input image, the color space should be BGR or gray; src will not
 *be altered or reallocate memory if src and dst do not share the same resource
 *@param dst : output
 *@param cl_space : color space of histogram equalization will apply on,
 * YCrCb is the default color space and will be choose if the cl_space
 * are not one of "BGR, YCrCb, HLS".
 *
 *@return : return true if equalize success, else return false
 */
void histogram_equalize(cv::Mat &src, cv::Mat &dst, OCV::colorSpace cl_space)
{
    switch(cl_space){
    case OCV::colorSpace::HLS :{
        histogram_equalize_tag(src, dst, HLS_T);
        break;
    }
    case OCV::colorSpace::YCrCb :{
        histogram_equalize_tag(src, dst, YCRCB_T);
        break;
    }
    default :{
        histogram_equalize_tag(src, dst, BGR_T);
        break;
    }
    }

    /*int const type = src.type();
    bool result = false;
    if(type == CV_8U || type == CV_8UC3 || type == CV_8UC4){
        cv::Mat temp_src = src;
        switch(src.channels()){
        case 4 :{
            temp_src = src.clone();
            cv::cvtColor(temp_src, dst, CV_BGRA2BGR);
        }
        case 3 :
            switch(cl_space){

            case OCV::colorSpace::HLS :{
                OCV::copy_if_not_same(temp_src, dst);
                cv::cvtColor(temp_src, dst, CV_BGR2HLS);
                histogram_equalize_helper(dst, 1);
                cv::cvtColor(dst, dst, CV_HLS2BGR);
                result = true;
                break;
            }
            case OCV::colorSpace::BGR :{
                OCV::copy_if_not_same(temp_src, dst);
                std::vector<cv::Mat> target_color_space;
                cv::split(dst, target_color_space);
                for(size_t i = 0; i != 3; ++i){
                    cv::equalizeHist(target_color_space[i], target_color_space[i]);
                }
                cv::merge(target_color_space, dst);
                result = true;
                break;
            }
            case OCV::colorSpace::YCrCb :
            default:{
                OCV::copy_if_not_same(temp_src, dst);
                cv::cvtColor(temp_src, dst, CV_BGR2YCrCb);
                histogram_equalize_helper(dst, 0);
                cv::cvtColor(dst, dst, CV_YCrCb2BGR);
                result = true;
                break;
            }
            }

            break;
        case 1 :{
            OCV::copy_if_not_same(src, dst);
            cv::equalizeHist(src, dst);
            result = true;
            break;
        }
        }
    }

    return result;*/
}

}
