#ifndef HISTOGRAMEQUALIZE_HPP
#define HISTOGRAMEQUALIZE_HPP

#include <stdexcept>

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <bugLocation.hpp>

#include "../colorFlag.hpp"
#include "../basicImageAlgo.hpp"
#include "../colorCorrection/colorSpaceEnum.hpp"

namespace OCV
{

void histogram_equalize_tag(cv::Mat &src, cv::Mat &dst, BGR_TAG tag);

void histogram_equalize_tag(cv::Mat &src, cv::Mat &dst, HLS_TAG tag);

void histogram_equalize_tag(cv::Mat &src, cv::Mat &dst, YCRCB_TAG tag);

template<typename Tag>
void histogram_equalize_tag(cv::Mat &src, cv::Mat &dst, Tag tag)
{
    if(src.type() != CV_8U || src.type() != CV_8UC3 || src.type() != CV_8UC4){
        throw std::runtime_error(COMMON_DEBUG_MESSAGE + "src.type() != CV_8U || src.type() != CV_8UC3 || src.type() != CV_8UC4");
    }

    cv::Mat temp_src = src;
    if(src.type() == CV_8UC4){
        temp_src = src.clone();
        cv::cvtColor(temp_src, temp_src, CV_BGRA2BGR);
    }

    if(src.channels() == 3){
        histogram_equalize_tag(temp_src, dst, tag);
    }else{
        OCV::copy_if_not_same(temp_src, dst);
        cv::equalizeHist(temp_src, dst);
    }
}

void histogram_equalize(cv::Mat &src, cv::Mat &dst, OCV::colorSpace cl_space);

}

#endif // HISTOGRAMEQUALIZE_HPP
