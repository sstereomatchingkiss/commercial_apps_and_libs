#ifndef BASICIMAGEALGOIMPL_HPP
#define BASICIMAGEALGOIMPL_HPP

//collect the overload functions of cv::Mat_<T>, don't use this file directly

template<typename T, typename UnaryFunctor>
void transform_channel(cv::Mat_<T> &src, int channel, UnaryFunctor functor);

template<typename T, typename UnaryFunctor>
void transform_channels(cv::Mat_<T> &src, UnaryFunctor functor);

template<typename T, typename UnaryFunctor>
void transform_continuous_channel(cv::Mat_<T> &src, UnaryFunctor functor);

template<typename T, typename BiFunc>
inline bool compare_channels(cv::Mat_<T> const &src, BiFunc func)
{
    cv::Mat temp_src = src;
    return compare_channels<typename cv::DataType<T>::channel_type>(temp_src, func);
}

/*
 *@brief comparison between two cv::Mat, if they have different rows and cols,
 * the result will be false.
 *
 *@param src1 : first input array
 *@param src2 : second input array
 *@param func : binary functor determine how should the function compare between src1 and src2
 *
 *@return : false if two cv::Mat do not satisfy the qualify of func; else true
 */
template<typename T, typename BiFunc>
inline bool compare_channels(cv::Mat_<T> const &src1, cv::Mat_<T> const &src2, BiFunc func)
{
    cv::Mat temp_src1 = src1;
    cv::Mat temp_src2 = src2;
    return compare_channels<typename cv::DataType<T>::channel_type>(temp_src1, temp_src2, func);
}

/*
 *@brief copy the data of src to the one dimension array
 *
 *@param src : input data, should be a one channel Mat(src.channels() == 1)
 *@param begin : input iterator point to the one dimensional array, the contents of
 *@param src will copy into this array, users should make sure begin has enough of spaces
 */
template<typename T, typename InputIter>
inline void copy_to_one_dim_array(cv::Mat_<T> const &src, InputIter begin)
{
    typedef typename cv::DataType<T>::channel_type CType;
    cv::Mat temp_src = src;
    copy_to_one_dim_array<CType>(temp_src, begin);
}

template<typename T>
std::vector<typename cv::DataType<T>::channel_type> const copy_to_one_dim_array(cv::Mat_<T> const &src)
{
    typedef typename cv::DataType<T>::channel_type CType;

    std::vector<CType> result(src.total() * src.channels());
    cv::Mat temp_src = src;
    copy_to_one_dim_array<CType>(temp_src, std::begin(result));

    return result;
}

/*
 * copy the data of src to the one dimension array.
 *
 *@param src : input
 *@param begin : input iterator point to the one dimensional array, the contents of
 *@param src will copy into this array, users should make sure begin has enough of spaces
 *@param channel : The channel want to copy
 *
 *@exception
 * Will throw std::out_of_range if the channel is negative or bigger than src.channels()
 */
template<typename T, typename InputIter>
inline void copy_to_one_dim_array(cv::Mat_<T> const &src, InputIter begin, int channel)
{
    typedef typename cv::DataType<T>::channel_type CType;
    cv::Mat temp_src = src;
    copy_to_one_dim_array_ch<CType>(temp_src, begin, channel);
}

template<typename T>
std::vector<typename cv::DataType<T>::channel_type> const copy_to_one_dim_array_ch(cv::Mat_<T> const &src, int channel)
{
    typedef typename cv::DataType<T>::channel_type CType;

    std::vector<CType> result(src.total());
    cv::Mat temp_src = src;
    copy_to_one_dim_array_ch<CType>(temp_src, std::begin(result), channel);

    return result;
}

template<typename T, typename UnaryFunc>
inline UnaryFunc for_each_channel(cv::Mat_<T> &input, int channel, UnaryFunc func)
{
    typedef typename cv::DataType<T>::channel_type channel_type;
    cv::Mat temp_input = input;
    return for_each_channel<channel_type>(temp_input, channel, func);
}

template<typename T, typename UnaryFunc>
inline UnaryFunc for_each_channels(cv::Mat_<T> &input, UnaryFunc func)
{
    typedef typename cv::DataType<T>::channel_type channel_type;
    if(input.isContinuous()){
        cv::Mat temp_input = input;
        return for_each_continuous_channels<channel_type>(temp_input, func);
    }else{
        cv::Mat temp_input = input;
        return for_each_channels_impl<channel_type>(temp_input, func);
    }
}

template<typename T, typename UnaryFunc>
inline UnaryFunc for_each_continuous_channels(cv::Mat_<T> &input, UnaryFunc func)
{
    typedef typename cv::DataType<T>::channel_type channel_type;
    cv::Mat temp_input = input;
    return for_each_continuous_channels<channel_type>(temp_input, func);
}

template<typename T>
inline void initialize_mat(cv::Mat_<T> &src)
{
    typedef typename cv::DataType<T>::channel_type Type;
    cv::Mat temp_src = src;
    initialize_mat<Type>(temp_src);
}

/**
 *@brief overload of process_three_channels
 */
template<typename T, typename TriFunc>
inline TriFunc process_three_channels(cv::Mat_<T> &input, TriFunc func)
{
    typedef typename cv::DataType<T>::channel_type channel_type;
    cv::Mat temp_input = input;
    return process_three_channels<channel_type>(temp_input, func);
}

/**
 *@brief overload of process_four_channels
 */
template<typename T, typename TriFunc>
inline TriFunc process_four_channels(cv::Mat_<T> &input, TriFunc func)
{
    typedef typename cv::DataType<T>::channel_type channel_type;
    cv::Mat temp_input = input;
    return process_four_channels<channel_type>(temp_input, func);
}

/*
 *@brief apply transformation on a specific channel pixel
 *@param src : the input
 *@param channel : the channel you want to alter
 *@param functor : Take the specific channel pixels and manipulate it
 */
template<typename T, typename UnaryFunctor>
inline void transform_channel(cv::Mat_<T> &src, int channel, UnaryFunctor functor)
{
    typedef typename cv::DataType<T>::channel_type CType;
    cv::Mat temp_input = src;
    transform_channel<CType>(temp_input, channel, functor);
}

/*
 *@brief apply transformation on a specific channel pixel
 *
 *@param src : input
 *@param dst : output
 *@param channel : the channel you want to alter
 *@param functor : Take the specific channel pixels and manipulate it
 */
template<typename T, typename UnaryFunctor>
void transform_channel(cv::Mat_<T> &src, cv::Mat_<T> &dst, int channel, UnaryFunctor functor)
{
    typedef typename cv::DataType<T>::channel_type CType;
    cv::Mat temp_src = src;
    cv::Mat temp_dst = dst;
    transform_channel<CType>(temp_src, temp_dst, channel, functor);
}

/*
 *@brief Apply transformation on all channels
 *
 *@param src : input
 *@param functor : Take the specific channel pixels and manipulate it
 */
template<typename T, typename UnaryFunctor>
inline void transform_channels(cv::Mat_<T> &src, UnaryFunctor functor)
{
    typedef typename cv::DataType<T>::channel_type CType;
    cv::Mat temp_src = src;
    transform_channels<CType>(temp_src, functor);
}

/*
 *@brief Apply transformation on all channels
 *
 *@param src : input
 *@param dst : output
 *@param functor : Take the specific channel pixels and manipulate it
 */
template<typename T, typename UnaryFunctor>
void transform_channels(cv::Mat_<T> &src, cv::Mat_<T> &dst, UnaryFunctor functor)
{
    typedef typename cv::DataType<T>::channel_type CType;
    cv::Mat temp_src = src;
    cv::Mat temp_dst = dst;
    transform_channels<CType>(temp_src, temp_dst, functor);
}

/*
 *@brief Apply transformation on all channels, it is user responsible to make sure
 * src1 and src2 has the same size(rows, cols). Will return false if src1 and
 * src2 don't have the same size
 *
 *@param functor : Take the specific channel pixels and manipulate it
 */
template<typename T, typename BinaryFunctor>
void transform_channels(cv::Mat_<T> &src1, cv::Mat_<T> &src2, cv::Mat_<T> &dst, BinaryFunctor functor)
{
    typedef typename cv::DataType<T>::channel_type CType;
    cv::Mat temp_src1 = src1;
    cv::Mat temp_src2 = src2;
    cv::Mat temp_dst = dst;
    transform_channels<CType>(temp_src1, temp_src2, temp_dst, functor);
}

/*
 *@brief Make use the advantages of the image with continuous memory layout,
 * transform the whole image continuously. This could improve the speed
 *
 *@param src : input
 *@param functor : Take the pixels and manipulate it
 */
template<typename T, typename UnaryFunctor>
inline void transform_continuous_channel(cv::Mat_<T> &src, UnaryFunctor functor)
{
    typedef typename cv::DataType<T>::channel_type CType;

    cv::Mat temp_src = src;
    transform_continuous_channel<CType>(temp_src, functor);
}

/*
 *@brief Make use the advantages of the image with continuous memory layout,
 *transform the whole image continuously. This could improve the speed
 *
 *@param src : input
 *@param dst : output
 *@param functor : Take the pixels and manipulate it
 *
 *@exception : may throw std::bad_alloc
 */
template<typename T, typename UnaryFunctor>
void transform_continuous_channel(cv::Mat_<T> &src, cv::Mat_<T> &dst, UnaryFunctor functor)
{
    typedef typename cv::DataType<T>::channel_type CType;
    cv::Mat temp_src = src;
    cv::Mat temp_dst = dst;
    transform_continuous_channel<CType>(temp_src, temp_dst, functor);
}

#endif // BASICIMAGEALGOIMPL_HPP
