#-------------------------------------------------
#
# Project created by QtCreator 2012-11-19T22:45:01
#
#-------------------------------------------------

QT += gui core

TARGET = imageAlgorithm
TEMPLATE = app

CONFIG  += warn_on
CONFIG  += c++11

DEFINES += DEBUG_OK

win32{
INCLUDEPATH += ../../3rdLibs/openCV/OpenCV-2.4.5/build/include

LIBS += -L../../3rdLibs/openCV/OpenCV-2.4.5/builded/bin -lopencv_core245 -lopencv_highgui245 -lopencv_imgproc245 -lopencv_photo245
}
mac{
INCLUDEPATH += /usr/local/include

LIBS += -L/usr/local/lib/ -lopencv_core.2.4.5 -lopencv_highgui.2.4.5 -lopencv_imgproc.2.4.5 -lopencv_photo.2.4.5

#INCLUDEPATH += /opt/local/include

#LIBS += -L/opt/local/lib/ -lopencv_core.2.4.6  -lopencv_imgproc.2.4.6 -lopencv_highgui.2.4.6 -lopencv_photo.2.4.6
}

QMAKE_CXXFLAGS += -Woverloaded-virtual

#INCLUDEPATH += ../../boost/boost_1_53_0
INCLUDEPATH += ../../commercial_libs/algorithms
INCLUDEPATH += ../../commercial_libs/debugHelper
INCLUDEPATH += ../../commercial_libs/OpenCVAndQt

SOURCES += \
    imageAlgorithm.cpp \      
    main.cpp \    
    ../OpenCVAndQt/openCVToQt.cpp \    
    basicImageAlgo.cpp \
    imageResize/resizeAlgo.cpp \
    imageResize/geometryStencils/stencilSet.cpp \
    imageResize/geometryStencils/stencilInterpolation.cpp \
    imageResize/geometryStencils/gcsInterpolation.cpp \
    colorCorrection/simplestColorBalance/simplestColorBalanceHelper.cpp \
    colorCorrection/simplestColorBalance/simplestColorBalance.cpp \    
    colorCorrection/piecewiseAffHist/piecewiseAffHist.cpp \
    denoise/denoise.cpp \
    colorCorrection/whiteBalance/perfectReflectorAssumption.cpp \
    colorCorrection/whiteBalance/otsuWhiteBalance.cpp \
    colorCorrection/whiteBalance/monitorBGR.cpp \
    colorCorrection/whiteBalance/grayWorldAssumption.cpp \
    colorCorrection/whiteBalance/dynamicRangeWhiteBalance.cpp \
    colorCorrection/whiteBalance/chromaticyHistWB.cpp \
    waterMark/waterMark.cpp \
    blur/adaptiveMediumBlur.cpp \
    thinning/zhanSuen.cpp \
    thinning/morphologySkeleton.cpp \    
    histogram/histogram.cpp \
    backProjection/histBackProject.cpp \
    sharpen/basicSharpenAlgo.cpp \
    histogram/calcHistogram.cpp \
    histogram/histogramEqualize.cpp \
    genericAlgo/genericTransform.cpp \
    genericAlgo/genericRegion.cpp \
    colorCorrection/colorReduction/colorReduce.cpp

HEADERS += \
    imageAlgorithm.hpp \    
    ../algorithms/metaProgrammingHelper.hpp \
    colorConversion.hpp \    
    basicImageAlgo.hpp \
    imageResize/hqx/hqxInterpolationBase.hpp \
    imageResize/hqx/hqxAuxiliary.hpp \
    imageResize/hqx/hqxAlgo.hpp \
    imageResize/hqx/hqx4xInterpolate.hpp \
    imageResize/hqx/hqx3xInterpolate.hpp \
    imageResize/hqx/hqx2xInterpolate.hpp \
    imageResize/bicubic/bicubicInterpolation.hpp \
    imageResize/bicubic/bicubicFunctor.hpp \
    imageResize/resizeAlgo.hpp \
    imageResize/geometryStencils/stencilSet.hpp \
    imageResize/geometryStencils/stencilInterpolation.hpp \
    imageResize/geometryStencils/stencilConst.hpp \
    imageResize/geometryStencils/gcsInterpolation.hpp \
    colorCorrection/simplestColorBalance/simplestColorBalanceHelper.hpp \
    colorCorrection/simplestColorBalance/simplestColorBalance.hpp \
    ../debugHelper/debugHelper.hpp \    
    colorCorrection/piecewiseAffHist/piecewiseAffHist.hpp \
    denoise/denoise.hpp \
    colorCorrection/colorSpaceEnum.hpp \
    basicImageAlgoImpl.hpp \
    basicImageAlgoFunctor.hpp \
    colorCorrection/whiteBalance/perfectReflectorAssumption.hpp \
    colorCorrection/whiteBalance/otsuWhiteBalance.hpp \
    colorCorrection/whiteBalance/monitorBGR.hpp \
    colorCorrection/whiteBalance/grayWorldAssumption.hpp \
    colorCorrection/whiteBalance/dynamicRangeWhiteBalance.hpp \
    colorCorrection/whiteBalance/chromaticyHistWB.hpp \
    waterMark/waterMark.hpp \
    blur/adaptiveMediumBlurImpl.hpp \
    blur/adaptiveMediumBlur.hpp \
    utility/unitTransform.hpp \
    utility/keepAspectRatio.hpp \
    utility/dimensionMapper.hpp \
    utility/getDataHelper.hpp \
    thinning/zhanSuen.hpp \
    thinning/morphologySkeleton.hpp \    
    histogram/histogram.hpp \
    backProjection/histBackProject.hpp \
    sharpen/basicSharpenAlgo.hpp \
    histogram/calcHistogram.hpp \
    histogram/histogramEqualize.hpp \
    colorFlag.hpp \
    genericAlgo/genericForEach.hpp \
    genericAlgo/genericTransform.hpp \
    genericAlgo/genericProcess.hpp \
    genericAlgo/genericRegion.hpp \
    colorCorrection/colorReduction/colorReduce.hpp \
    warpImage/warpUtility.hpp
unix:!symbian {
    maemo5 {
        target.path = /opt/usr/lib
    } else {
        target.path = /usr/lib
    }
    INSTALLS += target
}
