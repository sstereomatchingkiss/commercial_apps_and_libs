#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "basicImageAlgo.hpp"
#include "debugHelper.hpp"
#include "grayWorldAssumption.hpp"

namespace
{

/**
 * less efficiency implementation but easier to maintain
 */
template<typename T>
void gray_world_assumption_impl(cv::Mat &input, cv::Mat &output)
{    
    int channel_num = input.channels();
    OCV::copy_if_not_same(input, output);
    if(channel_num == 1){        
        return;
    }
    cv::Scalar average_bgr = cv::mean(input);
    for(size_t i = 0; i != 3; ++i){        
        average_bgr[i] = average_bgr[i] != 0 ? average_bgr[i] : 1;
    }    
    channel_num = channel_num == 4 ? 3 : channel_num; //do not process the alpha channel
    double common_gray = 0;
    for(int i = 0; i != channel_num; ++i){
        common_gray += average_bgr[i];
    }
    common_gray /= channel_num;

    for(int i = 0; i != channel_num; ++i){
        double const scaled_value = common_gray / average_bgr[i];
        OCV::for_each_channel<T>(output, i, [=](T &data){ data = cv::saturate_cast<T>(data * scaled_value); });
    }
}

}

namespace OCV{

/**
 * @brief gray world assumption(http://scien.stanford.edu/pages/labsite/1999/psych221/projects/99/jchiang/intro2.html)
 * @param input  : 1 : input image, input will not be altered if it did not share the same resource;
 *                 2 : the alpha should not be the first channel since the implementation would take the first three channels,
 *                     remember to change your channel order if the alpha channel is not the fourth channel
 *                 3 : the memory of the input will not be reallocated
 * @param output : output image
 */
void gray_world_assumption(cv::Mat &input, cv::Mat &output)
{
    if(OCV::is_uchar_channel(input.type())){
        gray_world_assumption_impl<uchar>(input, output);
    }else if(OCV::is_float_channel(input.type())){
        gray_world_assumption_impl<float>(input, output);
    }else{
        throw std::runtime_error(COMMON_DEBUG_MESSAGE + "channels should be CV_8UC1, 3, 4 or CV_32FC1, 3, 4");
    }
}

}
