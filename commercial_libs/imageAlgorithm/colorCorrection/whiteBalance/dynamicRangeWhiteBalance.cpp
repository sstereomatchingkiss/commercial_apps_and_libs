#include <algorithm>
#include <numeric>
#include <stdexcept>

#include <opencv2/imgproc/imgproc.hpp>

#include <bugLocation.hpp>

#include "basicImageAlgo.hpp"
#include "basicImageAlgoFunctor.hpp"
#include "dynamicRangeWhiteBalance.hpp"

namespace OCV{

dynamicRangeWhiteBalance::dynamicRangeWhiteBalance()
{
}

/**
 * @brief generation gain, remember to call "generate_mean_and_deviation" before calling this function.
 * This function and "generate_mean_and_deviation" are designed for real time optimization, if you don't need
 * to apply this algorithm on the same image, call "run" function directly.
 * @param input : input image, the color space should be YCrCb
 */
void dynamicRangeWhiteBalance::generate_gain(cv::Mat &input)
{    
    white_points(input);
    cv::Scalar const average_bgr = cv::mean(top10_bgr_); //bgr are the top 10% white points
    auto const max_iter = std::max_element(std::begin(white_point_), std::end(white_point_), [](yCrCb const &a, yCrCb const &b)
    {
            return a.y < b.y;
});

    for(size_t i = 0; i != 3; ++i){
        gain_[i] = average_bgr[i] != 0 ? max_iter->y / average_bgr[i] : 1;
    }
}

/**
 * @brief generate mean and deviation. This function and "generate_gain" are
 * designed for real time optimization, if you don't need
 * to apply this algorithm on the same image, call "run" function directly.
 * @param input : input image, the color space should be YCrCb
 * @param block_rows : height of the block
 * @param block_cols : width of the block
 */
void dynamicRangeWhiteBalance::generate_mean_and_abs_avg_sum(cv::Mat &input, int block_rows, int block_cols)
{    
    block_size_ = region_size(input, block_rows, block_cols);
    mean_cr_ = region_mean(input, 1);
    mean_cb_ = region_mean(input, 2);
    deviation_cr_ = region_deviation(input, 1, mean_cr_);
    deviation_cb_ = region_deviation(input, 2, mean_cb_);
}

/**
 * @brief The last step of this algorithm, multiply the gain on every channels(B, G, R)
 * @param input : input image, the color space should be BGR
 * @param output : output image
 */
void dynamicRangeWhiteBalance::generate_result(cv::Mat &input, cv::Mat &output)
{
    copy_if_not_same(input, output);
    process_three_channels<uchar>(output, [=](uchar &b, uchar &g, uchar &r)
    {
        b = cv::saturate_cast<uchar>(b * gain_[0]);
        g = cv::saturate_cast<uchar>(g * gain_[1]);
        r = cv::saturate_cast<uchar>(r * gain_[2]);
    });
}

/**
 * @brief apply dynamic range white balance on the input, the input will not reallocated the data if input and output
 *  do not share the same resource.This function assume the color space of the input is BGR and will transform it to YCrCb for
 *  further processing. If you need better control, don't use this function.
 * @param input : input image
 * @param output : output image
 * @param block_rows : height of the block
 * @param block_cols : width of the block
 */
void dynamicRangeWhiteBalance::run(cv::Mat &input, cv::Mat &output, int block_rows, int block_cols)
{
    if(is_uchar_channel(input.type())){
        cv::Mat temp_input = input.clone();
        switch(input.channels()){
        case 4:{
            std::cout<<"drwb 4 channels"<<std::endl;
            //temp_input = input.clone();
            cv::cvtColor(temp_input, temp_input, CV_BGRA2BGR);
            //don't need to break, need to do further process
        }
        case 3:{
            std::cout<<"drwb 3 channels"<<std::endl;
            //cv::cvtColor(temp_input, temp_input, CV_BGR2YCrCb);
            temp_input.convertTo(temp_input, CV_16SC3);
            cv::cvtColor(temp_input, temp_input, CV_BGR2YCrCb);
            temp_input -= 128;
            /*typedef short TYPE;
            OCV::process_three_channels<TYPE>(input, [](TYPE &b, TYPE &g, TYPE &r)
            {
                TYPE y = r * 0.299 - g * 0.168935 + 0.499813 * b;
                TYPE cr = r * 0.587 - g * 0.331665 - 0.418531 * b;
                TYPE cb = r * 0.114 - g * 0.50059 - 0.081282 * b;
                b = y - 128;
                g = cr;
                r = cb;
            });*/
            detail(temp_input, output, block_rows, block_cols);
            output = temp_input;
            break;
        }
        case 1:{
            std::cout<<"drwb 1 channels"<<std::endl;
            copy_if_not_same(input, output);
            break;
        }
        default:{
            break;
        }
        }
    }else{
        throw std::runtime_error(COMMON_DEBUG_MESSAGE + "channels should be CV_8UC1, 3, 4 or CV_32FC1, 3, 4");
    }
}

/**********************************************************
 ****************** implementation ************************
 **********************************************************/

void dynamicRangeWhiteBalance::detail(cv::Mat &input, cv::Mat &output, int block_rows, int block_cols)
{
    generate_mean_and_abs_avg_sum(input, block_rows, block_cols);
    generate_gain(input);
    cv::cvtColor(input, input, CV_YCrCb2BGR);
    generate_result(input, output);
}

/**
 * @brief find out the deviation of blocks
 * @param input : input img
 * @param channel : the channel you want to find out the mean
 * @param region_mean : mean of each region
 * @return Deviation of each block
 */
std::vector<double> dynamicRangeWhiteBalance::region_deviation(cv::Mat &input, int channel, std::vector<double> const &region_mean)
{
    regionAvgAbsDiff<uchar, double> region_deviation(region_mean);
    region_algo_channel<uchar>(input, channel, block_size_, std::ref(region_deviation));

    return region_deviation.result_;
}

/**
 * @brief find out the mean of blocks
 * @param input : input img
 * @param channel : the channel you want to find out the mean
 * @return Mean of each block
 */
std::vector<double> dynamicRangeWhiteBalance::region_mean(cv::Mat &input, int channel)
{
    regionMean<uchar, double> meanFunc;
    region_algo_channel<uchar>(input, channel, block_size_, std::ref(meanFunc));

    return meanFunc.result_;
}

void dynamicRangeWhiteBalance::white_points(cv::Mat &input)
{           
    size_t const block_number = deviation_cb_.size();
    double const final_dev_cb = std::accumulate(std::begin(deviation_cb_), std::end(deviation_cb_), double(0)) / block_number;
    double const final_dev_cr = std::accumulate(std::begin(deviation_cr_), std::end(deviation_cr_), double(0)) / block_number;
    double const final_mean_cb = std::accumulate(std::begin(mean_cb_), std::end(mean_cb_), double(0)) / block_number;
    double const final_mean_cr = std::accumulate(std::begin(mean_cr_), std::end(mean_cr_), double(0)) / block_number;

    double const cr_left_criteria = 1.5 * final_mean_cr + final_dev_cr * final_mean_cr;
    double const cr_right_criteria = 1.5 * final_dev_cr;
    double const cb_left_criteria = final_mean_cb + final_dev_cb * final_mean_cb;
    double const cb_right_criteria = 1.5 * final_dev_cb;
    white_point_.clear();
    process_three_channels<uchar>(input, [&](uchar y, uchar cr, uchar cb)
    {
        if(std::abs(cr - cr_left_criteria) < cr_right_criteria &&
                std::abs(cb - cb_left_criteria) < cb_right_criteria){
            white_point_.emplace_back(y, cr, cb);
        }
    });

    //inefficient implementation, but easier to maintain by now
    std::sort(std::begin(white_point_), std::end(white_point_), [](yCrCb const &a, yCrCb const &b)
    {
        return a.y < b.y;
    });

    int const top_10_position = white_point_.size() * 0.1;
    top10_bgr_.create(1, top_10_position, CV_8UC3);
    auto ptr = top10_bgr_.ptr<uchar>(0);
    for(int i = 0; i != top_10_position; ++i){
        *ptr = white_point_[i].y; ++ptr;
        *ptr = white_point_[i].cr; ++ptr;
        *ptr = white_point_[i].cb; ++ptr;
    }
    cv::cvtColor(top10_bgr_, top10_bgr_, CV_YCrCb2BGR);
}

}
