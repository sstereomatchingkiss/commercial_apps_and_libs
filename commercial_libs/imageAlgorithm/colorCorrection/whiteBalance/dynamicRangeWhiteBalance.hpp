#ifndef DYNAMICRANGEWHITEBALANCE_HPP
#define DYNAMICRANGEWHITEBALANCE_HPP

#include <utility>
#include <vector>

#include <opencv2/core/core.hpp>

#include "basicImageAlgoFunctor.hpp"

namespace OCV{

/**
 * @brief implementation of the paper "A Novel Automatic White Balance Method For Digital Still Cameras"
 *        Only support uchar with 1, 3, 4 channels.If the image is 1 channel, this algorithm would assign the
 *        input to the output, will copy the result to the output if input and output do not share the same resource.
 *        For performance sake(for real time computation on a same image), some implementation details are exposed to
 *        the users, therefore they can reduce the need of recalculation of the deviation and mean of the same images.
 *
 * @caution The result of the thesis maybe a false one, the implementation of this algorithm is postpone until everything clear,
 *  it will remove from the library if I find out the thesis fabricate the results.
 */
class dynamicRangeWhiteBalance
{
public:
    dynamicRangeWhiteBalance();    

    void generate_mean_and_abs_avg_sum(cv::Mat &input, int block_rows, int block_cols);
    void generate_gain(cv::Mat &input);
    void generate_result(cv::Mat &input, cv::Mat &output);

    void run(cv::Mat &input, cv::Mat &output, int block_rows, int block_cols);
    
private:
    struct yCrCb
    {
        yCrCb(uchar oy, uchar ocb, uchar ocr) : y(oy), cb(ocb), cr(ocr) {}

        uchar y;
        uchar cb;
        uchar cr;
    };

    void detail(cv::Mat &input, cv::Mat &output, int block_rows, int block_cols);

    std::vector<double> region_deviation(cv::Mat &input, int channel, std::vector<double> const &region_mean);
    std::vector<double> region_mean(cv::Mat &input, int channel);

    void white_points(cv::Mat &input);

private:
    std::pair<std::vector<int>, std::vector<int> > block_size_;
    cv::Mat top10_bgr_;

    cv::Scalar gain_;

    std::vector<yCrCb> white_point_; //possible white point of component Cb

    std::vector<double> deviation_cb_;
    std::vector<double> deviation_cr_;
    std::vector<double> mean_cb_;
    std::vector<double> mean_cr_;

    std::vector<double> deviation_collection_;
};

}

#endif // DYNAMICRANGEWHITEBALANCE_HPP
