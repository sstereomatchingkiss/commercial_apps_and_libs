#include <type_traits>

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "basicImageAlgo.hpp"
#include "debugHelper.hpp"
#include "monitorBGR.hpp"

namespace
{

template<typename T>
struct multiply
{
    multiply(T new_white, T origin_white) :
        new_white_(new_white == 0 ?
                       std::is_floating_point<T>::value ? 1.0 : 255
                                                        : new_white),
        origin_white_(origin_white) {}

    T operator()(T data) { return cv::saturate_cast<T>((origin_white_ / new_white_) * data); }

    T new_white_;
    T origin_white_;
};

template<typename T, typename BiFunc>
void scale_elements(cv::Mat &input, cv::Mat &output, cv::Vec3f const &new_white, cv::Vec3f const &origin_white)
{
    if(new_white[0] == 0 && new_white[1] == 0 && new_white[2] == 0){
        return;
    }

    switch(input.channels()){
    case 1 :{
        OCV::copy_if_not_same(input, output);
        OCV::transform_channels<T>(output, BiFunc((new_white[0] + new_white[1] + new_white[2]) / 3,
                (origin_white[0] + origin_white[1] + origin_white[2]) / 3));
        break;
    }
    case 3 : {
        OCV::copy_if_not_same(input, output);
        OCV::transform_three_channels<T>(output, BiFunc(new_white[0], origin_white[0]), BiFunc(new_white[1], origin_white[1]),
                BiFunc(new_white[2], origin_white[2]));
        break;
    }
    case 4 :{
        OCV::copy_if_not_same(input, output);
        OCV::transform_four_channels<T>(output, BiFunc(new_white[0], origin_white[0]), BiFunc(new_white[1], origin_white[1]),
                BiFunc(new_white[2], origin_white[2]), [](T a){ return a; });
        break;
    }
    default :
        throw std::runtime_error(COMMON_DEBUG_MESSAGE + "input channels should be 1, 3, or 4");
    }
}

}


namespace OCV{

/**
 * @brief apply        pixels value transform on bgr
 * @param input        input data, input will not be altered if it did not share the same resource
 * with output; input will not reallocate memory no matter what
 * @param output       output data
 * @param new_white    new value of white
 * @param origin_white origin value of white
 *
 * @exception derived from std::exception
 *
 * If it is a single channel, the algorithm will average the value of new_white and origin_white then apply
 * them on the channel.
 */
void monitor_bgr(cv::Mat &input, cv::Mat &output, cv::Vec3f const &new_white, cv::Vec3f const &origin_white)
{
    if(OCV::is_uchar_channel(input.type())){
        scale_elements<uchar, multiply<uchar> >(input, output, new_white, origin_white);
    }else if(OCV::is_float_channel(input.type())){
        scale_elements<float, multiply<float> >(input, output, new_white, origin_white);
    }else{
        throw std::runtime_error(COMMON_DEBUG_MESSAGE + "channels should be CV_8UC1, 3, 4 or CV_32FC1, 3, 4");
    }
}

}
