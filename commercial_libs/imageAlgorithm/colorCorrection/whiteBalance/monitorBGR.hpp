#ifndef MONITORBGR_H
#define MONITORBGR_H

#include <opencv2/core/core.hpp>

namespace OCV{

void monitor_bgr(cv::Mat &input, cv::Mat &output, cv::Vec3f const &new_white, cv::Vec3f const &origin_white = cv::Vec3f(255, 255, 255));

}

#endif // MONITORBGR_H
