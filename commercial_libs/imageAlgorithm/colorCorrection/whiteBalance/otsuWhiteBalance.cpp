#include <opencv2/imgproc/imgproc.hpp>

#include "basicImageAlgo.hpp"
#include "basicImageAlgoFunctor.hpp"
#include "imageAlgorithm.hpp"

#include "otsuWhiteBalance.hpp"

namespace OCV{

/**
 * @brief easy function for users, if you need to process a lot of images, use the class version of the otsuWhiteBalance,
 * because it could resue some memory and may have better performance
 * @param input  : 1 : input image, input will not be altered if it did not share the same resource;
 *                 2 : the alpha should not be the first channel since the implementation would take the first three channels,
 *                     remember to change your channel order if the alpha channel is not the fourth channel
 *                 3 : the memory of the input will not be reallocated
 * @param output : output image
 */
void otsu_white_balance(cv::Mat &input, cv::Mat &output)
{
   otsuWhiteBalance otsu;
   otsu.run(input, output);
}

/**
 * @brief implement algorithm of the paper "Improved automatic white balance based on Otsu threshold", only support
 * CV_8UC1, CV_8UC3, CV_8UC4
 * @param input  : 1 : input image, input will not be altered if it did not share the same resource;
 *                 2 : the alpha should not be the first channel since the implementation would take the first three channels,
 *                     remember to change your channel order if the alpha channel is not the fourth channel
 *                 3 : the memory of the input will not be reallocated
 * @param output : output image
 */
void otsuWhiteBalance::run(cv::Mat &input, cv::Mat &output)
{
    if(!is_uchar_channel(input.type())){
        throw std::runtime_error(COMMON_DEBUG_MESSAGE + "cv::Mat type should be CV_8UC1, CV_8UC3 and CV_8UC4");
    }

    int const channels = input.channels();
    cv::Mat temp_input = input;
    switch(channels){
    case 4:{        
        temp_input = input.clone();
        cv::cvtColor(temp_input, temp_input, CV_BGRA2BGR);
        //don't need to break
    }
    case 3:{       
        cv::split(temp_input, mask_);
        for(auto &mask : mask_){
            cv::threshold(mask, mask, 0, 255, CV_THRESH_OTSU); //0 == foreground; 255 == background
        }

        get_mean(temp_input);
        get_variance(temp_input);
        standard_deviation();      

        auto const sdw = get_sdw();
        auto const gain = get_gain(sdw);       
        copy_if_not_same(temp_input, output);
        OCV::process_three_channels<uchar>(output, [&](uchar &b, uchar &g, uchar &r)
        {
            b = cv::saturate_cast<uchar>(b * gain[0]);
            g = cv::saturate_cast<uchar>(g * gain[1]);
            r = cv::saturate_cast<uchar>(r * gain[2]);
        });

        break;
    }
    case 1:{
        copy_if_not_same(temp_input, output);
        break;
    }
    }
}

/**********************************************************
 ****************** implementation ************************
 **********************************************************/

std::array<double, 3> otsuWhiteBalance::get_gain(std::array<double, 3> const &sdw)
{
    std::array<double, 3> gain;
    double const sum = std::accumulate(std::begin(sdw), std::end(sdw), double(0)) / 3;
    for(size_t i = 0; i != sdw.size(); ++i){
        gain[i] = sum / sdw[i];
    }

    return gain;
}

void otsuWhiteBalance::get_mean(cv::Mat const &input)
{
    to_zero(mean_bg_);
    to_zero(mean_fg_);    
    std::array<uchar*, 3> mask_ptr;
    for(size_t i = 0; i != mask_ptr.size(); ++i){
        mask_ptr[i] = mask_[i].ptr<uchar>(0);
    }
    OCV::process_three_channels<uchar>(input, [&](uchar b, uchar g, uchar r)
    {                        
        uchar const pixels[] = {b, g, r};
        for(size_t i = 0; i != mask_ptr.size(); ++i){
            if(*mask_ptr[i] == 255){
                mean_bg_[i] += pixels[i];
            }else{
                mean_fg_[i] += pixels[i];
            }
            ++mask_ptr[i];
        }
    });

    int const total = input.total();
    for(size_t i = 0; i != mean_bg_.size(); ++i){
        mean_bg_[i] /= total;
        mean_fg_[i] /= total;
    }
}

std::array<double, 3> otsuWhiteBalance::get_sdw()
{
    std::array<double, 3> sdw;
    for(size_t i = 0; i != mean_bg_.size(); ++i){
        double const denominator = std_dev_bg_[i] + std_dev_fg_[i];
        if(denominator != 0){
            sdw[i] = (mean_bg_[i] * std_dev_bg_[i] + mean_fg_[i] * std_dev_fg_[i]) / (denominator);
        }else{
            sdw[i] = 1;
        }
    }

    return sdw;
}

void otsuWhiteBalance::get_variance(cv::Mat const &input)
{
    to_zero(variance_bg_);
    to_zero(variance_fg_);    
    std::array<uchar*, 3> mask_ptr;
    for(size_t i = 0; i != mask_ptr.size(); ++i){
        mask_ptr[i] = mask_[i].ptr<uchar>(0);
    }
    OCV::process_three_channels<uchar>(input, [&](uchar b, uchar g, uchar r)
    {        
        uchar const pixels[] = {b, g, r};
        for(size_t i = 0; i != mask_ptr.size(); ++i){
            if(*mask_ptr[i] == 255){
                int const diff = pixels[i] - mean_bg_[i];
                variance_bg_[i] += diff * diff;
            }else{
                int const diff = pixels[i] - mean_fg_[i];
                variance_fg_[i] += diff * diff;
            }
            ++mask_ptr[i];
        }
    });

    int const total = input.total();
    for(size_t i = 0; i != variance_bg_.size(); ++i){
        variance_bg_[i] /= total;
        variance_fg_[i] /= total;
    }
}

void otsuWhiteBalance::standard_deviation()
{
    for(size_t i = 0; i != variance_bg_.size(); ++i){
        std_dev_bg_[i] = std::sqrt(variance_bg_[i]);
        std_dev_fg_[i] = std::sqrt(variance_fg_[i]);
    }
}

void otsuWhiteBalance::to_zero(std::array<double, 3> &input) const
{
    for(auto &data : input){
        data = 0;
    }
}

}
