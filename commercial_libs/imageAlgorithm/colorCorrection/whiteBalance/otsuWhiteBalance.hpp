#ifndef OSTUWHITEBALANCE_HPP
#define OSTUWHITEBALANCE_HPP

#include <array>
#include <numeric>
#include <vector>

#include <opencv2/core/core.hpp>

namespace cv
{

class Mat;

}

namespace OCV{

class otsuWhiteBalance
{
public:
    otsuWhiteBalance() {}

    void run(cv::Mat &input, cv::Mat &output);

private:
    std::array<double, 3> get_gain(std::array<double, 3> const &sdw);
    void get_mean(cv::Mat const &input);
    std::array<double, 3> get_sdw();
    void get_variance(cv::Mat const &input);

    void standard_deviation();

    void to_zero(std::array<double, 3> &input) const;

private:
    std::vector<cv::Mat> mask_;

    std::array<double, 3> mean_fg_;
    std::array<double, 3> mean_bg_;
    std::array<double, 3> variance_fg_;
    std::array<double, 3> variance_bg_;
    std::array<double, 3> std_dev_fg_;
    std::array<double, 3> std_dev_bg_;
};

void otsu_white_balance(cv::Mat &input, cv::Mat &output);

}

#endif // OSTUWHITEBALANCE_HPP
