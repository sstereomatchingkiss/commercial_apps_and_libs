#ifndef GRAYWORLDASSUMPTION_HPP
#define GRAYWORLDASSUMPTION_HPP

namespace cv{

class Mat;

}

namespace OCV{

void gray_world_assumption(cv::Mat &input, cv::Mat &output);

}

#endif // GRAYWORLDASSUMPTION_HPP
