#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "basicImageAlgo.hpp"
#include "perfectReflectorAssumption.hpp"
#include "grayWorldAssumption.hpp"
#include "debugHelper.hpp"

namespace OCV{

perfectReflectorAssumption::perfectReflectorAssumption(){}

/**
 * @brief implementation of the funtion run, this is aim for the 3, 4 uchar channels;if the number of the channel is 1,
 * it will call gray_world_assumption;use std::sort could make the codes simpler, but may cause more run time
 * @param inout  : input and output
 * @param ratio  : the threshold of the white balance point
 *
 * @exception derived from std::exception
 */
void perfectReflectorAssumption::detail(cv::Mat &inout, int ratio)
{
    hist_table_.clear();
    hist_table_.resize(255 * 3 + 1, 0);
    int const total_pixels = inout.rows * inout.cols;
    summation_buffer_.clear();
    summation_buffer_.resize(total_pixels, 0);
    auto *sum_buffer_ptr = &summation_buffer_[0];
    int max_pixel_value = 0; //max value of (r + g + b) / 3

    //find out every summation of rgb, save them into a buffer and figure out the max value
    OCV::process_three_channels<uchar>(inout, [&](int b, int g, int r) //explicit transfer the uchar to int, because we need a bigger dynamic range
    {
        *sum_buffer_ptr = b + g + r;
        max_pixel_value = std::max(max_pixel_value, *sum_buffer_ptr);
        ++hist_table_[*sum_buffer_ptr];
        ++sum_buffer_ptr;        
    });
    max_pixel_value /= 3;
    //max_pixel_value = *std::max_element(std::begin(summation_buffer_), std::end(summation_buffer_)) / 3;

    //find threshold according to the ratio and the histagram
    int threshold = 0;
    {
        int const percentile = total_pixels * ratio / 100;
        int sum = 0;
        for(int index = max_pixel_value - 1; index >= 0; --index){
            sum += hist_table_[index];
            if(sum > percentile){
                threshold = index;
                break;
            }
        }
    }

    int amount = 0;
    float avg_b = 0;
    float avg_g = 0;
    float avg_r = 0;
    sum_buffer_ptr = &summation_buffer_[0];
    OCV::process_three_channels<uchar>(inout, [&](uchar b, uchar g, uchar r)
    {
        if(*sum_buffer_ptr >= threshold){
            avg_b += b;
            avg_g += g;
            avg_r += r;
            ++amount;
        }
        ++sum_buffer_ptr;
    });
    avg_b /= amount;
    avg_g /= amount;
    avg_r /= amount;

    //normalize the value to [0, 255]
    OCV::process_three_channels<uchar>(inout, [=](uchar &b, uchar &g, uchar &r)
    {
        b = cv::saturate_cast<uchar>(b * max_pixel_value / avg_b);
        g = cv::saturate_cast<uchar>(g * max_pixel_value / avg_g);
        r = cv::saturate_cast<uchar>(r * max_pixel_value / avg_r);
    });
}

/**
 * @brief perfect reflector assumption(http://scien.stanford.edu/pages/labsite/2000/psych221/projects/00/trek/PRimages.html)
 * @param input  : input image
 * @param output : output image
 * @param ratio  : the threshold of the white balance point
 *
 * @exception derived from std::exception
 */
void perfectReflectorAssumption::run(cv::Mat &input, cv::Mat &output, int ratio)
{
    if(ratio == 0){
        OCV::copy_if_not_same(input, output);
        return;
    }
    if(OCV::is_uchar_channel(input.type())){       
        switch(input.channels()){
        case 4 :{
            output = input.clone();
            cv::cvtColor(output, output, CV_BGRA2BGR);
            detail(output, ratio);

            break;
        }
        case 3:{
            OCV::copy_if_not_same(input, output);
            detail(output, ratio);

            break;
        }
        case 1:{
            OCV::copy_if_not_same(input, output);
            break;
        }
        default:{
            break;
        }
        }

    }else{
        throw std::runtime_error(COMMON_DEBUG_MESSAGE + "channels should be CV_8UC1, 3, 4");
    }
}

}
