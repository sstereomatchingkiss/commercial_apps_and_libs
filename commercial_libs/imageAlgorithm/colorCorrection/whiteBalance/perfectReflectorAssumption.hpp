#ifndef BASICWHITEBALANCE_HPP
#define BASICWHITEBALANCE_HPP

#include <vector>

namespace cv {

class Mat;

}

namespace OCV{

class perfectReflectorAssumption
{
public:
    perfectReflectorAssumption();

    void run(cv::Mat &input, cv::Mat &output, int ratio);

private:
    void detail(cv::Mat &inout, int ratio);

private:
    std::vector<int> hist_table_;

    std::vector<int> summation_buffer_;
};

}

#endif // COLORBALANCEHELPER_HPP
