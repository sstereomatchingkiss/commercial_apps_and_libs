#ifndef CHROMATICYHISTWB_HPP
#define CHROMATICYHISTWB_HPP

#include <vector>

#include <opencv2/core/core.hpp>

#include "imageAlgorithm.hpp"

namespace OCV{

/**
 * @brief implement algorithm of the paper "Auto White Balance Based on the Similarity of Chromaticity Histograms"
 */
class chromaticyHistWB
{
    typedef std::pair<double, double> TYPE;
public:
    chromaticyHistWB();

    void run(cv::Mat &input, cv::Mat &output);

private:
    std::pair<double, double> compare_hist_overlap_lazy(cv::Mat const &input_hist, cv::Mat const &reference_hist);

    void shift_hist(cv::Mat const &input, cv::Mat &output, double factor);

private:

    std::vector<cv::Mat> histograms_;
    OCV::calcHistogram hist_manager_;

    std::vector<TYPE> similarity_;

    cv::Mat temp_hist_;
    cv::Mat temp_input_;
};

inline void chromaticity_hist_wb(cv::Mat &input, cv::Mat &output)
{
    return chromaticyHistWB().run(input, output);
}

}

#endif // CHROMATICYHISTWB_HPP
