#include <numeric>
#include <utility>

#include <opencv2/imgproc/imgproc.hpp>

#include "basicImageAlgo.hpp"
#include "basicImageAlgoFunctor.hpp"
#include "chromaticyHistWB.hpp"

namespace OCV{

static int const hist_range = 255;

chromaticyHistWB::chromaticyHistWB()
{
    hist_manager_.set_hist_range(0, hist_range);
}

/**
 * @brief implement algorithm of the paper "Auto White Balance Based on the Similarity of Chromaticity Histograms", only support
 * CV_8UC1, CV_8UC3, CV_8UC4
 * @param input  : input image
 * @param output : output image
 */
void chromaticyHistWB::run(cv::Mat &input, cv::Mat &output)
{
    if(!is_uchar_channel(input.type())){
        throw std::runtime_error(COMMON_DEBUG_MESSAGE + "cv::Mat type should be CV_8UC1, CV_8UC3 and CV_8UC4");
    }

    input.copyTo(temp_input_);
    int const channels = temp_input_.channels();

    switch(channels){
    case 4:{
        cv::cvtColor(temp_input_, temp_input_, CV_BGRA2BGR);
        //don't need to break
    }
    case 3:{        
        temp_input_.copyTo(output);
        process_three_channels<uchar>(temp_input_, bgr2Chromaticiy(hist_range));
        cv::GaussianBlur(temp_input_, temp_input_, cv::Size(5, 5), 0);
        hist_manager_.calc_hist(temp_input_, histograms_);
        auto const result_b = compare_hist_overlap_lazy(histograms_[0], histograms_[1]);
        auto const result_r = compare_hist_overlap_lazy(histograms_[2], histograms_[1]);

        OCV::process_three_channels<uchar>(output, [=](uchar &b, uchar, uchar &r)
        {
            b = cv::saturate_cast<uchar>(b * result_b.second);
            r = cv::saturate_cast<uchar>(r * result_r.second);
        });

        break;
    }
    case 1:{
        copy_if_not_same(input, output);
        break;
    }
    default:
        break;
    }
}

/**********************************************************
 ****************** implementation ************************
 **********************************************************/

/**
 * @brief compare the similarity between two histogram, usually the reference histogram should be the histogram of g channel
 * @param input_hist : input histogram
 * @param reference_hist : reference histogram
 * @return first : minimum value of the similarity; second : gain(the value needed by von kries model)
 */
std::pair<double, double> chromaticyHistWB::compare_hist_overlap_lazy(cv::Mat const &input_hist, cv::Mat const &reference_hist)
{
    size_t const size = 250;
    similarity_.resize(size);
    create_mat(input_hist, temp_hist_);
    temp_hist_.setTo(0);
    double const acc_step = 0.01;
    double gain = acc_step;
    for(size_t h = 0; h != size; ++h){
        shift_hist(input_hist, temp_hist_, gain);
        similarity_[h].first = cv::compareHist(temp_hist_, reference_hist, CV_COMP_BHATTACHARYYA);
        similarity_[h].second = gain;
        gain += acc_step;
        temp_hist_.setTo(0);
    }

    return *std::min_element(std::begin(similarity_), std::end(similarity_), [](TYPE const &one, TYPE const &two)
    {
        return one.first < two.first;
    });
}

/**
 * @brief copy the histogram with offset
 * @param input : input histogram
 * @param output : output histogram
 * @param factor : offset factor
 */
void chromaticyHistWB::shift_hist(cv::Mat const &input, cv::Mat &output, double factor)
{
    if(factor > 1){
        auto input_ptr = input.ptr<float>(0);
        auto output_ptr = output.ptr<float>(0);
        int const last_position = input.rows - 1;
        for(int i = 1; i != input.rows; ++i){
            int const out_index = i * factor;
            if(out_index <= last_position){
                *(output_ptr + out_index) += *(input_ptr + i);
            }else{
                *(output_ptr + last_position) += *(input_ptr + i);
            }
        }
        *output_ptr += *input_ptr;
    }else{
        int const begin = input.rows - 1;
        auto input_ptr = input.ptr<float>(begin);
        auto output_ptr = output.ptr<float>(0);
        for(int i = begin; i != 0; --i){
            int const out_index = i * factor;
            *(output_ptr + out_index) += *(input_ptr);
            --input_ptr;
        }
        *output_ptr += *(input_ptr);
    }
}

}
