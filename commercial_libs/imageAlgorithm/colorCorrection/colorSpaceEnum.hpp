#ifndef COLORSPACEENUM_HPP
#define COLORSPACEENUM_HPP

namespace OCV
{

enum colorSpace: int {
    BGR, HLS, Hue, Intensity, Lab, YCrCb, YUV, LastElement, colorSpaceSize = LastElement
};

}

#endif // COLORSPACEENUM_HPP
