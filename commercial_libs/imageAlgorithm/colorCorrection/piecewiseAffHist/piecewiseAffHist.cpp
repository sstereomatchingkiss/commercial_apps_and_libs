#include <algorithm>
#include <cmath>
#include <limits>
#include <type_traits>
#include <vector>

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "basicImageAlgo.hpp"
#include "debugHelper.hpp"
#include "piecewiseAffHist.hpp"

namespace
{

struct affinePolicy
{
    explicit affinePolicy(float x0, float x1, float y0, float slope) : slope_(slope),
        x0_(x0), x1_(x1), y0_(y0)
    {}

    template<typename T>
    typename std::enable_if<std::is_floating_point<T>::value, T>::type
    operator()(T in, T out) const
    {
        if (in >= x0_ && in <= x1_) {
            out = y0_ + slope_ * (in - x0_);
            if (out > 1.0){
                out = 1.0;
            }else if (out < 0){
                out = 0;
            }
        }

        return out;
    }

    template<typename T>
    typename std::enable_if<std::is_same<T, uchar>::value, T>::type
    operator()(T in, T out) const
    {
        if (in >= x0_ && in <= x1_) {
            out = cv::saturate_cast<T>(y0_ + slope_ * (in - x0_));
        }

        return out;
    }

private:
    float slope_;

    float x0_, x1_, y0_;
};

/**
 * @brief affine transformation the interval [x0,x1] into [y0,y1]
 * @param input  input data
 * @param output output data
 * @param x0, x1 initial interval
 * @param y0, y1 transformed interval
 */
template<typename T>
void affine_transformation(cv::Mat &input, cv::Mat &output,
                           float x0, float x1, float y0, float y1,
                           int channel)
{
    //float const max_intensity = !std::is_floating_point<T>::value ? 255 : 1.0;
    float const slope = (y1 - y0) / (x1 - x0);

    affinePolicy policy(x0, x1, y0, slope);
    OCV::transform_channel<T>(input, output, output, channel, [&](T in, T out)
    {
        return policy(in, out);
    });
}


float get_new_dynamic_range(float x0, float x1, float y0, float y1, float smin, float smax)
{
    float const slope = (y1 - y0) / (x1 - x0);

    if (slope > smax){
        y1 = smax * (x1 - x0) + y0;
    }else if (slope < smin){
        y1 = smin * (x1 - x0) + y0;
    }

    return y1;
}

template<typename T>
void piecewise_transformation_impl(cv::Mat &input, cv::Mat &output, size_t interval, float smin, float smax, size_t channel)
{
    std::vector<T> sorted_input = OCV::copy_to_one_dim_array_ch<T>(input, channel);
    std::sort(std::begin(sorted_input), std::end(sorted_input));

    //if(interval == 0){
    ++interval;
    //}
    float const min = sorted_input[0];
    float const max = sorted_input.back();
    float x0 = min;
    float y0 = 0.;
    float const max_intensity = !std::is_floating_point<T>::value ? 255 : 1.0;
    int const total_pixels = sorted_input.size();
    for (size_t k = 1; k != interval; k++){
        //percent_tile : percentage of the maximum value of k~k+1 interval
        float const percent_tile = (static_cast<float>(total_pixels * k) / (interval));
        float y1 = (max_intensity * k) / (interval);
        //x1 : maximum value of k~k+1 interval, inverse of cumulative function
        float x1 = sorted_input[static_cast<size_t>(percent_tile)];
        if (x1 > x0){
            y1 = get_new_dynamic_range(x0, x1, y0, y1, smin, smax);
            affine_transformation<T>(input, output, x0, x1, y0, y1, channel);
            x0 = x1;
            y0 = y1;
        }
    }

    //make sure every pixels of the output are transformed
    if (x0 < max) {
        float const x1 = max;
        float const y1 = get_new_dynamic_range(x0, x1, y0, max_intensity, smin, smax);
        affine_transformation<T>(input, output, x0, x1, y0, y1, channel);
    }
}

template<typename T>
void piecewise_transformation_multi_channel(cv::Mat &input, cv::Mat &output, size_t interval, float smin, float smax, OCV::colorSpace cl_space)
{
    cv::Mat temp_input = input;
    switch(input.channels()){
    case 4 :{
        temp_input = input.clone();
        cv::cvtColor(temp_input, temp_input, CV_BGRA2BGR);
        //don't need to add break, since it has to do the following process
    }
    case 3 :{
        switch(cl_space){
        case OCV::colorSpace::Intensity :{
            cv::Mat gray;
            OCV::mat8u_to_mat32f(temp_input, 1 / 255.0);
            cv::cvtColor(temp_input, gray, CV_BGR2GRAY);
            OCV::create_mat(temp_input, output);
            cv::Mat transform = gray.clone();
            piecewise_transformation_impl<float>(gray, transform, interval, smin, smax, 0);
            for(int row = 0; row != temp_input.rows; ++row){
                auto input_ptr_b = temp_input.ptr<float>(row);
                auto input_ptr_g = input_ptr_b + 1;
                auto input_ptr_r = input_ptr_g + 1;
                auto gray_ptr = gray.ptr<float>(row);
                auto output_ptr_b = output.ptr<float>(row);
                auto output_ptr_g = output_ptr_b + 1;
                auto output_ptr_r = output_ptr_g + 1;
                auto transform_ptr = transform.ptr<float>(row);
                for(int col = 0; col != temp_input.cols; ++col){
                    auto A = (*transform_ptr) / *gray_ptr;
                    *output_ptr_b = (A * *input_ptr_b);
                    *output_ptr_g = (A * *input_ptr_g);
                    *output_ptr_r = (A * *input_ptr_r);

                    if(*output_ptr_b > 1.0 || *output_ptr_g > 1.0 || *output_ptr_r > 1.0){
                        float const temp[] = {*input_ptr_b, *input_ptr_g, *input_ptr_r};
                        auto const B = std::max_element(temp, temp + 3);
                        A = 1.0 / *B;
                        *output_ptr_b = (A * *input_ptr_b);
                        *output_ptr_g = (A * *input_ptr_g);
                        *output_ptr_r = (A * *input_ptr_r);
                    }

                    input_ptr_b  += 3;
                    input_ptr_g  += 3;
                    input_ptr_r  += 3;
                    output_ptr_b += 3;
                    output_ptr_g += 3;
                    output_ptr_r += 3;
                    ++gray_ptr;
                    ++transform_ptr;
                }
            }
            OCV::mat32f_to_mat8u(output, 255);

            break;
        }
        case OCV::colorSpace::BGR :{
            OCV::create_mat(input, output);
            piecewise_transformation_impl<T>(input, output, interval, smin, smax, 0);
            piecewise_transformation_impl<T>(input, output, interval, smin, smax, 1);
            piecewise_transformation_impl<T>(input, output, interval, smin, smax, 2);
            break;
        }
        default:
            break;
        }
        break;
    }
    case 1 :{
        OCV::create_mat(input, output);
        piecewise_transformation_impl<T>(input, output, interval, smin, smax, 0);
        break;
    }
    default :{
        throw std::runtime_error(COMMON_DEBUG_MESSAGE + "input channels should be 1, 3 or 4");
        break;
    }
    }
}

}

namespace OCV{

void piecewise_affine_hist(cv::Mat &input, cv::Mat &output, size_t interval, float smin, float smax, OCV::colorSpace cl_space)
{
    if(smin > smax){
        std::swap(smin, smax);
    }

    if(smin == 0 && smax == 0){
        OCV::copy_if_not_same(input, output);
    }else if(OCV::is_uchar_channel(input.type())){
        piecewise_transformation_multi_channel<uchar>(input, output, interval, smin, smax, cl_space);
    }else if(OCV::is_float_channel(input.type())){
        piecewise_transformation_multi_channel<float>(input, output, interval, smin, smax, cl_space);
    }else{
        throw std::runtime_error(COMMON_DEBUG_MESSAGE + "channel types should be CV8UC1, CV_8UC3~4 or CV32FC1, CV32FC3~4");
    }
}

}
