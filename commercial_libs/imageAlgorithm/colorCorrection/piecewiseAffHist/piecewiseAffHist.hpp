#ifndef PIECEWISEAFFHIST_HPP
#define PIECEWISEAFFHIST_HPP

namespace cv
{

class Mat;

}

#include <cstdlib>

#include "colorCorrection/colorSpaceEnum.hpp"

namespace OCV{

/**
 * @brief piecewise affine histogram from the paper "Color and Contrast Enhancement by Controlled Piecewise Affine Histogram Equalization",
 * only support CV_8UC1~CV_8UC4 and CV_32FC1~CV32FC4.If the channel type is float, the range should lie within 0~1
 * @param input     input data, input data would not be altered if it do not point to the same data as output
 * @param output    output data
 * @param cl_space  color space
 * @param interval  the interval need to do piecewise transformation(in most cases, this should be 5)
 * @param smin      minimum slope(in most cases, this should be zero)
 * @param smax      maximum slope(in most cases, this should not exceed 3)
 *
 *  If the image is too dark, 2 for the maximum slope and 10 sub-intervals maybe a judiciuos choice
 *  This algorithm simply do affine transformation between different intervals, to avoid the risk of losing
 *  too much constrast or amplify noise, the slope--smin and smax are provided.
 */
void piecewise_affine_hist(cv::Mat &input, cv::Mat &output, size_t interval = 5, float smin = 0, float smax = 3, OCV::colorSpace cl_space = OCV::colorSpace::Intensity);

}

#endif // PIECEAFFINEHISTOGRAM_HPP
