#include "simplestColorBalanceHelper.hpp"

namespace OCV{

/**
 * @brief implementation of get quantiles, such that a given
 * number of pixels is out of this interval.I implemented it as a class
 * template to avoid duplicate codes
 */
std::pair<size_t, size_t> const getQuantile<uchar>::get_quantile(cv::Mat &input, cv::MatND &hist, size_t nmin, size_t nmax, int channel) const
{
    int const hist_size = std::numeric_limits<uchar>::max() + 1;
    float const hranges[2] = {0, 255};
    float const *ranges[] = {hranges};

    //compute and cumulate the histogram
    cv::calcHist(&input, 1, &channel, cv::Mat(), hist, 1, &hist_size, ranges);
    auto *hist_ptr = hist.ptr<float>(0);
    for(int i = 1; i != hist_size; ++i){
        hist_ptr[i] += hist_ptr[i - 1];
    }

    // get the new min/max
    std::pair<int, int> min_max(0, hist_size - 1);
    while(min_max.first != hist_size && hist_ptr[min_max.first] <= nmin){
        ++min_max.first; // the corresponding histogram value is the current cell position
    }

    while(min_max.second > 0 && hist_ptr[min_max.second] > (input.total() - nmax)){
        --min_max.second; // the corresponding histogram value is the current cell position
    }

    if (min_max.second < hist_size - 1)
        ++min_max.second;

    return min_max;
}

/**
 * @brief rescale an unsigned char array
 *
 * This function operates in-place. It rescales the data by a bounded
 * affine function such that min becomes 0 and max becomes UCHAR_MAX.
 *
 * @param input input array
 * @param output output size
 * @param min, max the minimum and maximum of the input array
 */
void rescaleMat<uchar>::rescale(cv::Mat &input, cv::Mat &output, uchar min, uchar max, int channel) const
{
    size_t const hist_size = std::numeric_limits<uchar>::max() + 1;
    if (max <= min){
        OCV::for_each_channel<uchar>(input, channel, [=](uchar &a){ a = (hist_size - 1) / 2; });
    }
    else{
        // build a normalization table
        uchar norm[hist_size] = {0};
        double const temp = (hist_size - 1) / static_cast<double>(max - min);
        for (size_t i = min; i != max; ++i){
            norm[i] = static_cast<unsigned char>((i - min) * temp + 0.5);
        }
        std::fill(norm + max, norm + hist_size, hist_size - 1);
        // use the normalization table to transform the data
        OCV::transform_channel<uchar>(output, channel, [&](uchar a){ return norm[a]; });
    }
}

}
