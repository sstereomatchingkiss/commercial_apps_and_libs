#ifndef SIMPLESTCOLORBALANCE_HPP
#define SIMPLESTCOLORBALANCE_HPP

#include <utility>
//#include <vector>

#include <opencv2/core/core.hpp>

#include "colorCorrection/colorSpaceEnum.hpp"

namespace OCV{

/**
 * @brief color balance proposed by the paper "Simplest Color Balance", only
 * CV_8UC1~CV_8UC4 and CV_32FC1~CV32FC4 channels could work properly by now.
 * When the channel is float, the range between the input is expected between
 * 0~1.
 *
 */
class simplestColorBalance
{
public:    
    simplestColorBalance();
    simplestColorBalance(simplestColorBalance const&) = delete;
    simplestColorBalance& operator=(simplestColorBalance const&) = delete;

    void simplest_color_balance(cv::Mat &input, cv::Mat &output, size_t smin, size_t smax, OCV::colorSpace cl_space = OCV::colorSpace::HLS);
    void simplest_color_balance(cv::Mat &input, cv::Mat &output, size_t smin, size_t smax, int channel);

#ifndef UNIT_TEST
private:
#endif
    cv::MatND hist_;
};

}

#endif // SIMPLESTCOLORBALANCE_HPP
