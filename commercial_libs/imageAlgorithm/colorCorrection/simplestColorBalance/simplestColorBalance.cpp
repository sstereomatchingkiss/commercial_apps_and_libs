#include <algorithm>
#include <iostream>
#include <stdexcept>

#include <opencv2/imgproc/imgproc.hpp>

#include "basicImageAlgo.hpp"
#include "simplestColorBalance.hpp"
#include "simplestColorBalanceHelper.hpp"
#include "debugHelper.hpp"

namespace{

/**
 * @brief color balance proposed by the paper "Simplest Color Balance"
 * @param input   the input image
 * @param output  the output image
 * @param smin    percentile of minimum pixels, 0~100%
 * @param smax    percentile of maximum pixels, 0~100%
 * @param channel the channel used to compute the histogram
 *
 */
template<typename T>
void simplest_color_balance_impl(cv::Mat &input, cv::Mat &output, cv::MatND &hist, size_t smin, size_t smax, int channel)
{
    if(smin + smax >= 100){
        smin = 49;
        smax = 50;
    }

    size_t const nmin = input.total() * smin / 100;
    size_t const nmax = input.total() * smax / 100;
    std::pair<T, T> min_max;//first : min, second : max
    if (nmin != 0 || nmax != 0){
        min_max = OCV::get_quantile<T>(input, hist, nmin, nmax, channel);
    }
    else{
        OCV::for_each_channel<T>(input, channel, [&](T a)
        {
            if(min_max.first > a){
                min_max.first = a;
            }

            if(min_max.second < a){
                min_max.second = a;
            }
        });
    }

    OCV::rescale<T>(input, output, min_max.first, min_max.second, channel);
}

/**
 * @brief     If input is a color image,
 * there are two choices for the users
 * 1 : Transform the inputs to HLS and apply the color balance on the H channel(cl_space == HLS)
 * 2 : Apply the algorithms on 3 channels(B, G, R)
 * @exception all exceptions are derived from std::exception
 */
template<typename T>
void color_image_balance(cv::Mat &input, cv::Mat &output, cv::MatND &hist, size_t smin, size_t smax, OCV::colorSpace cl_space)
{    
    cv::Mat temp_input = input;
    switch(input.channels()){
    case 4 :{
        //std::cout<<"scb 4 channels"<<std::endl;
        temp_input = input.clone();
        cv::cvtColor(temp_input, temp_input, CV_BGRA2BGR);
    } //don't need to add break, since it has to the following process
    case 3 :{
        switch(cl_space){
        case OCV::colorSpace::HLS :{
            //std::cout<<"scb 3 channels + hls"<<std::endl;
            OCV::copy_if_not_same(temp_input, output);
            cv::cvtColor(output, output, CV_BGR2HLS);
            simplest_color_balance_impl<T>(temp_input, output, hist, smin, smax, 1);
            cv::cvtColor(output, output, CV_HLS2BGR);
            break;
        }
        case OCV::colorSpace::BGR :{
            //std::cout<<"scb 3 channels + rgb"<<std::endl;
            OCV::copy_if_not_same(temp_input, output);
            simplest_color_balance_impl<T>(temp_input, output, hist, smin, smax, 0);
            simplest_color_balance_impl<T>(temp_input, output, hist, smin, smax, 1);
            simplest_color_balance_impl<T>(temp_input, output, hist, smin, smax, 2);
            break;
        }
        default:
            break;
        }
        break;
    }
    case 1 :{
        //std::cout<<"scb 1 channel"<<std::endl;
        OCV::copy_if_not_same(temp_input, output);
        simplest_color_balance_impl<T>(temp_input, output, hist, smin, smax, 0);
        break;
    }
    default :{
        throw std::runtime_error(COMMON_DEBUG_MESSAGE + "only support images with 4, 3, 1 channels");
    }
    }
}

}

namespace OCV{

simplestColorBalance::simplestColorBalance() {}

/**
 * @brief color balance design for 4, 3, 1 channel
 * @param input input data, input data would not be altered if it do not point to the same data as output
 * @param output output data
 * @param smin   minimum percentile
 * @param smax   maximum percentile
 * @param cl_space color space, could be BGR or HLS
 * @exception all exceptions are derived from std::exception
 *
 * This algorithm only support uchar channel and float channel by now. If input is a color image,
 * there are two choices for the users
 * 1 : Transform the inputs to HLS and apply the color balance on the H channel(cl_space == HLS)
 * 2 : Apply the algorithms on 3 channels(B, G, R)
 * If you need more sophisticated control on each channels, call other overload version of "simplest_color_balance" with
 * "channel" parameter.
 */
void simplestColorBalance::simplest_color_balance(cv::Mat &input, cv::Mat &output, size_t smin, size_t smax, OCV::colorSpace cl_space)
{
    if(OCV::is_uchar_channel(input.type())){
        color_image_balance<uchar>(input, output, hist_, smin, smax, cl_space);
    }else if(OCV::is_float_channel(input.type())){
        color_image_balance<float>(input, output, hist_, smin, smax, cl_space);
    }else{
        throw std::runtime_error(COMMON_DEBUG_MESSAGE + "The channels type should be CV_8UC1, 3, 4 or CV32FC1, 3, 4");
    }
}

/**
 * @brief color balance design for single channel
 * @param input input data, input data would not be altered if it do not point to the same data as output
 * @param output output data
 * @param smin   minimum percentile
 * @param smax   maximum percentile
 * @param channel number of the channel need to do color balance
 *
 * @exception all exceptions are derived from std::exception
 *
 * .Remember to copy the contents of input to the output before calling this function
 */
void simplestColorBalance::simplest_color_balance(cv::Mat &input, cv::Mat &output, size_t smin, size_t smax, int channel)
{
    if(OCV::is_uchar_channel(input.type())){
        simplest_color_balance_impl<uchar>(input, output, hist_, smin, smax, channel);
    }else if(OCV::is_float_channel(input.type())){
        simplest_color_balance_impl<float>(input, output, hist_, smin, smax, channel);
    }else{
        throw std::runtime_error(COMMON_DEBUG_MESSAGE + "The channels type should be CV_8UC1, 3, 4 or CV32FC1, 3, 4");
    }
}

}

/**********************************************************
 ****************** implementation ************************
 **********************************************************/
