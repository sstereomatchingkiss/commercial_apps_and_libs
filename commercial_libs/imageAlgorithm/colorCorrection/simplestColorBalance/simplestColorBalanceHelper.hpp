#ifndef COLORBALANCEHELPER_HPP
#define COLORBALANCEHELPER_HPP

#include <algorithm>
#include <limits>
#include <stdexcept>
#include <utility>
#include <vector>

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "basicImageAlgo.hpp"
#include "debugHelper.hpp"
#include "simplestColorBalanceHelper.hpp"

namespace OCV{

/**
 * some structure design to reduce duplicate codes of the class simplestColorBalance
 */

template<typename T>
struct getQuantile
{
    std::pair<T, T> const get_quantile(cv::Mat &input, cv::MatND&, size_t nmin, size_t nmax, int channel) const;
};

/**
* @brief generic algorithm for other channel types except of uchar, the type cv::MatND is needed to keep the api coherence
* @param input   the input image
* @param output  the output image
* @param smin    total number of minimum pixels
* @param smax    total number maximum pixels
* @param channel the channel used to compute the histogram
*
* This algorithm only support uchar channel and float channel by now
*/
template<typename T>
std::pair<T, T> const getQuantile<T>::get_quantile(cv::Mat &input, cv::MatND&, size_t smin, size_t smax, int channel) const
{
    std::vector<T> temp_input = OCV::copy_to_one_dim_array_ch<T>(input, channel);
    std::sort(std::begin(temp_input), std::end(temp_input));

    return std::pair<T, T>(temp_input[smin], temp_input[temp_input.size() - 1 - smax]);
}

template<>
struct getQuantile<uchar>
{
    std::pair<size_t, size_t> const get_quantile(cv::Mat &input, cv::MatND &hist, size_t nmin, size_t nmax, int channel) const;
};

template<typename T>
struct rescaleMat
{
    void rescale(cv::Mat &input, cv::Mat &output, T min, T max, int channel) const;
};

/**
 * generic algorithm for other channel types except of uchar, the first parameter is needed to keep the api coherence
 */
template<typename T>
void rescaleMat<T>::rescale(cv::Mat&, cv::Mat &output, T min, T max, int channel) const
{
    if(max <= min){        
        OCV::transform_channel<T>(output, channel, [](T)
        {
           return 0.5;
        });
    }else{
        OCV::transform_channel<T>(output, channel, [=](T data)
        {
            return (min > data ? T(0) :
                                    (max < data ? T(1) : (data - min) / (max - min)));
        });
    }
}

template<>
struct rescaleMat<uchar>
{
    void rescale(cv::Mat &input, cv::Mat &output, uchar min, uchar max, int channel) const;
};

template<typename T>
inline std::pair<T, T> const get_quantile(cv::Mat &input, cv::MatND &hist, size_t nmin, size_t nmax, int channel)
{
    return getQuantile<T>().get_quantile(input, hist, nmin, nmax, channel);
}

template<typename T>
inline void rescale(cv::Mat &input, cv::Mat &output, T min, T max, int channel)
{
    rescaleMat<T>().rescale(input, output, min, max, channel);
}

}

#endif // COLORBALANCEHELPER_HPP
