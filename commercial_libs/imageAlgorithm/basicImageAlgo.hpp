#ifndef BASICIMAGEALGO_HPP
#define BASICIMAGEALGO_HPP

#include <cmath>
#include <functional>
#include <iostream>
#include <stdexcept>
#include <string>

#include <opencv2/core/core.hpp>

#include <bugLocation.hpp>

#include "genericAlgo/genericForEach.hpp"
#include "genericAlgo/genericProcess.hpp"
#include "genericAlgo/genericRegion.hpp"
#include "genericAlgo/genericTransform.hpp"

namespace OCV
{

/**
 *@brief add specific value to specific channel with the help of
 * cv::saturate_cast<T>
 *
 *@param src     : input image
 *@param weight  : weight add to src
 *@param channel : channel to be added
 */
void add_channel(cv::Mat &src, int weight, int channel);

int mat8u_to_mat32f(cv::Mat &src, double alpha = 1.0);
int mat8u_to_mat32f(cv::Mat const &input, cv::Mat &output, double alpha = 1.0);

int mat32f_to_mat8u(cv::Mat &src, double alpha = 1.0);
int mat32f_to_mat8u(cv::Mat const &input, cv::Mat &output, double alpha = 1.0);

bool is_float_channel(int type);
bool is_uchar_channel(int type);

/**
 *@brief easy function for compare_channels, user should make sure T is the correct
 *channel type of src1.
 */
template<typename T, typename UnaryFunc>
typename std::enable_if<!std::is_same<UnaryFunc, cv::Mat>::value, bool>::type
compare_channels(cv::Mat const &src, UnaryFunc func)
{        
    int rows = src.rows;
    int cols = src.cols * src.channels();

    if(src.isContinuous()){
        rows = 1;
        cols = src.total() * src.channels();
    }

    for(int row = 0; row != rows; ++row){
        T const *src_ptr = src.ptr<T>(row);
        for(int col = 0; col != cols; ++col){
            if(!func(src_ptr[col])){
                return false;
            }
            //++src_ptr;

        }
    }

    return true;
}

/**
 *@brief : easy function for compare_channels, user should make sure T is the correct
 *channel type of src1 and src2
 */
template<typename T, typename BiFunc = std::equal_to<T> >
bool compare_channels(cv::Mat const &src1, cv::Mat const &src2, BiFunc func = std::equal_to<T>())
{    
    if(src1.rows != src2.rows || src1.cols != src2.cols || src1.type() != src2.type()){
        return false;
    }

    if(src1.isContinuous() && src2.isContinuous()){
        return std::equal(src1.ptr<T>(0), src1.ptr<T>(0) + src1.total() * src1.channels(), src2.ptr<T>(0), func);
    }

    int const rows = src1.rows;
    int const pixels_per_row = src1.cols * src1.channels();
    for(int row = 0; row != rows; ++row){
        T const *src1_ptr = src1.ptr<T>(row);
        T const *src2_ptr = src2.ptr<T>(row);
        for(int col = 0; col != pixels_per_row; ++col){
            if(!func(src1_ptr[col], src2_ptr[col])){
                return false;
            }
            //++src1_ptr; ++src2_ptr;
        }
    }

    return true;
}

/**
 * @brief: copy src to dst if their rows, cols or type are different
 */
inline void copy_if_not_same(cv::Mat const &src, cv::Mat &dst)
{
    if(src.data != dst.data){
        src.copyTo(dst);
    }
}

inline void create_mat(cv::Mat const &src, cv::Mat &dst)
{
    dst.create(src.rows, src.cols, src.type());
}

/**
 * @brief : experimental version for cv::Mat, try to alleviate the problem
 * of code bloat.User should make sure the space of begin point to
 * have enough of spaces.
 */
template<typename T, typename InputIter>
void copy_to_one_dim_array(cv::Mat const &src, InputIter begin)
{       
    if(src.isContinuous()){
        auto ptr = src.ptr<T>(0);
        std::copy(ptr, ptr + src.total() * src.channels(), begin);
        return;
    }

    size_t const pixel_per_row = src.cols * src.channels();
    for(int row = 0; row != src.rows; ++row){
        auto ptr = src.ptr<T>(row);
        std::copy(ptr, ptr + pixel_per_row, begin);
        begin += pixel_per_row;
    }
}

template<typename T>
std::vector<T> copy_to_one_dim_array(cv::Mat const &src)
{
    std::vector<T> result(src.total() * src.channels());
    copy_to_one_dim_array<T>(src, std::begin(result));

    return result;
}

/**
 * @brief experimental version for cv::Mat, try to alleviate the problem
 * of code bloat.User should make sure the space of begin point to
 * have enough of spaces.
 */
template<typename T, typename InputIter>
void copy_to_one_dim_array_ch(cv::Mat const &src, InputIter begin, int channel)
{
    int const channel_number = src.channels();
    if(channel_number <= channel || channel < 0){
        throw std::runtime_error("channel value is invalid\n" + std::string(__FUNCTION__) +
                                 "\n" + std::string(__FILE__));
    }

    for(int row = 0; row != src.rows; ++row){
        auto ptr = src.ptr<T>(row) + channel;
        for(int col = 0; col != src.cols; ++col){
            *begin = *ptr;
            ++begin;
            ptr += channel_number;
        }
    }
}

template<typename T>
std::vector<T> copy_to_one_dim_array_ch(cv::Mat const &src, int channel)
{
    std::vector<T> result(src.total());
    copy_to_one_dim_array_ch<T>(src, std::begin(result), channel);

    return result;
}

/**
 *@brief return the associative channel type of uchar
 * ex : type is CV_32F, return CV_8U; type is CV_32FC3, return CV_8UC3
 *      and so on
 *
 *@param type : the type of the channel, should be floating type, ex :
 *       CV_32F, CV_64F and so on
 *
 *@return :
 *  return the type number of the uchar channel type(CV_8U~CV_8UC4) if
 *  the type is valid;else return -1
 */
int float_channel_to_uchar(int type);

/**
 * @brief initialize the matrix start from T()
 */
template<typename T>
inline void initialize_mat(cv::Mat &src)
{
    T i = T();
    transform_channels<T>(src, [&](T){ return i++; });
}

int  uchar_channel_to_float(int type);

#include "basicImageAlgoImpl.hpp"

}

#endif // BASIC_HPP
