#ifndef DENOISE_HPP
#define DENOISE_HPP

namespace cv
{

class Mat;

}

namespace OCV{

void denoising(cv::Mat &src, cv::Mat &dst, float h=3, float hColor=3, int templateWindowSize=7, int searchWindowSize=21);

}


#endif // DENOISE_HPP
