#include <opencv2/core/core.hpp>
#include <opencv2/photo/photo.hpp>

#include "denoise.hpp"
#include "basicImageAlgo.hpp"

namespace OCV{

/*
 *@brief Wrapper of fastNlMeansDenoising and fastNlMeansDenoisingColored of openCV,
 * call fastNlMeansDenoising or  fastNlMeansDenoisingColored depend on the
 * type of the src
 *
 *@param src – Input 8-bit 3-channel image.
 *@param dst - Output image with the same size and type as src.
 *@param templateWindowSize – Size in pixels of the template patch that is used to compute weights.
 *                      Should be odd. Recommended value 7 pixels
 *@param searchWindowSize – Size in pixels of the window that is used to compute weighted average
 *                    for given pixel. Should be odd. Affect performance linearly: greater
 *                    searchWindowsSize - greater denoising time. Recommended value 21 pixels
 *@param h – Parameter regulating filter strength for luminance component. Bigger h value perfectly
 *            removes noise but also removes image details, smaller h value preserves details but also preserves some noise
 *@param hColor – The same as h but for color components. For most images value equals 10 will be
 *                enought to remove colored noise and do not distort colors
 *
 */
void denoising(cv::Mat &src, cv::Mat &dst, float h, float hColor, int templateWindowSize, int searchWindowSize)
{
    OCV::copy_if_not_same(src, dst);

    if(src.type() != CV_8U || src.type() != CV_8S){
        cv::fastNlMeansDenoisingColored(src, dst, h, hColor, templateWindowSize, searchWindowSize);
    }else{
        cv::fastNlMeansDenoising(src, dst, h, templateWindowSize, searchWindowSize);
    }
}

}
