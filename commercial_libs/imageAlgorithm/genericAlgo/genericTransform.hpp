#ifndef GENERICTRANSFORM_HPP
#define GENERICTRANSFORM_HPP

#include <cmath>
#include <functional>
#include <iostream>
#include <stdexcept>
#include <string>

#include <opencv2/core/core.hpp>

namespace OCV
{

template<typename T, typename UnaryFunctor, typename Mat>
void transform_channel(Mat &&src, int channel, UnaryFunctor functor);

template<typename T, typename UnaryFunctor, typename Mat>
void transform_channels(Mat &&src, UnaryFunctor functor);

template<typename T, typename UnaryFunctor, typename Mat>
void transform_continuous_channels(Mat &&src, UnaryFunctor functor);

template<typename T, typename BiFunctor, typename Mat>
void transform_continuous_channels(Mat &&src1, Mat &&src2, Mat &&dst, BiFunctor functor);

void transform_channel_verify(cv::Mat const &src1, cv::Mat const &src2, cv::Mat &dst);

/**
 *@brief apply transformation on a specific channel pixel
 *
 *@param src : input and output
 *@param channel : the channel you want to alter
 *@param functor : Take the specific channel pixels and manipulate it
 */
template<typename T, typename UnaryFunctor, typename Mat>
void transform_channel(Mat &&src, int channel, UnaryFunctor functor)
{
    int const channels = src.channels();
    if(channels == 1 && src.isContinuous()){
        return transform_continuous_channels<T>(std::forward<Mat>(src), functor);
    }

    for(int row = 0; row != src.rows; ++row){
        auto dst_ptr = src.template ptr<T>(row) + channel;
        for(int col = 0; col != src.cols; ++col){
            *dst_ptr = functor(*dst_ptr);
            dst_ptr += channels;
        }
    }
}

/**
 *@brief apply transformation on a specific channel pixel
 *
 *@param src : input and output
 *@param channel : the channel you want to alter
 *@param functor : Take the specific channel pixels and manipulate it
 *
 *@exception may throw std::bad_alloc
 */
template<typename T, typename UnaryFunctor, typename Mat>
void transform_channel(Mat &&src, Mat &&dst, int channel, UnaryFunctor functor)
{
    int const channels = src.channels();
    if(channels == 1 && src.isContinuous()){
        return transform_continuous_channels<T>(std::forward<Mat>(src), std::forward<Mat>(dst), functor);
    }

    if(src.rows != dst.rows || src.cols != dst.cols || src.type() != dst.type()){
        dst = cv::Mat(src.size(), src.type());
    }

    int const rows = src.rows;
    int const cols = src.cols;
    for(int row = 0; row != rows; ++row){
        auto dst_ptr = dst.template ptr<T>(row) + channel;
        auto src_ptr = src.template ptr<T>(row) + channel;
        for(int col = 0; col != cols; ++col){
            *dst_ptr = functor(*src_ptr);
            dst_ptr += channels;
            src_ptr += channels;
        }
    }
}

/**
 *@brief Overload of transform_channels, this version could alleviate problem of
 * code bloats
 *
 *@param src : input and output
 *@param functor : Take the specific channel pixels and manipulate it
 *@param T : template parameter, channel type of src
 */
template<typename T, typename UnaryFunctor, typename Mat>
void transform_channels(Mat &&src, UnaryFunctor functor)
{
    if(src.isContinuous()){
        return transform_continuous_channels<T>(std::forward<Mat>(src), functor);
    }

    size_t const pixel_per_row = src.cols * src.channels();
    for(int row = 0; row != src.rows; ++row){
        auto dst_ptr = src.template ptr<T>(row);
        std::transform(dst_ptr, dst_ptr + pixel_per_row, dst_ptr, functor);
    }
}

/**
 *@brief apply transformation on every pixels
 *
 *@param src : input
 *@param dst : output
 *@param functor : Take the specific channel pixels and manipulate it
 *
 *@exception may throw std::bad_alloc
 */
template<typename T, typename UnaryFunctor, typename Mat>
void transform_channels(Mat &&src, Mat &&dst, UnaryFunctor functor)
{
    if(src.rows != dst.rows || src.cols != dst.cols || src.type() != dst.type()){
        dst = cv::Mat(src.size(), src.type());
    }

    CV_Assert(src.rows == dst.rows && src.cols == dst.cols && src.type() == dst.type());

    int const channels = src.channels();
    if(channels == 1 && src.isContinuous() && dst.isContinuous()){
        auto src_ptr = src.template ptr<T>(0);
        std::transform(src_ptr, src_ptr + src.total() * src.channels(), dst.template ptr<T>(0), functor);

        return;
    }

    int const rows = src.rows;
    int const pixels_per_row = src.cols * src.channels();
    for(int row = 0; row != rows; ++row){
        auto dst_ptr = dst.template ptr<T>(row);
        auto src_ptr = src.template ptr<T>(row);
        std::transform(src_ptr, src_ptr + pixels_per_row, dst_ptr, functor);
    }
}

/**
 *@brief transform three channels Mat
 *
 *@param src : input and output
 *@param func_one : Take the specific channel pixels and manipulate it
 *@param func_two : Take the specific channel pixels and manipulate it
 *@param func_three : Take the specific channel pixels and manipulate it
 *@param T : template parameter, channel type of src
 */
template<typename T, typename UnaryFuncOne, typename UnaryFuncTwo, typename UnaryFuncThree, typename Mat>
void transform_three_channels(Mat &&src, UnaryFuncOne func_one, UnaryFuncTwo func_two, UnaryFuncThree func_three)
{    
    CV_Assert(src.channels() == 3);

    int rows = src.rows;
    int cols = src.cols;
    if(src.isContinuous()){
        rows = 1;
        cols = src.total() * 3;
    }

    for(int row = 0; row != rows; ++row){
        auto dst_ptr_begin = src.template ptr<T>(row);
        auto dst_ptr_end = dst_ptr_begin + cols;
        for(; dst_ptr_begin != dst_ptr_end; dst_ptr_begin += 3){
            dst_ptr_begin[0] = func_one(dst_ptr_begin[0]); //++dst_ptr;
            dst_ptr_begin[1] = func_two(dst_ptr_begin[1]); //++dst_ptr;
            dst_ptr_begin[2] = func_three(dst_ptr_begin[2]); //++dst_ptr;
            //dst_ptr_begin += 3;
        }
    }
}

/**
 *@brief transform three channels Mat
 *
 *@param src : input and output
 *@param func_one : Take the specific channel pixels and manipulate it
 *@param func_two : Take the specific channel pixels and manipulate it
 *@param func_three : Take the specific channel pixels and manipulate it
 *@param func_four : Take the specific channel pixels and manipulate it
 *@param T : template parameter, channel type of src
 */
template<typename T, typename UnaryFuncOne, typename UnaryFuncTwo, typename UnaryFuncThree, typename UnaryFuncFour>
void transform_four_channels(cv::Mat &src, UnaryFuncOne func_one, UnaryFuncTwo func_two, UnaryFuncThree func_three, UnaryFuncFour func_four)
{        
    CV_Assert(src.channels() == 4);

    int rows = src.rows;
    int cols = src.cols;
    if(src.isContinuous()){
        rows = 1;
        cols = src.total() * 4;
    }

    for(int row = 0; row != rows; ++row){
        auto dst_ptr_begin = src.ptr<T>(row);
        auto dst_ptr_end = dst_ptr_begin + cols;
        for(; dst_ptr_begin != dst_ptr_end; dst_ptr_begin += 4){
            dst_ptr_begin[0] = func_one(dst_ptr_begin[0]); //++dst_ptr;
            dst_ptr_begin[1] = func_two(dst_ptr_begin[1]); //++dst_ptr;
            dst_ptr_begin[2] = func_three(dst_ptr_begin[2]); //++dst_ptr;
            dst_ptr_begin[3] = func_four(dst_ptr_begin[3]); //++dst_ptr;
            //dst_ptr_begin += 4;
        }
    }
}

/**
 *@brief Overload of transform_channels, this version could alleviate problem of
 *code bloats
 *
 *@param src1 : first input
 *@param src2 : second input
 *@param dst : output, will have the same sizes and type as src1 and src2
 *@param functor : Take the specific channel pixels and manipulate it
 *@param T : template parameter, channel type of src1, src2, dst
 *
 *@exception may throw std::runtime_error, std::bad_alloc or other
 */
template<typename T, typename BinaryFunctor, typename Mat>
void transform_channels(Mat &&src1, Mat &&src2, Mat &&dst, BinaryFunctor functor)
{
    transform_channel_verify(src1, src2, dst);

    if(src1.isContinuous() && src2.isContinuous() && dst.isContinuous()){
        transform_continuous_channels<T>(std::forward<Mat>(src1), std::forward<Mat>(src2), std::forward<Mat>(dst), functor);
        return;
    }

    size_t const pixel_per_row = src1.cols * src1.channels();
    for(int row = 0; row != src1.rows; ++row){
        auto src1_ptr = src1.template ptr<T>(row);
        auto src2_ptr = src2.template ptr<T>(row);
        auto dst_ptr  = dst.template ptr<T>(row);
        std::transform(src1_ptr, src1_ptr + pixel_per_row, src2_ptr, dst_ptr, functor);
    }
}

template<typename T, typename BinaryFunctor, typename Mat>
void transform_channel(Mat &&src1, Mat &&src2, Mat &&dst, int channel, BinaryFunctor functor)
{
    transform_channel_verify(src1, src2, dst);

    if(src1.isContinuous() && src2.isContinuous() && dst.isContinuous()){
        transform_continuous_channels<T>(std::forward<Mat>(src1), std::forward<Mat>(src2), std::forward<Mat>(dst), functor);
        return;
    }

    int const rows = src1.rows;
    int const cols = src1.cols;
    int const channels = src1.channels();
    for(int row = 0; row != rows; ++row){
        auto src1_ptr = src1.template ptr<T>(row) + channel;
        auto src2_ptr = src2.template ptr<T>(row) + channel;
        auto dst_ptr  = dst.template ptr<T>(row)  + channel;
        for(int col = 0; col != cols; ++col){
            *dst_ptr  = functor(*src1_ptr, *src2_ptr);
            dst_ptr  += channels;
            src1_ptr += channels;
            src2_ptr += channels;
        }
    }
}

/**
 * @brief part of the implementation of transform_continuous_channel, this algorithm
 * do not check the input is continuous or not, it is not recommended to use it
 */
template<typename T, typename UnaryFunctor, typename Mat>
void transform_continuous_channels(Mat &&src, UnaryFunctor functor)
{
    auto dst_ptr = src.template ptr<T>(0);
    size_t const total_pixel = static_cast<size_t>(src.total() * src.channels());
    std::transform(dst_ptr, dst_ptr + total_pixel, dst_ptr, functor);
}

/**
 * @brief part of the implementation of transform_continuous_channel, this algorithm
 * do not check the input is continuous or not, it is not recommended to use it
 */
template<typename T, typename UnaryFunctor, typename Mat>
void transform_continuous_channels(Mat &&src, Mat &&dst, UnaryFunctor functor)
{
    if(src.rows != dst.rows || src.cols != dst.cols || src.type() != dst.type()){
        dst = cv::Mat(src.size(), src.type());
    }

    auto src_ptr = src.template ptr<T>(0);
    auto dst_ptr = dst.template ptr<T>(0);
    size_t const total_pixel = static_cast<size_t>(src.rows * src.cols * src.channels());
    std::transform(src_ptr, src_ptr + total_pixel, dst_ptr, functor);
}

/**
 * @brief part of the implementation of transform_continuous_channel, this algorithm
 * do not check the input is continuous or not, it is not recommended to use it
 */
template<typename T, typename BiFunctor, typename Mat>
void transform_continuous_channels(Mat &&src1, Mat &&src2, Mat &&dst, BiFunctor functor)
{
    transform_channel_verify(src1, src2, dst);

    auto src1_ptr = src1.template ptr<T>(0);
    auto src2_ptr = src2.template ptr<T>(0);
    auto dst_ptr = dst.template ptr<T>(0);
    size_t const total_pixel = static_cast<size_t>(src1.total() * src1.channels());
    std::transform(src1_ptr, src1_ptr + total_pixel, src2_ptr, dst_ptr, functor);
}

}

#endif // GENERICTRANSFORM_HPP
