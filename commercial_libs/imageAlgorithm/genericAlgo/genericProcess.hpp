#ifndef GENERICPROCESS_HPP
#define GENERICPROCESS_HPP

namespace OCV
{

/**
 * @brief generic algorithms for processing any byte-align three channels images
 *
 * @param inout : input image, must be three channels and byte-align; the restriction of byte-align
 *  may removed in the future
 * @param func : trinary functor, accept the pixels of three channels and process them
 * @return : return the functor
 *
 * @exception : every exception are derived from std::exception
 */
template<typename T, typename TriFunc, typename Mat>
TriFunc process_three_channels(Mat &&inout, TriFunc func)
{
    if(inout.channels() != 3){
        throw std::runtime_error(COMMON_DEBUG_MESSAGE + "src.channels() != 3");
    }

    int rows = inout.rows;
    int cols = inout.cols;
    if(inout.isContinuous()){
        rows = 1;
        cols = inout.total() * 3;
    }

    for(int row = 0; row != rows; ++row){
        auto input_ptr_begin = inout.template ptr<T>(row);
        auto input_ptr_end = input_ptr_begin + cols;
        for(; input_ptr_begin != input_ptr_end; input_ptr_begin += 3){
            func(input_ptr_begin[0], input_ptr_begin[1], input_ptr_begin[2]);
            //input_ptr_begin += 3;
        }
    }

    return func;
}

/**
 * @brief generic algorithms for processing any byte-align four channels images
 * @param input : input image, must be four channels and byte-align; the restriction of byte-align
 *  may removed in the future
 * @param func : accept the pixels of four channels and process them
 * @return : return the functor
 */
template<typename T, typename Func, typename Mat>
Func process_four_channels(Mat &&input, Func func)
{
    if(input.channels() != 4){
        throw std::runtime_error(COMMON_DEBUG_MESSAGE + "src.channels() != 4");
    }

    int rows = input.rows;
    int cols = input.cols;
    if(input.isContinuous()){
        rows = 1;
        cols = input.total() * 4;
    }

    for(int row = 0; row != rows; ++row){
        auto input_ptr_begin = input.template ptr<T>(row);
        auto input_ptr_end = input_ptr_begin + cols;
        for(; input_ptr_begin != input_ptr_end; input_ptr_begin += 4){
            func(input_ptr_begin[0], input_ptr_begin[1], input_ptr_begin[2], input_ptr_begin[3]);
            //input_ptr_begin += 4;
        }
    }

    return func;
}

}

#endif // GENERICPROCESS_HPP
