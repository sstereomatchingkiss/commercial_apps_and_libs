#include <bugLocation.hpp>

#include "genericRegion.hpp"

namespace
{

/**
 * @brief find out the block size, implementation of region_size
 * @param original_size : original size(rows or cols)
 * @param block_size : desired block size(rows or cols)
 * @return The sizes(rows or cols) of each block
 */
std::vector<int> region_size_impl(int original_size, int block_size)
{
    bool const remainder_is_zero = std::fmod(static_cast<double>(original_size), block_size) == 0;

    int total_pixels = 0;
    int const size = original_size / block_size;
    std::vector<int> results(size);
    for(int i = 0; i != size; ++i){
        if(i != size - 1){
           total_pixels += block_size;
           results[i] = block_size;
        }else{
            if(remainder_is_zero){
                results[i] = block_size;
            }else{
                results[i] = original_size - total_pixels;
            }
        }
    }

    return results;
}

}

namespace OCV
{

/**
 * @brief find out the block size of the region.The regions will divided with size equally to
 *  block_width * block_height if possible, else the size of some regions may become bigger than other regions.
 *
 *  ex : 1920 * 1080 with block_width == block_height == 100, the blocks size of every rows(except of last row) would be
 *  100 * 100, 100 * 100 ......, 120 * 100, the last row will be
 *  100 * 180, ..., 120 * 180
 *
 * @param input : input image
 * @param block_width : block width
 * @param block_height : block height
 * @exception all derived from std::exception
 * @return row sizes and col sizes of each block;std::pair<width sizes, height sizes>
 */
std::pair<std::vector<int>, std::vector<int> > region_size(cv::Mat const &input, int block_rows, int block_cols)
{
    if(input.rows < block_rows || input.cols < block_cols){
        throw std::runtime_error(COMMON_DEBUG_MESSAGE + "the size of input should not smaller than block_width or block_height");
    }

    return std::make_pair(region_size_impl(input.rows, block_rows), region_size_impl(input.cols, block_cols));
}

}
