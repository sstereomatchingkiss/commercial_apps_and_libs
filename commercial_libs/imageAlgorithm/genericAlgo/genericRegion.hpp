#ifndef GENERICREGION_HPP
#define GENERICREGION_HPP

#include <vector>

#include <opencv2/core/core.hpp>

namespace OCV
{

std::pair<std::vector<int>, std::vector<int> > region_size(cv::Mat const &input, int block_rows, int block_cols);

/**
 * @brief overload of region_algo_channel, the only different is the paramter block_sizes
 *
 * @param block_sizes : the block sizes of each block(rows, cols)
 */
template<typename T, typename Func, typename Mat>
void region_algo_channel(Mat &&input, int channel, std::pair<std::vector<int>, std::vector<int> > const &block_sizes, Func func)
{
    int const rows = block_sizes.first.size();
    int const cols = block_sizes.second.size();
    int col_index = 0;
    int row_index = 0;
    for(int row = 0; row != rows; ++row){
        for(int col = 0; col != cols; ++col){
            cv::Mat roi = cv::Mat(std::forward<Mat>(input), cv::Rect(col_index, row_index, block_sizes.second[col], block_sizes.first[row]));
            col_index += block_sizes.second[col];
            func(roi, channel);
        }
        col_index = 0;
        row_index += block_sizes.first[row];
    }
}

/**
 * @brief Apply the algorithms on the divided regions.The regions will divided with size equally to
 *  block_width * block_height if possible, else the size of some regions may become bigger than other regions.
 *
 *  ex : 1920 * 1080 with block_width == block_height == 100, the blocks size of every rows(except of last row) would be
 *  100 * 100, 100 * 100 ......, 120 * 100, the last row will be
 *  100 * 180, ..., 120 * 180
 *
 * @param input : input image
 * @param channel : channel want to process
 * @param block_cols : block width
 * @param block_rows : block height
 * @exception all derived from std::exception
 * @return row sizes and col sizes of each block
 */
template<typename T, typename Func, typename Mat>
inline void region_algo_channel(Mat &&input, int channel, int block_rows, int block_cols, Func func)
{
    region_algo_channel<T>(std::forward<Mat>(input), channel, region_size(std::forward<Mat>(input), block_rows, block_cols), func);
}

/**
 * @brief overload of region_algo_channel, this version don't need to pass in channel
 *
 * @param block_sizes : the block sizes of each block(rows, cols)
 */
template<typename T, typename Func, typename Mat>
inline void region_algo_channel(Mat &&input, int block_rows, int block_cols, Func func)
{
    region_algo_channel<T>(std::forward<Mat>(input), 0, region_size(std::forward<Mat>(input), block_rows, block_cols), func);
}

/**
 * @brief overload of region_algo_channel, this version don't need to pass in channel
 *
 * @param block_sizes : the block sizes of each block(rows, cols)
 */
template<typename T, typename Func, typename Mat>
inline void region_algo_channel(Mat &&input, std::pair<std::vector<int>, std::vector<int> > const &block_sizes, Func func)
{
    region_algo_channel<T>(std::forward<Mat>(input), 0, block_sizes, func);
}

}

#endif // GENERICREGION_HPP
