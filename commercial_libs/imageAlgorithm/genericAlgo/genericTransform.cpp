#include "genericTransform.hpp"

namespace OCV
{

/**
 * @brief verify the input and output of the transform series algorithms with two inputs and one output are valid or not
 * @param src1   input data
 * @param src2   input data
 * @param dst    output data
 */
void transform_channel_verify(cv::Mat const &src1, cv::Mat const &src2, cv::Mat &dst)
{   
    CV_Assert(src1.rows == src2.rows && src1.cols == src2.cols && src1.type() == src2.type());

    if(dst.rows != src1.rows || dst.cols != src1.cols || dst.type() != src1.type()){
        dst.create(src1.size(), src1.type());
    }
}

}
