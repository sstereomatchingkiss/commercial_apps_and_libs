#ifndef BASICIMAGEALGOFUNCTOR_HPP
#define BASICIMAGEALGOFUNCTOR_HPP

#include "basicImageAlgo.hpp"

namespace OCV{

struct bgr2Chromaticiy
{
    bgr2Chromaticiy(int hist_range = 255) : hist_range_(hist_range) {}

    template<typename T>
    typename std::enable_if<std::is_floating_point<T>::value>::type operator()(T &b, T &g, T &r)
    {
        T const sum = b + g + r;
        b /= sum;
        g /= sum;
        r = 1 - (b + g);
        //r = r / sum * r;
    }

    template<typename T>
    typename std::enable_if<std::is_integral<T>::value && !std::is_same<bool, T>::value>::type operator()(T &b, T &g, T &r)
    {
        double const sum = b + g + r;
        b = cv::saturate_cast<uchar>(b / sum * hist_range_);
        g = cv::saturate_cast<uchar>(g / sum * hist_range_);
        r = hist_range_ - (b + g);
    }

private:
    int const hist_range_;
};

/**
 *  find the minimum and maximum of the Mat, could associate with
 *  for_each series(for_each_channel, for_each_channels and so on)
 *  algorithms, user should make sure the type T is copyable and assignable
 */
template<typename T>
struct minMax
{
    minMax(T min, T max) : min_max_(min, max) {}

    minMax(std::pair<T, T> const &min_max) : min_max_(min_max) {}

    minMax(std::pair<T, T> &&min_max) : min_max_(std::move(min_max)) {}

    void operator()(T value)
    {
        if(min_max_.first > value){
            min_max_.first = value;
        }

        if(min_max_.second < value){
            min_max_.second = value;
        }
    }

    std::pair<T, T> get_result() { return min_max_; }

private:
    std::pair<T, T> min_max_;
};

struct variance_one_ch
{
    variance_one_ch(double mean) : result_(0), mean_(mean) {}

    template<typename T>
    void operator()(T data)
    {
        double const temp = data - mean_;
        result_ += temp * temp;
    }

    double result_;

private:
    double mean_;
};

template<typename Mean>
struct variance_three_ch
{
    variance_three_ch(Mean const &mean) : result_(3), mean_(mean) {}

    template<typename T>
    void operator()(T b, T g, T r)
    {
        double const b_temp = b - mean_[0];
        double const g_temp = g - mean_[1];
        double const r_temp = r - mean_[2];

        result_[0] += b_temp * b_temp;
        result_[1] += g_temp * g_temp;
        result_[2] += r_temp * r_temp;
    }

    std::vector<double> result_;

private:
    Mean const &mean_;
};

template<typename Mean>
struct variance_four_ch
{
    variance_four_ch(Mean const &mean) : result_(4), mean_(mean) {}

    template<typename T>
    void operator()(T b, T g, T r, T a)
    {
        double const b_temp = b - mean_[0];
        double const g_temp = g - mean_[1];
        double const r_temp = r - mean_[2];
        double const a_temp = a - mean_[3];

        result_[0] += b_temp * b_temp;
        result_[1] += g_temp * g_temp;
        result_[2] += r_temp * r_temp;
        result_[3] += a_temp * a_temp;
    }

    std::vector<double> result_;

private:
    Mean const &mean_;
};

template<typename T = uchar, typename MeanType = float>
struct regionMean
{    
    void operator()(cv::Mat &input, int channel)
    {        
        MeanType sum = 0;
        for_each_channel<T>(input, channel, [&](T data)
        {
            sum += data;
        });
        result_.emplace_back(sum / input.total());
    }

    std::vector<MeanType> result_;
    //int channel_;
};

template<typename T = uchar, typename DiffType = float>
struct regionAvgAbsDiff
{
    regionAvgAbsDiff(std::vector<DiffType> const &mean) : index_(0), mean_(mean) {}

    void operator()(cv::Mat &input, int channel)
    {        
        DiffType sum = 0;
        for_each_channel<T>(input, channel, [&](T data)
        {
            sum += std::abs(data - mean_[index_]);
        });
        result_.emplace_back(sum / input.total());
        ++index_;
    }

    std::vector<DiffType> result_;

private:
    int index_;

    std::vector<DiffType> const &mean_;
};

}

#endif // BASICIMAGEALGOFUNCTOR_HPP
