#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "basicImageAlgo.hpp"
#include "basicSharpenAlgo.hpp"

namespace OCV
{

/**
 *@brief use average filter to sharpen image(unsharp mask)
 */
void sharpen_image_average(cv::Mat &src, cv::Mat &dst, cv::Size ksize, double amount, double threshold)
{
    cv::Mat blurred;
    cv::blur(src, blurred, ksize);

    dst = unsharp_image(src, blurred, amount, threshold);
}

void sharpen_image_bilateral(cv::Mat &src, cv::Mat &dst, int d, double sigma_color, double sigma_space, double amount, double threshold)
{
    cv::Mat blurred;
    cv::bilateralFilter(src, blurred, d, sigma_color, sigma_space);

    dst = unsharp_image(src, blurred, amount, threshold);
}

/**
 *@brief apply unsharp masking on source with the help of cv::GaussianBlur
 *
 *@param
 * sigma1sigma2 : The Gaussian kernel standard deviations in X and Y direction.
 *                If sigmaY is zero, it is set to be equal to sigmaX. If they are both zeros,
 *                they are computed from ksize.width and ksize.height.
 *                To fully control the result regardless of possible future modification
 *                of all this semantics, it is recommended to specify all of ksize,
 *                sigmaX and sigmaY
 *
 *@param
 * amout : controls the magnitude of each overshoot. This can also be thought of as how much
 *         contrast is added at the edges.
 *
 *@param
 * threshold : sets the minimum brightness change that will be sharpened. This is equivalent to
 *             clipping off the darkest non-black pixel levels in the unsharp mask. The threshold
 *             setting can be used to sharpen pronounced edges, while leaving subtle edges
 *             untouched. This is especially useful to avoid amplifying noise, or to sharpen an
 *             eye lash without also roughening skin texture.
 */
void sharpen_image_gaussian(cv::Mat &src, cv::Mat &dst, double sigma1, double sigma2, double amount, double threshold)
{
    cv::Mat blurred;
    cv::GaussianBlur(src, blurred, cv::Size(), sigma1, sigma2);

    dst = unsharp_image(src, blurred, amount, threshold);
}

void sharpen_image_filter(cv::Mat &src, cv::Mat &dst, int depth)
{
    cv::Mat const filter = (cv::Mat_<float>(3, 3) <<
                            0, -3, 0
                            -3, 21, -3,
                            0, -3, 0) / 9.0;

    cv::filter2D(src, dst, depth, filter);
}

void sharpen_image_laplacian(cv::Mat &src, cv::Mat &dst, int kernal_size)
{
    cv::Mat edges;
    cv::Laplacian(src, edges, CV_32F, kernal_size);
    cv::normalize(edges, edges, 0, 255, cv::NORM_MINMAX);

    cv::Mat temp_src = src.clone();
    OCV::mat8u_to_mat32f(temp_src);
    dst = temp_src - edges;
    cv::normalize(dst, dst, 0, 255, cv::NORM_MINMAX);

    OCV::mat32f_to_mat8u(dst);
}

/**
 *@brief use medium blur to sharpen image(unsharp masking), the color space of src
 * should be bgr(bgra will be transform to bgr).
 */
void sharpen_image_medium(cv::Mat &src, cv::Mat &dst, int ksize, double amount, double threshold)
{
    cv::Mat blurred;
    cv::medianBlur(src, blurred, ksize);

    dst = unsharp_image(src, blurred, amount, threshold);
}

/**
 *@brief use smart unsharp to sharpen the image.This algorithm will transform the image
 * from BGR to LAB(BGRA will transform to BGR).
 *
 *@param src : input, the type should be CV_8U, CV_8UC3, CV_8UC4;value range is 0~255;
 *      color space is BGR.
 *
 *@param aperture_size – aperture size for the Sobel() operator.Must be 3, 5, 7
 *
 *@param  amout : Controls the magnitude of each overshoot. This can also be thought of as how much
 *        contrast is added at the edges.
 *
 *@param  threshold : Sets the minimum brightness change that will be sharpened. This is equivalent to
 *            clipping off the darkest non-black pixel levels in the unsharp mask. The threshold
 *            setting can be used to sharpen pronounced edges, while leaving subtle edges
 *            untouched. This is especially useful to avoid amplifying noise, or to sharpen an
 *            eye lash without also roughening skin texture.
 */
void smart_unsharp_image(cv::Mat &src, cv::Mat &dst, int aperture_size, double amount, double threshold)
{
    if(src.data != dst.data){
        dst = src.clone();
    }

    cv::cvtColor(dst, dst, CV_BGR2Lab);
    std::vector<cv::Mat> target_color_space;
    cv::split(dst, target_color_space);

    cv::Mat edges;
    cv::Canny(target_color_space[0], edges, threshold, threshold * 3, aperture_size, true);

    cv::Mat blurred;
    cv::GaussianBlur(target_color_space[0], blurred, cv::Size(aperture_size, aperture_size), 0);
    cv::Mat const sharpen = target_color_space[0] * (amount + 1) + blurred * (-amount);
    sharpen.copyTo(target_color_space[0], edges);

    cv::merge(target_color_space, dst);
    cv::cvtColor(dst, dst, CV_Lab2BGR);
}

/**
 *@brief unsharp masking technique
 *
 *@param  amout : Controls the magnitude of each overshoot. This can also be thought of as how much
 *        contrast is added at the edges.
 *
 *@param  threshold : Sets the minimum brightness change that will be sharpened. This is equivalent to
 *            clipping off the darkest non-black pixel levels in the unsharp mask. The threshold
 *            setting can be used to sharpen pronounced edges, while leaving subtle edges
 *            untouched. This is especially useful to avoid amplifying noise, or to sharpen an
 *            eye lash without also roughening skin texture.
 */
cv::Mat unsharp_image(cv::Mat const &src, cv::Mat const &blurred, double amount, double threshold)
{
    //those pixels smaller than threshold will be set to 255
    cv::Mat lowConstrastMask = cv::abs(src - blurred) < threshold;
    OCV::mat32f_to_mat8u(lowConstrastMask);
    cv::Mat sharpened = src + (src - blurred) * amount;
    //do not sharpen those pixels smaller than threshold
    src.copyTo(sharpened, lowConstrastMask);

    return sharpened;
}

}
