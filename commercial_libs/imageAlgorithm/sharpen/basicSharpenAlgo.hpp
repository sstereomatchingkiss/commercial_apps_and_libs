#ifndef BASICSHARPENALGO_HPP
#define BASICSHARPENALGO_HPP

namespace cv
{

class Mat;

}

namespace OCV
{

void sharpen_image_average(cv::Mat const &src, cv::Mat &dst, cv::Size ksize, double amount = 1, double threshold = 5);

void sharpen_image_bilateral(cv::Mat const &src, cv::Mat &dst, int d, double sigma_color, double sigma_space, double amount = 1, double threshold = 5);

void sharpen_image_gaussian(cv::Mat &src, cv::Mat &dst, double sigma1, double sigma2 = 0, double amount = 1, double threshold = 5);

void sharpen_image_filter(cv::Mat &src, cv::Mat &dst, int depth = CV_8U);

void sharpen_image_laplacian(cv::Mat &src, cv::Mat &dst, int kernal_size = 3);

void sharpen_image_medium(cv::Mat &src, cv::Mat &dst, int ksize, double amount = 1, double threshold = 5);

void smart_unsharp_image(cv::Mat &src, cv::Mat &dst, int aperture_size, double amount = 1, double threshold = 5);

cv::Mat unsharp_image(cv::Mat const &src, cv::Mat const &blurred, double amount = 1, double threshold = 5);

}

#endif // BASICSHARPENALGO_HPP
