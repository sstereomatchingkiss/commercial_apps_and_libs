#ifndef COLORFLAG_HPP
#define COLORFLAG_HPP

#include <opencv2/imgproc/imgproc.hpp>

namespace OCV
{

constexpr class BGR2SBGR_TAG{} BGR2SBGR_T{};
constexpr class BGR2SGM_TAG{}  BGR2SGM_T{};
constexpr class SBGR2BGR_TAG{} SBGR2BGR_T{};
constexpr class SGM2BGR_TAG{}  SGM2BGR_T{};

constexpr class BGR_TAG{}       BGR_T{};
constexpr class HLS_TAG{}       HLS_T{};
constexpr class HUE_TAG{}       HUE_T{};
constexpr class INTENSITY_TAG{} INTENSITY_T{};
constexpr class LAB_TAG{}       LAB_T{};
constexpr class YCRCB_TAG{}     YCRCB_T{};
constexpr class YUV_TAG{}       YUV_T{};

}

#endif // COLORFLAG_HPP
