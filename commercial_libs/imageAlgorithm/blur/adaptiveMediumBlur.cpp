#include "adaptiveMediumBlurImpl.hpp"

namespace OCV
{

/*
 *@brief adaptive medium blur, support CV_8U, CV_8UC3, CV_8UC4,
 * CV_32F, CV_32FC3, CV_32FC4.
 *
 *@param src : input
 *@param dst : output
 *@param min_kernal_size : minimum kernal size
 *@param max_kernal_size : maximum kernal size
 */
void adaptive_medium_blur(cv::Mat &src, cv::Mat &dst, int min_kernal_size, int max_kernal_size)
{
    OCV::copy_if_not_same(src, dst);

    switch(src.type()){
    case CV_8U:
    case CV_8UC3:
    case CV_8UC4:{
        adaptive_medium_blur_impl<unsigned char>(src, dst, min_kernal_size, max_kernal_size);
        break;
    }  
    case CV_32F:
    case CV_32FC3:
    case CV_32FC4:{
        adaptive_medium_blur_impl<float>(src, dst, min_kernal_size, max_kernal_size);
        break;
    }
    default :
        break;
    }
}

}
