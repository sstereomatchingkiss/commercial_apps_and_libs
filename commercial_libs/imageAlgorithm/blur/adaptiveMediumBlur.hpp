#ifndef ADAPTIVEMEDIUMBLUR_HPP
#define ADAPTIVEMEDIUMBLUR_HPP

namespace cv{
class Mat;
}

namespace OCV
{

void adaptive_medium_blur(cv::Mat &src, cv::Mat &dst, int min_kernal_size = 3, int max_kernal_size = 15);

}

#endif // ADAPTIVEMEDIUMBLUR_HPP
