#ifndef ADAPTIVEMEDIUMBLURIMPL_HPP
#define ADAPTIVEMEDIUMBLURIMPL_HPP

#include <vector>

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "basicImageAlgo.hpp"
#include "utility/getDataHelper.hpp"

namespace OCV
{

/*
 *implementation of adaptive median filters
 *
 *@param : *src : The source channel image.
 *@param dst : The destination array; will have the same size and the same type as SRC
 *@param msize : The maximum aperture linear size. It must be odd and more than 1, i.e. 3, 5, 7 ...
 *@param T : template parameter refer to the channel type of src and dst
 *
 *@exception :
 * may throw std::bad_alloc
*/
template<typename T>
void adaptive_medium_blur_impl(cv::Mat &src, cv::Mat &dst, int min_kernal_size = 3, int max_kernal_size = 15)
{
    if(min_kernal_size > max_kernal_size){
        std::swap(min_kernal_size, max_kernal_size);
    }

    if(min_kernal_size < 3){
        min_kernal_size = 3;
    }else if(min_kernal_size % 2 != 1){
        ++min_kernal_size;
    }

    if(max_kernal_size % 2 != 1){
        ++max_kernal_size;
    }

    if(dst.rows != src.rows || dst.cols != src.cols){
        dst = cv::Mat(src.rows, src.cols, src.type());
    }else if(dst.type() != src.type()){
        dst.convertTo(dst, src.type());
    }

    int const span = max_kernal_size / 2; //the number of borders need to be interpolate
    cv::Mat temp;
    cv::copyMakeBorder(src, temp, span, span, span, span, cv::BORDER_REPLICATE);

    typedef T Value;
    std::vector<Value> block_value(max_kernal_size); //store the value of the block which surround the anchor pixel
    int const last_row = temp.rows - span;
    int const last_col = temp.cols - span;

    for(int row = span; row != last_row; ++row){
        int const row_num = row - span;
        auto dst_ptr = get_pointer<T>(dst, row_num);
        auto src_ptr = get_pointer<T>(src, row_num);
        for(int col = span; col != last_col; ++col){
            for(int channel = 0; channel != src.channels(); ++channel){
                for(int size = min_kernal_size; size <= max_kernal_size; size += 2){
                    int const half_size = size / 2;
                    cv::Mat const roi = temp(cv::Rect(col - half_size, row - half_size, size, size));
                    block_value.resize(size * size);
                    OCV::copy_to_one_dim_array<T>(roi, std::begin(block_value), channel);
                    std::sort(std::begin(block_value), std::end(block_value));
                    Value const min = block_value[0]; //minimum value of block
                    Value const max = block_value.back(); //maximum value of block
                    Value const mean = block_value[block_value.size() / 2]; //mean value of block
                    block_value.clear();

                    if(mean > min && mean < max){
                        if(*src_ptr > min && *src_ptr < max){
                            *dst_ptr = *src_ptr;
                        }else{
                            *dst_ptr = mean;
                        }
                        ++src_ptr; ++dst_ptr;
                        break;
                    }else if(size == max_kernal_size){
                        *dst_ptr = mean;
                        ++src_ptr; ++dst_ptr;
                    }
                }
            }
        }
    }
}

}

#endif // ADAPTIVEMEDIUMBLURIMPL_HPP
