#ifndef DRAWRECT_HPP
#define DRAWRECT_HPP

#include <opencv2/core/core.hpp>

#include <vector>

namespace OCV
{

void draw_rect(cv::Mat &inout, cv::Rect const &rect, cv::Scalar const &color = cv::Scalar(255, 0, 0));
void draw_rects(cv::Mat &inout, std::vector<cv::Rect> const &rects, cv::Scalar const &color = cv::Scalar(255, 0, 0));

/**
 * @brief generate points from the rectangle by clockwise order(top left,
 * top right, bottom right, bottom left)
 * @param rect   : input retangle
 * @param points : generated points
 */
template<typename Point>
void get_rect_points(cv::Rect const &rect, Point points[])
{
    points[0].x = rect.x;
    points[0].y = rect.y;
    points[1].x = rect.x + rect.width - 1;
    points[1].y = rect.y;
    points[2].x = points[1].x;
    points[2].y = rect.y + rect.height - 1;
    points[3].x = rect.x;
    points[3].y = points[2].y;
}

}

#endif // DRAWRECT_HPP
