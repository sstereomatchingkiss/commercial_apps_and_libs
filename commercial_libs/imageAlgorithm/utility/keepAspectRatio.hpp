#ifndef GETASPECTRATIOSIZE_HPP
#define GETASPECTRATIOSIZE_HPP

namespace OCV{

class pixelKeepAspectRatioWidth
{
public:
    constexpr double operator()(double ori_width, double ori_height, double new_height) const
    {        
        return ori_width /ori_height * new_height;
    }
};

class pixelKeepAspectRatioHeight
{
public:
    constexpr double operator()(double ori_width, double ori_height, double new_width) const
    {        
        return ori_height / ori_width * new_width;
    }
};

/*
 * input the new height of the image, return new width which keep aspect ratio
 *
 *@param ori_width  : original width
 *@param ori_height : original height
 *@param new_height : new height
 */
constexpr double pixel_base_keep_aspect_ratio_width(double ori_width, double ori_height, double new_height)
{
    return pixelKeepAspectRatioWidth()(ori_width, ori_height, new_height);
}

/*
 * input the new width of the image, return new height which keep aspect ratio
 *
 *@param ori_width  : original width
 *@param ori_height : original height
 *@param new_width  : new width
 */
constexpr double pixel_base_keep_aspect_ratio_height(double ori_width, double ori_height, double new_width)
{
    return pixelKeepAspectRatioHeight()(ori_width, ori_height, new_width);
}

}

#endif // GETASPECTRATIOSIZE_HPP
