#ifndef DIMENSIONMAPPER_HPP
#define DIMENSIONMAPPER_HPP

namespace OCV{

/*
 * map the index of two dimensional array
 * to index of one dimension array
 */
class mapDimTwo2One
{
  public :
     constexpr mapDimTwo2One() : channel_number_(1), element_per_row_(0) {}

     /*
      *@param channel_number : rgb has 3 channels, rgba has four channels and so on
      *@param element_per_row : a 3 channels 3*3 rectangle has 3(width) * 3(channel) number
      * of elements
      */
     constexpr explicit mapDimTwo2One(int channel_number, int element_per_row) :
     channel_number_(channel_number), element_per_row_(element_per_row)
     {}

    /*
     * transform two dimension index into one dimension index
     *
     *@param row : index of row
     *@param col : index of col
     */
    constexpr int operator()(int row, int col)
    {
        return row * element_per_row_ + col;
    }

    /*
     * transform two dimension index into one dimension index,
     * this function is useful when you want to deal with two
     * dimension interleave image
     *
     *@param row : index of row
     *@param col : index of col
     *@param channel : channel of the image.
     *
     *@example :
     *image(0, 0, 0), image(0, 0, 1), image(0, 0, 2)
     *represent location of pixel R, G, B
     */
    constexpr int operator()(int row, int col, int channel)
    {
        return row * element_per_row_ + col * channel_number_ + channel;
    }

    void set_channel_number_(int num) { channel_number_ = num;}
    void set_element_per_row(int num) { element_per_row_ = num;}

  private :
    int channel_number_; //channels of the image(rgb, bgr, yuv and so on)
    int element_per_row_; //column number of the two dimension array you want to transform
};

}

#endif // DIMENSIONMAPPER_HPP
