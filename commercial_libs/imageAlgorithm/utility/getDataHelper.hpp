#ifndef GETDATAHELPER_HPP
#define GETDATAHELPER_HPP

#include <opencv2/core/core.hpp>

#include "metaProgrammingHelper.hpp"

namespace OCV{

/*
 * help user get the pixel or pixel data, make the source codes become more generic
 */

template<typename T>
inline auto get_pixel_impl(cv::Mat_<T> const &src, int row, int col, int, int2Type<1>)->decltype(src(0, 0))
{
    return src(row, col);
}

template<typename T>
inline auto get_pixel_impl(cv::Mat_<T> const &src, int row, int col, int channel, int2Type<2>)->decltype(src(0, 0)[channel])
{
    return src(row, col)[channel];
}

template<typename T>
inline auto get_pixel_impl(cv::Mat_<T> const &src, int row, int col, int channel, int2Type<3>)->decltype(src(0, 0)[channel])
{
    return src(row, col)[channel];
}

template<typename T>
inline auto get_pixel_impl(cv::Mat_<T> const &src, int row, int col, int channel, int2Type<4>)->decltype(src(0, 0)[channel])
{
    return src(row, col)[channel];
}

template<typename T>
inline auto get_pointer_impl(cv::Mat_<T> &src, int row, int col, int2Type<1>)->decltype(&src(row, col))
{
    return &src(row, col);
}

template<typename T>
inline auto get_pointer_impl(cv::Mat_<T> &src, int row, int col, int2Type<2>)->decltype(&src(row, col)[0])
{
    return &src(row, col)[0];
}

template<typename T>
inline auto get_pointer_impl(cv::Mat_<T> &src, int row, int col, int2Type<3>)->decltype(&src(row, col)[0])
{
    return &src(row, col)[0];
}

template<typename T>
inline auto get_pointer_impl(cv::Mat_<T> &src, int row, int col, int2Type<4>)->decltype(&src(row, col)[0])
{
    return &src(row, col)[0];
}

template<typename T>
inline auto get_pointer_impl(cv::Mat_<T> const &src, int row, int col, int2Type<1>)->decltype(&src(row, col))
{
    return &src(row, col);
}

template<typename T>
inline auto get_pointer_impl(cv::Mat_<T> const &src, int row, int col, int2Type<2>)->decltype(&src(row, col)[0])
{
    return &src(row, col)[0];
}

template<typename T>
inline auto get_pointer_impl(cv::Mat_<T> const &src, int row, int col, int2Type<3>)->decltype(&src(row, col)[0])
{
    return &src(row, col)[0];
}

template<typename T>
inline auto get_pointer_impl(cv::Mat_<T> const &src, int row, int col, int2Type<4>)->decltype(&src(row, col)[0])
{
    return &src(row, col)[0];
}

template<typename T>
inline typename cv::DataType<T>::channel_type get_pixel(cv::Mat_<T> const &src, int row, int col, int channel)
{
    return get_pixel_impl(src, row, col, channel, int2Type<cv::DataType<T>::channels>());
}

template<typename T>
inline typename cv::DataType<T>::channel_type* get_pointer(cv::Mat_<T> &src, int row, int col)
{
    return get_pointer_impl(src, row, col, int2Type<cv::DataType<T>::channels>());
}

template<typename T>
inline typename cv::DataType<T>::channel_type const* get_pointer(cv::Mat_<T> const &src, int row, int col)
{
    return get_pointer_impl(src, row, col, int2Type<cv::DataType<T>::channels>());
}

template<typename T>
inline T* get_pointer(cv::Mat &src, int row)
{
    return src.ptr<T>(row);
}

template<typename T>
inline T* get_pointer(cv::Mat &src, int row, int col)
{
    return get_pointer<T>(src, row) + col * src.channels();
}

template<typename T>
inline T* get_pointer(cv::Mat &src, int row, int col, int channel)
{
    return get_pointer<T>(src, row, col) + channel;
}

template<typename T>
inline T const* get_pointer(cv::Mat const &src, int row)
{
    return src.ptr<T>(row);
}

template<typename T>
inline T const* get_pointer(cv::Mat const &src, int row, int col)
{
    return get_pointer<T>(src, row) + col * src.channels();
}

template<typename T>
inline T const* get_pointer(cv::Mat const &src, int row, int col, int channel)
{
    return get_pointer<T>(src, row, col) + channel;
}

}

#endif // GETDATAHELPER_HPP
