#ifndef REMOVECONTOURS_HPP
#define REMOVECONTOURS_HPP

#include <opencv2/core/core.hpp>

#include <vector>

namespace OCV {

void remove_contours(std::vector<std::vector<cv::Point> > &contours, double cmin, double cmax);

}

#endif // REMOVECONTOURS_HPP
