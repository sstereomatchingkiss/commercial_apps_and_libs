#include "drawRect.hpp"

namespace OCV
{

void draw_rect(cv::Mat &inout, cv::Rect const &rect, cv::Scalar const &color)
{
    cv::Point2f points[4];
    get_rect_points(rect, points);
    cv::line(inout, {points[0].x, points[0].y}, {points[1].x, points[1].y}, color, 2);
    cv::line(inout, {points[1].x, points[1].y}, {points[2].x, points[2].y}, color, 2);
    cv::line(inout, {points[2].x, points[2].y}, {points[3].x, points[3].y}, color, 2);
    cv::line(inout, {points[3].x, points[3].y}, {points[0].x, points[0].y}, color, 2);
}

void draw_rects(cv::Mat &inout, std::vector<cv::Rect> const &rects, cv::Scalar const &color)
{
    for(auto const &rect : rects){
        draw_rect(inout, rect, color);
    }
}

}
