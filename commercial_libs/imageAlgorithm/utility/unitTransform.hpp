#ifndef UNITTRANSFORM_HPP
#define UNITTRANSFORM_HPP

namespace OCV{

/*
 * unit transformation between centemeters, inches, percent, pixel,
 */

constexpr double pixel_to_inches(double data, double dpi);
constexpr double percent_to_centimeters(double data, double pixel, double dpi);
constexpr double percent_to_inches(double data, double pixel, double dpi);
constexpr double percent_to_pixel(double data, double pixel);

/*
 *@param data : centimeters want to transform to inches
 */
constexpr double centimeters_to_inches(double data)
{
    return data * 0.3937008;
}

/*
 *@param data : centimeters want to transform to percent
 *@param pixel : pixel number of original image
 *
 *@return
 * return positive number if everything are normal, else return negative number
 */
constexpr double centimeters_to_percent(double data, double pixel, double dpi)
{
    return pixel_to_inches(pixel, dpi) != 0 ?
           centimeters_to_inches(data) / pixel_to_inches(pixel, dpi) * 100 : -1;
}

/*
 *@param data : centimeters want to transform to pixel
 *
 */
constexpr double centimeters_to_pixel(double data, double dpi)
{
    return centimeters_to_inches(data) * dpi;
}

/*
 *@param data : inches want to transform to centimeters
 */
constexpr double inches_to_centimeters(double data)
{
    return data * 2.54;
}

/*
 *@param data : inches want to transform to pixel
 */
constexpr double inches_to_pixel(double data, double dpi)
{
    return data * dpi;
}

/*
 *@param data : inches want to transform to percent
 *@param pixel : pixel number of original image
 *
 *@return
 * return positive number if everything are normal, else return negative number
 */
constexpr double inches_to_percent(double data, double pixel, double dpi)
{
    return pixel_to_inches(pixel, dpi) != 0 ? data / pixel_to_inches(pixel, dpi) * 100 : -1;
}

/*
 *@param data : percent want to transform to centimeters
 *@param pixel : pixel number of original image
 *
 *@return
 * return positive number if everything are normal, else return negative number
 */
constexpr double percent_to_centimeters(double data, double pixel, double dpi)
{
    return percent_to_inches(data, pixel, dpi) * 2.54;
}

/*
 *@param data : percent want to transform to inches
 *@param pixel : pixel number of original image
 *
 *@return
 * return positive number if everything are normal, else return negative number
 */
constexpr double percent_to_inches(double data, double pixel, double dpi)
{
    return percent_to_pixel(data, pixel) / dpi;
}

/*
 *@param data : percent want to transform to pixel
 *@param pixel : pixel number of original image
 *
 *@return
 * return positive number if everything are normal, else return negative number
 */
constexpr double percent_to_pixel(double data, double pixel)
{
    return data * pixel / 100;
}

/*
 *@param data : pixel want to transform to centimeters
 *
 *@return
 * return positive number if everything are normal, else return negative number
 */
constexpr double pixel_to_centimeters(double data, double dpi)
{
    return pixel_to_inches(data, dpi) * 2.54;
}

/*
 *@param data  : pixel want to transform to inches
 *@param pixel : pixel number of original image
 *
 *@return
 * return positive number if everything are normal, else return negative number
 */
constexpr double pixel_to_inches(double data, double dpi)
{
    return dpi != 0 ? data / dpi : -1;
}

/*
 *@param data  : pixel want to transform to percent
 *
 *@return
 * return positive number if everything are normal, else return negative number
 */
constexpr double pixel_to_percent(double data, double pixel)
{
    return pixel != 0 ? data / pixel * 100 : -1;
}

}

#endif // UNITTRANSFORM_HPP
