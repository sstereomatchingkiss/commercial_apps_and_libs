#include <QString>

#include <QFont>
#include <QImage>
#include <QPainter>

#include "waterMark.hpp"

namespace OCV{

void add_water_mark(QImage &image, QString const &text)
{
    QPainter painter(&image);

    painter.setFont(QFont("Times"));
    painter.setRenderHint(QPainter::Antialiasing, true);
    painter.setPen(QPen(QColor("crimson"), 30, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
    painter.drawText(image.width() / 2, image.height() / 2, text);
}

}
