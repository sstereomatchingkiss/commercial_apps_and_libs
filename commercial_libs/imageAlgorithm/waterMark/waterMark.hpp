#ifndef WATERMARK_HPP
#define WATERMARK_HPP

class QImage;
class QString;

namespace OCV{

void add_water_mark(QImage &image, QString const &text);

}

#endif // WATERMARK_HPP
