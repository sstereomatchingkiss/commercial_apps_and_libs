#include "basicImageAlgo.hpp"

namespace{

/**
 *@brief add specific value to specific channel with the help of
 * cv::saturate_cast<T>
 *
 *@param src     : input image
 *@param weight  : weight add to src
 *@param channel : channel to be added
 *@param TrueType : the channel type of the src
 *@param CalcType : The type use to calculate when adding
 */
template<typename TrueType, typename CalcType>
inline void add_channel_impl(cv::Mat &src, int weight, int channel)
{
    OCV::transform_channel<TrueType>(src, channel, [=](CalcType data){
                         return cv::saturate_cast<TrueType>(data + weight);});
}

}

namespace OCV{

void add_channel(cv::Mat &src, int weight, int channel)
{    
    switch(src.type()){
    case CV_8U:
    case CV_8UC2:
    case CV_8UC3:
    case CV_8UC4:{
        add_channel_impl<uchar, int>(src, weight, channel);
        break;
    }
    case CV_16U:
    case CV_16UC2:
    case CV_16UC3:
    case CV_16UC4:{
        add_channel_impl<ushort, ushort>(src, weight, channel);
        break;
    }
    case CV_32F:
    case CV_32FC2:
    case CV_32FC3:
    case CV_32FC4:{
        add_channel_impl<float, float>(src, weight, channel);
        break;
    }
    default:
        break;
    }
}

int float_channel_to_uchar(int type)
{
    switch(type)
    {
    case CV_32F :
    case CV_64F :
        return CV_8U;
    case CV_32FC2 :
    case CV_64FC2 :
        return CV_8UC2;
    case CV_32FC3 :
    case CV_64FC3 :
        return CV_8UC3;
    case CV_32FC4 :
    case CV_64FC4 :
        return CV_8UC4;
    default:
        return -1;
    }
}

/**
 *@brief transform the 8u src to 32F src
 *
 *@example
 * src *= alpha;
 * src.convertTo(src, float_channel);
 *
 *@param :
 * alpha : optional scale factor
 */
int mat8u_to_mat32f(cv::Mat &src, double alpha)
{
    int const type  = OCV::uchar_channel_to_float(src.type());
    if(type != - 1){      
      src.convertTo(src, type, alpha);
    }

    return type;
}

int mat8u_to_mat32f(cv::Mat const &input, cv::Mat &output, double alpha)
{
    if(output.data != input.data){
      output = input.clone();
    }

    return mat8u_to_mat32f(output, alpha);
}

/**
 *@brief transform the 32F src to 8u src
 *
 *@example src *= alpha;
 * src.convertTo(src, float_channel);
 *
 *@param :
 * alpha : optional scale factor
 */
int mat32f_to_mat8u(cv::Mat &src, double alpha)
{
    int const type  = float_channel_to_uchar(src.type());
    if(type != - 1){      
      src *= alpha;
      src.convertTo(src, type);      
    }

    return type;
}

int mat32f_to_mat8u(cv::Mat const &input, cv::Mat &output, double alpha)
{
    if(output.data != input.data){
      output = input.clone();
    }

    return mat32f_to_mat8u(output, alpha);
}

/**
 * @brief return the associative channel type of float
 * ex : type is CV_8U, return CV_32F; type is CV_8UC3, return CV_32FC3
 *      and so on
 *
 *@param :
 *  type : the type of the channel, should be uchar, ex :
 *         CV_8U, CV_8UC3 and so on
 *
 *@return :
 *  return the type number of the uchar channel type(CV_8U~CV_8UC4) if
 *  the type is valid;else return -1
 */
int uchar_channel_to_float(int type)
{
    switch(type)
    {
    case CV_8U  :
    case CV_16U :    
        return CV_32F;
    case CV_8UC2  :
    case CV_16UC2 :   
        return CV_32FC2;
    case CV_8UC3  :
    case CV_16UC3 :   
        return CV_32FC3;
    case CV_8UC4  :
    case CV_16UC4 :    
        return CV_32FC4;
    default:
        return -1;
    }
}

bool is_float_channel(int type)
{
    if(type == CV_32F || type == CV_32FC2 || type == CV_32FC3 || type == CV_32FC4){
        return true;
    }else{
        return false;
    }
}

bool is_uchar_channel(int type)
{
    if(type == CV_8U || type == CV_8UC2 || type == CV_8UC3 || type == CV_8UC4){
        return true;
    }else{
        return false;
    }
}

}
