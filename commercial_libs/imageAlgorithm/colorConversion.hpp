#ifndef COLORCONVERSION_HPP
#define COLORCONVERSION_HPP

#include <cmath>
#include <type_traits>

#include <opencv2/core/core.hpp>

#include "basicImageAlgo.hpp"
#include "imageAlgorithm.hpp"

namespace{

/*
 *Original sigmoidal equation : 1.0/(1.0 + exp(contrast * threshold - signal))
 *With some conversion, we find out that
 *1/(1+exp(-t)) = (1+tanh(t/2))/2
 *So we get a sigmoidal equation express by tanh
 */
template<typename T>
inline T sigmoidal(T contrast, T threshold, T signal)
{
    return (1 + std::tanh(0.5 * contrast * (signal - threshold))) / 2;
}

}

namespace OCV
{

/*
 * design some functor for functions cvt_color
 */

/*
 * functor cope up with convert_sbgr_and_bgr_impl,
 * transform bgr to sbgr.
 *
 * formula :
 * BGR2SRGB
 *   signal <= 0.04045 -> power ~ signal / 12.92
 *   signal >  0.04045 -> power ~ ( (signal + 0.055) / (1 + 0.055) ) ^ 2.4
 */
class bgr2sbgr
{
public:
    float operator()(float signal) const
    {
        return signal <= 0.04045 ? signal / 12.92 :
                                   std::pow((signal + 0.055) / (1.055),  2.4);
    }
};


/*
 *bgr to sigmoidal conversion with threshold == 50%, contrast == 10
 *
 *formula :
 *( sigmoidal(contrast,threshold,signal) - sigmoidal(a,threshold,0) ) /
 *( sigmoidal(contrast,threshold,1) - sigmoidal(a,threshold,0) )
 *
 */
class bgr2sgm
{
public:
    bgr2sgm(float contrast, float threshold) : contrast_(contrast),
        temp_1(sigmoidal<float>(contrast, threshold, 0)),
        temp_2(sigmoidal<float>(contrast, threshold, 1) - temp_1),
        threshold_(threshold){}

    float operator()(float signal) const
    {
        return (sigmoidal<float>(contrast_, threshold_, signal) - temp_1) / temp_2;
    }

private:
    float const contrast_;

    float const temp_1;
    float const temp_2;
    float const threshold_;
};

/*
 * functor cope up with convert_sbgr_and_bgr_impl,
 * transform sbgr to bgr
 *
 *formula :
 * SRGB2BGR
 * signal <= 0.0031308 -> power ~ signal * 12.92
 * signal >  0.0031308 -> power ~ (1 + 0.055) * (signal ^ (1 / 2.4)) - (0.055 ^ 2.4)
 */
class sbgr2bgr
{
public:
    float operator()(float signal) const
    {
        return signal <= 0.0031308 ? signal * 12.92 :
                                     (1.055) * std::pow(signal, 1 / 2.4) - 9.4814e-4;
    }
};

/*
 * inverse from sgm to bgr
 * The input range of atanh is -1 < signal < 1, since the
 * "argument" of this functor may exceed the range, we have
 * to clamped it.The data type should be double, the precision
 * of float is not good enough for this task.
 *
 *formula:
 * BGR2SGM
 * sig0 = sigmoidal<double>(contrast, threshold, 0);
 * sig1 = sigmoidal<double>(contrast, threshold, 1);
 * threshold + (2.0 / contrat) * atanh(((sig1 - sig0)*signal + sig1)*2 -1)
 */
class sgm2bgr
{
public:
    sgm2bgr(double contrast, double threshold) : contrast_(contrast),
        temp_1(sigmoidal<double>(contrast, threshold, 0)),
        temp_2(sigmoidal<double>(contrast, threshold, 1) - temp_1),
        threshold_(threshold){}

    float operator()(double signal) const
    {
        double const epsilon = 1.0e-15;
        double const argument = (temp_2 * signal + temp_1) * 2 - 1;
        double const clamped  = argument < -1 + epsilon ? -1 + epsilon :
        ( argument > 1 - epsilon ? 1 - epsilon : argument);

        return threshold_ + (2.0 / contrast_) * std::atanh(clamped);
    }

private:
    float const contrast_;

    double const temp_1;
    double const temp_2;
    double const threshold_;
};

/*
 * Function design for cvt_color, reduce some duplicate codes,
 * this function will convert the color between bgr--sbgr, bgr--sgm.
 * The channel type of the Mat should be CV_32F
 *
 *@param src : input data, the range should within 0~255 if the color space is bgr;
 *       else the range should within 0~1.
 *       The output range of the source should become 0~1
 *@param flag : the color space want to transform
 *
 *@return
 * false if colorFlag are not related to colorFlag; else return true
 */
template<typename T>
bool cvt_color_impl(cv::Mat &src, OCV::colorFlag flag)
{        
    static_assert(std::is_same<T, float>::value, "Channel should be float(CV_32F)");

    OCV::mat8u_to_mat32f(src, 1 / 255.0);

    bool flag_exist = true;
    switch(static_cast<int>(flag)){
    case static_cast<int>(OCV::colorFlag::BGR2SBGR) :{
        OCV::transform_channels<T>(src, bgr2sbgr());
        break;
    }
    case static_cast<int>(OCV::colorFlag::SBGR2BGR) :{
        OCV::transform_channels<T>(src, sbgr2bgr());
        break;
    }
    case static_cast<int>(OCV::colorFlag::BGR2SGM) :{
        OCV::transform_channels<T>(src, bgr2sgm(6.5, 0.5));
        break;
    }
    case static_cast<int>(OCV::colorFlag::SGM2BGR) :{
        OCV::transform_channels<T>(src, sgm2bgr(6.5, 0.5));
        break;
    }
    default:{
        flag_exist = false;
        break;
    }
    }

    return flag_exist;
}

}

#endif // COLORCONVERSION_HPP
