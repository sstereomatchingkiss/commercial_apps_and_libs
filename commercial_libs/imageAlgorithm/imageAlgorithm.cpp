#ifdef DEBUG_OK
#include <iostream>
#endif

#include <vector>

#include <opencv2/highgui/highgui.hpp>
#include <opencv2/photo/photo.hpp>

#include "basicImageAlgo.hpp"
#include "colorConversion.hpp"
#include "imageAlgorithm.hpp"

namespace{

template<typename T>
inline void gamma_correction_lazy_impl(cv::Mat &inout, int channel, float pow)
{
    OCV::mat8u_to_mat32f(inout, inout, 1/ 255.0);
    OCV::transform_channel<T>(inout, channel, [=](T data){
        return std::pow(data, pow);
    });
    OCV::mat32f_to_mat8u(inout, inout, 255);
}

/**
 *@brief implementatino of staircase_percent.
 *
 *@param
 * cfunc : compare the original is greater or less than target
 * rfunc : deduce the results of every steps of the staircase
 */
template<typename CompareFunc, typename RateChangeFunctor>
std::vector<int> staircase_percent_impl(double original, double target,
                                        double percent, CompareFunc cfunc,
                                        RateChangeFunctor rfunc)
{
    double result = original;
    int const target_temp = static_cast<int>(target);
    std::vector<int> results;
    while(result != target){
        result = rfunc(result, percent);
        if(cfunc(result, target_temp)){
            break;
        }
        results.push_back(result);
    }

    results.push_back(target);

    return results;
}

}

namespace OCV
{

clahe::clahe() : clahe_(cv::createCLAHE())
{

}

/**
 *@brief apply CLAHE of src, Support CV_8U, CV_8UC3, CV_8UC4
 *
 *@param src : input image, the color space should be BGR or gray; src will not
 *be altered or reallocate memory if src and dst do not share the same resource.
 *Expected color space of src is BGR, BGRA or a gray image
 *@param dst : output
 *@param clip_limit : the peak want to clip
 *@param grid_size : size of the grid
 *@param cl_space : color space of histogram equalization will apply on,
 * YCrCb is the default color space.By now only support BGR and YCrCb
 *
 *@return : return true if equalize success, else return false
 */
void clahe::run(cv::Mat &src, cv::Mat &dst, double clip_limit, cv::Size const &grid_size, OCV::colorSpace cl_space)
{
    temp_src_ = src;
    switch (temp_src_.channels()) {
    case 4:{
        temp_src_ = src.clone();
        cv::cvtColor(temp_src_, temp_src_, CV_BGRA2BGR);
        OCV::copy_if_not_same(temp_src_, dst);
    } //don't need to break, still need to process the image
    case 3:{
        switch(cl_space){
        case OCV::colorSpace::BGR :{
            cv::split(temp_src_, buffer_);
            clahe_->setClipLimit(clip_limit);
            clahe_->setTilesGridSize(grid_size);
            for(int i = 0; i != 3; ++i){
                clahe_->apply(buffer_[i], buffer_[i]);
            }
            cv::merge(buffer_, dst);
            break;
        }
        case OCV::colorSpace::YCrCb :
        default:{
            OCV::copy_if_not_same(temp_src_, dst);
            cv::cvtColor(dst, dst, CV_BGR2YCrCb);
            helper(dst, clip_limit, grid_size, 0);
            cv::cvtColor(dst, dst, CV_YCrCb2BGR);
            break;
        }
        }
        break;
    }
    case 1:{
        OCV::copy_if_not_same(temp_src_, dst);
        clahe_->setClipLimit(clip_limit);
        clahe_->setTilesGridSize(grid_size);
        clahe_->apply(dst, dst);
        break;
    }
    default:
        break;
    }
}

void clahe::helper(cv::Mat &inout, double clip_limit, cv::Size const &grid_size, int channel)
{
    cv::split(inout, buffer_);
    clahe_->setClipLimit(clip_limit);
    clahe_->setTilesGridSize(grid_size);
    clahe_->apply(buffer_[channel], buffer_[channel]);
    cv::merge(buffer_, inout);
}

/**
 * @brief convert the input image from gray or bgra to bgr if the channels() == 1, 4
 * @param input : input image, the colorSpace should be bgra if channels() == 4;gray if
 * channels() == 1
 */
void gray_bgra_2_bgr(cv::Mat &input)
{
    if(input.channels() == 4){
        cv::cvtColor(input, input, CV_BGRA2BGR);
    }else if(input.channels() == 1){
        cv::cvtColor(input, input, CV_GRAY2BGR);
    }
}

/**
 * @brief convert the input image from bgra to bgr if the channels() == 4
 * @param input : input image, the colorSpace should be bgra if channels() == 4
 *
 * @return the image after transform if channels() == 4, else return the original image
 */
cv::Mat gray_bgra_2_bgr(cv::Mat const &input)
{
    cv::Mat result = input.clone();

    gray_bgra_2_bgr(result);

    return result;
}

/**
 *@brief convert the image to gray image
 *  construct and return the result directly
 *  determine appropriate color space conversion code at runtime
 *
 *@param src :input image
 *@param dst : output
 *@param copy : true : copy the data of src to dst if the channel of src is CV_8U or CV_8S;
 *false : src and dst share the same data if the channel of src is CV_8U or CV_8S
 *@param flag : CV_BGR2GRAY or CV_RGB2GRAY and so on
 *@param dstCn : number of channels in the destination image; if the parameter is 0,
 *  the number of the channels is derived automatically.
 *
 *@return
 * will return an empty Mat if cannot or don't need to convert to gray scale
 */
void cvt_to_gray(cv::Mat &src, cv::Mat &dst, bool copy, int dstCn, int flag)
{        
    if(src.type() == CV_8UC3 || src.type() == CV_8SC3){
        cv::cvtColor(src, dst, flag, dstCn);
    }else if(src.type() == CV_8UC4 || src.type() == CV_8SC4){
        cv::cvtColor(src, dst, flag, dstCn);
    }else if(src.type() == CV_8U || src.type() == CV_8S){
        if(copy){
            src.copyTo(dst);
        }else{
            dst = src;
        }
    }
}

/**
 * @brief implement some color transform algorithms which do not
 * implemented by openCV2 yet.Support conversion between
 * sBGR, BGR, gray to sRGB, sRGB to gray
 *
 *@param src : input data, the range should within 0~255 if the color space is bgr;
 *       else the range should within 0~1. The output range of the source should
 *       become 0~1
 *
 *@param flags : BGR2SBGR, SBGR2BGR support CV_8U, CV_8UC3, CV_8UC4,
 *         CV_32F, CV_32FC3, CV_32FC4.Whether it is gray image
 *         or color image, BGR2SBGR and SBGR2BGR would apply
 *         pixel transformation on each channels by formula
 *
 *@return :
 * return the result within range 0~1.
 */
void cvt_color(cv::Mat &src, cv::Mat &dst, colorFlag flags)
{
    OCV::copy_if_not_same(src, dst);
    switch(dst.type())
    {
    case CV_8U    :
    case CV_8UC3  :
    case CV_8UC4  :
    case CV_32F   :
    case CV_32FC2 :
    case CV_32FC3 :{
        cvt_color_impl<float>(dst, flags);
        break;
    }
    default :
        break;
    }
}

/**
 *@brief Apply gamma correction on src, if src is a single channel Mat,
 * this function would apply cv::pow on it;Else if src is a 3 or 4
 * channels, this function would transform the src to another color
 * space(CIELAB), apply gamma correction on the L* channel. Support
 * CV_8U, CV_8UC3, CV_8UC4, CV_32F, CV32FC3, CV32FC4
 *
 * Cautions !!! Only test the cases of CV_8UC3, CV_8UC4
 *
 *@param src : the source you want to apply gamma correction, the color space
 *       should be BGR or BGRA; src would not be altered or reallocate memory if
 *       src and dst do not share the resource
 *@param dst : result after gamma correction
 *@param pow : power of values
 *@param cl_space : support BGR, HSL, Lab, YCrCb, YUV
 */
void gamma_correction_lazy(cv::Mat &src, cv::Mat &dst, float pow, OCV::colorSpace cl_space)
{
    cv::Mat temp_src =src;
    switch(src.type())
    {
    case CV_8UC4  :
    case CV_32FC4 : {
        //std::cout<<"gamma 4 ch"<<std::endl;
        temp_src = src.clone();
        cv::cvtColor(temp_src, temp_src, CV_BGRA2BGR);
    } //don't need to break, still need to process the image
    case CV_8UC3   :
    case CV_32FC3  : {
        switch(cl_space){
        case OCV::colorSpace::Lab:{
            //std::cout<<"gamma 3 ch, lab"<<std::endl;
            OCV::copy_if_not_same(temp_src, dst);
            cv::cvtColor(temp_src, dst, CV_BGR2Lab);
            gamma_correction_lazy_impl<float>(dst, 0, pow);
            cv::cvtColor(dst, dst, CV_Lab2BGR);
            break;
        }
        case OCV::colorSpace::HLS: {
            //std::cout<<"gamma 3 ch, hls"<<std::endl;
            OCV::copy_if_not_same(temp_src, dst);
            cv::cvtColor(temp_src, dst, CV_BGR2HLS);
            gamma_correction_lazy_impl<float>(dst, 1, pow);
            cv::cvtColor(dst, dst, CV_HLS2BGR);
            break;
        }
        case OCV::colorSpace::YUV: {
            //std::cout<<"gamma 3 ch, yuv"<<std::endl;
            OCV::copy_if_not_same(temp_src, dst);
            cv::cvtColor(temp_src, dst, CV_BGR2YUV);
            gamma_correction_lazy_impl<float>(dst, 0, pow);
            cv::cvtColor(dst, dst, CV_YUV2BGR);
            break;
        }
        case OCV::colorSpace::BGR: {
            //std::cout<<"gamma 3 ch, bgr"<<std::endl;
            OCV::copy_if_not_same(temp_src, dst);
            OCV::mat8u_to_mat32f(dst, dst, 1/ 255.0);
            cv::pow(dst, pow, dst);
            OCV::mat32f_to_mat8u(dst, dst, 255);
            break;
        }
        case OCV::colorSpace::YCrCb: {
            //std::cout<<"gamma 3 ch, ycrcb"<<std::endl;
            OCV::copy_if_not_same(temp_src, dst);
            cv::cvtColor(temp_src, dst, CV_BGR2YCrCb);
            gamma_correction_lazy_impl<float>(dst, 0, pow);
            cv::cvtColor(dst, dst, CV_YCrCb2BGR);
            break;
        }
        default:{
            OCV::copy_if_not_same(temp_src, dst);
            cv::cvtColor(temp_src, dst, CV_BGR2YCrCb);
            gamma_correction_lazy_impl<float>(dst, 0, pow);
            cv::cvtColor(dst, dst, CV_YCrCb2BGR);
            break;
        }
        }
        break;
    }
    case CV_8U  :
    case CV_32F :{
        //std::cout<<"gamma 1 ch"<<std::endl;
        OCV::copy_if_not_same(temp_src, dst);
        gamma_correction_lazy_impl<float>(dst, 0, pow);
        break;
    }

    default:
        return;
    }
}

/**
 *@brief rotate image by factor of 90 degrees
 *
 *@param source : input image
 *@param dst : output image
 *@param angle : factor of 90, even it is not factor of 90, the angle
 * will be mapped to the range of [-360, 360].
 * {angle = 90n; n = {-4, -3, -2, -1, 0, 1, 2, 3, 4} }
 * if angle bigger than 360 or smaller than -360, the angle will
 * be map to -360 ~ 360.
 * mapping rule is : angle = ((angle / 90) % 4) * 90;
 *
 * ex : 89 will map to 0, 98 to 90, 179 to 90, 270 to 3, 360 to 0.
 *
 */
void rotate_image_90n(cv::Mat &src, cv::Mat &dst, int angle)
{   
    angle = ((angle / 90) % 4) * 90;

    //0 : flip vertical; 1 flip horizontal
    bool const flip_horizontal_or_vertical = angle > 0 ? 1 : 0;
    int const number = std::abs(angle / 90);

    if(src.data != dst.data){
        src.copyTo(dst);
    }

    for(int i = 0; i != number; ++i){
        cv::transpose(dst, dst);
        cv::flip(dst, dst, flip_horizontal_or_vertical);
    }
}

/**
 *@brief rotate the image which take the image center(src.cols / 2, src.rows / 2) as center
 * This function will create a new image and return it
 */
void rotate_image(cv::Mat &src, cv::Mat &dst, double angle)
{
    cv::Point2f src_center(src.cols / 2.0F, src.rows / 2.0F);
    cv::Mat rot_mat = cv::getRotationMatrix2D(src_center, angle, 1.0);
    cv::warpAffine(src, dst, rot_mat, dst.size());
}

/**
 *@brief make sure every step to resize the original to target will s<= percent.
 * The values of output will be truncated to integral(floor)
 *
 *ex :
 *original = 640
 *target = 1280
 *percent = 10(means 10%)
 *
 *the results will be
 *704, 774, 851, 936, 1029, 1131, 1280
 */
std::vector<int> staircase_percent(double original, double target, double percent)
{
    if(percent == 0){
        return std::vector<int>(1, target);
    }

    if(original > target){
        return staircase_percent_impl(original, target, percent, std::less<int>(),
                                      [](int result, int percents){ return result - result * percents / 100; });
    }else{
        return staircase_percent_impl(original, target, percent, std::greater<int>(),
                                      [](int result, int percents){ return result + result * percents / 100;});
    }
}

/**
 *@brief overload version staircase_percent, since this function is not a criticial path of the program
 * I implement it with a slower but more readable solution
 */
std::vector<cv::Size> staircase_percent(cv::Size_<double> const &original, cv::Size_<double> const &target, double percent)
{
    if(percent == 0){
        return std::vector<cv::Size>(1, target);
    }

    auto widths  = staircase_percent(original.width, target.width, percent);
    auto heights = staircase_percent(original.height, target.height, percent);
    size_t const biggest = widths.size() > heights.size() ? widths.size() : heights.size();
    heights.resize(biggest, heights.back());
    widths.resize(biggest, widths.back());

    std::vector<cv::Size> results(widths.size());
    for(size_t i = 0; i != biggest; ++i){
        results[i] = cv::Size(widths[i], heights[i]);
    }

    return results;
}

/**
 *@brief find out the size which staircase interpolation need to resize the image
 * with each step.
 *
 *@param :
 * original : original size of the image
 * target : final size of the image
 * step : steps of staircase interpolation
 *
 *@return :
 * the size of every step
 *
 *@example :
 * original = 640, target = 1024, step = 10
 * result = int(640*1.048), int(640*(1.048^2)), ..., int(640*(1.048^10))
 */
std::vector<int> staircase_steps(double original, double target, int step)
{
    auto const factor = std::pow(target / original, 1 / static_cast<double>(step));
    auto step_factor = factor;
    std::vector<int> result(step);
    for(int i = 0; i != step - 1; ++i){
        result[i] = step_factor * original;
        step_factor *= factor;
    }
    result[step - 1] = target;

    return result;
}

/**
 *@brief overlaod version of staircase_steps
 */
std::vector<cv::Size> staircase_steps(cv::Size_<double> const &original, cv::Size_<double> const &target, int step)
{
    auto const factor_heigth = std::pow(target.height / original.height, 1 / static_cast<double>(step));
    auto const factor_width  = std::pow(target.width / original.width, 1 / static_cast<double>(step));
    auto step_factor_height  = factor_heigth;
    auto step_factor_width   = factor_width;
    std::vector<cv::Size> result(step);
    for(int i = 0; i != step - 1; ++i){
        result[i].width     = step_factor_width * original.width;
        result[i].height    = step_factor_height * original.height;
        step_factor_width  *= factor_width;
        step_factor_height *= factor_heigth;
    }
    result[step - 1] = target;

    return result;
}

}
