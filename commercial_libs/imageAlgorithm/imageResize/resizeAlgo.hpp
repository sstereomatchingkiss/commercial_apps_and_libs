#ifndef RESIZEALGO_HPP
#define RESIZEALGO_HPP

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "bicubic/bicubicFunctor.hpp"
#include "bicubic/bicubicInterpolation.hpp"

namespace OCV{

/*
 * bicubic interpolation, support types
 * CV_8U, CV_8UC3, CV_8UC4, CV_32S, CV_32SC3, CV_32SC3,
 * CV_32F, CV_32FC3, CV_32FC4
 */
template<typename Interpolation>
cv::Mat const bicubic_interpolation(cv::Mat const &src, cv::Size dsize,
                                    float dx = 0, float dy = 0,
                                    Interpolation interpolation = bellFilter())
{    
    switch(src.type())
    {
    case CV_8U:
    case CV_8UC3:
    case CV_8UC4:{
        return bicubic_interpolation_impl<unsigned char>(src, dsize, dx, dy, interpolation);
    }
    case CV_32S:
    case CV_32SC3:
    case CV_32SC4:{
        return bicubic_interpolation_impl<cv::DataType<cv::Vec2i>::channel_type>(src, dsize, dx, dy, interpolation);
    }
    case CV_32F:
    case CV_32FC3:
    case CV_32FC4:{
        return bicubic_interpolation_impl<float>(src, dsize, dx, dy, interpolation);
    }

    default:
        return src;
    }
}

cv::Mat const hqx_algo(cv::Mat const &src, int scale);

}

#endif // RESIZEALGO_HPP
