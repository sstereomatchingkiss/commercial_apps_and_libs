#ifndef BICUBICINTERPOLATION_HPP
#define BICUBICINTERPOLATION_HPP

#include <cmath>

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "bicubicFunctor.hpp"
#include "utility/getDataHelper.hpp"
#include "metaProgrammingHelper.hpp"

namespace{

/*
 *@brief generate offset for bicubic_interpolation
 *
 *@param current_position : position of pixel
 *@param boundary : boundary of x-axis or y-axis
 *@param offset : the results
 */
inline void generate_offset(int current_position, int boundary, int offset[])
{
    offset[0] = current_position != 0 ? -1 : 0;
    offset[1] = 0;
    offset[2] = 1;
    offset[3] = current_position + 2 < boundary ? 2 : 1;
}

/*
 *@brief generate the vector(y axis) of bicubic interpolation
 *
 *@param vector : save the results
 *@param origin_vector : original vector
 */
template<typename Interpolation>
inline void generate_y_vector(float origin_vector, float vector[], Interpolation interpolation)
{
    vector[0] = interpolation(-1.0f - origin_vector);
    vector[1] = interpolation(-origin_vector);
    vector[2] = interpolation(1.0f - origin_vector);
    vector[3] = interpolation(2.0f - origin_vector);
}

/*
 *@brief generate the vector(x axis) of bicubic interpolation
 *
 *@param vector : save the results
 *@param origin_vector : original vector
 */
template<typename Interpolation>
inline void generate_x_vector(float origin_vector, float vector[], Interpolation interpolation)
{
    vector[0] = interpolation(origin_vector + 1.0f);
    vector[1] = interpolation(origin_vector);
    vector[2] = interpolation(origin_vector - 1.0f);
    vector[3] = interpolation(origin_vector - 2.0f);
}

}

/*
 *@brief Resizes an image by bicubic interpolation, same as previous version but
 * could generate less binary codes
 *
 *@param src – Source image.
 *@param dsize – he destination image size. If it is zero, then it is computed as:
 *dsize = Size(round(fx*src.cols), round(fy*src.rows)). Either dsize or both fx or fy must be non-zero.
 *@param dx – The scale factor along the horizontal axis. When 0, it is computed as (double)dsize.width/src.cols
 *@param dy – The scale factor along the vertical axis. When 0, it is computed as (double)dsize.height/src.rows
 *@param interpolation – The interpolation method: mitchellFilter or quadraticFilter
 *
 *@return
 * return cv::Mat after interpolate
 */
//bicubic from code project
template<typename T, typename Interpolation>
cv::Mat const bicubic_interpolation_impl(cv::Mat const &src, cv::Size dsize,
                                             float dx = 0, float dy = 0,
                                             Interpolation interpolation = bellFilter())
{
    if( ((dsize.height == src.rows) && (dsize.width == src.cols)) || ((dx == 1) && (dy == 1)) ||
        ((dsize.area() == 0) && ((dx == 0) || (dy == 0))) ) {
        return src;
    }

    cv::Mat dst = dsize.area() == 0 ? cv::Mat(src.rows * dy, src.cols * dx, src.type()) :
                                          cv::Mat(dsize, src.type());

    float const xratio = static_cast<float>((src.cols - 1)) / dst.cols;
    float const yratio = static_cast<float>((src.rows - 1)) / dst.rows;
    int const channels = src.channels();
    for(int row = 0; row != dst.rows; ++row)
    {
        float const temp_yratio = yratio * row;
        int y = static_cast<int>(temp_yratio);
        float const y_diff = (temp_yratio) - y; //distance of the nearest pixel(y axis)
        int y_offset[4]; generate_offset(y, src.rows, y_offset);
        float y_vector[4]; generate_y_vector(y_diff, y_vector, interpolation);
        auto dst_ptr = OCV::get_pointer<T>(dst, row);
        for(int col = 0; col != dst.cols; ++col)
        {
            float const temp_xratio = xratio * col;
            int x = static_cast<int>(temp_xratio);
            int x_offset[4]; generate_offset(x, src.cols, x_offset);
            float const x_diff = (temp_xratio) - x; //distance of the nearet pixel(x axis)
            float x_vector[4]; generate_x_vector(x_diff, x_vector, interpolation);
            for(int channel = 0; channel != channels; ++channel)
            {
                float sum = 0;
                float denom = 0;
                for(int m = 0; m != 4; ++m)
                {
                    int const y_index = y + y_offset[m];
                    auto src_ptr = OCV::get_pointer<T>(src, y_index, x);
                    float const mult[4] = {y_vector[m] * x_vector[0], y_vector[m] * x_vector[1],
                                           y_vector[m] * x_vector[2], y_vector[m] * x_vector[3]};
                    sum += *(src_ptr + x_offset[0] * channels + channel) * mult[0] +
                           *(src_ptr + channel) * mult[1] +
                           *(src_ptr + x_offset[2] * channels + channel) * mult[2] +
                           *(src_ptr + x_offset[3] * channels + channel) * mult[3];
                    denom += mult[0] + mult[1] + mult[2] + mult[3];
                }
                *dst_ptr++ = sum / denom;
           }
        }
    }

    return dst;
}

template<typename T, typename Interpolation>
inline cv::Mat_<T> const bicubic_interpolation_impl(cv::Mat_<T> const &src, cv::Size dsize,
                                                    float dx = 0, float dy = 0,
                                                    Interpolation interpolation = bellFilter())
{
    return bicubic_interpolation_impl<T>(src, dsize, dx, dy, interpolation);
}

//bilinear, experiment
template<typename T>
cv::Mat_<T> const bilinear_interpolation(cv::Mat_<T> const &src, cv::Size dsize,
                                         float dx, float dy)
{
    cv::Mat_<T> dst = dsize.area() == 0 ? cv::Mat_<T>(src.rows * dy, src.cols * dx) :
                                          cv::Mat_<T>(dsize);

    float const x_ratio = static_cast<float>((src.cols - 1)) / dst.cols;
    float const y_ratio = static_cast<float>((src.rows - 1)) / dst.rows;
    for(int row = 0; row != dst.rows; ++row)
    {
        int y = static_cast<int>(row * y_ratio);
        float const y_diff = (row * y_ratio) - y; //distance of the nearest pixel(y axis)
        float const y_diff_2 = 1 - y_diff;
        auto *dst_ptr = &dst(row, 0)[0];
        for(int col = 0; col != dst.cols; ++col)
        {
            int x = static_cast<int>(col * x_ratio);
            float const x_diff = (col * x_ratio) - x; //distance of the nearet pixel(x axis)
            float const x_diff_2 = 1 - x_diff;
            float const y2_cross_x2 = y_diff_2 * x_diff_2;
            float const y2_cross_x = y_diff_2 * x_diff;
            float const y_cross_x2 = y_diff * x_diff_2;
            float const y_cross_x = y_diff * x_diff;
            for(int channel = 0; channel != cv::DataType<T>::channels; ++channel)
            {
                *dst_ptr++ = y2_cross_x2 * src(y, x)[channel] +
                             y2_cross_x * src(y, x + 1)[channel] +
                             y_cross_x2 * src(y + 1, x)[channel] +
                             y_cross_x * src(y + 1, x + 1)[channel];

            }
        }
    }

    return dst;
}

#endif // BICUBICINTERPOLATION_HPP
