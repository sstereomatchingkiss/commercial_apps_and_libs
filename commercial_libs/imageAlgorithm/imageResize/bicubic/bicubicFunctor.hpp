#ifndef BICUBICFUNCTOR_HPP
#define BICUBICFUNCTOR_HPP

#include <cmath>

class bellFilter
{
public:
    float operator()(float x)
    {
        //float const f = x;
        float f = ( x / 2.0 ) * 1.5; // Converting -2 to +2 to -1.5 to +1.5
        if( f > -1.5 && f < -0.5 )
        {
            return( 0.5 * pow(f + 1.5, 2.0));
        }
        else if( f > -0.5 && f < 0.5 )
        {
            return 3.0 / 4.0 - ( f * f );
        }
        else if( ( f > 0.5 && f < 1.5 ) )
        {
            return( 0.5 * std::pow(f - 1.5, 2.0));
        }
        return 0.0;
    }
};

class boxFilterFilter
{

public:
    float operator()(float f)
    {
        f = std::abs(f / 4);
        if (f <= 0.5) return 1;
        return 0;
    }
};

class bsplineFilter
{
public:
    float operator()(float f)
    {
        f = std::abs(f);

        if( f >= 0.0 && f <= 1.0 )
        {
            return ( 2.0 / 3.0 ) + ( 0.5 ) * ( f * f * f ) - (f * f);
        }
        else if( f > 1.0 && f <= 2.0 )
        {
            return 1.0 / 6.0 * std::pow(2.0 - f, 3);
        }
        return 1.0;
    }
};

class catmullRomFilter
{
public:
    float operator()( float f )
    {
        const float B = 0.0;
        const float C = 0.5;

        f = std::abs(f);
        if( f < 1.0 )
        {
            return ( ( 12 - 9 * B - 6 * C ) * ( f * f * f ) +
                     ( -18 + 12 * B + 6 *C ) * ( f * f ) +
                     ( 6 - 2 * B ) ) / 6.0;
        }
        else if( f >= 1.0 && f < 2.0 )
        {
            return ( ( -B - 6 * C ) * ( f * f * f )
                     + ( 6 * B + 30 * C ) * ( f *f ) +
                     ( - ( 12 * B ) - 48 * C  ) * f +
                     8 * B + 24 * C)/ 6.0;
        }
        else
        {
            return 0.0;
        }
    }
};

class cosineFilter
{
public:
    float operator()(float f)
    {
        f = f / 2.0;
        if ((f >= -1) && (f <= 1)) return ((std::cos(f * 3.14159f) + 1) / 2.0f);
        return 0;
    }
};

class cubicConvolutionFilter
{
public:
    float operator()(double f)
    {
        f = std::abs((f / 2.0) * 3);
        float const temp = f * f;
        if (f <= 1) return ((4.0f/3.0f)*temp*f - (7.0f/3.0f)*temp + 1);
        if (f <= 2) return (- (7.0f/12.0f)*temp*f + 3.0f*temp - (59.0f/12.0f)*f + 2.5f);
        if (f <= 3) return ((1.0f/12.0f)*temp*f - (2.0f/3.0f)*temp + 1.75f*f - 1.5f);
        return 0;
    }
};

class hermiteFilter
{
public:
    float operator()(float f)
    {
        f = std::abs(f / 2.0);
        if (f < 1) return ((2 * f - 3)* f * f + 1);
        return 0;
    }
};

class kernelBSpline
{
public:
    float operator()(float x)
    {
        if (x > 2.0f) return 0.0f;
        // thanks to Kristian Kratzenstein
        float a, b, c, d;
        float xm1 = x - 1.0f; // Was calculatet anyway cause the "if((x-1.0f) < 0)"
        float xp1 = x + 1.0f;
        float xp2 = x + 2.0f;

        if ((xp2) <= 0.0f) a = 0.0f; else a = xp2*xp2*xp2; // Only float, not float -> double -> float
        if ((xp1) <= 0.0f) b = 0.0f; else b = xp1*xp1*xp1;
        if (x <= 0) c = 0.0f; else c = x*x*x;
        if ((xm1) <= 0.0f) d = 0.0f; else d = xm1*xm1*xm1;

        return (0.16666666666666666667f * (a - (4.0f * b) + (6.0f * c) - (4.0f * d)));
    }
};

/*
 * filter introduce by http://www.control.auc.dk/~awkr00/graphics/filtering/filtering.html
 */
class maxRestructionFilter
{
public:
    float operator()(float f)
    {
        float const s = 0.4810;
        float const t = 1.3712;

        f = std::abs(f / 2.0 * 2 * t);
        if( f >= 0 && f <= s)
        {
            return 1 - (f * f) / (s * t);
        }
        else if(f > s && f <= t)
        {
            return std::pow(t - s, 2) / (t * (t - s));
        }

        return 0;
    }
};

class mitchellFilter
{
public:
    float operator()(float f)
    {
        f = std::abs(f);
        float const C = 1/3;
        float temp =  f * f;
        if (f < 1) {
            f = (((12 - 9*C - 6*C)*(f*temp)) + ((- 18 + 12*C + 6*C)*temp) + (6 - 2*C));
            return (f/6);
        }
        if (f < 2) {
            f = (((- C - 6*C)*(f*temp)) + ((6*C + 30*C)*temp) + ((- 12*C - 48*C)*f) + (8*C + 24*C));
            return (f / 6.0);
        }
        return 0;
    }
};

class mitchellNetravaliRestructionFilter
{
public:
    float operator()(float f)
    {
        f = std::abs((f / 2.0));
        if(f <= 1){
            float const temp = f * f;
            return 7 * temp * f -12 * temp + (6 - 2/3.0f);
        }
        else if( f > 1 && f < 2)
        {
            float const temp = f * f;
            return (-7 / 3.0f) * temp * f + 12 * temp - 20 * f + 32 / 3.0f;
        }

        return 0;
    }
};

class quadraticFilter
{
public:
    float operator()(float f)
    {
        f = std::abs((f / 2.0) * 1.5);
        if (f <= 0.5) return (-2 * f * f + 1);
        if (f <= 1.5) return (f * f - 2.5 * f + 1.5);
        return 0;
    }
};

class quadraticBSplineFilter
{
public:

    float operator ()(float f)
    {
        f = std::abs((f / 2.0) * 1.5);
        if (f <= 0.5) return (- f*f + 0.75);
        if (f <= 1.5) return (0.5*f*f - 1.5*f + 1.125);
        return 0;
    }
};

class sinxxFilter
{
public:
    float operator()(float f)
    {
        f = std::abs(f);

        if (f < 1.0) {
            float temp = std::pow(f, 2);;
            return 0.5 * temp * f - temp + 2.0 / 3.0;
        }
        else if (f < 2.0) {
            f = 2.0 - f;
            //f *= f * f;
            return std::pow(f, 3) / 6.0;
        }
        else {
            return 0.0;
        }
    }
};

class triangular
{
public:
    float operator()(float f)
    {
        f = std::abs(2.0);
        return 1.0 - f;
    }
};

#endif // BICUBICFUNCTOR_HPP
