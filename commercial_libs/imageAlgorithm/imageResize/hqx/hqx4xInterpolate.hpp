#ifndef HQX4XINTERPOLATE_HPP
#define HQX4XINTERPOLATE_HPP


/*
 * implement functors to interpolate hqx4x
 */

#include <array>

#include <opencv2/core/core.hpp>

#include "utility/dimensionMapper.hpp"
#include "hqxAuxiliary.hpp"
#include "hqxInterpolationBase.hpp"

namespace OCV{

template<typename T>
class hqx4xInterpolate : private indexHelper<T>
{
public:
    using indexHelper<T>::index_;
    typedef typename cv::DataType<T>::channel_type channel_type;

    constexpr hqx4xInterpolate(std::array<channel_type, cv::DataType<T>::channels> const threshold) : indexHelper<T>(), threshold_(threshold) {}

    void interpolate_15(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_23(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_24(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_25(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_27(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_28(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_29(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_30(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_31(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_43(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_45(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_46(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_47(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_49(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_50(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_51(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_53(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_54(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_56(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_57(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_58(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_59(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_60(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_61(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_62(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_63(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_66(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_67(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_69(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_70(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_71(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_75(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_76(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_77(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_78(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_79(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_81(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_83(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_85(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_86(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_87(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_89(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_90(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_91(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_92(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_93(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_94(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_95(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_98(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_99(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_101(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_102(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_103(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_105(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_106(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_107(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_108(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_110(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_111(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_113(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_114(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_115(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_116(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_118(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_119(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_120(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_121(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_122(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_123(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_124(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_125(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_126(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_127(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_135(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_138(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_139(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_140(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_141(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_142(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_147(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_150(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_151(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_152(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_153(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_154(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_155(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_156(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_157(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_158(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_159(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_162(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_163(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_165(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_166(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_167(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_170(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_172(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_173(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_177(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_178(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_180(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_181(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_184(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_185(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_186(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_187(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_188(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_189(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_190(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_191(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_194(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_195(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_197(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_198(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_199(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_201(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_202(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_203(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_204(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_206(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_207(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_209(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_210(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_211(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_212(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_214(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_215(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_216(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_217(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_218(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_219(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_220(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_221(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_222(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_223(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_225(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_226(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_227(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_229(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_230(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_231(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_232(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_233(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_234(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_235(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_238(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_239(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_240(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_242(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_243(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_244(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_246(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_247(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_248(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_249(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_250(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_251(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_252(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_253(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_254(cv::Mat_<T> &dst, channel_type mask[], int row, int col);
    void interpolate_255(cv::Mat_<T> &dst, channel_type mask[], int row, int col);


private:
    std::array<channel_type, cv::DataType<T>::channels>  const threshold_;
};

template<typename T>
void hqx4xInterpolate<T>::interpolate_165(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i]         = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[3] + i]);
        dst(row_index, col_index + 1)[i]     = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[3] + i]);
        dst(row_index, col_index + 2)[i]     = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[5] + i]);
        dst(row_index, col_index + 3)[i]     = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[5] + i]);
        dst(row_index + 1, col_index)[i]     = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_6_to_1_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 2)[i] = mix_6_to_1_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[1] + i]);
        dst(row_index + 2, col_index)[i]     = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_6_to_1_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 2)[i] = mix_6_to_1_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index)[i]     = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[3] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[3] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[5] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[5] + i]);
    }
}

template<typename T>
void hqx4xInterpolate<T>::interpolate_162(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i]         = mix_5_to_3(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index, col_index + 1)[i]     = mix_3_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index, col_index + 2)[i]     = mix_3_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index, col_index + 3)[i]     = mix_5_to_3(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index)[i]     = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[0] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[2] + i]);
        dst(row_index + 2, col_index)[i]     = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_6_to_1_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 2)[i] = mix_6_to_1_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index)[i]     = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[3] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[3] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[5] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[5] + i]);
    }
}

template<typename T>
void hqx4xInterpolate<T>::interpolate_49(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i]         = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[3] + i]);
        dst(row_index, col_index + 1)[i]     = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[3] + i]);
        dst(row_index, col_index + 2)[i]     = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[2] + i]);
        dst(row_index, col_index + 3)[i]     = mix_5_to_3(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index)[i]     = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_6_to_1_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 2, col_index)[i]     = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_6_to_1_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index)[i]     = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[3] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[3] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[8] + i]);
    }
}

template<typename T>
void hqx4xInterpolate<T>::interpolate_69(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i]         = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[3] + i]);
        dst(row_index, col_index + 1)[i]     = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[3] + i]);
        dst(row_index, col_index + 2)[i]     = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[5] + i]);
        dst(row_index, col_index + 3)[i]     = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[5] + i]);
        dst(row_index + 1, col_index)[i]     = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_6_to_1_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 2)[i] = mix_6_to_1_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[1] + i]);
        dst(row_index + 2, col_index)[i]     = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[6] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index)[i]     = mix_5_to_3(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[8] + i]);
    }
}

template<typename T>
void hqx4xInterpolate<T>::interpolate_140(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i]         = mix_5_to_3(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index, col_index + 1)[i]     = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[0] + i]);
        dst(row_index, col_index + 2)[i]     = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[5] + i]);
        dst(row_index, col_index + 3)[i]     = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[5] + i]);
        dst(row_index + 1, col_index)[i]     = mix_3_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index + 1, col_index + 2)[i] = mix_6_to_1_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[1] + i]);
        dst(row_index + 2, col_index)[i]     = mix_3_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 2, col_index + 2)[i] = mix_6_to_1_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index)[i]     = mix_5_to_3(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[6] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[5] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[5] + i]);
    }
}

template<typename T>
void hqx4xInterpolate<T>::interpolate_163(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i]         = mix_5_to_3(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index, col_index + 1)[i]     = mix_7_to_1(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index, col_index + 2)[i]     = mix_3_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index, col_index + 3)[i]     = mix_5_to_3(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index)[i]     = mix_5_to_3(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[2] + i]);
        dst(row_index + 2, col_index)[i]     = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_6_to_1_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 2)[i] = mix_6_to_1_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index)[i]     = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[3] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[3] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[5] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[5] + i]);
    }
}

template<typename T>
void hqx4xInterpolate<T>::interpolate_166(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i]         = mix_5_to_3(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index, col_index + 1)[i]     = mix_3_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index, col_index + 2)[i]     = mix_7_to_1(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index, col_index + 3)[i]     = mix_5_to_3(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 1, col_index)[i]     = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[0] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 2, col_index)[i]     = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_6_to_1_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 2)[i] = mix_6_to_1_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index)[i]     = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[3] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[3] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[5] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[5] + i]);
    }
}

template<typename T>
void hqx4xInterpolate<T>::interpolate_53(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i]         = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[3] + i]);
        dst(row_index, col_index + 1)[i]     = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[3] + i]);
        dst(row_index, col_index + 2)[i]     = mix_5_to_3(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index, col_index + 3)[i]     = mix_5_to_3(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index)[i]     = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_6_to_1_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index + 2, col_index)[i]     = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_6_to_1_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index)[i]     = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[3] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[3] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[8] + i]);
    }
}

template<typename T>
void hqx4xInterpolate<T>::interpolate_177(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i]         = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[3] + i]);
        dst(row_index, col_index + 1)[i]     = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[3] + i]);
        dst(row_index, col_index + 2)[i]     = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[2] + i]);
        dst(row_index, col_index + 3)[i]     = mix_5_to_3(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index)[i]     = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_6_to_1_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 2, col_index)[i]     = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_6_to_1_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index)[i]     = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[3] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[3] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[7] + i]);
    }
}

template<typename T>
void hqx4xInterpolate<T>::interpolate_197(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i]         = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[3] + i]);
        dst(row_index, col_index + 1)[i]     = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[3] + i]);
        dst(row_index, col_index + 2)[i]     = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[5] + i]);
        dst(row_index, col_index + 3)[i]     = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[5] + i]);
        dst(row_index + 1, col_index)[i]     = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_6_to_1_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 2)[i] = mix_6_to_1_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[1] + i]);
        dst(row_index + 2, col_index)[i]     = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[6] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 3, col_index)[i]     = mix_5_to_3(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[5] + i]);
    }
}

template<typename T>
void hqx4xInterpolate<T>::interpolate_101(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i]         = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[3] + i]);
        dst(row_index, col_index + 1)[i]     = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[3] + i]);
        dst(row_index, col_index + 2)[i]     = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[5] + i]);
        dst(row_index, col_index + 3)[i]     = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[5] + i]);
        dst(row_index + 1, col_index)[i]     = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_6_to_1_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 2)[i] = mix_6_to_1_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[1] + i]);
        dst(row_index + 2, col_index)[i]     = mix_5_to_3(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index)[i]     = mix_5_to_3(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[8] + i]);
    }
}

template<typename T>
void hqx4xInterpolate<T>::interpolate_172(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index, col_index + 1)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[0] + i]);
        dst(row_index, col_index + 2)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[5] + i]);
        dst(row_index, col_index + 3)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[5] + i]);
        dst(row_index + 1, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index + 1, col_index + 2)[i] = mix_6_to_1_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[1] + i]);
        dst(row_index + 2, col_index)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 2)[i] = mix_6_to_1_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[5] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[5] + i]);
    }
}

template<typename T>
void hqx4xInterpolate<T>::interpolate_141(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i]         = mix_5_to_3(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index, col_index + 1)[i]     = mix_5_to_3(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index, col_index + 2)[i]     = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[5] + i]);
        dst(row_index, col_index + 3)[i]     = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[5] + i]);
        dst(row_index + 1, col_index)[i]     = mix_7_to_1(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 2)[i] = mix_6_to_1_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[1] + i]);
        dst(row_index + 2, col_index)[i]     = mix_3_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 2, col_index + 2)[i] = mix_6_to_1_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index)[i]     = mix_5_to_3(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[6] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[5] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[5] + i]);
    }
}

template<typename T>
void hqx4xInterpolate<T>::interpolate_50(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        if (diff(mask[index_[1] + i], mask[index_[5] + i], threshold_[i])) {
            dst(row_index, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[2] + i]);
            dst(row_index, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[2] + i]);
            dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[2] + i]);
            dst(row_index + 1, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        } else {
            dst(row_index, col_index + 2)[i] = mix_even(mask[index_[1] + i], mask[index_[4] + i]);
            dst(row_index, col_index + 3)[i] = mix_even(mask[index_[1] + i], mask[index_[5] + i]);
            dst(row_index + 1, col_index + 2)[i] = mask[index_[4] + i];
            dst(row_index + 1, col_index + 3)[i] = mix_even(mask[index_[5] + i], mask[index_[4] + i]);
        }
        dst(row_index + 1, col_index)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[0] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index + 2, col_index)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_6_to_1_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[3] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[3] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[8] + i]);
    }
}

template<typename T>
void hqx4xInterpolate<T>::interpolate_81(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[3] + i]);
        dst(row_index, col_index + 1)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[3] + i]);
        dst(row_index, col_index + 2)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[2] + i]);
        dst(row_index, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_6_to_1_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 2, col_index)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[6] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        if (diff(mask[index_[5] + i], mask[index_[7] + i], threshold_[i])) {
            dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[8] + i]);
            dst(row_index + 2, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[8] + i]);
            dst(row_index + 3, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[8] + i]);
            dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[8] + i]);
        } else {
            dst(row_index + 2, col_index + 2)[i] = mask[index_[4] + i];
            dst(row_index + 2, col_index + 3)[i] = mix_even(mask[index_[5] + i], mask[index_[4] + i]);
            dst(row_index + 3, col_index + 2)[i] = mix_even(mask[index_[7] + i], mask[index_[4] + i]);
            dst(row_index + 3, col_index + 3)[i] = mix_even(mask[index_[7] + i], mask[index_[5] + i]);
        }
        dst(row_index + 3, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[6] + i]);
    }
}

template<typename T>
void hqx4xInterpolate<T>::interpolate_76(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index, col_index + 1)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[0] + i]);
        dst(row_index, col_index + 2)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[5] + i]);
        dst(row_index, col_index + 3)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[5] + i]);
        dst(row_index + 1, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index + 1, col_index + 2)[i] = mix_6_to_1_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[1] + i]);
        if (diff(mask[index_[7] + i], mask[index_[3] + i], threshold_[i])) {
            dst(row_index + 2, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[6] + i]);
            dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[6] + i]);
            dst(row_index + 3, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[6] + i]);
            dst(row_index + 3, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        } else {
            dst(row_index + 2, col_index)[i] = mix_even(mask[index_[3] + i], mask[index_[4] + i]);
            dst(row_index + 2, col_index + 1)[i] = mask[index_[4] + i];
            dst(row_index + 3, col_index)[i] = mix_even(mask[index_[7] + i], mask[index_[3] + i]);
            dst(row_index + 3, col_index + 1)[i] = mix_even(mask[index_[7] + i], mask[index_[4] + i]);
        }
        dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[8] + i]);
    }
}

template<typename T>
void hqx4xInterpolate<T>::interpolate_138(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        if (diff(mask[index_[3] + i], mask[index_[1] + i], threshold_[i])) {
            dst(row_index, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[0] + i]);
            dst(row_index, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[0] + i]);
            dst(row_index + 1, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[0] + i]);
            dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        } else {
            dst(row_index, col_index)[i] = mix_even(mask[index_[1] + i], mask[index_[3] + i]);
            dst(row_index, col_index + 1)[i] = mix_even(mask[index_[1] + i], mask[index_[4] + i]);
            dst(row_index + 1, col_index)[i] = mix_even(mask[index_[3] + i], mask[index_[4] + i]);
            dst(row_index + 1, col_index + 1)[i] = mask[index_[4] + i];
        }
        dst(row_index, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[2] + i]);
        dst(row_index + 2, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 2, col_index + 2)[i] = mix_6_to_1_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[6] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[5] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[5] + i]);
    }
}

template<typename T>
void hqx4xInterpolate<T>::interpolate_66(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i]         = mix_5_to_3(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index, col_index + 1)[i]     = mix_3_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index, col_index + 2)[i]     = mix_3_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index, col_index + 3)[i]     = mix_5_to_3(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index)[i]     = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[0] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[2] + i]);
        dst(row_index + 2, col_index)[i]     = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[6] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index)[i]     = mix_5_to_3(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[8] + i]);
    }
}

template<typename T>
void hqx4xInterpolate<T>::interpolate_24(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i]         = mix_5_to_3(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index, col_index + 1)[i]     = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[0] + i]);
        dst(row_index, col_index + 2)[i]     = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[2] + i]);
        dst(row_index, col_index + 3)[i]     = mix_5_to_3(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index)[i]     = mix_3_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 2, col_index)[i]     = mix_3_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index)[i]     = mix_5_to_3(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[6] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[8] + i]);
    }
}

template<typename T>
void hqx4xInterpolate<T>::interpolate_135(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i]         = mix_5_to_3(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index, col_index + 1)[i]     = mix_7_to_1(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index, col_index + 2)[i]     = mix_7_to_1(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index, col_index + 3)[i]     = mix_5_to_3(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 1, col_index)[i]     = mix_5_to_3(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 2, col_index)[i]     = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_6_to_1_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 2)[i] = mix_6_to_1_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index)[i]     = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[3] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[3] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[5] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[5] + i]);
    }
}

template<typename T>
void hqx4xInterpolate<T>::interpolate_180(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[3] + i]);
        dst(row_index, col_index + 1)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[3] + i]);
        dst(row_index, col_index + 2)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_6_to_1_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index + 2, col_index)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_6_to_1_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[3] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[3] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[7] + i]);
    }
}

template<typename T>
void hqx4xInterpolate<T>::interpolate_225(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i]         = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[3] + i]);
        dst(row_index, col_index + 1)[i]     = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[3] + i]);
        dst(row_index, col_index + 2)[i]     = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[5] + i]);
        dst(row_index, col_index + 3)[i]     = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[5] + i]);
        dst(row_index + 1, col_index)[i]     = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_6_to_1_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 2)[i] = mix_6_to_1_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[1] + i]);
        dst(row_index + 2, col_index)[i]     = mix_5_to_3(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 3, col_index)[i]     = mix_5_to_3(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[5] + i]);
    }
}

template<typename T>
void hqx4xInterpolate<T>::interpolate_45(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i]         = mix_5_to_3(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index, col_index + 1)[i]     = mix_5_to_3(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index, col_index + 2)[i]     = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[5] + i]);
        dst(row_index, col_index + 3)[i]     = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[5] + i]);
        dst(row_index + 1, col_index)[i]     = mix_7_to_1(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 2)[i] = mix_6_to_1_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[1] + i]);
        dst(row_index + 2, col_index)[i]     = mix_7_to_1(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 2)[i] = mix_6_to_1_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index)[i]     = mix_5_to_3(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[5] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[5] + i]);
    }
}

template<typename T>
void hqx4xInterpolate<T>::interpolate_54(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        if (diff(mask[index_[1] + i], mask[index_[5] + i], threshold_[i])) {
            dst(row_index, col_index + 2)[i] = mask[index_[4] + i];
            dst(row_index, col_index + 3)[i] = mask[index_[4] + i];
            dst(row_index + 1, col_index + 3)[i] = mask[index_[4] + i];
        } else {
            dst(row_index, col_index + 2)[i] = mix_even(mask[index_[1] + i], mask[index_[4] + i]);
            dst(row_index, col_index + 3)[i] = mix_even(mask[index_[1] + i], mask[index_[5] + i]);
            dst(row_index + 1, col_index + 3)[i] = mix_even(mask[index_[5] + i], mask[index_[4] + i]);
        }
        dst(row_index + 1, col_index)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[0] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index + 1, col_index + 2)[i] = mask[index_[4] + i];
        dst(row_index + 2, col_index)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_6_to_1_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[3] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[3] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[8] + i]);
    }
}

template<typename T>
void hqx4xInterpolate<T>::interpolate_209(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[3] + i]);
        dst(row_index, col_index + 1)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[3] + i]);
        dst(row_index, col_index + 2)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[2] + i]);
        dst(row_index, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_6_to_1_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 2, col_index)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[6] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 2, col_index + 2)[i] = mask[index_[4] + i];
        if (diff(mask[index_[5] + i], mask[index_[7] + i], threshold_[i])) {
            dst(row_index + 2, col_index + 3)[i] = mask[index_[4] + i];
            dst(row_index + 3, col_index + 2)[i] = mask[index_[4] + i];
            dst(row_index + 3, col_index + 3)[i] = mask[index_[4] + i];
        } else {
            dst(row_index + 2, col_index + 3)[i] = mix_even(mask[index_[5] + i], mask[index_[4] + i]);
            dst(row_index + 3, col_index + 2)[i] = mix_even(mask[index_[7] + i], mask[index_[4] + i]);
            dst(row_index + 3, col_index + 3)[i] = mix_even(mask[index_[7] + i], mask[index_[5] + i]);
        }
        dst(row_index + 3, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[6] + i]);
    }
}

template<typename T>
void hqx4xInterpolate<T>::interpolate_108(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index, col_index + 1)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[0] + i]);
        dst(row_index, col_index + 2)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[5] + i]);
        dst(row_index, col_index + 3)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[5] + i]);
        dst(row_index + 1, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index + 1, col_index + 2)[i] = mix_6_to_1_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[1] + i]);
        if (diff(mask[index_[7] + i], mask[index_[3] + i], threshold_[i])) {
            dst(row_index + 2, col_index)[i] = mask[index_[4] + i];
            dst(row_index + 3, col_index)[i] = mask[index_[4] + i];
            dst(row_index + 3, col_index + 1)[i] = mask[index_[4] + i];
        } else {
            dst(row_index + 2, col_index)[i] = mix_even(mask[index_[3] + i], mask[index_[4] + i]);
            dst(row_index + 3, col_index)[i] = mix_even(mask[index_[7] + i], mask[index_[3] + i]);
            dst(row_index + 3, col_index + 1)[i] = mix_even(mask[index_[7] + i], mask[index_[4] + i]);
        }
        dst(row_index + 2, col_index + 1)[i] = mask[index_[4] + i];
        dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[8] + i]);
    }
}

template<typename T>
void hqx4xInterpolate<T>::interpolate_139(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        if (diff(mask[index_[3] + i], mask[index_[1] + i], threshold_[i])) {
            dst(row_index, col_index)[i] = mask[index_[4] + i];
            dst(row_index, col_index + 1)[i] = mask[index_[4] + i];
            dst(row_index + 1, col_index)[i] = mask[index_[4] + i];
        } else {
            dst(row_index, col_index)[i] = mix_even(mask[index_[1] + i], mask[index_[3] + i]);
            dst(row_index, col_index + 1)[i] = mix_even(mask[index_[1] + i], mask[index_[4] + i]);
            dst(row_index + 1, col_index)[i] = mix_even(mask[index_[3] + i], mask[index_[4] + i]);
        }
        dst(row_index, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index + 1)[i] = mask[index_[4] + i];
        dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[2] + i]);
        dst(row_index + 2, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 2, col_index + 2)[i] = mix_6_to_1_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[6] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[5] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[5] + i]);
    }
}

template<typename T>
void hqx4xInterpolate<T>::interpolate_51(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        if (diff(mask[index_[1] + i], mask[index_[5] + i], threshold_[i])) {
            dst(row_index, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[3] + i]);
            dst(row_index, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[3] + i]);
            dst(row_index, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[2] + i]);
            dst(row_index, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[2] + i]);
            dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[2] + i]);
            dst(row_index + 1, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        } else {
            dst(row_index, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[1] + i]);
            dst(row_index, col_index + 1)[i] = mix_3_to_1(mask[index_[1] + i], mask[index_[4] + i]);
            dst(row_index, col_index + 2)[i] = mix_5_to_3(mask[index_[1] + i], mask[index_[5] + i]);
            dst(row_index, col_index + 3)[i] = mix_even(mask[index_[1] + i], mask[index_[5] + i]);
            dst(row_index + 1, col_index + 2)[i] = mix_6_to_1_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[1] + i]);
            dst(row_index + 1, col_index + 3)[i] = mix_2_to_1_to_1(mask[index_[5] + i], mask[index_[4] + i], mask[index_[1] + i]);
        }
        dst(row_index + 1, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 2, col_index)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_6_to_1_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[3] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[3] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[8] + i]);
    }
}

template<typename T>
void hqx4xInterpolate<T>::interpolate_178(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        if (diff(mask[index_[1] + i], mask[index_[5] + i], threshold_[i])) {
            dst(row_index, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[2] + i]);
            dst(row_index, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[2] + i]);
            dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[2] + i]);
            dst(row_index + 1, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[2] + i]);
            dst(row_index + 2, col_index + 3)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[7] + i]);
            dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[7] + i]);
        } else {
            dst(row_index, col_index + 2)[i] = mix_2_to_1_to_1(mask[index_[1] + i], mask[index_[4] + i], mask[index_[5] + i]);
            dst(row_index, col_index + 3)[i] = mix_even(mask[index_[1] + i], mask[index_[5] + i]);
            dst(row_index + 1, col_index + 2)[i] = mix_6_to_1_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[1] + i]);
            dst(row_index + 1, col_index + 3)[i] = mix_5_to_3(mask[index_[5] + i], mask[index_[1] + i]);
            dst(row_index + 2, col_index + 3)[i] = mix_3_to_1(mask[index_[5] + i], mask[index_[4] + i]);
            dst(row_index + 3, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[5] + i]);
        }
        dst(row_index + 1, col_index)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[0] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index + 2, col_index)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_6_to_1_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[3] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[3] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[7] + i]);
    }
}

template<typename T>
void hqx4xInterpolate<T>::interpolate_85(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[3] + i]);
        dst(row_index, col_index + 1)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[3] + i]);
        dst(row_index, col_index + 2)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[1] + i]);
        if (diff(mask[index_[5] + i], mask[index_[7] + i], threshold_[i])) {
            dst(row_index, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[1] + i]);
            dst(row_index + 1, col_index + 3)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[1] + i]);
            dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[8] + i]);
            dst(row_index + 2, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[8] + i]);
            dst(row_index + 3, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[8] + i]);
            dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[8] + i]);
        } else {
            dst(row_index, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[5] + i]);
            dst(row_index + 1, col_index + 3)[i] = mix_3_to_1(mask[index_[5] + i], mask[index_[4] + i]);
            dst(row_index + 2, col_index + 2)[i] = mix_6_to_1_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[7] + i]);
            dst(row_index + 2, col_index + 3)[i] = mix_5_to_3(mask[index_[5] + i], mask[index_[7] + i]);
            dst(row_index + 3, col_index + 2)[i] = mix_2_to_1_to_1(mask[index_[7] + i], mask[index_[4] + i], mask[index_[5] + i]);
            dst(row_index + 3, col_index + 3)[i] = mix_even(mask[index_[7] + i], mask[index_[5] + i]);
        }
        dst(row_index + 1, col_index)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_6_to_1_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index + 2, col_index)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[6] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 3, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[6] + i]);
    }
}

template<typename T>
void hqx4xInterpolate<T>::interpolate_113(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[3] + i]);
        dst(row_index, col_index + 1)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[3] + i]);
        dst(row_index, col_index + 2)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[2] + i]);
        dst(row_index, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_6_to_1_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 2, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[3] + i]);
        if (diff(mask[index_[5] + i], mask[index_[7] + i], threshold_[i])) {
            dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[8] + i]);
            dst(row_index + 2, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[8] + i]);
            dst(row_index + 3, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[3] + i]);
            dst(row_index + 3, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[3] + i]);
            dst(row_index + 3, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[8] + i]);
            dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[8] + i]);
        } else {
            dst(row_index + 2, col_index + 2)[i] = mix_6_to_1_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[7] + i]);
            dst(row_index + 2, col_index + 3)[i] = mix_2_to_1_to_1(mask[index_[5] + i], mask[index_[4] + i], mask[index_[7] + i]);
            dst(row_index + 3, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[7] + i]);
            dst(row_index + 3, col_index + 1)[i] = mix_3_to_1(mask[index_[7] + i], mask[index_[4] + i]);
            dst(row_index + 3, col_index + 2)[i] = mix_5_to_3(mask[index_[7] + i], mask[index_[5] + i]);
            dst(row_index + 3, col_index + 3)[i] = mix_even(mask[index_[7] + i], mask[index_[5] + i]);
        }
    }
}

template<typename T>
void hqx4xInterpolate<T>::interpolate_204(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index, col_index + 1)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[0] + i]);
        dst(row_index, col_index + 2)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[5] + i]);
        dst(row_index, col_index + 3)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[5] + i]);
        dst(row_index + 1, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index + 1, col_index + 2)[i] = mix_6_to_1_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[1] + i]);
        if (diff(mask[index_[7] + i], mask[index_[3] + i], threshold_[i])) {
            dst(row_index + 2, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[6] + i]);
            dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[6] + i]);
            dst(row_index + 3, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[6] + i]);
            dst(row_index + 3, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[6] + i]);
            dst(row_index + 3, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[5] + i]);
            dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[5] + i]);
        } else {
            dst(row_index + 2, col_index)[i] = mix_2_to_1_to_1(mask[index_[3] + i], mask[index_[4] + i], mask[index_[7] + i]);
            dst(row_index + 2, col_index + 1)[i] = mix_6_to_1_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[7] + i]);
            dst(row_index + 3, col_index)[i] = mix_even(mask[index_[7] + i], mask[index_[3] + i]);
            dst(row_index + 3, col_index + 1)[i] = mix_5_to_3(mask[index_[7] + i], mask[index_[3] + i]);
            dst(row_index + 3, col_index + 2)[i] = mix_3_to_1(mask[index_[7] + i], mask[index_[4] + i]);
            dst(row_index + 3, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[7] + i]);
        }
        dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[5] + i]);
    }
}

template<typename T>
void hqx4xInterpolate<T>::interpolate_77(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        if (diff(mask[index_[7] + i], mask[index_[3] + i], threshold_[i])) {
            dst(row_index, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[1] + i]);
            dst(row_index + 1, col_index)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[1] + i]);
            dst(row_index + 2, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[6] + i]);
            dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[6] + i]);
            dst(row_index + 3, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[6] + i]);
            dst(row_index + 3, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        } else {
            dst(row_index, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[3] + i]);
            dst(row_index + 1, col_index)[i] = mix_3_to_1(mask[index_[3] + i], mask[index_[4] + i]);
            dst(row_index + 2, col_index)[i] = mix_5_to_3(mask[index_[3] + i], mask[index_[7] + i]);
            dst(row_index + 2, col_index + 1)[i] = mix_6_to_1_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[7] + i]);
            dst(row_index + 3, col_index)[i] = mix_even(mask[index_[7] + i], mask[index_[3] + i]);
            dst(row_index + 3, col_index + 1)[i] = mix_2_to_1_to_1(mask[index_[7] + i], mask[index_[4] + i], mask[index_[3] + i]);
        }
        dst(row_index, col_index + 1)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index, col_index + 2)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[5] + i]);
        dst(row_index, col_index + 3)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[5] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 2)[i] = mix_6_to_1_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[1] + i]);
        dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[8] + i]);
    }
}

template<typename T>
void hqx4xInterpolate<T>::interpolate_170(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        if (diff(mask[index_[3] + i], mask[index_[1] + i], threshold_[i])) {
            dst(row_index, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[0] + i]);
            dst(row_index, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[0] + i]);
            dst(row_index + 1, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[0] + i]);
            dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[0] + i]);
            dst(row_index + 2, col_index)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[7] + i]);
            dst(row_index + 3, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[7] + i]);
        } else {
            dst(row_index, col_index)[i] = mix_even(mask[index_[1] + i], mask[index_[3] + i]);
            dst(row_index, col_index + 1)[i] = mix_2_to_1_to_1(mask[index_[1] + i], mask[index_[4] + i], mask[index_[3] + i]);
            dst(row_index + 1, col_index)[i] = mix_5_to_3(mask[index_[3] + i], mask[index_[1] + i]);
            dst(row_index + 1, col_index + 1)[i] = mix_6_to_1_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[1] + i]);
            dst(row_index + 2, col_index)[i] = mix_3_to_1(mask[index_[3] + i], mask[index_[4] + i]);
            dst(row_index + 3, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[3] + i]);
        }
        dst(row_index, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[2] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 2)[i] = mix_6_to_1_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[5] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[5] + i]);
    }
}

template<typename T>
void hqx4xInterpolate<T>::interpolate_142(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        if (diff(mask[index_[3] + i], mask[index_[1] + i], threshold_[i])) {
            dst(row_index, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[0] + i]);
            dst(row_index, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[0] + i]);
            dst(row_index, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[5] + i]);
            dst(row_index, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[5] + i]);
            dst(row_index + 1, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[0] + i]);
            dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        } else {
            dst(row_index, col_index)[i] = mix_even(mask[index_[1] + i], mask[index_[3] + i]);
            dst(row_index, col_index + 1)[i] = mix_5_to_3(mask[index_[1] + i], mask[index_[3] + i]);
            dst(row_index, col_index + 2)[i] = mix_3_to_1(mask[index_[1] + i], mask[index_[4] + i]);
            dst(row_index, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[1] + i]);
            dst(row_index + 1, col_index)[i] = mix_2_to_1_to_1(mask[index_[3] + i], mask[index_[4] + i], mask[index_[1] + i]);
            dst(row_index + 1, col_index + 1)[i] = mix_6_to_1_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[1] + i]);
        }
        dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 2, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 2, col_index + 2)[i] = mix_6_to_1_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[6] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[5] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[5] + i]);
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_67(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i]         = mix_5_to_3(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index, col_index + 1)[i]     = mix_7_to_1(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index, col_index + 2)[i]     = mix_3_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index, col_index + 3)[i]     = mix_5_to_3(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index)[i]     = mix_5_to_3(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[2] + i]);
        dst(row_index + 2, col_index)[i]     = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[6] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index)[i]     = mix_5_to_3(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[8] + i]);
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_70(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i]         = mix_5_to_3(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index, col_index + 1)[i]     = mix_3_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index, col_index + 2)[i]     = mix_7_to_1(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index, col_index + 3)[i]     = mix_5_to_3(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 1, col_index)[i]     = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[0] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 2, col_index)[i]     = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[6] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index)[i]     = mix_5_to_3(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[8] + i]);
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_28(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i]         = mix_5_to_3(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index, col_index + 1)[i]     = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[0] + i]);
        dst(row_index, col_index + 2)[i]     = mix_5_to_3(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index, col_index + 3)[i]     = mix_5_to_3(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index)[i]     = mix_3_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index + 2, col_index)[i]     = mix_3_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index)[i]     = mix_5_to_3(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[6] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[8] + i]);
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_152(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i]         = mix_5_to_3(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index, col_index + 1)[i]     = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[0] + i]);
        dst(row_index, col_index + 2)[i]     = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[2] + i]);
        dst(row_index, col_index + 3)[i]     = mix_5_to_3(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index)[i]     = mix_3_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 2, col_index)[i]     = mix_3_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index)[i]     = mix_5_to_3(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[6] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[7] + i]);
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_194(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i]         = mix_5_to_3(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index, col_index + 1)[i]     = mix_3_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index, col_index + 2)[i]     = mix_3_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index, col_index + 3)[i]     = mix_5_to_3(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index)[i]     = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[0] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[2] + i]);
        dst(row_index + 2, col_index)[i]     = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[6] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 3, col_index)[i]     = mix_5_to_3(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[5] + i]);
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_98(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i]         = mix_5_to_3(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index, col_index + 1)[i]     = mix_3_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index, col_index + 2)[i]     = mix_3_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index, col_index + 3)[i]     = mix_5_to_3(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index)[i]     = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[0] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[2] + i]);
        dst(row_index + 2, col_index)[i]     = mix_5_to_3(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index)[i]     = mix_5_to_3(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[8] + i]);
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_56(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i]         = mix_5_to_3(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index, col_index + 1)[i]     = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[0] + i]);
        dst(row_index, col_index + 2)[i]     = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[2] + i]);
        dst(row_index, col_index + 3)[i]     = mix_5_to_3(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index)[i]     = mix_3_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 2, col_index)[i]     = mix_7_to_1(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index)[i]     = mix_5_to_3(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[8] + i]);
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_25(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index, col_index + 1)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index, col_index + 2)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[2] + i]);
        dst(row_index, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 2, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[6] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[8] + i]);
    }
}

template<typename T>
void hqx4xInterpolate<T>::interpolate_31(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        if (diff(mask[index_[3] + i], mask[index_[1] + i], threshold_[i])) {
            dst(row_index, col_index)[i] = mask[index_[4] + i];
            dst(row_index, col_index + 1)[i] = mask[index_[4] + i];
            dst(row_index + 1, col_index)[i] = mask[index_[4] + i];
        } else {
            dst(row_index, col_index)[i] = mix_even(mask[index_[1] + i], mask[index_[3] + i]);
            dst(row_index, col_index + 1)[i] = mix_even(mask[index_[1] + i], mask[index_[4] + i]);
            dst(row_index + 1, col_index)[i] = mix_even(mask[index_[3] + i], mask[index_[4] + i]);
        }
        if (diff(mask[index_[1] + i], mask[index_[5] + i], threshold_[i])) {
            dst(row_index, col_index + 2)[i] = mask[index_[4] + i];
            dst(row_index, col_index + 3)[i] = mask[index_[4] + i];
            dst(row_index + 1, col_index + 3)[i] = mask[index_[4] + i];
        } else {
            dst(row_index, col_index + 2)[i] = mix_even(mask[index_[1] + i], mask[index_[4] + i]);
            dst(row_index, col_index + 3)[i] = mix_even(mask[index_[1] + i], mask[index_[5] + i]);
            dst(row_index + 1, col_index + 3)[i] = mix_even(mask[index_[5] + i], mask[index_[4] + i]);
        }
        dst(row_index + 1, col_index + 1)[i] = mask[index_[4] + i];
        dst(row_index + 1, col_index + 2)[i] = mask[index_[4] + i];
        dst(row_index + 2, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[6] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[8] + i]);
    }
}

template<typename T>
void hqx4xInterpolate<T>::interpolate_214(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        if (diff(mask[index_[1] + i], mask[index_[5] + i], threshold_[i])) {
            dst(row_index, col_index + 2)[i] = mask[index_[4] + i];
            dst(row_index, col_index + 3)[i] = mask[index_[4] + i];
            dst(row_index + 1, col_index + 3)[i] = mask[index_[4] + i];
        } else {
            dst(row_index, col_index + 2)[i] = mix_even(mask[index_[1] + i], mask[index_[4] + i]);
            dst(row_index, col_index + 3)[i] = mix_even(mask[index_[1] + i], mask[index_[5] + i]);
            dst(row_index + 1, col_index + 3)[i] = mix_even(mask[index_[5] + i], mask[index_[4] + i]);
        }
        dst(row_index + 1, col_index)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[0] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index + 1, col_index + 2)[i] = mask[index_[4] + i];
        dst(row_index + 2, col_index)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[6] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 2, col_index + 2)[i] = mask[index_[4] + i];
        if (diff(mask[index_[5] + i], mask[index_[7] + i], threshold_[i])) {
            dst(row_index + 2, col_index + 3)[i] = mask[index_[4] + i];
            dst(row_index + 3, col_index + 2)[i] = mask[index_[4] + i];
            dst(row_index + 3, col_index + 3)[i] = mask[index_[4] + i];
        } else {
            dst(row_index + 2, col_index + 3)[i] = mix_even(mask[index_[5] + i], mask[index_[4] + i]);
            dst(row_index + 3, col_index + 2)[i] = mix_even(mask[index_[7] + i], mask[index_[4] + i]);
            dst(row_index + 3, col_index + 3)[i] = mix_even(mask[index_[7] + i], mask[index_[5] + i]);
        }
        dst(row_index + 3, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[6] + i]);
    }
}

template<typename T>
void hqx4xInterpolate<T>::interpolate_248(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index, col_index + 1)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[0] + i]);
        dst(row_index, col_index + 2)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[2] + i]);
        dst(row_index, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        if (diff(mask[index_[7] + i], mask[index_[3] + i], threshold_[i])) {
            dst(row_index + 2, col_index)[i] = mask[index_[4] + i];
            dst(row_index + 3, col_index)[i] = mask[index_[4] + i];
            dst(row_index + 3, col_index + 1)[i] = mask[index_[4] + i];
        } else {
            dst(row_index + 2, col_index)[i] = mix_even(mask[index_[3] + i], mask[index_[4] + i]);
            dst(row_index + 3, col_index)[i] = mix_even(mask[index_[7] + i], mask[index_[3] + i]);
            dst(row_index + 3, col_index + 1)[i] = mix_even(mask[index_[7] + i], mask[index_[4] + i]);
        }
        dst(row_index + 2, col_index + 1)[i] = mask[index_[4] + i];
        dst(row_index + 2, col_index + 2)[i] = mask[index_[4] + i];
        if (diff(mask[index_[5] + i], mask[index_[7] + i], threshold_[i])) {
            dst(row_index + 2, col_index + 3)[i] = mask[index_[4] + i];
            dst(row_index + 3, col_index + 2)[i] = mask[index_[4] + i];
            dst(row_index + 3, col_index + 3)[i] = mask[index_[4] + i];
        } else {
            dst(row_index + 2, col_index + 3)[i] = mix_even(mask[index_[5] + i], mask[index_[4] + i]);
            dst(row_index + 3, col_index + 2)[i] = mix_even(mask[index_[7] + i], mask[index_[4] + i]);
            dst(row_index + 3, col_index + 3)[i] = mix_even(mask[index_[7] + i], mask[index_[5] + i]);
        }
    }
}

template<typename T>
void hqx4xInterpolate<T>::interpolate_107(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        if (diff(mask[index_[3] + i], mask[index_[1] + i], threshold_[i])) {
            dst(row_index, col_index)[i] = mask[index_[4] + i];
            dst(row_index, col_index + 1)[i] = mask[index_[4] + i];
            dst(row_index + 1, col_index)[i] = mask[index_[4] + i];
        } else {
            dst(row_index, col_index)[i] = mix_even(mask[index_[1] + i], mask[index_[3] + i]);
            dst(row_index, col_index + 1)[i] = mix_even(mask[index_[1] + i], mask[index_[4] + i]);
            dst(row_index + 1, col_index)[i] = mix_even(mask[index_[3] + i], mask[index_[4] + i]);
        }
        dst(row_index, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index + 1)[i] = mask[index_[4] + i];
        dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[2] + i]);
        if (diff(mask[index_[7] + i], mask[index_[3] + i], threshold_[i])) {
            dst(row_index + 2, col_index)[i] = mask[index_[4] + i];
            dst(row_index + 3, col_index)[i] = mask[index_[4] + i];
            dst(row_index + 3, col_index + 1)[i] = mask[index_[4] + i];
        } else {
            dst(row_index + 2, col_index)[i] = mix_even(mask[index_[3] + i], mask[index_[4] + i]);
            dst(row_index + 3, col_index)[i] = mix_even(mask[index_[7] + i], mask[index_[3] + i]);
            dst(row_index + 3, col_index + 1)[i] = mix_even(mask[index_[7] + i], mask[index_[4] + i]);
        }
        dst(row_index + 2, col_index + 1)[i] = mask[index_[4] + i];
        dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[8] + i]);
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_27(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        if (diff(mask[index_[3] + i], mask[index_[1] + i], threshold_[i])) {
            dst(row_index, col_index)[i] = mask[index_[4] + i];
            dst(row_index, col_index + 1)[i] = mask[index_[4] + i];
            dst(row_index + 1, col_index)[i] = mask[index_[4] + i];
        } else {
            dst(row_index, col_index)[i] = mix_even(mask[index_[1] + i], mask[index_[3] + i]);
            dst(row_index, col_index + 1)[i] = mix_even(mask[index_[1] + i], mask[index_[4] + i]);
            dst(row_index + 1, col_index)[i] = mix_even(mask[index_[3] + i], mask[index_[4] + i]);
        }
        dst(row_index, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index + 1)[i] = mask[index_[4] + i];
        dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 2, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[6] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[8] + i]);
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_86(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        if (diff(mask[index_[1] + i], mask[index_[5] + i], threshold_[i])) {
            dst(row_index, col_index + 2)[i] = mask[index_[4] + i];
            dst(row_index, col_index + 3)[i] = mask[index_[4] + i];
            dst(row_index + 1, col_index + 3)[i] = mask[index_[4] + i];
        } else {
            dst(row_index, col_index + 2)[i] = mix_even(mask[index_[1] + i], mask[index_[4] + i]);
            dst(row_index, col_index + 3)[i] = mix_even(mask[index_[1] + i], mask[index_[5] + i]);
            dst(row_index + 1, col_index + 3)[i] = mix_even(mask[index_[5] + i], mask[index_[4] + i]);
        }
        dst(row_index + 1, col_index)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[0] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index + 1, col_index + 2)[i] = mask[index_[4] + i];
        dst(row_index + 2, col_index)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[6] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[8] + i]);
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_216(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index, col_index + 1)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[0] + i]);
        dst(row_index, col_index + 2)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[2] + i]);
        dst(row_index, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 2, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 2, col_index + 2)[i] = mask[index_[4] + i];
        if (diff(mask[index_[5] + i], mask[index_[7] + i], threshold_[i])) {
            dst(row_index + 2, col_index + 3)[i] = mask[index_[4] + i];
            dst(row_index + 3, col_index + 2)[i] = mask[index_[4] + i];
            dst(row_index + 3, col_index + 3)[i] = mask[index_[4] + i];
        } else {
            dst(row_index + 2, col_index + 3)[i] = mix_even(mask[index_[5] + i], mask[index_[4] + i]);
            dst(row_index + 3, col_index + 2)[i] = mix_even(mask[index_[7] + i], mask[index_[4] + i]);
            dst(row_index + 3, col_index + 3)[i] = mix_even(mask[index_[7] + i], mask[index_[5] + i]);
        }
        dst(row_index + 3, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[6] + i]);
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_106(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[2] + i]);
        if (diff(mask[index_[7] + i], mask[index_[3] + i], threshold_[i])) {
            dst(row_index + 2, col_index)[i] = mask[index_[4] + i];
            dst(row_index + 3, col_index)[i] = mask[index_[4] + i];
            dst(row_index + 3, col_index + 1)[i] = mask[index_[4] + i];
        } else {
            dst(row_index + 2, col_index)[i] = mix_even(mask[index_[3] + i], mask[index_[4] + i]);
            dst(row_index + 3, col_index)[i] = mix_even(mask[index_[7] + i], mask[index_[3] + i]);
            dst(row_index + 3, col_index + 1)[i] = mix_even(mask[index_[7] + i], mask[index_[4] + i]);
        }
        dst(row_index + 2, col_index + 1)[i] = mask[index_[4] + i];
        dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[8] + i]);
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_30(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        if (diff(mask[index_[1] + i], mask[index_[5] + i], threshold_[i])) {
            dst(row_index, col_index + 2)[i] = mask[index_[4] + i];
            dst(row_index, col_index + 3)[i] = mask[index_[4] + i];
            dst(row_index + 1, col_index + 3)[i] = mask[index_[4] + i];
        } else {
            dst(row_index, col_index + 2)[i] = mix_even(mask[index_[1] + i], mask[index_[4] + i]);
            dst(row_index, col_index + 3)[i] = mix_even(mask[index_[1] + i], mask[index_[5] + i]);
            dst(row_index + 1, col_index + 3)[i] = mix_even(mask[index_[5] + i], mask[index_[4] + i]);
        }
        dst(row_index + 1, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index + 1, col_index + 2)[i] = mask[index_[4] + i];
        dst(row_index + 2, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[6] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[8] + i]);
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_210(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[0] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 2, col_index)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[6] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 2, col_index + 2)[i] = mask[index_[4] + i];
        if (diff(mask[index_[5] + i], mask[index_[7] + i], threshold_[i])) {
            dst(row_index + 2, col_index + 3)[i] = mask[index_[4] + i];
            dst(row_index + 3, col_index + 2)[i] = mask[index_[4] + i];
            dst(row_index + 3, col_index + 3)[i] = mask[index_[4] + i];
        } else {
            dst(row_index + 2, col_index + 3)[i] = mix_even(mask[index_[5] + i], mask[index_[4] + i]);
            dst(row_index + 3, col_index + 2)[i] = mix_even(mask[index_[7] + i], mask[index_[4] + i]);
            dst(row_index + 3, col_index + 3)[i] = mix_even(mask[index_[7] + i], mask[index_[5] + i]);
        }
        dst(row_index + 3, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[6] + i]);
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_120(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index, col_index + 1)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[0] + i]);
        dst(row_index, col_index + 2)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[2] + i]);
        dst(row_index, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        if (diff(mask[index_[7] + i], mask[index_[3] + i], threshold_[i])) {
            dst(row_index + 2, col_index)[i] = mask[index_[4] + i];
            dst(row_index + 3, col_index)[i] = mask[index_[4] + i];
            dst(row_index + 3, col_index + 1)[i] = mask[index_[4] + i];
        } else {
            dst(row_index + 2, col_index)[i] = mix_even(mask[index_[3] + i], mask[index_[4] + i]);
            dst(row_index + 3, col_index)[i] = mix_even(mask[index_[7] + i], mask[index_[3] + i]);
            dst(row_index + 3, col_index + 1)[i] = mix_even(mask[index_[7] + i], mask[index_[4] + i]);
        }
        dst(row_index + 2, col_index + 1)[i] = mask[index_[4] + i];
        dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[8] + i]);
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_75(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        if (diff(mask[index_[3] + i], mask[index_[1] + i], threshold_[i])) {
            dst(row_index, col_index)[i] = mask[index_[4] + i];
            dst(row_index, col_index + 1)[i] = mask[index_[4] + i];
            dst(row_index + 1, col_index)[i] = mask[index_[4] + i];
        } else {
            dst(row_index, col_index)[i] = mix_even(mask[index_[1] + i], mask[index_[3] + i]);
            dst(row_index, col_index + 1)[i] = mix_even(mask[index_[1] + i], mask[index_[4] + i]);
            dst(row_index + 1, col_index)[i] = mix_even(mask[index_[3] + i], mask[index_[4] + i]);
        }
        dst(row_index, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index + 1)[i] = mask[index_[4] + i];
        dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[2] + i]);
        dst(row_index + 2, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[8] + i]);
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_29(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index, col_index + 1)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index, col_index + 2)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index + 2, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[6] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[8] + i]);
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_198(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 1, col_index)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[0] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 2, col_index)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[6] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 3, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[5] + i]);
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_184(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index, col_index + 1)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[0] + i]);
        dst(row_index, col_index + 2)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[2] + i]);
        dst(row_index, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 2, col_index)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[7] + i]);
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_99(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[2] + i]);
        dst(row_index + 2, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[8] + i]);
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_57(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index, col_index + 1)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index, col_index + 2)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[2] + i]);
        dst(row_index, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 2, col_index)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[8] + i]);
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_71(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 1, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 2, col_index)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[6] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[8] + i]);
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_156(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index, col_index + 1)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[0] + i]);
        dst(row_index, col_index + 2)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index + 2, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[6] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[7] + i]);
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_226(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[0] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[2] + i]);
        dst(row_index + 2, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 3, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[5] + i]);
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_60(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index, col_index + 1)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[0] + i]);
        dst(row_index, col_index + 2)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index + 2, col_index)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[8] + i]);
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_195(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[2] + i]);
        dst(row_index + 2, col_index)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[6] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 3, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[5] + i]);
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_102(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 1, col_index)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[0] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 2, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[8] + i]);
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_153(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index, col_index + 1)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index, col_index + 2)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[2] + i]);
        dst(row_index, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 2, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[6] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[7] + i]);
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_58(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        if (diff(mask[index_[3] + i], mask[index_[1] + i], threshold_[i])) {
            dst(row_index, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[0] + i]);
            dst(row_index, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[0] + i]);
            dst(row_index + 1, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[0] + i]);
            dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        } else {
            dst(row_index, col_index)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[3] + i]);
            dst(row_index, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[1] + i]);
            dst(row_index + 1, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[3] + i]);
            dst(row_index + 1, col_index + 1)[i] = mask[index_[4] + i];
        }
        if (diff(mask[index_[1] + i], mask[index_[5] + i], threshold_[i])) {
            dst(row_index, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[2] + i]);
            dst(row_index, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[2] + i]);
            dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[2] + i]);
            dst(row_index + 1, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        } else {
            dst(row_index, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[1] + i]);
            dst(row_index, col_index + 3)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[5] + i]);
            dst(row_index + 1, col_index + 2)[i] = mask[index_[4] + i];
            dst(row_index + 1, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[5] + i]);
        }
        dst(row_index + 2, col_index)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[8] + i]);
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_83(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[3] + i]);
        if (diff(mask[index_[1] + i], mask[index_[5] + i], threshold_[i])) {
            dst(row_index, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[2] + i]);
            dst(row_index, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[2] + i]);
            dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[2] + i]);
            dst(row_index + 1, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        } else {
            dst(row_index, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[1] + i]);
            dst(row_index, col_index + 3)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[5] + i]);
            dst(row_index + 1, col_index + 2)[i] = mask[index_[4] + i];
            dst(row_index + 1, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[5] + i]);
        }
        dst(row_index + 1, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 2, col_index)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[6] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        if (diff(mask[index_[5] + i], mask[index_[7] + i], threshold_[i])) {
            dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[8] + i]);
            dst(row_index + 2, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[8] + i]);
            dst(row_index + 3, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[8] + i]);
            dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[8] + i]);
        } else {
            dst(row_index + 2, col_index + 2)[i] = mask[index_[4] + i];
            dst(row_index + 2, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[5] + i]);
            dst(row_index + 3, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[7] + i]);
            dst(row_index + 3, col_index + 3)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[5] + i]);
        }
        dst(row_index + 3, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[6] + i]);
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_92(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index, col_index + 1)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[0] + i]);
        dst(row_index, col_index + 2)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[1] + i]);
        if (diff(mask[index_[7] + i], mask[index_[3] + i], threshold_[i])) {
            dst(row_index + 2, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[6] + i]);
            dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[6] + i]);
            dst(row_index + 3, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[6] + i]);
            dst(row_index + 3, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        } else {
            dst(row_index + 2, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[3] + i]);
            dst(row_index + 2, col_index + 1)[i] = mask[index_[4] + i];
            dst(row_index + 3, col_index)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[3] + i]);
            dst(row_index + 3, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[7] + i]);
        }
        if (diff(mask[index_[5] + i], mask[index_[7] + i], threshold_[i])) {
            dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[8] + i]);
            dst(row_index + 2, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[8] + i]);
            dst(row_index + 3, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[8] + i]);
            dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[8] + i]);
        } else {
            dst(row_index + 2, col_index + 2)[i] = mask[index_[4] + i];
            dst(row_index + 2, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[5] + i]);
            dst(row_index + 3, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[7] + i]);
            dst(row_index + 3, col_index + 3)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[5] + i]);
        }
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_202(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        if (diff(mask[index_[3] + i], mask[index_[1] + i], threshold_[i])) {
            dst(row_index, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[0] + i]);
            dst(row_index, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[0] + i]);
            dst(row_index + 1, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[0] + i]);
            dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        } else {
            dst(row_index, col_index)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[3] + i]);
            dst(row_index, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[1] + i]);
            dst(row_index + 1, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[3] + i]);
            dst(row_index + 1, col_index + 1)[i] = mask[index_[4] + i];
        }
        dst(row_index, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[2] + i]);
        if (diff(mask[index_[7] + i], mask[index_[3] + i], threshold_[i])) {
            dst(row_index + 2, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[6] + i]);
            dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[6] + i]);
            dst(row_index + 3, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[6] + i]);
            dst(row_index + 3, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        } else {
            dst(row_index + 2, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[3] + i]);
            dst(row_index + 2, col_index + 1)[i] = mask[index_[4] + i];
            dst(row_index + 3, col_index)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[3] + i]);
            dst(row_index + 3, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[7] + i]);
        }
        dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[5] + i]);
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_78(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        if (diff(mask[index_[3] + i], mask[index_[1] + i], threshold_[i])) {
            dst(row_index, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[0] + i]);
            dst(row_index, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[0] + i]);
            dst(row_index + 1, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[0] + i]);
            dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        } else {
            dst(row_index, col_index)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[3] + i]);
            dst(row_index, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[1] + i]);
            dst(row_index + 1, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[3] + i]);
            dst(row_index + 1, col_index + 1)[i] = mask[index_[4] + i];
        }
        dst(row_index, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[5] + i]);
        if (diff(mask[index_[7] + i], mask[index_[3] + i], threshold_[i])) {
            dst(row_index + 2, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[6] + i]);
            dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[6] + i]);
            dst(row_index + 3, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[6] + i]);
            dst(row_index + 3, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        } else {
            dst(row_index + 2, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[3] + i]);
            dst(row_index + 2, col_index + 1)[i] = mask[index_[4] + i];
            dst(row_index + 3, col_index)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[3] + i]);
            dst(row_index + 3, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[7] + i]);
        }
        dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[8] + i]);
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_154(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        if (diff(mask[index_[3] + i], mask[index_[1] + i], threshold_[i])) {
            dst(row_index, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[0] + i]);
            dst(row_index, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[0] + i]);
            dst(row_index + 1, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[0] + i]);
            dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        } else {
            dst(row_index, col_index)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[3] + i]);
            dst(row_index, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[1] + i]);
            dst(row_index + 1, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[3] + i]);
            dst(row_index + 1, col_index + 1)[i] = mask[index_[4] + i];
        }
        if (diff(mask[index_[1] + i], mask[index_[5] + i], threshold_[i])) {
            dst(row_index, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[2] + i]);
            dst(row_index, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[2] + i]);
            dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[2] + i]);
            dst(row_index + 1, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        } else {
            dst(row_index, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[1] + i]);
            dst(row_index, col_index + 3)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[5] + i]);
            dst(row_index + 1, col_index + 2)[i] = mask[index_[4] + i];
            dst(row_index + 1, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[5] + i]);
        }
        dst(row_index + 2, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[6] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[7] + i]);
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_114(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        if (diff(mask[index_[1] + i], mask[index_[5] + i], threshold_[i])) {
            dst(row_index, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[2] + i]);
            dst(row_index, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[2] + i]);
            dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[2] + i]);
            dst(row_index + 1, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        } else {
            dst(row_index, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[1] + i]);
            dst(row_index, col_index + 3)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[5] + i]);
            dst(row_index + 1, col_index + 2)[i] = mask[index_[4] + i];
            dst(row_index + 1, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[5] + i]);
        }
        dst(row_index + 1, col_index)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[0] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index + 2, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[3] + i]);
        if (diff(mask[index_[5] + i], mask[index_[7] + i], threshold_[i])) {
            dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[8] + i]);
            dst(row_index + 2, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[8] + i]);
            dst(row_index + 3, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[8] + i]);
            dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[8] + i]);
        } else {
            dst(row_index + 2, col_index + 2)[i] = mask[index_[4] + i];
            dst(row_index + 2, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[5] + i]);
            dst(row_index + 3, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[7] + i]);
            dst(row_index + 3, col_index + 3)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[5] + i]);
        }
        dst(row_index + 3, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[3] + i]);
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_89(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index, col_index + 1)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index, col_index + 2)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[2] + i]);
        dst(row_index, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        if (diff(mask[index_[7] + i], mask[index_[3] + i], threshold_[i])) {
            dst(row_index + 2, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[6] + i]);
            dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[6] + i]);
            dst(row_index + 3, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[6] + i]);
            dst(row_index + 3, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        } else {
            dst(row_index + 2, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[3] + i]);
            dst(row_index + 2, col_index + 1)[i] = mask[index_[4] + i];
            dst(row_index + 3, col_index)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[3] + i]);
            dst(row_index + 3, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[7] + i]);
        }
        if (diff(mask[index_[5] + i], mask[index_[7] + i], threshold_[i])) {
            dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[8] + i]);
            dst(row_index + 2, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[8] + i]);
            dst(row_index + 3, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[8] + i]);
            dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[8] + i]);
        } else {
            dst(row_index + 2, col_index + 2)[i] = mask[index_[4] + i];
            dst(row_index + 2, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[5] + i]);
            dst(row_index + 3, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[7] + i]);
            dst(row_index + 3, col_index + 3)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[5] + i]);
        }
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_90(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        if (diff(mask[index_[3] + i], mask[index_[1] + i], threshold_[i])) {
            dst(row_index, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[0] + i]);
            dst(row_index, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[0] + i]);
            dst(row_index + 1, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[0] + i]);
            dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        } else {
            dst(row_index, col_index)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[3] + i]);
            dst(row_index, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[1] + i]);
            dst(row_index + 1, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[3] + i]);
            dst(row_index + 1, col_index + 1)[i] = mask[index_[4] + i];
        }
        if (diff(mask[index_[1] + i], mask[index_[5] + i], threshold_[i])) {
            dst(row_index, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[2] + i]);
            dst(row_index, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[2] + i]);
            dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[2] + i]);
            dst(row_index + 1, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        } else {
            dst(row_index, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[1] + i]);
            dst(row_index, col_index + 3)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[5] + i]);
            dst(row_index + 1, col_index + 2)[i] = mask[index_[4] + i];
            dst(row_index + 1, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[5] + i]);
        }
        if (diff(mask[index_[7] + i], mask[index_[3] + i], threshold_[i])) {
            dst(row_index + 2, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[6] + i]);
            dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[6] + i]);
            dst(row_index + 3, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[6] + i]);
            dst(row_index + 3, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        } else {
            dst(row_index + 2, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[3] + i]);
            dst(row_index + 2, col_index + 1)[i] = mask[index_[4] + i];
            dst(row_index + 3, col_index)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[3] + i]);
            dst(row_index + 3, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[7] + i]);
        }
        if (diff(mask[index_[5] + i], mask[index_[7] + i], threshold_[i])) {
            dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[8] + i]);
            dst(row_index + 2, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[8] + i]);
            dst(row_index + 3, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[8] + i]);
            dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[8] + i]);
        } else {
            dst(row_index + 2, col_index + 2)[i] = mask[index_[4] + i];
            dst(row_index + 2, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[5] + i]);
            dst(row_index + 3, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[7] + i]);
            dst(row_index + 3, col_index + 3)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[5] + i]);
        }
    }
}

template<typename T>
void hqx4xInterpolate<T>::interpolate_23(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        if (diff(mask[index_[1] + i], mask[index_[5] + i], threshold_[i])) {
            dst(row_index, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[3] + i]);
            dst(row_index, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[3] + i]);
            dst(row_index, col_index + 2)[i] = mask[index_[4] + i];
            dst(row_index, col_index + 3)[i] = mask[index_[4] + i];
            dst(row_index + 1, col_index + 2)[i] = mask[index_[4] + i];
            dst(row_index + 1, col_index + 3)[i] = mask[index_[4] + i];
        } else {
            dst(row_index, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[1] + i]);
            dst(row_index, col_index + 1)[i] = mix_3_to_1(mask[index_[1] + i], mask[index_[4] + i]);
            dst(row_index, col_index + 2)[i] = mix_5_to_3(mask[index_[1] + i], mask[index_[5] + i]);
            dst(row_index, col_index + 3)[i] = mix_even(mask[index_[1] + i], mask[index_[5] + i]);
            dst(row_index + 1, col_index + 2)[i] = mix_6_to_1_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[1] + i]);
            dst(row_index + 1, col_index + 3)[i] = mix_2_to_1_to_1(mask[index_[5] + i], mask[index_[4] + i], mask[index_[1] + i]);
        }
        dst(row_index + 1, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 2, col_index)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_6_to_1_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[3] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[3] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[8] + i]);
    }
}

template<typename T>
void hqx4xInterpolate<T>::interpolate_150(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        if (diff(mask[index_[1] + i], mask[index_[5] + i], threshold_[i])) {
            dst(row_index, col_index + 2)[i] = mask[index_[4] + i];
            dst(row_index, col_index + 3)[i] = mask[index_[4] + i];
            dst(row_index + 1, col_index + 2)[i] = mask[index_[4] + i];
            dst(row_index + 1, col_index + 3)[i] = mask[index_[4] + i];
            dst(row_index + 2, col_index + 3)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[7] + i]);
            dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[7] + i]);
        } else {
            dst(row_index, col_index + 2)[i] = mix_2_to_1_to_1(mask[index_[1] + i], mask[index_[4] + i], mask[index_[5] + i]);
            dst(row_index, col_index + 3)[i] = mix_even(mask[index_[1] + i], mask[index_[5] + i]);
            dst(row_index + 1, col_index + 2)[i] = mix_6_to_1_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[1] + i]);
            dst(row_index + 1, col_index + 3)[i] = mix_5_to_3(mask[index_[5] + i], mask[index_[1] + i]);
            dst(row_index + 2, col_index + 3)[i] = mix_3_to_1(mask[index_[5] + i], mask[index_[4] + i]);
            dst(row_index + 3, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[5] + i]);
        }
        dst(row_index + 1, col_index)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[0] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index + 2, col_index)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_6_to_1_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[3] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[3] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[7] + i]);
    }
}

template<typename T>
void hqx4xInterpolate<T>::interpolate_212(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[3] + i]);
        dst(row_index, col_index + 1)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[3] + i]);
        dst(row_index, col_index + 2)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[1] + i]);
        if (diff(mask[index_[5] + i], mask[index_[7] + i], threshold_[i])) {
            dst(row_index, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[1] + i]);
            dst(row_index + 1, col_index + 3)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[1] + i]);
            dst(row_index + 2, col_index + 2)[i] = mask[index_[4] + i];
            dst(row_index + 2, col_index + 3)[i] = mask[index_[4] + i];
            dst(row_index + 3, col_index + 2)[i] = mask[index_[4] + i];
            dst(row_index + 3, col_index + 3)[i] = mask[index_[4] + i];
        } else {
            dst(row_index, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[5] + i]);
            dst(row_index + 1, col_index + 3)[i] = mix_3_to_1(mask[index_[5] + i], mask[index_[4] + i]);
            dst(row_index + 2, col_index + 2)[i] = mix_6_to_1_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[7] + i]);
            dst(row_index + 2, col_index + 3)[i] = mix_5_to_3(mask[index_[5] + i], mask[index_[7] + i]);
            dst(row_index + 3, col_index + 2)[i] = mix_2_to_1_to_1(mask[index_[7] + i], mask[index_[4] + i], mask[index_[5] + i]);
            dst(row_index + 3, col_index + 3)[i] = mix_even(mask[index_[7] + i], mask[index_[5] + i]);
        }
        dst(row_index + 1, col_index)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_6_to_1_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index + 2, col_index)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[6] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 3, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[6] + i]);
    }
}

template<typename T>
void hqx4xInterpolate<T>::interpolate_240(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[3] + i]);
        dst(row_index, col_index + 1)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[3] + i]);
        dst(row_index, col_index + 2)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[2] + i]);
        dst(row_index, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_6_to_1_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 2, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[3] + i]);
        if (diff(mask[index_[5] + i], mask[index_[7] + i], threshold_[i])) {
            dst(row_index + 2, col_index + 2)[i] = mask[index_[4] + i];
            dst(row_index + 2, col_index + 3)[i] = mask[index_[4] + i];
            dst(row_index + 3, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[3] + i]);
            dst(row_index + 3, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[3] + i]);
            dst(row_index + 3, col_index + 2)[i] = mask[index_[4] + i];
            dst(row_index + 3, col_index + 3)[i] = mask[index_[4] + i];
        } else {
            dst(row_index + 2, col_index + 2)[i] = mix_6_to_1_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[7] + i]);
            dst(row_index + 2, col_index + 3)[i] = mix_2_to_1_to_1(mask[index_[5] + i], mask[index_[4] + i], mask[index_[7] + i]);
            dst(row_index + 3, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[7] + i]);
            dst(row_index + 3, col_index + 1)[i] = mix_3_to_1(mask[index_[7] + i], mask[index_[4] + i]);
            dst(row_index + 3, col_index + 2)[i] = mix_5_to_3(mask[index_[7] + i], mask[index_[5] + i]);
            dst(row_index + 3, col_index + 3)[i] = mix_even(mask[index_[7] + i], mask[index_[5] + i]);
        }
    }
}

template<typename T>
void hqx4xInterpolate<T>::interpolate_232(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index, col_index + 1)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[0] + i]);
        dst(row_index, col_index + 2)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[5] + i]);
        dst(row_index, col_index + 3)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[5] + i]);
        dst(row_index + 1, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index + 1, col_index + 2)[i] = mix_6_to_1_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[1] + i]);
        if (diff(mask[index_[7] + i], mask[index_[3] + i], threshold_[i])) {
            dst(row_index + 2, col_index)[i] = mask[index_[4] + i];
            dst(row_index + 2, col_index + 1)[i] = mask[index_[4] + i];
            dst(row_index + 3, col_index)[i] = mask[index_[4] + i];
            dst(row_index + 3, col_index + 1)[i] = mask[index_[4] + i];
            dst(row_index + 3, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[5] + i]);
            dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[5] + i]);
        } else {
            dst(row_index + 2, col_index)[i] = mix_2_to_1_to_1(mask[index_[3] + i], mask[index_[4] + i], mask[index_[7] + i]);
            dst(row_index + 2, col_index + 1)[i] = mix_6_to_1_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[7] + i]);
            dst(row_index + 3, col_index)[i] = mix_even(mask[index_[7] + i], mask[index_[3] + i]);
            dst(row_index + 3, col_index + 1)[i] = mix_5_to_3(mask[index_[7] + i], mask[index_[3] + i]);
            dst(row_index + 3, col_index + 2)[i] = mix_3_to_1(mask[index_[7] + i], mask[index_[4] + i]);
            dst(row_index + 3, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[7] + i]);
        }
        dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[5] + i]);
    }
}

template<typename T>
void hqx4xInterpolate<T>::interpolate_105(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        if (diff(mask[index_[7] + i], mask[index_[3] + i], threshold_[i])) {
            dst(row_index, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[1] + i]);
            dst(row_index + 1, col_index)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[1] + i]);
            dst(row_index + 2, col_index)[i] = mask[index_[4] + i];
            dst(row_index + 2, col_index + 1)[i] = mask[index_[4] + i];
            dst(row_index + 3, col_index)[i] = mask[index_[4] + i];
            dst(row_index + 3, col_index + 1)[i] = mask[index_[4] + i];
        } else {
            dst(row_index, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[3] + i]);
            dst(row_index + 1, col_index)[i] = mix_3_to_1(mask[index_[3] + i], mask[index_[4] + i]);
            dst(row_index + 2, col_index)[i] = mix_5_to_3(mask[index_[3] + i], mask[index_[7] + i]);
            dst(row_index + 2, col_index + 1)[i] = mix_6_to_1_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[7] + i]);
            dst(row_index + 3, col_index)[i] = mix_even(mask[index_[7] + i], mask[index_[3] + i]);
            dst(row_index + 3, col_index + 1)[i] = mix_2_to_1_to_1(mask[index_[7] + i], mask[index_[4] + i], mask[index_[3] + i]);
        }
        dst(row_index, col_index + 1)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index, col_index + 2)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[5] + i]);
        dst(row_index, col_index + 3)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[5] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 2)[i] = mix_6_to_1_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[1] + i]);
        dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[8] + i]);
    }
}

template<typename T>
void hqx4xInterpolate<T>::interpolate_43(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        if (diff(mask[index_[3] + i], mask[index_[1] + i], threshold_[i])) {
            dst(row_index, col_index)[i] = mask[index_[4] + i];
            dst(row_index, col_index + 1)[i] = mask[index_[4] + i];
            dst(row_index + 1, col_index)[i] = mask[index_[4] + i];
            dst(row_index + 1, col_index + 1)[i] = mask[index_[4] + i];
            dst(row_index + 2, col_index)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[7] + i]);
            dst(row_index + 3, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[7] + i]);
        } else {
            dst(row_index, col_index)[i] = mix_even(mask[index_[1] + i], mask[index_[3] + i]);
            dst(row_index, col_index + 1)[i] = mix_2_to_1_to_1(mask[index_[1] + i], mask[index_[4] + i], mask[index_[3] + i]);
            dst(row_index + 1, col_index)[i] = mix_5_to_3(mask[index_[3] + i], mask[index_[1] + i]);
            dst(row_index + 1, col_index + 1)[i] = mix_6_to_1_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[1] + i]);
            dst(row_index + 2, col_index)[i] = mix_3_to_1(mask[index_[3] + i], mask[index_[4] + i]);
            dst(row_index + 3, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[3] + i]);
        }
        dst(row_index, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[2] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 2)[i] = mix_6_to_1_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[5] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[5] + i]);
    }
}

template<typename T>
void hqx4xInterpolate<T>::interpolate_15(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        if (diff(mask[index_[3] + i], mask[index_[1] + i], threshold_[i])) {
            dst(row_index, col_index)[i] = mask[index_[4] + i];
            dst(row_index, col_index + 1)[i] = mask[index_[4] + i];
            dst(row_index, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[5] + i]);
            dst(row_index, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[5] + i]);
            dst(row_index + 1, col_index)[i] = mask[index_[4] + i];
            dst(row_index + 1, col_index + 1)[i] = mask[index_[4] + i];
        } else {
            dst(row_index, col_index)[i] = mix_even(mask[index_[1] + i], mask[index_[3] + i]);
            dst(row_index, col_index + 1)[i] = mix_5_to_3(mask[index_[1] + i], mask[index_[3] + i]);
            dst(row_index, col_index + 2)[i] = mix_3_to_1(mask[index_[1] + i], mask[index_[4] + i]);
            dst(row_index, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[1] + i]);
            dst(row_index + 1, col_index)[i] = mix_2_to_1_to_1(mask[index_[3] + i], mask[index_[4] + i], mask[index_[1] + i]);
            dst(row_index + 1, col_index + 1)[i] = mix_6_to_1_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[1] + i]);
        }
        dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 2, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 2, col_index + 2)[i] = mix_6_to_1_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[6] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[5] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[5] + i]);
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_124(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index, col_index + 1)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[0] + i]);
        dst(row_index, col_index + 2)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[1] + i]);
        if (diff(mask[index_[7] + i], mask[index_[3] + i], threshold_[i])) {
            dst(row_index + 2, col_index)[i] = mask[index_[4] + i];
            dst(row_index + 3, col_index)[i] = mask[index_[4] + i];
            dst(row_index + 3, col_index + 1)[i] = mask[index_[4] + i];
        } else {
            dst(row_index + 2, col_index)[i] = mix_even(mask[index_[3] + i], mask[index_[4] + i]);
            dst(row_index + 3, col_index)[i] = mix_even(mask[index_[7] + i], mask[index_[3] + i]);
            dst(row_index + 3, col_index + 1)[i] = mix_even(mask[index_[7] + i], mask[index_[4] + i]);
        }
        dst(row_index + 2, col_index + 1)[i] = mask[index_[4] + i];
        dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[8] + i]);
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_203(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        if (diff(mask[index_[3] + i], mask[index_[1] + i], threshold_[i])) {
            dst(row_index, col_index)[i] = mask[index_[4] + i];
            dst(row_index, col_index + 1)[i] = mask[index_[4] + i];
            dst(row_index + 1, col_index)[i] = mask[index_[4] + i];
        } else {
            dst(row_index, col_index)[i] = mix_even(mask[index_[1] + i], mask[index_[3] + i]);
            dst(row_index, col_index + 1)[i] = mix_even(mask[index_[1] + i], mask[index_[4] + i]);
            dst(row_index + 1, col_index)[i] = mix_even(mask[index_[3] + i], mask[index_[4] + i]);
        }
        dst(row_index, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index + 1)[i] = mask[index_[4] + i];
        dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[2] + i]);
        dst(row_index + 2, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 3, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[5] + i]);
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_62(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        if (diff(mask[index_[1] + i], mask[index_[5] + i], threshold_[i])) {
            dst(row_index, col_index + 2)[i] = mask[index_[4] + i];
            dst(row_index, col_index + 3)[i] = mask[index_[4] + i];
            dst(row_index + 1, col_index + 3)[i] = mask[index_[4] + i];
        } else {
            dst(row_index, col_index + 2)[i] = mix_even(mask[index_[1] + i], mask[index_[4] + i]);
            dst(row_index, col_index + 3)[i] = mix_even(mask[index_[1] + i], mask[index_[5] + i]);
            dst(row_index + 1, col_index + 3)[i] = mix_even(mask[index_[5] + i], mask[index_[4] + i]);
        }
        dst(row_index + 1, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index + 1, col_index + 2)[i] = mask[index_[4] + i];
        dst(row_index + 2, col_index)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[8] + i]);
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_211(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 2, col_index)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[6] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 2, col_index + 2)[i] = mask[index_[4] + i];
        if (diff(mask[index_[5] + i], mask[index_[7] + i], threshold_[i])) {
            dst(row_index + 2, col_index + 3)[i] = mask[index_[4] + i];
            dst(row_index + 3, col_index + 2)[i] = mask[index_[4] + i];
            dst(row_index + 3, col_index + 3)[i] = mask[index_[4] + i];
        } else {
            dst(row_index + 2, col_index + 3)[i] = mix_even(mask[index_[5] + i], mask[index_[4] + i]);
            dst(row_index + 3, col_index + 2)[i] = mix_even(mask[index_[7] + i], mask[index_[4] + i]);
            dst(row_index + 3, col_index + 3)[i] = mix_even(mask[index_[7] + i], mask[index_[5] + i]);
        }
        dst(row_index + 3, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[6] + i]);
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_118(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        if (diff(mask[index_[1] + i], mask[index_[5] + i], threshold_[i])) {
            dst(row_index, col_index + 2)[i] = mask[index_[4] + i];
            dst(row_index, col_index + 3)[i] = mask[index_[4] + i];
            dst(row_index + 1, col_index + 3)[i] = mask[index_[4] + i];
        } else {
            dst(row_index, col_index + 2)[i] = mix_even(mask[index_[1] + i], mask[index_[4] + i]);
            dst(row_index, col_index + 3)[i] = mix_even(mask[index_[1] + i], mask[index_[5] + i]);
            dst(row_index + 1, col_index + 3)[i] = mix_even(mask[index_[5] + i], mask[index_[4] + i]);
        }
        dst(row_index + 1, col_index)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[0] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index + 1, col_index + 2)[i] = mask[index_[4] + i];
        dst(row_index + 2, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[8] + i]);
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_217(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index, col_index + 1)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index, col_index + 2)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[2] + i]);
        dst(row_index, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 2, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 2, col_index + 2)[i] = mask[index_[4] + i];
        if (diff(mask[index_[5] + i], mask[index_[7] + i], threshold_[i])) {
            dst(row_index + 2, col_index + 3)[i] = mask[index_[4] + i];
            dst(row_index + 3, col_index + 2)[i] = mask[index_[4] + i];
            dst(row_index + 3, col_index + 3)[i] = mask[index_[4] + i];
        } else {
            dst(row_index + 2, col_index + 3)[i] = mix_even(mask[index_[5] + i], mask[index_[4] + i]);
            dst(row_index + 3, col_index + 2)[i] = mix_even(mask[index_[7] + i], mask[index_[4] + i]);
            dst(row_index + 3, col_index + 3)[i] = mix_even(mask[index_[7] + i], mask[index_[5] + i]);
        }
        dst(row_index + 3, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[6] + i]);
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_110(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 1, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[5] + i]);
        if (diff(mask[index_[7] + i], mask[index_[3] + i], threshold_[i])) {
            dst(row_index + 2, col_index)[i] = mask[index_[4] + i];
            dst(row_index + 3, col_index)[i] = mask[index_[4] + i];
            dst(row_index + 3, col_index + 1)[i] = mask[index_[4] + i];
        } else {
            dst(row_index + 2, col_index)[i] = mix_even(mask[index_[3] + i], mask[index_[4] + i]);
            dst(row_index + 3, col_index)[i] = mix_even(mask[index_[7] + i], mask[index_[3] + i]);
            dst(row_index + 3, col_index + 1)[i] = mix_even(mask[index_[7] + i], mask[index_[4] + i]);
        }
        dst(row_index + 2, col_index + 1)[i] = mask[index_[4] + i];
        dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[8] + i]);
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_155(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        if (diff(mask[index_[3] + i], mask[index_[1] + i], threshold_[i])) {
            dst(row_index, col_index)[i] = mask[index_[4] + i];
            dst(row_index, col_index + 1)[i] = mask[index_[4] + i];
            dst(row_index + 1, col_index)[i] = mask[index_[4] + i];
        } else {
            dst(row_index, col_index)[i] = mix_even(mask[index_[1] + i], mask[index_[3] + i]);
            dst(row_index, col_index + 1)[i] = mix_even(mask[index_[1] + i], mask[index_[4] + i]);
            dst(row_index + 1, col_index)[i] = mix_even(mask[index_[3] + i], mask[index_[4] + i]);
        }
        dst(row_index, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index + 1)[i] = mask[index_[4] + i];
        dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 2, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[6] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[7] + i]);
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_188(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index, col_index + 1)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[0] + i]);
        dst(row_index, col_index + 2)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index + 2, col_index)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[7] + i]);
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_185(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index, col_index + 1)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index, col_index + 2)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[2] + i]);
        dst(row_index, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 2, col_index)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[7] + i]);
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_61(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index, col_index + 1)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index, col_index + 2)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index + 2, col_index)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[8] + i]);
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_157(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index, col_index + 1)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index, col_index + 2)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index + 2, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[6] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[7] + i]);
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_103(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 1, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 2, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[8] + i]);
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_227(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[2] + i]);
        dst(row_index + 2, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 3, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[5] + i]);
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_230(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 1, col_index)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[0] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 2, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 3, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[5] + i]);
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_199(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 1, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 2, col_index)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[6] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 3, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[5] + i]);
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_220(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index, col_index + 1)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[0] + i]);
        dst(row_index, col_index + 2)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[1] + i]);
        if (diff(mask[index_[7] + i], mask[index_[3] + i], threshold_[i])) {
            dst(row_index + 2, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[6] + i]);
            dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[6] + i]);
            dst(row_index + 3, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[6] + i]);
            dst(row_index + 3, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        } else {
            dst(row_index + 2, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[3] + i]);
            dst(row_index + 2, col_index + 1)[i] = mask[index_[4] + i];
            dst(row_index + 3, col_index)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[3] + i]);
            dst(row_index + 3, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[7] + i]);
        }
        dst(row_index + 2, col_index + 2)[i] = mask[index_[4] + i];
        if (diff(mask[index_[5] + i], mask[index_[7] + i], threshold_[i])) {
            dst(row_index + 2, col_index + 3)[i] = mask[index_[4] + i];
            dst(row_index + 3, col_index + 2)[i] = mask[index_[4] + i];
            dst(row_index + 3, col_index + 3)[i] = mask[index_[4] + i];
        } else {
            dst(row_index + 2, col_index + 3)[i] = mix_even(mask[index_[5] + i], mask[index_[4] + i]);
            dst(row_index + 3, col_index + 2)[i] = mix_even(mask[index_[7] + i], mask[index_[4] + i]);
            dst(row_index + 3, col_index + 3)[i] = mix_even(mask[index_[7] + i], mask[index_[5] + i]);
        }
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_158(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        if (diff(mask[index_[3] + i], mask[index_[1] + i], threshold_[i])) {
            dst(row_index, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[0] + i]);
            dst(row_index, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[0] + i]);
            dst(row_index + 1, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[0] + i]);
            dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        } else {
            dst(row_index, col_index)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[3] + i]);
            dst(row_index, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[1] + i]);
            dst(row_index + 1, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[3] + i]);
            dst(row_index + 1, col_index + 1)[i] = mask[index_[4] + i];
        }
        if (diff(mask[index_[1] + i], mask[index_[5] + i], threshold_[i])) {
            dst(row_index, col_index + 2)[i] = mask[index_[4] + i];
            dst(row_index, col_index + 3)[i] = mask[index_[4] + i];
            dst(row_index + 1, col_index + 3)[i] = mask[index_[4] + i];
        } else {
            dst(row_index, col_index + 2)[i] = mix_even(mask[index_[1] + i], mask[index_[4] + i]);
            dst(row_index, col_index + 3)[i] = mix_even(mask[index_[1] + i], mask[index_[5] + i]);
            dst(row_index + 1, col_index + 3)[i] = mix_even(mask[index_[5] + i], mask[index_[4] + i]);
        }
        dst(row_index + 1, col_index + 2)[i] = mask[index_[4] + i];
        dst(row_index + 2, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[6] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[7] + i]);
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_234(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        if (diff(mask[index_[3] + i], mask[index_[1] + i], threshold_[i])) {
            dst(row_index, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[0] + i]);
            dst(row_index, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[0] + i]);
            dst(row_index + 1, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[0] + i]);
            dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        } else {
            dst(row_index, col_index)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[3] + i]);
            dst(row_index, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[1] + i]);
            dst(row_index + 1, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[3] + i]);
            dst(row_index + 1, col_index + 1)[i] = mask[index_[4] + i];
        }
        dst(row_index, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[2] + i]);
        if (diff(mask[index_[7] + i], mask[index_[3] + i], threshold_[i])) {
            dst(row_index + 2, col_index)[i] = mask[index_[4] + i];
            dst(row_index + 3, col_index)[i] = mask[index_[4] + i];
            dst(row_index + 3, col_index + 1)[i] = mask[index_[4] + i];
        } else {
            dst(row_index + 2, col_index)[i] = mix_even(mask[index_[3] + i], mask[index_[4] + i]);
            dst(row_index + 3, col_index)[i] = mix_even(mask[index_[7] + i], mask[index_[3] + i]);
            dst(row_index + 3, col_index + 1)[i] = mix_even(mask[index_[7] + i], mask[index_[4] + i]);
        }
        dst(row_index + 2, col_index + 1)[i] = mask[index_[4] + i];
        dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[5] + i]);
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_242(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        if (diff(mask[index_[1] + i], mask[index_[5] + i], threshold_[i])) {
            dst(row_index, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[2] + i]);
            dst(row_index, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[2] + i]);
            dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[2] + i]);
            dst(row_index + 1, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        } else {
            dst(row_index, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[1] + i]);
            dst(row_index, col_index + 3)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[5] + i]);
            dst(row_index + 1, col_index + 2)[i] = mask[index_[4] + i];
            dst(row_index + 1, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[5] + i]);
        }
        dst(row_index + 1, col_index)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[0] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index + 2, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 2, col_index + 2)[i] = mask[index_[4] + i];
        if (diff(mask[index_[5] + i], mask[index_[7] + i], threshold_[i])) {
            dst(row_index + 2, col_index + 3)[i] = mask[index_[4] + i];
            dst(row_index + 3, col_index + 2)[i] = mask[index_[4] + i];
            dst(row_index + 3, col_index + 3)[i] = mask[index_[4] + i];
        } else {
            dst(row_index + 2, col_index + 3)[i] = mix_even(mask[index_[5] + i], mask[index_[4] + i]);
            dst(row_index + 3, col_index + 2)[i] = mix_even(mask[index_[7] + i], mask[index_[4] + i]);
            dst(row_index + 3, col_index + 3)[i] = mix_even(mask[index_[7] + i], mask[index_[5] + i]);
        }
        dst(row_index + 3, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[3] + i]);
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_59(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        if (diff(mask[index_[3] + i], mask[index_[1] + i], threshold_[i])) {
            dst(row_index, col_index)[i] = mask[index_[4] + i];
            dst(row_index, col_index + 1)[i] = mask[index_[4] + i];
            dst(row_index + 1, col_index)[i] = mask[index_[4] + i];
        } else {
            dst(row_index, col_index)[i] = mix_even(mask[index_[1] + i], mask[index_[3] + i]);
            dst(row_index, col_index + 1)[i] = mix_even(mask[index_[1] + i], mask[index_[4] + i]);
            dst(row_index + 1, col_index)[i] = mix_even(mask[index_[3] + i], mask[index_[4] + i]);
        }
        if (diff(mask[index_[1] + i], mask[index_[5] + i], threshold_[i])) {
            dst(row_index, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[2] + i]);
            dst(row_index, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[2] + i]);
            dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[2] + i]);
            dst(row_index + 1, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        } else {
            dst(row_index, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[1] + i]);
            dst(row_index, col_index + 3)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[5] + i]);
            dst(row_index + 1, col_index + 2)[i] = mask[index_[4] + i];
            dst(row_index + 1, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[5] + i]);
        }
        dst(row_index + 1, col_index + 1)[i] = mask[index_[4] + i];
        dst(row_index + 2, col_index)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[8] + i]);
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_121(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index, col_index + 1)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index, col_index + 2)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[2] + i]);
        dst(row_index, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        if (diff(mask[index_[7] + i], mask[index_[3] + i], threshold_[i])) {
            dst(row_index + 2, col_index)[i] = mask[index_[4] + i];
            dst(row_index + 3, col_index)[i] = mask[index_[4] + i];
            dst(row_index + 3, col_index + 1)[i] = mask[index_[4] + i];
        } else {
            dst(row_index + 2, col_index)[i] = mix_even(mask[index_[3] + i], mask[index_[4] + i]);
            dst(row_index + 3, col_index)[i] = mix_even(mask[index_[7] + i], mask[index_[3] + i]);
            dst(row_index + 3, col_index + 1)[i] = mix_even(mask[index_[7] + i], mask[index_[4] + i]);
        }
        dst(row_index + 2, col_index + 1)[i] = mask[index_[4] + i];
        if (diff(mask[index_[5] + i], mask[index_[7] + i], threshold_[i])) {
            dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[8] + i]);
            dst(row_index + 2, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[8] + i]);
            dst(row_index + 3, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[8] + i]);
            dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[8] + i]);
        } else {
            dst(row_index + 2, col_index + 2)[i] = mask[index_[4] + i];
            dst(row_index + 2, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[5] + i]);
            dst(row_index + 3, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[7] + i]);
            dst(row_index + 3, col_index + 3)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[5] + i]);
        }
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_87(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[3] + i]);
        if (diff(mask[index_[1] + i], mask[index_[5] + i], threshold_[i])) {
            dst(row_index, col_index + 2)[i] = mask[index_[4] + i];
            dst(row_index, col_index + 3)[i] = mask[index_[4] + i];
            dst(row_index + 1, col_index + 3)[i] = mask[index_[4] + i];
        } else {
            dst(row_index, col_index + 2)[i] = mix_even(mask[index_[1] + i], mask[index_[4] + i]);
            dst(row_index, col_index + 3)[i] = mix_even(mask[index_[1] + i], mask[index_[5] + i]);
            dst(row_index + 1, col_index + 3)[i] = mix_even(mask[index_[5] + i], mask[index_[4] + i]);
        }
        dst(row_index + 1, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 1, col_index + 2)[i] = mask[index_[4] + i];
        dst(row_index + 2, col_index)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[6] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        if (diff(mask[index_[5] + i], mask[index_[7] + i], threshold_[i])) {
            dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[8] + i]);
            dst(row_index + 2, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[8] + i]);
            dst(row_index + 3, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[8] + i]);
            dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[8] + i]);
        } else {
            dst(row_index + 2, col_index + 2)[i] = mask[index_[4] + i];
            dst(row_index + 2, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[5] + i]);
            dst(row_index + 3, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[7] + i]);
            dst(row_index + 3, col_index + 3)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[5] + i]);
        }
        dst(row_index + 3, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[6] + i]);
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_79(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        if (diff(mask[index_[3] + i], mask[index_[1] + i], threshold_[i])) {
            dst(row_index, col_index)[i] = mask[index_[4] + i];
            dst(row_index, col_index + 1)[i] = mask[index_[4] + i];
            dst(row_index + 1, col_index)[i] = mask[index_[4] + i];
        } else {
            dst(row_index, col_index)[i] = mix_even(mask[index_[1] + i], mask[index_[3] + i]);
            dst(row_index, col_index + 1)[i] = mix_even(mask[index_[1] + i], mask[index_[4] + i]);
            dst(row_index + 1, col_index)[i] = mix_even(mask[index_[3] + i], mask[index_[4] + i]);
        }
        dst(row_index, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 1, col_index + 1)[i] = mask[index_[4] + i];
        dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[5] + i]);
        if (diff(mask[index_[7] + i], mask[index_[3] + i], threshold_[i])) {
            dst(row_index + 2, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[6] + i]);
            dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[6] + i]);
            dst(row_index + 3, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[6] + i]);
            dst(row_index + 3, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        } else {
            dst(row_index + 2, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[3] + i]);
            dst(row_index + 2, col_index + 1)[i] = mask[index_[4] + i];
            dst(row_index + 3, col_index)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[3] + i]);
            dst(row_index + 3, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[7] + i]);
        }
        dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[8] + i]);
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_122(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        if (diff(mask[index_[3] + i], mask[index_[1] + i], threshold_[i])) {
            dst(row_index, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[0] + i]);
            dst(row_index, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[0] + i]);
            dst(row_index + 1, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[0] + i]);
            dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        } else {
            dst(row_index, col_index)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[3] + i]);
            dst(row_index, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[1] + i]);
            dst(row_index + 1, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[3] + i]);
            dst(row_index + 1, col_index + 1)[i] = mask[index_[4] + i];
        }
        if (diff(mask[index_[1] + i], mask[index_[5] + i], threshold_[i])) {
            dst(row_index, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[2] + i]);
            dst(row_index, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[2] + i]);
            dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[2] + i]);
            dst(row_index + 1, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        } else {
            dst(row_index, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[1] + i]);
            dst(row_index, col_index + 3)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[5] + i]);
            dst(row_index + 1, col_index + 2)[i] = mask[index_[4] + i];
            dst(row_index + 1, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[5] + i]);
        }
        if (diff(mask[index_[7] + i], mask[index_[3] + i], threshold_[i])) {
            dst(row_index + 2, col_index)[i] = mask[index_[4] + i];
            dst(row_index + 3, col_index)[i] = mask[index_[4] + i];
            dst(row_index + 3, col_index + 1)[i] = mask[index_[4] + i];
        } else {
            dst(row_index + 2, col_index)[i] = mix_even(mask[index_[3] + i], mask[index_[4] + i]);
            dst(row_index + 3, col_index)[i] = mix_even(mask[index_[7] + i], mask[index_[3] + i]);
            dst(row_index + 3, col_index + 1)[i] = mix_even(mask[index_[7] + i], mask[index_[4] + i]);
        }
        dst(row_index + 2, col_index + 1)[i] = mask[index_[4] + i];
        if (diff(mask[index_[5] + i], mask[index_[7] + i], threshold_[i])) {
            dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[8] + i]);
            dst(row_index + 2, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[8] + i]);
            dst(row_index + 3, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[8] + i]);
            dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[8] + i]);
        } else {
            dst(row_index + 2, col_index + 2)[i] = mask[index_[4] + i];
            dst(row_index + 2, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[5] + i]);
            dst(row_index + 3, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[7] + i]);
            dst(row_index + 3, col_index + 3)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[5] + i]);
        }
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_94(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        if (diff(mask[index_[3] + i], mask[index_[1] + i], threshold_[i])) {
            dst(row_index, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[0] + i]);
            dst(row_index, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[0] + i]);
            dst(row_index + 1, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[0] + i]);
            dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        } else {
            dst(row_index, col_index)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[3] + i]);
            dst(row_index, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[1] + i]);
            dst(row_index + 1, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[3] + i]);
            dst(row_index + 1, col_index + 1)[i] = mask[index_[4] + i];
        }
        if (diff(mask[index_[1] + i], mask[index_[5] + i], threshold_[i])) {
            dst(row_index, col_index + 2)[i] = mask[index_[4] + i];
            dst(row_index, col_index + 3)[i] = mask[index_[4] + i];
            dst(row_index + 1, col_index + 3)[i] = mask[index_[4] + i];
        } else {
            dst(row_index, col_index + 2)[i] = mix_even(mask[index_[1] + i], mask[index_[4] + i]);
            dst(row_index, col_index + 3)[i] = mix_even(mask[index_[1] + i], mask[index_[5] + i]);
            dst(row_index + 1, col_index + 3)[i] = mix_even(mask[index_[5] + i], mask[index_[4] + i]);
        }
        dst(row_index + 1, col_index + 2)[i] = mask[index_[4] + i];
        if (diff(mask[index_[7] + i], mask[index_[3] + i], threshold_[i])) {
            dst(row_index + 2, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[6] + i]);
            dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[6] + i]);
            dst(row_index + 3, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[6] + i]);
            dst(row_index + 3, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        } else {
            dst(row_index + 2, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[3] + i]);
            dst(row_index + 2, col_index + 1)[i] = mask[index_[4] + i];
            dst(row_index + 3, col_index)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[3] + i]);
            dst(row_index + 3, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[7] + i]);
        }
        if (diff(mask[index_[5] + i], mask[index_[7] + i], threshold_[i])) {
            dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[8] + i]);
            dst(row_index + 2, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[8] + i]);
            dst(row_index + 3, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[8] + i]);
            dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[8] + i]);
        } else {
            dst(row_index + 2, col_index + 2)[i] = mask[index_[4] + i];
            dst(row_index + 2, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[5] + i]);
            dst(row_index + 3, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[7] + i]);
            dst(row_index + 3, col_index + 3)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[5] + i]);
        }
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_218(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        if (diff(mask[index_[3] + i], mask[index_[1] + i], threshold_[i])) {
            dst(row_index, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[0] + i]);
            dst(row_index, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[0] + i]);
            dst(row_index + 1, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[0] + i]);
            dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        } else {
            dst(row_index, col_index)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[3] + i]);
            dst(row_index, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[1] + i]);
            dst(row_index + 1, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[3] + i]);
            dst(row_index + 1, col_index + 1)[i] = mask[index_[4] + i];
        }
        if (diff(mask[index_[1] + i], mask[index_[5] + i], threshold_[i])) {
            dst(row_index, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[2] + i]);
            dst(row_index, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[2] + i]);
            dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[2] + i]);
            dst(row_index + 1, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        } else {
            dst(row_index, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[1] + i]);
            dst(row_index, col_index + 3)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[5] + i]);
            dst(row_index + 1, col_index + 2)[i] = mask[index_[4] + i];
            dst(row_index + 1, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[5] + i]);
        }
        if (diff(mask[index_[7] + i], mask[index_[3] + i], threshold_[i])) {
            dst(row_index + 2, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[6] + i]);
            dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[6] + i]);
            dst(row_index + 3, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[6] + i]);
            dst(row_index + 3, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        } else {
            dst(row_index + 2, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[3] + i]);
            dst(row_index + 2, col_index + 1)[i] = mask[index_[4] + i];
            dst(row_index + 3, col_index)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[3] + i]);
            dst(row_index + 3, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[7] + i]);
        }
        dst(row_index + 2, col_index + 2)[i] = mask[index_[4] + i];
        if (diff(mask[index_[5] + i], mask[index_[7] + i], threshold_[i])) {
            dst(row_index + 2, col_index + 3)[i] = mask[index_[4] + i];
            dst(row_index + 3, col_index + 2)[i] = mask[index_[4] + i];
            dst(row_index + 3, col_index + 3)[i] = mask[index_[4] + i];
        } else {
            dst(row_index + 2, col_index + 3)[i] = mix_even(mask[index_[5] + i], mask[index_[4] + i]);
            dst(row_index + 3, col_index + 2)[i] = mix_even(mask[index_[7] + i], mask[index_[4] + i]);
            dst(row_index + 3, col_index + 3)[i] = mix_even(mask[index_[7] + i], mask[index_[5] + i]);
        }
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_91(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        if (diff(mask[index_[3] + i], mask[index_[1] + i], threshold_[i])) {
            dst(row_index, col_index)[i] = mask[index_[4] + i];
            dst(row_index, col_index + 1)[i] = mask[index_[4] + i];
            dst(row_index + 1, col_index)[i] = mask[index_[4] + i];
        } else {
            dst(row_index, col_index)[i] = mix_even(mask[index_[1] + i], mask[index_[3] + i]);
            dst(row_index, col_index + 1)[i] = mix_even(mask[index_[1] + i], mask[index_[4] + i]);
            dst(row_index + 1, col_index)[i] = mix_even(mask[index_[3] + i], mask[index_[4] + i]);
        }
        if (diff(mask[index_[1] + i], mask[index_[5] + i], threshold_[i])) {
            dst(row_index, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[2] + i]);
            dst(row_index, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[2] + i]);
            dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[2] + i]);
            dst(row_index + 1, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        } else {
            dst(row_index, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[1] + i]);
            dst(row_index, col_index + 3)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[5] + i]);
            dst(row_index + 1, col_index + 2)[i] = mask[index_[4] + i];
            dst(row_index + 1, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[5] + i]);
        }
        dst(row_index + 1, col_index + 1)[i] = mask[index_[4] + i];
        if (diff(mask[index_[7] + i], mask[index_[3] + i], threshold_[i])) {
            dst(row_index + 2, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[6] + i]);
            dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[6] + i]);
            dst(row_index + 3, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[6] + i]);
            dst(row_index + 3, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        } else {
            dst(row_index + 2, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[3] + i]);
            dst(row_index + 2, col_index + 1)[i] = mask[index_[4] + i];
            dst(row_index + 3, col_index)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[3] + i]);
            dst(row_index + 3, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[7] + i]);
        }
        if (diff(mask[index_[5] + i], mask[index_[7] + i], threshold_[i])) {
            dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[8] + i]);
            dst(row_index + 2, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[8] + i]);
            dst(row_index + 3, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[8] + i]);
            dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[8] + i]);
        } else {
            dst(row_index + 2, col_index + 2)[i] = mask[index_[4] + i];
            dst(row_index + 2, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[5] + i]);
            dst(row_index + 3, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[7] + i]);
            dst(row_index + 3, col_index + 3)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[5] + i]);
        }
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_229(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[3] + i]);
        dst(row_index, col_index + 1)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[3] + i]);
        dst(row_index, col_index + 2)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[5] + i]);
        dst(row_index, col_index + 3)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[5] + i]);
        dst(row_index + 1, col_index)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_6_to_1_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 2)[i] = mix_6_to_1_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[1] + i]);
        dst(row_index + 2, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 3, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[5] + i]);
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_167(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 1, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 2, col_index)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_6_to_1_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 2)[i] = mix_6_to_1_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[3] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[3] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[5] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[5] + i]);
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_173(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index, col_index + 1)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index, col_index + 2)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[5] + i]);
        dst(row_index, col_index + 3)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[5] + i]);
        dst(row_index + 1, col_index)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 2)[i] = mix_6_to_1_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[1] + i]);
        dst(row_index + 2, col_index)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 2)[i] = mix_6_to_1_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[5] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[5] + i]);
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_181(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[3] + i]);
        dst(row_index, col_index + 1)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[3] + i]);
        dst(row_index, col_index + 2)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_6_to_1_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index + 2, col_index)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_6_to_1_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[3] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[3] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[7] + i]);
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_186(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        if (diff(mask[index_[3] + i], mask[index_[1] + i], threshold_[i])) {
            dst(row_index, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[0] + i]);
            dst(row_index, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[0] + i]);
            dst(row_index + 1, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[0] + i]);
            dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        } else {
            dst(row_index, col_index)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[3] + i]);
            dst(row_index, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[1] + i]);
            dst(row_index + 1, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[3] + i]);
            dst(row_index + 1, col_index + 1)[i] = mask[index_[4] + i];
        }
        if (diff(mask[index_[1] + i], mask[index_[5] + i], threshold_[i])) {
            dst(row_index, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[2] + i]);
            dst(row_index, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[2] + i]);
            dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[2] + i]);
            dst(row_index + 1, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        } else {
            dst(row_index, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[1] + i]);
            dst(row_index, col_index + 3)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[5] + i]);
            dst(row_index + 1, col_index + 2)[i] = mask[index_[4] + i];
            dst(row_index + 1, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[5] + i]);
        }
        dst(row_index + 2, col_index)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[7] + i]);
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_115(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[3] + i]);
        if (diff(mask[index_[1] + i], mask[index_[5] + i], threshold_[i])) {
            dst(row_index, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[2] + i]);
            dst(row_index, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[2] + i]);
            dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[2] + i]);
            dst(row_index + 1, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        } else {
            dst(row_index, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[1] + i]);
            dst(row_index, col_index + 3)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[5] + i]);
            dst(row_index + 1, col_index + 2)[i] = mask[index_[4] + i];
            dst(row_index + 1, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[5] + i]);
        }
        dst(row_index + 1, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 2, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[3] + i]);
        if (diff(mask[index_[5] + i], mask[index_[7] + i], threshold_[i])) {
            dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[8] + i]);
            dst(row_index + 2, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[8] + i]);
            dst(row_index + 3, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[8] + i]);
            dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[8] + i]);
        } else {
            dst(row_index + 2, col_index + 2)[i] = mask[index_[4] + i];
            dst(row_index + 2, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[5] + i]);
            dst(row_index + 3, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[7] + i]);
            dst(row_index + 3, col_index + 3)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[5] + i]);
        }
        dst(row_index + 3, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[3] + i]);
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_93(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index, col_index + 1)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index, col_index + 2)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[1] + i]);
        if (diff(mask[index_[7] + i], mask[index_[3] + i], threshold_[i])) {
            dst(row_index + 2, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[6] + i]);
            dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[6] + i]);
            dst(row_index + 3, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[6] + i]);
            dst(row_index + 3, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        } else {
            dst(row_index + 2, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[3] + i]);
            dst(row_index + 2, col_index + 1)[i] = mask[index_[4] + i];
            dst(row_index + 3, col_index)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[3] + i]);
            dst(row_index + 3, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[7] + i]);
        }
        if (diff(mask[index_[5] + i], mask[index_[7] + i], threshold_[i])) {
            dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[8] + i]);
            dst(row_index + 2, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[8] + i]);
            dst(row_index + 3, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[8] + i]);
            dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[8] + i]);
        } else {
            dst(row_index + 2, col_index + 2)[i] = mask[index_[4] + i];
            dst(row_index + 2, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[5] + i]);
            dst(row_index + 3, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[7] + i]);
            dst(row_index + 3, col_index + 3)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[5] + i]);
        }
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_206(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        if (diff(mask[index_[3] + i], mask[index_[1] + i], threshold_[i])) {
            dst(row_index, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[0] + i]);
            dst(row_index, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[0] + i]);
            dst(row_index + 1, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[0] + i]);
            dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        } else {
            dst(row_index, col_index)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[3] + i]);
            dst(row_index, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[1] + i]);
            dst(row_index + 1, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[3] + i]);
            dst(row_index + 1, col_index + 1)[i] = mask[index_[4] + i];
        }
        dst(row_index, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[5] + i]);
        if (diff(mask[index_[7] + i], mask[index_[3] + i], threshold_[i])) {
            dst(row_index + 2, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[6] + i]);
            dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[6] + i]);
            dst(row_index + 3, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[6] + i]);
            dst(row_index + 3, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        } else {
            dst(row_index + 2, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[3] + i]);
            dst(row_index + 2, col_index + 1)[i] = mask[index_[4] + i];
            dst(row_index + 3, col_index)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[3] + i]);
            dst(row_index + 3, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[7] + i]);
        }
        dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[5] + i]);
    }
}

template<typename T>
void hqx4xInterpolate<T>::interpolate_201(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index, col_index + 1)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index, col_index + 2)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[5] + i]);
        dst(row_index, col_index + 3)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[5] + i]);
        dst(row_index + 1, col_index)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 2)[i] = mix_6_to_1_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[1] + i]);
        if (diff(mask[index_[7] + i], mask[index_[3] + i], threshold_[i])) {
            dst(row_index + 2, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[6] + i]);
            dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[6] + i]);
            dst(row_index + 3, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[6] + i]);
            dst(row_index + 3, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        } else {
            dst(row_index + 2, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[3] + i]);
            dst(row_index + 2, col_index + 1)[i] = mask[index_[4] + i];
            dst(row_index + 3, col_index)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[3] + i]);
            dst(row_index + 3, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[7] + i]);
        }
        dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[5] + i]);
    }
}

template<typename T>
void hqx4xInterpolate<T>::interpolate_46(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        if (diff(mask[index_[3] + i], mask[index_[1] + i], threshold_[i])) {
            dst(row_index, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[0] + i]);
            dst(row_index, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[0] + i]);
            dst(row_index + 1, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[0] + i]);
            dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        } else {
            dst(row_index, col_index)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[3] + i]);
            dst(row_index, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[1] + i]);
            dst(row_index + 1, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[3] + i]);
            dst(row_index + 1, col_index + 1)[i] = mask[index_[4] + i];
        }
        dst(row_index, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 2, col_index)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 2)[i] = mix_6_to_1_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[5] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[5] + i]);
    }
}

template<typename T>
void hqx4xInterpolate<T>::interpolate_147(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[3] + i]);
        if (diff(mask[index_[1] + i], mask[index_[5] + i], threshold_[i])) {
            dst(row_index, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[2] + i]);
            dst(row_index, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[2] + i]);
            dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[2] + i]);
            dst(row_index + 1, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        } else {
            dst(row_index, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[1] + i]);
            dst(row_index, col_index + 3)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[5] + i]);
            dst(row_index + 1, col_index + 2)[i] = mask[index_[4] + i];
            dst(row_index + 1, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[5] + i]);
        }
        dst(row_index + 1, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 2, col_index)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_6_to_1_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[3] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[3] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[7] + i]);
    }
}

template<typename T>
void hqx4xInterpolate<T>::interpolate_116(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[3] + i]);
        dst(row_index, col_index + 1)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[3] + i]);
        dst(row_index, col_index + 2)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_6_to_1_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index + 2, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[3] + i]);
        if (diff(mask[index_[5] + i], mask[index_[7] + i], threshold_[i])) {
            dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[8] + i]);
            dst(row_index + 2, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[8] + i]);
            dst(row_index + 3, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[8] + i]);
            dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[8] + i]);
        } else {
            dst(row_index + 2, col_index + 2)[i] = mask[index_[4] + i];
            dst(row_index + 2, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[5] + i]);
            dst(row_index + 3, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[7] + i]);
            dst(row_index + 3, col_index + 3)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[5] + i]);
        }
        dst(row_index + 3, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[3] + i]);
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_189(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index, col_index + 1)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index, col_index + 2)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index + 2, col_index)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[7] + i]);
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_231(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 1, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 2, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 3, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[5] + i]);
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_126(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        if (diff(mask[index_[1] + i], mask[index_[5] + i], threshold_[i])) {
            dst(row_index, col_index + 2)[i] = mask[index_[4] + i];
            dst(row_index, col_index + 3)[i] = mask[index_[4] + i];
            dst(row_index + 1, col_index + 3)[i] = mask[index_[4] + i];
        } else {
            dst(row_index, col_index + 2)[i] = mix_even(mask[index_[1] + i], mask[index_[4] + i]);
            dst(row_index, col_index + 3)[i] = mix_even(mask[index_[1] + i], mask[index_[5] + i]);
            dst(row_index + 1, col_index + 3)[i] = mix_even(mask[index_[5] + i], mask[index_[4] + i]);
        }
        dst(row_index + 1, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index + 1, col_index + 2)[i] = mask[index_[4] + i];
        if (diff(mask[index_[7] + i], mask[index_[3] + i], threshold_[i])) {
            dst(row_index + 2, col_index)[i] = mask[index_[4] + i];
            dst(row_index + 3, col_index)[i] = mask[index_[4] + i];
            dst(row_index + 3, col_index + 1)[i] = mask[index_[4] + i];
        } else {
            dst(row_index + 2, col_index)[i] = mix_even(mask[index_[3] + i], mask[index_[4] + i]);
            dst(row_index + 3, col_index)[i] = mix_even(mask[index_[7] + i], mask[index_[3] + i]);
            dst(row_index + 3, col_index + 1)[i] = mix_even(mask[index_[7] + i], mask[index_[4] + i]);
        }
        dst(row_index + 2, col_index + 1)[i] = mask[index_[4] + i];
        dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[8] + i]);
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_219(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        if (diff(mask[index_[3] + i], mask[index_[1] + i], threshold_[i])) {
            dst(row_index, col_index)[i] = mask[index_[4] + i];
            dst(row_index, col_index + 1)[i] = mask[index_[4] + i];
            dst(row_index + 1, col_index)[i] = mask[index_[4] + i];
        } else {
            dst(row_index, col_index)[i] = mix_even(mask[index_[1] + i], mask[index_[3] + i]);
            dst(row_index, col_index + 1)[i] = mix_even(mask[index_[1] + i], mask[index_[4] + i]);
            dst(row_index + 1, col_index)[i] = mix_even(mask[index_[3] + i], mask[index_[4] + i]);
        }
        dst(row_index, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index + 1)[i] = mask[index_[4] + i];
        dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 2, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 2, col_index + 2)[i] = mask[index_[4] + i];
        if (diff(mask[index_[5] + i], mask[index_[7] + i], threshold_[i])) {
            dst(row_index + 2, col_index + 3)[i] = mask[index_[4] + i];
            dst(row_index + 3, col_index + 2)[i] = mask[index_[4] + i];
            dst(row_index + 3, col_index + 3)[i] = mask[index_[4] + i];
        } else {
            dst(row_index + 2, col_index + 3)[i] = mix_even(mask[index_[5] + i], mask[index_[4] + i]);
            dst(row_index + 3, col_index + 2)[i] = mix_even(mask[index_[7] + i], mask[index_[4] + i]);
            dst(row_index + 3, col_index + 3)[i] = mix_even(mask[index_[7] + i], mask[index_[5] + i]);
        }
        dst(row_index + 3, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[6] + i]);
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_125(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        if (diff(mask[index_[7] + i], mask[index_[3] + i], threshold_[i])) {
            dst(row_index, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[1] + i]);
            dst(row_index + 1, col_index)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[1] + i]);
            dst(row_index + 2, col_index)[i] = mask[index_[4] + i];
            dst(row_index + 2, col_index + 1)[i] = mask[index_[4] + i];
            dst(row_index + 3, col_index)[i] = mask[index_[4] + i];
            dst(row_index + 3, col_index + 1)[i] = mask[index_[4] + i];
        } else {
            dst(row_index, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[3] + i]);
            dst(row_index + 1, col_index)[i] = mix_3_to_1(mask[index_[3] + i], mask[index_[4] + i]);
            dst(row_index + 2, col_index)[i] = mix_5_to_3(mask[index_[3] + i], mask[index_[7] + i]);
            dst(row_index + 2, col_index + 1)[i] = mix_6_to_1_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[7] + i]);
            dst(row_index + 3, col_index)[i] = mix_even(mask[index_[7] + i], mask[index_[3] + i]);
            dst(row_index + 3, col_index + 1)[i] = mix_2_to_1_to_1(mask[index_[7] + i], mask[index_[4] + i], mask[index_[3] + i]);
        }
        dst(row_index, col_index + 1)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index, col_index + 2)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[8] + i]);
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_221(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index, col_index + 1)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index, col_index + 2)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[1] + i]);
        if (diff(mask[index_[5] + i], mask[index_[7] + i], threshold_[i])) {
            dst(row_index, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[1] + i]);
            dst(row_index + 1, col_index + 3)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[1] + i]);
            dst(row_index + 2, col_index + 2)[i] = mask[index_[4] + i];
            dst(row_index + 2, col_index + 3)[i] = mask[index_[4] + i];
            dst(row_index + 3, col_index + 2)[i] = mask[index_[4] + i];
            dst(row_index + 3, col_index + 3)[i] = mask[index_[4] + i];
        } else {
            dst(row_index, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[5] + i]);
            dst(row_index + 1, col_index + 3)[i] = mix_3_to_1(mask[index_[5] + i], mask[index_[4] + i]);
            dst(row_index + 2, col_index + 2)[i] = mix_6_to_1_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[7] + i]);
            dst(row_index + 2, col_index + 3)[i] = mix_5_to_3(mask[index_[5] + i], mask[index_[7] + i]);
            dst(row_index + 3, col_index + 2)[i] = mix_2_to_1_to_1(mask[index_[7] + i], mask[index_[4] + i], mask[index_[5] + i]);
            dst(row_index + 3, col_index + 3)[i] = mix_even(mask[index_[7] + i], mask[index_[5] + i]);
        }
        dst(row_index + 1, col_index)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index + 2, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 3, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[6] + i]);
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_207(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        if (diff(mask[index_[3] + i], mask[index_[1] + i], threshold_[i])) {
            dst(row_index, col_index)[i] = mask[index_[4] + i];
            dst(row_index, col_index + 1)[i] = mask[index_[4] + i];
            dst(row_index, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[5] + i]);
            dst(row_index, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[5] + i]);
            dst(row_index + 1, col_index)[i] = mask[index_[4] + i];
            dst(row_index + 1, col_index + 1)[i] = mask[index_[4] + i];
        } else {
            dst(row_index, col_index)[i] = mix_even(mask[index_[1] + i], mask[index_[3] + i]);
            dst(row_index, col_index + 1)[i] = mix_5_to_3(mask[index_[1] + i], mask[index_[3] + i]);
            dst(row_index, col_index + 2)[i] = mix_3_to_1(mask[index_[1] + i], mask[index_[4] + i]);
            dst(row_index, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[1] + i]);
            dst(row_index + 1, col_index)[i] = mix_2_to_1_to_1(mask[index_[3] + i], mask[index_[4] + i], mask[index_[1] + i]);
            dst(row_index + 1, col_index + 1)[i] = mix_6_to_1_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[1] + i]);
        }
        dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 2, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 3, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[5] + i]);
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_238(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 1, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[5] + i]);
        if (diff(mask[index_[7] + i], mask[index_[3] + i], threshold_[i])) {
            dst(row_index + 2, col_index)[i] = mask[index_[4] + i];
            dst(row_index + 2, col_index + 1)[i] = mask[index_[4] + i];
            dst(row_index + 3, col_index)[i] = mask[index_[4] + i];
            dst(row_index + 3, col_index + 1)[i] = mask[index_[4] + i];
            dst(row_index + 3, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[5] + i]);
            dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[5] + i]);
        } else {
            dst(row_index + 2, col_index)[i] = mix_2_to_1_to_1(mask[index_[3] + i], mask[index_[4] + i], mask[index_[7] + i]);
            dst(row_index + 2, col_index + 1)[i] = mix_6_to_1_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[7] + i]);
            dst(row_index + 3, col_index)[i] = mix_even(mask[index_[7] + i], mask[index_[3] + i]);
            dst(row_index + 3, col_index + 1)[i] = mix_5_to_3(mask[index_[7] + i], mask[index_[3] + i]);
            dst(row_index + 3, col_index + 2)[i] = mix_3_to_1(mask[index_[7] + i], mask[index_[4] + i]);
            dst(row_index + 3, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[7] + i]);
        }
        dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[5] + i]);
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_190(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        if (diff(mask[index_[1] + i], mask[index_[5] + i], threshold_[i])) {
            dst(row_index, col_index + 2)[i] = mask[index_[4] + i];
            dst(row_index, col_index + 3)[i] = mask[index_[4] + i];
            dst(row_index + 1, col_index + 2)[i] = mask[index_[4] + i];
            dst(row_index + 1, col_index + 3)[i] = mask[index_[4] + i];
            dst(row_index + 2, col_index + 3)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[7] + i]);
            dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[7] + i]);
        } else {
            dst(row_index, col_index + 2)[i] = mix_2_to_1_to_1(mask[index_[1] + i], mask[index_[4] + i], mask[index_[5] + i]);
            dst(row_index, col_index + 3)[i] = mix_even(mask[index_[1] + i], mask[index_[5] + i]);
            dst(row_index + 1, col_index + 2)[i] = mix_6_to_1_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[1] + i]);
            dst(row_index + 1, col_index + 3)[i] = mix_5_to_3(mask[index_[5] + i], mask[index_[1] + i]);
            dst(row_index + 2, col_index + 3)[i] = mix_3_to_1(mask[index_[5] + i], mask[index_[4] + i]);
            dst(row_index + 3, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[5] + i]);
        }
        dst(row_index + 1, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index + 2, col_index)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[7] + i]);
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_187(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        if (diff(mask[index_[3] + i], mask[index_[1] + i], threshold_[i])) {
            dst(row_index, col_index)[i] = mask[index_[4] + i];
            dst(row_index, col_index + 1)[i] = mask[index_[4] + i];
            dst(row_index + 1, col_index)[i] = mask[index_[4] + i];
            dst(row_index + 1, col_index + 1)[i] = mask[index_[4] + i];
            dst(row_index + 2, col_index)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[7] + i]);
            dst(row_index + 3, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[7] + i]);
        } else {
            dst(row_index, col_index)[i] = mix_even(mask[index_[1] + i], mask[index_[3] + i]);
            dst(row_index, col_index + 1)[i] = mix_2_to_1_to_1(mask[index_[1] + i], mask[index_[4] + i], mask[index_[3] + i]);
            dst(row_index + 1, col_index)[i] = mix_5_to_3(mask[index_[3] + i], mask[index_[1] + i]);
            dst(row_index + 1, col_index + 1)[i] = mix_6_to_1_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[1] + i]);
            dst(row_index + 2, col_index)[i] = mix_3_to_1(mask[index_[3] + i], mask[index_[4] + i]);
            dst(row_index + 3, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[3] + i]);
        }
        dst(row_index, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[7] + i]);
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_243(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 2, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[3] + i]);
        if (diff(mask[index_[5] + i], mask[index_[7] + i], threshold_[i])) {
            dst(row_index + 2, col_index + 2)[i] = mask[index_[4] + i];
            dst(row_index + 2, col_index + 3)[i] = mask[index_[4] + i];
            dst(row_index + 3, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[3] + i]);
            dst(row_index + 3, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[3] + i]);
            dst(row_index + 3, col_index + 2)[i] = mask[index_[4] + i];
            dst(row_index + 3, col_index + 3)[i] = mask[index_[4] + i];
        } else {
            dst(row_index + 2, col_index + 2)[i] = mix_6_to_1_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[7] + i]);
            dst(row_index + 2, col_index + 3)[i] = mix_2_to_1_to_1(mask[index_[5] + i], mask[index_[4] + i], mask[index_[7] + i]);
            dst(row_index + 3, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[7] + i]);
            dst(row_index + 3, col_index + 1)[i] = mix_3_to_1(mask[index_[7] + i], mask[index_[4] + i]);
            dst(row_index + 3, col_index + 2)[i] = mix_5_to_3(mask[index_[7] + i], mask[index_[5] + i]);
            dst(row_index + 3, col_index + 3)[i] = mix_even(mask[index_[7] + i], mask[index_[5] + i]);
        }
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_119(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        if (diff(mask[index_[1] + i], mask[index_[5] + i], threshold_[i])) {
            dst(row_index, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[3] + i]);
            dst(row_index, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[3] + i]);
            dst(row_index, col_index + 2)[i] = mask[index_[4] + i];
            dst(row_index, col_index + 3)[i] = mask[index_[4] + i];
            dst(row_index + 1, col_index + 2)[i] = mask[index_[4] + i];
            dst(row_index + 1, col_index + 3)[i] = mask[index_[4] + i];
        } else {
            dst(row_index, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[1] + i]);
            dst(row_index, col_index + 1)[i] = mix_3_to_1(mask[index_[1] + i], mask[index_[4] + i]);
            dst(row_index, col_index + 2)[i] = mix_5_to_3(mask[index_[1] + i], mask[index_[5] + i]);
            dst(row_index, col_index + 3)[i] = mix_even(mask[index_[1] + i], mask[index_[5] + i]);
            dst(row_index + 1, col_index + 2)[i] = mix_6_to_1_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[1] + i]);
            dst(row_index + 1, col_index + 3)[i] = mix_2_to_1_to_1(mask[index_[5] + i], mask[index_[4] + i], mask[index_[1] + i]);
        }
        dst(row_index + 1, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 2, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[8] + i]);
    }
}

template<typename T>
void hqx4xInterpolate<T>::interpolate_233(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index, col_index + 1)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index, col_index + 2)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[5] + i]);
        dst(row_index, col_index + 3)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[5] + i]);
        dst(row_index + 1, col_index)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 2)[i] = mix_6_to_1_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[1] + i]);
        dst(row_index + 2, col_index)[i] = mask[index_[4] + i];
        dst(row_index + 2, col_index + 1)[i] = mask[index_[4] + i];
        dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[5] + i]);
        if (diff(mask[index_[7] + i], mask[index_[3] + i], threshold_[i])) {
            dst(row_index + 3, col_index)[i] = mask[index_[4] + i];
        } else {
            dst(row_index + 3, col_index)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[3] + i]);
        }
        dst(row_index + 3, col_index + 1)[i] = mask[index_[4] + i];
        dst(row_index + 3, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[5] + i]);
    }
}

template<typename T>
void hqx4xInterpolate<T>::interpolate_47(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        if (diff(mask[index_[3] + i], mask[index_[1] + i], threshold_[i])) {
            dst(row_index, col_index)[i] = mask[index_[4] + i];
        } else {
            dst(row_index, col_index)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[3] + i]);
        }
        dst(row_index, col_index + 1)[i] = mask[index_[4] + i];
        dst(row_index, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 1, col_index)[i] = mask[index_[4] + i];
        dst(row_index + 1, col_index + 1)[i] = mask[index_[4] + i];
        dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 2, col_index)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 2)[i] = mix_6_to_1_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[5] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[5] + i]);
    }
}

template<typename T>
void hqx4xInterpolate<T>::interpolate_151(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index, col_index + 2)[i] = mask[index_[4] + i];
        if (diff(mask[index_[1] + i], mask[index_[5] + i], threshold_[i])) {
            dst(row_index, col_index + 3)[i] = mask[index_[4] + i];
        } else {
            dst(row_index, col_index + 3)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[5] + i]);
        }
        dst(row_index + 1, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 1, col_index + 2)[i] = mask[index_[4] + i];
        dst(row_index + 1, col_index + 3)[i] = mask[index_[4] + i];
        dst(row_index + 2, col_index)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_6_to_1_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[3] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[3] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[7] + i]);
    }
}

template<typename T>
void hqx4xInterpolate<T>::interpolate_244(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[3] + i]);
        dst(row_index, col_index + 1)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[3] + i]);
        dst(row_index, col_index + 2)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_6_to_1_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index + 2, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 2, col_index + 2)[i] = mask[index_[4] + i];
        dst(row_index + 2, col_index + 3)[i] = mask[index_[4] + i];
        dst(row_index + 3, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 3, col_index + 2)[i] = mask[index_[4] + i];
        if (diff(mask[index_[5] + i], mask[index_[7] + i], threshold_[i])) {
            dst(row_index + 3, col_index + 3)[i] = mask[index_[4] + i];
        } else {
            dst(row_index + 3, col_index + 3)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[5] + i]);
        }
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_250(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        if (diff(mask[index_[7] + i], mask[index_[3] + i], threshold_[i])) {
            dst(row_index + 2, col_index)[i] = mask[index_[4] + i];
            dst(row_index + 3, col_index)[i] = mask[index_[4] + i];
            dst(row_index + 3, col_index + 1)[i] = mask[index_[4] + i];
        } else {
            dst(row_index + 2, col_index)[i] = mix_even(mask[index_[3] + i], mask[index_[4] + i]);
            dst(row_index + 3, col_index)[i] = mix_even(mask[index_[7] + i], mask[index_[3] + i]);
            dst(row_index + 3, col_index + 1)[i] = mix_even(mask[index_[7] + i], mask[index_[4] + i]);
        }
        dst(row_index + 2, col_index + 1)[i] = mask[index_[4] + i];
        dst(row_index + 2, col_index + 2)[i] = mask[index_[4] + i];
        if (diff(mask[index_[5] + i], mask[index_[7] + i], threshold_[i])) {
            dst(row_index + 2, col_index + 3)[i] = mask[index_[4] + i];
            dst(row_index + 3, col_index + 2)[i] = mask[index_[4] + i];
            dst(row_index + 3, col_index + 3)[i] = mask[index_[4] + i];
        } else {
            dst(row_index + 2, col_index + 3)[i] = mix_even(mask[index_[5] + i], mask[index_[4] + i]);
            dst(row_index + 3, col_index + 2)[i] = mix_even(mask[index_[7] + i], mask[index_[4] + i]);
            dst(row_index + 3, col_index + 3)[i] = mix_even(mask[index_[7] + i], mask[index_[5] + i]);
        }
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_123(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        if (diff(mask[index_[3] + i], mask[index_[1] + i], threshold_[i])) {
            dst(row_index, col_index)[i] = mask[index_[4] + i];
            dst(row_index, col_index + 1)[i] = mask[index_[4] + i];
            dst(row_index + 1, col_index)[i] = mask[index_[4] + i];
        } else {
            dst(row_index, col_index)[i] = mix_even(mask[index_[1] + i], mask[index_[3] + i]);
            dst(row_index, col_index + 1)[i] = mix_even(mask[index_[1] + i], mask[index_[4] + i]);
            dst(row_index + 1, col_index)[i] = mix_even(mask[index_[3] + i], mask[index_[4] + i]);
        }
        dst(row_index, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index + 1)[i] = mask[index_[4] + i];
        dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        if (diff(mask[index_[7] + i], mask[index_[3] + i], threshold_[i])) {
            dst(row_index + 2, col_index)[i] = mask[index_[4] + i];
            dst(row_index + 3, col_index)[i] = mask[index_[4] + i];
            dst(row_index + 3, col_index + 1)[i] = mask[index_[4] + i];
        } else {
            dst(row_index + 2, col_index)[i] = mix_even(mask[index_[3] + i], mask[index_[4] + i]);
            dst(row_index + 3, col_index)[i] = mix_even(mask[index_[7] + i], mask[index_[3] + i]);
            dst(row_index + 3, col_index + 1)[i] = mix_even(mask[index_[7] + i], mask[index_[4] + i]);
        }
        dst(row_index + 2, col_index + 1)[i] = mask[index_[4] + i];
        dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[8] + i]);
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_95(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        if (diff(mask[index_[3] + i], mask[index_[1] + i], threshold_[i])) {
            dst(row_index, col_index)[i] = mask[index_[4] + i];
            dst(row_index, col_index + 1)[i] = mask[index_[4] + i];
            dst(row_index + 1, col_index)[i] = mask[index_[4] + i];
        } else {
            dst(row_index, col_index)[i] = mix_even(mask[index_[1] + i], mask[index_[3] + i]);
            dst(row_index, col_index + 1)[i] = mix_even(mask[index_[1] + i], mask[index_[4] + i]);
            dst(row_index + 1, col_index)[i] = mix_even(mask[index_[3] + i], mask[index_[4] + i]);
        }
        if (diff(mask[index_[1] + i], mask[index_[5] + i], threshold_[i])) {
            dst(row_index, col_index + 2)[i] = mask[index_[4] + i];
            dst(row_index, col_index + 3)[i] = mask[index_[4] + i];
            dst(row_index + 1, col_index + 3)[i] = mask[index_[4] + i];
        } else {
            dst(row_index, col_index + 2)[i] = mix_even(mask[index_[1] + i], mask[index_[4] + i]);
            dst(row_index, col_index + 3)[i] = mix_even(mask[index_[1] + i], mask[index_[5] + i]);
            dst(row_index + 1, col_index + 3)[i] = mix_even(mask[index_[5] + i], mask[index_[4] + i]);
        }
        dst(row_index + 1, col_index + 1)[i] = mask[index_[4] + i];
        dst(row_index + 1, col_index + 2)[i] = mask[index_[4] + i];
        dst(row_index + 2, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[8] + i]);
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_222(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        if (diff(mask[index_[1] + i], mask[index_[5] + i], threshold_[i])) {
            dst(row_index, col_index + 2)[i] = mask[index_[4] + i];
            dst(row_index, col_index + 3)[i] = mask[index_[4] + i];
            dst(row_index + 1, col_index + 3)[i] = mask[index_[4] + i];
        } else {
            dst(row_index, col_index + 2)[i] = mix_even(mask[index_[1] + i], mask[index_[4] + i]);
            dst(row_index, col_index + 3)[i] = mix_even(mask[index_[1] + i], mask[index_[5] + i]);
            dst(row_index + 1, col_index + 3)[i] = mix_even(mask[index_[5] + i], mask[index_[4] + i]);
        }
        dst(row_index + 1, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index + 1, col_index + 2)[i] = mask[index_[4] + i];
        dst(row_index + 2, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 2, col_index + 2)[i] = mask[index_[4] + i];
        if (diff(mask[index_[5] + i], mask[index_[7] + i], threshold_[i])) {
            dst(row_index + 2, col_index + 3)[i] = mask[index_[4] + i];
            dst(row_index + 3, col_index + 2)[i] = mask[index_[4] + i];
            dst(row_index + 3, col_index + 3)[i] = mask[index_[4] + i];
        } else {
            dst(row_index + 2, col_index + 3)[i] = mix_even(mask[index_[5] + i], mask[index_[4] + i]);
            dst(row_index + 3, col_index + 2)[i] = mix_even(mask[index_[7] + i], mask[index_[4] + i]);
            dst(row_index + 3, col_index + 3)[i] = mix_even(mask[index_[7] + i], mask[index_[5] + i]);
        }
        dst(row_index + 3, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[6] + i]);
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_252(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index, col_index + 1)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[0] + i]);
        dst(row_index, col_index + 2)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[1] + i]);
        if (diff(mask[index_[7] + i], mask[index_[3] + i], threshold_[i])) {
            dst(row_index + 2, col_index)[i] = mask[index_[4] + i];
            dst(row_index + 3, col_index)[i] = mask[index_[4] + i];
            dst(row_index + 3, col_index + 1)[i] = mask[index_[4] + i];
        } else {
            dst(row_index + 2, col_index)[i] = mix_even(mask[index_[3] + i], mask[index_[4] + i]);
            dst(row_index + 3, col_index)[i] = mix_even(mask[index_[7] + i], mask[index_[3] + i]);
            dst(row_index + 3, col_index + 1)[i] = mix_even(mask[index_[7] + i], mask[index_[4] + i]);
        }
        dst(row_index + 2, col_index + 1)[i] = mask[index_[4] + i];
        dst(row_index + 2, col_index + 2)[i] = mask[index_[4] + i];
        dst(row_index + 2, col_index + 3)[i] = mask[index_[4] + i];
        dst(row_index + 3, col_index + 2)[i] = mask[index_[4] + i];
        if (diff(mask[index_[5] + i], mask[index_[7] + i], threshold_[i])) {
            dst(row_index + 3, col_index + 3)[i] = mask[index_[4] + i];
        } else {
            dst(row_index + 3, col_index + 3)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[5] + i]);
        }
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_249(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index, col_index + 1)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index, col_index + 2)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[2] + i]);
        dst(row_index, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 2, col_index)[i] = mask[index_[4] + i];
        dst(row_index + 2, col_index + 1)[i] = mask[index_[4] + i];
        dst(row_index + 2, col_index + 2)[i] = mask[index_[4] + i];
        if (diff(mask[index_[5] + i], mask[index_[7] + i], threshold_[i])) {
            dst(row_index + 2, col_index + 3)[i] = mask[index_[4] + i];
            dst(row_index + 3, col_index + 2)[i] = mask[index_[4] + i];
            dst(row_index + 3, col_index + 3)[i] = mask[index_[4] + i];
        } else {
            dst(row_index + 2, col_index + 3)[i] = mix_even(mask[index_[5] + i], mask[index_[4] + i]);
            dst(row_index + 3, col_index + 2)[i] = mix_even(mask[index_[7] + i], mask[index_[4] + i]);
            dst(row_index + 3, col_index + 3)[i] = mix_even(mask[index_[7] + i], mask[index_[5] + i]);
        }
        if (diff(mask[index_[7] + i], mask[index_[3] + i], threshold_[i])) {
            dst(row_index + 3, col_index)[i] = mask[index_[4] + i];
        } else {
            dst(row_index + 3, col_index)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[3] + i]);
        }
        dst(row_index + 3, col_index + 1)[i] = mask[index_[4] + i];
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_235(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        if (diff(mask[index_[3] + i], mask[index_[1] + i], threshold_[i])) {
            dst(row_index, col_index)[i] = mask[index_[4] + i];
            dst(row_index, col_index + 1)[i] = mask[index_[4] + i];
            dst(row_index + 1, col_index)[i] = mask[index_[4] + i];
        } else {
            dst(row_index, col_index)[i] = mix_even(mask[index_[1] + i], mask[index_[3] + i]);
            dst(row_index, col_index + 1)[i] = mix_even(mask[index_[1] + i], mask[index_[4] + i]);
            dst(row_index + 1, col_index)[i] = mix_even(mask[index_[3] + i], mask[index_[4] + i]);
        }
        dst(row_index, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index + 1)[i] = mask[index_[4] + i];
        dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[2] + i]);
        dst(row_index + 2, col_index)[i] = mask[index_[4] + i];
        dst(row_index + 2, col_index + 1)[i] = mask[index_[4] + i];
        dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[5] + i]);
        if (diff(mask[index_[7] + i], mask[index_[3] + i], threshold_[i])) {
            dst(row_index + 3, col_index)[i] = mask[index_[4] + i];
        } else {
            dst(row_index + 3, col_index)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[3] + i]);
        }
        dst(row_index + 3, col_index + 1)[i] = mask[index_[4] + i];
        dst(row_index + 3, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[5] + i]);
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_111(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        if (diff(mask[index_[3] + i], mask[index_[1] + i], threshold_[i])) {
            dst(row_index, col_index)[i] = mask[index_[4] + i];
        } else {
            dst(row_index, col_index)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[3] + i]);
        }
        dst(row_index, col_index + 1)[i] = mask[index_[4] + i];
        dst(row_index, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 1, col_index)[i] = mask[index_[4] + i];
        dst(row_index + 1, col_index + 1)[i] = mask[index_[4] + i];
        dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[5] + i]);
        if (diff(mask[index_[7] + i], mask[index_[3] + i], threshold_[i])) {
            dst(row_index + 2, col_index)[i] = mask[index_[4] + i];
            dst(row_index + 3, col_index)[i] = mask[index_[4] + i];
            dst(row_index + 3, col_index + 1)[i] = mask[index_[4] + i];
        } else {
            dst(row_index + 2, col_index)[i] = mix_even(mask[index_[3] + i], mask[index_[4] + i]);
            dst(row_index + 3, col_index)[i] = mix_even(mask[index_[7] + i], mask[index_[3] + i]);
            dst(row_index + 3, col_index + 1)[i] = mix_even(mask[index_[7] + i], mask[index_[4] + i]);
        }
        dst(row_index + 2, col_index + 1)[i] = mask[index_[4] + i];
        dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[5] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[8] + i]);
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_63(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        if (diff(mask[index_[3] + i], mask[index_[1] + i], threshold_[i])) {
            dst(row_index, col_index)[i] = mask[index_[4] + i];
        } else {
            dst(row_index, col_index)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[3] + i]);
        }
        dst(row_index, col_index + 1)[i] = mask[index_[4] + i];
        if (diff(mask[index_[1] + i], mask[index_[5] + i], threshold_[i])) {
            dst(row_index, col_index + 2)[i] = mask[index_[4] + i];
            dst(row_index, col_index + 3)[i] = mask[index_[4] + i];
            dst(row_index + 1, col_index + 3)[i] = mask[index_[4] + i];
        } else {
            dst(row_index, col_index + 2)[i] = mix_even(mask[index_[1] + i], mask[index_[4] + i]);
            dst(row_index, col_index + 3)[i] = mix_even(mask[index_[1] + i], mask[index_[5] + i]);
            dst(row_index + 1, col_index + 3)[i] = mix_even(mask[index_[5] + i], mask[index_[4] + i]);
        }
        dst(row_index + 1, col_index)[i] = mask[index_[4] + i];
        dst(row_index + 1, col_index + 1)[i] = mask[index_[4] + i];
        dst(row_index + 1, col_index + 2)[i] = mask[index_[4] + i];
        dst(row_index + 2, col_index)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[8] + i]);
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_159(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        if (diff(mask[index_[3] + i], mask[index_[1] + i], threshold_[i])) {
            dst(row_index, col_index)[i] = mask[index_[4] + i];
            dst(row_index, col_index + 1)[i] = mask[index_[4] + i];
            dst(row_index + 1, col_index)[i] = mask[index_[4] + i];
        } else {
            dst(row_index, col_index)[i] = mix_even(mask[index_[1] + i], mask[index_[3] + i]);
            dst(row_index, col_index + 1)[i] = mix_even(mask[index_[1] + i], mask[index_[4] + i]);
            dst(row_index + 1, col_index)[i] = mix_even(mask[index_[3] + i], mask[index_[4] + i]);
        }
        dst(row_index, col_index + 2)[i] = mask[index_[4] + i];
        if (diff(mask[index_[1] + i], mask[index_[5] + i], threshold_[i])) {
            dst(row_index, col_index + 3)[i] = mask[index_[4] + i];
        } else {
            dst(row_index, col_index + 3)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[5] + i]);
        }
        dst(row_index + 1, col_index + 1)[i] = mask[index_[4] + i];
        dst(row_index + 1, col_index + 2)[i] = mask[index_[4] + i];
        dst(row_index + 1, col_index + 3)[i] = mask[index_[4] + i];
        dst(row_index + 2, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[6] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[7] + i]);
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_215(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index, col_index + 2)[i] = mask[index_[4] + i];
        if (diff(mask[index_[1] + i], mask[index_[5] + i], threshold_[i])) {
            dst(row_index, col_index + 3)[i] = mask[index_[4] + i];
        } else {
            dst(row_index, col_index + 3)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[5] + i]);
        }
        dst(row_index + 1, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 1, col_index + 2)[i] = mask[index_[4] + i];
        dst(row_index + 1, col_index + 3)[i] = mask[index_[4] + i];
        dst(row_index + 2, col_index)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[6] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 2, col_index + 2)[i] = mask[index_[4] + i];
        if (diff(mask[index_[5] + i], mask[index_[7] + i], threshold_[i])) {
            dst(row_index + 2, col_index + 3)[i] = mask[index_[4] + i];
            dst(row_index + 3, col_index + 2)[i] = mask[index_[4] + i];
            dst(row_index + 3, col_index + 3)[i] = mask[index_[4] + i];
        } else {
            dst(row_index + 2, col_index + 3)[i] = mix_even(mask[index_[5] + i], mask[index_[4] + i]);
            dst(row_index + 3, col_index + 2)[i] = mix_even(mask[index_[7] + i], mask[index_[4] + i]);
            dst(row_index + 3, col_index + 3)[i] = mix_even(mask[index_[7] + i], mask[index_[5] + i]);
        }
        dst(row_index + 3, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[6] + i]);
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_246(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        if (diff(mask[index_[1] + i], mask[index_[5] + i], threshold_[i])) {
            dst(row_index, col_index + 2)[i] = mask[index_[4] + i];
            dst(row_index, col_index + 3)[i] = mask[index_[4] + i];
            dst(row_index + 1, col_index + 3)[i] = mask[index_[4] + i];
        } else {
            dst(row_index, col_index + 2)[i] = mix_even(mask[index_[1] + i], mask[index_[4] + i]);
            dst(row_index, col_index + 3)[i] = mix_even(mask[index_[1] + i], mask[index_[5] + i]);
            dst(row_index + 1, col_index + 3)[i] = mix_even(mask[index_[5] + i], mask[index_[4] + i]);
        }
        dst(row_index + 1, col_index)[i] = mix_4_to_2_to_1(mask[index_[4] + i], mask[index_[3] + i], mask[index_[0] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index + 1, col_index + 2)[i] = mask[index_[4] + i];
        dst(row_index + 2, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 2, col_index + 2)[i] = mask[index_[4] + i];
        dst(row_index + 2, col_index + 3)[i] = mask[index_[4] + i];
        dst(row_index + 3, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 3, col_index + 2)[i] = mask[index_[4] + i];
        if (diff(mask[index_[5] + i], mask[index_[7] + i], threshold_[i])) {
            dst(row_index + 3, col_index + 3)[i] = mask[index_[4] + i];
        } else {
            dst(row_index + 3, col_index + 3)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[5] + i]);
        }
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_254(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        if (diff(mask[index_[1] + i], mask[index_[5] + i], threshold_[i])) {
            dst(row_index, col_index + 2)[i] = mask[index_[4] + i];
            dst(row_index, col_index + 3)[i] = mask[index_[4] + i];
            dst(row_index + 1, col_index + 3)[i] = mask[index_[4] + i];
        } else {
            dst(row_index, col_index + 2)[i] = mix_even(mask[index_[1] + i], mask[index_[4] + i]);
            dst(row_index, col_index + 3)[i] = mix_even(mask[index_[1] + i], mask[index_[5] + i]);
            dst(row_index + 1, col_index + 3)[i] = mix_even(mask[index_[5] + i], mask[index_[4] + i]);
        }
        dst(row_index + 1, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[0] + i]);
        dst(row_index + 1, col_index + 2)[i] = mask[index_[4] + i];
        if (diff(mask[index_[7] + i], mask[index_[3] + i], threshold_[i])) {
            dst(row_index + 2, col_index)[i] = mask[index_[4] + i];
            dst(row_index + 3, col_index)[i] = mask[index_[4] + i];
            dst(row_index + 3, col_index + 1)[i] = mask[index_[4] + i];
        } else {
            dst(row_index + 2, col_index)[i] = mix_even(mask[index_[3] + i], mask[index_[4] + i]);
            dst(row_index + 3, col_index)[i] = mix_even(mask[index_[7] + i], mask[index_[3] + i]);
            dst(row_index + 3, col_index + 1)[i] = mix_even(mask[index_[7] + i], mask[index_[4] + i]);
        }
        dst(row_index + 2, col_index + 1)[i] = mask[index_[4] + i];
        dst(row_index + 2, col_index + 2)[i] = mask[index_[4] + i];
        dst(row_index + 2, col_index + 3)[i] = mask[index_[4] + i];
        dst(row_index + 3, col_index + 2)[i] = mask[index_[4] + i];
        if (diff(mask[index_[5] + i], mask[index_[7] + i], threshold_[i])) {
            dst(row_index + 3, col_index + 3)[i] = mask[index_[4] + i];
        } else {
            dst(row_index + 3, col_index + 3)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[5] + i]);
        }
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_253(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index, col_index + 1)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index, col_index + 2)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[1] + i]);
        dst(row_index + 2, col_index)[i] = mask[index_[4] + i];
        dst(row_index + 2, col_index + 1)[i] = mask[index_[4] + i];
        dst(row_index + 2, col_index + 2)[i] = mask[index_[4] + i];
        dst(row_index + 2, col_index + 3)[i] = mask[index_[4] + i];
        if (diff(mask[index_[7] + i], mask[index_[3] + i], threshold_[i])) {
            dst(row_index + 3, col_index)[i] = mask[index_[4] + i];
        } else {
            dst(row_index + 3, col_index)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[3] + i]);
        }
        dst(row_index + 3, col_index + 1)[i] = mask[index_[4] + i];
        dst(row_index + 3, col_index + 2)[i] = mask[index_[4] + i];
        if (diff(mask[index_[5] + i], mask[index_[7] + i], threshold_[i])) {
            dst(row_index + 3, col_index + 3)[i] = mask[index_[4] + i];
        } else {
            dst(row_index + 3, col_index + 3)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[5] + i]);
        }
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_251(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        if (diff(mask[index_[3] + i], mask[index_[1] + i], threshold_[i])) {
            dst(row_index, col_index)[i] = mask[index_[4] + i];
            dst(row_index, col_index + 1)[i] = mask[index_[4] + i];
            dst(row_index + 1, col_index)[i] = mask[index_[4] + i];
        } else {
            dst(row_index, col_index)[i] = mix_even(mask[index_[1] + i], mask[index_[3] + i]);
            dst(row_index, col_index + 1)[i] = mix_even(mask[index_[1] + i], mask[index_[4] + i]);
            dst(row_index + 1, col_index)[i] = mix_even(mask[index_[3] + i], mask[index_[4] + i]);
        }
        dst(row_index, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index + 1)[i] = mask[index_[4] + i];
        dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[2] + i]);
        dst(row_index + 2, col_index)[i] = mask[index_[4] + i];
        dst(row_index + 2, col_index + 1)[i] = mask[index_[4] + i];
        dst(row_index + 2, col_index + 2)[i] = mask[index_[4] + i];
        if (diff(mask[index_[5] + i], mask[index_[7] + i], threshold_[i])) {
            dst(row_index + 2, col_index + 3)[i] = mask[index_[4] + i];
            dst(row_index + 3, col_index + 2)[i] = mask[index_[4] + i];
            dst(row_index + 3, col_index + 3)[i] = mask[index_[4] + i];
        } else {
            dst(row_index + 2, col_index + 3)[i] = mix_even(mask[index_[5] + i], mask[index_[4] + i]);
            dst(row_index + 3, col_index + 2)[i] = mix_even(mask[index_[7] + i], mask[index_[4] + i]);
            dst(row_index + 3, col_index + 3)[i] = mix_even(mask[index_[7] + i], mask[index_[5] + i]);
        }
        if (diff(mask[index_[7] + i], mask[index_[3] + i], threshold_[i])) {
            dst(row_index + 3, col_index)[i] = mask[index_[4] + i];
        } else {
            dst(row_index + 3, col_index)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[3] + i]);
        }
        dst(row_index + 3, col_index + 1)[i] = mask[index_[4] + i];
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_239(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        if (diff(mask[index_[3] + i], mask[index_[1] + i], threshold_[i])) {
            dst(row_index, col_index)[i] = mask[index_[4] + i];
        } else {
            dst(row_index, col_index)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[3] + i]);
        }
        dst(row_index, col_index + 1)[i] = mask[index_[4] + i];
        dst(row_index, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 1, col_index)[i] = mask[index_[4] + i];
        dst(row_index + 1, col_index + 1)[i] = mask[index_[4] + i];
        dst(row_index + 1, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 1, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 2, col_index)[i] = mask[index_[4] + i];
        dst(row_index + 2, col_index + 1)[i] = mask[index_[4] + i];
        dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[5] + i]);
        if (diff(mask[index_[7] + i], mask[index_[3] + i], threshold_[i])) {
            dst(row_index + 3, col_index)[i] = mask[index_[4] + i];
        } else {
            dst(row_index + 3, col_index)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[3] + i]);
        }
        dst(row_index + 3, col_index + 1)[i] = mask[index_[4] + i];
        dst(row_index + 3, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[5] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[5] + i]);
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_127(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        if (diff(mask[index_[3] + i], mask[index_[1] + i], threshold_[i])) {
            dst(row_index, col_index)[i] = mask[index_[4] + i];
        } else {
            dst(row_index, col_index)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[3] + i]);
        }
        dst(row_index, col_index + 1)[i] = mask[index_[4] + i];
        if (diff(mask[index_[1] + i], mask[index_[5] + i], threshold_[i])) {
            dst(row_index, col_index + 2)[i] = mask[index_[4] + i];
            dst(row_index, col_index + 3)[i] = mask[index_[4] + i];
            dst(row_index + 1, col_index + 3)[i] = mask[index_[4] + i];
        } else {
            dst(row_index, col_index + 2)[i] = mix_even(mask[index_[1] + i], mask[index_[4] + i]);
            dst(row_index, col_index + 3)[i] = mix_even(mask[index_[1] + i], mask[index_[5] + i]);
            dst(row_index + 1, col_index + 3)[i] = mix_even(mask[index_[5] + i], mask[index_[4] + i]);
        }
        dst(row_index + 1, col_index)[i] = mask[index_[4] + i];
        dst(row_index + 1, col_index + 1)[i] = mask[index_[4] + i];
        dst(row_index + 1, col_index + 2)[i] = mask[index_[4] + i];
        if (diff(mask[index_[7] + i], mask[index_[3] + i], threshold_[i])) {
            dst(row_index + 2, col_index)[i] = mask[index_[4] + i];
            dst(row_index + 3, col_index)[i] = mask[index_[4] + i];
            dst(row_index + 3, col_index + 1)[i] = mask[index_[4] + i];
        } else {
            dst(row_index + 2, col_index)[i] = mix_even(mask[index_[3] + i], mask[index_[4] + i]);
            dst(row_index + 3, col_index)[i] = mix_even(mask[index_[7] + i], mask[index_[3] + i]);
            dst(row_index + 3, col_index + 1)[i] = mix_even(mask[index_[7] + i], mask[index_[4] + i]);
        }
        dst(row_index + 2, col_index + 1)[i] = mask[index_[4] + i];
        dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[8] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[8] + i]);
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_191(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        if (diff(mask[index_[3] + i], mask[index_[1] + i], threshold_[i])) {
            dst(row_index, col_index)[i] = mask[index_[4] + i];
        } else {
            dst(row_index, col_index)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[3] + i]);
        }
        dst(row_index, col_index + 1)[i] = mask[index_[4] + i];
        dst(row_index, col_index + 2)[i] = mask[index_[4] + i];
        if (diff(mask[index_[1] + i], mask[index_[5] + i], threshold_[i])) {
            dst(row_index, col_index + 3)[i] = mask[index_[4] + i];
        } else {
            dst(row_index, col_index + 3)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[5] + i]);
        }
        dst(row_index + 1, col_index)[i] = mask[index_[4] + i];
        dst(row_index + 1, col_index + 1)[i] = mask[index_[4] + i];
        dst(row_index + 1, col_index + 2)[i] = mask[index_[4] + i];
        dst(row_index + 1, col_index + 3)[i] = mask[index_[4] + i];
        dst(row_index + 2, col_index)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 2)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 2, col_index + 3)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index + 2)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[7] + i]);
        dst(row_index + 3, col_index + 3)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[7] + i]);
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_223(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        if (diff(mask[index_[3] + i], mask[index_[1] + i], threshold_[i])) {
            dst(row_index, col_index)[i] = mask[index_[4] + i];
            dst(row_index, col_index + 1)[i] = mask[index_[4] + i];
            dst(row_index + 1, col_index)[i] = mask[index_[4] + i];
        } else {
            dst(row_index, col_index)[i] = mix_even(mask[index_[1] + i], mask[index_[3] + i]);
            dst(row_index, col_index + 1)[i] = mix_even(mask[index_[1] + i], mask[index_[4] + i]);
            dst(row_index + 1, col_index)[i] = mix_even(mask[index_[3] + i], mask[index_[4] + i]);
        }
        dst(row_index, col_index + 2)[i] = mask[index_[4] + i];
        if (diff(mask[index_[1] + i], mask[index_[5] + i], threshold_[i])) {
            dst(row_index, col_index + 3)[i] = mask[index_[4] + i];
        } else {
            dst(row_index, col_index + 3)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[5] + i]);
        }
        dst(row_index + 1, col_index + 1)[i] = mask[index_[4] + i];
        dst(row_index + 1, col_index + 2)[i] = mask[index_[4] + i];
        dst(row_index + 1, col_index + 3)[i] = mask[index_[4] + i];
        dst(row_index + 2, col_index)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 2, col_index + 2)[i] = mask[index_[4] + i];
        if (diff(mask[index_[5] + i], mask[index_[7] + i], threshold_[i])) {
            dst(row_index + 2, col_index + 3)[i] = mask[index_[4] + i];
            dst(row_index + 3, col_index + 2)[i] = mask[index_[4] + i];
            dst(row_index + 3, col_index + 3)[i] = mask[index_[4] + i];
        } else {
            dst(row_index + 2, col_index + 3)[i] = mix_even(mask[index_[5] + i], mask[index_[4] + i]);
            dst(row_index + 3, col_index + 2)[i] = mix_even(mask[index_[7] + i], mask[index_[4] + i]);
            dst(row_index + 3, col_index + 3)[i] = mix_even(mask[index_[7] + i], mask[index_[5] + i]);
        }
        dst(row_index + 3, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[6] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_3_to_1(mask[index_[4] + i], mask[index_[6] + i]);
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_247(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        dst(row_index, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index, col_index + 2)[i] = mask[index_[4] + i];
        if (diff(mask[index_[1] + i], mask[index_[5] + i], threshold_[i])) {
            dst(row_index, col_index + 3)[i] = mask[index_[4] + i];
        } else {
            dst(row_index, col_index + 3)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[5] + i]);
        }
        dst(row_index + 1, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 1, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 1, col_index + 2)[i] = mask[index_[4] + i];
        dst(row_index + 1, col_index + 3)[i] = mask[index_[4] + i];
        dst(row_index + 2, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 2, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 2, col_index + 2)[i] = mask[index_[4] + i];
        dst(row_index + 2, col_index + 3)[i] = mask[index_[4] + i];
        dst(row_index + 3, col_index)[i] = mix_5_to_3(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 3, col_index + 1)[i] = mix_7_to_1(mask[index_[4] + i], mask[index_[3] + i]);
        dst(row_index + 3, col_index + 2)[i] = mask[index_[4] + i];
        if (diff(mask[index_[5] + i], mask[index_[7] + i], threshold_[i])) {
            dst(row_index + 3, col_index + 3)[i] = mask[index_[4] + i];
        } else {
            dst(row_index + 3, col_index + 3)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[5] + i]);
        }
    }
}
template<typename T>
void hqx4xInterpolate<T>::interpolate_255(cv::Mat_<T> &dst, channel_type mask[], int row, int col)
{
    int const col_index = col * 4;
    int const row_index = row * 4;
    for(int i = 0; i != cv::DataType<T>::channels; ++i)
    {
        if (diff(mask[index_[3] + i], mask[index_[1] + i], threshold_[i])) {
            dst(row_index, col_index)[i] = mask[index_[4] + i];
        } else {
            dst(row_index, col_index)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[3] + i]);
        }
        dst(row_index, col_index + 1)[i] = mask[index_[4] + i];
        dst(row_index, col_index + 2)[i] = mask[index_[4] + i];
        if (diff(mask[index_[1] + i], mask[index_[5] + i], threshold_[i])) {
            dst(row_index, col_index + 3)[i] = mask[index_[4] + i];
        } else {
            dst(row_index, col_index + 3)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[1] + i], mask[index_[5] + i]);
        }
        dst(row_index + 1, col_index)[i] = mask[index_[4] + i];
        dst(row_index + 1, col_index + 1)[i] = mask[index_[4] + i];
        dst(row_index + 1, col_index + 2)[i] = mask[index_[4] + i];
        dst(row_index + 1, col_index + 3)[i] = mask[index_[4] + i];
        dst(row_index + 2, col_index)[i] = mask[index_[4] + i];
        dst(row_index + 2, col_index + 1)[i] = mask[index_[4] + i];
        dst(row_index + 2, col_index + 2)[i] = mask[index_[4] + i];
        dst(row_index + 2, col_index + 3)[i] = mask[index_[4] + i];
        if (diff(mask[index_[7] + i], mask[index_[3] + i], threshold_[i])) {
            dst(row_index + 3, col_index)[i] = mask[index_[4] + i];
        } else {
            dst(row_index + 3, col_index)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[3] + i]);
        }
        dst(row_index + 3, col_index + 1)[i] = mask[index_[4] + i];
        dst(row_index + 3, col_index + 2)[i] = mask[index_[4] + i];
        if (diff(mask[index_[5] + i], mask[index_[7] + i], threshold_[i])) {
            dst(row_index + 3, col_index + 3)[i] = mask[index_[4] + i];
        } else {
            dst(row_index + 3, col_index + 3)[i] = mix_2_to_1_to_1(mask[index_[4] + i], mask[index_[7] + i], mask[index_[5] + i]);
        }
    }
}

}

#endif // HQX4XINTERPOLATE_HPP
