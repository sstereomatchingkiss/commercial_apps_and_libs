#ifndef HQXALGO_HPP
#define HQXALGO_HPP

/*
 * implement hq2x, hq3x and hq4x algorithm, there are many
 * things could optimize.
 */
#include <array>

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "../../utility/dimensionMapper.hpp"
#include "hqxAuxiliary.hpp"
#include "hqx2xInterpolate.hpp"
#include "hqx3xInterpolate.hpp"
#include "hqx4xInterpolate.hpp"
#include "metaProgrammingHelper.hpp"

namespace OCV{

template<typename ColorSpace = cv::Vec3i>
class hqxAlgo
{
public:
    typedef typename cv::DataType<ColorSpace>::channel_type channel_type;
    typedef typename cv::DataType<ColorSpace>::value_type   value_type;
    typedef typename cv::DataType<ColorSpace>::work_type    work_type;

    /*
     * enlarge the image by factor of 2, this function can specify threshold
     */
    cv::Mat_<ColorSpace> const hq2x_bgr(cv::Mat_<ColorSpace> const &src, channel_type threshold[]) const
    {        
        cv::Mat_<ColorSpace> dst(src.rows * 2, src.cols * 2);
        return hqx2x_selector(src, dst, threshold, int2Type<cv::DataType<ColorSpace>::channels>());
    }

    /*
     * enlarge the image by factor of 2
     */
    cv::Mat_<ColorSpace> const hq2x_bgr(cv::Mat_<ColorSpace> const &src) const
    {        
        cv::Mat_<ColorSpace> dst(src.rows * 2, src.cols * 2);
        return hqx2x_selector(src, dst, int2Type<cv::DataType<ColorSpace>::channels>());
    }

    /*
     * enlarge the image by factor of 3, this function can specify threshold
     */
    cv::Mat_<ColorSpace> const hq3x_bgr(cv::Mat_<ColorSpace> const &src, channel_type threshold[]) const
    {
        cv::Mat_<ColorSpace> dst(src.rows * 3, src.cols * 3);
        return hqx3x_selector(src, dst, threshold, int2Type<cv::DataType<ColorSpace>::channels>());
    }

    /*
     * enlarge the image by factor of 3
     */
    cv::Mat_<ColorSpace> const hq3x_bgr(cv::Mat_<ColorSpace> const &src) const
    {
        cv::Mat_<ColorSpace> dst(src.rows * 3, src.cols * 3);
        return hqx3x_selector(src, dst, int2Type<cv::DataType<ColorSpace>::channels>());
    }

    /*
     * enlarge the image by factor of 4, this function can specify threshold
     */
    cv::Mat_<ColorSpace> const hq4x_bgr(cv::Mat_<ColorSpace> const &src, channel_type threshold[]) const
    {
        cv::Mat_<ColorSpace> dst(src.rows * 4, src.cols * 4);
        return hqx4x_selector(src, dst, threshold, int2Type<cv::DataType<ColorSpace>::channels>());
    }

    /*
     * enlarge the image by factor of 4
     */
    cv::Mat_<ColorSpace> const hq4x_bgr(cv::Mat_<ColorSpace> const &src) const
    {
        cv::Mat_<ColorSpace> dst(src.rows * 4, src.cols * 4);
        return hqx4x_selector(src, dst, int2Type<cv::DataType<ColorSpace>::channels>());
    }

private:
    /*
     * implementation of hqx(2x, 3x and 4x)
     */
    template<typename Interpolate>
    cv::Mat_<ColorSpace> const hqx_bgr(cv::Mat_<ColorSpace> const &src, cv::Mat_<ColorSpace> &dst, channel_type threshold[], Interpolate func) const;

    /*
     * These sets of selector are designed for two color space, bgr and bgra.
     * Currently we do not support bgra yet, some selector are useless
     */

    cv::Mat_<ColorSpace> const hqx2x_selector(cv::Mat_<ColorSpace> const &src, cv::Mat_<ColorSpace> &dst, int2Type<3>) const
    {        
        channel_type threshold[] = {48, 7, 6};
        return hqx_bgr(src, dst, threshold, hqx2xInterpolate<ColorSpace>(std::array<channel_type, cv::DataType<ColorSpace>::channels>{{48, 7, 6}}));
    }

    cv::Mat_<ColorSpace> const hqx2x_selector(cv::Mat_<ColorSpace> const &src, cv::Mat_<ColorSpace> &dst, int2Type<4>) const
    {
        channel_type threshold[] = {48, 7, 6, 0};
        return hqx_bgr(src, dst, threshold, hqx2xInterpolate<ColorSpace>(std::array<channel_type, cv::DataType<ColorSpace>::channels>{{48, 7, 6, 0}}));
    }

    cv::Mat_<ColorSpace> const hqx2x_selector(cv::Mat_<ColorSpace> const &src, cv::Mat_<ColorSpace> &dst, channel_type threshold[], int2Type<3>) const
    {
        return hqx_bgr(src, dst, threshold, hqx2xInterpolate<ColorSpace>(std::array<channel_type, cv::DataType<ColorSpace>::channels>{{threshold[0], threshold[1], threshold[2]}}));
    }

    cv::Mat_<ColorSpace> const hqx2x_selector(cv::Mat_<ColorSpace> const &src, cv::Mat_<ColorSpace> &dst, channel_type threshold[], int2Type<4>) const
    {
        return hqx_bgr(src, dst, threshold, hqx2xInterpolate<ColorSpace>(std::array<channel_type, cv::DataType<ColorSpace>::channels>{{threshold[0], threshold[1], threshold[2], threshold[3]}}));
    }

    cv::Mat_<ColorSpace> const hqx3x_selector(cv::Mat_<ColorSpace> const &src, cv::Mat_<ColorSpace> &dst, int2Type<3>) const
    {
        channel_type threshold[] = {48, 7, 6};
        return hqx_bgr(src, dst, threshold, hqx3xInterpolate<ColorSpace>(std::array<channel_type, cv::DataType<ColorSpace>::channels>{{48, 7, 6}}));
    }

    cv::Mat_<ColorSpace> const hqx3x_selector(cv::Mat_<ColorSpace> const &src, cv::Mat_<ColorSpace> &dst, int2Type<4>) const
    {
        channel_type threshold[] = {48, 7, 6, 0};
        return hqx_bgr(src, dst, threshold, hqx3xInterpolate<ColorSpace>(std::array<channel_type, cv::DataType<ColorSpace>::channels>{{48, 7, 6, 0}}));
    }

    cv::Mat_<ColorSpace> const hqx3x_selector(cv::Mat_<ColorSpace> const &src, cv::Mat_<ColorSpace> &dst, channel_type threshold[], int2Type<3>) const
    {
        return hqx_bgr(src, dst, threshold, hqx3xInterpolate<ColorSpace>(std::array<channel_type, cv::DataType<ColorSpace>::channels>{{threshold[0], threshold[1], threshold[2]}}));
    }

    cv::Mat_<ColorSpace> const hqx3x_selector(cv::Mat_<ColorSpace> const &src, cv::Mat_<ColorSpace> &dst, channel_type threshold[], int2Type<4>) const
    {
        return hqx_bgr(src, dst, threshold, hqx3xInterpolate<ColorSpace>(std::array<channel_type, cv::DataType<ColorSpace>::channels>{{threshold[0], threshold[1], threshold[2], threshold[3]}}));
    }

    cv::Mat_<ColorSpace> const hqx4x_selector(cv::Mat_<ColorSpace> const &src, cv::Mat_<ColorSpace> &dst, int2Type<3>) const
    {
        channel_type threshold[] = {48, 7, 6};
        return hqx_bgr(src, dst, threshold, hqx4xInterpolate<ColorSpace>(std::array<channel_type, cv::DataType<ColorSpace>::channels>{{48, 7, 6}}));
    }

    cv::Mat_<ColorSpace> const hqx4x_selector(cv::Mat_<ColorSpace> const &src, cv::Mat_<ColorSpace> &dst, int2Type<4>) const
    {
        channel_type threshold[] = {48, 7, 6, 0};
        return hqx_bgr(src, dst, threshold, hqx4xInterpolate<ColorSpace>(std::array<channel_type, cv::DataType<ColorSpace>::channels>{{48, 7, 6, 0}}));
    }

    cv::Mat_<ColorSpace> const hqx4x_selector(cv::Mat_<ColorSpace> const &src, cv::Mat_<ColorSpace> &dst, channel_type threshold[], int2Type<3>) const
    {
        return hqx_bgr(src, dst, threshold, hqx4xInterpolate<ColorSpace>(std::array<channel_type, cv::DataType<ColorSpace>::channels>{{threshold[0], threshold[1], threshold[2]}}));
    }

    cv::Mat_<ColorSpace> const hqx4x_selector(cv::Mat_<ColorSpace> const &src, cv::Mat_<ColorSpace> &dst, channel_type threshold[], int2Type<4>) const
    {
        return hqx_bgr(src, dst, threshold, hqx4xInterpolate<ColorSpace>(std::array<channel_type, cv::DataType<ColorSpace>::channels>{{threshold[0], threshold[1], threshold[2], threshold[3]}}));
    }
};

/**********************************************************
 ****************** implementation ************************
 **********************************************************/

/*
 * hqx interpolation, support 2x, 3x and 4x
 * The width and height of the dst must be
 * 2x, 3x or 4x of the src view. The color space of
 * source and the view must be bgr, in the future
 * may support bgra too.
 *
 *@param
 * src : the source view(bgr)
 * dst : destination view(bgr)
 * threshold : threshold of YUV(index 0 map to Y, 1 map to U, 2 map to V)
 * func : interpolate image and check the size of the image(use assert)
 */
template<typename ColorSpace>
template<typename Interpolate>
cv::Mat_<ColorSpace> const hqxAlgo<ColorSpace>::hqx_bgr(cv::Mat_<ColorSpace> const &src, cv::Mat_<ColorSpace> &dst, channel_type threshold[], Interpolate func) const
{    
    int const channels = cv::DataType<ColorSpace>::channels;    
    static_assert(channels == 3, "The channels should be 3");
    typedef cv::Vec3w MaskColorSpace;
    int const elem_per_row = 3 * channels;
    channel_type bgr_mask[3 * elem_per_row];
    channel_type yuv_mask[3 * elem_per_row];
    int const bytes_per_row = sizeof(channel_type) * elem_per_row;
    constexpr mapDimTwo2One mapper(channels, elem_per_row);
    cv::Mat_<MaskColorSpace> bgr_mask_view(3, 3, (MaskColorSpace*)bgr_mask, bytes_per_row);
    cv::Mat_<MaskColorSpace> yuv_mask_view(3, 3, (MaskColorSpace*)yuv_mask, bytes_per_row);

    for (int row = 0; row != src.rows; ++row)
    {
        int const prev_line = (row > 0) ? -1 : 0;
        int const next_line = (row < src.rows - 1) ? 1 : 0;

        for(int col = 0; col != src.cols; ++col)
        {           
            deduceMask<2, ColorSpace>::
            deduce_mask(src, bgr_mask, mapper, prev_line, next_line, row, col);

            cv::cvtColor(bgr_mask_view, yuv_mask_view, CV_BGR2YUV);

            channel_type const pattern = deduce_pattern(yuv_mask, threshold, mapper);            

            switch (pattern)
            {
            case 0:
            case 1:
            case 4:
            case 32:
            case 128:
            case 5:
            case 132:
            case 160:
            case 33:
            case 129:
            case 36:
            case 133:
            case 164:
            case 161:
            case 37:
            case 165:
            {
                func.interpolate_165(dst, bgr_mask, row, col);
                break;
            }
            case 2:
            case 34:
            case 130:
            case 162:
            {
                func.interpolate_162(dst, bgr_mask, row, col);
                break;
            }
            case 16:
            case 17:
            case 48:
            case 49:
            {
                func.interpolate_49(dst, bgr_mask, row, col);
                break;
            }
            case 64:
            case 65:
            case 68:
            case 69:
            {
                func.interpolate_69(dst, bgr_mask, row, col);
                break;
            }
            case 8:
            case 12:
            case 136:
            case 140:
            {
                func.interpolate_140(dst, bgr_mask, row, col);
                break;
            }
            case 3:
            case 35:
            case 131:
            case 163:
            {
                func.interpolate_163(dst, bgr_mask, row, col);
                break;
            }
            case 6:
            case 38:
            case 134:
            case 166:
            {
                func.interpolate_166(dst, bgr_mask, row, col);
                break;
            }
            case 20:
            case 21:
            case 52:
            case 53: {
                func.interpolate_53(dst, bgr_mask, row, col);
                break;
            }
            case 144:
            case 145:
            case 176:
            case 177: {
                func.interpolate_177(dst, bgr_mask, row, col);
                break;
            }
            case 192:
            case 193:
            case 196:
            case 197: {
                func.interpolate_197(dst, bgr_mask, row, col);
                break;
            }
            case 96:
            case 97:
            case 100:
            case 101: {
                func.interpolate_101(dst, bgr_mask, row, col);
                break;
            }
            case 40:
            case 44:
            case 168:
            case 172: {
                func.interpolate_172(dst, bgr_mask, row, col);
                break;
            }
            case 9:
            case 13:
            case 137:
            case 141: {
                func.interpolate_141(dst, bgr_mask, row, col);
                break;
            }
            case 18:
            case 50: {
                func.interpolate_50(dst, bgr_mask, row, col);
                break;
            }
            case 80:
            case 81: {
                func.interpolate_81(dst, bgr_mask, row, col);
                break;
            }
            case 72:
            case 76: {
                func.interpolate_76(dst, bgr_mask, row, col);
                break;
            }
            case 10:
            case 138: {
                func.interpolate_138(dst, bgr_mask, row, col);
                break;
            }
            case 66: {
                func.interpolate_66(dst, bgr_mask, row, col);
                break;
            }
            case 24: {
                func.interpolate_24(dst, bgr_mask, row, col);
                break;
            }
            case 7:
            case 39:
            case 135: {
                func.interpolate_135(dst, bgr_mask, row, col);
                break;
            }
            case 148:
            case 149:
            case 180: {
                func.interpolate_180(dst, bgr_mask, row, col);
                break;
            }
            case 224:
            case 228:
            case 225: {
                func.interpolate_225(dst, bgr_mask, row, col);
                break;
            }
            case 41:
            case 169:
            case 45: {
                func.interpolate_45(dst, bgr_mask, row, col);
                break;
            }
            case 22:
            case 54: {
                func.interpolate_54(dst, bgr_mask, row, col);
                break;
            }
            case 208:
            case 209: {
                func.interpolate_209(dst, bgr_mask, row, col);
                break;
            }
            case 104:
            case 108: {
                func.interpolate_108(dst, bgr_mask, row, col);
                break;
            }
            case 11:
            case 139: {
                func.interpolate_139(dst, bgr_mask, row, col);
                break;
            }
            case 19:
            case 51: {
                func.interpolate_51(dst, bgr_mask, row, col);
                break;
            }
            case 146:
            case 178: {
                func.interpolate_178(dst, bgr_mask, row, col);
                break;
            }
            case 84:
            case 85: {
                func.interpolate_85(dst, bgr_mask, row, col);
                break;
            }
            case 112:
            case 113: {
                func.interpolate_113(dst, bgr_mask, row, col);
                break;
            }
            case 200:
            case 204: {
                func.interpolate_204(dst, bgr_mask, row, col);
                break;
            }
            case 73:
            case 77: {
                func.interpolate_77(dst, bgr_mask, row, col);
                break;
            }
            case 42:
            case 170: {
                func.interpolate_170(dst, bgr_mask, row, col);
                break;
            }
            case 14:
            case 142: {
                func.interpolate_142(dst, bgr_mask, row, col);
                break;
            }
            case 67: {
                func.interpolate_67(dst, bgr_mask, row, col);
                break;
            }
            case 70: {
                func.interpolate_70(dst, bgr_mask, row, col);
                break;
            }
            case 28: {
                func.interpolate_28(dst, bgr_mask, row, col);
                break;
            }
            case 152: {
                func.interpolate_152(dst, bgr_mask, row, col);
                break;
            }
            case 194: {
                func.interpolate_194(dst, bgr_mask, row, col);
                break;
            }
            case 98: {
                func.interpolate_98(dst, bgr_mask, row, col);
                break;
            }
            case 56: {
                func.interpolate_56(dst, bgr_mask, row, col);
                break;
            }
            case 25: {
                func.interpolate_25(dst, bgr_mask, row, col);
                break;
            }
            case 26:
            case 31: {
                func.interpolate_31(dst, bgr_mask, row, col);
                break;
            }
            case 82:
            case 214: {
                func.interpolate_214(dst, bgr_mask, row, col);
                break;
            }
            case 88:
            case 248: {
                func.interpolate_248(dst, bgr_mask, row, col);
                break;
            }
            case 74:
            case 107: {
                func.interpolate_107(dst, bgr_mask, row, col);
                break;
            }
            case 27: {
                func.interpolate_27(dst, bgr_mask, row, col);
                break;
            }
            case 86: {
                func.interpolate_86(dst, bgr_mask, row, col);
                break;
            }
            case 216: {
                func.interpolate_216(dst, bgr_mask, row, col);
                break;
            }
            case 106: {
                func.interpolate_106(dst, bgr_mask, row, col);
                break;
            }
            case 30: {
                func.interpolate_30(dst, bgr_mask, row, col);
                break;
            }
            case 210: {
                func.interpolate_210(dst, bgr_mask, row, col);
                break;
            }
            case 120: {
                func.interpolate_120(dst, bgr_mask, row, col);
                break;
            }
            case 75: {
                func.interpolate_75(dst, bgr_mask, row, col);
                break;
            }
            case 29: {
                func.interpolate_29(dst, bgr_mask, row, col);
                break;
            }
            case 198: {
                func.interpolate_198(dst, bgr_mask, row, col);
                break;
            }
            case 184: {
                func.interpolate_184(dst, bgr_mask, row, col);
                break;
            }
            case 99: {
                func.interpolate_99(dst, bgr_mask, row, col);
                break;
            }
            case 57: {
                func.interpolate_57(dst, bgr_mask, row, col);
                break;
            }
            case 71: {
                func.interpolate_71(dst, bgr_mask, row, col);
                break;
            }
            case 156: {
                func.interpolate_156(dst, bgr_mask, row, col);
                break;
            }
            case 226: {
                func.interpolate_226(dst, bgr_mask, row, col);
                break;
            }
            case 60: {
                func.interpolate_60(dst, bgr_mask, row, col);
                break;
            }
            case 195: {
                func.interpolate_195(dst, bgr_mask, row, col);
                break;
            }
            case 102: {
                func.interpolate_102(dst, bgr_mask, row, col);
                break;
            }
            case 153: {
                func.interpolate_153(dst, bgr_mask, row, col);
                break;
            }
            case 58: {
                func.interpolate_58(dst, bgr_mask, row, col);
                break;
            }
            case 83: {
                func.interpolate_83(dst, bgr_mask, row, col);
                break;
            }
            case 92: {
                func.interpolate_92(dst, bgr_mask, row, col);
                break;
            }
            case 202: {
                func.interpolate_202(dst, bgr_mask, row, col);
                break;
            }
            case 78: {
                func.interpolate_78(dst, bgr_mask, row, col);
                break;
            }
            case 154: {
                func.interpolate_154(dst, bgr_mask, row, col);
                break;
            }
            case 114: {
                func.interpolate_114(dst, bgr_mask, row, col);
                break;
            }
            case 89: {
                func.interpolate_89(dst, bgr_mask, row, col);
                break;
            }
            case 90: {
                func.interpolate_90(dst, bgr_mask, row, col);
                break;
            }
            case 55:
            case 23: {
                func.interpolate_23(dst, bgr_mask, row, col);
                break;
            }
            case 182:
            case 150: {
                func.interpolate_150(dst, bgr_mask, row, col);
                break;
            }
            case 213:
            case 212: {
                func.interpolate_212(dst, bgr_mask, row, col);
                break;
            }
            case 241:
            case 240: {
                func.interpolate_240(dst, bgr_mask, row, col);
                break;
            }
            case 236:
            case 232: {
                func.interpolate_232(dst, bgr_mask, row, col);
                break;
            }
            case 109:
            case 105: {
                func.interpolate_105(dst, bgr_mask, row, col);
                break;
            }
            case 171:
            case 43: {
                func.interpolate_43(dst, bgr_mask, row, col);
                break;
            }
            case 143:
            case 15: {
                func.interpolate_15(dst, bgr_mask, row, col);
                break;
            }
            case 124: {
                func.interpolate_124(dst, bgr_mask, row, col);
                break;
            }
            case 203: {
                func.interpolate_203(dst, bgr_mask, row, col);
                break;
            }
            case 62: {
                func.interpolate_62(dst, bgr_mask, row, col);
                break;
            }
            case 211: {
                func.interpolate_211(dst, bgr_mask, row, col);
                break;
            }
            case 118: {
                func.interpolate_118(dst, bgr_mask, row, col);
                break;
            }
            case 217: {
                func.interpolate_217(dst, bgr_mask, row, col);
                break;
            }
            case 110: {
                func.interpolate_110(dst, bgr_mask, row, col);
                break;
            }
            case 155: {
                func.interpolate_155(dst, bgr_mask, row, col);
                break;
            }
            case 188: {
                func.interpolate_188(dst, bgr_mask, row, col);
                break;
            }
            case 185: {
                func.interpolate_185(dst, bgr_mask, row, col);
                break;
            }
            case 61: {
                func.interpolate_61(dst, bgr_mask, row, col);
                break;
            }
            case 157: {
                func.interpolate_157(dst, bgr_mask, row, col);
                break;
            }
            case 103: {
                func.interpolate_103(dst, bgr_mask, row, col);
                break;
            }
            case 227: {
                func.interpolate_227(dst, bgr_mask, row, col);
                break;
            }
            case 230: {
                func.interpolate_230(dst, bgr_mask, row, col);
                break;
            }
            case 199: {
                func.interpolate_199(dst, bgr_mask, row, col);
                break;
            }
            case 220: {
                func.interpolate_220(dst, bgr_mask, row, col);
                break;
            }
            case 158: {
                func.interpolate_158(dst, bgr_mask, row, col);
                break;
            }
            case 234: {
                func.interpolate_234(dst, bgr_mask, row, col);
                break;
            }
            case 242: {
                func.interpolate_242(dst, bgr_mask, row, col);
                break;
            }
            case 59: {
                func.interpolate_59(dst, bgr_mask, row, col);
                break;
            }
            case 121: {
                func.interpolate_121(dst, bgr_mask, row, col);
                break;
            }
            case 87: {
                func.interpolate_87(dst, bgr_mask, row, col);
                break;
            }
            case 79: {
                func.interpolate_79(dst, bgr_mask, row, col);
                break;
            }
            case 122: {
                func.interpolate_122(dst, bgr_mask, row, col);
                break;
            }
            case 94: {
                func.interpolate_94(dst, bgr_mask, row, col);
                break;
            }
            case 218: {
                func.interpolate_218(dst, bgr_mask, row, col);
                break;
            }
            case 91: {
                func.interpolate_91(dst, bgr_mask, row, col);
                break;
            }
            case 229: {
                func.interpolate_229(dst, bgr_mask, row, col);
                break;
            }
            case 167: {
                func.interpolate_167(dst, bgr_mask, row, col);
                break;
            }
            case 173: {
                func.interpolate_173(dst, bgr_mask, row, col);
                break;
            }
            case 181: {
                func.interpolate_181(dst, bgr_mask, row, col);
                break;
            }
            case 186: {
                func.interpolate_186(dst, bgr_mask, row, col);
                break;
            }
            case 115: {
                func.interpolate_115(dst, bgr_mask, row, col);
                break;
            }
            case 93: {
                func.interpolate_93(dst, bgr_mask, row, col);
                break;
            }
            case 206: {
                func.interpolate_206(dst, bgr_mask, row, col);
                break;
            }
            case 205:
            case 201: {
               func.interpolate_201(dst, bgr_mask, row, col);
                break;
            }
            case 174:
            case 46: {
                func.interpolate_46(dst, bgr_mask, row, col);
                break;
            }
            case 179:
            case 147: {
               func.interpolate_147(dst, bgr_mask, row, col);
                break;
            }
            case 117:
            case 116: {
                func.interpolate_116(dst, bgr_mask, row, col);
                break;
            }
            case 189: {
                func.interpolate_189(dst, bgr_mask, row, col);
                break;
            }
            case 231: {
               func.interpolate_231(dst, bgr_mask, row, col);
                break;
            }
            case 126: {
                func.interpolate_126(dst, bgr_mask, row, col);
                break;
            }
            case 219: {
               func.interpolate_219(dst, bgr_mask, row, col);
                break;
            }
            case 125: {
                func.interpolate_125(dst, bgr_mask, row, col);
                break;
            }
            case 221: {
                func.interpolate_221(dst, bgr_mask, row, col);
                break;
            }
            case 207: {
                func.interpolate_207(dst, bgr_mask, row, col);
                break;
            }
            case 238: {
                func.interpolate_238(dst, bgr_mask, row, col);
                break;
            }
            case 190: {
                func.interpolate_190(dst, bgr_mask, row, col);
                break;
            }
            case 187: {
                func.interpolate_187(dst, bgr_mask, row, col);
                break;
            }
            case 243: {
                func.interpolate_243(dst, bgr_mask, row, col);
                break;
            }
            case 119: {
                func.interpolate_119(dst, bgr_mask, row, col);
                break;
            }
            case 237:
            case 233: {
                func.interpolate_233(dst, bgr_mask, row, col);
                break;
            }
            case 175:
            case 47: {
                func.interpolate_47(dst, bgr_mask, row, col);
                break;
            }
            case 183:
            case 151: {
                func.interpolate_151(dst, bgr_mask, row, col);
                break;
            }
            case 245:
            case 244: {
                func.interpolate_244(dst, bgr_mask, row, col);
                break;
            }
            case 250: {
                func.interpolate_250(dst, bgr_mask, row, col);
                break;
            }
            case 123: {
               func.interpolate_123(dst, bgr_mask, row, col);
                break;
            }
            case 95: {
                func.interpolate_95(dst, bgr_mask, row, col);
                break;
            }
            case 222: {
                func.interpolate_222(dst, bgr_mask, row, col);
                break;
            }
            case 252: {
                func.interpolate_252(dst, bgr_mask, row, col);
                break;
            }
            case 249: {
                func.interpolate_249(dst, bgr_mask, row, col);
                break;
            }
            case 235: {
                func.interpolate_235(dst, bgr_mask, row, col);
                break;
            }
            case 111: {
                func.interpolate_111(dst, bgr_mask, row, col);
                break;
            }
            case 63: {
                func.interpolate_63(dst, bgr_mask, row, col);
                break;
            }
            case 159: {
                func.interpolate_159(dst, bgr_mask, row, col);
                break;
            }
            case 215: {
                func.interpolate_215(dst, bgr_mask, row, col);
                break;
            }
            case 246: {
                func.interpolate_246(dst, bgr_mask, row, col);
                break;
            }
            case 254: {
                func.interpolate_254(dst, bgr_mask, row, col);
                break;
            }
            case 253: {
                func.interpolate_253(dst, bgr_mask, row, col);
                break;
            }
            case 251: {
                func.interpolate_251(dst, bgr_mask, row, col);
                break;
            }
            case 239: {
                func.interpolate_239(dst, bgr_mask, row, col);
                break;
            }
            case 127: {
                func.interpolate_127(dst, bgr_mask, row, col);
                break;
            }
            case 191: {
                func.interpolate_191(dst, bgr_mask, row, col);
                break;
            }
            case 223: {
                func.interpolate_223(dst, bgr_mask, row, col);
                break;
            }
            case 247: {
                func.interpolate_247(dst, bgr_mask, row, col);
                break;
            }
            case 255: {
                func.interpolate_255(dst, bgr_mask, row, col);
                break;
            }

            }
        }
    }

    return dst;
}

}

#endif // HQXALGO_HPP
