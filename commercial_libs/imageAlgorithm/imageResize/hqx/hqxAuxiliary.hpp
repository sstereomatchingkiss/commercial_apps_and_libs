#ifndef GILHQXAUXILIARY_HPP
#define GILHQXAUXILIARY_HPP

/*
 * deploy some helper functions for gilHqxAlgo.hpp, this header file
 * is implementation details of hqxAlgo.hpp, don't use it directly
 */

#include <cmath>

#include <opencv2/imgproc/imgproc.hpp>

#include "utility/dimensionMapper.hpp"

namespace OCV{

template<int Channel, typename T>
class unrollDiff;

template<typename T>
bool constexpr diff(T c1, T c2, T threshold);

/*
 * find out the 3x3 area color
 *
 *@param
 * channel : the channel number of the src, make it as nontype
 * template parameter could calculate the index of mask at compile time
 * src : source pixell
 * mask : 3x3 area color
 * row : index of row
 * col : index of col
 */
template<int Channel, typename ColorSpace>
class deduceMask
{
public:
    static void deduce_mask(cv::Mat_<ColorSpace> const &src, typename cv::DataType<ColorSpace>::channel_type mask[],
                            mapDimTwo2One const mapper, int prev_line, int next_line, int row, int col);
};

template<int Channel, typename ColorSpace>
void deduceMask<Channel, ColorSpace>::
deduce_mask(cv::Mat_<ColorSpace> const &src, typename cv::DataType<ColorSpace>::channel_type mask[],
            mapDimTwo2One const mapper, int prev_line, int next_line, int row, int col)
{        
    mask[mapper(0, 1, Channel)] = src(row + prev_line, col)[Channel];
    mask[mapper(1, 1, Channel)] = src(row, col)[Channel];
    mask[mapper(2, 1, Channel)] = src(row + next_line, col)[Channel];

    if (col != 0)
    {
        mask[mapper(0, 0, Channel)] = src(row + prev_line, col - 1)[Channel];
        mask[mapper(1, 0, Channel)] = src(row, col - 1)[Channel];
        mask[mapper(2, 0, Channel)] = src(row + next_line, col - 1)[Channel];
    }
    else
    {
        mask[mapper(0, 0, Channel)] = mask[mapper(0, 1, Channel)];
        mask[mapper(1, 0, Channel)] = mask[mapper(1, 1, Channel)];
        mask[mapper(2, 0, Channel)] = mask[mapper(2, 1, Channel)];
    }

    if (col != src.cols - 1)
    {
        mask[mapper(0, 2, Channel)] = src(row + prev_line, col + 1)[Channel];
        mask[mapper(1, 2, Channel)] = src(row, col + 1)[Channel];
        mask[mapper(2, 2, Channel)] = src(row + next_line, col + 1)[Channel];
    }
    else
    {
        mask[mapper(0, 2, Channel)] = mask[mapper(0, 1, Channel)];
        mask[mapper(1, 2, Channel)] = mask[mapper(1, 1, Channel)];
        mask[mapper(2, 2, Channel)] = mask[mapper(2, 1, Channel)];
    }

    deduceMask<Channel - 1, ColorSpace>::deduce_mask(src, mask, mapper, prev_line, next_line, row, col);
}

template<typename ColorSpace>
class deduceMask<-1, ColorSpace>
{
public:
    static void deduce_mask(cv::Mat_<ColorSpace> const&, typename cv::DataType<ColorSpace>::channel_type[],
                            mapDimTwo2One const, int, int, int, int)
    {

    }
};

/*
 * deduce the pattern of the hqx need(vary between 0~255)
 */
template<typename T>
T deduce_pattern(T mask[], T threshold[], mapDimTwo2One const mapper)
{
    size_t const dimension = 3;
    T flag = 1;
    T pattern = 0;
    for (size_t row = 0; row != dimension; row++)
    {
        for(size_t col = 0; col != dimension; ++col)
        {
            if (row == 1 && col == 1) continue;

            if(mask[mapper(1, 1, 0)] != mask[mapper(row, col, 0)] &&
               mask[mapper(1, 1, 1)] != mask[mapper(row, col, 1)] &&
               mask[mapper(1, 1, 2)] != mask[mapper(row, col, 2)])
            {
                if(diff(mask[mapper(1, 1, 0)], mask[mapper(row, col, 0)], threshold[0]) ||
                   diff(mask[mapper(1, 1, 1)], mask[mapper(row, col, 1)], threshold[1]) ||
                   diff(mask[mapper(1, 1, 2)], mask[mapper(row, col, 2)], threshold[2]))
                {
                    pattern += flag;
                }
            }

            flag *= 2;
        }
    }

    return pattern;
}

/*
 * Compares two pixels value according to the provided thresholds.
 * @param
 *  c1 : a pixel value
 *  c2 : a second pixel value
 *  threshold : threshold of c1 and c2
 *
 * @return
 *  return true if colors differ more than the thresholds permit, false otherwise
 */
template<typename T>
bool constexpr diff(T c1, T c2, T threshold)
{
    return std::abs(c1 - c2) > threshold;
}

template<typename T>
inline T mix_3_to_1(T c1, T c2)
{
    if (c1 == c2) {
        return c1;
    }

    return (c1 * 3 + c2) / 4;
}

template<typename T>
constexpr T mix_2_to_1_to_1(T c1, T c2, T c3)
{
    return (c1 * 2 + c2 + c3) / 4;
}

template<typename T>
inline T mix_7_to_1(T c1, T c2)
{
    if (c1 == c2) {
        return c1;
    }

    return (c1 * 7 + c2) / 8;
}

template<typename T>
constexpr T mix_2_to_7_to_7(T c1, T c2, T c3)
{
    return (c1 * 2 + (c2 + c3) * 7) / 16;
}

template<typename T>
inline T mix_even(T c1, T c2)
{
    if (c1 == c2) {
        return c1;
    }

    return (c1 + c2) / 2;
}

template<typename T>
constexpr T mix_4_to_2_to_1(T c1, T c2, T c3)
{
    return (c1 * 5 + c2 * 2 + c3) / 8;
}

template<typename T>
constexpr T mix_6_to_1_to_1(T c1, T c2, T c3)
{
    return (c1 * 6 + c2 + c3) / 8;
}

template<typename T>
inline T mix_5_to_3(T c1, T c2)
{
    if (c1 == c2)
    {
        return c1;
    }

    return (c1 * 5 + c2 * 3) / 8;
}

template<typename T>
constexpr T mix_2_to_3_to_3(T c1, T c2, T c3)
{
    return (c1* 2 + (c2 + c3) * 3) / 8;
}

template<typename T>
constexpr T mix_14_to_1_to_1(T c1, T c2, T c3)
{
    return (c1 * 14 + c2 + c3) / 16;
}

/*
 * unroll diff, this is design for member function deduce_pattern(belongs to class hqxAlgo)
 */
template<int Channel, typename T>
class unrollDiff
{
public:
    static bool unroll_diff(T mask[], T threshold[], mapDimTwo2One const mapper, int row, int col)
    {
        return diff(mask[mapper(1, 1, Channel)], mask[mapper(row, col, Channel)], threshold[Channel]) ||
               unrollDiff<Channel - 1, T>::unroll_diff(mask, threshold, mapper, row, col);
    }
};


template<typename T>
class unrollDiff<0, T>
{
public:
    static bool unroll_diff(T mask[], T threshold[], mapDimTwo2One const mapper, int row, int col)
    {
        return diff(mask[mapper(1, 1, 0)], mask[mapper(row, col, 0)], threshold[0]);
    }
};

}

#endif // GILHQXAUXILIARY_HPP
