#ifndef HQXINTERPOLATIONBASE_HPP
#define HQXINTERPOLATIONBASE_HPP

/*
 * base class of hq2x, hq3x and hq4x, design constant index to
 * lower some computation
 */

namespace OCV{

template<typename T>
class indexHelper
{
   enum {Channels = cv::DataType<T>::channels, ElemPerRow = 3 * Channels};
public:    
    constexpr indexHelper() {}

   constexpr int get_index(int index) const { return index_[index]; }

protected:
   static int const index_[9];
};

template<typename T> int const indexHelper<T>::index_[9] =
{0,            1 * Channels, 2 * Channels,
 3 * Channels, 4 * Channels, 5 * Channels,
 6 * Channels, 7 * Channels, 8 * Channels};

}

#endif // HQXINTERPOLATIONBASE_HPP
