#include <cmath>
#include <iostream>
#include <stdexcept>

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "../../basicImageAlgo.hpp"
#include "utility/getDataHelper.hpp"

#include "stencilSet.hpp"

namespace
{

float const PI = 3.14159265358979323846264338327950288;

/** @brief The angular resolution for stencil quantization */
int const NUMANGLES = 64;
/** @brief Number of quadrature panels to use for integrals */
int const QUAD_RES  = 8;
/** @brief Initial capacity of a stencil set */
int const INITIAL_CAPACITY = 16;
/** @brief Stability threshold */
double const STABILITY_THRESH = 1e-4;

/** @brief phi function for the circle stencil */
inline double circle(double x, double y, void const*)
{
    return std::sqrt(x * x + y * y);
}

}

namespace OCV{

stencilSet::stencilEntry::stencilEntry() : phi_(nullptr)
{

}

/**
 * @param stencil_radius the circular radius of \f$\Omega\f$
 * @param psf point spread function modeling the sampling of the input image
 * @param psf_param parameters to pass to Psf
 * @param psf_radius support radius of Psf
 */
stencilSet::stencilSet(double stencil_radius,
                       double (*psf)(double, double, const void*),
                       void const *psf_param, double psf_radius)
{
    int const R = static_cast<int>(std::floor(stencil_radius));

    if(R <= 0 || psf_radius < 0){
        throw std::runtime_error("R <= 0 || psf_radius < 0");
    }

    // Initializes an empty stencil set
    num_stencils_ = num_cells_ = 0;
    psf_ = psf;
    psf_param_ = psf_param;
    psf_radius_ = (psf) ? psf_radius : 0;
    cell_.resize(4 * R * R);

    // precompute cell quadrature weights
    compute_quad_weights();

    // Construct the circular neighborhood
    double const stencil_radius_2 = stencil_radius * stencil_radius;
    for(int y = -R; y < R; ++y)
        for(int x = -R; x < R; ++x){
            if((x + 0.5) * (x + 0.5) + (y + 0.5) * (y + 0.5)
                    <= stencil_radius_2){
                cell_[num_cells_].x_ = x;
                cell_[num_cells_].y_ = y;
                ++num_cells_;
            }
        }

    add_stencil(circle, nullptr, 0);
}

void stencilSet::add_stencil(double (*phi)(double, double, void const*),
                             void const *phi_param, double rotation)
{               
    stencil_.push_back(stencilEntry());
    int const S = num_stencils_++;
    stencilEntry &entry = stencil_[S];
    entry.alpha_.resize(num_cells_);
    entry.beta_.resize(num_cells_);
    entry.phi_ = phi;
    entry.phi_param_ = phi_param;
    entry.phi_trans_[0] = std::cos(rotation);
    entry.phi_trans_[1] = std::sin(rotation);

    int const R = static_cast<int>(std::ceil(QUAD_RES * psf_radius_));
    double sum = 0;
    for(int n = 0; n < num_cells_; ++n){
        double alpha = 0;
        double beta = 0;

        // Compute the average of (grad (Psf * Phi))^perp over the cell
        for(int y = -R, offset = 0; y <= QUAD_RES + R; ++y){
            for(int x = -R; x <= QUAD_RES + R; ++x, ++offset){
                double const temp = eval_phi(S, cell_[n].x_ + static_cast<double>(x) / QUAD_RES,
                                             cell_[n].y_ + static_cast<double>(y) / QUAD_RES);
                beta  -= quad_dx_[offset] * temp;
                alpha += quad_dy_[offset] * temp;
            }
        }

        entry.alpha_[n] = alpha;
        entry.beta_[n] = beta;
        sum += std::sqrt(alpha * alpha + beta * beta);
    }

    // Normalize the stencil
    for(int n = 0; n < num_cells_; ++n){
        entry.alpha_[n] /= sum;
        entry.beta_[n]  /= sum;
    }

    // Quantize the stencil
    stencil_table_.resize(num_cells_ + num_cells_ * S);
    for(int n = 0; n < num_cells_; ++n){
        int i = static_cast<int>(std::floor(std::atan2(entry.beta_[n], entry.alpha_[n]) * NUMANGLES / PI + 0.5));

        while(i < 0){
            i += NUMANGLES;
        }
        while(i >= NUMANGLES){
            i -= NUMANGLES;
        }

        stencil_table_[n + num_cells_ * S].angle_index_ = i;
        stencil_table_[n + num_cells_ * S].weight_ =
                static_cast<float>(std::sqrt(entry.alpha_[n] * entry.alpha_[n]
                                             + entry.beta_[n] * entry.beta_[n]));
    }
}

/**
 * @brief Evaluate the \f$\varphi_\mathcal{S}\f$ associated with a stencil
 * @param S index of the stencil
 * @param x,y location to evaluate
 *
 * Evaluate the distance function \f$\varphi_\mathcal{S}\f$ associated with
 * stencil S at (x,y).  The stencil rotation from stencilentry::PhiTrans is
 * applied.
 */
double stencilSet::eval_phi(int S, double x, double y)
{
    stencilEntry const &entry = stencil_[S];

    return entry.phi_(entry.phi_trans_[0] * x + entry.phi_trans_[1] * y,
            -entry.phi_trans_[1] * x + entry.phi_trans_[0] * y, entry.phi_param_);
}

/**
 * @brief Determine the best-fitting stencil at each point in an image
 * @param image the input image
 * @return the selected best-fitting stencils \f$\mathcal{S}^\star\f$
 *
 * This is the main routine for determining the best-fitting stencils on an
 * image.  The result is stored in Stencil, which should be an array of size
 * ImageWidth by ImageHeight.  For each pixel, the index of the best-fitting
 * stencil is stored in Stencil.
 *
 * For computational efficiency, the stencil fitting uses the quantized stencil
 * vectors ssetstruct::StencilTable to approximate the cell total variations
 * \f[ V_{i,j}(\mathcal{S}, v) \approx |\mathcal{S}_{i,j}| \cdot
 *     V_{i,j}\bigl( (\!\begin{smallmatrix} \cos \theta \\
 *     \sin \theta \end{smallmatrix}\!), v\bigr), \quad
 *     |\theta - \angle \mathcal{S}_{i,j}| \le \tfrac{\pi}{128}. \f]
 * First, the cell total variation are computed for each pixel at each angle
 * 0, ..., #NUMANGLES-1.  Then then stencils are computed by summing the cell
 * total variations.  Finally, the stencil with the smallest total variation is
 * identified as the best-fitting stencil,
 * \f[ \mathcal{S}^\star(k) = \operatorname*{arg\,min}_{\mathcal{S}\in\Sigma}
 *                            \, V\bigl(\mathcal{S},v(\cdot-k)\bigr). \f]
 * If the difference between the smallest and second smallest total variations
 * is below a threshold, the circle stencil is selected.
 */
std::vector<int> stencilSet::fit_stencils(cv::Mat const &image)
{                                  
    auto const tv_thresh = STABILITY_THRESH * (4 * std::sqrt(static_cast<float>(2)));
    std::vector<int> stencils;
    stencils.reserve(image.rows * image.cols);
    std::vector<int> neighbor_offset(num_cells_);
    int const cells_per_row = image.cols - 1;
    int const rows = image.rows;
    int const cols = image.cols;
    for(int n = 0; n != num_cells_; ++n){
        neighbor_offset[n] = NUMANGLES * (cell_[n].x_ - cells_per_row * cell_[n].y_);
    }
    std::vector<float> tv_cell = compute_cell_total_variant(image);
    for(int y = 0; y < rows; ++y){
        for(int x = 0; x < cols; ++x){
            int best_s = 0;
            float min_tv   = 1e30f;
            float min_2_tv = 1e30f;
            auto const *tv_cell_ptr = &tv_cell[0] + NUMANGLES * (x + cells_per_row * (y - 1));
            auto const *table_ptr = &stencil_table_[0];

            for(int S = 0; S < num_stencils_; ++S, table_ptr += num_cells_){
                float tv = 0;

                // Compute the stencil TV for the Sth stencil
                for(int n = 0; n < num_cells_; ++n){
                    int nx = x + cell_[n].x_;

                    if(nx >= 0 && nx < cells_per_row){
                        int ny = y - cell_[n].y_ - 1;

                        if(ny >= 0 && ny < rows - 1)
                            tv += table_ptr[n].weight_ * tv_cell_ptr[
                                    neighbor_offset[n] + table_ptr[n].angle_index_];
                    }
                }

                // Determine the best-fitting stencil
                if(tv < min_2_tv){
                    if(tv < min_tv){
                        best_s = S;
                        min_2_tv = min_tv;
                        min_tv = tv;
                    }
                    else{
                        min_2_tv = tv;
                    }
                }
            }

            // If the separation between smallest and second smallest TVs
            // is below the threshold, select the circle stencil.
            if(min_2_tv - min_tv < tv_thresh){
                best_s = 0;
            }
            stencils.emplace_back(best_s);
        }
    }

    return stencils;
}

std::vector<float> stencilSet::compute_cell_total_variant(cv::Mat const &image)
{
    cv::Mat float_image;
    OCV::mat8u_to_mat32f(image, float_image);
    if(image.channels() == 4){
        cv::cvtColor(float_image, float_image, CV_BGRA2BGR);
    }
    int const rows = image.rows;
    int const cols = image.cols;
    if(image.channels() == 3){
        for(int i = 0; i != rows; ++i){
            auto *image_ptr = float_image.ptr<float>(i);
            for(int j = 0; j != cols; ++j){

                float const r = image_ptr[0];
                float const g = image_ptr[1];
                float const b = image_ptr[2];

                image_ptr[0] =  0.299f*r    + 0.587f*g    + 0.114f*b;
                image_ptr[1] = -0.168736f*r - 0.331264f*g + 0.5f*b;
                image_ptr[2] =  0.5f*r      - 0.418688f*g - 0.081312f*b;
                image_ptr += 3;
            }
        }
    }

    std::vector<float> ab(float_image.channels()), ac(float_image.channels()), bd(float_image.channels()), cd(float_image.channels());
    int const cells_per_row = image.cols - 1;
    int const channels = image.channels();
    std::vector<float> tv_cell(NUMANGLES * cells_per_row * (image.rows - 1), 0);
    std::vector<int> neighbor_offset(num_cells_);
    for(int n = 0; n != num_cells_; ++n){
        neighbor_offset[n] = NUMANGLES * (cell_[n].x_ - cells_per_row * cell_[n].y_);
    }

    float alpha[NUMANGLES], beta[NUMANGLES];
    for(int n = 0; n != NUMANGLES; ++n){
        alpha[n] = static_cast<float>(std::cos((PI * n) / NUMANGLES));
        beta[n]  = static_cast<float>(std::sin((PI * n) / NUMANGLES));
    }

    auto tv_cell_ptr = &tv_cell[0];
    for(int y = 0; y != rows - 1; ++y){
        for(int x = 0; x != cells_per_row; ++x, tv_cell_ptr += NUMANGLES){
            // Get the four corners of the cell in YPbPr representation
            //
            //    a---b
            //    |   |
            //    c---d
            //
            auto const *a = get_pointer<float>(float_image, y, x);
            auto const *b = a + float_image.channels();
            auto const *c = get_pointer<float>(float_image, y + 1, x);
            auto const *d = c + float_image.channels();

            for(int i = 0; i != channels; ++i){
                ab[i] = a[i] - b[i];
                ac[i] = a[i] - c[i];
                bd[i] = b[i] - d[i];
                cd[i] = c[i] - d[i];
            }

            // Angles between 0 and pi/2
            int n = 1;
            for(; n < (NUMANGLES / 2); ++n){
                for(int i = 0; i != channels; ++i){
                    tv_cell_ptr[n] += std::abs(alpha[n] * ab[i] - beta[n] * ac[i])
                            +  std::abs(beta[n] * bd[i] - alpha[n] * cd[i]);

                }
            }

            for(int i = 0; i != channels; ++i){
                //Horizontal TV
                tv_cell_ptr[0] += std::abs(ab[i]) + std::abs(cd[i]);

                // Vertical TV
                tv_cell_ptr[n] += std::abs(bd[i]) + std::abs(ac[i]);
            }

            // Angles between pi/2 and pi
            while(++n != NUMANGLES){
                for(int i = 0; i != channels; ++i){
                    tv_cell_ptr[n] += std::abs(alpha[n] * ab[i] - beta[n] * bd[i])
                            +  std::abs(beta[n] * ac[i] -  alpha[n] * cd[i]);
                }
            }
        }
    }

    return tv_cell;
}

/**
 * @brief Compute quadrature weights for integrating over cells
 * @param Set the stencil set
 * @return 1 on success, 0 on failure
 *
 * Quadrature weights are computed for evaluating integrals of the form
 * \f[ \int_{\Omega_k} \nabla (h * \varphi)^\perp \, dx. \f]
 * For efficiency, these weights are stored in ssetstruct::QuadDx and
 * ssetstruct::QuadDy so that they are computed only once and then reused.
 */
void stencilSet::compute_quad_weights()
{
    int const R = static_cast<int>(std::ceil(QUAD_RES * psf_radius_));
    int const W = 2 * R + 1;

    quad_dx_.resize((QUAD_RES + W)*(QUAD_RES + W));
    quad_dy_.resize((QUAD_RES + W)*(QUAD_RES + W));
    std::vector<double> psf_samples = sample_psf();

    // Compute quadrature weights that incorporate convolution with the PSF
    double sum;
    int i, j;
    for(int y = -R, offset = 0; y <= QUAD_RES + R; y++){
        for(int x = -R; x <= QUAD_RES + R; x++, offset++){
            for(j = -y, sum = 0; j <= QUAD_RES - y; j++){
                if(std::abs(static_cast<double>(j)) <= R){
                    i = QUAD_RES - x;

                    if(std::abs(static_cast<double>(i)) <= R)
                        sum += psf_samples[(i + R) + W*(j + R)];

                    i = -x;

                    if(std::abs(static_cast<double>(i)) <= R)
                        sum -= psf_samples[(i + R) + W*(j + R)];
                }
            }

            quad_dx_[offset] = sum;

            for(i = -x, sum = 0; i <= QUAD_RES - x; ++i){
                if(std::abs(static_cast<double>(i)) <= R){
                    j = QUAD_RES - y;

                    if(std::abs(static_cast<double>(j)) <= R)
                        sum += psf_samples[(i + R) + W * (j + R)];

                    j = -y;

                    if(std::abs(static_cast<double>(j)) <= R)
                        sum -= psf_samples[(i + R) + W * (j + R)];
                }
            }

            quad_dy_[offset] = sum;
        }
    }
}

/**
 * @brief Compute samples of the PSF
 * @param Set the stencil set
 * @return a pointer to an array of PSF samples, NULL on failure
 */
std::vector<double> stencilSet::sample_psf() const
{
    int const R = static_cast<int>(std::ceil(QUAD_RES * psf_radius_));
    int const W = 2 * R + 1;

    std::vector<double> samples(W * W);

    if(!psf_){
        samples[0] = QUAD_RES * QUAD_RES;
    }
    else{
        for(int j = -R, offset = 0; j <= R; ++j)
            for(int i = -R; i <= R; ++i, ++offset)
                samples[offset] = psf_(i / static_cast<double>(QUAD_RES),
                                       j / static_cast<double>(QUAD_RES), psf_param_);
    }

    return samples;
}

}
