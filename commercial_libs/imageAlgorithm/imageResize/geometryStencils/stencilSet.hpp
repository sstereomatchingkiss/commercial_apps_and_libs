#ifndef STENCIL_SET_HPP
#define STENCIL_SET_HPP

#include <vector>

namespace cv{
  class Mat;
}

namespace OCV{

struct stencilEntry;
struct stencilQVec;
struct stencilCell;

/**
 * @brief A contour stencil set \f$\Sigma\f$
 *
 * A stencil set is a collection of contour stencils.  This is represented as
 * an array ssetstruct::Stencil of type #stencilentry.  For computational
 * efficiency, an array of quantized stencil vectors #stencilqvec is also
 * stored in ssetstruct::StencilTable. The locations of the cells are stored
 * in stencilsetcell::Cell.  The point spread function (PSF) to model the
 * sampling of the input image is specified by ssetstruct::Psf.
 */
class stencilSet
{    

public:
    typedef double (*PSF)(double, double, const void*);

    stencilSet(double stencil_radius,
               double (*psf)(double, double, void const*),
               void const *psf_param, double psf_radius);

    void add_stencil(double (*phi)(double, double, void const*),
                     void const *phi_param, double rotation);

    double eval_phi(int S, double x, double y);

    std::vector<int> fit_stencils(cv::Mat const &image);

    int get_num_stencils() const
    { return num_stencils_; }
    PSF get_psf() const
    { return psf_; }
    void const *get_psf_param() const
    { return psf_param_; }
    double get_psf_radius() const
    { return psf_radius_; }   

#ifndef UNIT_TEST
private:
#endif
    std::vector<float> compute_cell_total_variant(cv::Mat const &image);
    void compute_quad_weights();

    std::vector<double> sample_psf() const;

#ifndef UNIT_TEST
private:
#endif
    /**
     * @brief A single contour stencil \f$\mathcal{S}\f$
     *
     * This data structure represents a single contour stencil \f$\mathcal{S}\f$.
     * It comprises a list of vectors (Alpha[k],Beta[k]) =
     *     \f$(\alpha_k, \beta_k),\f$
     * indicating the value of \f$\mathcal{S}\f$ in the kth cell \f$\Omega_k\f$, a
     * function pointer Phi (and supporting parameters) for the distance function
     * \f$\varphi\f$ from which the stencil was discretized, and "glyph" data used
     * by DrawContours() for a graphical representation of the stencil.
     *
     * The cell locations are stored in ssetstruct::Cell.
     */
    struct stencilEntry
    {
        stencilEntry();

        /** @brief Alpha[k] is the x-component of the stencil in the kth cell */
        std::vector<double> alpha_;
        /** @brief Beta[k] is the y-component of the stencil in the kth cell */
        std::vector<double> beta_;

        /**
         * @brief Function pointer to the stencil's distance function \f$\varphi\f$
         *
         * Phi is a function with the syntax Phi(x,y,PhiParam), where
         * stencilentry::PhiParam may be used to pass additional parameters to Phi.
         * The function is rotated by stencilentry::PhiTrans.
         */
        double (*phi_)(double, double, const void*);
        /**
         * @brief Cosine and sine values of the stencil rotation
         *
         * The values PhiTrans[0] = cos(Rotation), PhiTrans[1] = sin(Rotation) are
         * computed once and reused for all rotation calculations.  The rotation is
         * performed as
         * \f[ \begin{pmatrix} x' \\ y' \end{pmatrix}
         *     = \begin{pmatrix} \mathtt{PhiTrans}[0] & \mathtt{PhiTrans}[1] \\
         *     -\mathtt{PhiTrans}[1] & \mathtt{PhiTrans}[0] \end{pmatrix}
         *     \begin{pmatrix} x \\ y \end{pmatrix} \f]
         */
        double phi_trans_[2];
        /**
         * @brief User-specified parameters to pass to stencilentry::Phi
         *
         * PhiParam is passed as the third argument to Phi.  PhiParam should be
         * used to point to a data structure containing whatever additional
         * parameters are needed for evaluating Phi.  For example, CurveDist()
         * uses PhiParam to specify the curviness parameter "a" of the parabola.
         */
        void const *phi_param_;
    };

    /** @brief A cell in a contour stencil, \f$\Omega_k \subset \mathbb{R}^2\f$
     *
     * The cell is a square with unit side lengths and (x,y) specifying the
     * bottom-left corner.  The cell is
     *    \f[\Omega_k = [x,x+1] \times [y,y+1].\f]
     * The stencilsetcell type is used by ssetstruct::Cell to represent all the
     * cells in the stencil.
     */
    struct stencilCell
    {
        /** @brief x-component of the left edge of the cell */
        int x_;
        /** @brief y-component of the bottom edge of the cell */
        int y_;
    };

    /**
     * @brief Quantized vector
     *
     * For computational efficiency in FitStencils(), the stencil vectors
     * (stencilentry::Alpha[k], stencilentry::Beta[k]) are converted to polar form
     * and quantized to #NUMANGLES possible angles.  stencilqvec::AngleIndex is a
     * number between 0 and #NUMANGLES-1, and stencilqvec::Weight is the magnitude
     * of the vector.
     */
    struct stencilQVec
    {
        /** @brief Quantized angle index, a number between 0 and #NUMANGLES-1 */
        int32_t angle_index_;
        /** @brief Vector magnitude */
        float weight_;
    };

#ifndef UNIT_TEST
private:
#endif
    /** @brief The cells, \f$\Omega = \bigcup(\Omega_0,\Omega_1,\ldots)\f$ */
    std::vector<stencilCell> cell_;

    /** @brief Number of cells in the stencil */
    int num_cells_;
    /** @brief Number of stencils in the set */
    int num_stencils_;

    /** @brief Point spread function (PSF) used to sample the input image */
    double (*psf_)(double, double, const void*);

    /** @brief Array of stencils \f$(\mathcal{S}_0,\mathcal{S}_1,\ldots)\f$ */
    std::vector<stencilEntry> stencil_;
    /** @brief Quantized stencil vectors for FitStencils() */
    std::vector<stencilQVec>  stencil_table_;

    /** @brief Quadrature weights for computing integrals over the cell */
    std::vector<double> quad_dx_;
    /** @brief Quadrature weights for computing integrals over the cell */
    std::vector<double> quad_dy_;

    /** @brief Point spread function (PSF) used to sample the input image */
    //double (*Psf)(double, double, const void*);
    /** @brief User-defined parameters to pass to Psf */
    void const *psf_param_;
    /** @brief Support radius of the PSF */
    double psf_radius_;
};

}

#endif // STENCIL_SET_HPP
