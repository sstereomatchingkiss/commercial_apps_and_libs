#ifndef GCSINTERPOLATION_HPP
#define GCSINTERPOLATION_HPP

#include <functional>
#include <memory>
#include <vector>

namespace cv {

class Mat;

}

namespace OCV{

class stencilSet;
class stencilInterpolation;

class gcsInterpolation
{
public:
    explicit gcsInterpolation(double psf_sigma = 0.35);
    ~gcsInterpolation();

    gcsInterpolation(gcsInterpolation const&) = delete;
    gcsInterpolation& operator=(gcsInterpolation const&) = delete;

    void interpolate(cv::Mat &input, cv::Mat &output, float x_scale_factor, float y_scale_factor, double psf_sigma = 0.35);

    void set_create_stencils_strategy(std::function<void(stencilSet&)> const &functor);    

private:
    void reset();

private:
    std::vector<int> best_stencils_;

    std::function<void(stencilSet&)> create_stencils_strategy_;

    int const filter_scale_factor_ = 2;

    double psf_sigma_;

    std::vector<float> rho_sample_;

    std::unique_ptr<stencilSet> stencil_set_;
    std::unique_ptr<stencilInterpolation> stencil_interp_;
};

}

#endif // GCSINTERPOLATION_HPP
