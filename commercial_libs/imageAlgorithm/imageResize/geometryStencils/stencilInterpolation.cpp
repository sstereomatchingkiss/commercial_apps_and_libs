#include <cmath>

#include <opencv2/core/core.hpp>

#include "../../basicImageAlgo.hpp"
#include "utility/getDataHelper.hpp"
#include "stencilInterpolation.hpp"
#include "stencilSet.hpp"

#ifdef UNIT_TEST
#include <iostream>
#include "debugHelper.hpp"
#endif

namespace
{

float const PI = 3.14159265358979323846264338327950288;

/**
* @brief Boundary handling function for constant extension
* @param n is the data length
* @param i is an index into the data
* @return an index that is always between 0 and n - 1
*/
static int const_extension(int n, int i)
{
    if(i < 0){
        return 0;
    }
    else if(i >= n){
        return n - 1;
    }

    return i;
}

/** @brief The oriented function rho used in local reconstructions */
static double rho(double x, double y, double theta,
                  float sigma_tangent, float sigma_normal, float anisotropy)
{
    sigma_normal = anisotropy * sigma_normal + (1 - anisotropy) * sigma_tangent;
    double const t = (std::cos(theta) * x + std::sin(theta) * y)  / sigma_tangent;
    double const n = (-std::sin(theta) * x + std::cos(theta) * y) / sigma_normal;

    return std::exp(-0.5 * (t * t + n * n));
}

/** @brief Compute samples of the PSF */
static std::vector<double> const sample_psf(double (*psf)(double, double, const void*),
                                            const void *psf_param, int r, double step)
{
    int const W = 2 * r + 1;
    std::vector<double> samples(W * W);

    if(!psf){
        samples[0] = 1 / (step * step);
    }
    else{
        for(int j = -r, offset = 0; j <= r; j++){
            for(int i = -r; i <= r; i++, offset++){
                samples[offset] = psf(step * i, step * j, psf_param);
            }
        }
    }

    return samples;
}

/** @brief Evaluate x^2 */
static inline float sqr(float x)
{
    return x * x;
}

/** @brief The window function, a cubic B-spline */
static float window(float x)
{
    x = std::abs(x) / (static_cast<float>(OCV::stencilInterConst::WINDOWRADIUS) / 2);

    if(x < 1){
        return (x / 2 - 1) * x * x + 0.66666666666666667f;
    }
    else if(x < 2){
        x = 2 - x;
        return x * x * x / 6;
    }

    return 0;
}

}

namespace OCV{

stencilInterpolation::stencilInterpolation(stencilSet &stencil_set, float rho_sigma_tangent, float rho_sigma_normal)
{            
    re_initialize(stencil_set, rho_sigma_tangent, rho_sigma_normal);
}

stencilInterpolation::~stencilInterpolation()
{

}

/**
 * @brief Scale a color image by a possibly non-integer scale factor
 * @param Output the output image
 * @param Stencil the best-fitting stencils \f$\mathcal{S}^\star\f$
 * @param Input the input image in row-major interleaved order
 * @param ScaleFactor scale factor between input and output images
 * @param CenteredGrid use centered grid if nonzero or top-left otherwise
 * @return 1 on success, 0 on failure
 *
 * This routine implements for a possibly non-integer scale factor the
 * interpolation formula
 * \f[ u(x) = \sum_{k \in \mathbb{Z}^2} w(x-k) \Bigl[ v_k +
 *     \sum_{m\in\mathcal{N}} c_m^k \,
 *     \rho_{\mathcal{S}^\star(k)}^m(x - m - k) \Bigr], \f]
 * where the coefficients \f$c_m^k\f$ are computed as
 * \f[ c_m^k = \sum_{n\in\mathcal{N}} (A_{\mathcal{S}^\star(k)}^{-1})_{m,n}
 *     (v_{k+n} - v_k), \quad m \in \mathcal{N}. \f]
 * For more accurate deconvolution, Prefilter() should be called first to
 * prefilter the input image.
 *
 * In the code, the mathematical meanings of the variables are as follows:
 *  - Output: \f$u\f$
 *  - Input: \f$v\f$
 *  - Coeff[3*(NUMNEIGH + 1)*k + m]: \f$c_m^k\f$
 *  - SInterp->StencilInterp[S].Matrix: \f$A_{\mathcal{S}}^{-1}\f$
 *  - WindowWeight: \f$w(x-k)\f$
 *
 * For computational efficiency, RhoFast() is used to evaluate \f$\rho\f$
 * instead of Rho(), as \f$\rho\f$ needs to be evaluated here many times.
 */
void stencilInterpolation::arbitrary_scale(cv::Mat const &input, cv::Mat &output,
                                           std::vector<int> const &stencil, float y_scale_factor,
                                           float x_scale_factor, bool centered_grid)
{
    if(!OCV::is_float_channel(input.type())){
        throw std::runtime_error("input should be float channel");
    }

    std::vector<float> const coeff = compute_coefficient(input, stencil);
    output.create(input.rows * y_scale_factor, input.cols * x_scale_factor, input.type());
    std::vector<float> u(input.channels());
    std::vector<float> uk(input.channels());
    float x_start, y_start;
    if(centered_grid){
        x_start = (1 / x_scale_factor - 1
                   + (input.cols - output.cols / x_scale_factor)) / 2;
        y_start = (1 / y_scale_factor - 1
                   + (input.rows - output.rows / y_scale_factor)) / 2;
    }
    else{
        x_start = y_start = 0;
    }
    float window_weight_x[2*stencilInterConst::WINDOWRADIUS], window_weight_y[2*stencilInterConst::WINDOWRADIUS];       

    for(int y = 0, k = 0; y < output.rows; ++y){
        float const Y = y_start + y / y_scale_factor;
        int iy = static_cast<int>(std::ceil(Y - stencilInterConst::WINDOWRADIUS));       

        for(int n = 0; n < 2 * stencilInterConst::WINDOWRADIUS; ++n){
            window_weight_y[n] = window(Y - iy - n);
        }

        auto *output_ptr = get_pointer<float>(output, y);        
        for(int x = 0; x < output.cols; ++x, k += input.channels()){
            float X = x_start + x / x_scale_factor;
            int ix = static_cast<int>(std::ceil(X - stencilInterConst::WINDOWRADIUS));

            for(int n = 0; n < 2 * stencilInterConst::WINDOWRADIUS; ++n){
                window_weight_x[n] = window(X - ix - n);
            }

            float denom_sum = 0;
            std::fill(std::begin(u), std::end(u), 0);

            for(int my = 0; my < 2 * stencilInterConst::WINDOWRADIUS; ++my){
                if((iy + my) < 0 || (iy + my) >= input.rows){
                    continue;
                }

                float const yp = Y - (iy + my);
                int i = ix + input.cols * (iy + my);                

                for(int mx = 0; mx < 2 * stencilInterConst::WINDOWRADIUS; ++mx, ++i){
                    if((ix + mx) < 0 || (ix + mx) >= input.cols){
                        continue;
                    }

                    float const xp = X - (ix + mx);
                    auto const *coeff_ptr = &coeff[0] + i * input.channels() * (stencilInterConst::NUMNEIGH + 1);                    
                    int const S = stencil[i];
                    std::copy(coeff_ptr, coeff_ptr + input.channels(), std::begin(uk));
                    for(int ny = -stencilInterConst::NEIGHRADIUS, n = 0; ny <= stencilInterConst::NEIGHRADIUS; ++ny){
                        for(int nx = -stencilInterConst::NEIGHRADIUS; nx <= stencilInterConst::NEIGHRADIUS; ++nx, ++n){
                            float const weight = rho_fast(xp - nx, -(yp + ny),
                                                          stencil_inter_entry_[S].orientation_[n],
                                                          rho_sigma_tangent_, rho_sigma_normal_,
                                                          stencil_inter_entry_[S].anisotropy_);
                            int const offset = input.channels() * (n + 1);
                            for(int i = 0; i != input.channels(); ++i){
                                uk[i] += coeff_ptr[offset + i] * weight;                                
                            }
                        }
                    }

                    float const window_weight = window_weight_x[mx] * window_weight_y[my];
                    denom_sum += window_weight;
                    for(int i = 0; i != input.channels(); ++i){
                        u[i] += window_weight * uk[i];                        
                    }
                }
            }

            for(int i = 0; i != input.channels(); ++i){
                output_ptr[i] = u[i] / denom_sum;                
            }
            output_ptr += input.channels();
        }
    }    
}

/**
 * @brief Precompute samples of \f$\Tilde\rho\f$ for an integer scale factor
 * @param scale_factor the integer scale factor
 * @param centered_grid use centered grid if nonzero or top-left otherwise
 * @return pointer to samples, or NULL on failure
 *
 * This routine evaluates samples of
 * \f[ \Tilde{\rho}_{\mathcal{S}}^n(x) = w(x) \sum_{m\in\mathcal{N}}
 *     (A_{\mathcal{S}}^{-1})_{m,n} \, \rho_{\mathcal{S}}^m(x-m). \f]
 * at locations on a fine grid with step size 1/ScaleFactor, where ScaleFactor
 * is an integer.  This precomputation significantly accelerates image scaling
 * by an integer factor.
 *
 * The samples are computed for every stencil \f$\mathcal{S}\in\Sigma\f$ for
 * every neighbor \f$m\in\mathcal{N}\f$ over a square window of size
 * SupportWidth by SupportWidth, where
 *    SupportWidth = 2*WINDOWRADIUS*ScaleFactor
 * or this value minus 1, depending on the choice of grid and the oddness of
 * ScaleFactor.
 */
std::vector<float> stencilInterpolation::compute_rho_samples(int scale_factor, bool centered_grid)
{
    double sample_offset;
    int support_width;
    if(centered_grid && scale_factor % 2 == 0){
        // If using the centered grid and the scale factor is even
        support_width = 2 * stencilInterConst::WINDOWRADIUS * scale_factor;
        sample_offset = 0.5 / scale_factor - stencilInterConst::WINDOWRADIUS;
    }
    else{
        // If using the top-left grid or scale factor is odd
        support_width = 2 * stencilInterConst::WINDOWRADIUS * scale_factor - 1;
        sample_offset = 1.0 / scale_factor - stencilInterConst::WINDOWRADIUS;
    }

    int const support_size = support_width * support_width;
    std::vector<float> rho_samples(stencilInterConst::NUMNEIGH * stencil_set_->get_num_stencils() * support_size);
    double rho_s[stencilInterConst::NUMNEIGH];
    // Compute the samples of the Rho functions
    for(int S = 0; S != stencil_set_->get_num_stencils(); ++S){

        auto const *matrix = stencil_inter_entry_[S].matrix_;
        for(int sy = support_width - 1, i = 0; sy >= 0; --sy){
            for(int sx = 0; sx < support_width; ++sx, ++i){
                double const x = sample_offset + static_cast<double>(sx) / scale_factor;
                double const y = sample_offset + static_cast<double>(sy) / scale_factor;

                for(int n = 0; n != stencilInterConst::NUMNEIGH; ++n){
                    rho_s[n] = 0;
                }
                std::fill(std::begin(rho_s), std::begin(rho_s) + stencilInterConst::NUMNEIGH, 0);

                rho_s[stencilInterConst::NEIGHCENTER] = 1;

                for(int my = -stencilInterConst::NEIGHRADIUS, m = 0; my <= stencilInterConst::NEIGHRADIUS; ++my){
                    for(int mx = -stencilInterConst::NEIGHRADIUS; mx <= stencilInterConst::NEIGHRADIUS; ++mx, ++m){
                        double const weight = rho(x - mx, y - my,
                                                  stencil_inter_entry_[S].orientation_[m],
                                                  rho_sigma_tangent_, rho_sigma_normal_,
                                                  stencil_inter_entry_[S].anisotropy_);

                        for(int ny = -stencilInterConst::NEIGHRADIUS, n = 0; ny <= stencilInterConst::NEIGHRADIUS; ++ny){
                            for(int nx = -stencilInterConst::NEIGHRADIUS; nx <= stencilInterConst::NEIGHRADIUS; ++nx, ++n){
                                if(n != stencilInterConst::NEIGHCENTER){
                                    rho_s[n] += matrix[m + stencilInterConst::NUMNEIGH * n] * weight;
                                }
                            }
                        }
                    }
                }

                for(int n = 0; n != stencilInterConst::NUMNEIGH; ++n){
                    if(n != stencilInterConst::NEIGHCENTER){
                        rho_s[stencilInterConst::NEIGHCENTER] -= rho_s[n];
                    }
                }

                double const weight = window(static_cast<float>(x)) * window(static_cast<float>(y));
                for(int n = 0; n < stencilInterConst::NUMNEIGH; ++n){
                    rho_samples[i + support_size * (n + stencilInterConst::NUMNEIGH * S)]
                            = static_cast<float>(rho_s[n] * weight);
                }
            }
        }
    }

    return rho_samples;
}

/**
 * @brief Compute deconvolution residual for prefiltering
 * @param coarse the coarse image, channel type should be float
 * @param interp the interpolated image, channel type should be float
 * @param residual the residual image, channel type should be float
 * @param scale_factor scale factor between coarse and interpolated images
 * @param centered_grid use centered grid if nonzero or top-left otherwise
 * @param psf_sigma Gaussian PSF standard deviation in units of input pixels
 *
 * This routine computes the deconvolution residual
 * \f[ r = v - \operatorname{sample}(h * u), \f]
 * where v is the coarse image and u is the interpolation.  This residual is
 * used in the iterative refinement procedure implemented in Prefilter().
 */
float stencilInterpolation::deconv_residual(
        cv::Mat const &coarse, cv::Mat const &interp, cv::Mat &residual,
        float scale_factor, float psf_sigma, bool centered_grid)
{        
    if(coarse.type() != interp.type() || !OCV::is_float_channel(coarse.type())){
        throw std::runtime_error("coarse.type() != interp.type() || !is_float_channel(coarse.type())");
    }

    float const psf_radius = 4 * psf_sigma * scale_factor;
    int const psf_width = static_cast<int>(std::ceil(2 * psf_radius));
    std::vector<float> temp(coarse.cols * interp.rows * coarse.channels());
    std::vector<float> psf_buf(psf_width);
    residual.create(coarse.size(), coarse.type());

    float x_start, y_start;
    if(centered_grid){
        x_start = (1 / scale_factor - 1
                   + (coarse.cols - interp.cols / scale_factor)) / 2;
        y_start = (1 / scale_factor - 1
                   + (coarse.rows - interp.rows / scale_factor)) / 2;
    }
    else{
        x_start = y_start = 0;
    }

    float const exp_denom = 2 * sqr(psf_sigma * scale_factor);
    int const coarse_stride = coarse.channels() * coarse.cols;
    std::vector<float> sum(coarse.channels());
    for(int x = 0; x < coarse.cols; ++x){
        float const X = (-x_start + x) * scale_factor;
        int const index_x0 = static_cast<int>(std::ceil(X - psf_radius));

        // Evaluate the PSF
        for(int n = 0; n < psf_width; ++n){
            psf_buf[n] = std::exp(-sqr(X - (index_x0 + n)) / exp_denom);
        }

        for(int y = 0, dest_offset = coarse.channels() * x; y < interp.rows;
            ++y, dest_offset += coarse_stride){            

            std::fill(std::begin(sum), std::end(sum), 0);
            float denom_sum = 0;
            for(int n = 0; n < psf_width; ++n){
                float const weight = psf_buf[n];
                denom_sum += weight;
                auto const *interp_ptr = get_pointer<float>(interp, y, const_extension(interp.cols, index_x0 + n));

                for(int c = 0; c < interp.channels(); ++c){
                    sum[c] += weight * interp_ptr[c];
                }
            }

            for(int c = 0; c < interp.channels(); ++c){
                temp[dest_offset + c] = sum[c] / denom_sum;
            }
        }
    }

    float max_residual = 0;
    for(int y = 0; y != coarse.rows; ++y){
        float const Y = (-y_start + y) * scale_factor;
        int const index_y0 = static_cast<int>(std::ceil(Y - psf_radius));

        // Evaluate the PSF
        for(int n = 0; n < psf_width; ++n){
            psf_buf[n] = std::exp(-sqr(Y - (index_y0 + n)) / exp_denom);
        }

        for(int x = 0; x < coarse_stride; x += coarse.channels()){

            std::fill(std::begin(sum), std::end(sum), 0);
            float denom_sum = 0;
            for(int n = 0; n < psf_width; ++n){
                int const src_offset = x + coarse_stride * const_extension(interp.rows, index_y0 + n);
                float const weight = psf_buf[n];
                denom_sum += weight;

                for(int c = 0; c < coarse.channels(); ++c){
                    sum[c] += weight * temp[src_offset + c];
                }
            }

            auto const *coarse_ptr = coarse.ptr<float>(y);
            auto *residual_ptr     = residual.ptr<float>(y);
            for(int c = 0; c != coarse.channels(); ++c){
                residual_ptr[x + c] = coarse_ptr[x + c] - sum[c] / denom_sum;

                if(std::abs(residual_ptr[x + c]) > max_residual){
                    max_residual = std::abs(residual_ptr[x + c]);
                }
            }
        }
    }

    return max_residual;
}

/**
 * @brief Scale a color image by an integer factor
 * @param output the output image
 * @param stencil the best-fitting stencils \f$\mathcal{S}^\star\f$
 * @param rho_samples precomputed samples of \f$\Tilde\rho\f$
 * @param input the input image in row-major interleaved order(channel must be floating type)
 * @param ScaleFactor scale factor between input and output images
 * @param CenteredGrid use centered grid if nonzero or top-left otherwise
 * @exception throw runtime_error if input channel is not float;throw std::bad_alloc if
 *  out of memory
 *
 * This routine implements for a integer scale factor the interpolation
 * formula
 * \f[ u(x) = \sum_{k \in \mathbb{Z}^2} \biggl[ w(x-k) v_k +
 *     \sum_{n \in\mathcal{N}\backslash\{0\}} (v_{k+n} - v_k) \,
 *     \Tilde{\rho}_{\mathcal{S}^\star(k)}^n(x-k) \biggr]. \f]
 */
void stencilInterpolation::integer_scale_pass(
        cv::Mat const &input, cv::Mat &output,
        std::vector<int> const &stencil, std::vector<float> const &rho_samples,
        int scale_factor, bool centered_grid)
{           
    if(!OCV::is_float_channel(input.type())){
        throw std::runtime_error(std::string(__FUNCTION__) + "\n" + __FILE__
                                 + "\ninput channel should be float");
    }

    int const output_width = scale_factor * input.cols;
    int const output_height = scale_factor * input.rows;
    if(output.rows != output_height || output.cols != output_width || input.type() != output.type()){
        output = cv::Mat::zeros(output_height, output_width, input.type());
    }

    std::vector<float> v(input.channels());
    int support_width, sample_offset;
    if(centered_grid && scale_factor % 2 == 0){
        // If using the centered grid and the scale factor is even
        support_width = 2 * stencilInterConst::WINDOWRADIUS * scale_factor;
        sample_offset = stencilInterConst::WINDOWRADIUS * scale_factor - scale_factor / 2;
    }
    else{
        // If using the top-left grid or scale factor is odd
        support_width = 2 * stencilInterConst::WINDOWRADIUS * scale_factor - 1;
        sample_offset = stencilInterConst::WINDOWRADIUS * scale_factor - 1;

        if(centered_grid){
            sample_offset -= (scale_factor - 1) / 2;
        }
    }

    int const support_size = support_width * support_width;
    int const output_offset = input.channels() * sample_offset * (1 + output_width);

    int interior_lower = (sample_offset + scale_factor - 1) / scale_factor;
    int interior_upper_x = (output_width + sample_offset - support_width + 1) / scale_factor;
    int interior_upper_y = (output_height + sample_offset - support_width + 1) / scale_factor;

    if(interior_lower < stencilInterConst::NEIGHRADIUS){
        interior_lower = stencilInterConst::NEIGHRADIUS;
    }
    if(interior_upper_x > input.cols - stencilInterConst::NEIGHRADIUS - 1){
        interior_upper_x = input.cols - stencilInterConst::NEIGHRADIUS - 1;
    }
    if(interior_upper_y > output_height - stencilInterConst::NEIGHRADIUS - 1){
        interior_upper_y = output_height - stencilInterConst::NEIGHRADIUS - 1;
    }
    // Determine the interior of the image    
    for(int iy = -stencilInterConst::WINDOWRADIUS; iy != input.rows + stencilInterConst::WINDOWRADIUS; ++iy){
        for(int ix = -stencilInterConst::WINDOWRADIUS; ix != input.cols + stencilInterConst::WINDOWRADIUS; ++ix){
            if(!(interior_lower <= ix && ix < interior_upper_x
                 && interior_lower <= iy && iy < interior_upper_y)){
                // General code for near the boundaries of the image
                auto const *rho_sample_ptr = &rho_samples[0] + support_size * stencilInterConst::NUMNEIGH *
                        stencil[const_extension(input.cols, ix)
                        + input.cols * const_extension(input.rows, iy)];

                for(int ny = -stencilInterConst::NEIGHRADIUS; ny <= stencilInterConst::NEIGHRADIUS; ++ny){
                    for(int nx = -stencilInterConst::NEIGHRADIUS; nx <= stencilInterConst::NEIGHRADIUS; ++nx){

                        auto const *input_ptr = get_pointer<float>(input, const_extension(input.rows, iy - ny), const_extension(input.cols, ix + nx));
                        int y = scale_factor * iy - sample_offset;                        
                        for(int sy = 0; sy < support_width;
                            ++sy, ++y, rho_sample_ptr += support_width){
                            if(0 <= y && y < output_height){

                                int x = scale_factor * ix - sample_offset;                               
                                auto output_ptr = get_pointer<float>(output, y, x);                                                              
                                for(int sx = 0; sx < support_width;
                                    ++sx, ++x, output_ptr += input.channels()){

                                    if(0 <= x && x < output_width){
                                        float const weight = rho_sample_ptr[sx];                                        
                                        for(int i = 0; i != input.channels(); ++i){
                                            output_ptr[i] += weight * input_ptr[i];
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else{
                // Faster code for the interior of the image
                auto const *rho_sample_ptr = &rho_samples[0] +
                        support_size * stencilInterConst::NUMNEIGH * stencil[ix + input.cols * iy];                
                for(int ny = 0; ny != stencilInterConst::NEIGHDIAMETER; ++ny){
                    auto const *input_ptr = get_pointer<float>(input, iy - ny + stencilInterConst::NEIGHRADIUS, ix - stencilInterConst::NEIGHRADIUS);
                    for(int nx = 0; nx < input.channels() * stencilInterConst::NEIGHDIAMETER;){

                        for(int i = 0; i != input.channels(); ++i){
                            v[i] = input_ptr[nx++];                            
                        }

                        for(int sy = 0; sy != support_width; ++sy, rho_sample_ptr += support_width){
                            auto *output_ptr = get_pointer<float>(output, scale_factor * iy + sy, scale_factor * ix) - output_offset;                           
                            for(int sx = 0; sx != support_width; ++sx){
                                float const weight = rho_sample_ptr[sx];
                                for(int j = 0; j != input.channels(); ++j){
                                    output_ptr[j] += weight * v[j];
                                }
                                output_ptr += input.channels();                                
                            }
                        }
                    }
                }
            }
        }
    }   
}

/**
 * @brief Compute the coefficients c_m^k for every pixel which needed by the function arbitrary_scale
 * @param input the input image
 * @param Stencil the best-fitting stencils \f$\mathcal{S}^\star\f$
 * @return return the coefficients
 */
std::vector<float> const stencilInterpolation::compute_coefficient(cv::Mat const &input, std::vector<int> const &stencil)
{
    std::vector<float> coeff((stencilInterConst::NUMNEIGH + 1) * input.channels() * input.total());
    std::vector<float> v(input.channels());
    std::vector<float> c(input.channels() * (stencilInterConst::NUMNEIGH + 1));
    /* Compute the coefficients c_m^k for every pixel */
    for(int y = 0, k = 0; y != input.rows; ++y){
        auto const *input_ptr = get_pointer<float>(input, y);
        for(int x = 0; x != input.cols; ++x, ++k){
            int const S = stencil[x + input.cols * y];
            auto const *matrix = stencil_inter_entry_[S].matrix_;
            for(int i = 0; i != input.channels(); ++i){
                c[i] = *input_ptr;
                ++input_ptr;
            }

            std::fill(std::begin(c) + input.channels(), std::end(c), 0);
            for(int ny = -stencilInterConst::NEIGHRADIUS, n = 0; ny <= stencilInterConst::NEIGHRADIUS; ++ny){
                for(int nx = -stencilInterConst::NEIGHRADIUS; nx <= stencilInterConst::NEIGHRADIUS; ++nx, ++n){
                    if(nx != 0 || ny != 0){
                        auto const *input_ptr_2 = get_pointer<float>(input, const_extension(input.rows, y - ny), const_extension(input.cols, x + nx));
                        for(int i = 0; i != input.channels(); ++i){
                            v[i] = input_ptr_2[i] - c[i];
                        }

                        /* Compute c_m^k */
                        for(int m = 0; m < stencilInterConst::NUMNEIGH; m++){
                            for(int i = 0; i != input.channels(); ++i){
                                c[input.channels() * (m + 1) + i] += matrix[m + stencilInterConst::NUMNEIGH * n] * v[i];
                            }
                        }
                    }
                }
            }

            /* Save the coefficients */
            size_t const temp = input.channels() * (stencilInterConst::NUMNEIGH + 1);
            std::copy(std::begin(c), std::begin(c) + temp, std::begin(coeff) + temp * k);
        }
    }

    return coeff;
}

void stencilInterpolation::interpolate_stencil()
{
    std::vector<double> psf_samples(sample_psf(stencil_set_->get_psf(), stencil_set_->get_psf_param(),
                                               static_cast<int>(std::ceil(stencilInterConst::QUAD_RES * stencil_set_->get_psf_radius())),
                                               1.0 / stencilInterConst::QUAD_RES));

    for(int S = 0; S != stencil_set_->get_num_stencils(); ++S){
        interpolate_stencil(S, psf_samples);
    }
}

/**
 * @brief Compute interpolation information for a stencil
 * @param stencil_index index of the stencil to interpolate
 * @param PsfSamples precomputed samples of the PSF
 * @return 1 on success, 0 on failure
 *
 * The orientation and anisotropy of the \f$\rho_\mathcal{S}^n\f$ synthesis
 * functions are computed,
 * \f[\begin{aligned}
 *    \theta_\mathcal{S}^n &= \angle \int_{[-\frac{1}{2},+\frac{1}{2}]^2}
 *    \nabla\varphi_\mathcal{S}^\perp \bigl(x - n) \, dx, \\
 *    \mu_\mathcal{S} &= \min_{n\in \mathcal{N}}\,
 *    \Bigl\lvert\int_{[-\frac{1}{2},+\frac{1}{2}]^2} \nabla\varphi_\mathcal{S}
 *    \bigl(x - n) \, dx\Bigr\rvert.
 *    \end{aligned}\f]
 * The interpolation matrix
 * \f[ (A_\mathcal{S})_{m,n} = (h * \rho^n_\mathcal{S})(m - n),
 *     \quad m,n\in\mathcal{N} \f]
 * is computed and inverted with InvertMatrix().
 */
void stencilInterpolation::interpolate_stencil(int stencil_index, std::vector<double> const &psf_samples)
{    
    if(stencil_index < 0 || stencil_index >= stencil_set_->get_num_stencils()){
        throw std::runtime_error("stencil_index < 0 || stencil_index >= stencil_set_->get_num_stencils()");
    }

    int const R = static_cast<int>(std::ceil(stencilInterConst::QUAD_RES * stencil_set_->get_psf_radius()));
    double min_mag = 1e10;
    double A[stencilInterConst::NUMNEIGH * stencilInterConst::NUMNEIGH];

    //Compute orientations for the synthesis functions
    for(int ny = -stencilInterConst::NEIGHRADIUS, n = 0; ny <= stencilInterConst::NEIGHRADIUS; ++ny){
        for(int nx = -stencilInterConst::NEIGHRADIUS; nx <= stencilInterConst::NEIGHRADIUS; ++nx, ++n){

            double beta = 0;
            for(int y = 0; y <= stencilInterConst::QUAD_RES; ++y){
                int weight = (y == 0 || y == stencilInterConst::QUAD_RES) ? 1 : 2;
                double X = nx + 0.5;
                double Y = ny - 0.5 + static_cast<double>(y) / stencilInterConst::QUAD_RES;
                beta += weight * (stencil_set_->eval_phi(stencil_index, X, Y)
                                  - stencil_set_->eval_phi(stencil_index, X - 1, Y));
            }

            double alpha = 0;
            for(int x = 0; x <= stencilInterConst::QUAD_RES; ++x){
                int weight = (x == 0 || x == stencilInterConst::QUAD_RES) ? 1 : 2;
                double X = nx - 0.5 + static_cast<double>(x) / stencilInterConst::QUAD_RES;
                double Y = ny + 0.5;
                alpha -= weight * (stencil_set_->eval_phi(stencil_index, X, Y)
                                   - stencil_set_->eval_phi(stencil_index, X, Y - 1));
            }

            alpha /= (2 * stencilInterConst::QUAD_RES);
            beta  /= (2 * stencilInterConst::QUAD_RES);
            double const mag = alpha * alpha + beta * beta;

            if(mag < min_mag){
                min_mag = mag;
            }

            stencil_inter_entry_[stencil_index].orientation_[n] = static_cast<float>(std::atan2(beta, alpha));

            while(stencil_inter_entry_[stencil_index].orientation_[n] < 0){
                stencil_inter_entry_[stencil_index].orientation_[n] += static_cast<float>(PI);
            }
            while(stencil_inter_entry_[stencil_index].orientation_[n] >= static_cast<float>(PI)){
                stencil_inter_entry_[stencil_index].orientation_[n] -= static_cast<float>(PI);
            }
        }
    }

    min_mag = std::sqrt(min_mag);
    stencil_inter_entry_[stencil_index].anisotropy_ = static_cast<float>(std::pow(min_mag, 4));

    // Compute interpolation matrix A_{m,n} = (Psf * Rho^n)(m - n)
    for(int my = -stencilInterConst::NEIGHRADIUS, m = 0; my <= stencilInterConst::NEIGHRADIUS; ++my){
        for(int mx = -stencilInterConst::NEIGHRADIUS; mx <= stencilInterConst::NEIGHRADIUS; ++mx, ++m){
            for(int ny = -stencilInterConst::NEIGHRADIUS, n = 0; ny <= stencilInterConst::NEIGHRADIUS; ++ny)
                for(int nx = -stencilInterConst::NEIGHRADIUS; nx <= stencilInterConst::NEIGHRADIUS; ++nx, ++n){

                    double sum = 0;
                    for(int y = -R, offset = 0; y <= R; ++y){
                        for(int x = -R; x <= R; ++x, ++offset){
                            sum += psf_samples[offset]
                                    * rho(mx - nx - static_cast<double>(x) / stencilInterConst::QUAD_RES,
                                          my - ny - static_cast<double>(y) / stencilInterConst::QUAD_RES,
                                          stencil_inter_entry_[stencil_index].orientation_[n],
                                          rho_sigma_tangent_, rho_sigma_normal_,
                                          stencil_inter_entry_[stencil_index].anisotropy_);
                        }
                    }

                    A[m + stencilInterConst::NUMNEIGH * n] = sum / (stencilInterConst::QUAD_RES * stencilInterConst::QUAD_RES);
                }
        }
    }

    double inverse_a[stencilInterConst::NUMNEIGH * stencilInterConst::NUMNEIGH];
    cv::Mat_<double> mat_a(stencilInterConst::NUMNEIGH, stencilInterConst::NUMNEIGH, A, sizeof(double) * stencilInterConst::NUMNEIGH);
    cv::Mat_<double> mat_inverse_a(stencilInterConst::NUMNEIGH, stencilInterConst::NUMNEIGH, inverse_a, sizeof(double) * stencilInterConst::NUMNEIGH);
    cv::invert(mat_a, mat_inverse_a, cv::DECOMP_SVD);

    for(int n = 0; n !=  stencilInterConst::NUMNEIGH * stencilInterConst::NUMNEIGH; ++n){
        stencil_inter_entry_[stencil_index].matrix_[n] = static_cast<float>(inverse_a[n]);
    }
}

/**
 * @brief Prefilter a color image to improve deconvolution accuracy
 * @param RhoSamples precomputed samples of \f$\Tilde\rho\f$
 * @param Input the input image in row-major interleaved order
 * @param Filtered the prefiltered image
 * @param Stencil the best-fitting stencils \f$\mathcal{S}^\star\f$
 * @param ScaleFactor scale factor between input and output images
 * @param CenteredGrid use centered grid if nonzero or top-left otherwise
 * @param PsfSigma Gaussian PSF standard deviation in units of input pixels
 * @param NumPasses number of passes of iterative refinement to perform
 *
 * This routine performs the iterative refinement prefiltering
 * \f[ v^0 = v, \quad v^{i+1} = v^i
 *     + \bigl(v - \operatorname{sample}(h*\mathcal{R}v^i)\bigr). \f]
 * Interpolation with a prefiltered image improves the resulting
 * deconvolution accuracy.  Interpolation is performed using the prefiltered
 * image as input with either IntegerScalePass() for an integer scale factor or
 * by ArbitraryScale() for a non-integer scale factor.
 *
 * Prefiltering is implemented with IntegerScalePass() to perform the
 * interpolations \f$\mathcal{R}v^i\f$ and DeconvResidual() to compute the
 * deconvolution residuals \f$v - \operatorname{sample}(h*\mathcal{R}v^i)\f$.
 */
void stencilInterpolation::pre_filter(cv::Mat const &input, cv::Mat &filtered,
                                      std::vector<int> const &stencil, std::vector<float> const &rho_samples,
                                      int scale_factor, float psf_sigma, int num_passes, bool centered_grid)
{            
    cv::Mat interp;
    cv::Mat residual = cv::Mat::zeros(input.rows, input.cols, input.type());
    input.copyTo(filtered);    
    for(int pass = 0; pass < num_passes; ++pass){
        if(pass == 0){
            integer_scale_pass(input, interp, stencil, rho_samples,
                               scale_factor, centered_grid);            
        }
        else{
            integer_scale_pass(residual, interp, stencil, rho_samples,
                               scale_factor, centered_grid);
        }

        float const max_residual = deconv_residual(input, interp, residual,
                                                   scale_factor, psf_sigma, centered_grid);
        if(max_residual < 0){
            return;
        }        

        filtered += residual;
    }    
}

void stencilInterpolation::re_initialize(stencilSet &stencil_set, float rho_sigma_tangent, float rho_sigma_normal)
{
    rho_sigma_tangent_ = rho_sigma_tangent;
    rho_sigma_normal_  = rho_sigma_normal;
    stencil_set_ = &stencil_set;
    stencil_inter_entry_.resize(stencil_set_->get_num_stencils());
}

void stencilInterpolation::reset_stencil_set(stencilSet &stencil_set)
{
    stencil_set_ = &stencil_set;
    stencil_inter_entry_.resize(stencil_set_->get_num_stencils());
}

/** @brief The oriented function rho used in local reconstructions */
float stencilInterpolation::rho_fast(float x, float y, float theta, float sigma_tangent,
                                     float sigma_normal, float anisotropy)
{    
    static const float exp_arg_scale = 37.0236f;
    static const float trig_scale = static_cast<float>((stencilInterConst::TRIG_TABLE_SIZE - 1) / PI);

    if(table_cos_.empty() || table_exp_.empty() || table_sin_.empty()){
        table_cos_.resize(stencilInterConst::TRIG_TABLE_SIZE);
        table_exp_.resize(stencilInterConst::EXP_TABLE_SIZE);
        table_sin_.resize(stencilInterConst::TRIG_TABLE_SIZE);

        // Precompute exp and trig tables
        for(int i = 0; i != stencilInterConst::EXP_TABLE_SIZE; ++i){
            table_exp_[i] = std::exp(-(i - 0.5) / exp_arg_scale);
        }

        for(int i = 0; i != stencilInterConst::TRIG_TABLE_SIZE; ++i){
            table_cos_[i] = std::cos(i / trig_scale);
            table_sin_[i] = std::sin(i / trig_scale);
        }       
    }

    sigma_normal = anisotropy * sigma_normal + (1 - anisotropy) * sigma_tangent;

    int const i = static_cast<int>(theta * trig_scale + 0.5f);
    //int const i = temp < stencilInterConst::TRIG_TABLE_SIZE ? temp : stencilInterConst::TRIG_TABLE_SIZE - 1;
    float t = (table_cos_[i] * x + table_sin_[i] * y) / sigma_tangent;
    float const n = (-table_sin_[i] * x + table_cos_[i] * y) / sigma_normal;    
    t = (t * t + n * n) * exp_arg_scale / 2;

    if(t >= stencilInterConst::EXP_TABLE_SIZE){
        return 0;
    }
    else{
        return table_exp_[static_cast<int>(t)];
    }
}

}
