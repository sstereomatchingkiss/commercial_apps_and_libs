#include <cmath>

#include <opencv2/core/core.hpp>

#include "basicImageAlgo.hpp"
#include "extraAlgorithm.hpp"
#include "gcsInterpolation.hpp"
#include "imageAlgorithm.hpp"
#include "stencilInterpolation.hpp"
#include "stencilSet.hpp"

#ifdef UNIT_TEST
#include <iostream>
#endif

namespace{

float const PI_4 = 0.785398163397448309615660845819875721;

void default_stencils_creator(OCV::stencilSet &set);

double linear_patch(double, double y,
                    const void*);

/* Math constants (Hart & Cheney) */
double const m_2_pi = 6.28318530717958647692528676655900576;

/** @brief Distance function for a corner stencil */
double corner(double x, double y, const void*)
{
    return (x < y) ? x : y;
}

/** @brief Distance function for a T-junction stencil */
double curve_dist(double x, double y, const void *Param)
{
    const double a = *(reinterpret_cast<double const*>(Param));
    const double a2 = a * a;
    const double xa = std::abs(x) / a2;
    const double z = (1 - a * y) * (2 / ( 3 * a2));
    double x0, Temp;

    if((Temp = xa * xa + z * z * z) >= 0)
    {
        Temp = std::sqrt(Temp);
        x0 = std::pow(xa + Temp, 1.0/3.0)
                + ((a * y < 1) ? -1:1) * std::pow(std::abs(-xa + Temp), 1.0 / 3.0);
    }
    else
    {
        Temp = std::sqrt(-z);
        x0 =  2 * Temp * std::cos(std::acos(xa / (-z * Temp)) / 3);
    }

    Temp = x0 * x0;
    return std::sqrt(a2 * Temp + 1) * ((a / 2) * Temp - y);
}

void default_stencils_creator(OCV::stencilSet &set)
{
    /** @brief The constant pi/8 */
    double const m_pi_8 = 0.39269908169872415480783042290993786;
    /** @brief The constant 1/sqrt(2) */
    static double const m_1_sqrt_2 = 0.70710678118654752440084436210484904;

    for(int S = 0; S < 32; ++S){
        set.add_stencil(linear_patch, nullptr, S * m_pi_8 / 4);
    }

    for(int S = 0; S < 8; ++S){
        set.add_stencil(curve_dist, &m_1_sqrt_2, S * PI_4);
    }

    static const double a2 = 1;
    for(int S = 0; S < 8; ++S){
        set.add_stencil(curve_dist, &a2, S * PI_4);
    }

    for(int S = 0; S < 8; ++S){
        set.add_stencil(corner, nullptr, S * PI_4);
    }
}

/** @brief The point spread function (PSF) */
double gaussian_psf(double x, double y, const void *Param)
{
    const double Sigma = *(reinterpret_cast<double const*>(Param));
    const double SigmaSqr = Sigma * Sigma;

    return std::exp(-(x * x + y * y) / (2.0 * SigmaSqr)) / (m_2_pi * SigmaSqr);
}

/** @brief Distance function for a line-shaped stencil */
double linear_patch(double, double y,
                    const void*)
{
    return y;
}

}

namespace OCV{

gcsInterpolation::gcsInterpolation(double psf_sigma) : create_stencils_strategy_(default_stencils_creator), psf_sigma_(psf_sigma)
{
}

gcsInterpolation::~gcsInterpolation()
{

}

void gcsInterpolation::interpolate(cv::Mat &input, cv::Mat &output, float x_scale_factor, float y_scale_factor, double psf_sigma)
{
    if(psf_sigma > 1.0){
        psf_sigma = 1.0;
    }else if(psf_sigma <= 0){
        psf_sigma = 0.1;
    }
    if(!fuzzy_compare(psf_sigma, psf_sigma_) || !stencil_set_ || !stencil_interp_){
        psf_sigma_ = psf_sigma;
        reset();
    }

    std::vector<int> const best_stencil = stencil_set_->fit_stencils(input);

    int const type = OCV::mat8u_to_mat32f(input, 1 / 255.0);
    cv::Mat filtered;
    int const refinement_time = 2;
    stencil_interp_->pre_filter(input, filtered, best_stencil,
                                rho_sample_, filter_scale_factor_,
                                psf_sigma_, refinement_time);

    float temp;
    if(std::modf(x_scale_factor, &temp) == 0.0 && std::modf(y_scale_factor, &temp) == 0.0 &&
            static_cast<size_t>(x_scale_factor) == static_cast<size_t>(y_scale_factor)){

        stencil_interp_->integer_scale_pass(filtered, output,
                                            best_stencil, rho_sample_,
                                            x_scale_factor);
    }else{
        stencil_interp_->arbitrary_scale(filtered, output, best_stencil,
                                         x_scale_factor, y_scale_factor,
                                         true);
    }

    OCV::mat32f_to_mat8u(output, 255);
    if(type != -1){
        OCV::mat32f_to_mat8u(input, 255);
    }
}

void gcsInterpolation::set_create_stencils_strategy(std::function<void(stencilSet&)> const &functor)
{
    create_stencils_strategy_ = functor;
    reset();
}

/**********************************************************
 ****************** implementation ************************
 **********************************************************/

void gcsInterpolation::reset()
{
    stencil_set_.reset(new stencilSet(2, gaussian_psf, &psf_sigma_, 4 * psf_sigma_));

    //create_stencils_strategy_(*stencil_set_);
    default_stencils_creator(*stencil_set_);

    double const sigma_tangent = 1.2;
    double const sigma_normal = 0.6;
    stencil_interp_.reset(new stencilInterpolation(*stencil_set_, sigma_tangent, sigma_normal));
    stencil_interp_->interpolate_stencil();

    rho_sample_ = stencil_interp_->compute_rho_samples(filter_scale_factor_, true);
}

}
