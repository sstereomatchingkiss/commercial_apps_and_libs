#ifndef STENCILCONST_HPP
#define STENCILCONST_HPP

namespace OCV{

struct stencilInterConst
{
    /** @brief The size of the synthesis neighborhood \f$\mathcal{N}\f$ */
    static int const NEIGHRADIUS = 1;
    /** @brief Quadrature resolution to use for integrals */
    static int const QUAD_RES = 8;
    /** @brief Size of the trignometry tables used in RhoFast */
    static int const TRIG_TABLE_SIZE = 256;
    /** @brief Size of the exponential table used in RhoFast */
    static int const EXP_TABLE_SIZE = 1024;

    /* Quantities derived from NEIGHRADIUS */
    /** @brief Support radius of the window function \f$w(x)\f$ */
    static int const WINDOWRADIUS = 1 + NEIGHRADIUS;
    /** @brief Diameter of the neighborhood */
    static int const NEIGHDIAMETER = 1 + 2 * NEIGHRADIUS;
    /** @brief Index of the neighborhood center */
    static int const NEIGHCENTER = NEIGHRADIUS + NEIGHRADIUS * NEIGHDIAMETER;
    /** @brief Number of neighbors in the neighborhood, \f$|\mathcal{N}|\f$ */
    static int const NUMNEIGH = NEIGHDIAMETER * NEIGHDIAMETER;
};

}

#endif // STENCILCONST_HPP
