#ifndef STENCIL_INTERPOLATION_HPP
#define STENCIL_INTERPOLATION_HPP

namespace cv {

class Mat;

}

#include <vector>

#include "stencilConst.hpp"

namespace OCV{

class stencilSet;

class stencilInterpolation
{
public:    
    stencilInterpolation(stencilSet &stencil_set, float rho_sigma_tangent, float rho_sigma_normal);

    ~stencilInterpolation();

    void arbitrary_scale(cv::Mat const &input, cv::Mat &output,
                         std::vector<int> const &stencil, float y_scale_factor,
                         float x_scale_factor, bool centered_grid = true);

    std::vector<float> compute_rho_samples(int scale_factor, bool centered_grid = true);

    float deconv_residual(cv::Mat const &coarse, cv::Mat const &interp, cv::Mat &residual,
                          float scale_factor, float psf_sigma, bool centered_grid = true);

    void integer_scale_pass(cv::Mat const &input, cv::Mat &output,
                            std::vector<int> const &stencil, std::vector<float> const &rho_samples,
                            int scale_factor, bool centered_grid = true);
    void interpolate_stencil();

    void pre_filter(cv::Mat const &input, cv::Mat &filtered,
                    std::vector<int> const &stencil, std::vector<float> const &rho_samples,
                    int scale_factor, float psf_sigma, int num_passes, bool centered_grid = true);

    void re_initialize(stencilSet &stencil_set, float rho_sigma_tangent, float rho_sigma_normal);
    void reset_stencil_set(stencilSet &stencil_set);

#ifndef UNIT_TEST
private:
#endif
    std::vector<float> const compute_coefficient(cv::Mat const &input, std::vector<int> const &stencil);

    void interpolate_stencil(int stencil_index, std::vector<double> const &psf_samples);

    float rho_fast(float x, float y, float theta,
                   float sigma_tangent, float sigma_normal, float anisotropy);

#ifndef UNIT_TEST
private:
#endif
    struct stencilInterpolateEntry
    {
        /** @brief Synthesis function orientations \f$\theta_\mathcal{S}^n\f$ */
        float orientation_[stencilInterConst::NUMNEIGH];
        /** @brief Interpolation matrix \f$A_\mathcal{S}^{-1}\f$ */
        float matrix_[stencilInterConst::NUMNEIGH * stencilInterConst::NUMNEIGH];
        /** @brief Anisotropy \f$\mu_\mathcal{S}\f$ */
        float anisotropy_;
    };

#ifndef UNIT_TEST
private:
#endif
    float rho_sigma_normal_;
    float rho_sigma_tangent_;

    std::vector<stencilInterpolateEntry> stencil_inter_entry_;
    stencilSet *stencil_set_;

    std::vector<float> table_cos_;
    std::vector<float> table_exp_;
    std::vector<float> table_sin_;
};

}

#endif // STENCIL_INTERPOLATION_HPP
