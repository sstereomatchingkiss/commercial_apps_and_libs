#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "hqx/hqxAlgo.hpp"

namespace OCV{

/*
 * apply hqx(2x,3x,4x) on bgr source
 */
cv::Mat const hqx_algo(cv::Mat const &src, int scale)
{
    cv::Mat mat = src;

    if(scale >= 2 && scale <= 4)
    {
        if(mat.channels() == 4 || mat.channels() == 1){
            cv::cvtColor(mat, mat, CV_BGRA2BGR);
        }
        int const type = src.type();
        hqxAlgo<> hqx;
        cv::Mat_<cv::Vec3i> input = mat;

        if(scale == 2){
            mat = hqx.hq2x_bgr(input);
        }
        else if(scale == 3){
            mat = hqx.hq3x_bgr(input);
        }
        else if(scale == 4){
            mat = hqx.hq4x_bgr(input);
        }

        mat.convertTo(mat, type);
    }

    return mat;
}

}
