#include <array>

#include <QList>
#include <QString>
#include <QtTest>

#include "basicAlgorithms.hpp"
#include "extraAlgorithm.hpp"
#include "stlAlgoWrapper.hpp"
#include "stlAlgoReimplement.hpp"
#include "timeEstimate.hpp"

class AlgorithmUnitTestTest : public QObject
{
    Q_OBJECT
    
public:
    AlgorithmUnitTestTest();    
    
private Q_SLOTS:
    void test_insertion_sort();
    void test_max_is_arith();
    void test_median();
    void test_partial_maximum_array();
    void test_unguarded_sort();

private:
    template<typename T = size_t, size_t N = 10>
    std::array<T, N> create_random_array();
};

AlgorithmUnitTestTest::AlgorithmUnitTestTest()
{
}

void AlgorithmUnitTestTest::test_insertion_sort()
{
    auto array1 = create_random_array();
    auto array2 = array1;

    sort(array1);
    insertion_sort(std::begin(array2), std::begin(array2) );

    bool const result = std::equal(std::begin(array1), std::begin(array1), std::begin(array2));
    QCOMPARE(result, true);
}

void AlgorithmUnitTestTest::test_max_is_arith()
{
    char num_char = -34;
    double num_double = 99.345;
    float num_float = 33.22342;
    int num_int = 77;
    unsigned char num_uchar = 55;

    double result = max_is_arith(num_char, num_double, num_float, num_int, num_uchar);
    QCOMPARE(result, 99.345);
}

void AlgorithmUnitTestTest::test_median()
{
    size_t array[] = {1, 2, 3};
    bool next_permutation = true;
    while(next_permutation){
        size_t const actual = median<size_t>(array[0], array[1], array[2]);
        size_t const expected = 2;
        QCOMPARE(actual, expected);
        next_permutation = std::next_permutation(std::begin(array), std::end(array) );
    }
}

void AlgorithmUnitTestTest::test_partial_maximum_array()
{
    int array[] = {13, -3, -25, 20, -3, -16, -23, 18, 20, -7, 12, -5, -22, 15, -4, 7};
    auto const it = partial_maximum_subarray(std::begin(array), std::end(array), 0);

    QCOMPARE(43, std::get<2>(it) );
    QCOMPARE(*std::get<0>(it), 18);
    QCOMPARE(*std::get<1>(it), -5);
}

void AlgorithmUnitTestTest::test_unguarded_sort()
{
    QList<size_t> array = {0, 16, 5, 6, 11, 7, 9, 1, 8, 4, 19, 17, 3, 18, 10, 14, 12, 15, 13, 2};
    unguarded_partition<size_t>(std::begin(array), std::end(array), 2);
    QList<size_t> expected = {0, 2, 1, 6, 11, 7, 9, 5, 8, 4, 19, 17, 3, 18, 10,14, 12, 15, 13, 16};
    QCOMPARE(array, expected);
}

template<typename T, size_t N>
std::array<T, N> AlgorithmUnitTestTest::create_random_array()
{
    srand(0);
    std::array<T, N> array;
    for(auto &data : array){
        data = rand() % 100;
    }

    return array;
}

QTEST_APPLESS_MAIN(AlgorithmUnitTestTest)

#include "tst_algorithmunittesttest.moc"
