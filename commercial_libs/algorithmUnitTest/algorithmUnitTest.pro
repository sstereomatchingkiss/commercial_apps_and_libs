#-------------------------------------------------
#
# Project created by QtCreator 2013-02-09T14:14:04
#
#-------------------------------------------------

QT       += testlib

QT       -= gui

TARGET = tst_algorithmunittesttest
CONFIG   += console
CONFIG   += c++11
CONFIG   -= app_bundle

TEMPLATE = app

INCLUDEPATH += ../algorithms

SOURCES += tst_algorithmunittesttest.cpp
DEFINES += SRCDIR=\\\"$$PWD/\\\"

HEADERS += \
    ../algorithms/variadicTemplateAlgo.hpp \
    ../algorithms/typeTraitsHelp.hpp \
    ../algorithms/timeEstimate.hpp \
    ../algorithms/timeElapsed.hpp \
    ../algorithms/stlAlgoWrapper.hpp \
    ../algorithms/stlAlgoReimplement.hpp \
    ../algorithms/metaProgrammingHelper.hpp \
    ../algorithms/extraAlgorithm.hpp \
    ../algorithms/containerHelper.hpp \
    ../algorithms/basicAlgorithms.hpp
