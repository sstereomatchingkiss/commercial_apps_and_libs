#ifndef THREADSAFEMAP_HPP
#define THREADSAFEMAP_HPP

#include <utility>

#include <QMutex>
#include <QMutexLocker>

/*
 * generic thread safe map, only implement some functions
 * every container which satisfy the concepts of std::map
 * should match this threadSafeVector
 */
template<typename T, typename Mutex = QMutex, typename WriteLocker = QMutexLocker, typename ReadLocker = WriteLocker>
class threadSafeMap
{
public:
    typedef typename T::const_iterator const_iterator;
    typedef typename T::key_type       key_type;
    typedef typename T::mapped_type    mapped_type;
    typedef typename T::iterator       iterator;
    typedef typename T::size_type      size_type;
    typedef typename T::value_type     value_type;

    threadSafeMap();

    threadSafeMap(threadSafeMap && value) noexcept
    {
        WriteLocker guard(&lock_);
        *this = move_rvalue(std::forward<threadSafeMap>(value));
    }

    threadSafeMap& operator=(threadSafeMap &&value) noexcept;

    explicit threadSafeMap(threadSafeMap const&) = delete;
    threadSafeMap& operator=(threadSafeMap const&) = delete;

    void clear ( )
    {
        WriteLocker guard(&lock_);
        map_.clear();
    }

    size_type erase (key_type const &x)
    {
        WriteLocker guard(&lock_);
        return map_.erase(x);
    }

    iterator find (key_type const &x )
    {
        ReadLocker guard(&lock_);
        return map_.find(x);
    }

    const_iterator find (key_type const &x) const
    {
        ReadLocker guard(&lock_);
        return map_.find(x);
    }

    std::pair<iterator,bool> insert (value_type const &x)
    {
        WriteLocker guard(&lock_);
        return map_.insert(std::make_pair(x.first, x.second) );
    }

    /*
     * the thread safe responsible will left for user
     * when they get the value by this function
     */
    mapped_type& operator[] (key_type const &x)
    {
        ReadLocker guard(&lock_);
        return map_[x];
    }

private:
    void move_rvalue(threadSafeMap &&value) noexcept
    {
        map_. = std::move(value);
    }

private:
    mutable Mutex lock_;
    T map_;
};

template<typename T, typename Mutex, typename WriteLocker, typename ReadLocker>
threadSafeMap<T, Mutex, WriteLocker, ReadLocker>::threadSafeMap() {}

template<typename T, typename Mutex, typename WriteLocker, typename ReadLocker>
threadSafeMap<T, Mutex, WriteLocker, ReadLocker>& threadSafeMap<T, Mutex, WriteLocker, ReadLocker>::
operator=(threadSafeMap<T, Mutex, WriteLocker, ReadLocker> &&value) noexcept
{
    typedef threadSafeMap<T, Mutex, WriteLocker, ReadLocker> ClassType;

    WriteLocker guard(&lock_);
    if(this != &value)    
        move_rvalue(std::forward<ClassType> >(value) );

    return *this;
}

#endif // THREADSAFEMAP_HPP
