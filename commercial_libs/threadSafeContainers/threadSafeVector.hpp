#ifndef THREADSAFEVECTOR_HPP
#define THREADSAFEVECTOR_HPP

#include <iterator>
#include <utility>

#include <QMutex>
#include <QMutexLocker>

/*
 * generic thread safe vector, only implement some functions
 * every container which satisfy the concepts of std::vector
 * should match this threadSafeVector
 */
template<typename T, typename Mutex = QMutex, typename WriteLocker = QMutexLocker, typename ReadLocker = WriteLocker>
class threadSafeVector
{

typedef threadSafeVector<T, Mutex,  WriteLocker, ReadLocker> ThisType;

public:
    typedef typename T::const_iterator  const_iterator;
    typedef typename T::iterator        iterator;
    typedef typename T::size_type       size_type;
    typedef typename T::value_type      value_type;
    typedef typename T::reference       reference;
    typedef typename T::const_reference const_reference;

    threadSafeVector();

    threadSafeVector(threadSafeVector && value) noexcept
    {
        WriteLocker guard(&lock_);
        *this = move_rvalue(std::forward<threadSafeVector>(value));
    }

    reference operator[] ( size_type n )
    {
        return const_cast<reference>(static_cast<const ThisType&>(*this)[n]);
    }

    const_reference operator[] ( size_type n ) const
    {
        ReadLocker guard(&lock_);
        return vector_[n];
    }

    threadSafeVector& operator=(threadSafeVector &&value) noexcept;

    explicit threadSafeVector(threadSafeVector const&) = delete;
    threadSafeVector& operator=(threadSafeVector const&) = delete;

    iterator begin()
    {
        WriteLocker guard(&lock_);
        return std::begin(vector_);
    }

    void clear ( )
    {
        WriteLocker guard(&lock_);
        vector_.clear();
    }

    bool empty() const
    {
        ReadLocker guard(&lock_);
        return vector_.empty();
    }

    iterator end()
    {
        WriteLocker guard(&lock_);
        return std::end(vector_);
    }

    void pop_back ( )
    {
        WriteLocker guard(&lock_);
        vector_.pop_back();
    }

    void push_back(value_type const &x)
    {
        WriteLocker guard(&lock_);
        vector_.push_back(x);
    }

    void push_back(value_type &&x)
    {
        WriteLocker guard(&lock_);
        vector_.push_back(std::move(x) );
    }

    size_type size() const
    {
        ReadLocker guard(&lock_);
        return vector_.size();
    }

private:
    inline void move_rvalue(threadSafeVector &&value) noexcept;

private:
    mutable Mutex lock_;
    T vector_;
};

template<typename T, typename Mutex, typename WriteLocker, typename ReadLocker>
threadSafeVector<T, Mutex, WriteLocker, ReadLocker>::threadSafeVector() {}

template<typename T, typename Mutex, typename WriteLocker, typename ReadLocker>
threadSafeVector<T, Mutex, WriteLocker, ReadLocker>& threadSafeVector<T, Mutex, WriteLocker, ReadLocker>::
operator=(threadSafeVector<T, Mutex, WriteLocker, ReadLocker> &&value) noexcept
{
    typedef threadSafeVector<T, Mutex, WriteLocker, ReadLocker> ClassType;

    WriteLocker guard(&lock_);
    if(this != &value)
        move_rvalue(std::forward<ClassType> >(value) );

    return *this;
}

template<typename T, typename Mutex, typename WriteLocker, typename ReadLocker>
void threadSafeVector<T, Mutex, WriteLocker, ReadLocker>::move_rvalue
(threadSafeVector<T, Mutex, WriteLocker, ReadLocker> &&value) noexcept
{    
    vector_ = std::move(value);
}

#endif // THREADSAFEVECTOR_HPP
