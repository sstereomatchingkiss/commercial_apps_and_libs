#-------------------------------------------------
#
# Project created by QtCreator 2012-08-25T16:52:32
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = threadSafeContainers
TEMPLATE = app

win32{
QMAKE_CXXFLAGS += -std=c++0x
}
mac{
LIBS += -stdlib=libc++

QMAKE_CXXFLAGS += -stdlib=libc++
QMAKE_CXXFLAGS += -std=c++11
QMAKE_CXXFLAGS += -mmacosx-version-min=10.7
QMAKE_LFLAGS += -mmacosx-version-min=10.7
}

QMAKE_CXXFLAGS += -Woverloaded-virtual

SOURCES += main.cpp

HEADERS  += \
    threadSafeMap.hpp \
    threadSafeVector.hpp \
    threadSafeArray.hpp
