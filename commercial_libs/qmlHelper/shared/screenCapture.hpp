#ifndef SCREENCAPTURE_HPP
#define SCREENCAPTURE_HPP

#include <QObject>

class QQuickView;

class screenCapture : public QObject
{
    Q_OBJECT
public:    
    explicit screenCapture(QQuickView *parent = 0);
    screenCapture& operator=(screenCapture const&) = delete;
    screenCapture(screenCapture const&) = delete;
    
public slots:
    int capture(QString const &path, int hint = 0, QString const &image_type = "jpg");

    QString get_previous_file_name() const;
    QString get_previous_name() const;

private:
    //QtQuick2ApplicationViewer *currentView_;
    QQuickView *currentView_;

    QString previous_name_;
    
};

#endif // SCREENCAPTURE_HPP
