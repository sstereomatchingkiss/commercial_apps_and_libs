#include "systemInformation.hpp"

systemInformation::systemInformation(QObject *parent) : QObject(parent)
{
}

int systemInformation::get_height() const
{
    return info_.geometry().height();
}

int systemInformation::get_width() const
{
    return info_.geometry().width();
}
