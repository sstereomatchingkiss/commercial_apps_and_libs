import QtQuick 2.0

ShaderEffect {
    id : root

    property variant source
    property string fragmentShaderText
    property string vertexShaderText

    QtObject {
        id: d
        property string fragmentShaderCommon: "
            #ifdef GL_ES
                precision mediump float;
            #else
            #   define lowp
            #   define mediump
            #   define highp
            #endif // GL_ES
        "
    }

    //property string fragResult: fragmentShader

    // The following is a workaround for the fact that ShaderEffect
    // doesn't provide a way for shader programs to be read from a file,
    // rather than being inline in the QML file

    onFragmentShaderTextChanged:
        fragmentShader = d.fragmentShaderCommon + root.fragmentShaderText

    onVertexShaderTextChanged:
        vertexShader = vertexShaderContent
}
