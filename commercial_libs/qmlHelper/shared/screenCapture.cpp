#include <QDir>
#include <QFileInfo>
#include <QImage>
#include <QQuickView>
#include <QString>

#include "fileAuxiliary.hpp"
#include "screenCapture.hpp"

screenCapture::screenCapture(QQuickView *parent) :
    QObject(0), currentView_(parent)
{
}

/**
 * @brief screenCapture::capture
 * @param path
 * @param hint: initial number of the file name, could reduce the times of iterative
 *  if assign it properly
 * @param image_type: type of the image, ex : jpg, png
 *
 * @return the hint of the
 */
int screenCapture::capture(QString const &path, int hint, QString const &image_type)
{
    QImage img = currentView_->grabWindow();
    if(!img.isNull()){
        QString const name = path.isEmpty() ? generate_noduplicate_number_name(QDir::homePath() + "/Pictures", "img." + image_type, 4, hint) :
                                              generate_noduplicate_number_name(path, "img." + image_type, 4, hint);
        img.save(name);
        previous_name_ = name;
    }

    return hint;
}

QString screenCapture::get_previous_file_name() const
{
    return QFileInfo(previous_name_).fileName();
}

QString screenCapture::get_previous_name() const
{
    return previous_name_;
}
