#include <QDebug>

#include <QDir>
#include <QFileInfo>
#include <QTextStream>

#include <stlAlgoWrapper.hpp>

#include "fileProcess.hpp"
#include "fileProcessConstant.hpp"

fileProcess::fileProcess(QObject *parent) :
    QObject(parent), image_filter_{QStringLiteral("*.bmp"), QStringLiteral("*.jpg"), QStringLiteral("*.JPEG"),
                                   QStringLiteral("*.png"), QStringLiteral("*.ppm"), QStringLiteral("*.tiff"),
                                   QStringLiteral("*.tif"), QStringLiteral("*.xbm"), QStringLiteral("*.xpm")}
{
}

fileProcess::~fileProcess()
{
    if(!dir_to_remove_.isEmpty()){
        QDir(dir_to_remove_).removeRecursively();
    }
}

int fileProcess::checkImageSizeInFolder(QString const &folder, QList<QString> const &filter) const
{
    return getImagesList(folder, filter).size();
}

/**
 * @brief create folder
 * @param : name of the folder
 * @return Returns true on success; otherwise returns false.
 * If the directory already exists when this function is called, it will return false.
 */
bool fileProcess::createFolder(QString const& name) const
{    
    return QDir().mkdir(QFileInfo(name).absolutePath());
}

QString fileProcess::getCurrentPath() const
{
    return QDir::currentPath();
}

QString fileProcess::getDirToRemove() const
{
    return dir_to_remove_;
}

/**
 * @brief   get home path
 * @return  return QDir::homePath()
 */
QString fileProcess::getHomePath() const
{
    return QDir::homePath();
}

/**
 * @brief set the folder need to remove when destructor called
 * @param dir : the directory want to remove
 */
void fileProcess::removeDirWhenDestruct(QString const &dir)
{
    QString const temp_str = removeQmlPrefix(dir);
    QDir temp_dir(temp_str);
    if(temp_dir.exists()){
        dir_to_remove_ = temp_str;
        dir_to_remove_.append(QStringLiteral("0")); //make sure it is an unique folder
    }else{
        dir_to_remove_ = temp_dir.mkdir(temp_str) ? temp_str : "";
    }
}

/**
 * @brief find out the different images under the folder compare with the old data
 * @param old_data : the images already exist
 * @param folder : the folder we select, add the images haven't show on the old_data if any
 * @param filter : the files want to select under the folder
 * @return the new images haven't show on the old_data
 */
QList<QString> fileProcess::uniqueFolderImages(QList<QString> old_data, QUrl const &folder) const
{
    QString dir_name = folder.toString();
    removeQmlPrefix(dir_name);
    QDir temp_dir(dir_name);
    if(temp_dir.exists() && !dir_name.isEmpty()){
        auto new_files = temp_dir.entryList(image_filter_, QDir::Files | QDir::NoSymLinks | QDir::NoDotAndDotDot);
        dir_name.prepend(fileProcessConstant::qmlPrefix);
        dir_name += QStringLiteral("/");
        for(auto &data : new_files){
            data.prepend(dir_name);
            data = QDir::toNativeSeparators(data);
        }

        return uniqueTwoStrs(new_files, old_data);
    }

    return QList<QString>();
}

/**
 * @brief find out the different string of new_data compare with the old data
 * @param old_data : the images already exist
 * @param new_data : new images may or may exist(or partial exist) within the old_data
 * @return the different string of new_data compare with the old data
 *
 * @example old_data = {1, 2, 3, 4}, new_data = {1, 3, 4, 5}, results = {5}
 *
 * @caution : qml site could not recognize the parameters with &(even it can, it still copy the parameters rather than take the reference)
 */
QList<QString> fileProcess::uniqueUrlsStrs(QList<QUrl> new_data, QList<QString> old_data) const
{       
    auto temp = urlsToStrs(new_data);
    return set_difference_lazy(temp, old_data);
}

QList<QString> fileProcess::uniqueStrsUrls(QList<QString> new_data, QList<QUrl> old_data) const
{  
    auto temp = urlsToStrs(old_data);
    return set_difference_lazy(new_data, temp);
}

QList<QString> fileProcess::uniqueTwoStrs(QList<QString> new_data, QList<QString> old_data) const
{   
    return set_difference_lazy(new_data, old_data);
}

/**********************************************************
 ****************** implementation ************************
 **********************************************************/

QStringList fileProcess::getImagesList(QString const &folder,  QList<QString> const &filter) const
{
    QString dir_name = folder;
    removeQmlPrefix(dir_name);
    QDir temp_dir(dir_name);
    if(temp_dir.exists() && !dir_name.isEmpty()){
        if(filter.isEmpty()){
            return temp_dir.entryList(image_filter_, QDir::Files | QDir::NoSymLinks | QDir::NoDotAndDotDot);
        }else{
            QStringList list(filter);
            return temp_dir.entryList(list, QDir::Files | QDir::NoSymLinks | QDir::NoDotAndDotDot);
        }
    }

    return QStringList();
}

QString fileProcess::readWholeFile(QString file_name) const
{
    QString content;
    removeQmlPrefix(file_name);
    QFile file(file_name);
    if (file.open(QIODevice::ReadOnly)) {
        content = QTextStream(&file).readAll();
    }
    return content;
}

/**
 * @brief remove prefix of the file names under qml
 * @param data : the string need to process
 */
void fileProcess::removeQmlPrefix(QString &data) const
{
    //remove words "file:///", this is needed to tell qml this file do not come from qrc, but
    //prepend this word would make the QDir fail to find the folder
    if(data.contains(fileProcessConstant::qmlPrefix)){
        data.remove(0, fileProcessConstant::removePosition);
    }
}

QString fileProcess::removeQmlPrefix(QString const &data) const
{
    QString temp = data;
    removeQmlPrefix(temp);

    return temp;
}

QList<QString> fileProcess::urlsToStrs(QList<QUrl> const &urls) const
{
    QList<QString> temp_urls;
    int const size = urls.size();
    temp_urls.reserve(size);

    for(int i = 0; i != size; ++i){
        temp_urls.push_back(urls[i].toString());
    }

    return temp_urls;
}
