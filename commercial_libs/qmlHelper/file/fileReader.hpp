#ifndef FILEREADER_HPP
#define FILEREADER_HPP

#include <QObject>

class FileReader : public QObject
{
    Q_OBJECT
public:
    explicit FileReader(QObject *parent = 0);
    FileReader& operator=(FileReader const&) = delete;
    FileReader(FileReader const&) = delete;

    Q_INVOKABLE QString read_file(QString const &fileName) const;        

};

#endif // FILEREADER_HPP
