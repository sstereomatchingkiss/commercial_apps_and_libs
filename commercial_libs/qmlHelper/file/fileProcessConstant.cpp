#include "fileProcessConstant.hpp"

#ifdef MAC_OS

QString const fileProcessConstant::qmlPrefix = QStringLiteral("file://");

#endif

#ifdef WIN_OS
QString const fileProcessConstant::qmlPrefix = QStringLiteral("file:///");
#endif
