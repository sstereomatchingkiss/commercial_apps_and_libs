#include <QDir>
#include <QFile>
#include <QFileInfo>
#include <QTextStream>

#include "fileReader.hpp"

FileReader::FileReader(QObject *parent) :
    QObject(parent)
{
}

QString FileReader::read_file(QString const &fileName) const
{
    QString content;
    QFile file(fileName);
    if (file.open(QIODevice::ReadOnly)) {
        QTextStream stream(&file);
        content = stream.readAll();
    }
    return content;
}

