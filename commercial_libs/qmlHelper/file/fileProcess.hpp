#ifndef FILEPROCESS_HPP
#define FILEPROCESS_HPP

#include <QDebug>

#include <QList>
#include <QObject>
#include <QString>
#include <QStringList>
#include <QUrl>

class fileProcess : public QObject
{
    Q_OBJECT
public:
    explicit fileProcess(QObject *parent = nullptr);
    ~fileProcess();

    Q_INVOKABLE int checkImageSizeInFolder(QString const &folder, QList<QString> const &filter = QList<QString>()) const;
    Q_INVOKABLE bool createFolder(QString const& name) const;

    Q_INVOKABLE QString getCurrentPath() const;
    Q_INVOKABLE QString getDirToRemove() const;
    Q_INVOKABLE QString getHomePath() const;

    Q_INVOKABLE QString readWholeFile(QString file_name) const;
    Q_INVOKABLE void removeDirWhenDestruct(QString const &dir);

    Q_INVOKABLE QList<QString> uniqueFolderImages(QList<QString> old_data, QUrl const &folder) const;
    //qml site do not support operator overloading, hence we have to make up different names
    Q_INVOKABLE QList<QString> uniqueUrlsStrs(QList<QUrl> new_data, QList<QString> old_data) const;
    Q_INVOKABLE QList<QString> uniqueStrsUrls(QList<QString> new_data, QList<QUrl> old_data) const;
    Q_INVOKABLE QList<QString> uniqueTwoStrs(QList<QString> new_data, QList<QString> old_data) const;

private:
    QStringList getImagesList(QString const &folder,  QList<QString> const &filter = QList<QString>()) const;

    //this function is designed for debug only
    template<typename T>
    void printStrsImpl(T const &strs) const
    {
        for(auto const &str: strs){
            qDebug()<<"print strs : "<<str;
        }
    }

    void removeQmlPrefix(QString &data) const;
    QString removeQmlPrefix(QString const &data) const;


    QList<QString> urlsToStrs(QList<QUrl> const &urls) const;

private:
    QString dir_to_remove_;
    QStringList const image_filter_;    
};

#endif // FILEPROCESS_HPP
