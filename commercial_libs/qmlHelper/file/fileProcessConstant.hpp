#ifndef FILEPROCESSCONSTANT_HPP
#define FILEPROCESSCONSTANT_HPP

#include <QString>

class fileProcessConstant
{
public:
#ifdef MAC_OS
    static QString const qmlPrefix;
    static int const removePosition = 7;
#endif

#ifdef WIN_OS
    static QString const qmlPrefix;
    static int const removePosition = 8;
#endif

};

#endif // FILEPROCESSCONSTANT_HPP
