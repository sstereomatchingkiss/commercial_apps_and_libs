#include "generalUnitConverter.hpp"

#include "generalParser.hpp"

#include <string>

generalUnitConverter::generalUnitConverter(QObject *parent) :
    QObject(parent)
{
}

QString generalUnitConverter::scale_number(QString const &str, double scale)
{
    namespace qi = boost::spirit::qi;

    return parse_to_qstring_scale(str, qi::double_, scale);
}

/**
 * @brief covert between different capacity units
 * @param number input number
 * @param unit   unit(binary, byte, kibibyte and so on), this is the input unit of the input number
 * @param scale  scale value need to transfer number to bytes
 * @param mapper mapping unit and scale
 * @return value after transfer to different units
 */
QString generalUnitConverter::batch_mapper_conversion(
        QString const &number,
        QString const &unit,
        double scale,
        std::vector<std::pair<QString, double>> const &mapper) const
{
    namespace qi = boost::spirit::qi;

    auto const value = parse_to_numerical(number, qi::double_, scale);
    if(!value.first){
        return "invalid input";
    }

    std::string const number_per_byte = QString::number(value.second).toStdString();
    QString result;
    for(size_t i = 0, size = mapper.size(); i != size; ++i){
        if(unit != mapper[i].first){
            QString temp = parse_to_qstring_scale(std::begin(number_per_byte), std::end(number_per_byte), qi::double_, mapper[i].second);
            if(temp != "invalid input"){
                result += temp + " " + mapper[i].first + "\n";
            }else{
                result += temp + "\n";
            }
        }else{
            result += number + " " + mapper[i].first + "\n";
        }
    }

    return result;
}
