#include "baseConverter.hpp"
#include "generalParser.hpp"

#include <functional>

#include <QString>

baseConverter::baseConverter(QObject *parent) :
    QObject(parent), bases_{{"bin", 2}, {"oct", 8}, {"dec", 10}, {"hex", 16}}
{   
}

QString baseConverter::bin_to_others(QString const &str) const
{
    namespace qi = boost::spirit::qi;

    if(!is_valid_input(str, qi::bin)) return "invalid input";

    return bases_converter(str, 2);
}

QString baseConverter::oct_to_others(QString const &str) const
{
    namespace qi = boost::spirit::qi;

    if(!is_valid_input(str, qi::oct)) return "invalid input";

    return bases_converter(str, 8);
}

QString baseConverter::dec_to_others(QString const &str) const
{
    namespace qi = boost::spirit::qi;

    if(!is_valid_input(str, qi::uint_)) return "invalid input";

    return bases_converter(str, 10);
}

QString baseConverter::hex_to_others(QString const &str) const
{
    namespace qi = boost::spirit::qi;

    if(!is_valid_input(str, qi::hex)) return "invalid input";

    return bases_converter(str, 16);
}

QString baseConverter::bin_to_decade(QString const &str) const
{
    namespace qi = boost::spirit::qi;

    return parse_to_qstring_base(str, qi::bin, 10);
}

QString baseConverter::decade_to_bin(QString const &str) const
{
    namespace qi = boost::spirit::qi;

    return parse_to_qstring_base(str, qi::uint_, 2);
}

QString baseConverter::decade_to_hex(QString const &str) const
{
    namespace qi = boost::spirit::qi;

    return parse_to_qstring_base(str, qi::uint_, 16);
}

QString baseConverter::decade_to_octave(QString const &str) const
{
    namespace qi = boost::spirit::qi;

    return parse_to_qstring_base(str, qi::uint_, 8);
}

QString baseConverter::hex_to_decade(QString const &str) const
{
    namespace qi = boost::spirit::qi;

    return parse_to_qstring_base(str, qi::hex, 10);
}

QString baseConverter::octave_to_decade(QString const &str) const
{
    namespace qi = boost::spirit::qi;

    return parse_to_qstring_base(str, qi::oct, 10);
}

/**********************************************************
 ****************** implementation ************************
 **********************************************************/

/**
 * @brief convert number to different bases
 * @param number : input number
 * @param base_of_number : base of the number
 * @return number with different bases
 */
QString baseConverter::bases_converter(QString const &number, size_t base_of_number) const
{
    size_t const value = number.toUInt(nullptr, base_of_number);

    QString result;
    for(size_t i = 0, size = bases_.size(); i != size; ++i){
        if(bases_[i].second != base_of_number){
            result += bases_[i].first + " = " + QString::number(value, bases_[i].second) + "\n";
        }else{
            result += bases_[i].first + " = " + number + "\n";
        }
    }

    return result;
}
