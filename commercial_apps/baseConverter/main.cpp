//#include <QtGui/QGuiApplication>
#include <QApplication>
#include "qtquick2applicationviewer.h"

#include <QQuickPaintedItem>

#include <qmlHelper/shared/systemInformation.hpp>

#include "baseConverter.hpp"
#include "computerUnitConverter.hpp"
#include "lengthConverter.hpp"
#include "temparatureUnitConverter.hpp"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    qmlRegisterType<baseConverter>("Converter", 1, 0, "BaseConverter");
    qmlRegisterType<computerUnitConverter>("Converter", 1, 0, "PCUnitConverter");
    qmlRegisterType<lengthConverter>("Converter", 1, 0, "LengthUnitConverter");
    qmlRegisterType<temparatureUnitConverter>("Converter", 1, 0, "TemparatureUnitConverter");

    qmlRegisterType<systemInformation>("SystemInformation", 1, 0, "SysInfo");

    QtQuick2ApplicationViewer viewer;
    //viewer.setMainQmlFile(QStringLiteral("qml/baseConverter/main.qml"));
    viewer.setSource(QStringLiteral("qrc:///qml/baseConverter/main.qml"));
    viewer.showExpanded();

    return app.exec();
}
