#ifndef LENGTHCONVERTER_HPP
#define LENGTHCONVERTER_HPP

#include <QObject>
#include <QString>

#include <utility>
#include <vector>

class lengthConverter : public QObject
{
    Q_OBJECT
public:
    explicit lengthConverter(QObject *parent = nullptr);

    Q_INVOKABLE QString m_to_others(QString const &number) const;
    Q_INVOKABLE QString dm_to_others(QString const &number) const;
    Q_INVOKABLE QString cm_to_others(QString const &number) const;
    Q_INVOKABLE QString mm_to_others(QString const &number) const;
    Q_INVOKABLE QString km_to_others(QString const &number) const;
    Q_INVOKABLE QString in_to_others(QString const &number) const;
    Q_INVOKABLE QString ft_to_others(QString const &number) const;
    Q_INVOKABLE QString yards_to_others(QString const &number) const;
    Q_INVOKABLE QString miles_to_others(QString const &number) const;
    Q_INVOKABLE QString nautical_miles_to_others(QString const &number) const;
    Q_INVOKABLE QString cables_to_others(QString const &number) const;

private:
    std::vector<std::pair<QString, double>> mapper_; //first: name of unit; second: scale value of unit
};

#endif // LENGTHCONVERTER_HPP
