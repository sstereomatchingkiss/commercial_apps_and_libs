import QtQuick 2.2

QtObject{
    id: root

    function generalKeyBoard(theText, inputNumber, obj){
        if(theText == "X"){
            inputNumber.popText();
        }else if(theText == "C"){
            inputNumber.clearText();
        }else{
            inputNumber.appendText(theText);
        }
    }
}
