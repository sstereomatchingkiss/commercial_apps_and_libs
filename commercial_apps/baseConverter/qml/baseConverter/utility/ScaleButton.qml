import QtQuick 2.2
import QtQuick.Controls 1.1
import QtQuick.Controls.Styles 1.1

Button {
    id: root

    property alias theText: text.text
    property real buttonRadius: 0

    signal buttonClicked()

    width: 100
    height: 62
    state: obj.stateNormal

    style: ButtonStyle{
        background:Rectangle{
            implicitWidth: root.width
            implicitHeight: root.height
            border.width: 1
            border.color: "#888"
            radius: buttonRadius

            gradient: Gradient {
                GradientStop {
                    position: 0
                    Behavior on color {ColorAnimation { duration: obj.duration }}
                    color: mouse.pressed ? "#e0e0e0" : "#fff"
                }
                GradientStop {
                    position: 1
                    Behavior on color {ColorAnimation { duration: obj.duration }}
                    color: mouse.pressed ? "#e0e0e0" : mouse.containsMouse ? "#f5f5f5" : "#eee"
                }
            }
        }
    }

    QtObject{
        id: obj

        property int duration: 500

        property string stateNormal: "normal"
        property string stateClicked: "clicked"
    }

    Text{
        id: text

        anchors.centerIn: parent
        wrapMode: Text.WrapAnywhere
    }

    /*MaxEmitter{
        id: emitter

        enableEmitter: mouse.pressed
    }*/

    MouseArea{
        id: mouse

        anchors.fill: parent

        onClicked: {
            buttonClicked()
            root.state = obj.stateClicked
            //console.log("aaa")
        }

        onReleased: {
            root.state = obj.stateNormal
            //console.log("bbb")
        }
    }

    transitions: [
        Transition {
            from: obj.stateNormal; to: obj.stateClicked
            SequentialAnimation {
                ParallelAnimation{
                    ColorAnimation{target: text; property: "color"; from: "black"; to: "red"; duration: obj.duration }
                    NumberAnimation{target: text; property: "scale"; from: 1.0; to: 1.5; duration: obj.duration}
                }

                ParallelAnimation{
                    ColorAnimation { target: text; property: "color"; from: "red"; to: "black"; duration: obj.duration }
                    NumberAnimation {target: text; property: "scale"; from: 1.5; to: 1.0; duration: obj.duration}
                }
            }
        }
    ]
}
