import QtQuick 2.2
import QtQuick.Controls 1.1

Column{
    id: root

    function appendText(theText){
        if(input.text == "0"){
            input.text = ""
        }

        input.text = input.text + theText
    }

    function appendUnitModel(units){
        for(var i = 0; i != units.length; ++i){
            unitModel.append({"unit": units[i]});
        }
    }

    function clearText(){
        input.text = "0"
    }

    function getCurrentUnit(){
        return unitSelector.currentText
    }

    function popText(){
        input.text = input.text.substring(0, input.text.length - 1)
    }

    signal currentUnit(string unit);

    property alias inputText: input.text

    signal showKeyBoard()

    SystemPalette{id: palette}   

    ListModel{
        id: unitModel
    }       

    Component{
        id: unitComp
        ComboBox{
            id: unitSelector

            height: input.height
            width: root.width - input.width
        }
    }

    Row{
        spacing: 5

        Text{
            id: input

            color: palette.highlight
            height: root.height / 2
            width: root.width * 0.6
            focus: true
            font.family: "Helvetica"
            font.pixelSize: 16; font.bold: true

            //Behavior on height{ NumberAnimation{duration: 500} }

            MouseArea{
                anchors.fill: parent

                onClicked: {
                    showKeyBoard()
                }
            }
        }       

        ComboBox{
            id: unitSelector

            model: unitModel
            textRole: "unit"
            height: input.height
            width: root.width - input.width

            Component.onCompleted: {
                model = unitModel
            }

            onCurrentTextChanged: {
                currentUnit(unitSelector.currentText)
            }
        }
    }   
}
