import QtQuick 2.2
import QtQuick.Controls 1.1

Grid {
    id: root

    columns: 3
    //columnSpacing: 32
    //rowSpacing: 16

    signal inputText(string theText)

    onOpacityChanged: {
        if(root.opacity == 0){
            root.enabled = false
        }else{
            root.enabled = true
        }
    }

    QtObject{
        id: obj

        property int buttonWidth: root.width / root.columns
        property int buttonHeight: root.height / 5
    }

    Repeater{
        model: 10
        KeyBoardButton {
            text: index; width: obj.buttonWidth; height: obj.buttonHeight
            onPress: {
                inputText(text);
            }
        }
    }

    KeyBoardButton {
        text: "."; width: obj.buttonWidth; height: obj.buttonHeight
        onPress: {
            inputText(text);
        }
    }
    KeyBoardButton {
        text: "X"; width: obj.buttonWidth; height: obj.buttonHeight
        onPress: {
            inputText(text);
        }
    }
    KeyBoardButton {
        text: "C"; width: obj.buttonWidth; height: obj.buttonHeight
        onPress: {
            inputText(text);
        }
    }
    KeyBoardButton {
        text: "Enter"; width: obj.buttonWidth; height: obj.buttonHeight
        onPress: {
            root.opacity = 0
            inputText(text);
        }
    }

    Behavior on opacity {NumberAnimation { duration: 500 } }
}
