import QtQuick 2.2

Rectangle{
    id: root

    width: 480
    height: 320

    property alias text: text.text

    BorderImage {
        id: textBackGround
        source: "../../../images/vector-old-paper.jpg"
        anchors.fill: parent
        border.left: 5; border.top: 5
        border.right: 5; border.bottom: 5

        Text{
            id: text
            anchors.centerIn: parent
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }
    }

    Behavior on opacity { NumberAnimation{duration: 500} }
}
