import QtQuick 2.2
import QtQuick.Controls 1.1
import QtQuick.Controls.Styles 1.1

Button {
    id: root

    signal press()

    QtObject{
        id: obj

        property int duration: 500
        property real biggerSize: 1.3
        property real smallerSize: 1
    }

    width: 100
    height: 62
    state: "One"

    style: ButtonStyle{
        background:Rectangle{

            implicitWidth: root.width
            implicitHeight: root.height
            border.width: 5
            border.color: "#234"

            gradient: Gradient {
                GradientStop {
                    position: 0
                    Behavior on color {ColorAnimation { duration: obj.duration }}
                    color: root.pressed ? "steelRed" : "steelBlue"
                }
                GradientStop {
                    position: 1
                    Behavior on color {ColorAnimation { duration: obj.duration }}
                    color: root.pressed ? "lightSteelred" : "lightSteelblue"
                }
            }
        }
    }

    onClicked: {
        root.state = root.state == "One" ? "Two" : "One"
        press()
    }

    /*transitions: [
        Transition {
            from: "One"; to: "Two"

            SequentialAnimation{
                //ScaleAnimator{target:root; from: obj.smallerSize; to: obj.biggerSize; duration:obj.duration }
                //ScaleAnimator{target:root; from: obj.biggerSize; to: obj.smallerSize; duration:obj.duration }
                ColorAnimation { target: root.text; from: "black"; to: "red"; duration: obj.duration }
                ColorAnimation { target: root.text; from: "red"; to: "black"; duration: obj.duration }
            }
        },
        Transition {
            from: "Two"; to: "One"

            SequentialAnimation{
                //ScaleAnimator{target:root; from: obj.smallerSize; to: obj.biggerSize; duration:obj.duration }
                //ScaleAnimator{target:root; from: obj.biggerSize; to: obj.smallerSize; duration:obj.duration }
                ColorAnimation { target: root.text; from: "black"; to: "red"; duration: obj.duration }
                ColorAnimation { target: root.text; from: "red"; to: "black"; duration: obj.duration }
            }
        }
    ]*/
}
