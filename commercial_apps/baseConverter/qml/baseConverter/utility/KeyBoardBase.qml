import QtQuick 2.2
import QtQuick.Controls 1.1

Grid {
    id: root

    columns: 3
    //columnSpacing: 32
    //rowSpacing: 16

    signal inputText(string theText)

    onOpacityChanged: {
        if(root.opacity == 0){
            root.enabled = false
        }else{
            root.enabled = true
        }
    }

    QtObject{
        id: obj

        property int buttonWidth: root.width / root.columns
        property int buttonHeight: root.height / 6
    }

    Repeater{
        model: 10
        KeyBoardButton {
            text: index; width: obj.buttonWidth; height: obj.buttonHeight
            onPress: {
                inputText(text);
            }
        }
    }

    KeyBoardButton {
        text: "a"; width: obj.buttonWidth; height: obj.buttonHeight
        onPress: {
            inputText(text);
        }
    }

    KeyBoardButton {
        text: "b"; width: obj.buttonWidth; height: obj.buttonHeight
        onPress: {
            inputText(text);
        }
    }

    KeyBoardButton {
        text: "c"; width: obj.buttonWidth; height: obj.buttonHeight
        onPress: {
            inputText(text);
        }
    }

    KeyBoardButton {
        text: "d"; width: obj.buttonWidth; height: obj.buttonHeight
        onPress: {
            inputText(text);
        }
    }

    KeyBoardButton {
        text: "e"; width: obj.buttonWidth; height: obj.buttonHeight
        onPress: {
            inputText(text);
        }
    }

    KeyBoardButton {
        text: "f"; width: obj.buttonWidth; height: obj.buttonHeight
        onPress: {
            inputText(text);
        }
    }
    KeyBoardButton {
        text: "<-"; width: obj.buttonWidth; height: obj.buttonHeight
        onPress: {
            inputText(text);
        }
    }

    Behavior on opacity {NumberAnimation { duration: 500 } }
}
