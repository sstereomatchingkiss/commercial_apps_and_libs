import QtQuick 2.2
import QtQuick.Particles 2.0

ParticleSystem {
    id: sys

    property alias enableEmitter: bursty.enabled

    anchors.fill: parent
    onEmptyChanged: if (empty) sys.pause();

    ImageParticle {
        system: sys
        id: cp
        source: "qrc:///particleresources/glowdot.png"
        colorVariation: 0.6
        color: "#000000FF"
    }

    Emitter {
        //burst on click
        id: bursty
        system: sys
        enabled: enableEmitter
        x: parent.width / 2
        y: parent.height / 2
        emitRate: 16000
        maximumEmitted: 4000
        acceleration: AngleDirection {angleVariation: 360; magnitude: 360; }
        size: 8
        endSize: 16
        sizeVariation: 4

        onEnabledChanged: {
            if(enabled){
                sys.resume()
            }
        }
    }
}
