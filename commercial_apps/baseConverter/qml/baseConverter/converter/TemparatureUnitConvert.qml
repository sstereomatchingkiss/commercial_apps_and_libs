//celsius, fahrenheit kelvin rankine reaumur

import QtQuick 2.2

import Converter 1.0

GeneralConverter{
    id: root

    width: 100
    height: 62
    color: palette.shadow

    Component.onCompleted: {
        units = ["celsius", "fahrenheit", "kelvin",
                 "rankine", "reaumur"]
        initialUnit = "celsius"
        var style = ["Text", "BarChart", "LineChart"]
        appendUnitStyle(style)
    }

    TemparatureUnitConverter{id: converter}

    onDisplayAns: {
        if(unit == "celsius"){
            answerText = converter.celsius_to_others(number)
        }else if(unit == "fahrenheit"){
            answerText = converter.fahrenheit_to_others(number)
        }else if(unit == "kelvin"){
            answerText = converter.kelvin_to_others(number)
        }else if(unit == "rankine"){
            answerText = converter.rankine_to_others(number)
        }else if(unit == "reaumur"){
            answerText = converter.reaumur_to_others(number)
        }
    }
}
