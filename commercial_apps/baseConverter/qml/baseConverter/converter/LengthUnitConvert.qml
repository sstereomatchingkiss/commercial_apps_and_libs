import QtQuick 2.2

import Converter 1.0

GeneralConverter{
    id: root

    width: 100
    height: 62
    color: palette.shadow

    Component.onCompleted: {
        units = ["meters", "decimeters", "centimeters",
                 "millimeters", "kilometers", "inches",
                 "feet", "yards", "miles", "nautical miles",
                 "cables"]
        initialUnit = "meters"

        var style = ["Text", "BarChart", "LineChart"]
        appendUnitStyle(style)
    }

    LengthUnitConverter{id: converter}

    onDisplayAns: {
        if(unit == "meters"){
            answerText = converter.m_to_others(number)
        }else if(unit == "decimeters"){
            answerText = converter.dm_to_others(number)
        }else if(unit == "centimeters"){
            answerText = converter.cables_to_others(number)
        }else if(unit == "millimeters"){
            answerText = converter.mm_to_others(number)
        }else if(unit == "kilometers"){
            answerText = converter.km_to_others(number)
        }else if(unit == "inches"){
            answerText = converter.in_to_others(number)
        }else if(unit == "feet"){
            answerText = converter.ft_to_others(number)
        }else if(unit == "yards"){
            answerText = converter.yards_to_others(number)
        }else if(unit == "miles"){
            answerText = converter.miles_to_others(number)
        }else if(unit == "nautical miles"){
            answerText = converter.nautical_miles_to_others(number)
        }else if(unit == "cables"){
            answerText = converter.cables_to_others(number)
        }
    }
}
