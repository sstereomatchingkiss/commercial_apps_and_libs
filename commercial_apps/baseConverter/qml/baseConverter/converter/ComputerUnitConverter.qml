import QtQuick 2.2

import Converter 1.0

GeneralConverter{
    id: root

    width: 100
    height: 62
    color: palette.shadow

    Component.onCompleted: {
        units = ["bit", "byte", "kibibyte",
                 "mebibyte", "gibibyte", "tebibyte"]
        initialUnit = "bit"
        var style = ["Text", "BarChart", "LineChart"]
        appendUnitStyle(style)
    }

    PCUnitConverter{id: converter}

    onDisplayAns: {
        if(unit == "bit"){
            answerText = converter.bit_to_others(number)
        }else if(unit == "byte"){
            answerText = converter.byte_to_others(number)
        }else if(unit == "kibibyte"){
            answerText = converter.kibibyte_to_others(number)
        }else if(unit == "mebibyte"){
            answerText = converter.mebibyte_to_others(number)
        }else if(unit == "gibibyte"){
            answerText = converter.gibibyte_to_others(number)
        }else if(unit == "tebibyte"){
            answerText = converter.tebibyte_to_others(number)
        }
    }
}
