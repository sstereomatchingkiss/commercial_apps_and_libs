//binary octave decimal heximal

import QtQuick 2.2

import Converter 1.0

GeneralConverter{
    id: root

    width: 100
    height: 62
    color: palette.shadow

    Component.onCompleted: {
        units = ["binary", "octave", "decimal",
                 "heximal"]
        initialUnit = "binary"
        var style = ["Text"]
        appendUnitStyle(style)
    }

    BaseConverter{id: converter}

    onDisplayAns: {
        if(unit == "binary"){
            answerText = converter.bin_to_others(number)
        }else if(unit == "octave"){
            answerText = converter.oct_to_others(number)
        }else if(unit == "decimal"){
            answerText = converter.dec_to_others(number)
        }else if(unit == "heximal"){
            answerText = converter.hex_to_others(number)
        }
    }
}
