import QtQuick 2.2
import QtQuick.Controls 1.1
import "../utility" as Utility

Rectangle {
    id:root

    signal displayAns(string unit, string number)

    function appendUnitStyle(style){
        for(var i = 0; i != style.length; ++i){
            unitPresenterModel.append({"style": style[i]})
        }
    }

    property var units
    property var unitsStyle
    property string initialUnit
    property alias answerText: answer.text

    width: 100
    height: 62
    color: palette.shadow

    SystemPalette{id: palette}

    ListModel{
        id: unitPresenterModel
    }

    Utility.KeyBoardFunction{id: keyBoardFunction}

    Utility.TextInputAndClear{
        id: inputAndClear

        height: root.height * 0.2
        width: root.width

        onShowKeyBoard: {
            keyBoard.opacity = keyBoard.opacity == 0 ? 1 : 0
        }

        Component.onCompleted: {
            inputAndClear.appendUnitModel(root.units)
            inputText = "0"
            displayAns(root.initialUnit, inputAndClear.inputText)
        }

        onCurrentUnit: {
            displayAns(unit, inputAndClear.inputText)
        }

        ComboBox{
            id: unitPresenter

            model: unitPresenterModel
            textRole: "style"
            height: inputAndClear.height / 2
            width: inputAndClear.width
        }
    }

    Utility.KeyBoardNumeric{
        id: keyBoard

        anchors.top: inputAndClear.bottom
        width: root.width
        height: root.height * 0.8

        opacity: 0
        z:1

        onInputText: {
            if(theText != "Enter"){
                keyBoardFunction.generalKeyBoard(theText, inputAndClear, root)
            }else{
                displayAns(inputAndClear.getCurrentUnit(), inputAndClear.inputText)
            }
        }
    }

    Utility.AnswerCanvas{
        id: answer

        anchors.top: inputAndClear.bottom
        z: 1
        width: keyBoard.width
        height: keyBoard.height

        opacity: keyBoard.opacity == 0 ? 1 : 0
    }
}
