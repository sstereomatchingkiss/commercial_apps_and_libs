import QtQuick 2.2
import QtQuick.Controls 1.1
import QtQuick.Controls.Styles 1.1

import SystemInformation 1.0

import "converter"

Rectangle {
    id: root

    width: info.get_width() != 0 ? info.get_width() : 320
    height: info.get_height() != 0 ? info.get_height() : 240

    SystemPalette{id: palette}

    SysInfo{id: info}

    color: palette.shadow

    state: "List"

    QtObject{
        id: obj

        property int duration: 500
    }

    ListModel {
        id: pageModel
        ListElement {
            title: "Base converter"
            page: "converter/BaseConvert.qml"
        }
        ListElement {
            title: "Capacity converter"
            page: "converter/ComputerUnitConverter.qml"
        }
        ListElement {
            title: "Length converter"
            page: "converter/LengthUnitConvert.qml"
        }
        ListElement {
            title: "Temparature converter"
            page: "converter/TemparatureUnitConvert.qml"
        }
    }

    Column{
        StackView {
            id: stackView
            //anchors.fill: parent
            width: root.width
            height: root.height * 0.9

            initialItem: Item {
                width: parent.width
                height: parent.height
                ListView {
                    model: pageModel
                    anchors.fill: parent
                    delegate: Button {
                        width: parent.width
                        height: parent.height / pageModel.count
                        text: title
                        onClicked: {
                            stackView.push(Qt.resolvedUrl(page))
                            root.state = "Converter"
                        }
                    }
                }
            }
        }

        Button{
            id: listButton
            width: root.width
            height: root.height * 0.1
            text: "List"
            transform: Rotation{id: listButtonR; origin.x: root.width / 2; origin.y: listButton.y + listButton.height / 2; axis { x: 0; y: 1; z: 0 }}

            style: ButtonStyle{
                background:Rectangle{
                    implicitWidth: listButton.width
                    implicitHeight: listButton.height
                    border.width: 5
                    border.color: "#234"
                    color: palette.button
                    radius: 20
                }
            }

            onClicked: {
                root.state = "List"
                stackView.pop()
            }
        }
    }

    states: [
        State {
            name: "List"
            PropertyChanges { target: listButton; opacity: 0}
        },
        State{
            name: "Converter"
            PropertyChanges { target: listButton; opacity: 1}
        }
    ]

    transitions: [
        Transition {
            from: "List"; to: "Converter"

            ParallelAnimation{
                NumberAnimation{target: listButtonR; property: "angle"; from: 0; to: 360; duration: obj.duration}
                NumberAnimation{target: listButton; property: "opacity"; duration: obj.duration}
            }
        },
        Transition {
            from: "Converter"; to: "List"

            ParallelAnimation{
                NumberAnimation{target: listButtonR; property: "angle"; from: 360; to: 0; duration: obj.duration}
                NumberAnimation{target: listButton; property: "opacity"; duration: obj.duration}
            }
        }
    ]
}
