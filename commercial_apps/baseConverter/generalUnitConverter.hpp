#ifndef GENERALUNITCONVERTER_HPP
#define GENERALUNITCONVERTER_HPP

#include <QObject>
#include <QString>

#include <utility>
#include <vector>

class generalUnitConverter : public QObject
{
    Q_OBJECT
public:
    explicit generalUnitConverter(QObject *parent = nullptr);

    QString batch_mapper_conversion(
            QString const &number,
            QString const &unit,
            double scale,
            std::vector<std::pair<QString, double>> const &mapper) const;

    Q_INVOKABLE QString scale_number(QString const &str, double scale);
};

#endif // GENERALUNITCONVERTER_HPP
