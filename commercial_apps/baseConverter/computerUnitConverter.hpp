#ifndef COMPUTERUNITCONVERTER_HPP
#define COMPUTERUNITCONVERTER_HPP

#include <QObject>
#include <QString>

#include <utility>
#include <vector>

class computerUnitConverter : public QObject
{
    Q_OBJECT
public:
    explicit computerUnitConverter(QObject *parent = nullptr);

    Q_INVOKABLE QString bit_to_others(QString const &str) const;
    Q_INVOKABLE QString byte_to_others(QString const &str) const;
    Q_INVOKABLE QString kibibyte_to_others(QString const &str) const;
    Q_INVOKABLE QString mebibyte_to_others(QString const &str) const;
    Q_INVOKABLE QString gibibyte_to_others(QString const &str) const;
    Q_INVOKABLE QString tebibyte_to_others(QString const &str) const;

private:
    //std::map<QString, std::vector<double>> map_;
    std::vector<std::pair<QString, double>> units_; //first: name of unit; second: scale value of unit

};

#endif // COMPUTERUNITCONVERTER_HPP
