#ifndef STRINGVALIDATOR_HPP
#define STRINGVALIDATOR_HPP

#include <QObject>
#include <QString>

#include <map>
#include <vector>

class QString;

class baseConverter : public QObject
{
    Q_OBJECT
public:
    explicit baseConverter(QObject *parent = nullptr);

    Q_INVOKABLE QString bin_to_others(QString const &str) const;
    Q_INVOKABLE QString oct_to_others(QString const &str) const;
    Q_INVOKABLE QString dec_to_others(QString const &str) const;
    Q_INVOKABLE QString hex_to_others(QString const &str) const;

    Q_INVOKABLE QString bin_to_decade(QString const &str) const;
    Q_INVOKABLE QString decade_to_bin(QString const &str) const;
    Q_INVOKABLE QString decade_to_hex(QString const &str) const;
    Q_INVOKABLE QString decade_to_octave(QString const &str) const;
    Q_INVOKABLE QString hex_to_decade(QString const &str) const;
    Q_INVOKABLE QString octave_to_decade(QString const &str) const;

private:
    QString bases_converter(QString const &number, size_t base_of_number) const;

private:
    std::vector<std::pair<QString, size_t>> bases_; //first : bases name; second : base number
};

#endif // STRINGVALIDATOR_HPP
