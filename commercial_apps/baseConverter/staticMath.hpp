#ifndef STATICMATH_HPP
#define STATICMATH_HPP

inline
constexpr double static_pow(double value, size_t exp)
{
    return exp != 0 ? static_pow(value, exp - 1) * value : 1;
}

#endif // STATICMATH_HPP
