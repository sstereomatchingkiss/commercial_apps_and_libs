# Add more folders to ship with the application, here
folder_01.source = qml/baseConverter
folder_01.target = qml
DEPLOYMENTFOLDERS = folder_01

# Additional import path used to resolve QML modules in Creator's code model
QML_IMPORT_PATH =

QT += core widgets

CONFIG += c++11

win32{
INCLUDEPATH += ../../../3rdLibs/boost/boost_1_52_0/
}

INCLUDEPATH += ../../../3rdLibs/boost/boost_1_55_0/
INCLUDEPATH += ../../commercial_libs/

# The .cpp file which was generated for your project. Feel free to hack it.
SOURCES += main.cpp \
    baseConverter.cpp \
    computerUnitConverter.cpp \
    generalUnitConverter.cpp \
    lengthConverter.cpp \
    temparatureUnitConverter.cpp \
    ../../commercial_libs/qmlHelper/shared/systemInformation.cpp

# Installation path
# target.path =

# Please do not modify the following two lines. Required for deployment.
include(qtquick2applicationviewer/qtquick2applicationviewer.pri)
qtcAddDeployment()

HEADERS += \
    baseConverter.hpp \
    computerUnitConverter.hpp \
    generalParser.hpp \
    staticMath.hpp \
    generalUnitConverter.hpp \
    lengthConverter.hpp \
    temparatureUnitConverter.hpp \
    ../../commercial_libs/qmlHelper/shared/systemInformation.hpp

RESOURCES += \
    qml.qrc \
    js.qrc \
    images.qrc

ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android

OTHER_FILES += \
    android/AndroidManifest.xml \
    js/KeyBoardLogic.js \
    qml/baseConverter/utility/AnswerCanvas.qml
