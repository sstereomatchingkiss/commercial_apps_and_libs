#include "computerUnitConverter.hpp"

#include "generalParser.hpp"
#include "generalUnitConverter.hpp"
#include "staticMath.hpp"

namespace{

constexpr double bit_to_byte = 1.0 / 8.0;

}

/**
 * the logic of the conversion is setting the unit "byte" as the reference unit, other units
 * will convert to each other by reference to the unit "byte"
 */

computerUnitConverter::computerUnitConverter(QObject *parent) :
    QObject(parent),units_{
{"bit", 8.0}, {"byte", 1}, {"kibibyte", 1.0/static_pow(1024, 1)},
{"mebibyte", 1.0/static_pow(1024, 2)}, {"gibibyte", 1.0/static_pow(1024, 3)}, {"tebibyte", 1.0/static_pow(1024, 4)}}
{        
}

QString computerUnitConverter::bit_to_others(QString const &str) const
{   
    return generalUnitConverter().batch_mapper_conversion(str, units_[0].first , 1.0/8.0, units_);
}

QString computerUnitConverter::byte_to_others(QString const &str) const
{    
    return generalUnitConverter().batch_mapper_conversion(str, units_[0].first , 1.0, units_);
}

QString computerUnitConverter::kibibyte_to_others(QString const &str) const
{
    return generalUnitConverter().batch_mapper_conversion(str, units_[2].first , static_pow(1024, 1), units_);
}

QString computerUnitConverter::mebibyte_to_others(QString const &str) const
{
    return generalUnitConverter().batch_mapper_conversion(str, units_[3].first , static_pow(1024, 2), units_);
}

QString computerUnitConverter::gibibyte_to_others(QString const &str) const
{
    return generalUnitConverter().batch_mapper_conversion(str, units_[4].first , static_pow(1024, 3), units_);
}

QString computerUnitConverter::tebibyte_to_others(QString const &str) const
{
    return generalUnitConverter().batch_mapper_conversion(str, units_[5].first , static_pow(1024, 4), units_);
}

/**********************************************************
 ****************** implementation ************************
 **********************************************************/
