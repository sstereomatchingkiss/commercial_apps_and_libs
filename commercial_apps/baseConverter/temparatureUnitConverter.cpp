#include "temparatureUnitConverter.hpp"

#include "generalParser.hpp"

#include <QString>

temparatureUnitConverter::temparatureUnitConverter(QObject *parent) :
    QObject(parent)
{
}

QString temparatureUnitConverter::celsius_to_others(QString const &number) const
{
    namespace qi = boost::spirit::qi;

    auto const result = parse_to_numerical(number, qi::double_, 1.0);
    if(result.first){
        return batch_conversion(result.second);
    }

    return "invalid input";
}

QString temparatureUnitConverter::fahrenheit_to_others(QString const &number) const
{
    namespace qi = boost::spirit::qi;

    auto const result = parse_to_numerical(number, qi::double_, 1.0);
    if(result.first){
        return batch_conversion((result.second - 32.0) / 1.8);
    }

    return "invalid input";
}

QString temparatureUnitConverter::kelvin_to_others(QString const &number) const
{
    namespace qi = boost::spirit::qi;

    auto const result = parse_to_numerical(number, qi::double_, 1.0);
    if(result.first){
        return batch_conversion(result.second - 273.15);
    }

    return "invalid input";
}

QString temparatureUnitConverter::rankine_to_others(QString const &number) const
{
    namespace qi = boost::spirit::qi;

    auto const result = parse_to_numerical(number, qi::double_, 1.0);
    if(result.first){
        return batch_conversion((result.second - 32.0 - 459.67) / 1.8);
    }

    return "invalid input";
}

QString temparatureUnitConverter::reaumur_to_others(QString const &number) const
{
    namespace qi = boost::spirit::qi;

    auto const result = parse_to_numerical(number, qi::double_, 1.0);
    if(result.first){
        return batch_conversion(result.second * 1.25);
    }

    return "invalid input";
}

/**********************************************************
 ****************** implementation ************************
 **********************************************************/

QString temparatureUnitConverter::batch_conversion(double number) const
{
    QString result;

    result += QString::number(number) + " celcius\n";
    result += QString::number((number * 1.8) + 32) + " farenheit\n";
    result += QString::number(number + 273.15) + " kelvin\n";
    result += QString::number(number * 1.8 + 32 + 459.67) + " rankine\n";
    result += QString::number(number * 0.8) + " reaumur\n";

    return result;
}
