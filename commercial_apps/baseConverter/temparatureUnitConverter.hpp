#ifndef TEMPARATUREUNITCONVERTER_HPP
#define TEMPARATUREUNITCONVERTER_HPP

#include <QObject>

class QString;

class temparatureUnitConverter : public QObject
{
    Q_OBJECT
public:
    explicit temparatureUnitConverter(QObject *parent = nullptr);

    Q_INVOKABLE QString celsius_to_others(QString const &number) const;
    Q_INVOKABLE QString fahrenheit_to_others(QString const &number) const;
    Q_INVOKABLE QString kelvin_to_others(QString const &number) const;
    Q_INVOKABLE QString rankine_to_others(QString const &number) const;
    Q_INVOKABLE QString reaumur_to_others(QString const &number) const;

private:
    QString batch_conversion(double number) const;
};

#endif // TEMPARATUREUNITCONVERTER_HPP
