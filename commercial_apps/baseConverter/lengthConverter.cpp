#include "generalUnitConverter.hpp"

#include "lengthConverter.hpp"

/**
 * use meters as reference unit
 */

lengthConverter::lengthConverter(QObject *parent) :
    QObject(parent), mapper_({
{"meters", 1}, {"decimeters", 10}, {"centimeters", 100}, {"millimeters", 1000},
{"kilometers", 0.001}, {"inches", 39.3700787}, {"feet", 3.28084}, {"yards", 1.09361},
{"miles", 0.000621371}, {"nautical miles", 0.000539957}, {"cables", 0.00539957} })
{
}

QString lengthConverter::m_to_others(QString const &number) const
{
    return generalUnitConverter().batch_mapper_conversion(number, mapper_[0].first , 1.0, mapper_);
}

QString lengthConverter::dm_to_others(QString const &number) const
{
    return generalUnitConverter().batch_mapper_conversion(number, mapper_[1].first , 0.1, mapper_);
}

QString lengthConverter::cm_to_others(QString const &number) const
{
    return generalUnitConverter().batch_mapper_conversion(number, mapper_[2].first , 0.01, mapper_);
}

QString lengthConverter::mm_to_others(QString const &number) const
{
    return generalUnitConverter().batch_mapper_conversion(number, mapper_[3].first , 0.001, mapper_);
}

QString lengthConverter::km_to_others(QString const &number) const
{
    return generalUnitConverter().batch_mapper_conversion(number, mapper_[4].first , 1000, mapper_);
}

QString lengthConverter::in_to_others(QString const &number) const
{
    return generalUnitConverter().batch_mapper_conversion(number, mapper_[5].first , 0.0254, mapper_);
}

QString lengthConverter::ft_to_others(QString const &number) const
{
    return generalUnitConverter().batch_mapper_conversion(number, mapper_[6].first , 0.3, mapper_);
}

QString lengthConverter::yards_to_others(QString const &number) const
{
    return generalUnitConverter().batch_mapper_conversion(number, mapper_[7].first , 0.91, mapper_);
}

QString lengthConverter::miles_to_others(QString const &number) const
{
    return generalUnitConverter().batch_mapper_conversion(number, mapper_[8].first , 1609, mapper_);
}

QString lengthConverter::nautical_miles_to_others(QString const &number) const
{
    return generalUnitConverter().batch_mapper_conversion(number, mapper_[9].first , 1852, mapper_);
}

QString lengthConverter::cables_to_others(QString const &number) const
{
    return generalUnitConverter().batch_mapper_conversion(number, mapper_[10].first , 185, mapper_);
}
