#ifndef GENERALPARSER_HPP
#define GENERALPARSER_HPP

#include <QString>

#include <boost/spirit/include/qi.hpp>

#include <string>
#include <utility>

#include <boost/spirit/include/qi.hpp>

/**
 * @brief check  the input is valid or not
 * @param begin  initial positions in a sequence. The range used is [begin,end)
 * @param end    end positions in a sequence. The range used is [begin,end)
 * @param parser parser
 * @return true  if valid, else return false
 */
template<typename Parser, typename ForwardIterator>
bool is_valid_input(ForwardIterator begin, ForwardIterator end, Parser const &parser)
{
    namespace qi = boost::spirit::qi;

    bool const success = qi::parse(begin, end, parser);

    return success && begin == end;
}

/**
 * @brief check the input is valid or not
 * @param input  : input
 * @param parser : parser
 * @return true if valid, else return false
 */
template<typename Parser>
bool is_valid_input(QString const &input, Parser const &parser)
{
    namespace qi = boost::spirit::qi;
    std::string const &strs = input.toStdString();    

    return is_valid_input(std::begin(strs), std::end(strs), parser);
}

/**
 *@brief parse the input(expect the input is numerical(hex, oct or other bases are allowed))
 *@param T      type of the parser parse to, this type has to support default constructor
 *@param begin  initial positions in a sequence. The range used is [begin,end)
 *@param end    end positions in a sequence. The range used is [begin,end)
 *@param parser  : qi parser
 *@param UnaryFunctor : functor accept type T and return std::pair<bool, T>
 *@return first is true if parsing success, else false; second is the parse result
 */
template<typename T, typename Parser, typename ForwardIterator, typename UnaryFunctor>
auto parse_to_numerical(ForwardIterator begin, ForwardIterator end, Parser const &parser, UnaryFunctor functor)->decltype(functor(T()))
{
    namespace qi = boost::spirit::qi;

    T number = T();
    bool const success = qi::parse(begin, end, parser, number);

    if(success && begin == end){
        return functor(number);
    }

    return {false, number};
}

/**
 *@brief parse the input(expect the input is numerical(hex, oct or other bases are allowed))
 *@param begin  initial positions in a sequence. The range used is [begin,end)
 *@param end    end positions in a sequence. The range used is [begin,end)
 *@param parser  : qi parser
 *@param scale : scale the result after success. ex : result * scale
 *@return first is true if parsing success, else false; second is the parse result
 */
template<typename Parser, typename ForwardIterator>
std::pair<bool, double> parse_to_numerical(ForwardIterator begin, ForwardIterator end, Parser const &parser, double scale)
{   
    return parse_to_numerical<double>(begin, end, parser, [=](double number){ return std::make_pair(true, number * scale); });
}

/**
 *@brief parse the input to numerical
 *@param input   : input, expect to be numerical(hex, oct or other bases are allowed)
 *@param parser  : qi parser
 *@param scale : scale the result after success. ex : result * scale
 *@return first is true if parsing success, else false; second is the parse result
 */
template<typename Parser>
std::pair<bool, double> parse_to_numerical(QString const &input, Parser const &parser, double scale)
{
    namespace qi = boost::spirit::qi;

    std::string const &strs = input.toStdString();

    return parse_to_numerical(std::begin(strs), std::end(strs), parser, scale);
}

/**
 *@brief parse the input to QString
 *@param begin  initial positions in a sequence. The range used is [begin,end)
 *@param end    end positions in a sequence. The range used is [begin,end)
 *@param parser  : qi parser
 *@param base : output the input based on the base indicated by to_base
 *@return the number after scale if success, else return "invalid input"
 */
template<typename T, typename Parser, typename ForwardIterator, typename UnaryFunctor>
QString parse_to_qstring(ForwardIterator begin, ForwardIterator end, Parser const &parser, UnaryFunctor functor)
{
    auto const result = parse_to_numerical<T>(begin, end, parser, functor);

    if(result.first){
        return QString::number(result.second);
    }

    return "invalid input";
}

/**
 *@brief parse the input to QString(expect the input is floating type numerical(only support dec))
 *@param begin  initial positions in a sequence. The range used is [begin,end)
 *@param end    end positions in a sequence. The range used is [begin,end)
 *@param parser  : qi parser
 *@param scale : scale the result after success. ex : result * scale
 *@return the number after scale if success, else return "invalid input"
 */
template<typename Parser, typename ForwardIterator>
inline
QString parse_to_qstring_scale(ForwardIterator begin, ForwardIterator end, Parser const &parser, double scale)
{     
    return parse_to_qstring<double>(begin, end, parser, [=](double value){ return std::make_pair(true, value * scale); });
}

/**
 *@brief parse the input to QString
 *@param input   : input, expect to be floating type numerical(only support dec)
 *@param parser  : qi parser
 *@param scale : scale the result after success. ex : result * scale
 *@return the number after scale if success, else return "invalid input"
 */
template<typename Parser>
QString parse_to_qstring_scale(QString const &input, Parser const &parser, double scale)
{
    namespace qi = boost::spirit::qi;

    std::string const &strs = input.toStdString();

    return parse_to_qstring_scale(std::begin(strs), std::end(strs), parser, scale);
}

/**
 *@brief parse the input to QString(expect input is unsigned numerical(hex, oct or other bases are allowed))
 *@param begin  initial positions in a sequence. The range used is [begin,end)
 *@param end    end positions in a sequence. The range used is [begin,end)
 *@param parser  : qi parser
 *@param to_base : output the input based on the base indicated by to_base
 *@return the number after base converter if success, else return "invalid input"
 */
template<typename Parser, typename ForwardIterator>
inline
QString parse_to_qstring_base(ForwardIterator begin, ForwardIterator end, Parser const &parser, int to_base)
{    
    namespace qi = boost::spirit::qi;

    size_t number = 0;
    bool const success = qi::parse(begin, end, parser, number);

    if(success && begin == end){
        return QString::number(number, to_base);
    }

    return "invalid input";
}


/**
 *@brief parse the input to QString
 *@param input   : input, expect to be unsigned numerical(hex, oct or other bases are allowed)
 *@param parser  : qi parser
 *@param to_base : output the input based on the base indicated by to_base
 *@return the number after base converter if success, else return "invalid input"
 */
template<typename Parser>
QString parse_to_qstring_base(QString const &input, Parser const &parser, int to_base)
{
    std::string const &strs = input.toStdString();

    return parse_to_qstring_base(std::begin(strs), std::end(strs), parser, to_base);
}

#endif // GENERALPARSER_HPP
