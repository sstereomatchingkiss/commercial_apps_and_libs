#include <QApplication>
//#include <QDir>
//#include <QFileInfo>
//#include <QGuiApplication>
#include <QQuickView>
#include <QString>
#include <QtQml>

#include <file/fileProcess.hpp>

#include "customImage/QChromaticityHistWB.hpp"
#include "customImage/QCLAHE.hpp"
#include "customImage/QGammaCorrection.hpp"
#include "customImage/QGrayWorldAssumption.hpp"
#include "customImage/QHistogramEqualize.hpp"
#include "customImage/QHSVCorrection.hpp"
#include "customImage/QPerfectReflectAssumption.hpp"
#include "customImage/QPiecewiseAffHist.hpp"
#include "customImage/QSimplestColorBalance.hpp"
#include "customImage/QWhiteBalanceBGR.hpp"

//#include "qtquick2applicationviewer.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);    

    qmlRegisterType<QChromaticityHistWB>("ColorAlgo", 1, 0, "ChromaticityWhiteBalance");
    qmlRegisterType<QCLAHE>("ColorAlgo", 1, 0, "CLAHE");
    qmlRegisterType<QGammaCorrection>("ColorAlgo", 1, 0, "GammaCorrection");
    qmlRegisterType<QGrayWorldAssumption>("ColorAlgo", 1, 0, "GrayWorldAssumption");
    qmlRegisterType<QHistogramEqualize>("ColorAlgo", 1, 0, "HistogramEqualize");
    qmlRegisterType<QHSVCorrection>("ColorAlgo", 1, 0, "HSVCorrection");
    qmlRegisterType<QPerfectReflectAssumption>("ColorAlgo", 1, 0, "PerfectReflect");
    qmlRegisterType<QPiecewiseAffHist>("ColorAlgo", 1, 0, "PiecewiseAffHist");
    qmlRegisterType<QSimplestColorBalance>("ColorAlgo", 1, 0, "SimplestColorBalance");
    qmlRegisterType<QWhiteBalanceBGR>("ColorAlgo", 1, 0, "MonitorRGB");

    qmlRegisterType<fileProcess>("File", 1, 0, "FileProcess");        

    QQmlApplicationEngine engine(QUrl("qrc:/qml/colorCorrection/mainDesktop.qml"));
    //QQmlApplicationEngine engine(QUrl("/Users/Qt/program/experiment_apps_and_libs/colorCorrection/qml/colorCorrection/desktop/main.qml"));
    //engine.rootContext()->setContextProperty("dirToRemove", "file://" + process.get_dir_to_remove());
    QObject *topLevel = engine.rootObjects().value(0);
    QQuickWindow *window = qobject_cast<QQuickWindow *>(topLevel);
    if (!window) {
        qWarning("Error: Your root item has to be a Window.");
        return -1;
    }
    window->show();

    return app.exec();
}

