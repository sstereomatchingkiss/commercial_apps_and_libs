#!/bin/bash

# $1 : openCV version
# $2 : app name

#*****************openCV libraries****************************

#****************update openCV id***********************#

install_name_tool -id @executable_path/../Frameworks/libopencv_core.$1.dylib      $2.app/Contents/Frameworks/libopencv_core.$1.dylib
install_name_tool -id @executable_path/../Frameworks/libopencv_imgproc.$1.dylib    $2.app/Contents/Frameworks/libopencv_imgproc.$1.dylib

#****************change openCV libraries references***********************#

install_name_tool -change  lib/libopencv_core.$1.dylib      @executable_path/../Frameworks/libopencv_core.$1.dylib      "$2.app/Contents/MacOS/$2"
install_name_tool -change  lib/libopencv_imgproc.$1.dylib   @executable_path/../Frameworks/libopencv_imgproc.$1.dylib   "$2.app/Contents/MacOS/$2"

#****************change openCV internal libraries cross-references***********************#

install_name_tool -change  lib/libopencv_core.$1.dylib      @executable_path/../Frameworks/libopencv_core.$1.dylib       "$2.app/Contents/Frameworks/libopencv_imgproc.$1.dylib"

exit 0