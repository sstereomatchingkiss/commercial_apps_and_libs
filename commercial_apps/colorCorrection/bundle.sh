#!/bin/bash

#sh workAround.sh

#macdeployqt.app/Contents/MacOS/macdeployqt 

mkdir colorCorrection.app/Contents/Frameworks

cp -R qml/colorCorrection/helps/license    colorCorrection.app/Contents/MacOS

#source ~/.bash_profile2 #macdeployqt path

../macdeployqt.app/Contents/MacOS/macdeployqt /Users/Qt/program/commercial_apps_and_libs/commercial_apps/colorCorrection/colorCorrection.app -qmldir=/Users/Qt/program/commercial_apps_and_libs/commercial_apps/colorCorrection/qml/colorCorrection -verbose=2 -dmg
#macdeployqt /Users/Qt/program/commercial_apps_and_libs/commercial_apps/colorCorrection/colorCorrection.app -qmldir=/Users/Qt/program/commercial_apps_and_libs/commercial_apps/colorCorrection/qml/colorCorrection -verbose=2 -dmg

#*****************deploy openCV libraries****************************
# $1 : openCV version, $2 : app name
sh openCVMacDeploy.sh 2.4 colorCorrection

exit 0