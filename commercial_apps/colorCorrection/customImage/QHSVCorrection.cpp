//#include <QDebug>

#include <opencv2/imgproc/imgproc.hpp>

#include <QPainter>
#include <QQuickItem>

#include <QsLog.h>
#include <QsLogDest.h>

#include <basicImageAlgo.hpp>
#include <debugHelper.hpp>
#include <openCVToQt.hpp>

#include "QHSVCorrection.hpp"
//#include "setBusyProtector.hpp"

QHSVCorrection::QHSVCorrection(QQuickItem *parent): customImage(parent),
    valueH_(0),
    valueS_(0),
    valueV_(0)
{
    QLOG_INFO()<<QCOMMON_DEBUG_MESSAGE<<"hsv created";
}

void QHSVCorrection::paint(QPainter *painter)
{
    //qDebug()<<"paint hsv";
    QLOG_INFO()<<QCOMMON_DEBUG_MESSAGE<<"paint hsv";
    if(!imageScaled().isNull()){
        //qDebug()<<"painting hsv";
        QLOG_INFO()<<QCOMMON_DEBUG_MESSAGE<<"painting hsv";

        try{
            if(valueH_ != 0 || valueS_ != 0 || valueV_ != 0){
                if(!matInput_.empty()){
                    //qDebug()<<"hsv mask";
                    matInput_.copyTo(matOutput_);
                    addChannel(matOutput_);                                     
                    maskResult(matInput_, matOutput_, rubberBand());
                    cv::cvtColor(matOutput_, matOutput_, CV_HSV2BGR);
                    QImage const result = mat_to_qimage_ref(matOutput_);
                    paintResult(result, painter);
                }else{
                    paintResult(imageScaled(), painter);
                }
            }else{
                paintResult(imageScaled(), painter);
            }
        }catch(std::exception const &ex){
            setErrorLog(COMMON_DEBUG_MESSAGE + ex.what());
        }
    }
}

void QHSVCorrection::setValueH(int value)
{
    //qDebug()<<"value h";
    QLOG_INFO()<<QCOMMON_DEBUG_MESSAGE<<"set value h";
    //setBusyProtector protect(this);
    if(value != valueH_){
        valueH_ = value;
        update();
        emit valueHChanged();
    }
}

void QHSVCorrection::setValueS(int value)
{
    //qDebug()<<"value s";
    QLOG_INFO()<<QCOMMON_DEBUG_MESSAGE<<"set value s";
    //setBusyProtector protect(this);
    if(value != valueS_){
        valueS_ = value;
        update();
        emit valueSChanged();
    }
}

void QHSVCorrection::setValueV(int value)
{
    //qDebug()<<"value v";
    QLOG_INFO()<<QCOMMON_DEBUG_MESSAGE<<"set value v";
    //setBusyProtector protect(this);
    if(value != valueV_){
        valueV_ = value;
        update();
        emit valueVChanged();
    }
}

int QHSVCorrection::valueH() const
{
    return valueH_;
}

int QHSVCorrection::valueS() const
{
    return valueS_;
}

int QHSVCorrection::valueV() const
{
    return valueV_;
}


/**********************************************************
 ****************** implementation ************************
 **********************************************************/

void QHSVCorrection::addChannel(cv::Mat &mat)
{
    if(!mat.empty()){
        //qDebug()<<"add channel";
        QLOG_INFO()<<QCOMMON_DEBUG_MESSAGE<<"add channel";
        //qDebug()<<"add channel h";
        OCV::add_channel(mat, valueH_, 0);
        //qDebug()<<"add channel s";
        OCV::add_channel(mat, valueS_, 1);
        //qDebug()<<"add channel v";
        OCV::add_channel(mat, valueV_, 2);
    }
}

void QHSVCorrection::convertImage(cv::Mat &input, cv::Mat &output)
{
    //qDebug()<<"image scaled is null : "<<img.isNull();
    //QLOG_INFO()<<QCOMMON_DEBUG_MESSAGE<<"image scaled is null : "<<img.isNull();

    if(!input.empty()){
        switch(input.channels()){
        case 4 :{
            OCV::copy_if_not_same(input, output);
            //qDebug()<<"ch 4 start";
            QLOG_INFO()<<QCOMMON_DEBUG_MESSAGE<<"ch 4 start";
            cv::cvtColor(output, output, CV_BGRA2BGR);
            //qDebug()<<"ch 4 end";
            QLOG_INFO()<<QCOMMON_DEBUG_MESSAGE<<"ch 4 end";
            break;
        }
        case 3:{
            //qDebug()<<"ch 3 start";
            QLOG_INFO()<<QCOMMON_DEBUG_MESSAGE<<"ch 3 start";
            OCV::copy_if_not_same(input, output);
            //qDebug()<<"ch 3 end";
            QLOG_INFO()<<QCOMMON_DEBUG_MESSAGE<<"ch 3 end";
            break;
        }
        case 1:{
            //qDebug()<<"ch1 start";
            OCV::copy_if_not_same(input, output);
            QLOG_INFO()<<QCOMMON_DEBUG_MESSAGE<<"ch 1 start";
            cv::cvtColor(output, output, CV_GRAY2BGR);
            QLOG_INFO()<<QCOMMON_DEBUG_MESSAGE<<"ch1 end";
            //qDebug()<<"ch1 end";
            break;
        }
        default:{            
            break;
        }
        }

        cv::cvtColor(output, output, CV_BGR2HSV);
    }
}

void QHSVCorrection::processAfterChangeSource()
{
    matInput_ = qimage_to_mat_cpy(imageScaled());
    if(!matInput_.empty()){
        convertImage(matInput_, matInput_);
    }
}

/**
 * @brief implementation of saveImage, this function already wrapped by try...catch by the function
 * which call it.
 * @param input : input image
 *
 * @return return true if the image is saved, else return false
 */
cv::Mat QHSVCorrection::saveImplement(QImage &input)
{
    QLOG_INFO()<<QCOMMON_DEBUG_MESSAGE<<"save implement";
    cv::Mat inout = qimage_to_mat_cpy(input);
    convertImage(inout, inout);
    if(!inout.empty()){
        addChannel(inout);
        cv::cvtColor(inout, inout, CV_HSV2BGR);
    }

    return inout;
}


