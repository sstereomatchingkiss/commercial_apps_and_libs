#ifndef QCLAHE_HPP
#define QCLAHE_HPP

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <imageAlgorithm.hpp>

#include "customImage.hpp"

class QCLAHE : public customImage
{
    Q_OBJECT
    Q_PROPERTY(float clipLimit READ clipLimit WRITE setClipLimit NOTIFY clipLimitChanged)
    Q_PROPERTY(int gridSize READ gridSize WRITE setGridSize NOTIFY gridSizeChanged)
public:
    QCLAHE(QQuickItem *parent = nullptr);

    float clipLimit() const;
    int gridSize() const;

    virtual void paint(QPainter *painter);

    void setClipLimit(float value);
    void setGridSize(int value);

signals:
    void clipLimitChanged();

    void gridSizeChanged();

protected:
    virtual void processAfterChangeSource();

    virtual cv::Mat saveImplement(QImage &input);

private:
    OCV::clahe clahe_;
    float clipLimit_;
    int count_; //for setProtector

    int gridSize_;

    cv::Mat matInput_;
    cv::Mat matOutput_;
};

#endif // QCLAHE_HPP
