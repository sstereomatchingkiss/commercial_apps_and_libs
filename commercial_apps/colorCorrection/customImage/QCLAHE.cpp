#include <QQuickItem>

//#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <QsLog.h>
#include <QsLogDest.h>

#include <debugHelper.hpp>
#include <openCVToQt.hpp>

#include "QCLAHE.hpp"
//#include "setBusyProtector.hpp"

QCLAHE::QCLAHE(QQuickItem *parent) : customImage(parent),
    clahe_(),
    clipLimit_(1),
    gridSize_(3)
{
    QLOG_INFO()<<QCOMMON_DEBUG_MESSAGE<<"qclahe start";
}

float QCLAHE::clipLimit() const
{
    return clipLimit_;
}

int QCLAHE::gridSize() const
{
    return gridSize_;
}

void QCLAHE::paint(QPainter *painter)
{
    if(!imageScaled().isNull()){
        //qDebug()<<"painting clahe"
        QLOG_INFO()<<QCOMMON_DEBUG_MESSAGE<<"painting clahe";
        try{           
            if(!matInput_.empty()){
                clahe_.run(matInput_, matOutput_, clipLimit_, cv::Size(gridSize_, gridSize_), colorSpaceCV());
                maskResult(matInput_, matOutput_, rubberBand());                
                QImage const result = mat_to_qimage_ref(matOutput_);
                paintResult(result, painter);
                //paintResult(imageScaled(), painter);
            }else{
                setErrorLog(COMMON_DEBUG_MESSAGE + "matInput is empty");
                paintResult(imageScaled(), painter);
            }
        }catch(std::exception const &ex){
            setErrorLog(COMMON_DEBUG_MESSAGE + ex.what());
        }
    }
}

/**********************************************************
 ****************** implementation ************************
 **********************************************************/

void QCLAHE::processAfterChangeSource()
{
    matInput_ = qimage_to_mat_cpy(imageScaled());
}

/**
 * @brief implementation of saveImage, this function already wrapped by try...catch by the function
 * which call it.
 * @param input : input image
 *
 * @return return true if the image is saved, else return false
 */
cv::Mat QCLAHE::saveImplement(QImage &input)
{
    QLOG_INFO()<<QCOMMON_DEBUG_MESSAGE<<"clahe save implement";   
    cv::Mat inout = qimage_to_mat_cpy(input);
    //qDebug()<<"input channel = "<<inout.channels();
    if(!inout.empty()){
        clahe_.run(inout, inout, clipLimit_, cv::Size(gridSize_, gridSize_), colorSpaceCV());               
    }

    return inout;
}

void QCLAHE::setClipLimit(float value)
{
    QLOG_INFO()<<QCOMMON_DEBUG_MESSAGE<<"set clahe clip limit";
    //setBusyProtector protect(this);
    if(value != clipLimit_){
        clipLimit_ = value;
        update();
        emit clipLimitChanged();
    }
}

void QCLAHE::setGridSize(int value)
{
    QLOG_INFO()<<QCOMMON_DEBUG_MESSAGE<<"set clahe grid size";
    //setBusyProtector protect(this);
    if(value != gridSize_){
        gridSize_ = value;
        update();
        emit gridSizeChanged();
    }
}
