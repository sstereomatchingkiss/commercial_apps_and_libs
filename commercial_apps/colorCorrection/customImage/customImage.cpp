#include <functional>
#include <string>

#include <opencv2/imgproc/imgproc.hpp>

#include <QtConcurrent/QtConcurrentMap>
#include <QtConcurrent/QtConcurrentRun>
#include <QDir>
#include <QFontMetrics>
#include <QMutexLocker>
#include <QPainter>

//#include <opencv2/core/core.hpp>
//#include <opencv2/highgui/highgui.hpp>

#include <QsLog.h>
#include <QsLogDest.h>

#include <imageAlgorithm.hpp>
#include <colorCorrection/piecewiseAffHist/piecewiseAffHist.hpp>
#include <debugHelper.hpp>
#include <fileAuxiliary.hpp>
#include <openCVToQt.hpp>
#include <qImageHelper.hpp>

#include "customImage.hpp"
#include <file/fileProcessConstant.hpp>
//#include "setBusyProtector.hpp"

customImage::customImage(QQuickItem *parent) : QQuickPaintedItem(parent),    
    colorSpace_(BGR),
    colorSpaceCV_(OCV::colorSpace::BGR),
    imageHeight_(180),
    imageWidth_(320),
    isOneChannel_(false),
    //mutex_(QMutex::Recursive),
    numberOfSeries_(4),
    processManyImages_(false),
    progressExit_(true),
    progressMaximum_(1),
    progressValue_(0),
    rubberBand_(false)
{
    setRenderTarget(QQuickPaintedItem::FramebufferObject);

    // init the logging mechanism
    QsLogging::Logger& logger = QsLogging::Logger::instance();
    logger.setLoggingLevel(QsLogging::TraceLevel);

    QString const sLogPath(QDir(QDir::currentPath()).filePath(QStringLiteral("log.txt")));
    QsLogging::DestinationPtr fileDestination(QsLogging::DestinationFactory::MakeFileDestination(sLogPath) );
    logger.addDestination(fileDestination);

    connect(&progressResults_, SIGNAL(progressValueChanged(int)), this, SLOT(setProgressValue(int)));
    connect(&progressResults_, SIGNAL(finished()), this, SLOT(makeProgressExit()));

    //if there are many images have to be processed and the computer are not powerful enough, open more than
    //one thread may cause the machine of the users become very slow, so I only open one thread
    //in the ver1.1, I will let the users choose the thread number if their machine support
    QThreadPool::globalInstance()->setMaxThreadCount(1);
}

customImage::~customImage()
{
}

customImage::ColorSpaceEnum customImage::colorSpace() const
{
    return colorSpace_;
}

OCV::colorSpace customImage::colorSpaceCV() const
{
    return colorSpaceCV_;
}

QString customImage::errorLog() const
{
    return log_;
}

/**
 * @brief  generate the image for preview, make sure the previewDir() is valid before called
 * @return true if the preview generated, else false
 */
bool customImage::generatePreviewImage()
{    
    if(previewDir().isEmpty() || !QDir(previewDir()).exists()){
        setErrorLog(COMMON_DEBUG_MESSAGE + "preview dir do not exist");
    }

    //qDebug()<<"preview dir : "<<previewDir();
    setPreviewName(generate_noduplicate_number_name(previewDir(), "preview.png", 4));

    return generatePreviewImageImpl();
}

int customImage::imageHeight() const
{    
    return imageHeight_;
}

int customImage::imageWidth() const
{    
    return imageWidth_;
}

bool customImage::isOneChannel() const
{
    return isOneChannel_;
}

bool customImage::rubberBand() const
{
    return rubberBand_;
}

QString customImage::previewDir() const
{
    return previewDir_;
}

QString customImage::previewName() const
{
    return previewName_;
}

bool customImage::progressExit() const
{
    return progressExit_;
}

int customImage::progressMaximum() const
{
    return progressMaximum_;
}

int customImage::progressValue() const
{
    return progressValue_;
}

void customImage::paint(QPainter*)
{
}

/**
 *@brief save the image
 *
 *@param path : the path of the image
 *@param imageType : saving type of the image(the types supported by QImage)
 *@param path : the path of saving image
 *@param series : series name of the image when the name is empty(ex : img0000, img0001, img0002 and so on)

 *
 *ps :the data between qml and c++ are always copy
 */
void customImage::saveImage(QString path, QString imageType, QString series, int numberOfSeries)
{   
    processManyImages_ = false;
    clearErrorLog();

    QList<QString> name = {source_};
    saveImages(path, name, imageType, series, numberOfSeries);
}

void customImage::saveImages(QString path, QList<QString> names, QString imageType, QString series, int numberOfSeries)
{
    namespace ph = std::placeholders;

    //setBusyProtector protect(this);
    setProgressExit(false);
    setProgressMaximum(names.size());
    setProgressValue(0);
    imageType_ = imageType;
    clearErrorLog();
    numberOfSeries_ = numberOfSeries;
    path_ = path;
    processManyImages_ = names.size() > 1 ? true : false;
    series_ = series;
    for(auto &name : names){
        removePrefix(name);
    }
    auto results = QtConcurrent::mapped(names, std::bind(&customImage::saveImagesImplement, this, ph::_1));
    progressResults_.setFuture(results);
}

void customImage::setColorSpace(ColorSpaceEnum colorSpace)
{   
    if(colorSpace_ != colorSpace){
        colorSpace_ = colorSpace;
        colorSpaceCV_ = transformColorSpace();
        update();
        emit colorSpaceChanged();
    }
}

void customImage::setImageHeight(int value)
{    
    if(value != imageHeight_){
        imageHeight_ = value;
        //qDebug()<<"imageHeight : "<<imageHeight_;
        resizeImage();
        update();
        emit imageHeightChanged();
    }
}

void customImage::setImageWidth(int value)
{
    //setBusyProtector protect(this);
    if(value != imageWidth_){
        imageWidth_ = value;
        //qDebug()<<"imageWidth : "<<imageWidth_;
        resizeImage();
        update();

        emit imageWidthChanged();
    }
}

void customImage::setPreviewDir(QString const &address)
{    
    if(previewDir_ != address){
        previewDir_ = address;

        emit previewDirChanged();
    }
}

void customImage::setPreviewName(QString const &name)
{    
    if(previewName_ != name){
        previewName_ = name;

        emit previewNameChanged();
    }
}

void customImage::setRubberBand(bool value)
{
    if(rubberBand_ != value){
        rubberBand_ = value;
        //qDebug()<<"rubberBand = "<<rubberBand_;
        update();

        emit rubberBandChanged();
    }
}

void customImage::setSelectedArea(int x, int y, int width, int height)
{
    cv::Rect const temp = cv::Rect(x, y, width, height);
    if(selectedArea_ != temp){
        selectedArea_ = temp;
        //QSizeF ratio((float)imageScaled_.width() / imageOrigin_.width(), (float)imageScaled_.height() / imageOrigin_.height());
        //cv::Rect const temp(selectedArea_.x * ratio.width(), selectedArea_.y * ratio.height(),
        //                    selectedArea_.width * ratio.width(), selectedArea_.height * ratio.height());
        //qDebug()<<"scale image size = "<<imageScaled_.size();
        //qDebug()<<"ratio = "<<ratio;
        //qDebug()<<"rect = "<<temp.x<<", "<<temp.y<<", "<<temp.width<<", "<<temp.height;
        //qDebug()<<selectedArea_.x<<", "<<selectedArea_.y<<", "<<selectedArea_.width<<", "<<selectedArea_.height;
        update();
    }
}

void customImage::setSource(QString source)
{   
    if(source != ""){
        //qDebug()<<"original source = "<<source;

        removePrefix(source);
        //qDebug()<<"alter source = "<<source;

        if(source != source_){
            source_ = source;
            imageOrigin_.load(source_);
            checkIsOneChannel();
            resizeImage();
            processAfterChangeSource();
            update();

            //qDebug()<<"source : "<<source_;

            emit sourceChanged();
        }
    }
}

QString customImage::source() const
{
    return source_;
}

/**********************************************************
 ****************** implementation ************************
 **********************************************************/

void customImage::checkIsOneChannel()
{
    if(!imageOrigin_.isNull() ){
        if(imageOrigin_.format() == QImage::Format_Indexed8 && isOneChannel_ == false){
            isOneChannel_ = true;
            //qDebug()<<"INDEX 8 is one channel : "<<isOneChannel_;
            QLOG_INFO()<<QCOMMON_DEBUG_MESSAGE<<"INDEX 8 is one channel : "<<isOneChannel_;
            emit isOneChannelChanged();
        }else if(imageOrigin_.format() != QImage::Format_Indexed8 && isOneChannel_ == true){
            isOneChannel_ = false;
            //qDebug()<<"is one channel : "<<isOneChannel_;
            QLOG_INFO()<<QCOMMON_DEBUG_MESSAGE<<"INDEX 8 is one channel : "<<isOneChannel_;
            emit isOneChannelChanged();
        }
    }
}

void customImage::clearErrorLog()
{
    if(!log_.isEmpty()){
        log_.clear();
        emit errorLogChanged();
    }
}

/**
 * @brief  implement detail of generatePreviewImage, don't need to call setBusyProtector
 * because generatePreviewImage already call it
 * @return : should return true if the image generated successful;else return false
 */
bool customImage::generatePreviewImageImpl()
{
    return false;
}

/**
 * @brief get the mask
 * @param input : input image
 * @param scale : true : scale the selected area to the size of the preview image; false : don't scale
 * @return the mask
 */
cv::Mat customImage::getMask(cv::Mat const &input, bool scale)
{
    maskImage_.create(input.rows, input.cols, CV_8U);
    maskImage_.setTo(1);
    if(!scale){
        maskImage_(selectedArea_).setTo(0);
        return maskImage_;
    }

    maskImage_(getMaskArea()).setTo(0);

    return maskImage_;
}

/**
 * @brief get the area(after mapping to the size of preview image) of mask
 * @return the area of mask after mapping
 */
cv::Rect customImage::getMaskArea()
{
    std::pair<float, float> const ratio(static_cast<float>(imageScaled_.width()) / imageOrigin_.width(),
                                        static_cast<float>(imageScaled_.height()) / imageOrigin_.height());
    cv::Rect area(selectedArea_.x * ratio.first, selectedArea_.y * ratio.second,
                  selectedArea_.width * ratio.first, selectedArea_.height * ratio.second);
    if(area.x + area.width >= imageScaled_.width()){
        area.width = imageScaled_.width() - area.x;
    }
    if(area.y + area.height >= imageScaled_.height()){
        area.height = imageScaled_.height() - area.y;
    }

    //qDebug()<<"original image size = "<<imageOrigin_.size();
    //qDebug()<<"scale image size = "<<imageScaled_.size();
    //qDebug()<<"selected area = "<<selectedArea_.x<<", "<<selectedArea_.y<<", "<<selectedArea_.width<<", "<<selectedArea_.height;
    //qDebug()<<"ratio = "<<ratio.first<<", "<<ratio.second;
    //qDebug()<<"selected area * ratio = "<<area.x<<", "<<area.y<<", "<<area.width<<", "<<area.height;

    return area;
}

int customImage::getFontSize(QImage const &input, QString const &text)
{
    int size = 360;
    int const ratio = 4;
    while(size >= 20){
        QFont font(QStringLiteral("times"), size);
        QFontMetrics fm(font);
        if(fm.width(text) > input.width() || fm.height() > input.height() ||
                fm.width(text) > input.width() / ratio || fm.height() > input.height() / ratio){
            size -= 10;
        }else{
            break;
        }
    }

    return size;
}

/**
 * @brief customImage::generateResult
 * @param input : input image
 * @param name : the name of the image(name be saved)
 * @param addWaterMark : true, add water mark; false, don't add
 * @return true if success and vice versa
 */
bool customImage::generateResult(QImage &input, QString const &name, bool addWaterMark)
{
    cv::Mat img = saveImplement(input);
    if(!img.empty()){
        //qDebug()<<"process many images"<<processManyImages_;
        //qDebug()<<"rubberBand"<<rubberBand_;
        if(!processManyImages_ && rubberBand_){
            //qDebug()<<"mask image";
            cv::Mat temp = qimage_to_mat_cpy(input);
            OCV::gray_bgra_2_bgr(temp);
            OCV::gray_bgra_2_bgr(img);
            //QImage result = mat_to_qimage_cpy(img);
            //result.save("/Users/yyyy/Pictures/ColorCorrectionPictures/temp1.jpg");
            maskResult(temp, img, false);
            //result = mat_to_qimage_cpy(img);
            //result.save("/Users/yyyy/Pictures/ColorCorrectionPictures/temp2.jpg");
        }
    }

    QImage result = mat_to_qimage_ref(img);
    if(!result.isNull()){
        if(addWaterMark){
            QPainter p(&result);
            p.setRenderHint(QPainter::TextAntialiasing);
            p.setPen(QPen(Qt::red));
            int const size = getFontSize(result, QStringLiteral("colorCorrection"));
            p.setFont(QFont(QStringLiteral("Times"), size, QFont::Bold));
            p.drawText(QRectF(size, size, result.width(), result.height()), QStringLiteral("colorCorrection"));
        }

#ifdef DEBUG_OK
        //this way we could generate all of the image types without switching them one by one from gui
        //not a optimal solution, should find a better way to build a unit test
        QStringList const suffix = {".bmp", ".jpg", ".JPEG", ".png", ".ppm", ".tiff", ".tif", ".xbm", ".xpm"};
        QFileInfo info(name);
        //qDebug()<<"file name = "<<info.fileName();
        //qDebug()<<"file path = "<<info.filePath();
        //qDebug()<<"base name = "<<info.baseName();
        //qDebug()<<"path = "<<info.path();
        for(auto &data : suffix){
            saveErrorHandle(COMMON_DEBUG_MESSAGE, result, info.path() + "/" + info.completeBaseName() + data);
        }
#else
        saveErrorHandle(COMMON_DEBUG_MESSAGE, result, name);
#endif

        return true;
    }else{
        setErrorLog(COMMON_DEBUG_MESSAGE + "cannot save the image [" + name.toStdString() + "]");
    }

    return false;
}

void customImage::makeProgressExit()
{
    if(!log_.isEmpty()){
        emit errorLogChanged();
    }
    setProgressExit(true);
}

/**
 * @brief mask the output result by the selectedArea(if rubberBand mode is active)
 * @param input : original image need to "mask" onto the output image
 * @param scale : true : scale the selected area to the size of the preview image; false : don't scale
 * @param output : image after mask
 */
void customImage::maskResult(cv::Mat const &input, cv::Mat &output, bool scale)
{
    if(rubberBand_){
        cv::Mat const mask = getMask(input, scale);
        input.copyTo(output, mask);
    }
}

void customImage::paintResult(QImage const &img, QPainter *painter)
{
    if(!img.isNull()){
        painter->drawImage((width() - imageScaled().width()) / 2,
                           (height() - imageScaled().height()) / 2, img);
    }else{
        painter->drawImage((width() - imageScaled().width()) / 2,
                           (height() - imageScaled().height()) / 2, imageScaled());
    }
}

/**
 * @brief process the image when the source changed and scaled
 */
void customImage::processAfterChangeSource()
{
}

void customImage::removePrefix(QString &input)
{
    if(input.contains(fileProcessConstant::qmlPrefix)){
        input.remove(0, fileProcessConstant::removePosition);
    }
}

void customImage::resizeImage()
{    
    if(!imageOrigin_.isNull()){
        imageScaled_ = imageOrigin_.scaled(imageWidth_, imageHeight_, Qt::KeepAspectRatio);
        if(!imageScaled_.isNull()){
            imageScaled_ = qimage_4ch_to_other(imageScaled_, QImage::Format_RGB888);
        }
    }
}

/**
 * @brief handle the error when saving the image(add log, change log_ etc)
 * @param error : error message
 * @param img   : image want to save
 * @param name  : name of the image
 * @return Returns true if the image was successfully saved; otherwise returns false.
 */
bool customImage::saveErrorHandle(std::string const &error, QImage const &img, QString const &name)
{
    if(!img.isNull()){
        if(!img.save(name)){
            setErrorLog(error + "save function of QImage fail to save the image");
            return false;
        }
        return true;
    }

    setErrorLog(error + "Can't save the image because the image is null");

    return false;
}

/**
 * @brief  return the area selected by rubberBand
 * @return the area selected by rubberBand
 */
cv::Rect customImage::selectedArea()
{
    return selectedArea_;
}

/**
 * @brief  process the image and save it, don't need to mess up with the issuse of name or file type,
 * it is the responsibilities of "saveImage" in this class
 *
 * @return Returns image after process
 */
cv::Mat customImage::saveImplement(QImage&)
{
    return cv::Mat();
}

/**
 *@brief save the image
 *
 *@param input : the input image
 *@param path : the path of the image
 *@param imageType : saving type of the image(the types supported by QImage)
 *@param path : the path of saving image
 *@param series : series name of the image when the name is empty(ex : img0000, img0001, img0002 and so on)
 *
 *@return Returns true if the image was successfully saved; otherwise returns false.
 *
 *ps :the data between qml and c++ are always copy
 */
bool customImage::saveImageImpl(QImage &input, QString path, QString imageType, QString series, int numberOfSeries)
{
    try{
        //setBusyProtector protect(this);
        if(!path.isEmpty()){
            removePrefix(path);

            QDir temp(path);
            if(!temp.exists()){
                temp.mkdir(path);
            }
            QString name = generate_noduplicate_number_name(path,
                                                            series + imageType, numberOfSeries);

#ifdef PREVIEW
            return generateResult(input, name, true);
#else
            return generateResult(input, name);
#endif
        }else{
            QDir temp(QDir::homePath() + "/ProcessdImage");
            if(!temp.exists()){
                temp.mkdir(QDir::homePath() + "/ProcessdImage");
            }
            QString name = generate_noduplicate_number_name(QDir::homePath() + "/ProcessdImage",
                                                            series + imageType, numberOfSeries);
#ifdef PREVIEW
            return generateResult(input, name, true);
#else
            return generateResult(input, name);
#endif
        }
    }catch(std::exception const &ex){
        setErrorLog(COMMON_DEBUG_MESSAGE + ex.what());
        return false;
    }
}

int customImage::saveImagesImplement(QString const &name)
{
    QMutexLocker lock(&mutex_);
    //qDebug()<<"multi thread : "<<name;
    //imageOrigin_.load(name);
    QImage img(name);
    //setErrorLog("kekeke");
    //qDebug()<<"name loaded";
    saveImageImpl(img, path_, imageType_, series_, numberOfSeries_);
    //qDebug()<<"saved image";
    return 0; //this return value is meaning less, QtConccurrent::mapped need it; QtConccurrent::map is a better choice but have bugs
}

void customImage::setErrorLog(std::string const &log)
{    
    log_ += QString::fromStdString(log) + "\n";
    //qDebug()<<"log = "<<log_;
    QLOG_DEBUG()<<log_;
    if(!processManyImages_){
        emit errorLogChanged();
    }
}

void customImage::setProgressExit(bool value)
{
    if(progressExit_ != value){
        progressExit_ = value;
        emit progressExitChanged();
    }
}

void customImage::setProgressMaximum(int value)
{
    if(progressMaximum_ != value){
        progressMaximum_ = value;
        emit progressMaximumChanged();
    }
}

void customImage::setProgressValue(int value)
{
    if(value != progressValue_){
        progressValue_ = value;
        emit progressValueChanged();
    }
}

OCV::colorSpace customImage::transformColorSpace()
{
    switch(colorSpace_){
    case BGR:{
        return OCV::colorSpace::BGR;
    }
    case HLS:{
        return OCV::colorSpace::HLS;
    }
    case Hue:{
        return OCV::colorSpace::Hue;
    }
    case Lab:{
        return OCV::colorSpace::Lab;
    }
    case Intensity:{
        return OCV::colorSpace::Intensity;
    }
    case YCrCb:{
        return OCV::colorSpace::YCrCb;
    }
    case YUV:{
        return OCV::colorSpace::YUV;
    }
    default:
        return OCV::colorSpace::BGR;
    }
}

