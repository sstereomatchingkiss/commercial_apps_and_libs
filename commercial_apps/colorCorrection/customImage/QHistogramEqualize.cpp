#include <QPainter>
#include <QQuickItem>

#include <QsLog.h>
#include <QsLogDest.h>

#include <debugHelper.hpp>
#include <imageAlgorithm.hpp>
#include <openCVToQt.hpp>

#include "QHistogramEqualize.hpp"
//#include "setBusyProtector.hpp"

QHistogramEqualize::QHistogramEqualize(QQuickItem *parent): customImage(parent)
{
}

void QHistogramEqualize::paint(QPainter *painter)
{
    //qDebug()<<"paint he";
    QLOG_INFO()<<QCOMMON_DEBUG_MESSAGE<<"paint he";
    if(!imageScaled().isNull()){
        //qDebug()<<"painting he";
        QLOG_INFO()<<QCOMMON_DEBUG_MESSAGE<<"painting he";

        try{            
            if(!matInput_.empty()){
                histogram_equalize(matInput_, matOutput_, colorSpaceCV());
                maskResult(matInput_, matOutput_, rubberBand());                
                QImage const result = mat_to_qimage_ref(matOutput_);
                paintResult(result, painter);
            }else{
                paintResult(imageScaled(), painter);
            }
        }catch(std::exception const &ex){
            setErrorLog(COMMON_DEBUG_MESSAGE + ex.what());
        }
    }
}

/**********************************************************
 ****************** implementation ************************
 **********************************************************/

void QHistogramEqualize::processAfterChangeSource()
{
    matInput_ = qimage_to_mat_cpy(imageScaled());
}

/**
 * @brief implementation of saveImage, this function already wrapped by try...catch by the function
 * which call it.
 * @param input : input image
 *
 * @return return true if the image is saved, else return false
 */
cv::Mat QHistogramEqualize::saveImplement(QImage &input)
{
    QLOG_INFO()<<QCOMMON_DEBUG_MESSAGE<<"save implement";
    cv::Mat inout = qimage_to_mat_cpy(input);
    if(!inout.empty()){
        histogram_equalize(inout, inout, colorSpaceCV());
    }

    return inout;
}
