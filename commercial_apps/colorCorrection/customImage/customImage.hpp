#ifndef CUSTOMIMAGE_HPP
#define CUSTOMIMAGE_HPP

#include <opencv2/core/core.hpp>

#include <QFutureWatcher>
#include <QImage>
#include <QMutex>
#include <QQuickPaintedItem>
#include <QString>

#include <colorCorrection/colorSpaceEnum.hpp>

class QPainter;
class QQuickItem;

namespace cv{

class Mat;

}

/**
 * @brief The customImage class
 */
class customImage : public QQuickPaintedItem
{
    Q_OBJECT
    Q_PROPERTY(ColorSpaceEnum colorSpace READ colorSpace WRITE setColorSpace NOTIFY colorSpaceChanged)
    Q_ENUMS(ColorSpaceEnum)

    Q_PROPERTY(int imageHeight READ imageHeight WRITE setImageHeight NOTIFY imageHeightChanged)
    Q_PROPERTY(int imageWidth READ imageWidth WRITE setImageWidth NOTIFY imageWidthChanged)
    Q_PROPERTY(bool isOneChannel READ isOneChannel NOTIFY isOneChannelChanged)    
    Q_PROPERTY(QString previewDir READ previewDir WRITE setPreviewDir NOTIFY previewDirChanged)    
    Q_PROPERTY(QString previewName READ previewName WRITE setPreviewName NOTIFY previewNameChanged)
    Q_PROPERTY(bool progressExit READ progressExit NOTIFY progressExitChanged)
    Q_PROPERTY(int progressMaximum READ progressMaximum NOTIFY progressMaximumChanged)
    Q_PROPERTY(int progressValue READ progressValue NOTIFY progressValueChanged)
    Q_PROPERTY(bool rubberBand READ rubberBand WRITE setRubberBand NOTIFY rubberBandChanged)
    Q_PROPERTY(QString source READ source WRITE setSource NOTIFY sourceChanged)
    Q_PROPERTY(QString errorLog READ errorLog NOTIFY errorLogChanged)
public:
    enum ColorSpaceEnum{
        BGR,
        HLS,
        Hue,
        Intensity,
        Lab,
        YCrCb,
        YUV,
        LastElement,
        colorSpaceSize = LastElement
    };

    explicit customImage(QQuickItem *parent = nullptr);
    customImage(customImage const&) = delete;
    customImage& operator=(customImage const&) = delete;

    virtual ~customImage();   

    ColorSpaceEnum colorSpace() const;
    OCV::colorSpace colorSpaceCV() const;

    QString errorLog() const;

    Q_INVOKABLE bool generatePreviewImage();

    int imageHeight() const;
    int imageWidth() const;
    bool isOneChannel() const;    

    void paint(QPainter *painter);
    QString previewDir() const;
    QString previewName() const;
    bool progressExit() const;
    int progressMaximum() const;
    int progressValue() const;

    bool rubberBand() const;

    void setColorSpace(ColorSpaceEnum colorSpace);
    Q_INVOKABLE void saveImage(QString path, QString imageType = ".jpg", QString series = "img", int numberOfSeries = 4);
    Q_INVOKABLE void saveImages(QString path, QList<QString> names, QString imageType = ".jpg", QString series = "img", int numberOfSeries = 4);
    void setImageHeight(int value);
    void setImageWidth(int value);
    void setPreviewDir(QString const &address);
    void setPreviewName(QString const &name);  
    void setRubberBand(bool value);
    Q_INVOKABLE void setSelectedArea(int x, int y, int width, int height);
    void setSource(QString source);
    QString source() const;    

protected:    
    virtual void processAfterChangeSource();    

    virtual bool generatePreviewImageImpl();
    cv::Mat getMask(cv::Mat const &input, bool scale);
    cv::Rect getMaskArea();

    QImage& imageOrigin() { return imageOrigin_; }
    QImage& imageScaled() { return imageScaled_; }
    bool processManyImage() const { return processManyImages_; }

    void maskResult(cv::Mat const &input, cv::Mat &output, bool scale);

    void paintResult(QImage const &img, QPainter *painter);    

    void resizeImage();

    virtual bool saveErrorHandle(std::string const &error, QImage const &img, QString const &name);
    virtual cv::Mat saveImplement(QImage &input) = 0;
    cv::Rect selectedArea();
    void setErrorLog(std::string const &log);   

    OCV::colorSpace transformColorSpace();

signals:
    void busyChanged();

    void colorSpaceChanged();

    void errorLogChanged();

    void imageHeightChanged();
    void imageWidthChanged();
    void isOneChannelChanged();    

    void previewDirChanged();
    void previewNameChanged();
    void progressExitChanged();
    void progressMaximumChanged();
    void progressValueChanged();

    void rubberBandChanged();

    void sourceChanged();

private:
    void checkIsOneChannel();
    void clearErrorLog();

    int getFontSize(QImage const &input, QString const &text);
    bool generateResult(QImage &input, QString const &name, bool addWaterMark = false);

    void removePrefix(QString &input);

    bool saveImageImpl(QImage &input, QString path, QString imageType, QString series, int numberOfSeries);
    int  saveImagesImplement(QString const &name);
    void setProgressExit(bool value);
    void setProgressMaximum(int value);

private slots:
    void makeProgressExit();
    void setProgressValue(int value);

private:          
    ColorSpaceEnum      colorSpace_;   //color space of the image
    OCV::colorSpace     colorSpaceCV_; //color space of the image too, but a different type which use on the imageAlgorithms module

    QImage imageOrigin_; //original image first loaded
    QImage imageScaled_; //original image after scale
    int imageHeight_;    //imageHeight_ and imageWidth_ are needed to trigger the update() function
    QString imageType_;
    int imageWidth_;
    bool isOneChannel_; //if the channel is one, return true else return false    

    QString log_;     //record error message if any    

    cv::Mat maskImage_;
    QMutex mutex_;

    int numberOfSeries_;

    QString path_;
    QString previewDir_;    //the preivew image will save at this dir
    QString previewName_;   //the name of the preview image(path + name)
    bool processManyImages_; //indicate it is processing single image or more than one image
    bool progressExit_;     //indicate the process of multiple image processing exit or not
    int progressMaximum_;   //same as the number of the images need to process
    QFutureWatcher<int> progressResults_; //I need this to update the ProgressBar value, can't use QtConcurrent::map since there are some
                                          //bug in Qt5.1
    int progressValue_;  //progress of multiple images

    bool rubberBand_; //true : the algorithm should process the region wrapped by the rubberband; false, process the whole image

    QString series_;//series name of the image
    QString source_; //address of the origin image
    cv::Rect selectedArea_; //the area selected by rubberBand
};

#endif // CUSTOMIMAGE_HPP
