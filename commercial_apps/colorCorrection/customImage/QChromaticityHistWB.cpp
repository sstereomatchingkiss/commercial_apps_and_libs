#include <QQuickItem>

//#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <QsLog.h>
#include <QsLogDest.h>

#include <debugHelper.hpp>
#include <openCVToQt.hpp>

#include "QChromaticityHistWB.hpp"

QChromaticityHistWB::QChromaticityHistWB(QQuickItem *parent) : customImage(parent)
{
}

void QChromaticityHistWB::paint(QPainter *painter)
{
    if(!imageScaled().isNull()){
        //qDebug()<<"painting chromaticityHistWB"
        QLOG_INFO()<<QCOMMON_DEBUG_MESSAGE<<"painting chromaticityHistWB";
        try{            
            if(!matInput_.empty()){
                chwb_.run(matInput_, matOutput_);
                maskResult(matInput_, matOutput_, rubberBand());                
                QImage const result = mat_to_qimage_ref(matOutput_);
                paintResult(result, painter);
            }else{
                setErrorLog(COMMON_DEBUG_MESSAGE + "matInput is empty");
                paintResult(imageScaled(), painter);
            }
            //qDebug()<<"channels = "<<matInput_.channels();
            //cv::cvtColor(matInput_, matInput_, CV_BGR2RGB);
        }catch(std::exception const &ex){
            setErrorLog(COMMON_DEBUG_MESSAGE + ex.what());
        }
    }
}

/**********************************************************
 ****************** implementation ************************
 **********************************************************/

void QChromaticityHistWB::processAfterChangeSource()
{
    matInput_ = qimage_to_mat_cpy(imageScaled());
}

/**
 * @brief implementation of saveImage, this function already wrapped by try...catch by the function
 * which call it.
 * @param input : input image
 *
 * @return return true if the image is saved, else return false
 */
cv::Mat QChromaticityHistWB::saveImplement(QImage &input)
{
    QLOG_INFO()<<QCOMMON_DEBUG_MESSAGE<<"chromaticityHistWB save implement";
    cv::Mat inout = qimage_to_mat_cpy(input);
    if(!inout.empty()){
        chwb_.run(inout, inout);
    }

    return inout;
}
