//#include <QDebug>

#include <QQuickItem>

#include <debugHelper.hpp>
#include <imageAlgorithm.hpp>
#include <openCVToQt.hpp>

#include <QsLog.h>
#include <QsLogDest.h>

#include "QGammaCorrection.hpp"
//#include "setBusyProtector.hpp"

QGammaCorrection::QGammaCorrection(QQuickItem *parent) : customImage(parent),
    gamma_(0)
{
    //qDebug()<<"gamma create";
    QLOG_INFO()<<QCOMMON_DEBUG_MESSAGE<<"gamma created";
}

float QGammaCorrection::gamma() const
{
    return gamma_;
}

void QGammaCorrection::paint(QPainter *painter)
{
    //qDebug()<<"paint gamma";
    QLOG_INFO()<<QCOMMON_DEBUG_MESSAGE<<"paint gamma";
    if(!imageScaled().isNull()){
        //qDebug()<<"painting gamma";
        QLOG_INFO()<<QCOMMON_DEBUG_MESSAGE<<"painting gamma";
        try{            
            if(!matInput_.empty()){                
                gamma_correction_lazy(matInput_, matOutput_, gamma_, colorSpaceCV());
                maskResult(matInput_, matOutput_, rubberBand());                
                QImage const result = mat_to_qimage_ref(matOutput_);                
                paintResult(result, painter);
            }else{
                paintResult(imageScaled(), painter);
            }
        }catch(std::exception const &ex){
            setErrorLog(COMMON_DEBUG_MESSAGE + ex.what());
        }
    }
}

void QGammaCorrection::setGamma(float value)
{
    QLOG_INFO()<<QCOMMON_DEBUG_MESSAGE<<"set gamma";
    //setBusyProtector protect(this);
    if(gamma_ != value){
        gamma_ = value;
        //qDebug() << "gamma : "<<gamma_;
        update();
        emit gammaChanged();
    }
}

/**********************************************************
 ****************** implementation ************************
 **********************************************************/

void QGammaCorrection::processAfterChangeSource()
{
    matInput_ = qimage_to_mat_cpy(imageScaled());
}

/**
 * @brief implementation of saveImage, this function already wrapped by try...catch by the function
 * which call it.
 * @param input : input image
 *
 * @return return true if the image is saved, else return false
 */
cv::Mat QGammaCorrection::saveImplement(QImage &input)
{
    QLOG_INFO()<<QCOMMON_DEBUG_MESSAGE<<"save implement";    
    cv::Mat inout = qimage_to_mat_cpy(input);
    if(!inout.empty()){
        gamma_correction_lazy(inout, inout, gamma_, colorSpaceCV());               
    }

    return inout;
}
