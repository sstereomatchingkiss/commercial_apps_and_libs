//#include <QDebug>

#include <QPainter>

#include <QsLog.h>
#include <QsLogDest.h>

#include <basicImageAlgo.hpp>
#include <colorCorrection/piecewiseAffHist/piecewiseAffHist.hpp>
#include <debugHelper.hpp>
#include <openCVToQt.hpp>

#include "QPiecewiseAffHist.hpp"
//#include "setBusyProtector.hpp"

QPiecewiseAffHist::QPiecewiseAffHist(QQuickItem *parent) : customImage(parent),
    interval_(0),
    maxSlope_(0),
    minSlope_(0)
{    
    QLOG_INFO()<<QCOMMON_DEBUG_MESSAGE<<"pae created";
}

int QPiecewiseAffHist::interval() const
{
    return interval_;
}

int QPiecewiseAffHist::maxSlope() const
{
    return maxSlope_;
}

int QPiecewiseAffHist::minSlope() const
{
    return minSlope_;
}

void QPiecewiseAffHist::paint(QPainter *painter)
{
    //qDebug()<<"paint pah";
    QLOG_INFO()<<QCOMMON_DEBUG_MESSAGE<<"paint pah";
    if(!imageScaled().isNull()){
        //qDebug()<<"painting pah";
        QLOG_INFO()<<QCOMMON_DEBUG_MESSAGE<<"painting pah";
        try{
            if(interval_ != 0 || minSlope_ != 0 || maxSlope_ != 0){               
                if(!matInput_.empty()){
                    piecewise_affine_hist(matInput_, matOutput_, interval_, minSlope_, maxSlope_, colorSpaceCV());
                    maskResult(matInput_, matOutput_, rubberBand());                    
                    paintResult(mat_to_qimage_ref(matOutput_), painter);
                }else{
                    QLOG_INFO()<<QCOMMON_DEBUG_MESSAGE<<"pah result is empty";
                    paintResult(imageScaled(), painter);
                }
            }else{
                paintResult(imageScaled(), painter);
            }
        }catch(std::exception const &ex){
            setErrorLog(COMMON_DEBUG_MESSAGE + ex.what());
        }
    }
}


void QPiecewiseAffHist::setInterval(int interval)
{
    QLOG_INFO()<<QCOMMON_DEBUG_MESSAGE<<"set interval";
    //setBusyProtector protect(this);
    if(interval_ != interval){
        interval_ = interval;
        update();
        emit intervalChanged();
    }
}

void QPiecewiseAffHist::setMaxSlope(int slope)
{
    QLOG_INFO()<<QCOMMON_DEBUG_MESSAGE<<"set max slope";
    //setBusyProtector protect(this);
    if(slope != maxSlope_){
        maxSlope_ = slope;
        update();
        emit maxSlopeChanged();
    }
}

void QPiecewiseAffHist::setMinSlope(int slope)
{
    QLOG_INFO()<<QCOMMON_DEBUG_MESSAGE<<"set min slope";
    //setBusyProtector protect(this);
    if(slope != minSlope_){
        minSlope_ = slope;
        update();
        emit minSlopeChanged();
    }
}

/**********************************************************
 ****************** implementation ************************
 **********************************************************/

void QPiecewiseAffHist::processAfterChangeSource()
{
    matInput_ = qimage_to_mat_cpy(imageScaled());
}

/**
 * @brief implementation of saveImage, this function already wrapped by try...catch by the function
 * which call it.
 * @param input : input image
 *
 * @return return true if the image is saved, else return false
 */
cv::Mat QPiecewiseAffHist::saveImplement(QImage &input)
{
    QLOG_INFO()<<QCOMMON_DEBUG_MESSAGE<<"save implement";
    cv::Mat inout = qimage_to_mat_cpy(input);
    if(!inout.empty()){
        piecewise_affine_hist(inout, inout, interval_, minSlope_, maxSlope_, colorSpaceCV());        
    }

    return inout;
}
