#ifndef QHSVCORRECTION_HPP
#define QHSVCORRECTION_HPP

#include <opencv2/core/core.hpp>

#include "customImage.hpp"

class QHSVCorrection : public customImage
{
    Q_OBJECT
    Q_PROPERTY(int valueH READ valueH WRITE setValueH NOTIFY valueHChanged)
    Q_PROPERTY(int valueS READ valueS WRITE setValueS NOTIFY valueSChanged)
    Q_PROPERTY(int valueV READ valueV WRITE setValueV NOTIFY valueVChanged)
public:
    explicit QHSVCorrection(QQuickItem *parent = nullptr);    

    void paint(QPainter *painter);

    void setValueH(int value);
    void setValueS(int value);
    void setValueV(int value);

    int valueH() const;
    int valueS() const;
    int valueV() const;

protected:

    virtual void processAfterChangeSource();

    virtual cv::Mat saveImplement(QImage &input);

signals:
    void valueHChanged();
    void valueSChanged();
    void valueVChanged();

private:
    void addChannel(cv::Mat &mat);
    void convertImage(cv::Mat &input, cv::Mat &output);

private:
    cv::Mat matInput_;
    cv::Mat matOutput_;

    int valueH_;
    int valueS_;
    int valueV_;
};

#endif // QHSVCORRECTION_HPP
