#ifndef QPERFECTREFLECTASSUMPTION_HPP
#define QPERFECTREFLECTASSUMPTION_HPP

#include <opencv2/core/core.hpp>

#include <colorCorrection/whiteBalance/perfectReflectorAssumption.hpp>

#include "customImage.hpp"

class QPerfectReflectAssumption : public customImage
{
    Q_OBJECT
    Q_PROPERTY(int ratio READ ratio WRITE setRatio NOTIFY ratioChanged)
public:
    explicit QPerfectReflectAssumption(QQuickItem *parent = nullptr);

    void paint(QPainter *painter);

    int ratio() const;

    void setRatio(int value);

protected:
    virtual void processAfterChangeSource();

    virtual cv::Mat saveImplement(QImage &input);

signals:
    void ratioChanged();

private:
    OCV::perfectReflectorAssumption perfectReflector_;

    int ratio_;

    cv::Mat matInput_;
    cv::Mat matOutput_;
};

#endif // QPERFECTREFLECTASSUMPTION_HPP
