#ifndef QPIECEWISEAFFHIST_HPP
#define QPIECEWISEAFFHIST_HPP

#include <opencv2/core/core.hpp>

#include "customImage.hpp"

class QPiecewiseAffHist : public customImage
{
    Q_OBJECT
    Q_PROPERTY(int interval READ interval WRITE setInterval NOTIFY intervalChanged)
    Q_PROPERTY(int minSlope READ minSlope WRITE setMinSlope NOTIFY minSlopeChanged)
    Q_PROPERTY(int maxSlope READ maxSlope WRITE setMaxSlope NOTIFY maxSlopeChanged)
public:    
    explicit QPiecewiseAffHist(QQuickItem *parent = nullptr);

    int interval() const;
    int maxSlope() const;
    int minSlope() const;

    void paint(QPainter *painter);

    void setInterval(int interval);
    void setMaxSlope(int slope);
    void setMinSlope(int slope);

signals:    
    void intervalChanged();
    void maxSlopeChanged();
    void minSlopeChanged();

protected:
    virtual void processAfterChangeSource();

    virtual cv::Mat saveImplement(QImage &input);

private:
    int interval_;

    cv::Mat matInput_;
    cv::Mat matOutput_;
    int maxSlope_;
    int minSlope_;    

    QString source_;
};

#endif // QPIECEWISEAFFHIST_HPP
