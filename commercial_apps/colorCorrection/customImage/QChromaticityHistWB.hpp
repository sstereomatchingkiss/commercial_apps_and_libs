#ifndef QCHROMATICITYHISTWB_HPP
#define QCHROMATICITYHISTWB_HPP

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <colorCorrection/whiteBalance/chromaticyHistWB.hpp>

#include "customImage.hpp"

class QChromaticityHistWB : public customImage
{
public:
    QChromaticityHistWB(QQuickItem *parent = nullptr);

    virtual void paint(QPainter *painter);

protected:
    virtual void processAfterChangeSource();

    virtual cv::Mat saveImplement(QImage &input);

private:
    OCV::chromaticyHistWB chwb_;

    cv::Mat matInput_;
    cv::Mat matOutput_;
};

#endif // QCHROMATICITYHISTWB_HPP
