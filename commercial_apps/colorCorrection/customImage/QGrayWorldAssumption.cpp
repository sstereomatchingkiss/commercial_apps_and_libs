//#include <QDebug>

#include <QQuickItem>

#include <QsLog.h>
#include <QsLogDest.h>

#include <colorCorrection/whiteBalance/grayWorldAssumption.hpp>
#include <debugHelper.hpp>
#include <openCVToQt.hpp>

#include "QGrayWorldAssumption.hpp"

QGrayWorldAssumption::QGrayWorldAssumption(QQuickItem *parent) :
    customImage(parent)
{
    QLOG_INFO()<<QCOMMON_DEBUG_MESSAGE<<"gray world created";
}

void QGrayWorldAssumption::paint(QPainter *painter)
{
    //qDebug()<<"paint gray world";
    QLOG_INFO()<<QCOMMON_DEBUG_MESSAGE<<"paint gray world";
    if(!imageScaled().isNull()){
        //qDebug()<<"painting gray world";
        QLOG_INFO()<<QCOMMON_DEBUG_MESSAGE<<"painting gray world";
        try{            
            if(!matInput_.empty()){
                OCV::gray_world_assumption(matInput_, matOutput_);
                maskResult(matInput_, matOutput_, rubberBand());                
                QImage const result = mat_to_qimage_ref(matOutput_);
                paintResult(result, painter);
            }else{
                paintResult(imageScaled(), painter);
            }
        }catch(std::exception const &ex){
            setErrorLog(COMMON_DEBUG_MESSAGE + ex.what());
        }
    }
}

/**********************************************************
 ****************** implementation ************************
 **********************************************************/

void QGrayWorldAssumption::processAfterChangeSource()
{
    matInput_ = qimage_to_mat_cpy(imageScaled());
}

/**
 * @brief implementation of saveImage, this function already wrapped by try...catch by the function
 * which call it.
 * @param input : input image
 *
 * @return return true if the image is saved, else return false
 */
cv::Mat QGrayWorldAssumption::saveImplement(QImage &input)
{
    QLOG_INFO()<<QCOMMON_DEBUG_MESSAGE<<"save implement";
    cv::Mat inout = qimage_to_mat_cpy(input);
    if(!inout.empty()){
        OCV::gray_world_assumption(inout, inout);        
    }

    return inout;
}
