#ifndef QGAMMACORRECTION_HPP
#define QGAMMACORRECTION_HPP

#include <opencv2/core/core.hpp>

#include "customImage.hpp"

class QGammaCorrection : public customImage
{
    Q_OBJECT
    Q_PROPERTY(float gamma READ gamma WRITE setGamma NOTIFY gammaChanged)
public:
    explicit QGammaCorrection(QQuickItem *parent = nullptr);    

    float gamma() const;

    void paint(QPainter *painter);

    void setGamma(float value);

signals:
    void gammaChanged();

protected:    
    virtual void processAfterChangeSource();

    virtual cv::Mat saveImplement(QImage &input);

private:
    int count_;

    float gamma_;

    cv::Mat matInput_;
    cv::Mat matOutput_;
};

#endif // QGAMMACORRECTION_HPP
