#ifndef QWHITEBALANCEBGR_HPP
#define QWHITEBALANCEBGR_HPP

#include <opencv2/core/core.hpp>

#include "customImage.hpp"

class QWhiteBalanceBGR : public customImage
{
    Q_OBJECT
    Q_PROPERTY(int intensity READ intensity WRITE setIntensity NOTIFY intensityChanged)
    Q_PROPERTY(int valueB READ valueB WRITE setValueB NOTIFY valueBChanged)
    Q_PROPERTY(int valueG READ valueG WRITE setValueG NOTIFY valueGChanged)
    Q_PROPERTY(int valueR READ valueR WRITE setValueR NOTIFY valueRChanged)

public:
    explicit QWhiteBalanceBGR(QQuickItem *parent = nullptr);

    int intensity() const;

    void paint(QPainter *painter);

    void setIntensity(int value);
    void setValueB(int value);
    void setValueG(int value);
    void setValueR(int value);

    int valueB() const;
    int valueG() const;
    int valueR() const;

signals:
    void intensityChanged();

    void valueBChanged();
    void valueGChanged();
    void valueRChanged();

protected:
    virtual void processAfterChangeSource();

    virtual cv::Mat saveImplement(QImage &input);

private:
    int intensity_;

    cv::Mat matInput_;
    cv::Mat matOutput_;

    cv::Vec3f originalWhite_;

    int valueB_;
    int valueG_;
    int valueR_;
};

#endif // QWHITEBALANCEBGR_HPP
