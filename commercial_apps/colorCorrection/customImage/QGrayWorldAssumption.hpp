#ifndef QGRAYWORLDASSUMPTION_HPP
#define QGRAYWORLDASSUMPTION_HPP

#include "customImage.hpp"

class QGrayWorldAssumption : public customImage
{
public:
    explicit QGrayWorldAssumption(QQuickItem *parent = nullptr);

    void paint(QPainter *painter);

protected:
     virtual void processAfterChangeSource();

     virtual cv::Mat saveImplement(QImage &input);

private:

    cv::Mat matInput_;
    cv::Mat matOutput_;
};

#endif // QGRAYWORLDASSUMPTION_HPP
