#include <QPainter>
#include <QQuickItem>

#include <QsLog.h>
#include <QsLogDest.h>

#include <basicImageAlgo.hpp>
#include <colorCorrection/whiteBalance/monitorBGR.hpp>
#include <debugHelper.hpp>
#include <openCVToQt.hpp>

#include "QWhiteBalanceBGR.hpp"

QWhiteBalanceBGR::QWhiteBalanceBGR(QQuickItem *parent) : customImage(parent),
    intensity_(0),
    originalWhite_(255, 255, 255),
    valueB_(0),
    valueG_(0),
    valueR_(0)
{
}

int QWhiteBalanceBGR::intensity() const
{
    return intensity_;
}

void QWhiteBalanceBGR::paint(QPainter *painter)
{
    //qDebug()<<"paint wb";
    QLOG_INFO()<<QCOMMON_DEBUG_MESSAGE<<"paint wb";
    if(!imageScaled().isNull()){
        //qDebug()<<"painting wb";
        QLOG_INFO()<<QCOMMON_DEBUG_MESSAGE<<"painting wb";

        try{
            //matInput_ = qimage_to_mat_ref(imageScaled());
            if(!matInput_.empty()){                
                if(matInput_.channels() != 1){
                    //qDebug()<<"BGR is not one channel";
                    QLOG_INFO()<<QCOMMON_DEBUG_MESSAGE<<"BGR is not one channel";
                    if(valueB_ == 0 && valueG_ == 0 && valueR_ == 0){
                        paintResult(imageScaled(), painter);
                    }else{
                        OCV::monitor_bgr(matInput_, matOutput_, cv::Vec3f(valueB_, valueG_, valueR_), originalWhite_);
                        maskResult(matInput_, matOutput_, rubberBand());
                        paintResult(mat_to_qimage_ref(matOutput_), painter);
                    }
                }else{
                    //qDebug()<<"BGR is one channel";
                    QLOG_INFO()<<QCOMMON_DEBUG_MESSAGE<<"BGR is one channel";
                    if(intensity_ == 0){
                        paintResult(imageScaled(), painter);
                    }else{
                        OCV::monitor_bgr(matInput_, matOutput_, cv::Vec3f(intensity_, intensity_, intensity_), originalWhite_);
                        maskResult(matInput_, matOutput_, rubberBand());
                        paintResult(mat_to_qimage_ref(matOutput_), painter);
                    }
                }                              

            }else{
                QLOG_INFO()<<QCOMMON_DEBUG_MESSAGE<<"monitor bgr result is empty";
                paintResult(imageScaled(), painter);
            }
        }catch(std::exception const &ex){
            setErrorLog(COMMON_DEBUG_MESSAGE + ex.what());
        }
    }
}

void QWhiteBalanceBGR::setIntensity(int value)
{
    QLOG_INFO()<<QCOMMON_DEBUG_MESSAGE<<"set intensity";
    //setBusyProtector protect(this);
    if(value != intensity_){
        intensity_ = value;
        update();
        emit intensityChanged();
    }
}

void QWhiteBalanceBGR::setValueB(int value)
{
    QLOG_INFO()<<QCOMMON_DEBUG_MESSAGE<<"set b";
    //setBusyProtector protect(this);
    if(value != valueB_){
        valueB_ = value;
        update();
        emit valueBChanged();
    }
}

void QWhiteBalanceBGR::setValueG(int value)
{
    QLOG_INFO()<<QCOMMON_DEBUG_MESSAGE<<"set g";
    //setBusyProtector protect(this);
    if(value != valueG_){
        valueG_ = value;
        update();
        emit valueGChanged();
    }
}

void QWhiteBalanceBGR::setValueR(int value)
{
    QLOG_INFO()<<QCOMMON_DEBUG_MESSAGE<<"set r";
    //setBusyProtector protect(this);
    if(value != valueR_){
        valueR_ = value;
        update();
        emit valueRChanged();
    }
}

int QWhiteBalanceBGR::valueB() const
{
    return valueB_;
}

int QWhiteBalanceBGR::valueG() const
{
    return valueG_;
}

int QWhiteBalanceBGR::valueR() const
{
    return valueR_;
}

/**********************************************************
 ****************** implementation ************************
 **********************************************************/

void QWhiteBalanceBGR::processAfterChangeSource()
{    
    QLOG_INFO()<<QCOMMON_DEBUG_MESSAGE<<"process after change source";
    matInput_ = qimage_to_mat_cpy(imageScaled());
    if(!matInput_.empty()){
        if(OCV::is_uchar_channel(matInput_.type())){
            originalWhite_ = cv::Vec3f(255, 255, 255);
        }else{
            originalWhite_ = cv::Vec3f(1.0, 1.0, 1.0);
        }
    }
}

/**
 * @brief implementation of saveImage, this function already wrapped by try...catch by the function
 * which call it.
 * @param input : input image
 *
 * @return return true if the image is saved, else return false
 */
cv::Mat QWhiteBalanceBGR::saveImplement(QImage &input)
{
    QLOG_INFO()<<QCOMMON_DEBUG_MESSAGE<<"save implement";
    cv::Mat inout = qimage_to_mat_cpy(input);
    if(!inout.empty()){
        if(inout.channels() != 1){
            OCV::monitor_bgr(inout, inout, cv::Vec3f(valueB_, valueG_, valueR_), originalWhite_);
        }else{
            //qDebug()<<"white b begin";
            OCV::monitor_bgr(inout, inout, cv::Vec3f(intensity_, intensity_, intensity_), originalWhite_);
            //qDebug()<<"white b end";
        }
    }

    return inout;
}
