//#include <QDebug>

#include <QPainter>
#include <QQuickItem>

#include <QsLog.h>
#include <QsLogDest.h>

#include <colorCorrection/simplestColorBalance/simplestColorBalance.hpp>
#include <debugHelper.hpp>
#include <openCVToQt.hpp>

#include "QSimplestColorBalance.hpp"
//#include "setBusyProtector.hpp"

QSimplestColorBalance::QSimplestColorBalance(QQuickItem *parent) : customImage(parent),
    maxPercentile_(0),
    minPercentile_(0)
{
    //qDebug()<<"scbCreate";
    QLOG_INFO()<<QCOMMON_DEBUG_MESSAGE<<"simplest cb created";
}

int QSimplestColorBalance::minPercentile() const
{
    return minPercentile_;
}

int QSimplestColorBalance::maxPercentile() const
{
    return maxPercentile_;
}

void QSimplestColorBalance::paint(QPainter *painter)
{
    //qDebug()<<"paint scb";
    QLOG_INFO()<<QCOMMON_DEBUG_MESSAGE<<"paint scb";
    if(!imageScaled().isNull()){
        //qDebug()<<"painting scb";
        QLOG_INFO()<<QCOMMON_DEBUG_MESSAGE<<"painting scb";

        try{
            if(minPercentile_ != 0 || maxPercentile_ != 0){               
                if(!matInput_.empty()){
                    OCV::simplestColorBalance().simplest_color_balance(matInput_, matOutput_, minPercentile_, maxPercentile_, colorSpaceCV());
                    maskResult(matInput_, matOutput_, rubberBand());                    
                    QImage const result = mat_to_qimage_ref(matOutput_);
                    paintResult(result, painter);
                }else{
                    QLOG_INFO()<<QCOMMON_DEBUG_MESSAGE<<"scb result is empty";
                    paintResult(imageScaled(), painter);
                }
            }else{
                paintResult(imageScaled(), painter);
            }
        }catch(std::exception const &ex){
            setErrorLog(COMMON_DEBUG_MESSAGE + ex.what());
        }
    }
}

void QSimplestColorBalance::setMaxPercentile(int max)
{
    QLOG_INFO()<<QCOMMON_DEBUG_MESSAGE<<"set max percentile";
    //setBusyProtector protect(this);
    if(max != maxPercentile_){
        maxPercentile_ = max;
        update();
        emit maxPercentileChanged();
    }
}

void QSimplestColorBalance::setMinPercentile(int min)
{
    QLOG_INFO()<<QCOMMON_DEBUG_MESSAGE<<"set min percentile";
    //setBusyProtector protect(this);
    if(min != maxPercentile_){
        minPercentile_ = min;
        update();
        emit minPercentileChanged();
    }
}

/**********************************************************
 ****************** implementation ************************
 **********************************************************/

void QSimplestColorBalance::processAfterChangeSource()
{
    matInput_ = qimage_to_mat_cpy(imageScaled());
}

/**
 * @brief implementation of saveImage, this function already wrapped by try...catch by the function
 * which call it.
 * @param input : input image
 *
 * @return return true if the image is saved, else return false
 */
cv::Mat QSimplestColorBalance::saveImplement(QImage &input)
{
    QLOG_INFO()<<QCOMMON_DEBUG_MESSAGE<<"save implement";
    cv::Mat inout = qimage_to_mat_cpy(input);
    if(!inout.empty()){
        OCV::simplestColorBalance().simplest_color_balance(inout, inout, minPercentile_, maxPercentile_, colorSpaceCV());        
    }

    return inout;
}
