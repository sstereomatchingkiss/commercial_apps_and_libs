//#include <QDebug>

#include <QPainter>
#include <QQuickItem>

#include <QsLog.h>
#include <QsLogDest.h>

#include <debugHelper.hpp>
#include <openCVToQt.hpp>

#include "QPerfectReflectAssumption.hpp"
//#include "setBusyProtector.hpp"

QPerfectReflectAssumption::QPerfectReflectAssumption(QQuickItem *parent) : customImage(parent),
    ratio_(0)
{
}

void QPerfectReflectAssumption::paint(QPainter *painter)
{
    //qDebug()<<"paint perfect world";
    QLOG_INFO()<<QCOMMON_DEBUG_MESSAGE<<"paint perfect world";
    if(!imageScaled().isNull()){
        //qDebug()<<"painting perfect world";
        QLOG_INFO()<<QCOMMON_DEBUG_MESSAGE<<"painting perfect world";
        try{
            if(ratio_ != 0){               
                if(!matInput_.empty()){
                    perfectReflector_.run(matInput_, matOutput_, ratio_);
                    maskResult(matInput_, matOutput_, rubberBand());                    
                    QImage const result = mat_to_qimage_ref(matOutput_);
                    paintResult(result, painter);
                }else{
                    paintResult(imageScaled(), painter);
                }
            }else{
                paintResult(imageScaled(), painter);
            }
        }catch(std::exception const &ex){
            setErrorLog(COMMON_DEBUG_MESSAGE + ex.what());
        }
    }
}

int QPerfectReflectAssumption::ratio() const
{
    return ratio_;
}

void QPerfectReflectAssumption::setRatio(int value)
{
    QLOG_INFO()<<QCOMMON_DEBUG_MESSAGE<<"set ratio";
    //setBusyProtector protect(this);
    if(value != ratio_){
        ratio_ = value;
        update();
        emit ratioChanged();
    }
}

/**********************************************************
 ****************** implementation ************************
 **********************************************************/

void QPerfectReflectAssumption::processAfterChangeSource()
{
    matInput_ = qimage_to_mat_cpy(imageScaled());
}

/**
 * @brief implementation of saveImage, this function already wrapped by try...catch by the function
 * which call it.
 * @param input : input image
 *
 * @return return true if the image is saved, else return false
 */
cv::Mat QPerfectReflectAssumption::saveImplement(QImage &input)
{
    QLOG_INFO()<<QCOMMON_DEBUG_MESSAGE<<"save implement";
    cv::Mat inout = qimage_to_mat_cpy(input);
    if(!inout.empty()){
        perfectReflector_.run(inout, inout, ratio_);        
    }

    return inout;
}
