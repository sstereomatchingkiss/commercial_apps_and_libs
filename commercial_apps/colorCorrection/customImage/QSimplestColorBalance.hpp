#ifndef QSIMPLESTCOLORBALANCE_HPP
#define QSIMPLESTCOLORBALANCE_HPP

#include <opencv2/core/core.hpp>

#include "customImage.hpp"

class QSimplestColorBalance : public customImage
{
    Q_OBJECT
    Q_PROPERTY(int minPercentile READ minPercentile WRITE setMinPercentile NOTIFY minPercentileChanged)
    Q_PROPERTY(int maxPercentile READ maxPercentile WRITE setMaxPercentile NOTIFY maxPercentileChanged)
public:
    explicit QSimplestColorBalance(QQuickItem *parent = nullptr);    

    int minPercentile() const;
    int maxPercentile() const;

    void paint(QPainter *painter);

    void setMaxPercentile(int max);
    void setMinPercentile(int min);

signals:
    void maxPercentileChanged();
    void minPercentileChanged();

protected:
    virtual void processAfterChangeSource();

    virtual cv::Mat saveImplement(QImage &input);

private:
    cv::Mat matInput_;
    cv::Mat matOutput_;
    int maxPercentile_;
    int minPercentile_;
};

#endif // QSIMPLESTCOLORBALANCE_HPP
