#ifndef QHISTOGRAMEQUALIZE_HPP
#define QHISTOGRAMEQUALIZE_HPP

#include <opencv2/core/core.hpp>

#include "customImage.hpp"

class QHistogramEqualize : public customImage
{
public:
    explicit QHistogramEqualize(QQuickItem *parent = nullptr);

    void paint(QPainter *painter);

protected:
    virtual void processAfterChangeSource();

    virtual cv::Mat saveImplement(QImage &input);

private:
    cv::Mat matInput_;
    cv::Mat matOutput_;
};

#endif // QHISTOGRAMEQUALIZE_HPP
