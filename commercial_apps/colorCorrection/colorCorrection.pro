# Add more folders to ship with the application, here
#folder_01.source = qml/colorCorrection
#folder_01.target = qml
#DEPLOYMENTFOLDERS = folder_01

QT += concurrent core qml quick widgets
qtHaveModule(widgets) {
    QT += widgets
}

CONFIG  += c++11 exceptions

#DEFINES += DEBUG_OK
#DEFINES += PREVIEW

win32{

DEFINES += WIN_OS

INCLUDEPATH += ../../../3rdLibs/openCV/OpenCV-2.4.5/build/include

#for mingw
#LIBS += -L../../../3rdLibs/openCV/OpenCV-2.4.5/builded/bin -lopencv_core245 -lopencv_imgproc245
CV_LIBS_PATH = -L../../../3rdLibs/openCV/OpenCV-2.4.5/builded/bin
CV_LIBS_POST = 245


#for vc series
#LIBS += -L../../3rdLibs/openCV/OpenCV-2.4.5/build/x86/vc11/lib -lopencv_core245 -lopencv_imgproc245
}
mac{

# Bundle identifier for your application
#BUNDLEID = com.yourcompany.MyApp

DEFINES += MAC_OS

RC_FILE = color_management.icns

#INCLUDEPATH += /opt/local/include

#LIBS += -L/opt/local/lib/ -lopencv_core.2.4.6  -lopencv_imgproc.2.4.6

INCLUDEPATH += /usr/local/include

CV_LIBS_PATH = -L/usr/local/lib/
CV_LIBS_POST = .2.4.5

#LIBS += -L/usr/local/lib/ -lopencv_core.2.4.5  -lopencv_imgproc.2.4.5
#LIBS += -L/usr/local/lib/ -lopencv_highgui.2.4.5 -lopencv_photo.2.4.5
}

#LIBS += $$CV_LIBS_PATH -lopencv_calib3d$$CV_LIBS_POST
#LIBS += $$CV_LIBS_PATH -lopencv_contrib$$CV_LIBS_POST
LIBS += $$CV_LIBS_PATH -lopencv_core$$CV_LIBS_POST
#LIBS += $$CV_LIBS_PATH -lopencv_features2d$$CV_LIBS_POST
#LIBS += $$CV_LIBS_PATH -lopencv_flann$$CV_LIBS_POST
#LIBS += $$CV_LIBS_PATH -lopencv_gpu$$CV_LIBS_POST
#LIBS += $$CV_LIBS_PATH -lopencv_highgui$$CV_LIBS_POST
LIBS += $$CV_LIBS_PATH -lopencv_imgproc$$CV_LIBS_POST
#LIBS += $$CV_LIBS_PATH -lopencv_legacy$$CV_LIBS_POST
#LIBS += $$CV_LIBS_PATH -lopencv_ml$$CV_LIBS_POST
#LIBS += $$CV_LIBS_PATH -lopencv_nonfree$$CV_LIBS_POST
#LIBS += $$CV_LIBS_PATH -lopencv_objdetect$$CV_LIBS_POST
#LIBS += $$CV_LIBS_PATH -lopencv_ocl$$CV_LIBS_POST
#LIBS += $$CV_LIBS_PATH -lopencv_photo$$CV_LIBS_POST
#LIBS += $$CV_LIBS_PATH -lopencv_stitching$$CV_LIBS_POST
#LIBS += $$CV_LIBS_PATH -lopencv_superres$$CV_LIBS_POST
#LIBS += $$CV_LIBS_PATH -lopencv_ts$$CV_LIBS_POST
#LIBS += $$CV_LIBS_PATH -lopencv_video$$CV_LIBS_POST
#LIBS += $$CV_LIBS_PATH -lopencv_videostab$$CV_LIBS_POST

INCLUDEPATH += ../../../3rdLibs/QsLog_2.0b1

INCLUDEPATH += ../../commercial_libs/algorithms
INCLUDEPATH += ../../commercial_libs/debugHelper
INCLUDEPATH += ../../commercial_libs/imageAlgorithm
INCLUDEPATH += ../../commercial_libs/OpenCVAndQt
INCLUDEPATH += ../../commercial_libs/QFileAndQString
INCLUDEPATH += ../../commercial_libs/qmlHelper
#INCLUDEPATH += ../../commercial_libs/qmlHelper/fileSupport

# The .cpp file which was generated for your project. Feel free to hack it.
SOURCES += main.cpp \
    ../../commercial_libs/OpenCVAndQt/openCVToQt.cpp \
    ../../commercial_libs/imageAlgorithm/basicImageAlgo.cpp \
    ../../commercial_libs/imageAlgorithm/colorCorrection/piecewiseAffHist/piecewiseAffHist.cpp \
    ../../commercial_libs/imageAlgorithm/colorCorrection/simplestColorBalance/simplestColorBalanceHelper.cpp \
    ../../commercial_libs/imageAlgorithm/colorCorrection/simplestColorBalance/simplestColorBalance.cpp \
    ../../commercial_libs/imageAlgorithm/imageAlgorithm.cpp \
    ../../commercial_libs/QFileAndQString/qstringAlgo.cpp \
    ../../commercial_libs/QFileAndQString/fileAuxiliary.cpp \    
    ../../commercial_libs/imageAlgorithm/colorCorrection/whiteBalance/monitorBGR.cpp \
    ../../commercial_libs/imageAlgorithm/colorCorrection/whiteBalance/grayWorldAssumption.cpp \
    ../../commercial_libs/imageAlgorithm/colorCorrection/whiteBalance/perfectReflectorAssumption.cpp \
    customImage/QWhiteBalanceBGR.cpp \
    customImage/QSimplestColorBalance.cpp \
    customImage/QPiecewiseAffHist.cpp \
    customImage/QPerfectReflectAssumption.cpp \
    customImage/QHSVCorrection.cpp \
    customImage/QHistogramEqualize.cpp \
    customImage/QGrayWorldAssumption.cpp \
    customImage/QGammaCorrection.cpp \
    customImage/QCLAHE.cpp \
    customImage/customImage.cpp \
    ../../commercial_libs/imageAlgorithm/colorCorrection/whiteBalance/otsuWhiteBalance.cpp \
    ../../commercial_libs/imageAlgorithm/colorCorrection/whiteBalance/chromaticyHistWB.cpp \
    customImage/QChromaticityHistWB.cpp \
    ../../commercial_libs/OpenCVAndQt/qImageHelper.cpp \
    ../../../3rdLibs/QsLog_2.0b1/QsLogDest.cpp \
    ../../../3rdLibs/QsLog_2.0b1/QsLogDestFile.cpp \
    ../../../3rdLibs/QsLog_2.0b1/QsLogDestConsole.cpp \
    ../../../3rdLibs/QsLog_2.0b1/QsLog.cpp \
    ../../commercial_libs/imageAlgorithm/histogram/histogramEqualize.cpp \
    ../../commercial_libs/imageAlgorithm/histogram/calcHistogram.cpp \
    ../../commercial_libs/imageAlgorithm/genericAlgo/genericRegion.cpp \
    ../../commercial_libs/imageAlgorithm/genericAlgo/genericTransform.cpp \
    ../../commercial_libs/qmlHelper/file/fileProcess.cpp \
    ../../commercial_libs/qmlHelper/file/fileProcessConstant.cpp

# Please do not modify the following two lines. Required for deployment.
include(qtquick2applicationviewer/qtquick2applicationviewer.pri)
qtcAddDeployment()

HEADERS += \
    ../../commercial_libs/OpenCVAndQt/openCVToQt.hpp \
    ../../commercial_libs/imageAlgorithm/basicImageAlgo.hpp \
    ../../commercial_libs/imageAlgorithm/colorCorrection/colorSpaceEnum.hpp \
    ../../commercial_libs/imageAlgorithm/colorCorrection/piecewiseAffHist/piecewiseAffHist.hpp \
    ../../commercial_libs/imageAlgorithm/colorCorrection/simplestColorBalance/simplestColorBalanceHelper.hpp \
    ../../commercial_libs/imageAlgorithm/colorCorrection/simplestColorBalance/simplestColorBalance.hpp \
    ../../commercial_libs/algorithms/metaProgrammingHelper.hpp \
    ../../commercial_libs/imageAlgorithm/imageAlgorithm.hpp \
    ../../commercial_libs/QFileAndQString/qstringAlgo.hpp \
    ../../commercial_libs/QFileAndQString/fileAuxiliary.hpp \   
    ../../commercial_libs/debugHelper/debugHelper.hpp \
    ../../commercial_libs/imageAlgorithm/colorCorrection/whiteBalance/monitorBGR.hpp \
    ../../commercial_libs/imageAlgorithm/colorCorrection/whiteBalance/grayWorldAssumption.hpp \
    ../../commercial_libs/imageAlgorithm/colorCorrection/whiteBalance/perfectReflectorAssumption.hpp \
    ../../commercial_libs/imageAlgorithm/basicImageAlgoFunctor.hpp \
    customImage/QWhiteBalanceBGR.hpp \
    customImage/QSimplestColorBalance.hpp \
    customImage/QPiecewiseAffHist.hpp \
    customImage/QPerfectReflectAssumption.hpp \
    customImage/QHSVCorrection.hpp \
    customImage/QHistogramEqualize.hpp \
    customImage/QGrayWorldAssumption.hpp \
    customImage/QGammaCorrection.hpp \
    customImage/QCLAHE.hpp \
    customImage/customImage.hpp \
    ../../commercial_libs/imageAlgorithm/colorCorrection/whiteBalance/otsuWhiteBalance.hpp \
    ../../commercial_libs/imageAlgorithm/colorCorrection/whiteBalance/chromaticyHistWB.hpp \
    customImage/QChromaticityHistWB.hpp \
    ../../commercial_libs/imageAlgorithm/utility/getDataHelper.hpp \
    ../../commercial_libs/OpenCVAndQt/qImageHelper.hpp \
    ../../../3rdLibs/QsLog_2.0b1/QsLogLevel.h \
    ../../../3rdLibs/QsLog_2.0b1/QsLogDisableForThisFile.h \
    ../../../3rdLibs/QsLog_2.0b1/QsLogDestFile.h \
    ../../../3rdLibs/QsLog_2.0b1/QsLogDestConsole.h \
    ../../../3rdLibs/QsLog_2.0b1/QsLogDest.h \
    ../../../3rdLibs/QsLog_2.0b1/QsLog.h \
    ../../commercial_libs/imageAlgorithm/histogram/histogramEqualize.hpp \
    ../../commercial_libs/imageAlgorithm/colorFlag.hpp \
    ../../commercial_libs/imageAlgorithm/histogram/calcHistogram.hpp \
    ../../commercial_libs/imageAlgorithm/genericAlgo/genericForEach.hpp \
    ../../commercial_libs/imageAlgorithm/genericAlgo/genericRegion.hpp \
    ../../commercial_libs/imageAlgorithm/genericAlgo/genericProcess.hpp \
    ../../commercial_libs/imageAlgorithm/genericAlgo/genericTransform.hpp \
    ../../commercial_libs/qmlHelper/file/fileProcessConstant.hpp \
    ../../commercial_libs/qmlHelper/file/fileProcess.hpp
    ../../commercial_libs/algorithms/stlAlgoWrapper.hpp

RESOURCES += \
    icons.qrc \
    qml.qrc \
    text.qrc

OTHER_FILES += \
    ../../commercial_libs/imageAlgorithm/opengl/colorCorrection/piecewiseAffHist.fsh \
    ../../../3rdLibs/QsLog_2.0b1/QsLog.pri \
    ../../../3rdLibs/QsLog_2.0b1/QsLogReadme.txt \
    ../../../3rdLibs/QsLog_2.0b1/QsLogChanges.txt \
    qml/colorCorrection/mainDesktop.qml \
    qml/colorCorrection/desktop/WindowConfigure.qml \
    qml/colorCorrection/js/style.js \
    qml/colorCorrection/shaders/piecewiseAffHistFragment.fsh \
    qml/colorCorrection/utility/Help.qml \
    qml/colorCorrection/utility/algoBar/AlgoBarSimpleCBUI.qml \
    qml/colorCorrection/utility/algoBar/AlgoBarPerfectReflect.qml \
    qml/colorCorrection/utility/algoBar/AlgoBarPAHUI.qml \
    qml/colorCorrection/utility/algoBar/AlgoBarMonitorRGBUI.qml \
    qml/colorCorrection/utility/algoBar/AlgoBarHSVUI.qml \
    qml/colorCorrection/utility/algoBar/AlgoBarGlobalHE.qml \
    qml/colorCorrection/utility/algoBar/AlgoBarGammaCorrectUI.qml \
    qml/colorCorrection/utility/algoBar/AlgoBarCLAHE.qml \
    qml/colorCorrection/utility/algoBar/AlgoBar.qml \
    qml/colorCorrection/utility/imageViewer/ThumbsImageView.qml \
    qml/colorCorrection/utility/imageViewer/multiSelectTableView.qml \
    qml/colorCorrection/utility/imageViewer/ImageViewer.qml \
    qml/colorCorrection/utility/imageViewer/ImagesNamesView.qml \
    qml/colorCorrection/utility/algoBar/AlgoBarDRWB.qml \
    qml/colorCorrection/utility/RubberBand.qml \
    qml/colorCorrection/utility/ScrollAbleImage.qml
