import QtQuick 2.1
import QtQuick.Controls 1.0

Rectangle {
    id: root

    color: "black"

    property int fillMode: Image.PreserveAspectFit
    property bool enableRubberBand: false
    property bool scrollAble: true //when the fillMode is Pad and the scrollAble is true, the Image will wrap by ScrollView;
    //else it will wrapped by Flickable
    property string source    

    signal getRubberBandArea(var x, var y, var width, var height) //emit when change source or release rubberBand

    function getRubberBandX(){
        return rubberBand.getX()
    }

    function getRubberBandY(){
        return rubberBand.getY()
    }

    function getRubberBandWidth(){
        return rubberBand.getWidth()
    }

    function getRubberBandHeight(){
        return rubberBand.getHeight()
    }

    onFillModeChanged: {
        if(root.fillMode != Image.Pad){
            root.enableRubberBand = false
        }
    }

    onSourceChanged: {
        object.adjustRubberBandPostion()
        getRubberBandArea(rubberBand.getX(), rubberBand.getY(), rubberBand.getWidth(), rubberBand.getHeight())
    }

    QtObject{
        id: object

        function adjustRubberBandPostion()
        {
            rubberBand.setToInitialSize()
            rubberBand.x = 25
            rubberBand.y = 25            
        }
    }

    Image{
        id: imageLoading

        anchors.centerIn: root

        source: "../images/emotion_gear.png"
        visible: imageNormal.status == Image.Loading || imageFlickAble.status == Image.Loading ||
                 imageScrollAble.status == Image.Loading

        NumberAnimation on rotation { loops: Animation.Infinite; from: 0; to: 360; duration: 1000;
            running: imageLoading.visible}
    }

    Image{
        id: imageNormal
        anchors.fill: parent
        asynchronous: true
        cache: false
        fillMode: root.fillMode
        smooth: true
        source: imageNormal.visible ? root.source : ""
        enabled: imageNormal.visible
        visible: root.fillMode != Image.Pad
        z: 1                
    }

    Flickable{
        anchors.fill: parent
        //boundsBehavior: Flickable.DragOverBounds
        height: imageFlickAble.height
        width: imageFlickAble.width
        clip: true
        contentHeight: imageFlickAble.height
        contentWidth: imageFlickAble.width
        //visible: root.fillMode == Image.Pad && !root.scrollAble
        z: 1
        Image{
            id: imageFlickAble
            asynchronous: true
            fillMode: root.fillMode
            source: imageFlickAble.visible ? root.source : ""
            enabled: imageFlickAble.visible
            visible: root.fillMode == Image.Pad && !root.scrollAble
        }        
    }

    ScrollView{
        id: scrollView

        height: root.height
        width: root.width
        opacity: root.fillMode == Image.Pad && root.scrollAble ? 1 : 0
        z: 1
        Item{
            id: scrollAbleContainer
            height: Math.max(imageScrollAble.implicitHeight, root.height - 20)
            width: Math.max(imageScrollAble.implicitWidth, root.width - 20)
            Image{
                id: imageScrollAble
                anchors.centerIn: parent                
                asynchronous: true
                enabled: parent.opacity == 1
                source: imageScrollAble.visible ? root.source : ""

                RubberBand{
                    id: rubberBand

                    enabled: root.enableRubberBand
                    visible: root.enableRubberBand

                    z: 2

                    onRubberBandRelease: {
                        //console.log("X, Y, width, height = " + rubberBand.getX() + ", " + rubberBand.getY() + ", "
                       //             + rubberBand.getWidth() + ", " + rubberBand.getHeight())

                        getRubberBandArea(rubberBand.getX(), rubberBand.getY(), rubberBand.getWidth(), rubberBand.getHeight())
                    }

                    /*onXChanged: {
                        console.log("X change = " + rubberBand.x)
                    }
                    onYChanged: {
                        console.log("Y change = " + rubberBand.y)
                    }*/
                }
            }            
        }       
    }   
}
