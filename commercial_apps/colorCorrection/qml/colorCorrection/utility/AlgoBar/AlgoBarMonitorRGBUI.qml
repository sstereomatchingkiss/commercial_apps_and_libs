import QtQuick 2.1
import QtQuick.Controls 1.0
import QtQuick.Layouts 1.0

ColumnLayout{

    QtObject{
        id: object

        property int duration: 300
    }

    GroupBox{
        id: pahParamBox

        title: "Parameters"
        Layout.fillWidth: true

        ColumnLayout{
            //anchors.fill: parent

            Button{
                text: qsTr("Default")
                Layout.fillWidth: true

                onClicked: {
                    monitorRSlider.value = 0
                    monitorGSlider.value = 0
                    monitorBSlider.value = 0
                    monitorIntensitySlider.value = 0
                }
            }

            GroupBox{
                id: boxR

                title: "R"
                Layout.fillWidth: true                               

                enabled: !loaderPreview.item.isOneChannel
                opacity: !loaderPreview.item.isOneChannel ? 1.0 : 0.5

                RowLayout{
                    Slider{
                        id: monitorRSlider                                               

                        maximumValue: 255
                        onValueChanged: {
                            loaderPreview.item.valueR = value
                            if(monitorRSpinBox){
                                if(monitorRSpinBox.value != value){
                                    monitorRSpinBox.value = value
                                }
                            }
                        }
                    }

                    SpinBox {
                        id: monitorRSpinBox                                               

                        maximumValue: monitorRSlider.maximumValue
                        onValueChanged: {
                            if(monitorRSlider){
                                if(monitorRSlider.value != value){
                                    monitorRSlider.value = value
                                }
                            }
                        }
                    }
                }

                Behavior on opacity { NumberAnimation{duration: object.duration} }
            }

            GroupBox{
                title: "G"
                Layout.fillWidth: true

                enabled: boxR.enabled
                opacity: boxR.opacity

                RowLayout{
                    Slider{
                        id: monitorGSlider

                        maximumValue: 255
                        stepSize: 1.0
                        onValueChanged: {
                            loaderPreview.item.valueG = value
                            if(monitorGSpinBox){
                                if(monitorGSpinBox.value != value){
                                    monitorGSpinBox.value = value
                                }
                            }
                        }
                    }

                    SpinBox {
                        id: monitorGSpinBox

                        maximumValue: monitorGSlider.maximumValue
                        onValueChanged: {
                            if(monitorGSlider){
                                if(monitorGSlider.value != value){
                                    monitorGSlider.value = value
                                }
                            }
                        }
                    }
                }

                Behavior on opacity { NumberAnimation{duration: object.duration} }
            }

            GroupBox{
                title: "B"
                Layout.fillWidth: true

                enabled: boxR.enabled
                opacity: boxR.opacity

                RowLayout{
                    Slider{
                        id: monitorBSlider                                              

                        maximumValue: 255
                        stepSize: 1.0
                        onValueChanged: {
                            loaderPreview.item.valueB = value

                            if(monitorBSpinBox){
                                if(monitorBSpinBox.value != value){
                                    monitorBSpinBox.value = value
                                }
                            }
                        }
                    }

                    SpinBox {
                        id: monitorBSpinBox                                              

                        maximumValue: monitorBSlider.maximumValue
                        onValueChanged: {
                            if(monitorBSlider){
                                if(monitorBSlider.value != value){
                                    monitorBSlider.value = value
                                }
                            }
                        }
                    }
                }

                Behavior on opacity { NumberAnimation{duration: object.duration} }
            }

            GroupBox{
                title: "Intensity"
                Layout.fillWidth: true

                enabled: loaderPreview.item.isOneChannel
                opacity: loaderPreview.item.isOneChannel ? 1.0 : 0.3

                RowLayout{
                    Slider{
                        id: monitorIntensitySlider

                        maximumValue: 255
                        stepSize: 1.0
                        onValueChanged: {
                            loaderPreview.item.intensity = value

                            if(monitorIntensitySpinBox){
                                if(monitorIntensitySpinBox.value != value){
                                    monitorIntensitySpinBox.value = value
                                }
                            }
                        }
                    }

                    SpinBox {
                        id: monitorIntensitySpinBox

                        maximumValue: monitorIntensitySlider.maximumValue
                        onValueChanged: {
                            if(monitorIntensitySlider){
                                if(monitorIntensitySlider.value != value){
                                    monitorIntensitySlider.value = value
                                }
                            }
                        }
                    }
                }

                Behavior on opacity { NumberAnimation{duration: object.duration} }
            }
        }
    }
}
