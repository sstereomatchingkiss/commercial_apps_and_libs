import QtQuick 2.1
import QtQuick.Controls 1.0
import QtQuick.Layouts 1.0

//ui of simplest color space
Column{    

    GroupBox{
        anchors.horizontalCenter: parent.horizontalCenter
        title: "Color space"

        RowLayout{
            //anchors.fill: parent

            ComboBox{
                id: boxGammaColorSpace

                model: modelGammaSpace
                Layout.fillWidth: true

                onCurrentTextChanged: {
                    object.changeColorBase(boxGammaColorSpace.currentText)
                }
            }
        }
    }

    GroupBox{
        id: gammaParamBox

        title: "Parameters"
        //Layout.fillWidth: true

        ColumnLayout{
            //anchors.fill: parent

            Button{
                text: "Default"
                Layout.fillWidth: true

                onClicked: {
                    gammaCorrectSlider.value = 100
                }
            }

            GroupBox{
                title: "Gamma"
                //Layout.fillWidth: true

                RowLayout{
                    Slider{
                        id: gammaCorrectSlider

                        maximumValue: 699
                        stepSize: 1.0
                        value: 100
                        onValueChanged: {
                            loaderPreview.item.gamma = value / 100.0
                            if(gammaCorrectSpinBox){
                                if(gammaCorrectSpinBox.value != value){
                                    gammaCorrectSpinBox.value = value
                                }
                            }
                        }
                    }

                    SpinBox{
                        id: gammaCorrectSpinBox

                        maximumValue: gammaCorrectSlider.maximumValue
                        stepSize: gammaCorrectSlider.stepSize
                        value: 100

                        onValueChanged: {
                            if(gammaCorrectSlider){
                                if(gammaCorrectSlider.value != value){
                                    gammaCorrectSlider.value = value
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
