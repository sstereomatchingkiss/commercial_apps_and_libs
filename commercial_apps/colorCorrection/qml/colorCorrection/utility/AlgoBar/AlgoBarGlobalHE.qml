import QtQuick 2.1
import QtQuick.Controls 1.0
import QtQuick.Layouts 1.0

Column{

    GroupBox{
        title: "Color space"
        anchors.horizontalCenter: parent.horizontalCenter

        RowLayout{
            //anchors.fill: parent

            ComboBox{
                id: boxHistColorSpace

                currentIndex: 0
                model: modelHistColorSpace
                Layout.fillWidth: true

                onCurrentTextChanged: {
                    object.changeColorBase(boxHistColorSpace.currentText)
                }
            }
        }
    }
}
