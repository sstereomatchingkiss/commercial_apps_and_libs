import QtQuick 2.1
import QtQuick.Controls 1.0
import QtQuick.Layouts 1.0

//ui of simplest color space
Column{

    GroupBox{
        anchors.horizontalCenter: parent.horizontalCenter
        title: qsTr("Color space")

        RowLayout{
            //anchors.fill: parent

            ComboBox{
                id: scbColorSpaceBox

                model: modelScbColorSpace
                Layout.fillWidth: true

                onCurrentTextChanged: {
                    object.changeColorBase(scbColorSpaceBox.currentText)
                }
            }
        }
    }

    GroupBox{
        anchors.horizontalCenter: parent.horizontalCenter
        title: qsTr("Presets")

        RowLayout{
            //anchors.fill: parent
            //Layout.fillWidth: true

            ComboBox{
                id: scbPreset

                model: modelPreset
                //Layout.fillWidth: true

                onCurrentTextChanged: {
                    if(scbPreset.currentText == "Custom"){
                        scbParamBox.enabled = true
                    }else{
                        scbParamBox.enabled = false
                    }

                    if(scbPreset.currentText == "Default"){
                        maxSCBPercentileSlider.value = 0
                        minSCBPercentileSlider.value = 0
                    }else if(scbPreset.currentText == "Weak"){
                        maxSCBPercentileSlider.value = 1
                        minSCBPercentileSlider.value = 0
                    }else if(scbPreset.currentText == "Moderate"){
                        maxSCBPercentileSlider.value = 5
                        minSCBPercentileSlider.value = 0
                    }else if(scbPreset.currentText == "Strong"){
                        maxSCBPercentileSlider.value = 10
                        minSCBPercentileSlider.value = 0
                    }
                }
            }
        }
    }

    GroupBox{
        id: scbParamBox

        title: qsTr("Parameters")
        //Layout.fillWidth: true

        ColumnLayout{
            //anchors.fill: parent

            GroupBox{
                title: "Minimum percentile"
                //Layout.fillWidth: true

                RowLayout{
                    Slider{
                        id: minSCBPercentileSlider

                        maximumValue: 99                       
                        onValueChanged: {
                            loaderPreview.item.minPercentile = value
                            if(minSCBPercentileSpinBox){
                                if(minSCBPercentileSpinBox.value != value){
                                    minSCBPercentileSpinBox.value = value
                                }
                            }
                        }
                    }

                    SpinBox {
                        id: minSCBPercentileSpinBox

                        maximumValue: minSCBPercentileSlider.maximumValue
                        onValueChanged: {
                            if(minSCBPercentileSlider){
                                if(minSCBPercentileSlider.value != value){
                                    minSCBPercentileSlider.value = value
                                }
                            }
                        }
                    }
                }
            }

            GroupBox{
                title: "Maximum percentile"
                //Layout.fillWidth: true

                RowLayout{
                    Slider{
                        id: maxSCBPercentileSlider

                        maximumValue: 99                       
                        onValueChanged: {
                            loaderPreview.item.maxPercentile = value
                            if(maxSCBPercentileSpinBox){
                                if(maxSCBPercentileSpinBox.value != value){
                                    maxSCBPercentileSpinBox.value = value
                                }
                            }
                        }
                    }

                    SpinBox {
                        id: maxSCBPercentileSpinBox

                        maximumValue: maxSCBPercentileSlider.maximumValue
                        onValueChanged: {
                            if(maxSCBPercentileSlider){
                                if(maxSCBPercentileSlider.value != value){
                                    maxSCBPercentileSlider.value = value
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
