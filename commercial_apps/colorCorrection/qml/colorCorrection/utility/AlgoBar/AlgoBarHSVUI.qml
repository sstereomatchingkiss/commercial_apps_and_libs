import QtQuick 2.1
import QtQuick.Controls 1.0
import QtQuick.Layouts 1.0

//gui of hsv correction
ColumnLayout{

    GroupBox{
        title: "Parameters"
        Layout.fillWidth: true

        ColumnLayout{
           // anchors.fill: parent

            Button{
                text: qsTr("Default")
                Layout.fillWidth: true

                onClicked: {
                    valueHSlider.value = 255;
                    valueSSlider.value = 255;
                    valueVSlider.value = 255;
                }
            }

            GroupBox{
                title: "H"
                Layout.fillWidth: true

                RowLayout{
                    Slider{
                        id: valueHSlider

                        maximumValue: 511
                        stepSize: 1.0
                        value: 255
                        onValueChanged: {
                            loaderPreview.item.valueH = value - 255
                            if(valueHSpinBox){
                                if(valueHSpinBox.value != value){
                                    valueHSpinBox.value = value
                                }
                            }
                        }
                    }

                    SpinBox {
                        id: valueHSpinBox

                        maximumValue: valueHSlider.maximumValue
                        value: 255
                        onValueChanged: {
                            if(valueHSlider){
                                if(valueHSlider.value != value){
                                    valueHSlider.value = value
                                }
                            }
                        }
                    }
                }
            }

            GroupBox{
                title: "S"
                Layout.fillWidth: true

                RowLayout{
                    Slider{
                        id: valueSSlider

                        maximumValue: 511
                        stepSize: 1.0
                        value: 255
                        onValueChanged: {
                            loaderPreview.item.valueS = value - 255
                            if(valueSSpinBox){
                                if(valueSSpinBox.value != value){
                                    valueSSpinBox.value = value
                                }
                            }
                        }
                    }

                    SpinBox {
                        id: valueSSpinBox

                        maximumValue: valueSSlider.maximumValue
                        stepSize: valueSSlider.stepSize
                        value: 255
                        onValueChanged: {
                            if(valueSSlider){
                                if(valueSSlider.value != value){
                                    valueSSlider.value = value
                                }
                            }
                        }
                    }
                }
            }

            GroupBox{
                title: "V"
                Layout.fillWidth: true

                RowLayout{
                    Slider{
                        id: valueVSlider

                        maximumValue: 511
                        stepSize: 1.0
                        value: 255
                        onValueChanged: {
                            loaderPreview.item.valueV = value - 255
                            if(valueVSpinBox != null && valueVSpinBox.value != value){
                                valueVSpinBox.value = value
                            }
                        }
                    }

                    SpinBox {
                        id: valueVSpinBox

                        maximumValue: valueVSlider.maximumValue
                        stepSize: valueVSlider.stepSize
                        value: 255
                        onValueChanged: {
                            if(valueVSlider.value != value){
                                valueVSlider.value = value
                            }
                        }
                    }
                }
            }            
        }
    }
}
