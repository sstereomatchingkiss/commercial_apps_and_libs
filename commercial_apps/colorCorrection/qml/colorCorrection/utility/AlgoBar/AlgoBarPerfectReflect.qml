import QtQuick 2.1
import QtQuick.Controls 1.0
import QtQuick.Layouts 1.0

//ui of simplest color space
ColumnLayout{

    GroupBox{
        id: perfectReflectParamBox

        title: "Parameters"
        Layout.fillWidth: true

        ColumnLayout{
            //anchors.fill: parent

            Button{
                text: qsTr("Default")
                Layout.fillWidth: true

                onClicked: {
                    perfectReflectSlider.value = 0
                }
            }

            GroupBox{
                title: qsTr("Ratio")
                Layout.fillWidth: true

                RowLayout{
                    Slider{
                        id: perfectReflectSlider

                        maximumValue: 100
                        stepSize: 1.0
                        onValueChanged: {
                            loaderPreview.item.ratio = value
                            if(perfectReflectSpinBox){
                                if(perfectReflectSpinBox.value != value){
                                    perfectReflectSpinBox.value = value
                                }
                            }
                        }
                    }

                    SpinBox {
                        id: perfectReflectSpinBox

                        maximumValue: perfectReflectSlider.maximumValue
                        stepSize: perfectReflectSlider.stepSize
                        onValueChanged: {
                            if(perfectReflectSlider){
                                if(perfectReflectSlider.value != value){
                                    perfectReflectSlider.value = value
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
