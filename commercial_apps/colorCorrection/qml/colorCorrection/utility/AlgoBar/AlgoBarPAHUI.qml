import QtQuick 2.1
import QtQuick.Controls 1.0
import QtQuick.Layouts 1.0

Column{

    GroupBox{
        anchors.horizontalCenter: parent.horizontalCenter
        title: "Color space"        

        RowLayout{
            //anchors.fill: parent

            ComboBox{
                id: pahColorSpaceBox

                model: modelPahColorSpace
                Layout.fillWidth: true

                onCurrentTextChanged: {
                    object.changeColorBase(pahColorSpaceBox.currentText)
                }
            }
        }
    }

    GroupBox{
        title: qsTr("Presets")
        anchors.horizontalCenter: parent.horizontalCenter

        RowLayout{
            //anchors.fill: parent


            ComboBox{
                id: pahPreset

                model: modelPreset
                Layout.fillWidth: true

                onCurrentTextChanged: {
                    if(pahPreset.currentText == "Custom"){
                        pahParamBox.enabled = true
                    }else{
                        pahParamBox.enabled = false
                    }

                    if(pahPreset.currentText == "Default"){
                        pahIntervalSlider.value = 0
                        maxPAHSlopeSlider.value = 0
                        minPAHSlopeSlider.value = 0
                    }else if(pahPreset.currentText == "Weak"){
                        pahIntervalSlider.value = 5
                        maxPAHSlopeSlider.value = 10
                        minPAHSlopeSlider.value = 0
                    }else if(pahPreset.currentText == "Moderate"){
                        pahIntervalSlider.value = 5
                        maxPAHSlopeSlider.value = 30
                        minPAHSlopeSlider.value = 0
                    }else if(pahPreset.currentText == "Strong"){
                        pahIntervalSlider.value = 10
                        maxPAHSlopeSlider.value = 30
                        minPAHSlopeSlider.value = 20
                    }
                }
            }
        }
    }

    GroupBox{
        id: pahParamBox

        title: qsTr("Parameters")
        Layout.fillWidth: true

        ColumnLayout{
            //anchors.fill: parent

            GroupBox{
                title: "Interval"
                Layout.fillWidth: true

                RowLayout{
                    Slider{
                        id: pahIntervalSlider

                        maximumValue: 100
                        onValueChanged: {                            
                            loaderPreview.item.interval = value / 10
                            if(pahIntervalSpinBox){
                                if(pahIntervalSpinBox.value != value){
                                    pahIntervalSpinBox.value = value
                                }
                            }
                        }
                    }

                    SpinBox {
                        id: pahIntervalSpinBox

                        maximumValue: pahIntervalSlider.maximumValue
                        onValueChanged: {
                            if(pahIntervalSlider){
                                if(pahIntervalSlider.value != value){
                                    pahIntervalSlider.value = value
                                }
                            }
                        }
                    }
                }
            }

            GroupBox{
                title: "Minimum slope"
                Layout.fillWidth: true

                RowLayout{
                    Slider{
                        id: minPAHSlopeSlider

                        maximumValue: maxPAHSlopeSlider.value
                        stepSize: 1.0
                        onValueChanged: {                            
                            loaderPreview.item.minSlope = value / 10
                            if(minPAHSlopeSpinBox){
                                if(minPAHSlopeSpinBox.value != value){
                                    minPAHSlopeSpinBox.value = value
                                }
                            }
                        }
                    }

                    SpinBox {
                        id: minPAHSlopeSpinBox

                        maximumValue: minPAHSlopeSlider.maximumValue
                        onValueChanged: {
                            if(minPAHSlopeSlider){
                                if(minPAHSlopeSlider.value != value){
                                    minPAHSlopeSlider.value = value
                                }
                            }
                        }
                    }
                }
            }

            GroupBox{
                title: "Maximum slope"
                Layout.fillWidth: true

                RowLayout{
                    Slider{
                        id: maxPAHSlopeSlider

                        maximumValue: 99
                        stepSize: 1.0
                        onValueChanged: {
                            loaderPreview.item.maxSlope = value / 10
                            if(maxPAHSlopeSpinBox){
                                if(maxPAHSlopeSpinBox.value != value){
                                    maxPAHSlopeSpinBox.value = value
                                }
                            }
                        }
                    }

                    SpinBox {
                        id: maxPAHSlopeSpinBox

                        maximumValue: maxPAHSlopeSlider.maximumValue
                        onValueChanged: {
                            if(maxPAHSlopeSlider){
                                if(maxPAHSlopeSlider.value != value){
                                    maxPAHSlopeSlider.value = value
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
