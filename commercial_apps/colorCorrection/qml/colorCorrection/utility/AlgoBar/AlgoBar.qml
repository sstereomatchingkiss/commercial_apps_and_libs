import QtQuick 2.1
import QtQuick.Controls 1.0
import QtQuick.Layouts 1.0

import ColorAlgo 1.0

import "../../js/style.js" as Style

/**
 * Collection of UI to interact with different algorithms
 */
Rectangle {
    id: root

    width: 100
    height: 62

    state: ""   

    property bool enableRubberBand: false

    property bool processManyImage: false //true mean there are many images to processe;false mean there are one image to process

    property string removeDirPath //directory will be deleted by the FileProcess when the destructor called

    //this name will be used if the users do not specify file name
    property string source //the image being selected

    signal processBegin() //emit when the image processing jobs begin
    signal processExit() //emit when the image processing jobs exit   

    signal setSelectedAreaSignal() //emit when changing algorithm(preview image)

    onEnableRubberBandChanged: {
        loaderPreview.item.rubberBand = root.enableRubberBand
    }

    function getErrorLog(){
        return object.errorLog
    }

    function saveImage(path, imageType, series){
        if(loaderPreview.status == Loader.Ready){
            return loaderPreview.item.saveImage(path, imageType, series)
        }

        return false
    }

    function saveImages(path, names, imageType, series){
        if(loaderPreview.status == Loader.Ready){
            loaderPreview.item.saveImages(path, names, imageType, series)
        }
    }

    function setSelectedArea(x, y, width, height){
        loaderPreview.item.setSelectedArea(x, y, width, height);
    }

    QtObject{
        id: object

        property bool busy: false
        property string errorLog : loaderPreviewIsReady ? loaderPreview.item.errorLog : ""
        property bool loaderPreviewIsReady: loaderPreview.status == Loader.Ready        

        /*onErrorLogChanged: {
            console.log("errorLog change : " + object.errorLog)
        }*/

        //due to some weird bug, the value of the progressBar won't bind to loaderPreview.item.progressValue
        //that is why I choose this awkward work around
        property int progressValue: object.loaderPreviewIsReady ? loaderPreview.item.progressValue : 0

        onProgressValueChanged: {
            progressBar.value = object.progressValue
        }        

        function changeColorBase(text) {
            if(text == "RGB"){
                loaderPreview.item.colorSpace = PiecewiseAffHist.BGR
            }else if(text == "Intensity"){
                loaderPreview.item.colorSpace = PiecewiseAffHist.Intensity
            }else if(text == "HLS"){
                loaderPreview.item.colorSpace = PiecewiseAffHist.HLS
            }else if(text == "Lab"){
                loaderPreview.item.colorSpace = PiecewiseAffHist.Lab
            }else if(text == "YCrCb"){
                loaderPreview.item.colorSpace = PiecewiseAffHist.HLS
            }else if(text == "YUV"){
                loaderPreview.item.colorSpace = PiecewiseAffHist.YUV
            }
        }

        function processExit(){
            loaderUI.enabled = true
            loaderUI.opacity = 1.0
            object.busy = false
            root.processExit()
        }
    }

    Component{
        id: compChromaWBPreview

        ChromaticityWhiteBalance{
            id: chromaWBPreview

            source: root.source

            onProgressExitChanged: {
                if(progressExit){
                    object.processExit()
                }
            }
        }
    }

    Component{
        id: compCLAHEPreview

        CLAHE{
            id: clahePreview

            source: root.source

            onProgressExitChanged: {
                if(progressExit){
                    object.processExit()
                }
            }
        }
    }    

    Component{
        id: compGammaCorrectPreview
        GammaCorrection{
            id: gammaCorrectPreview

            source: root.source

            onProgressExitChanged: {
                if(progressExit){
                    object.processExit()
                }
            }
        }
    }

    Component{
        id: compGrayWorldPreview
        GrayWorldAssumption{
            id: grayWorldPreview

            source: root.source

            onProgressExitChanged: {
                if(progressExit){
                    object.processExit()
                }
            }
        }
    }

    Component{
        id: compHisEqualPreview
        HistogramEqualize{
            id: hisEqualPreview

            source: root.source

            onProgressExitChanged: {
                if(progressExit){
                    object.processExit()
                }
            }
        }
    }

    Component{
        id: compHsvCorrectionPreview
        HSVCorrection{
            id: hsvCorrectionPreview

            source: root.source

            onProgressExitChanged: {
                if(progressExit){
                    object.processExit()
                }
            }
        }
    }

    Component{
        id: compMonitorRGBPreview
        MonitorRGB{
            id: monitorRGBPreview

            source: root.source

            onProgressExitChanged: {
                if(progressExit){
                    object.processExit()
                }
            }
        }
    }

    Component{
        id: compPahPreview
        PiecewiseAffHist{
            id: pahPreview

            source: root.source

            onProgressExitChanged: {
                if(progressExit){
                    object.processExit()
                }
            }
        }
    }

    Component{
        id: compPerfectReflectPreview
        PerfectReflect{
            id: perfectReflectPreview

            source: root.source

            onProgressExitChanged: {
                if(progressExit){
                    object.processExit()
                }
            }
        }
    }

    Component{
        id: compScbPreview
        SimplestColorBalance{
            id: scbPreview

            source: root.source

            onProgressExitChanged: {
                if(progressExit){
                    object.processExit()
                }
            }
        }
    }

    Column{
        id: mainLayout

        anchors.fill: parent
        spacing: 10

        Loader{
            id: loaderUI

            enabled: !object.busy
            opacity: !object.busy ? 1.0 : 0.3

            anchors.horizontalCenter: parent.horizontalCenter
            asynchronous: false
            sourceComponent: null
        }

        GroupBox{
            title: "Preview"

            enabled: !object.busy
            opacity: !object.busy ? 1.0 : 0.3

            anchors.horizontalCenter: parent.horizontalCenter
            height: 220
            width: 340

            Loader{
                id: loaderPreview

                //anchors.fill: parent
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                //Layout.fillWidth: true
                asynchronous: false
                sourceComponent: null

                onStatusChanged: {
                    if(loaderPreview.status == Loader.Ready){
                        loaderPreview.item.imageHeight = Style.previewHeight
                        loaderPreview.item.imageWidth = Style.previewWidth
                        loaderPreview.item.height = Style.previewHeight
                        loaderPreview.item.width = Style.previewWidth
                        loaderPreview.item.rubberBand = root.enableRubberBand
                        setSelectedAreaSignal()

                        loaderPreview.item.previewDir = root.removeDirPath
                    }
                }
            }
        }

        Button{
            id: buttonProcessAndSave

            enabled: !object.busy
            opacity: !object.busy ? 1.0 : 0.3

            anchors.horizontalCenter: parent.horizontalCenter
            width: parent.width - 20

            text: qsTr("Process image and save")
            tooltip: qsTr("Process and save the image")

            onClicked: {               
                loaderUI.enabled = false
                loaderUI.opacity = 0.3
                object.busy = true
                processBegin()
            }
        }


        ProgressBar{
            id: progressBar

            anchors.horizontalCenter: parent.horizontalCenter
            width: parent.width - 20
            visible: processManyImage

            maximumValue: object.loaderPreviewIsReady ? loaderPreview.item.progressMaximum : 1            
        }
    }


    ListModel{
        id: modelCLAHEColorSpace

        ListElement { text: "Lab" }
        ListElement { text: "RGB"}
        ListElement { text: "YCrCb"}
    }

    ListModel {
        id: modelGammaSpace

        ListElement { text: "Lab" }
        ListElement { text: "HLS"}
        ListElement { text: "RGB" }
        ListElement { text: "YCrCb"}
        ListElement { text: "YUV"}
    }

    ListModel {
        id: modelHistColorSpace

        ListElement { text: "RGB" }
        ListElement { text: "YCrCb" }
    }

    ListModel {
        id: modelPreset

        ListElement{ text: "Default"}
        ListElement{ text: "Weak"}
        ListElement{ text: "Moderate"}
        ListElement{ text: "Strong"}
        ListElement{ text: "Custom"}
    }

    ListModel {
        id: modelPahColorSpace

        ListElement { text: "Intensity" }
        ListElement { text: "RGB" }
    }

    ListModel {
        id: modelScbColorSpace

        ListElement { text: "HLS" }
        ListElement { text: "RGB" }
    }

    //gui of clahe
    Component{
        id: compCLAHEUI

        AlgoBarCLAHE{
            id: claheUI
        }
    }   

    //gui of gamma correction
    Component {
        id: compGammaCorrectUI

        AlgoBarGammaCorrectUI {
            id: gammaCorrectUI
        }
    }

    //gui of simple color balance
    Component {
        id: compHistoEqualizeUI

        AlgoBarGlobalHE {
            id: globalHEUI
        }
    }

    //gui of hsv correction
    Component {
        id: compHsvCorrectionUI

        AlgoBarHSVUI {
            id: hsvUI
        }
    }

    //gui of monitor rgb
    Component {
        id: compMonitorRGBUI

        AlgoBarMonitorRGBUI {
            id: monitorRGBUI
        }
    }

    Component {
        id: compNoParamUI

        Text {
            anchors.fill: parent

            text: qsTr("Don't need to set parameter")
            horizontalAlignment: Text.AlignHCenter
        }
    }

    //gui of piecewise affine histogram
    Component {
        id: compPahUI

        AlgoBarPAHUI {
            id: pahUI
        }
    }

    Component {
        id: compPerfectReflectUI

        AlgoBarPerfectReflect {
            id: perfectReflectUI
        }
    }

    //gui of simple color balance
    Component {
        id: compScbUI

        AlgoBarSimpleCBUI {
            id: simpleCBUI
        }
    }

    states:[
        State{
            name: ""
            PropertyChanges{ target: loaderPreview; sourceComponent: null}
            PropertyChanges{ target: loaderUI; sourceComponent: null}
        },
        State{
            name: Style.stateChromaWB

            PropertyChanges{ target: loaderPreview; sourceComponent: compChromaWBPreview}
            PropertyChanges{ target: loaderUI; sourceComponent: compNoParamUI}
        },
        State{
            name: Style.stateCLAHE

            PropertyChanges{ target: loaderPreview; sourceComponent: compCLAHEPreview}
            PropertyChanges{ target: loaderUI; sourceComponent: compCLAHEUI}
        },        
        State{
            name: Style.stateGammaCorrect

            PropertyChanges{ target: loaderPreview; sourceComponent: compGammaCorrectPreview}
            PropertyChanges{ target: loaderUI; sourceComponent: compGammaCorrectUI}
        },
        State{
            name: Style.stateGlobalHE

            PropertyChanges{ target: loaderPreview; sourceComponent: compHisEqualPreview}
            PropertyChanges{ target: loaderUI; sourceComponent: compHistoEqualizeUI}
        },
        State{
            name: Style.stateGrayWorld

            PropertyChanges{ target: loaderPreview; sourceComponent: compGrayWorldPreview}
            PropertyChanges{ target: loaderUI; sourceComponent: compNoParamUI}
        },
        State{
            name: Style.stateHSVCorrect

            PropertyChanges{ target: loaderPreview; sourceComponent: compHsvCorrectionPreview}
            PropertyChanges{ target: loaderUI; sourceComponent: compHsvCorrectionUI}
        },
        State{
            name: Style.stateMRGB

            PropertyChanges{ target: loaderPreview; sourceComponent: compMonitorRGBPreview}
            PropertyChanges{ target: loaderUI; sourceComponent: compMonitorRGBUI}
        },
        State{
            name: Style.statePAH

            PropertyChanges{ target: loaderPreview; sourceComponent: compPahPreview}
            PropertyChanges{ target: loaderUI; sourceComponent: compPahUI}
        },
        State{
            name: Style.statePerfectReflect

            PropertyChanges{ target: loaderPreview; sourceComponent: compPerfectReflectPreview}
            PropertyChanges{ target: loaderUI; sourceComponent: compPerfectReflectUI}
        },
        State{
            name: Style.stateSimpleCB

            PropertyChanges{ target: loaderPreview; sourceComponent: compScbPreview}
            PropertyChanges{ target: loaderUI; sourceComponent: compScbUI}
        }
    ]

}
