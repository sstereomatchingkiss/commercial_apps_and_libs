import QtQuick 2.1
import QtQuick.Controls 1.0
import QtQuick.Layouts 1.0

//ui of simplest color space
Column{

    //Layout.fillWidth: true

    GroupBox{
        anchors.horizontalCenter: parent.horizontalCenter
        title: "Color space"        

        Row{
            ComboBox{
                id: boxCLAHEColorSpace

                model: modelCLAHEColorSpace                

                onCurrentTextChanged: {
                    object.changeColorBase(boxCLAHEColorSpace.currentText)
                }
            }
        }
    }

    GroupBox{
        id: boxCLAHEParam

        title: "Parameters"
        //Layout.fillWidth: true

        ColumnLayout{
            //anchors.fill: parent

            Button{
                text: qsTr("Default")
                Layout.fillWidth: true

                onClicked: {
                    sliderCLAHECLip.value = 10
                    sliderCLAHEGrid.value = 3
                }
            }

            GroupBox{
                title: qsTr("Clip limit")
                Layout.fillWidth: true

                RowLayout{
                    Slider{
                        id: sliderCLAHECLip

                        minimumValue: 1
                        maximumValue: 40
                        stepSize: 1.0
                        value: 10
                        onValueChanged: {
                            loaderPreview.item.clipLimit = value / 10

                            if(spinBoxCLAHEClip){
                                if(spinBoxCLAHEClip.value != value){
                                    spinBoxCLAHEClip.value = value
                                }
                            }
                        }
                    }

                    SpinBox{
                        id: spinBoxCLAHEClip

                        minimumValue: sliderCLAHECLip.minimumValue
                        maximumValue: sliderCLAHECLip.maximumValue
                        stepSize: sliderCLAHECLip.stepSize
                        value: 10

                        onValueChanged: {
                            if(sliderCLAHECLip){
                                if(sliderCLAHECLip.value != value){
                                    sliderCLAHECLip.value = value
                                }
                            }
                        }
                    }
                }
            }

            GroupBox{
                title: qsTr("Grid size")
                //Layout.fillWidth: true

                RowLayout{
                    Slider{
                        id: sliderCLAHEGrid

                        maximumValue: 12
                        minimumValue: 3
                        stepSize: 1.0
                        value: 3
                        onValueChanged: {
                            loaderPreview.item.gridSize = value

                            if(spinBoxCLAHEGrid){
                                if(spinBoxCLAHEGrid.value != value){
                                    spinBoxCLAHEGrid.value = value
                                }
                            }
                        }
                    }

                    SpinBox{
                        id: spinBoxCLAHEGrid

                        maximumValue: sliderCLAHEGrid.maximumValue
                        minimumValue: sliderCLAHEGrid.minimumValue
                        stepSize: sliderCLAHEGrid.stepSize
                        value: 3

                        onValueChanged: {
                            if(sliderCLAHEGrid){
                                if(sliderCLAHEGrid.value != value){
                                    sliderCLAHEGrid.value = value
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
