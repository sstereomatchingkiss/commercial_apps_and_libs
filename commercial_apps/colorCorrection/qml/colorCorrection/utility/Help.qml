import QtQuick 2.1
import QtQuick.Controls 1.0
import QtQuick.Dialogs 1.0
import QtQuick.Layouts 1.0
import QtQuick.Window 2.1

Window{
    id: root

    color: syspal.window
    flags: Qt.Dialog
    minimumHeight: 300
    minimumWidth: 1200
    title: "Help"

    property string helpFiles: "helpIndex.html"

    QtObject{
        id: object

        property string helpResource: ":/qml/colorCorrection/helps/"
    }    
    
    ScrollView{
        anchors.fill: parent

    Text{
        id: help
        text: fileProcess.readWholeFile(object.helpResource + helpFiles)
        horizontalAlignment: Text.horizontalCenter
        textFormat: Text.RichText
        onLinkActivated: {
            root.helpFiles = link
        }        
    }

    }
}
