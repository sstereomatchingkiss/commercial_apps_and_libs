import QtQuick 2.1

Item {
    id: root

    property real parentWidth: parent.width
    property real parentHeight: parent.height

    signal rubberBandRelease() //emit when release mouse

    function getX(){
        return root.x + leftWall.x + object.spareWidth
    }

    function getY(){
        return root.y + topWall.y + object.spareHeight
    }

    function getWidth(){
        return Math.max((object.rectWidth - object.spareWidth), 0)
    }

    function getHeight(){
        return Math.max((object.rectHeight - object.spareHeight), 0)
    }

    function setToInitialSize(){
        leftWall.x = -object.minimumWidth
        rightWall.x = object.minimumWidth
        topWall.y = -object.minimumHeight
        bottomWall.y = object.minimumHeight
    }

    QtObject{
        id: object

        readonly property real minimumWidth: 25    //minimum witdh of the resizeRect(initial width = minimumWidth * 2 - spareWidth)
        readonly property real minimumHeight: object.minimumWidth  //minimum height of the resizeRect(initial height = minimumHeight * 2 - spareHeight)

        readonly property real opac: 0.5

        readonly property real spareWidth: 10 //width of the left wall and right wall
        readonly property real spareHeight: object.spareWidth //height of the top wall and bottom wall

        readonly property real rectWidth: rightWall.x - leftWall.x
        readonly property real rectHeight: bottomWall.y - topWall.y               
    }

    onXChanged: {
        //console.log("root.getX = " + root.getX())
        //console.log("leftWall.x = " + leftWall.x)
        //console.log("rightWall.x = " + rightWall.x)
        //console.log("real x = " + root.getX())
    }
    onYChanged: {
        //console.log("y = " + root.getY())
        //console.log("resizeRect.y = " + resizeRect.y)
        //console.log("bottomWall.y = " + bottomWall.y)
        //console.log("topWall.y = " + topWall.y)
        //console.log("real y = " + getY())
    }

    Component.onCompleted: {
        leftWall.anchors.top = topWall.bottom
        rightWall.anchors.top = topWall.bottom
        leftWall.anchors.bottom = bottomWall.top
        rightWall.anchors.bottom = bottomWall.top
        topWall.anchors.left = leftWall.right
        bottomWall.anchors.left = leftWall.right
        topWall.anchors.right = rightWall.left
        bottomWall.anchors.right = rightWall.left

        //if you don't set this anchors to null, you can't drag the root
        root.anchors.centerIn = null
    }

    Rectangle {
        id: resizeRect

        width: object.rectWidth
        height: object.rectHeight
        color: "transparent"
        anchors.right: rightWall.left
        anchors.bottom: bottomWall.top
        anchors.top: topWall.bottom
        anchors.left: leftWall.right
    }

    MouseArea {
        id: resizeRectDragMA

        anchors.fill: resizeRect
        drag.target: root
        drag.minimumX: leftWall.x < 0 ? Math.abs(object.spareWidth + leftWall.x) : -object.spareWidth - leftWall.x
        drag.maximumX: root.parentWidth - rightWall.x
        drag.minimumY: topWall.y < 0 ?  Math.abs(object.spareHeight + topWall.y) : -object.spareHeight - topWall.y
        drag.maximumY: root.parentHeight - bottomWall.y

        onPressed: {
            //console.log("width = " + root.getWidth())
            //console.log("height = " + root.getHeight())
        }

        onReleased: {
            rubberBandRelease() //emit signal
        }
    }

    Rectangle {
        id: topWall

        x: -object.minimumWidth
        y: -object.minimumHeight
        width: object.rectWidth
        height: object.spareHeight
        color: "red"
        opacity: object.opac

        MouseArea {
            id: topWallMA

            anchors.fill: topWall
            drag.target: topWall
            drag.axis: Drag.YAxis

            drag.minimumY: -root.y -object.spareHeight
            drag.maximumY: bottomWall.y - object.minimumHeight

            onReleased: {
                rubberBandRelease() //emit signal
            }
        }
    }

    Rectangle {
        id: bottomWall

        x:-object.minimumWidth
        y: object.minimumHeight
        width: object.rectWidth
        height: object.spareHeight
        color: "purple"
        opacity: object.opac

        MouseArea {
            id: bottomWallMA

            anchors.fill: bottomWall
            drag.target: bottomWall
            drag.axis: Drag.YAxis
            drag.minimumY: topWall.y + object.minimumHeight
            drag.maximumY: root.parentHeight - root.y

            onReleased: {
                rubberBandRelease() //emit signal
            }
        }
    }

    Rectangle{
        id: leftWall

        x: -object.minimumWidth
        y: -object.minimumHeight
        width: object.spareWidth
        height: object.rectHeight
        color: "yellow"
        opacity: object.opac

        MouseArea {
            id: leftWallMA

            anchors.fill: leftWall
            drag.target: leftWall
            drag.axis: Drag.XAxis
            drag.minimumX: -root.x -object.spareWidth
            drag.maximumX : rightWall.x - object.minimumWidth

            onReleased: {
                rubberBandRelease() //emit signal
            }
        }
    }

    Rectangle {
        id: rightWall

        x: object.minimumWidth
        y: -object.minimumHeight
        width: object.spareWidth
        height: object.rectHeight
        color: "orange"
        opacity: object.opac

        MouseArea {
            id: rightWallMA

            anchors.fill: rightWall
            drag.target: rightWall
            drag.axis: Drag.XAxis
            drag.minimumX: leftWall.x + object.minimumWidth
            drag.maximumX: root.parentWidth - root.x

            onReleased: {
                rubberBandRelease() //emit signal
            }
        }
    }//*/
}
