import QtQuick 2.1
import QtQuick.Controls 1.0
import QtQuick.Layouts 1.0

//Item{
    //id: root

    //property var model: view.model
    //property int model
    //property alias model: view.model

    TableView{
        id: view

        anchors.fill: parent
        currentRow: rowCount != 0 ? 0 : currentRow
        //model: object.getModel()
        //model: root.model

        TableViewColumn {
            id: imageColumn

            role: "source"
            title: "image"
            width: object.imageWidth

            delegate: Image {
                id: tableImage
                //anchors.fill: parent
                //source: imageColumn.role
                source: styleData.value//view.model.count != 0 && styleData.row != -1 ? view.model.get(styleData.row).source : ""
                asynchronous: true
                fillMode: Image.PreserveAspectFit
                height: object.imageHeight
                width: object.imageWidth
            }
        }

        TableViewColumn {
            role: "source"
            title: "name"

            delegate: Text{
                x: 20
                text: styleData.value//view.model.count != 0 && styleData.row != -1 ? view.model.get(styleData.row).source : ""
            }
        }

        rowDelegate: Item{
            id: rowDel

            height: styleData.selected ? object.imageHeight + 30 : object.imageHeight
            width: object.imageWidth
            z: 1

            Behavior on height{ NumberAnimation{} }

            Rectangle{
                id: selected
                anchors.fill: parent
                color: styleData.selected ? Qt.lighter("blue") : Qt.lighter("gray")
                //visible: styleData.selected

                SequentialAnimation {
                    running: styleData.selected; loops: Animation.Infinite
                    NumberAnimation { target:selected; property: "opacity"; to: 1.0; duration: 900}
                    NumberAnimation { target:selected; property: "opacity"; to: 0.5; duration: 900}
                }
            }
        }
    }
//}

