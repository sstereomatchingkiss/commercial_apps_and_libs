import QtQuick 2.1
import QtQuick.Controls 1.0
import QtQuick.Layouts 1.0

/**
  *  Show the images by different view and model, this component handle the view by "state";
  *  change the model with property modelType.The "view" and "model" are same as the concept
  *  of Qt model and view pattern
  */
Item {
    id: root

    width: 100
    height: 62

    state: object.stateImagesNames

    readonly property alias previewModelHasFile: object.previewModelHasFile
    readonly property alias processModelHasFile: object.processModelHasFile

    readonly property alias modelHasFile: object.modelHasFile
    readonly property alias modelsHasFile: object.modelsHasFile

    //append the files after select
    function appendImages(files){
        if(object.modelType == object.modelProcessImage){
            for(var i = 0; i != files.length; ++i){
                modelProcessImage.append({"source": files[i]})
            }
        }else{
            for(var i = 0; i != files.length; ++i){
                modelPreviewImage.append({"source": files[i]})
            }
        }
    }

    //change the view of the imageView
    /*function changeView(view){
        if(view == "TableView"){
            loaderLayout.sourceComponent = compImagesNames
        }
    }*/

    function clearImages(){
        if(object.modelType == object.modelProcessImage){
            modelProcessImage.clear();
        }else if(object.modelType == object.modelPreviewImage){
            modelPreviewImage.clear();
        }
    }

    //enable the large image
    function enableLargeImage(enable){
        if(enable){
            largeImage.opacity = 1
            largeImage.enabled = true
        }else{
            largeImage.opacity = 0
            largeImage.enabled = false
        }
    }

    //return the number of elements in the filesModel
    function getFileCount(){
        if(object.modelType == object.modelProcessImage){
            return modelProcessImage.count
        }else{
            return modelPreviewImage.count
        }
    }

    function getPreviewFileCount(){
        return modelPreviewImage.count
    }

    function getProcessFileCount(){
        return modelProcessImage.count
    }

    //get the image select currently
    function getCurrentImageName(){
        if(object.readyToSelectView){
            if(root.state == object.stateMultiSelectOne && modelMultiSelectLeft.count != 0){
                //console.log("multi select = " + modelMultiSelectLeft.get(loaderLayout.item.currentRow).source)
                return modelMultiSelectLeft.get(loaderLayout.item.currentRow).source
            }

            if(object.modelType == object.modelProcessImage && modelProcessImage.count != 0){
                //console.log("process image = " + modelProcessImage.get(loaderLayout.item.currentRow).source)
                return modelProcessImage.get(loaderLayout.item.currentRow).source
            }else if(object.modelType == object.modelPreviewImage && modelPreviewImage.count != 0){
                //console.log("preview image = " + modelPreviewImage.get(loaderLayout.item.currentRow).source)
                return modelPreviewImage.get(loaderLayout.item.currentRow).source
            }
        }

        return ""
    }

    //get the sources(address of the images)
    function getImageNames(){
        var names = []
        if(object.modelType == object.modelProcessImage){
            for(var i = 0; i != modelProcessImage.count; ++i){
                names.push(modelProcessImage.get(i).source)
            }
        }else if(object.modelType == object.modelPreviewImage){
            for(var i = 0; i != modelPreviewImage.count; ++i){
                names.push(modelPreviewImage.get(i).source)
            }
        }

        return names
    }

    function getModelMultiSelectRightNames(){
        var names = []
        for(var i = 0; i != modelMultiSelectRight.count; ++i){
            names.push(modelMultiSelectRight.get(i).source)
        }

        return names
    }

    function modelMultiSelectRightHasManyImages(){
        return modelMultiSelectRight.count > 1
    }

    function removeImage(){
        if(object.readyToSelectView){
            if(object.modelType == object.modelProcessImage){
                modelProcessImage.remove(loaderLayout.item.currentRow)
            }else{
                modelPreviewImage.remove(loaderLayout.item.currentRow)
            }
        }
    }

    function toModelMultiSelectOne(){
        object.modelType = object.modelMultiSelectOne
    }

    function toModelPreviewImage(){
        object.modelType = object.modelPreviewImage
    }

    function toModelProcessImage(){
        object.modelType = object.modelProcessImage
    }

    function toViewImagesNames(){
        object.setObjectCurrentRow()
        root.state = object.stateImagesNames
    }

    function toViewMultiSelectOne(){
        modelMultiSelectLeft.clear()
        modelMultiSelectRight.clear()
        if(object.modelType == object.modelProcessImage){
            for(var i = 0; i != modelProcessImage.count; ++i){
                modelMultiSelectLeft.append({"source": modelProcessImage.get(i).source})
            }
        }else if(object.modelType == object.modelPreviewImage){
            for(var i = 0; i != modelPreviewImage.count; ++i){
                modelMultiSelectLeft.append({"source": modelPreviewImage.get(i).source})
            }
        }

        object.setObjectCurrentRow()
        root.state = object.stateMultiSelectOne
    }

    function toViewThumbsImage(){
        object.setObjectCurrentRow()
        root.state = object.stateThumbsImage
    }

    QtObject{
        id: object

        //save the currentRow of the loaderLayout, so users don't need to reselect the image
        //when changing view
        property int currentRow: 0

        property int imageHeight: 45
        property int imageWidth: 80

        property bool modelHasFile: root.getFileCount() != 0
        property bool modelsHasFile: root.getPreviewFileCount() != 0 && root.getProcessFileCount() != 0
        property string modelMultiSelectOne: "Model multi select one"
        property string modelPreviewImage: "Preview images"
        property string modelProcessImage: "Process images"
        property string modelType: object.modelProcessImage

        property bool previewModelHasFile: root.getPreviewFileCount() != 0
        property bool processModelHasFile: root.getProcessFileCount() != 0

        property bool readyToSelectView: loaderLayout.status == Loader.Ready && loaderLayout.item.currentRow != -1 ? true : false

        property string stateMultiSelectOne: "MultiSelectOne"
        property string stateImagesNames: "Images/names"
        property string stateThumbsImage: "Thumbs/image"

        function allToLeftModel(){
            //console.log("all to left")

            //console.log("all to left ready to select view")
            for(var i = 0; i != modelMultiSelectRight.count; ++i){
                modelMultiSelectLeft.append({"source": modelMultiSelectRight.get(i).source})
                //console.log("to left : " + modelMultiSelectRight.get(i).source)
            }

            modelMultiSelectRight.clear()
        }

        function allToRightModel(){

            for(var i = 0; i != modelMultiSelectLeft.count; ++i){
                modelMultiSelectRight.append({"source": modelMultiSelectLeft.get(i).source})
            }

            modelMultiSelectLeft.clear()
        }

        function getModel(){
            if(object.modelType == object.modelProcessImage){
                //console.log("model type is Process images")
                return modelProcessImage
            }else if(object.modelType == object.modelPreviewImage){
                //console.log("model type is Preview images")
                return modelPreviewImage
            }

            return null
        }

        function setObjectCurrentRow(){
            if(object.readyToSelectView){
                object.currentRow = loaderLayout.item.currentRow
            }
        }

        function toLeftModel(index){
            if(index != -1){
                modelMultiSelectLeft.append({"source": modelMultiSelectRight.get(index).source})
                modelMultiSelectRight.remove(index)
                loaderLayout.item.toLeftViewLowestImage()
            }
        }

        function toRightModel(index){
            if(index != -1){
                modelMultiSelectRight.append({"source": modelMultiSelectLeft.get(index).source})
                modelMultiSelectLeft.remove(index)
                loaderLayout.item.toRightViewLowestImage()
            }
        }
    }

    ListModel{
        id: modelMultiSelectLeft
    }

    ListModel{
        id: modelMultiSelectRight
    }

    ListModel{
        id: modelProcessImage
    }

    ListModel{
        id: modelPreviewImage
    }

    ColumnLayout{
        anchors.fill: parent

        Loader{
            id: loaderLayout

            anchors.fill: parent
            sourceComponent: null

            onStatusChanged: {
                if(loaderLayout.status == Loader.Ready){
                    loaderLayout.item.currentRow = object.currentRow
                }
            }
        }
    }

    Component{
        id: compImagesNames

        ImagesNamesView{
            model: object.getModel()
        }
    }

    Component{
        id: compMultiSelectViewOne

        MultiSelectViewOne{

        }
    }

    Component{
        id: componentThumbsImage

        ThumbsImageView{
            model: object.getModel()
        }
    }

    states:[
        State{
            name: ""

            PropertyChanges {target: loaderLayout; sourceComponent: null}
        },
        State{
            name: object.stateImagesNames

            PropertyChanges {target: loaderLayout; sourceComponent: compImagesNames}
        },
        State{
            name: object.stateMultiSelectOne

            PropertyChanges {target: loaderLayout; sourceComponent: compMultiSelectViewOne}
        },
        State{
            name: object.stateThumbsImage

            PropertyChanges {target: loaderLayout; sourceComponent: componentThumbsImage}
        }
    ]
}
