import QtQuick 2.1
import QtQuick.Controls 1.0
import QtQuick.Layouts 1.0

TableView{
    id: root

    anchors.fill: parent
    currentRow: rowCount != 0 ? 0 : currentRow

    property alias titleImage: imageColumn.title

    TableViewColumn {
        id: imageColumn

        //role: "source"
        title: "image"
        width: object.imageWidth

        delegate: Image {

            //source: imageColumn.role
            x: (root.width - object.imageWidth) / 2
            Layout.alignment: Layout.Center
            source: styleData.value
            asynchronous: true
            fillMode: Image.PreserveAspectFit
            height: object.imageHeight
            width: object.imageWidth
        }
    }

    rowDelegate: Item{
        id: rowDel

        height: styleData.selected ? object.imageHeight + 30 : object.imageHeight
        width: object.imageWidth
        z: 1

        Behavior on height{ NumberAnimation{} }

        Rectangle{
            id: selected
            anchors.fill: parent
            color: styleData.selected ? Qt.lighter("blue") : Qt.lighter("gray")
            //visible: styleData.selected

            SequentialAnimation {
                running: styleData.selected; loops: Animation.Infinite
                NumberAnimation { target:selected; property: "opacity"; to: 1.0; duration: 900}
                NumberAnimation { target:selected; property: "opacity"; to: 0.5; duration: 900}
            }
        }
    }
}
