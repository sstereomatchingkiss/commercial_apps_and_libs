import QtQuick 2.1
import QtQuick.Controls 1.0


TableView {
    id: table

    property var indexes: []
    property int __clicks: 0
    SystemPalette{ id: syspal }

    rowDelegate: Rectangle {
        property bool selected: table.__clicks && indexes.indexOf(styleData.row) > -1
        width: parent.width ; height: 18
        color: selected ? syspal.highlight : syspal.base
    }

    itemDelegate: Text {
        property bool selected: table.__clicks && indexes.indexOf(styleData.row) > -1
        anchors.fill: parent
        color: selected ? syspal.highlightedText : syspal.text
        anchors.leftMargin: 6
        verticalAlignment: Text.AlignVCenter
        renderType: Text.NativeRendering
        elide: styleData.elideMode
        text: styleData.value
    }

    onClicked: {
        var indexAt = indexes.indexOf(row)
        if (indexAt > -1)
            indexes.splice(indexAt, 1)
        else
            indexes.push(row)
        __clicks++  // force a re-evaluation of indexes in the delegates
    }
}

