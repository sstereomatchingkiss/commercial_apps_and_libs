import QtQuick 2.1
import QtQuick.Controls 1.0
import QtQuick.Layouts 1.0

Row{
    id: root

    anchors.fill: parent

    property alias currentRow: leftView.currentRow

    function toLeftViewLowestImage(){
        if(modelMultiSelectLeft.count != 0){
            leftView.currentRow = modelMultiSelectLeft.count - 1
        }
    }

    function toRightViewLowestImage(){
        if(modelMultiSelectRight.count != 0){
            rightView.currentRow = modelMultiSelectRight.count - 1
        }
    }

    Item{
        id: leftImages

        height: parent.height
        width: (parent.width) / 2 - panelMove.width / 2
        ThumbsImageView{
            id: leftView
            height: parent.height
            width: parent.width
            model: modelMultiSelectLeft
            titleImage: qsTr("Select image want to process")

            onClicked: {
                if(boxAutoMove.checked){
                    object.toRightModel(leftView.currentRow)
                }
            }
        }
    }

    ColumnLayout{
        id: panelMove

        visible: modelHasFile
        //height: parent.height
        width: 200

        Button{
            id: buttonRight

            Layout.fillWidth: true
            iconSource: "../../images/bullet_arrow_right.png"
            tooltip: "Move <b style=\"color:red\">image</b> to the <b style=\"color:red\">right</b> side"

            onClicked: {
                object.toRightModel(leftView.currentRow)
            }
        }

        Button{
            id: buttonLeft

            Layout.fillWidth: true
            iconSource: "../../images/bullet_arrow_left.png"
            tooltip: "Move <b style=\"color:red\">image</b> to the <b style=\"color:red\">left</b> side"

            onClicked: {
                object.toLeftModel(rightView.currentRow)
            }
        }

        Button{
            id: buttonRightAll

            Layout.fillWidth: true
            iconSource: "../../images/arrow_right.png"
            tooltip: "Move <b style=\"color:red\">all images</b> to the <b style=\"color:red\">right</b> side"

            onClicked: {
                object.allToRightModel()
            }
        }

        Button{
            id: buttonLeftAll

            Layout.fillWidth: true
            iconSource: "../../images/arrow_left.png"
            tooltip: "Move <b style=\"color:red\">all images</b> to the <b style=\"color:red\">left</b> side"

            onClicked: {
                object.allToLeftModel()
            }
        }

        GroupBox{
            Layout.fillWidth: true

            title: qsTr("Auto move")

            CheckBox{
                id: boxAutoMove

                checked: true
                text: qsTr("Auto move")
            }
        }
    }

    Item{
        height: leftImages.height
        width: leftImages.width
        ThumbsImageView{
            id: rightView

            height: parent.height
            width: parent.width
            model: modelMultiSelectRight
            titleImage: qsTr("Image would be processed")

            onClicked: {
                if(boxAutoMove.checked){
                    object.toLeftModel(rightView.currentRow)
                }
            }
        }
    }
}

