.pragma library

//model between process images and preview images
var modelPreviewFiles = "Preview images"
var modelProcessFiles = "Process images"

//states of the AlgoBar, in charge on different UI of algorithms
var stateChromaWB = "Chromaticiy white balance"
var stateCLAHE = "Contrast Limited adaptive histogram"
var stateGammaCorrect = "Gamma correction"
var stateGlobalHE = "Histogram equalization"
var stateGrayWorld = "Gray world assumption"
var stateHSVCorrect = "HSV Correction"
var stateMRGB     = "Monitor RGB"
var statePAH      = "Piecewise affine histogram"
var statePerfectReflect = "Perfect reflector"
var stateSimpleCB = "Simple color balance"

//states of the layoutImageViewer in mainDesktop.qml
var stateImagesNames = "Images/names"
var stateMultiSelectOne = "MultiSelectOne"
var stateNoFiles = "NoFiles"
var stateThumbsImage = "Thumbs/image"

//sizes of the small preview images
var previewHeight = 180
var previewWidth = 320
