import QtQuick 2.1
import QtQuick.Controls 1.0
import QtQuick.Dialogs 1.0
import QtQuick.Layouts 1.0

import File 1.0

import "desktop"
import "js/style.js" as Style
import "utility"
import "utility/algoBar"
import "utility/imageViewer"

ApplicationWindow{
    id: root

    title: "Color Correction"

    color: syspal.window
    width: 1450
    height: 750
    minimumHeight: 750
    minimumWidth: 1400

    Component.onCompleted: {
        object.enablePreviewModel()
    }

    QtObject{
        id: object

        property bool addImages: false

        property int durationShort: 600
        property int durationNormal: 1000
        property int durationMedium: 2000

        property bool selectFileModel: false
        property bool selectPreviewModel: false

        function enablePreviewModel(){
            if(fileProcess.checkImageSizeInFolder(winConfigure.getImagePath()) >= 1){
                actionSelectPreviewModel.enabled = true
                toolButtonSelectModels.enabled = true
            }else{
                actionSelectPreviewModel.enabled = false
                toolButtonSelectModels.enabled = false
                actionSelectProcessFilesModel.trigger()
            }
        }

        function enabledUI(enabled, opac){
            imageLarger.enabled = enabled
            imageViewer.enabled = enabled
            toolbar.enabled = enabled
            if(!enabled){
                winConfigure.visible = false
            }

            imageLarger.opacity = opac
            imageViewer.opacity = opac
        }

        function insertImages(){
            var count = imageViewer.getFileCount() //this variable is for the animation sake

            if(object.addImages){
                imageViewer.appendImages(fileProcess.uniqueUrlsStrs(fileDialog.fileUrls, imageViewer.getImageNames()))
            }else{
                imageViewer.appendImages(fileProcess.uniqueFolderImages(imageViewer.getImageNames(), folderDialog.fileUrl))
            }
            if(count == 0 && imageViewer.getFileCount() != 0){
                actionLayoutImagesNames.trigger()
            }
            if(layoutImageViewer.state == Style.stateMultiSelectOne){
                actionLayoutMultiSelectOne.trigger()
            }
        }
    }

    SystemPalette {id: syspal}

    FileProcess{
        id: fileProcess
    }

    FileDialog{
        id: fileDialog
        selectMultiple: true
        nameFilters: [ "Image files (*.bmp *.jpg *.JPEG *.png *.ppm *.tif *.tiff *.xbm *.xpm)" ]
        onAccepted: {
            object.insertImages()
        }
    }

    FileDialog{
        id: folderDialog
        selectFolder: true
        onAccepted: {
            object.insertImages()
        }
    }

    //add the images selected by users
    Action{
        id: actionAddImages

        enabled: actionSelectProcessFilesModel.checked
        text: qsTr("Add images")
        //shortcut: "Ctrl+O"
        iconSource: "images/photo_add.png"
        onTriggered:{
            object.addImages = true
            fileDialog.open()
        }
        tooltip: qsTr("Add images")
    }

    //add the images under the folder selected by users
    Action{
        id: actionAddFolder

        enabled: actionSelectProcessFilesModel.checked
        text: qsTr("Add folder")
        //shortcut: "Ctrl+F"
        iconSource: "images/folder_add.png"
        onTriggered: {
            object.addImages = false
            folderDialog.open()
        }
        tooltip: qsTr("Add folder")
    }

    //configure some parameters, handle the place image will be save, series name, fill mode of the largeImage and so on
    Action{
        id: actionConfigure
        text: qsTr("Configure")
        iconSource: "images/toolbox.png"
        onTriggered: {
            winConfigure.visible = !winConfigure.visible
        }
        tooltip: qsTr("Configure")
    }

    //show the help
    Action{
        id: actionHelp
        text: qsTr("Help")
        //shortcut: "Ctrl+H"
        iconSource: "images/help.png"
        onTriggered: {
            winHelp.visible = !winHelp.visible
        }
        tooltip: qsTr("Help")
    }

    //change the layout of the apps
    ExclusiveGroup {
        id: actionLayoutGroup

        Action {
            id: actionLayoutImagesNames

            text: qsTr("Images/names")
            checkable: true
            Component.onCompleted: checked = true

            onTriggered: {
                imageViewer.toViewImagesNames()
                imageLarger.enableRubberBand = false
                layoutImageViewer.state = Style.stateImagesNames
            }
        }

        Action {
            id: actionLayoutThumbsImage
            text: qsTr("Thumbs/image")
            checkable: true

            onTriggered: {
                imageViewer.toViewThumbsImage()
                layoutImageViewer.state = Style.stateThumbsImage
            }
        }

        Action {
            id: actionLayoutMultiSelectOne

            text: qsTr("Multi-select")
            checkable: true

            onTriggered: {
                imageViewer.toViewMultiSelectOne()
                imageLarger.enableRubberBand = false
                layoutImageViewer.state = Style.stateMultiSelectOne
            }
        }
    }

    //allow the users to select different models, currently I support two models
    //one for process the images selected by users, the other is aim for showing and process
    //the images save by users
    ExclusiveGroup {
        id: actionModelGroup

        Action{
            id: actionSelectProcessFilesModel
            text: qsTr("Image want to process")
            checkable: true
            Component.onCompleted: checked = true
            onTriggered: {
                var currentState = layoutImageViewer.state
                imageViewer.toModelProcessImage()
                if(!imageViewer.modelHasFile){
                    return
                }else if(currentState == Style.stateNoFiles || currentState == Style.stateImagesNames){
                    actionLayoutImagesNames.trigger()
                }else if(currentState == Style.stateThumbsImage){
                    actionLayoutThumbsImage.trigger()
                }else if(currentState == Style.stateMultiSelectOne){
                    actionLayoutMultiSelectOne.trigger()
                }
            }
        }

        Action {
            id: actionSelectPreviewModel
            text: qsTr("Show images after process")
            checkable: true
            onTriggered: {
                //when you change to the model "modelPreviewFiles", the items of the "modelPreviewFiles" may be zero at first
                //this would trigger the state and make it back to the state "stateNoFiles" and cause the layout become weird
                //so we need a state to record current state and select appropriate layout according to the state
                var currentState = layoutImageViewer.state
                imageViewer.toModelPreviewImage()
                imageViewer.clearImages()
                imageViewer.appendImages(fileProcess.uniqueFolderImages(imageViewer.getImageNames(), winConfigure.getImagePath()))
                if(!imageViewer.modelHasFile){
                    return
                }else if(currentState == Style.stateNoFiles || currentState == Style.stateImagesNames){
                    actionLayoutImagesNames.trigger()
                }else if(currentState == Style.stateThumbsImage){
                    actionLayoutThumbsImage.trigger()
                }else if(currentState == Style.stateMultiSelectOne){
                    actionLayoutMultiSelectOne.trigger()
                }
            }
        }
    }

    //Remove the images selected by users(wouldn't delete the image)
    Action{
        id: actionRemovePhoto

        enabled: imageViewer.modelHasFile && layoutImageViewer.state != Style.stateMultiSelectOne
        text: qsTr("Remove image from list")
        //shortcut: "Ctrl+D"
        iconSource: "images/photo_delete.png"
        onTriggered: {
            imageViewer.removeImage()
        }
        tooltip: actionRemovePhoto.text
    }

    //Remove all of the images(wouldn't delete the image)
    Action{
        id: actionRemoveAll

        enabled: imageViewer.modelHasFile && layoutImageViewer.state != Style.stateMultiSelectOne
        text: qsTr("Remove all images from list")
        //shortcut: "Ctrl+R"
        iconSource: "images/page_white_delete.png"
        onTriggered: {
            imageViewer.clearImages()
        }
        tooltip: actionRemoveAll.text
    }

    //use rubberband to select the image(single) region you want to process, only work when the image fillMode == Image.Pad
    Action{
        id: actionRubberBand

        enabled: imageViewer.modelHasFile
        text: qsTr("Select the region want to process")
        //shortcut: "Ctrl+R"
        iconSource: "images/select_restangular.png"
        onTriggered: {
            //winConfigure.setImageToOriginalSize()
            imageLarger.enableRubberBand = !imageLarger.enableRubberBand
            if(imageLarger.enableRubberBand){                
                winConfigure.setImageToOriginalSize()
                actionLayoutThumbsImage.trigger()
            }
        }
        tooltip: actionRubberBand.text
    }

    //list the available color correction, enhancement algorithms
    ListModel{
        id: modelAlgorithms

        ListElement { text: "Chromaticiy white balance"}
        ListElement { text: "Contrast Limited adaptive histogram" }
        ListElement { text: "Gamma correction" }
        ListElement { text: "Histogram equalization" }
        ListElement { text: "Gray world assumption" }
        ListElement { text: "HSV Correction" }
        ListElement { text: "Monitor RGB" }
        ListElement { text: "Perfect reflector" }
        ListElement { text: "Piecewise affine histogram" }
        ListElement { text: "Simple color balance" }
    }

    toolBar: ToolBar{
        id: toolbar

        //enabled: !algoBar.busy
        RowLayout {
            id: toolbarLayout
            spacing: 0
            width: parent.width
            ToolButton {
                action: actionAddImages
                //opacity: actionSelectProcessFilesModel.checked ? 1.0 : 0.3
                rotation: imageViewer.processModelHasFile || actionSelectPreviewModel.checked ? 0 : rotation

                //Behavior on opacity{ NumberAnimation{duration: object.durationNormal} }

                NumberAnimation on rotation { loops: Animation.Infinite; from: 0; to: 360; duration: object.durationMedium;
                    running: !imageViewer.processModelHasFile && actionSelectProcessFilesModel.checked}
            }

            ToolButton {
                action: actionAddFolder
                //opacity: actionSelectProcessFilesModel.checked ? 1.0 : 0.3
                rotation: imageViewer.processModelHasFile || actionSelectPreviewModel.checked ? 0 : rotation

                NumberAnimation on rotation { loops: Animation.Infinite; from: 0; to: 360; duration: object.durationMedium;
                    running: !imageViewer.processModelHasFile && actionSelectProcessFilesModel.checked}
            }

            ToolButton {
                id: toolButtonSelectModels
                iconSource: "images/add_package.png"
                text: qsTr("Select models")
                tooltip: qsTr("Select models")

                //enabled: false

                Menu {
                    id: modelMenu

                    MenuItem {
                        action: actionSelectProcessFilesModel
                        text: qsTr(actionSelectProcessFilesModel.text)
                    }
                    MenuItem {
                        action: actionSelectPreviewModel
                        text: qsTr(actionSelectPreviewModel.text)
                    }
                }

                onClicked:{
                    object.enablePreviewModel()
                    modelMenu.popup()
                }
            }

            ToolButton {
                action: actionRemovePhoto
                enabled: imageViewer.modelHasFile
                opacity: imageViewer.modelHasFile ? 1 : 0.3

                Behavior on opacity { NumberAnimation{duration: object.durationNormal} }
            }

            ToolButton {
                action: actionRemoveAll
                enabled: imageViewer.modelHasFile
                opacity: imageViewer.modelHasFile ? 1 : 0.3

                Behavior on opacity { NumberAnimation{duration: object.durationNormal} }
            }

            ToolButton {
                enabled: imageViewer.modelHasFile
                opacity: imageViewer.modelHasFile ? 1 : 0.3

                iconSource: "images/layout.png"
                text: qsTr("Layout")
                tooltip: qsTr("Layout")

                Menu {
                    id: layoutMenu
                    MenuItem {
                        action: actionLayoutImagesNames
                        text: Style.stateImagesNames
                    }
                    MenuItem {
                        action: actionLayoutThumbsImage
                        text: Style.stateThumbsImage
                    }
                    MenuItem{
                        action: actionLayoutMultiSelectOne
                        text: Style.stateMultiSelectOne
                    }
                }

                onClicked:{
                    layoutMenu.popup()
                }

                Behavior on opacity { NumberAnimation{duration: object.durationNormal} }
            }

            ToolButton{
                action: actionRubberBand
            }

            ToolButton {
                action: actionConfigure
            }

            ToolButton {
                action: actionHelp
            }

            Item { Layout.fillWidth: true }

            GroupBox {
                id: algoGroupBox
                title: qsTr("Algorithms")
                Layout.minimumWidth: 300

                enabled: imageViewer.modelHasFile
                opacity: imageViewer.modelHasFile ? 1 : 0

                RowLayout {
                    id: algoBoxLayout
                    anchors.fill: parent
                    Layout.minimumWidth: 300

                    ComboBox {
                        id: algoComboBox

                        model: modelAlgorithms
                        Layout.fillWidth: true

                        onCurrentTextChanged: {
                            algoBar.state = algoComboBox.currentText
                        }
                    }
                }

                Behavior on opacity{ NumberAnimation{duration: object.durationShort} }
            }
        }
    }

    Help{
        id: winHelp
    }

    WindowConfigure {
        id: winConfigure
    }

    ErrorLog{
        id: errorLog

        width: 400
        height: 300

        text: algoBar.getErrorLog()
    }

    Row{
        id: layoutImageViewer

        anchors.fill: parent

        state: Style.stateNoFiles

        ImageViewer {
            id: imageViewer
        }

        ScrollAbleImage{
            id: imageLarger

            fillMode: winConfigure.getImageFillMode()
            source: imageViewer.getCurrentImageName()

            onGetRubberBandArea: {
                algoBar.setSelectedArea(x, y, width, height)
            }
        }

        AlgoBar{
            id: algoBar

            color: syspal.window
            enableRubberBand: imageLarger.enableRubberBand
            processManyImage: imageViewer.modelMultiSelectRightHasManyImages() && layoutImageViewer.state == Style.stateMultiSelectOne
            source: imageViewer.getCurrentImageName()

            onProcessBegin: {
                object.enabledUI(false, 0.3)
                if(!processManyImage){
                    algoBar.saveImage(winConfigure.getImagePath(), winConfigure.getSaveType(), winConfigure.getSeries())
                }else{
                    algoBar.saveImages(winConfigure.getImagePath(), imageViewer.getModelMultiSelectRightNames(),
                                       winConfigure.getSaveType(), winConfigure.getSeries())
                }
            }

            onProcessExit: {
                object.enabledUI(true, 1.0)
                object.enablePreviewModel()

                if(errorLog.text != ""){
                    errorLog.visible = true
                }
            }

            onSetSelectedAreaSignal: {
                algoBar.setSelectedArea(imageLarger.getRubberBandX(), imageLarger.getRubberBandY(),
                                        imageLarger.getRubberBandWidth(), imageLarger.getRubberBandHeight())
            }

            //onEnableRubberBandChanged: {
                //console.log("enabled rubberband = " + imageLarger.enableRubberBand)
            //}
        }

        states: [
            State{
                name: Style.stateNoFiles
                when: !imageViewer.modelHasFile

                PropertyChanges { target: algoBar; opacity: 0; height: 0; width: 0}
                PropertyChanges { target: imageLarger; height: 0; width: 0}
                PropertyChanges { target: imageLarger; opacity: 0}
                PropertyChanges { target: imageViewer; height: 0; width: 0}
            },
            State{
                name: Style.stateImagesNames

                PropertyChanges { target: algoBar; opacity: 1; height: root.height; width: root.width / 4}
                PropertyChanges { target: imageLarger; height: 0; width: 0}
                PropertyChanges { target: imageLarger; opacity: 0}
                PropertyChanges { target: imageViewer; height: root.height - toolBar.height; width: root.width - algoBar.width}
            },
            State{
                name: Style.stateMultiSelectOne

                PropertyChanges { target: algoBar; opacity: 1; height: root.height; width: root.width / 4}
                PropertyChanges { target: imageLarger; height: 0; width: 0}
                PropertyChanges { target: imageLarger; opacity: 0}
                PropertyChanges { target: imageViewer; height: root.height - toolBar.height; width: root.width - algoBar.width}
            },
            State{
                name: Style.stateThumbsImage

                PropertyChanges { target: algoBar; opacity: 1; height: root.height; width: root.width / 4}
                PropertyChanges { target: imageLarger; height: root.height - toolBar.height; width: root.width - algoBar.width - 120}
                PropertyChanges { target: imageLarger; opacity: 1.0}
                PropertyChanges { target: imageViewer; height: root.height - toolBar.height; width: 120}
            }
        ]

        transitions: [
            Transition {
                from: "*"; to: Style.stateThumbsImage
                ParallelAnimation {
                    NumberAnimation { target: imageLarger; property: "height"; duration: object.durationShort }
                    NumberAnimation { target: imageLarger; property: "width";  duration: object.durationShort }
                    NumberAnimation { target: imageViewer; property: "width";  duration: object.durationShort }
                }
            },
            Transition {
                from: "*"; to: Style.stateImagesNames
                ParallelAnimation {
                    NumberAnimation { target: imageLarger; property: "height"; duration: object.durationShort }
                    NumberAnimation { target: imageLarger; property: "width";  duration: object.durationShort }
                    NumberAnimation { target: imageViewer; property: "width";  duration: object.durationShort }
                }
            },
            Transition {
                from: "*"; to: Style.stateMultiSelectOne
                ParallelAnimation {
                    NumberAnimation { target: imageLarger; property: "height"; duration: object.durationShort }
                    NumberAnimation { target: imageLarger; property: "width";  duration: object.durationShort }
                    NumberAnimation { target: imageViewer; property: "width";  duration: object.durationShort }
                }
            }
        ]
    }//*/
}
