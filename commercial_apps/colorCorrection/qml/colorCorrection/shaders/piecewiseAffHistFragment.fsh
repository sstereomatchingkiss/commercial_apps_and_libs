uniform sampler2D source;
varying highp vec4 qt_TexCoord0;

void main(void)
{
    gl_FragColor = texture2D(source, qt_TexCoord0.st);
}
