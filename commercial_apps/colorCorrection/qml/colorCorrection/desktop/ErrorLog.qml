import QtQuick 2.1
import QtQuick.Controls 1.0
import QtQuick.Window 2.1

Window{
    id: root

    color: syspal.window
    flags: Qt.Dialog
    minimumHeight: 200
    minimumWidth: 400
    title: qsTr("Error message")

    property alias text: log.text

    TextArea{
        id: log

        anchors.centerIn: parent
        //anchors.fill: root
        height:parent.height / 2
        width: parent.width / 2

        horizontalAlignment: TextEdit.AlignHCenter
        verticalAlignment: TextEdit.AlignVCenter
    }

}

