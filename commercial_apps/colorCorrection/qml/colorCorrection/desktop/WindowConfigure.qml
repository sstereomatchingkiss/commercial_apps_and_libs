import QtQuick 2.1
import QtQuick.Controls 1.0
import QtQuick.Dialogs 1.0
import QtQuick.Layouts 1.0
import QtQuick.Window 2.1

Window{
    id: root

    color: syspal.window
    flags: Qt.Dialog
    minimumHeight: 200
    minimumWidth: 400
    title: qsTr("Configure")


    function getImageFillMode(){
        if(boxImageFillMode.currentText == "Keep aspect ratio"){
            return Image.PreserveAspectFit
        }else if(boxImageFillMode.currentText == "Keep aspect ratio and crop"){
            return Image.PreserveAspectCrop
        }else if(boxImageFillMode.currentText == "Stretch"){
            return Image.Stretch
        }else if(boxImageFillMode.currentText == "Tile"){
            return Image.Tile
        }else if(boxImageFillMode.currentText == "Tile vertically"){
            return Image.TileVertically
        }else if(boxImageFillMode.currentText == "Tile horizontally"){
            return Image.TileHorizontally
        }else if(boxImageFillMode.currentText == "Original size"){
            return Image.Pad
        }
    }

    function getImagePath(){
        if(textFieldImagePath.text != ""){
            return textFieldImagePath.text
        }

        return fileProcess.getHomePath() + "/ColorCorrectionPictures"
    }

    function getSaveType(){
        return boxSaveType.currentText
    }

    function getSeries(){
        if(textFieldSName != ""){
            return textFieldSName.text
        }

        return "img"
    }

    function setImageToOriginalSize(){
        boxImageFillMode.currentIndex = 3
    }

    ListModel{
        id: modelFillImage

        ListElement { text: "Keep aspect ratio"}
        ListElement { text: "Keep aspect ratio and crop"}
        ListElement { text: "Stretch"}
        //ListElement { text: "Tile"}
        //ListElement { text: "Tile vertically"}
        //ListElement { text: "Tile horizontally"}
        ListElement { text: "Original size"}
    }

    ListModel{
        id: modelSaveAble

        ListElement { text: ".bmp" }
        ListElement { text: ".jpg" }
        ListElement { text: ".jpeg" }
        ListElement { text: ".png" }
        ListElement { text: ".ppm" }
        ListElement { text: ".xbm" }
        ListElement { text: ".xpm" }
    }

    QtObject{
        id: object

        property string imagePath: fileProcess.getHomePath() + "/Pictures/ColorCorrectionPictures"
        property string series: "img"
    }

    GroupBox{
        anchors.fill: parent
        GridLayout{
            anchors.fill: parent
            columns: 2
            rows: 6
            Label{
                text: qsTr("Save at");
            }
            TextField {
                id: textFieldImagePath
                Layout.fillWidth: true
                text: object.imagePath
            }

            Label{
                text: qsTr("Series name");
            }

            TextField {
                id: textFieldSName
                Layout.fillWidth: true
                text: object.series
            }

            Label{
                text: qsTr("Save type");
            }

            ComboBox {
                id: boxSaveType
                Layout.fillWidth: true
                currentIndex: 1
                model: modelSaveAble
            }

            Label{
                text: qsTr("Image fill mode")
            }

            ComboBox{
                id: boxImageFillMode
                Layout.fillWidth: true
                model :modelFillImage
            }

            Button{
                text: qsTr("Default")
                Layout.fillWidth: true
                Layout.columnSpan: 2

                onClicked: {
                    boxImageFillMode.currentIndex = 0
                    boxSaveType.currentIndex = 1
                    textFieldSName.text = object.series
                    textFieldImagePath.text = object.imagePath
                }
            }

            Button{
                text: qsTr("Close")
                Layout.fillWidth: true
                Layout.columnSpan: 2

                onClicked: {
                    root.close()
                }
            }
        }
    }
}
