#ifndef SMOKEDETECTORCUSTOM00_HPP
#define SMOKEDETECTORCUSTOM00_HPP

#include "smokeDetectorGeneric.hpp"

#include "smokeDetectorPolicy/detect/smokeDetectCustom00.hpp"
#include "smokeDetectorPolicy/structure/structureCustom00.hpp"
#include "smokeDetectorPolicy/utility/getSmokeArea.hpp"

/**
 *@brief implementation of the algorithm described by the paper
 * "smoke detection algorithm for intelligent video surveillance system"
 */

using smokeDetectorCustom00 = smokeDetectorGeneric<StructCustom00, smokeDetectCustom00, getSmokeArea>;

#endif // SMOKEDETECTORCUSTOM00_HPP
