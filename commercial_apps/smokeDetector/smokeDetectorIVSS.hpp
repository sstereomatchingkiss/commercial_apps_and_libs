#ifndef SMOKEDETECTORIVSS_HPP
#define SMOKEDETECTORIVSS_HPP

#include "smokeDetectorGeneric.hpp"

#include "smokeDetectorPolicy/detect/smokeDetectIVSS.hpp"
#include "smokeDetectorPolicy/structure/structureIVSS.hpp"
#include "smokeDetectorPolicy/utility/getSmokeArea.hpp"

//template<typename Structure, typename DetectPolicy, ,typename PreprocessPolicy, typename UtilityPolicy>

/**
 *@brief implementation of the algorithm described by the paper
 * "smoke detection algorithm for intelligent video surveillance system"
 */

using smokeDetectorIVSS = smokeDetectorGeneric<StructIVSS, smokeDetectIVSS, getSmokeArea>;

#endif // SMOKEDETECTORIVSS_HPP
