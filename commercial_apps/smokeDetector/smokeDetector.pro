# Add more folders to ship with the application, here
folder_01.source = qml/smokeDetector
folder_01.target = qml
DEPLOYMENTFOLDERS = folder_01

# Additional import path used to resolve QML modules in Creator's code model
QML_IMPORT_PATH =

CONFIG += c++11

QT += multimedia multimediawidgets

win32{

DEFINES += WIN_OS

INCLUDEPATH += ../../../3rdLibs/openCV/OpenCV-2.4.5/build/include

#for mingw
#CV_LIBS_PATH = -L../../../3rdLibs/openCV/OpenCV-2.4.5/builded/bin
#CV_LIBS_POST = 245
#C:\Qt\3rdLibs\openCV\OpenCV-2.4.7\builded\install\x64\mingw\bin
CV_LIBS_PATH = -L../../../3rdLibs/openCV/OpenCV-2.4.7/builded/bin
CV_LIBS_POST = 247

#for vc series
#LIBS += -L../../3rdLibs/openCV/OpenCV-2.4.5/build/x86/vc11/lib -lopencv_core245 -lopencv_imgproc245
}
mac{

CONFIG -= app_bundle

DEFINES += MAC_OS

INCLUDEPATH += /usr/local/include

CV_LIBS_PATH = -L/usr/local/lib/
CV_LIBS_POST = .2.4.5
}

#LIBS += $$CV_LIBS_PATH -lopencv_calib3d$$CV_LIBS_POST
#LIBS += $$CV_LIBS_PATH -lopencv_contrib$$CV_LIBS_POST
LIBS += $$CV_LIBS_PATH -lopencv_core$$CV_LIBS_POST
#LIBS += $$CV_LIBS_PATH -lopencv_features2d$$CV_LIBS_POST
#LIBS += $$CV_LIBS_PATH -lopencv_flann$$CV_LIBS_POST
#LIBS += $$CV_LIBS_PATH -lopencv_gpu$$CV_LIBS_POST
LIBS += $$CV_LIBS_PATH -lopencv_highgui$$CV_LIBS_POST
LIBS += $$CV_LIBS_PATH -lopencv_imgproc$$CV_LIBS_POST
#LIBS += $$CV_LIBS_PATH -lopencv_legacy$$CV_LIBS_POST
#LIBS += $$CV_LIBS_PATH -lopencv_ml$$CV_LIBS_POST
#LIBS += $$CV_LIBS_PATH -lopencv_nonfree$$CV_LIBS_POST
#LIBS += $$CV_LIBS_PATH -lopencv_objdetect$$CV_LIBS_POST
#LIBS += $$CV_LIBS_PATH -lopencv_ocl$$CV_LIBS_POST
#LIBS += $$CV_LIBS_PATH -lopencv_photo$$CV_LIBS_POST
#LIBS += $$CV_LIBS_PATH -lopencv_stitching$$CV_LIBS_POST
#LIBS += $$CV_LIBS_PATH -lopencv_superres$$CV_LIBS_POST
#LIBS += $$CV_LIBS_PATH -lopencv_ts$$CV_LIBS_POST
LIBS += $$CV_LIBS_PATH -lopencv_video$$CV_LIBS_POST
#LIBS += $$CV_LIBS_PATH -lopencv_videostab$$CV_LIBS_POST

INCLUDEPATH += ../../commercial_libs/algorithms
INCLUDEPATH += ../../commercial_libs/imageAlgorithm
INCLUDEPATH += ../../libs
DEFINES += DISABLED_QT

# The .cpp file which was generated for your project. Feel free to hack it.
SOURCES += main.cpp \
    ../../commercial_libs/qmlHelper/file/fileProcessConstant.cpp \
    ../../commercial_libs/qmlHelper/file/fileProcess.cpp \       
    ../../commercial_libs/imageAlgorithm/utility/removeContours.cpp \
    ../../commercial_libs/imageAlgorithm/utility/drawRect.cpp \
    ../../commercial_libs/imageAlgorithm/genericAlgo/genericTransform.cpp \    
    smokeDetectorPolicy/backGroundSubtract/backGroundSubtractorIVSS.cpp \
    smokeDetectorPolicy/utility/weberBlobClassification.cpp \
    smokeDetectorPolicy/structure/structureIVSS.cpp \
    smokeDetectorPolicy/utility/dwt2DEnergyRatio.cpp \
    smokeDetectorPolicy/utility/contoursExtraction.cpp \
    smokeDetectorPolicy/utility/splitRect.cpp

# Installation path
# target.path =

# Please do not modify the following two lines. Required for deployment.
include(qtquick2applicationviewer/qtquick2applicationviewer.pri)
qtcAddDeployment()

HEADERS += \
    ../../commercial_libs/qmlHelper/file/fileProcessConstant.hpp \
    ../../commercial_libs/qmlHelper/file/fileProcess.hpp \    
    dwt.hpp \    
    ../../commercial_libs/algorithms/stlAlgoWrapper.hpp \
    ../../commercial_libs/imageAlgorithm/utility/removeContours.hpp \
    ../../commercial_libs/imageAlgorithm/utility/drawRect.hpp \
    ../../commercial_libs/imageAlgorithm/genericAlgo/genericForEach.hpp \
    ../../commercial_libs/imageAlgorithm/genericAlgo/genericTransform.hpp \
    smokeDetectorGeneric.hpp \   
    smokeDetectorPolicy/backGroundSubtract/backGroundSubtractorIVSS.hpp \
    smokeDetectorPolicy/utility/weberBlobClassification.hpp \
    smokeDetectorPolicy/preprocess/resizeAndBlur.hpp \
    smokeDetectorPolicy/structure/structureIVSS.hpp \
    smokeDetectorPolicy/detect/smokeDetectIVSS.hpp \
    smokeDetectorIVSS.hpp \
    smokeDetectorPolicy/utility/getSmokeArea.hpp \
    smokeDetectorCustom00.hpp \
    smokeDetectorPolicy/detect/smokeDetectCustom00.hpp \
    smokeDetectorPolicy/utility/dwt2DEnergyRatio.hpp \
    smokeDetectorPolicy/preprocess/resizeImage.hpp \
    smokeDetectorPolicy/structure/structureCustom00.hpp \
    smokeDetectorPolicy/utility/contoursExtraction.hpp \
    smokeDetectorPolicy/utility/splitRect.hpp

RESOURCES += \
    qml.qrc
