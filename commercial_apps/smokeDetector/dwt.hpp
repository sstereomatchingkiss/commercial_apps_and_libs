#ifndef DWT_HPP
#define DWT_HPP

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>

/**
 * @brief haar wavelet transform
 * @param src input image, only support single channel by now(CV_32FC1, CV_16U1, CV_8UC1 and so on, CV_8UC1 may not
 * gain reasonable result)
 * @param dst output image
 */
template<typename T>
void haar_wavelet(cv::Mat const &src, cv::Mat &dst)
{
    int const InnerHeight = src.rows / 2;
    int const InnerWidth = src.cols / 2;
    dst.create(InnerHeight * 2, InnerWidth * 2, src.type());

    for(int y = 0; y != InnerHeight; ++y){
        auto src_1_ptr = src.ptr<T>(2 * y);
        auto src_2_ptr = src.ptr<T>(2 * y + 1);
        auto dst_1_ptr = dst.ptr<T>(y);
        auto dst_2_ptr = dst.ptr<T>(y + InnerHeight);
        for(int x = 0; x != InnerWidth; ++x){
            T const p00 = src_1_ptr[2 * x];
            T const p01 = src_1_ptr[2 * x + 1];
            T const p10 = src_2_ptr[2 * x];
            T const p11 = src_2_ptr[2 * x + 1];

            //c = (src.at<T>(2*y, 2*x) + src.at<T>(2*y, 2*x+1) + src.at<T>(2*y+1,2*x) + src.at<T>(2*y+1,2*x+1)) * 0.5;
            dst_1_ptr[x] = p00 + p01 + p10 + p11;

            //dh = (src.at<T>(2*y, 2*x) + src.at<T>(2*y+1,2*x) - src.at<T>(2*y, 2*x+1) - src.at<T>(2*y+1,2*x+1)) * 0.5;
            dst_1_ptr[x + InnerWidth] = p00 - p01 + p10 - p11;

            //dv = (src.at<T>(2*y,2*x) + src.at<T>(2*y,2*x+1) - src.at<T>(2*y+1,2*x) - src.at<T>(2*y+1, 2*x+1)) * 0.5;
            dst_2_ptr[x] = p00 + p01 - p10 - p11;

            //dd = (src.at<T>(2*y, 2*x) - src.at<T>(2*y, 2*x+1) - src.at<T>(2*y+1, 2*x) + src.at<T>(2*y+1, 2*x+1)) * 0.5;
            dst_2_ptr[x + InnerWidth] = p00 - p01 - p10 + p11;
        }
    }

    dst *= 0.5;
}

#endif // DWT_HPP
