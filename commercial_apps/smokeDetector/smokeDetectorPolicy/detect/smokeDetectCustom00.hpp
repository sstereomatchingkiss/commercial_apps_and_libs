#ifndef SMOKEDETECTCUSTOM00_HPP
#define SMOKEDETECTCUSTOM00_HPP

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/video/background_segm.hpp>

#include "../preprocess/resizeImage.hpp"
#include "../utility/contoursExtraction.hpp"
#include "../utility/dwt2DEnergyRatio.hpp"
#include "../utility/splitRect.hpp"

/**
 *@brief try to mix different algorithms study from papers to detect smoke
 */
template<typename Structure>
class smokeDetectCustom00
{
    using ContourType = std::vector<std::vector<cv::Point>>;
public:
    explicit smokeDetectCustom00(Structure &structure) :
        preprocess_(structure),
        structure_(structure){}

    smokeDetectCustom00(smokeDetectCustom00 const&) = delete;
    smokeDetectCustom00& operator=(smokeDetectCustom00 const&) = delete;

    bool has_smoke(cv::Mat const &input);

private:
    void energy_ratio_classification(cv::Mat const &input);

    void split_rect();

private:
    cv::Mat background_;
    cv::BackgroundSubtractorMOG2 bg_subtract_;
    ContourType contours_;
    contoursExtraction contours_extract_;
    dwt2DEnergyRatio energy_ratio_;
    cv::Mat foreground_mask_;
    resizeImage<Structure> preprocess_;
    splitRect split_rect_;

    Structure &structure_;

    cv::Mat input_float_;
    cv::Mat input_gray_;
    cv::Mat background_float_;
    cv::Mat background_gray_;

    //cv::Mat wavelet_energy_2d_;
};

/**
 *@brief detect the input image has smoke or not, will save the smoke regions if any
 *@param input input image
 */
template<typename Structure>
bool smokeDetectCustom00<Structure>::has_smoke(cv::Mat const &input)
{
    //cv::imshow("current frame", input);

    preprocess_.preprocess(input);

    //1 : foreground background extraction
    bg_subtract_(structure_.resize_, foreground_mask_);
    //cv::imshow("fore ground", foreground_mask_);
    //cv::waitKey(30);

    //2 : exclude small contours
    contours_extract_.extract_contours(foreground_mask_, contours_);

    //3 : generate bounding rects
    contours_extract_.generate_bounding_rects(contours_, structure_.bounding_rect_);

    if(!contours_.empty()){
        cv::cvtColor(input, input_gray_, CV_BGR2GRAY);
        //4 : split big rects to small rects
        split_rect();

        //5 : classify the input has smoke or not by energy ratio
        energy_ratio_classification(input);
    }

    return false;
}

/**
 *@brief classify the input image has smoke or not by energy ratio criteria
 * mentioned by the paper "smoke detection using spatial and temporal analyses"
 *@param input input image
 */
template<typename Structure>
void smokeDetectCustom00<Structure>::energy_ratio_classification(cv::Mat const &input)
{
    //float big_ratio = 0;
    //float small_ratio = 0;
    bg_subtract_.getBackgroundImage(background_);
    cv::cvtColor(background_, background_gray_, CV_BGR2GRAY);
    input_gray_.convertTo(input_float_, CV_32FC1);
    background_gray_.convertTo(background_float_, CV_32FC1);
    //1 : verify it is smoke or not by 2d spatial wavelet analysis
    for(auto &rect : structure_.bounding_rect_){
        double const FrontEnergy = energy_ratio_.haar_wavelet_energy(cv::Mat(input_float_, rect));
        double const BackEnergy = energy_ratio_.haar_wavelet_energy(cv::Mat(background_float_, rect));

        double const EnergyRatio = BackEnergy != 0 ? FrontEnergy / BackEnergy :
                                                     9999;
        //std::cout<<"energy ratio : "<<EnergyRatio<<std::endl;

        //if(EnergyRatio < 1.0){
            //++small_ratio;
            //std::cout<<"energy ratio < 1.0 "<<EnergyRatio<<std::endl;
        //}

        //exclude the region with high energy ratio
        if(EnergyRatio >= 1.0){
            //++big_ratio;
            //std::cout<<"energy ratio >= "<<EnergyRatio<<std::endl;
            rect.width = 0;
            rect.height = 0;
        }

        //std::cout<<"small ratio = "<<small_ratio<<", big ratio = "<<big_ratio<<std::endl;
        //std::cout<<"energy ratio : "<<(small_ratio) / (small_ratio + big_ratio)<<std::endl;
    }
}

/**
 *@brief split big rectangles into small rectangles
 */
template<typename Structure>
void smokeDetectCustom00<Structure>::split_rect()
{
    std::vector<cv::Rect> temp = std::move(structure_.bounding_rect_);
    split_rect_.split(input_gray_, 25, 25, temp, structure_.bounding_rect_);
}

#endif // SMOKEDETECTCUSTOM00_HPP
