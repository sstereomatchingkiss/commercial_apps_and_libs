#ifndef SMOKEDETECTIVSS_HPP
#define SMOKEDETECTIVSS_HPP

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "utility/removeContours.hpp"

#include "../backGroundSubtract/backGroundSubtractorIVSS.hpp"
#include "../preprocess/resizeAndBlur.hpp"
#include "../structure/structureIVSS.hpp"
#include "../utility/weberBlobClassification.hpp"

/**
 * @brief encapsulate the smoke detection algorithm described by the paper
 * "smoke detection algorithm for intelligent video surveillance system"
 */
template<typename Structure>
class smokeDetectIVSS
{
public:
    explicit smokeDetectIVSS(Structure &data) : preprocess_(data), structure_(data) {}
    smokeDetectIVSS(smokeDetectIVSS const&) = delete;
    smokeDetectIVSS& operator=(smokeDetectIVSS const&) = delete;

    bool has_smoke(cv::Mat const &input);

    void set_min_countour(double size)
    {
        structure_.min_countour_size_ = size;
    }

private:
    backGroundSubtractorIVSS bg_subtract_;
    weberBlobClassification bl_classify_;
    resizeAndBlur<Structure> preprocess_;

    Structure &structure_;
};

template<typename Structure>
bool smokeDetectIVSS<Structure>::has_smoke(cv::Mat const &input)
{
    constexpr float PercentageOfBlock = 20;
    constexpr float WeberContrast = 0.5;

    preprocess_.preprocess(input);

    bg_subtract_(structure_.resize_, structure_.foreground_mask_);

    //connected component analysis
    cv::morphologyEx(structure_.foreground_mask_, structure_.foreground_mask_, cv::MORPH_OPEN, structure_.StructuringElement_);
    cv::morphologyEx(structure_.foreground_mask_, structure_.foreground_mask_, cv::MORPH_CLOSE, structure_.StructuringElement_);

    structure_.foreground_mask_.copyTo(structure_.mask_buffer_);
    cv::findContours(structure_.mask_buffer_, structure_.contours_, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_NONE);
    OCV::remove_contours(structure_.contours_, structure_.min_countour_size_, structure_.MaxContourSize_);

    structure_.bounding_rect_.clear();
    for(auto const &data : structure_.contours_){
        structure_.bounding_rect_.emplace_back(cv::boundingRect(data));
    }

    cv::Mat background;
    bg_subtract_.getBackgroundImage(background);

    cv::Mat frame_current;
    cv::Mat frame_previous;
    bg_subtract_.get_foreground_current(frame_current);
    bg_subtract_.get_foreground_previous(frame_previous);

    float const p = bl_classify_.measure_similarity(frame_previous, frame_current);
    if(p > PercentageOfBlock){
        auto it = std::partition(std::begin(structure_.bounding_rect_), std::end(structure_.bounding_rect_), [&](cv::Rect const &rect)
        {
                return bl_classify_.weber_contrast(cv::Mat(frame_current, rect), cv::Mat(background, rect)) > WeberContrast;
        });
        structure_.bounding_rect_.erase(it, std::end(structure_.bounding_rect_));
    }

    return !structure_.bounding_rect_.empty();
}

#endif // SMOKEDETECTIVSS_HPP
