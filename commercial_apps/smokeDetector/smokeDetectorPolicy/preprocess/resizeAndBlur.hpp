#ifndef RESIZEANDBLUR_HPP
#define RESIZEANDBLUR_HPP

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>

/**
 * @brief encapsulate the preprocess algorithm described by the paper
 * "smoke detection algorithm for intelligent video surveillance system"
 */
template<typename Structure>
class resizeAndBlur
{
public:
    explicit resizeAndBlur(Structure &data) : structure_(data) {}
    resizeAndBlur(resizeAndBlur const&) = delete;
    resizeAndBlur& operator=(resizeAndBlur const&) = delete;

    void preprocess(cv::Mat const &input);

private:
    Structure &structure_;
};

template<typename Structure>
void resizeAndBlur<Structure>::preprocess(const cv::Mat &input)
{
    constexpr int   GKernalSize = 7;
    constexpr float MaxWidth = 320;

    cv::cvtColor(input, structure_.equalize_, CV_BGR2GRAY);
    cv::equalizeHist(structure_.equalize_, structure_.equalize_);

    //the paper suggest dwt to haar basis, but openCV do not support it yet
    //this is a fast and dirty solution compare to dwt
    if(input.cols > MaxWidth){
        float const ratio = MaxWidth / input.cols;
        cv::resize(structure_.equalize_, structure_.resize_,
        {static_cast<int>(input.cols * ratio),
         static_cast<int>(input.rows * ratio)});
    }else{
        structure_.resize_ = structure_.equalize_;
    }

    cv::GaussianBlur(structure_.resize_, structure_.resize_, {GKernalSize, GKernalSize}, 0);
}

#endif // RESIZEANDBLUR_HPP
