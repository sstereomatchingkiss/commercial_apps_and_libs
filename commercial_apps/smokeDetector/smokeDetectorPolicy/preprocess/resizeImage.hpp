#ifndef RESIZEIMAGE_HPP
#define RESIZEIMAGE_HPP

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>

/**
 * @brief shrink the image into approriate size
 * @param 1 : should provide a cv::Mat which called "resize_", the image after
 * resize will save into this Mat;
 * 2 : should provide a cv::Mat which called "gray_"
 */
template<typename Structure>
class resizeImage
{
public:
    resizeImage(Structure &structure) : structure_(structure) {}

    void preprocess(cv::Mat const &input);

private:
    Structure &structure_;
};

/**
 *@brief change the input to gray and resize it
 *@param input input image, expect to be a bgr image
 */
template<typename Structure>
void resizeImage<Structure>::preprocess(cv::Mat const &input)
{
    constexpr float MaxWidth = 320;

    //cv::cvtColor(input, structure_.gray_, CV_BGR2GRAY);

    if(input.cols > MaxWidth){
        float const ratio = MaxWidth / input.cols;
        cv::resize(input, structure_.resize_,
        {static_cast<int>(input.cols * ratio),
         static_cast<int>(input.rows * ratio)});
    }else{
        structure_.resize_ = input;
    }
}

#endif // RESIZEIMAGE_HPP
