#include "structureIVSS.hpp"

#include <limits>

#include <opencv2/imgproc/imgproc.hpp>

namespace
{

constexpr int   SEKernalSize = 3; //structuring element kernal size

}

StructIVSS::StructIVSS() : MaxContourSize_(std::numeric_limits<double>::max()),
min_countour_size_(2000),
StructuringElement_(cv::getStructuringElement(cv::MORPH_RECT, {SEKernalSize, SEKernalSize}))
{

}
