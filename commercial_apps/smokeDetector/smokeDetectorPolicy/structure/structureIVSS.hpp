#ifndef STRUCTUREIVSS_HPP
#define STRUCTUREIVSS_HPP

#include <vector>

#include <opencv2/core/core.hpp>

/**
 * @brief This structure encapsulate the data member needed to implement the algorithm
 * by the paper "smoke detection algorithm for intelligent video surveillance system"
 */
struct StructIVSS final
{
    using ContourType = std::vector<std::vector<cv::Point>>;

    StructIVSS();
    StructIVSS(StructIVSS const &) = delete;
    StructIVSS& operator=(StructIVSS const &) = delete;

    std::vector<cv::Rect> bounding_rect_;
    ContourType contours_;

    double const MaxContourSize_;
    double min_countour_size_; //minimum size of the contour

    cv::Mat const StructuringElement_;

    cv::Mat equalize_;
    cv::Mat foreground_mask_;
    cv::Mat mask_buffer_;
    cv::Mat resize_;
};

#endif // STRUCTUREIVSS_HPP
