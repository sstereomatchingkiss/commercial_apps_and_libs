#ifndef STRUCTURECUSTOM00_HPP
#define STRUCTURECUSTOM00_HPP

#include <vector>

#include <opencv2/core/core.hpp>

/**
 * @brief This structure encapsulate the data member needed to implement the algorithm
 * by the paper "smoke detection algorithm for intelligent video surveillance system"
 */
struct StructCustom00 final
{
    StructCustom00() = default;
    StructCustom00(StructCustom00 const &) = delete;
    StructCustom00& operator=(StructCustom00 const &) = delete;

    std::vector<cv::Rect> bounding_rect_;

    cv::Mat gray_;
    cv::Mat resize_;
};

#endif // STRUCTURECUSTOM00_HPP
