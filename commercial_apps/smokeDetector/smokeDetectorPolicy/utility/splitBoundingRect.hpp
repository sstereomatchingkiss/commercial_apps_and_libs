#ifndef SPLITBOUNDINGRECT_HPP
#define SPLITBOUNDINGRECT_HPP

#include <opencv2/core/core.hpp>

/**
 * @brief split the rect to many small rectangles
 */
class splitBoundingRect
{
public:
    splitBoundingRect();

    void set_size(cv::Size const &size);
    std::vector<cv::Rect>& split_and_get(cv::Rect const &rect);

private:
    std::vector<cv::Rect> rects_;
    cv::Size size_;
};

#endif // SPLITBOUNDINGRECT_HPP
