#include "dwt2DEnergyRatio.hpp"

#include <genericAlgo/genericForEach.hpp>

namespace{

/**
 * @brief haar wavelet transform without processing the LL Band
 * @param src input image, only support single channel by now(CV_32FC1, CV_16U1, CV_8UC1 and so on, CV_8UC1 may not
 * gain reasonable result)
 * @param dst output image
 */
template<typename T>
void haar_wavelet_highband(cv::Mat const &src, cv::Mat &dst)
{
    int const InnerHeight = src.rows / 2;
    int const InnerWidth = src.cols / 2;
    dst.create(InnerHeight * 2, InnerWidth * 2, src.type());

    for(int y = 0; y != InnerHeight; ++y){
        auto src_1_ptr = src.ptr<T>(2 * y);
        auto src_2_ptr = src.ptr<T>(2 * y + 1);
        auto dst_1_ptr = dst.ptr<T>(y);
        auto dst_2_ptr = dst.ptr<T>(y + InnerHeight);
        for(int x = 0; x != InnerWidth; ++x){
            T const p00 = src_1_ptr[2 * x];
            T const p01 = src_1_ptr[2 * x + 1];
            T const p10 = src_2_ptr[2 * x];
            T const p11 = src_2_ptr[2 * x + 1];

            //dh
            dst_1_ptr[x + InnerWidth] = p00 - p01 + p10 - p11;

            //dv
            dst_2_ptr[x] = p00 + p01 - p10 - p11;

            //dhv
            dst_2_ptr[x + InnerWidth] = p00 - p01 - p10 + p11;
        }
    }

    dst *= 0.5;
}

}

/**
 * @brief calculate the energy of the 2d spatial haar wavelet transform(H, V, HV)
 * @param input  input image, only support CV_32FC1 by now
 * @return energy of the 2d spatial haar wavelet transform(H, V, HV)
 */
double dwt2DEnergyRatio::haar_wavelet_energy(cv::Mat const &input)
{       
    CV_Assert(input.type() == CV_32FC1);

    input.copyTo(haar_wavelet_);

    haar_wavelet_highband<float>(input, haar_wavelet_);

    int const bandWidth = haar_wavelet_.cols / 2;
    int const bandHeight = haar_wavelet_.rows / 2;

    double const EH = calculate_energy(cv::Mat(input, cv::Rect(bandWidth, 0, bandWidth, bandHeight)));
    double const EV = calculate_energy(cv::Mat(input, cv::Rect(0, bandHeight, bandWidth, bandHeight)));
    double const ED = calculate_energy(cv::Mat(input, cv::Rect(bandWidth, bandHeight, bandWidth, bandHeight)));

    return EH + EV + ED;
}

/**
 * @brief  get the haar image
 * @return haar image
 */
cv::Mat dwt2DEnergyRatio::get_haar_image() const
{
    return haar_wavelet_;
}

/**********************************************************
 ****************** implementation ************************
 **********************************************************/

/**
 * @brief find the energy of the input
 * @param input input image
 * @return energy of the block
 */
double dwt2DEnergyRatio::calculate_energy(cv::Mat const &input) const
{
    double sum = 0;
    OCV::for_each_channels<float>(input, [&](float a)
    {
        sum += a * a;
    });

    return sum;
}
