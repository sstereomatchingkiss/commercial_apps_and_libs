#include "contoursExtraction.hpp"

#include <opencv2/imgproc/imgproc.hpp>

#include <limits>

#include "utility/removeContours.hpp"

namespace
{

constexpr int SEKernalSize = 3; //structuring element kernal size
constexpr double MinContourSize = 500;

}

contoursExtraction::contoursExtraction() :
    MaxContourSize_(std::numeric_limits<double>::max()),
    StructuringElement_(cv::getStructuringElement(cv::MORPH_RECT, {SEKernalSize, SEKernalSize}))
{
}

/**
 * @brief extract contours from input image
 * @param input input image, this image will be altered
 * @param contours found countours
 */
void contoursExtraction::extract_contours(cv::Mat &input, std::vector<std::vector<cv::Point>> &contours)
{
    //connected component analysis
    cv::morphologyEx(input, input, cv::MORPH_OPEN, StructuringElement_);
    cv::morphologyEx(input, input, cv::MORPH_CLOSE, StructuringElement_);

    cv::findContours(input, contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_NONE);
    OCV::remove_contours(contours, MinContourSize, MaxContourSize_);
}

/**
 * @brief generate bounding rect which cover the contours
 * @param contours : input contours
 * @param bounding_rects : bounding rects which cover the contours
 */
void contoursExtraction::generate_bounding_rects(std::vector<std::vector<cv::Point>> const &contours,
                                                 std::vector<cv::Rect> &bounding_rects)
{
    bounding_rects.clear();
    for(auto const &data : contours){
        bounding_rects.emplace_back(cv::boundingRect(data));
    }
}
