#ifndef GETSMOKEAREA_HPP
#define GETSMOKEAREA_HPP

#include <vector>

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <algorithm>
//#include <iostream>
#include <iterator>

#include "utility/drawRect.hpp"

/**
 *@brief get the smoke region of the frame
 */
template<typename Structure>
class getSmokeArea
{
public:
    explicit getSmokeArea(Structure &structure) : structure_(structure) {}

    void draw_detected_smoke(cv::Mat &input) const;

    void smoke_region(std::vector<cv::Rect> &rects) const;
    std::vector<cv::Rect> const& smoke_region_ref() const;

private:
    Structure &structure_;
};

template<typename Structure>
void getSmokeArea<Structure>::draw_detected_smoke(cv::Mat &input) const
{
    OCV::draw_rects(input, structure_.bounding_rect_);
}

template<typename Structure>
void getSmokeArea<Structure>::smoke_region(std::vector<cv::Rect> &rects) const
{
    rects.clear();
    std::copy(std::begin(structure_.bounding_rect_), std::end(structure_.bounding_rect_), std::back_inserter(rects));
}

template<typename Structure>
std::vector<cv::Rect> const& getSmokeArea<Structure>::smoke_region_ref() const
{
    return structure_.bounding_rect_;
}

#endif // GETSMOKEAREA_HPP
