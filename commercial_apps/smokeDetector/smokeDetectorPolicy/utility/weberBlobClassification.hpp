#ifndef WEBERBLOBCLASSIFICATION_HPP
#define WEBERBLOBCLASSIFICATION_HPP

#include <opencv2/core/core.hpp>

/**
 * @brief encapsulate the blob classfication algorithm described by the paper
 * "smoke detection algorithm for intelligent video surveillance system"
 */
class weberBlobClassification final
{
public:
    weberBlobClassification();

    weberBlobClassification(weberBlobClassification const&) = delete;
    weberBlobClassification& operator=(weberBlobClassification const&) = delete;

    float measure_similarity(cv::Mat const &frame_previous, cv::Mat const &frame_current) const;

    float weber_contrast(cv::Mat const &frame_current, cv::Mat const &back_ground) const;
};

#endif // WEBERBLOBCLASSIFICATION_HPP
