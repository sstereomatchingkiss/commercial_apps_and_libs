#include "weberBlobClassification.hpp"

#include <algorithm>

#include <genericAlgo/genericForEach.hpp>

namespace{

constexpr int Step = 2;

/**
 * measure the similarity between a and b(0~1)
 * the bigger the return value, the higher the similarity
 */
template<typename T>
inline
float similarity(T a, T b)
{
    return a > b ? static_cast<float>(b) / a :
                   a < b ? static_cast<float>(a) / b :
                           1;
}

/**
 * find the maximum similarity between pixel of previous frame and current frame
 */
template<typename T>
inline float max_similarity(T frame_pre, T const *const frame_next)
{
    return std::max(std::max(similarity(frame_pre, frame_next[-1]), similarity(frame_pre, frame_next[0])),
            similarity(frame_pre, frame_next[1]));
}

}

weberBlobClassification::weberBlobClassification()
{
}

/**
 * @brief  measure the similarity between previous frame and current frame
 * @param  frame_previous previous frame, should be CV_8CU1
 * @param  frame_current  current frame, should be CV_8CU1
 * @return similarity percentage, if > 0.2, we classify it is a smoke candidate
 */
float weberBlobClassification::measure_similarity(cv::Mat const &frame_previous, cv::Mat const &frame_current) const
{
    using Type = uchar;

    int const RowSize = (frame_previous.rows - 1) / Step;
    int const ColSize = (frame_previous.cols - 1) / Step;
    float cs = 0;

    for(int rows = 1; rows < RowSize; rows += Step){
        Type const *frame_pre_ptr = frame_previous.ptr<Type>(rows);
        Type const *frame_cur_ptr = frame_current.ptr<Type>(rows - 1);
        for(int cols = 1; cols < ColSize; cols += Step){
            cs += max_similarity(frame_pre_ptr[cols], frame_cur_ptr + cols);
        }
    }

    return cs / (RowSize * ColSize) * 100.0;
}

/**
 * @brief find weber contrast
 * @param frame_current fore ground, should be CV_8CU1
 * @param back_ground   back ground, should be CV_8CU1
 * @return contrast between two frames
 */
float weberBlobClassification::weber_contrast(cv::Mat const &frame_current, cv::Mat const &back_ground) const
{
    using Type = uchar;

    float cw = 0;
    OCV::for_each_channels<Type>(frame_current, back_ground, [&](float f, Type b)
    {
        cw += b != 0 ? (f - b) / b : f;
    });

    return cw / frame_current.total();
}
