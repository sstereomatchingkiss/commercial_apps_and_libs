#ifndef SPLITRECT_HPP
#define SPLITRECT_HPP

#include <opencv2/core/core.hpp>

#include <vector>

/**
 * @brief split the cv::Rect into small rects
 */
class splitRect
{
public:
    splitRect();

    std::vector<cv::Rect>& get_rects();

    std::vector<cv::Rect>& split(cv::Mat const &input, int width, int height);
    void split(cv::Mat const &input, int width, int height, std::vector<cv::Rect> const &big_rects, std::vector<cv::Rect> &small_rects);

private:
    std::vector<cv::Rect> rects_;
};

#endif // SPLITRECT_HPP
