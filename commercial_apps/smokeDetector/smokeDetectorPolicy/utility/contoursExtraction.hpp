#ifndef CONTOURSEXTRACTION_HPP
#define CONTOURSEXTRACTION_HPP

#include <opencv2/core/core.hpp>

#include <vector>

/**
 * @brief
 */
class contoursExtraction
{
public:
    contoursExtraction();

    void extract_contours(cv::Mat &input, std::vector<std::vector<cv::Point>> &contours);
    void generate_bounding_rects(std::vector<std::vector<cv::Point>> const &contours,
                                 std::vector<cv::Rect> &bounding_rects);

private:
    double const MaxContourSize_;
    cv::Mat const StructuringElement_;
};

#endif // CONTOURSEXTRACTION_HPP
