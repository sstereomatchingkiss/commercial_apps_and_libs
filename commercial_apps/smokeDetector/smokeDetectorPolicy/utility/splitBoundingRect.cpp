#include "splitBoundingRect.hpp"

splitBoundingRect::splitBoundingRect()
{
}

void splitBoundingRect::set_size(cv::Size const &size)
{
    size_ = size;
}

/**
 * @brief split the rect to many small rectangles
 * @param rect input rect need to be split
 * @return the rect after split
 */
std::vector<cv::Rect>& splitBoundingRect::split_and_get(cv::Rect const &rect)
{
    rects_.clear();

    if(size_.width <= rect.width && size_.height <= rect.height){
        int const Width = rect.width / size_.width;
        int const Height = rect.height / size_.height;

        for(int h = 0; h < Height; h += size_.height){
            for(int w = 0; w < Width; w += size_.width){
                rects_.emplace_back({rect.x + w, rect.height + h, size_.width, size_.height});
            }
        }
    }

    return rects_;
}
