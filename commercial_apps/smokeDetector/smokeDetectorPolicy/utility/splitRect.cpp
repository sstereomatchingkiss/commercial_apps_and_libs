#include "splitRect.hpp"

splitRect::splitRect()
{
}

/**
 * @brief   get the rectangles after split
 * @return  return the rectangles after split
 */
std::vector<cv::Rect>& splitRect::get_rects()
{
    return rects_;
}

/**
 * @brief  split  the input to small rectangles with size == width * height
 * @param  input  channel must be CV_8U
 * @param  width  width of the split rectangle(must > 0)
 * @param  height height of the split rectangle(must > 0)
 * @return return the rectangles after split
 * @warning this class do not perform any error handles, you must make sure your inputs
 * are valid by yourself
 */
std::vector<cv::Rect>& splitRect::split(cv::Mat const &input, int width, int height)
{
    int const RectHeight = input.rows / height;
    int const RectWidth = input.cols / width;

    rects_.resize(RectHeight * RectWidth);
    auto *rect_ptr = &rects_[0];
    int y_step = 0;
    for(int row = 0; row != RectHeight; ++row){
        int x_step = 0;
        for(int col = 0; col != RectWidth; ++col){
            rect_ptr->x = x_step;
            rect_ptr->y = y_step;
            rect_ptr->width = width;
            rect_ptr->height = height;
            ++rect_ptr;
            x_step += width;
        }
        y_step += height;
    }

    return rects_;
}

/**
 * @brief split  split the big rects into small rects
 * @param input  input image, the channel must be CV_8U
 * @param width  width of the split rectangle(must > 0)
 * @param height height of the split rectangle(must > 0)
 * @param small_rects small rectangles
 * @param big_rects   big rectangles
 */
void splitRect::split(cv::Mat const &input, int width, int height, std::vector<cv::Rect> const &big_rects, std::vector<cv::Rect> &small_rects)
{
    small_rects.clear();
    for(auto const &rect : big_rects){
        split(cv::Mat(input, rect), width, height);
        for(auto const &data : rects_){
            small_rects.emplace_back(data);
        }
    }
}
