#ifndef DWT2DENERGYRATIO_HPP
#define DWT2DENERGYRATIO_HPP

#include <opencv2/core/core.hpp>

namespace cv{

class Mat;

}

class dwt2DEnergyRatio
{
public:
    dwt2DEnergyRatio() = default;
    dwt2DEnergyRatio(dwt2DEnergyRatio const&) = delete;
    dwt2DEnergyRatio& operator=(dwt2DEnergyRatio const&) = delete;

    double haar_wavelet_energy(cv::Mat const &input);
    cv::Mat get_haar_image() const;

private:
    double calculate_energy(cv::Mat const &input) const;

private:
    cv::Mat haar_wavelet_;
};

#endif // DWT2DENERGYRATIO_HPP
