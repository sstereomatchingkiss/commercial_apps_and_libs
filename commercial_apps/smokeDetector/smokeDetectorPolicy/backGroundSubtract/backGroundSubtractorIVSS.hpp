#ifndef BACKGROUNDSUBTRACTORIVSS_HPP
#define BACKGROUNDSUBTRACTORIVSS_HPP

#include <opencv2/core/core.hpp>

/**
 * @brief encapsulate the background subtraction algorithm described by the paper
 * "smoke detection algorithm for intelligent video surveillance system"
 */
class backGroundSubtractorIVSS final
{
public:
    backGroundSubtractorIVSS();
    void operator()(cv::InputArray image, cv::OutputArray fg_mask, double learningRate = 0);
    void getBackgroundImage(cv::OutputArray background_image) const;
    void get_foreground_current(cv::Mat &fore_ground) const;
    void get_foreground_previous(cv::Mat &fore_ground) const;

    backGroundSubtractorIVSS(backGroundSubtractorIVSS const&) = delete;
    backGroundSubtractorIVSS& operator=(backGroundSubtractorIVSS const&) = delete;

private:
    void get_background();
    void get_foreground();
    void get_threshold();

    void set_background_and_frame(cv::Mat const &input, cv::Mat &frame);

private:
    cv::Mat background_current_; //time == t
    cv::Mat background_output_; //time == t
    cv::Mat background_previous_; //time == t - 1

    cv::Mat foreground_mask_; //time == t + 1
    cv::Mat foreground_current_; //time == t + 1
    cv::Mat foreground_previous_; //time == t
    cv::Mat frame_pre_3_; //time == t - 2
    cv::Mat frame_pre_2_; //time == t - 1
    cv::Mat frame_pre_1_; //time == t
    cv::Mat frame_current_; //current frame, time == t + 1

    size_t image_size_;

    cv::Mat resize_;

    cv::Mat threshold_current_;
    cv::Mat threshold_previous_;
};

#endif // BACKGROUNDSUBTRACTORIVSS_HPP
