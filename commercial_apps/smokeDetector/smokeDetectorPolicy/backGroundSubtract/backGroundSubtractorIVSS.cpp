#include "backGroundSubtractorIVSS.hpp"

#include <opencv2/imgproc/imgproc.hpp>

namespace
{

constexpr float Alpha = 0.95;
constexpr float Beta = 0.38;

inline bool is_moving(int threshold, int frame_pre_1, int frame_pre_2, int frame_pre_3)
{
    return std::abs(frame_pre_1 - frame_pre_2) > threshold &&
           std::abs(frame_pre_1 - frame_pre_3) > threshold;
}

}

backGroundSubtractorIVSS::backGroundSubtractorIVSS() : image_size_(0)
{
}

/**
 * @brief separate foreground and background
 * @param image    current frame, should be single channel(CV_8U)
 * @param fg_mask  fore ground of the current frame. 255 == foreground, 0 == background
 */
void backGroundSubtractorIVSS::operator()(cv::InputArray image, cv::OutputArray fg_mask, double)
{
    cv::Mat const input = image.getMat();
    switch(image_size_){
    case 4 :{
        frame_pre_3_ = frame_pre_2_;
        frame_pre_2_ = frame_pre_1_;
        frame_pre_1_ = frame_current_;
        frame_current_ = input;

        threshold_current_.copyTo(threshold_previous_);
        //background_current_.copyTo(background_previous_);
        background_previous_ = background_current_;
        //threshold_previous_ = threshold_current_;

        get_background();
        get_threshold();
        get_foreground();
        break;
    }
    case 3 :{
        set_background_and_frame(input, frame_current_); //time == t + 1
        break;
    }
    case 2 :{
        set_background_and_frame(input, frame_pre_1_); //time == t
        break;
    }
    case 1 :{
        set_background_and_frame(input, frame_pre_2_); //time == t - 1
        break;
    }
    case 0 :{
        set_background_and_frame(input, frame_pre_3_); //time == t -2
        background_current_ = input.clone();
        background_previous_ = input.clone();
        threshold_current_.create(frame_pre_3_.size(), CV_8U);
        threshold_current_ = 245;
        threshold_previous_ = threshold_current_.clone();
        foreground_current_ = input.clone();
        foreground_previous_ = input.clone();
        foreground_mask_ = cv::Mat::zeros(input.size(), input.type());
        break;
    }
    default:
        break;
    }

    fg_mask.getMatRef() = foreground_mask_;
}

/**
 * @brief get the background of current frame
 * @param backgroundImage background
 */
void backGroundSubtractorIVSS::getBackgroundImage(cv::OutputArray background_image) const
{
    //cv::Mat background = background_image.getMatRef();
    //background_current_.copyTo(background);
    //background *= 255;
    background_image.getMatRef() = background_current_;
}

/**
 * @brief get fore ground, time frame == t + 1
 * @param fore_ground fore ground
 */
void backGroundSubtractorIVSS::get_foreground_current(cv::Mat &fore_ground) const
{
    fore_ground = foreground_current_;
}

/**
 * @brief get fore ground, time frame == t
 * @param fore_ground fore ground
 */
void backGroundSubtractorIVSS::get_foreground_previous(cv::Mat &fore_ground) const
{
    fore_ground = foreground_previous_;
}

/**********************************************************
 ****************** implementation ************************
 **********************************************************/

/**
 * @brief backGroundSubtractorSmoke::get_background
 */
void backGroundSubtractorIVSS::get_background()
{
    using TYPE = uchar;

    for(int rows = 0; rows != background_current_.rows; ++rows){
        auto bg_cur_ptr = background_current_.ptr<TYPE>(rows);
        auto bg_pre_ptr = background_previous_.ptr<TYPE>(rows);
        auto const frame_pre_1_ptr = frame_pre_1_.ptr<TYPE>(rows);
        auto const frame_pre_2_ptr = frame_pre_2_.ptr<TYPE>(rows);
        auto const frame_pre_3_ptr = frame_pre_3_.ptr<TYPE>(rows);
        auto th_cur_ptr = threshold_current_.ptr<TYPE>(rows);
        for(int cols = 0; cols != background_current_.cols; ++cols){
            if(is_moving(th_cur_ptr[cols], frame_pre_1_ptr[cols], frame_pre_2_ptr[cols], frame_pre_3_ptr[cols])){
                bg_cur_ptr[cols] = cv::saturate_cast<TYPE>(Alpha * bg_pre_ptr[cols] + (1 - Alpha) * frame_pre_2_ptr[cols]);
            }
        }
    }
}

/**
 * @brief generate foreground.
 *  foreground_current = (frame_current_ - (1 - Beta_) * background_current_) / Beta_;
 */
void backGroundSubtractorIVSS::get_foreground()
{
    //equal to foreground_current = (frame_current_ - (1 - Beta_) * background_current_) / Beta_;
    foreground_current_.copyTo(foreground_previous_);
    cv::addWeighted(frame_current_, 1 / Beta, background_current_, 1 - 1 / Beta, 0, foreground_current_);
    cv::threshold(foreground_current_, foreground_mask_, 245, 255, CV_THRESH_BINARY);
}

/**
 * @brief generate threshold
 */
void backGroundSubtractorIVSS::get_threshold()
{
    using TYPE = uchar;

    for(int rows = 0; rows != threshold_current_.rows; ++rows){
        auto bg_pre_ptr = background_previous_.ptr<TYPE>(rows);
        auto const frame_pre_1_ptr = frame_pre_1_.ptr<TYPE>(rows);
        auto const frame_pre_2_ptr = frame_pre_2_.ptr<TYPE>(rows);
        auto const frame_pre_3_ptr = frame_pre_3_.ptr<TYPE>(rows);
        auto th_cur_ptr = threshold_current_.ptr<TYPE>(rows);
        auto th_pre_ptr = threshold_previous_.ptr<TYPE>(rows);
        for(int cols = 0; cols != threshold_current_.cols; ++cols){
            if(is_moving(th_cur_ptr[cols], frame_pre_1_ptr[cols], frame_pre_2_ptr[cols], frame_pre_3_ptr[cols])){
                th_cur_ptr[cols] = cv::saturate_cast<TYPE>(
                            Alpha * th_pre_ptr[cols] +
                            (1 - Alpha) * 5 * ( std::abs(frame_pre_2_ptr[cols] - bg_pre_ptr[cols]) )
                            );
            }
        }
    }
}

/**
 * @brief set background and frame
 * @param input input image
 * @param frame the frame need to assign
 */
void backGroundSubtractorIVSS::set_background_and_frame(cv::Mat const &input, cv::Mat &frame)
{
    input.copyTo(frame);
    ++image_size_;
}
