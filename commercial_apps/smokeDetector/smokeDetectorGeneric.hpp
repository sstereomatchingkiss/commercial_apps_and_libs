#ifndef SMOKEDETECTORGENERIC_HPP
#define SMOKEDETECTORGENERIC_HPP

/**
 *@brief generic skeleton of smoke detector algorithms
 *@param Structure : data structure of the policies
 *@param DetectPolicy : implement the smoke detection algorithms.The api must provided are
 * 1 : constructor which accept the template parameter Structure(ex : smokeDetectIVSS.hpp)
 * 2 : bool has_smoke(cv::Mat const& input); //detect the input image has smoke or not
 *
 *@param UtilityPolicy : provide helper api(ex : getSmokeArea.hpp).The api must provided are
 * 1 : constructor which accept the template parameter Structure(ex : getSmokeArea.hpp)
 */
template<typename Structure,
         template<typename T> class DetectPolicy,
         template<typename T> class UtilityPolicy>
class smokeDetectorGeneric final : public DetectPolicy<Structure>, public UtilityPolicy<Structure>
{
public:
    smokeDetectorGeneric();

private:
    Structure structure_;
};

template<typename Structure,
         template<typename T> class DetectPolicy,
         template<typename T> class UtilityPolicy>
smokeDetectorGeneric<Structure, DetectPolicy, UtilityPolicy>::smokeDetectorGeneric() :
    DetectPolicy<Structure>(structure_),
    UtilityPolicy<Structure>(structure_)
{

}

#endif // SMOKEDETECTORGENERIC_HPP
