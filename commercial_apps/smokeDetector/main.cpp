#include <QtGui/QGuiApplication>
//#include <QApplication>
//#include "qtquick2applicationviewer.h"
#include <QQuickView>

#include <iomanip>
#include <iostream>
#include <string>
#include <strstream>
#include <vector>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/video/background_segm.hpp>

#include <QByteArray>
#include <QDebug>
#include <QCamera>
#include <QDir>
#include <QFile>
#include <QLabel>
#include <QMediaRecorder>
#include <QVideoWidget>

#include "dwt.hpp"
#include "smokeDetectorCustom00.hpp"
#include "smokeDetectorIVSS.hpp"
#include "smokeDetectorPolicy/utility/dwt2DEnergyRatio.hpp"

std::vector<std::string> get_paths()
{
    return {"visor_1196179837385_movie11_viper.mpg.flv",
        "visor_1196343040120_movie12_viper.mpg.flv",
        "visor_1196343142698_movie13_viper.mpg.flv",
        "visor_1196343179807_movie14_viper.mpg.flv",
        "visor_1197283980290_10_hangar.avi.mpg",
        "visor_1197283985149_02_explosion.avi",
        "visor_1197283985415_03_burnout_contest.avi.mpg",
        "visor_1197283990821_04_fumogeno1.avi.flv",
        "visor_1197284001212_05_fumogeno2.avi.flv",
        "visor_1197284006306_06_fumogeno3.avi.flv",
        "visor_1197284015696_07_fumogeno4.avi.flv",
        "visor_1197284021149_08_fumogeno5.avi.flv",
        "visor_1197284028899_09_fumogeno6.avi.flv",
        "visor_1197293099227_01_ballistic_output.avi"
    };
}

std::string prefix_path()
{
    return "C:/Qt/videos/";
}

void test_haar_wavelet_transform()
{
    std::string const prefix = prefix_path();
    std::vector<std::string> const paths = get_paths();

    constexpr size_t Index = 10;
    auto &path = paths[Index];
    cv::VideoCapture cap(prefix + path);
    //cv::VideoCapture cap(0);
    if(!cap.isOpened()){
        std::cout<<"capture can't open"<<std::endl;
        return;
    }
    cv::Mat current_frame;
    cv::Mat gray;
    cv::Mat haar_input;
    cv::Mat haar_output;
    cv::Mat output;
    dwt2DEnergyRatio energy_ratio;
    while(true){
        cap >> current_frame;
        if(current_frame.cols * current_frame.rows){
            cv::cvtColor(current_frame, gray, CV_BGR2GRAY);
            gray.convertTo(haar_input, CV_32FC1);
            haar_output.create(haar_input.size(), haar_input.type());
            haar_output.setTo(0);
            //haar_wavelet<float>(haar_input, haar_output);
            energy_ratio.haar_wavelet_energy(haar_input);
            cv::normalize(haar_output, output, 0, 255, CV_MINMAX, CV_8UC1);
            cv::imshow("haar wavelet output", output);
            int const key = cv::waitKey(30);
            if(key == 'e' || key == 'E'){
                break;
            }
        }
    }
    /*std::cout<<(prefix + "pngToVideo.txt")<<std::endl;
    //QFile file(QString::fromStdString(prefix + "pngToVideo.txt"));
    QFile file(QDir::currentPath() + "Makefile");
    std::cout<<file.isOpen()<<std::endl;
    qDebug()<<QDir::currentPath();*/
}

void detect_bg_mog()
{
    cv::Mat frame;
    cv::Mat back;
    cv::Mat fore;
    cv::VideoCapture cap(0);
    cv::BackgroundSubtractorMOG2 bg;

    std::vector<std::vector<cv::Point> > contours;

    while(true)
    {
        cap >> frame;
        if(frame.rows * frame.cols != 0){
            bg(frame, fore);
            bg.getBackgroundImage(back);
            cv::imshow("fore", fore);
            std::cout<<frame.size()<<", "<<back.size()<<std::endl;
            cv::erode(fore, fore, cv::Mat());
            cv::dilate(fore, fore, cv::Mat());
            cv::findContours(fore, contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_NONE);
            cv::drawContours(frame, contours, -1, cv::Scalar(0, 0, 255), 2);

            cv::imshow("Frame", frame);
            if(back.rows * back.cols != 0){
                cv::imshow("Background", back);
            }
            int const key = cv::waitKey(30);
            if(key == 'e' || key == 'E'){
                break;
            }
        }
        std::cout<<"enter loop"<<std::endl;
    }
}

void detect_smoke_00()
{
    std::string const prefix = prefix_path();
    std::vector<std::string> paths = get_paths();

    constexpr size_t Index = 10;
    auto &path = paths[Index];
    cv::VideoCapture cap(prefix + path);
    path.erase(std::begin(path) + path.find_first_of("."), std::end(path));
    path.insert(0, prefix + "recorded/");
    path += "_";
    cv::Mat current_frame;
    cv::Mat mask;
    smokeDetectorIVSS sm_detect;
    size_t frame = 0;
    while(cap.read(current_frame)){
        sm_detect.has_smoke(current_frame);
        current_frame.copyTo(mask);
        sm_detect.draw_detected_smoke(mask);

        //cv::imshow("frame", current_frame);
        cv::imshow("mask", mask);

        std::stringstream strs;
        strs << path << std::setw(6) << std::setfill('0') << frame++ << ".png";
        cv::imwrite(strs.str(), mask);
        //write << mask;

        int const key = cv::waitKey(30);
        if(key == 'e' || key == 'E'){
            break;
        }
    }
}

void detect_smoke_custom_00()
{
    std::string const prefix = prefix_path();
    std::vector<std::string> paths = get_paths();

    constexpr size_t Index = 10;
    auto &path = paths[Index];
    //cv::VideoCapture cap(prefix + path);
    cv::VideoCapture cap(0);
    path.erase(std::begin(path) + path.find_first_of("."), std::end(path));
    path.insert(0, prefix + "recorded/");
    path += "_";
    cv::Mat current_frame;
    cv::Mat mask;
    smokeDetectorCustom00 sm_detect;
    //size_t frame = 0;
    while(true){
        cap>>current_frame;
        if(current_frame.rows * current_frame.cols != 0){
            //cv::imshow("current frame", current_frame);
            sm_detect.has_smoke(current_frame);
            current_frame.copyTo(mask);
            sm_detect.draw_detected_smoke(mask);

            //cv::imshow("frame", current_frame);
            cv::imshow("mask", mask);

            //std::stringstream strs;
            //strs << path << std::setw(6) << std::setfill('0') << frame++ << ".png";
            //cv::imwrite(strs.str(), mask);
            //write << mask;

            int const key = cv::waitKey(30);
            if(key == 'e' || key == 'E'){
                break;
            }
        }
    }
}

int main(int argc, char *argv[])
{
    try{

        //QGuiApplication app(argc, argv);
        /*QApplication app(argc, argv);

        QByteArray device = QCamera::availableDevices()[0];
        QCamera camera(device);
        QVideoWidget surface;
        surface.resize(320, 240);
        camera.setViewfinder(&surface);

        camera.start();

        surface.show();*/

        /*QtQuick2ApplicationViewer viewer;
    viewer.setMainQmlFile(QStringLiteral("qrc:qml/smokeDetector/main.qml"));
    viewer.showExpanded();*/

        /*QQuickView view;
    view.setSource(QUrl("qrc:qml/smokeDetector/main.qml"));
    view.show();*/

        //detect_bg_mog();
        //detect_smoke_00();
        detect_smoke_custom_00();
        //test_haar_wavelet_transform();

        //return app.exec();
    }catch(std::exception const &ex){
        std::cout<<ex.what()<<std::endl;
    }

    return 0;
}
