# Add more folders to ship with the application, here
folder_01.source = qml/photoWizard
folder_01.target = qml
DEPLOYMENTFOLDERS = folder_01

# Additional import path used to resolve QML modules in Creator's code model
QML_IMPORT_PATH =

CONFIG += c++11

DEFINES += MAC_OS

INCLUDEPATH += ../../commercial_libs/algorithms
INCLUDEPATH += ../../commercial_libs/qmlHelper

# The .cpp file which was generated for your project. Feel free to hack it.
SOURCES += main.cpp \
    ../../commercial_libs/qmlHelper/file/fileProcessConstant.cpp \
    ../../commercial_libs/qmlHelper/file/fileProcess.cpp

# Installation path
# target.path =

# Please do not modify the following two lines. Required for deployment.
include(qtquick2applicationviewer/qtquick2applicationviewer.pri)
qtcAddDeployment()

HEADERS += \
    ../../commercial_libs/qmlHelper/file/fileProcessConstant.hpp \
    ../../commercial_libs/qmlHelper/file/fileProcess.hpp
