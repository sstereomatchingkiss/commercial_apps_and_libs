.pragma library

var cameraButtonHeight = 70
var cameraButtonWidth = 144
var colorOverlayInitialValue = 100

var effectButtonHeight = 32
var effectButtonWidth = 200
var effectListColor = "peru"

var panelOpacity = 0.7

var photoSelectBarWidth = 160

var toolBarWidth = 144

//states
var blurList = "BLURLIST"
var colorList = "COLOR"
var effectCategory = "EFFECTCATEGORY"
var motionBlurList = "MOTIONBLURLIST"
var shadowList = "SHADOWLIST"
var sharpenList = "SHARPENLIST"
var specialOneList = "SPECIALONELIST"
var specialTwoList = "SPECIALTWOLIST"
var specialThreeList = "SPECIALTHREELIST"
//states



