function invert_opacity(value) {
    return value == 0 ? 1 : 0
}

function shadow_sample_value(radius){
    for(var i = 4; i != 16; i += 4){
        if(radius <= i){
            return i * 2;
        }
    }
    return 32;
}
