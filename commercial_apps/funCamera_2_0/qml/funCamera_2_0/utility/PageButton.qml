import QtQuick 2.0

Rectangle {
    id: root

    property string startColor: "#202060"
    property string endColor: "#101070"

    property alias theText: text.text
    property alias textColor: text.color

    width: 100
    height: 62

    Text {
        id: text

        anchors.centerIn: root
        anchors.margins: 8
        text: qsTr("close")
        color: "red"
        style: Text.Raised;
        styleColor: "black"

        Behavior on color{ColorAnimation{duration: 200 }}
    }

    gradient: Gradient {
        GradientStop {
            position: 0
            Behavior on color {ColorAnimation { duration: 100 }}
            color: startColor
        }
        GradientStop {
            position: 1
            Behavior on color {ColorAnimation { duration: 100 }}
            color: endColor
        }
    }

    /*Rectangle{
        anchors.left: root.right
        width: 1
        height: root.height
    }*/

    Behavior on color{ ColorAnimation {duration: 200 } }
}
