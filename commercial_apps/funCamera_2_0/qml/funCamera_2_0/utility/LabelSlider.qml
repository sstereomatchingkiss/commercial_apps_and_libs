import QtQuick 2.0
import QtQuick.Controls 1.0

Item {
    id: root

    width: 180
    height: 20

    property alias maximum: slider.maximumValue
    property alias minimum: slider.minimumValue

    property alias sliderValue: slider.value
    property alias step: slider.stepSize

    property alias textBlockWidth: rect.width
    property alias textColor: text.color
    property alias text: text.text
    property alias textWidth: text.width

    signal labelSliderValueChanged(real value)

    Row{
        spacing: 5       

        Rectangle {
            id: rect

            width: text.width + 10
            height: text.contentHeight + 10
            gradient: Gradient {
                GradientStop { position: 0.0; color: "green" }
                GradientStop { position: 1.0; color: "blue" }
            }
            border.color: Qt.lighter("blue")
            opacity: 0.8

            //property alias theText: text.text
            //property alias textColor: text.color

            Text {
                id: text

                //anchors.verticalCenter: rect.verticalCenter
                //anchors.horizontalCenter: : rect.verticalCenter
                anchors.centerIn: rect
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter

                width: contentWidth
                height: contentHeight
                text: qsTr("close")
                color: "pink"
            }
        }

        Slider {
            id: slider
            width: root.width; height: root.height
            maximumValue: 360            

            onValueChanged: {
                labelSliderValueChanged(slider.value)
            }
        }       
    }
}
