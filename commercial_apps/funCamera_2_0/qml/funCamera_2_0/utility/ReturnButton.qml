import QtQuick 2.0
import "../../../js/style.js" as Style

Rectangle {
    id: root

    width: 100
    height: Style.effectButtonHeight
    radius: 20
    color: sysPalette.button
    border.color: Qt.lighter("blue")
    opacity: Style.panelOpacity

    SystemPalette{id : sysPalette}

    gradient: Gradient {
        GradientStop {
            position: 0
            //Behavior on color {ColorAnimation { duration: 100 }}
            color: root.color
        }
        GradientStop {
            position: 1
            //Behavior on color {ColorAnimation { duration: 100 }}
            color: "#f5f5f5"
        }
    }

    property alias theText: text.text
    property alias textColor: text.color

    signal click()

    Text {
        id: text

        anchors.centerIn: root
        anchors.margins: 8
        text: qsTr("close")
        color: "blue"
        style: Text.Raised;
        styleColor: "black"

        Behavior on color{ColorAnimation{duration: 200 }}
    }   

    MouseArea{
        anchors.fill: parent
        hoverEnabled: true

        onClicked: {
            click()
        }
        onEntered: {
            root.opacity = 1.0
            text.color = "red"
        }
        onExited: {
            root.opacity = Style.panelOpacity
            text.color = "blue"
        }
    }

    Behavior on opacity{NumberAnimation{duration: 200}}
}
