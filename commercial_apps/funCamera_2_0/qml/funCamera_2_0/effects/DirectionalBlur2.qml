import QtGraphicalEffects 1.0
import QtMultimedia 5.0
import QtQuick 2.0

import "../../../js/logic.js" as Logic
import "../effectsPanel" as Panel

Rectangle {
    width: 100
    height: 62

    Camera{
        id: camera
    }

    VideoOutput {
        id: videoOutput

        anchors.fill: parent
        source: camera
    }

    Panel.DirectionalBlur2Panel{
        id: panel

        z: 1
    }

    MouseArea{
        anchors.fill: parent
        onClicked: {
            panel.visible = !panel.visible
        }
    }

    DirectionalBlur {
        id: directionalBlur

        anchors.fill: videoOutput
        source: videoOutput
        samples: 24
        angle: panel.directionBlurAngle
        length: panel.directionBlurLength
    }

}
