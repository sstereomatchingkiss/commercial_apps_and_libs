import QtQuick 2.0
import QtMultimedia 5.0

import File 1.0

import "../../../../../commercial_libs/qmlHelper/shared" as Share
import "../effectsPanel" as Panel

Rectangle {
    width: 100
    height: 62

    Camera{
        id: camera
    }

    VideoOutput {
        id: videoOutput

        anchors.fill: parent
        source: camera
    }

    FileReader{id: fileReader}

    Panel.ToonPanel{
        id: panel

        z: 1
    }

    MouseArea{
        anchors.fill: parent
        onClicked: {
            panel.visible = !panel.visible
        }
    }

    Share.ImageEffect{
        id: effect

        property real threshold: panel.toonThreshold
        property real targetSize: 250 - (200 * threshold) // TODO: fix ...
        property real resS: targetSize
        property real resT: targetSize

        property real magTol: 0.3
        property real quantize: 8.0

        anchors.fill : videoOutput
        source : ShaderEffectSource { sourceItem: videoOutput }

        fragmentShaderText: fileReader.read_file(":/commercial_libs/shaderEffect/toonFragment.fsh")
    }
}
