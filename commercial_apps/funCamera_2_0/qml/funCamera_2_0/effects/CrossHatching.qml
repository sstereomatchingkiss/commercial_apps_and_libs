import QtQuick 2.0
import QtMultimedia 5.0

import File 1.0

import "../../../../../commercial_libs/qmlHelper/shared" as Share
import "../effectsPanel" as Panel

Rectangle {
    width: 100
    height: 62

    Camera{
        id: camera
    }

    VideoOutput {
        id: videoOutput

        anchors.fill: parent
        source: camera
    }

    FileReader{id: fileReader}

    Panel.CrossHatchingPanel{
        id: panel

        z: 1
    }

    MouseArea{
        anchors.fill: parent
        onClicked: {
            panel.visible = !panel.visible
        }
    }

    Share.ImageEffect{
        id: effect

        property real hatch_y_offset: 6.0
        property real lum_threshold_1: panel.crossHatchingLuminance - 0.3
        property real lum_threshold_2: panel.crossHatchingLuminance - 0.5
        property real lum_threshold_3: panel.crossHatchingLuminance - 0.7
        property real lum_threshold_4: panel.crossHatchingLuminance

        property real imageHeight: parent.height
        property real imageWidth: parent.width

        anchors.fill : videoOutput
        source : ShaderEffectSource { sourceItem: videoOutput }

        fragmentShaderText: fileReader.read_file(":/commercial_libs/shaderEffect/crossHatchingFragment.fsh")
    }
}
