import QtQuick 2.0
import QtMultimedia 5.0

import File 1.0

import "../../../../../commercial_libs/qmlHelper/shared" as Share

Rectangle {
    width: 100
    height: 62

    Camera{
        id: camera
    }

    VideoOutput {
        id: videoOutput

        anchors.fill: parent
        source: camera
    }

    FileReader{id: fileReader}

    Share.ImageEffect{
        id: effect

        property real amount: 5

        anchors.fill : videoOutput        
        source : ShaderEffectSource { sourceItem: videoOutput }

        fragmentShaderText : fileReader.read_file(":/commercial_libs/shaderEffect/sharpenFragment.fsh")
    }

}
