import QtQuick 2.0
import QtMultimedia 5.0

import File 1.0

import "../../../../../commercial_libs/qmlHelper/shared" as Share
import "../effectsPanel" as Panel

Rectangle {
    width: 100
    height: 62

    Camera{
        id: camera
    }

    VideoOutput {
        id: videoOutput

        anchors.fill: parent
        source: camera
    }

    FileReader{id: fileReader}

    Panel.ShockWavePanel{
        id: panel

        z: 1
    }

    MouseArea{
        anchors.fill: parent
        onClicked: {
            panel.visible = !panel.visible
        }
    }

    Share.ImageEffect{
        id: effect

        // Transform slider values, and bind result to shader uniforms
        property real weight: panel.shockWaveWeight

        property real centerX
        property real centerY
        property bool running: effect.weight != 0
        property real time

        SequentialAnimation {
            running: effect.running
            loops: Animation.Infinite
            ScriptAction {
                script: {
                    effect.centerX = Math.random()
                    effect.centerY = Math.random()
                }
            }
            NumberAnimation {
                target: effect
                property: "time"
                from: 0
                to: 1
                duration: 1000
            }
        }

        anchors.fill : videoOutput
        source : ShaderEffectSource { sourceItem: videoOutput }

        fragmentShaderText: fileReader.read_file(":/commercial_libs/shaderEffect/shockWaveFragment.fsh")
    }
}
