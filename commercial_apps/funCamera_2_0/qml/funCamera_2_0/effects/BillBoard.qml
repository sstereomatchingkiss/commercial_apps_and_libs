import QtQuick 2.0
import QtMultimedia 5.0

import File 1.0

import "../../../../../commercial_libs/qmlHelper/shared" as Share
import "../effectsPanel" as Panel

Rectangle {
    width: 100
    height: 62

    Camera{
        id: camera
    }

    VideoOutput {
        id: videoOutput

        anchors.fill: parent
        source: camera
    }

    FileReader{id: fileReader}

    Panel.BillBoardPanel{
        id: panel

        z: 1
    }

    MouseArea{
        anchors.fill: parent
        onClicked: {
            panel.visible = !panel.visible
        }
    }

    Share.ImageEffect{
        id: effect

        // Transform slider values, and bind result to shader uniforms
        property real grid: panel.billBoardGrid

        property variant source

        property real step_x: 0.0015625
        property real step_y: parent.height ? (step_x * parent.width / parent.height) : 0.0

        //property real targetHeight: parent.height
        //property real targetWidth: parent.width

        anchors.fill : videoOutput        
        source : ShaderEffectSource { sourceItem: videoOutput }

        fragmentShaderText : fileReader.read_file(":/commercial_libs/shaderEffect/billBoardFragment.fsh")
    }

}
