import QtQuick 2.0
import QtMultimedia 5.0

import File 1.0

import "../../../../../commercial_libs/qmlHelper/shared" as Share
import "../effectsPanel" as Panel

Rectangle {
    width: 100
    height: 62

    Camera{
        id: camera
    }

    VideoOutput {
        id: videoOutput

        anchors.fill: parent
        source: camera
    }

    FileReader{id: fileReader}

    Panel.WobblePanel{
        id: panel

        z: 1
    }

    MouseArea{
        anchors.fill: parent
        onClicked: {
            panel.visible = !panel.visible
        }
    }

    Share.ImageEffect{
        id: effect

        property real amplitude: panel.wobbleAmplitude

        property real duration: 600
        property real frequency: panel.wobbleFrequency
        property bool running: true
        property real time

        NumberAnimation on time { loops: Animation.Infinite; from: 0; to: Math.PI * 2; duration: effect.duration; running: effect.running }

        anchors.fill : videoOutput
        source : ShaderEffectSource { sourceItem: videoOutput; hideSource: true }

        fragmentShaderText: fileReader.read_file(":/commercial_libs/shaderEffect/wobbleFragment.fsh")
    }
}
