import QtGraphicalEffects 1.0
import QtMultimedia 5.0
import QtQuick 2.0

import "../../../js/logic.js" as Logic
import "../effectsPanel" as Panel

Rectangle {
    width: 100
    height: 62

    Camera{
        id: camera
    }

    VideoOutput {
        id: videoOutput

        anchors.fill: parent
        source: camera
    }

    Panel.ColorOverlay2Panel{
        id: panel

        z: 1
    }

    MouseArea{
        anchors.fill: parent
        onClicked: {
            panel.visible = !panel.visible
        }
    }

    ColorOverlay {
        anchors.fill: videoOutput
        source: videoOutput
        color: "#" + panel.colorOverlayAlpha.toString(16) + panel.colorOverlayRed.toString(16) +
               panel.colorOverlayGreen.toString(16) + panel.colorOverlayBlue.toString(16)
    }

}
