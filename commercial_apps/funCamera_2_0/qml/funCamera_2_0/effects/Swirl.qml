import QtQuick 2.0
import QtMultimedia 5.0

import File 1.0

import "../../../../../commercial_libs/qmlHelper/shared" as Share
import "../effectsPanel" as Panel

Rectangle {
    width: 100
    height: 62

    Camera{
        id: camera
    }

    VideoOutput {
        id: videoOutput

        anchors.fill: parent
        source: camera
    }

    FileReader{id: fileReader}

    Panel.SwirlPanel{
        id: panel

        z: 1
    }

    MouseArea{
        anchors.fill: parent
        onClicked: {
            panel.visible = !panel.visible
        }
    }

    Share.ImageEffect{
        id: effect

        property real angle: panel.swirlAngle
        property real centerX: 0.5
        property real centerY: 0.5
        property real duration: 6000
        property real radius: panel.swirlRadius
        property bool running: true
        property real time: 1

        NumberAnimation on time { loops: Animation.Infinite; from: 0; to: Math.PI * 2; duration: effect.duration; running: effect.running }

        SequentialAnimation {
            running: effect.running
            loops: Animation.Infinite

            NumberAnimation {
                target: effect
                property: "time"
                from: 0
                to: Math.PI * 2
                duration: effect.duration
            }

            NumberAnimation {
                target: effect
                property: "time"
                from: Math.PI * 2
                to: 0
                duration: effect.duration
            }
        }

        anchors.fill : videoOutput
        source : ShaderEffectSource { sourceItem: videoOutput }

        fragmentShaderText: fileReader.read_file(":/commercial_libs/shaderEffect/swirlFragment.fsh")
    }
}
