import QtQuick 2.0
import QtMultimedia 5.0

import File 1.0

import "../../../../../commercial_libs/qmlHelper/shared" as Share
import "../effectsPanel" as Panel

Rectangle {
    width: 100
    height: 62

    Camera{
        id: camera
    }

    VideoOutput {
        id: videoOutput

        anchors.fill: parent
        source: camera
    }

    FileReader{id: fileReader}

    Panel.RipplePanel{
        id: panel

        z: 1
    }

    MouseArea{
        anchors.fill: parent
        onClicked: {
            panel.visible = !panel.visible
        }
    }

    Share.ImageEffect{
        id: effect

        property real amplitude: panel.rippleAmplitude * 0.03
        property real duration: 600
        property real frequency: panel.rippleFrequency
        property bool running: true
        property real imageHeight: parent.height
        property real imageWidth: parent.width
        property real time

        anchors.fill : videoOutput
        source : ShaderEffectSource { sourceItem: videoOutput; hideSource: true }

        NumberAnimation on time { loops: Animation.Infinite; from: 0; to: Math.PI * 2; duration: effect.duration; running: effect.running}

        fragmentShaderText: fileReader.read_file(":/commercial_libs/shaderEffect/rippleFragment.fsh")
    }
}
