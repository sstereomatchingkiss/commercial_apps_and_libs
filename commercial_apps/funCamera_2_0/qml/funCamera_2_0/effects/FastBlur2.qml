import QtGraphicalEffects 1.0
import QtMultimedia 5.0
import QtQuick 2.0

import "../effectsPanel" as Panel

Rectangle {
    width: 100
    height: 62

    Camera{
        id: camera
    }

    VideoOutput {
        id: videoOutput

        anchors.fill: parent
        source: camera
    }

    Panel.FastBlur2Panel{
        id: panel

        z: 1
    }

    MouseArea{
        anchors.fill: parent
        onClicked: {
            panel.visible = !panel.visible
        }
    }

    FastBlur {
        anchors.fill: videoOutput
        source: videoOutput
        radius: panel.fastBlurRadius
    }

}
