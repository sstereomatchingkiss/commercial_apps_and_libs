import QtQuick 2.0

import "../../../../commercial_libs/qmlHelper/shared" as Share
import "utility" as Utility

import SystemInformation 1.0

Rectangle {
    id: root

    width: info.get_width() != 0 ? info.get_width() : 320
    height: info.get_height() != 0 ? info.get_height() : 240

    state: obj.stateEffectList

    SysInfo{id: info}

    QtObject{
        id: obj

        property int pageButtonHeight: root.height * 0.1
        property int pageButtonWidth: root.width / 3

        property string stateEffectList: "EffectList"
        property string stateCaptureImage: "CaptureImage"
    }

    Share.LauncherList {
        id: launcher
        width: root.width
        height: root.height * 0.9
        Component.onCompleted: {
            addExample("BillBoard", "",  Qt.resolvedUrl("effects/BillBoard.qml"));
            addExample("BlackAndWhite", "",  Qt.resolvedUrl("effects/BlackAndWhite.qml"));
            addExample("BlueOnly", "",  Qt.resolvedUrl("effects/BlueOnly.qml"));
            addExample("BrightnessContrast", "",  Qt.resolvedUrl("effects/BrightnessContrast2.qml"));
            addExample("ColorOverlay", "",  Qt.resolvedUrl("effects/ColorOverlay2.qml"));
            addExample("ColorRize", "",  Qt.resolvedUrl("effects/ColorRize.qml"));
            addExample("ColorToGray", "",  Qt.resolvedUrl("effects/Desaturate2.qml"));
            addExample("CrossHatching", "",  Qt.resolvedUrl("effects/CrossHatching.qml"));
            addExample("CrossStiching", "",  Qt.resolvedUrl("effects/CrossStiching.qml"));
            addExample("DirectionalBlur", "",  Qt.resolvedUrl("effects/DirectionalBlur2.qml"));
            addExample("Emboss", "",  Qt.resolvedUrl("effects/Emboss.qml"));
            addExample("FastBlur", "",  Qt.resolvedUrl("effects/FastBlur2.qml"));
            addExample("GammaAdjust", "",  Qt.resolvedUrl("effects/Gamma.qml"));
            addExample("GaussianBlur", "",  Qt.resolvedUrl("effects/GaussianBlur2.qml"));
            addExample("Glow", "",  Qt.resolvedUrl("effects/Glow.qml"));
            addExample("GreenOnly", "",  Qt.resolvedUrl("effects/GreenOnly.qml"));
            addExample("LensCircle", "",  Qt.resolvedUrl("effects/LensCircle.qml"));
            //addExample("OilPaiting", "",  Qt.resolvedUrl("effects/OilPainting.qml"));
            addExample("Pixelate", "",  Qt.resolvedUrl("effects/Pixelate.qml"));
            //addExample("Posterize", "",  Qt.resolvedUrl("effects/Posterize.qml"));
            addExample("RadialBlur", "",  Qt.resolvedUrl("effects/RadialBlur2.qml"));
            addExample("RedOnly", "",  Qt.resolvedUrl("effects/RedOnly.qml"));
            addExample("Ripple", "",  Qt.resolvedUrl("effects/Ripple.qml"));
            addExample("Sepia", "",  Qt.resolvedUrl("effects/Sepia.qml"));
            addExample("Sharpen", "",  Qt.resolvedUrl("effects/Sharpen.qml"));
            addExample("ShockWave", "",  Qt.resolvedUrl("effects/ShockWave.qml"));
            addExample("Sketch", "",  Qt.resolvedUrl("effects/Sketch.qml"));
            addExample("Swirl", "",  Qt.resolvedUrl("effects/Swirl.qml"));
            addExample("TiltShift", "",  Qt.resolvedUrl("effects/TiltShift.qml"));
            addExample("Toon", "",  Qt.resolvedUrl("effects/Toon.qml"));
            addExample("Vignette", "",  Qt.resolvedUrl("effects/Vignette.qml"));
            addExample("Warhol", "",  Qt.resolvedUrl("effects/Warhol.qml"));
            addExample("Wobble", "",  Qt.resolvedUrl("effects/Wobble.qml"));
            addExample("YellowOnly", "",  Qt.resolvedUrl("effects/YellowOnly.qml"));
            addExample("ZoomBlur", "",  Qt.resolvedUrl("effects/ZoomBlur2.qml"));
        }
    }

    Row{
        anchors.top: launcher.bottom

        Utility.PageButton{
            width: obj.pageButtonWidth
            height: obj.pageButtonHeight
            theText: "List"

            MouseArea{
                anchors.fill: parent

                onClicked: {
                    root.state = obj.stateEffectList
                    launcher.clearExample()
                }
            }
        }

        Utility.PageButton{
            width: obj.pageButtonWidth
            height: obj.pageButtonHeight
            theText: "Capture"

            MouseArea{
                anchors.fill: parent

                onClicked: {
                    root.state = obj.stateCaptureImage
                }
            }
        }

        Utility.PageButton{
            width: obj.pageButtonWidth
            height: obj.pageButtonHeight
            theText: "Quit"

            MouseArea{
                anchors.fill: parent

                onClicked: {
                    Qt.quit()
                }
            }
        }
    }


    states: [
        State{
            name: obj.stateEffectList
        },
        State{
            name: obj.stateCaptureImage
        }
    ]

    /*transitions: [
        Transition {
            from: "*"; to: Style.stateThumbsImage
            ParallelAnimation {
                NumberAnimation { target: imageLarger; property: "height"; duration: object.durationShort }
                NumberAnimation { target: imageLarger; property: "width";  duration: object.durationShort }
                NumberAnimation { target: imageViewer; property: "width";  duration: object.durationShort }
            }
        },
        Transition {
            from: "*"; to: Style.stateImagesNames
            ParallelAnimation {
                NumberAnimation { target: imageLarger; property: "height"; duration: object.durationShort }
                NumberAnimation { target: imageLarger; property: "width";  duration: object.durationShort }
                NumberAnimation { target: imageViewer; property: "width";  duration: object.durationShort }
            }
        },
        Transition {
            from: "*"; to: Style.stateMultiSelectOne
            ParallelAnimation {
                NumberAnimation { target: imageLarger; property: "height"; duration: object.durationShort }
                NumberAnimation { target: imageLarger; property: "width";  duration: object.durationShort }
                NumberAnimation { target: imageViewer; property: "width";  duration: object.durationShort }
            }
        }
    ]*/
}
