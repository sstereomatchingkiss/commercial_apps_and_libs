import QtQuick 2.0
import "../utility" as Utility
import "../../../js/style.js" as Style

//UI for setting the parameters of DirectionalBlur
Item {
    id: root

    width: parent.width
    height: Style.effectButtonHeight * 3

    //prefer bind rather than signal, because signal would not initialize the value of the
    //componet PhotoView.qml when startup
    property real directionBlurAngle: sliderAngle.sliderValue - 180
    property real directionBlurLength: sliderLength.sliderValue

    signal click(string state)
    signal hide()

    Column{
        anchors.fill: parent

        spacing: 10

        Utility.LabelSlider{
            id: sliderLength

            text: "length"
            maximum: 100
            minimum: 0
            width: root.width - sliderLength.textBlockWidth - 10
        }

        Utility.LabelSlider{
            id: sliderAngle

            text: "angle"
            textWidth: sliderLength.textWidth
            maximum: 360
            minimum: 0
            width: sliderLength.width
        }

        Utility.ReturnButton{
            id: defaultButton

            width: sliderLength.textBlockWidth + sliderLength.width
            theText: "default"

            MouseArea{
                anchors.fill: parent

                onClicked: {
                    sliderAngle.sliderValue = 0
                    sliderLength.sliderValue = 0
                }
            }
        }
    }

    Behavior on opacity{ NumberAnimation{ duration: 500} }
}
