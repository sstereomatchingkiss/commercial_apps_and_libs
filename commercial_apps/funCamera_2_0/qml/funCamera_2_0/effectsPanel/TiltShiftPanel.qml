import QtQuick 2.0

import "../utility" as Utility
import "../../../js/style.js" as Style

Item {
    id: root

    width: parent.width
    height: Style.effectButtonHeight * 3

    property real tiltShiftLowerBound: sliderLowerBound.sliderValue
    property real tiltShiftUpperBound: sliderUpperBound.sliderValue

    signal click(string state)
    signal hide()

    Column{
        anchors.top: parent.bottom

        spacing: 10

        Utility.LabelSlider{
            id: sliderLowerBound

            text: qsTr("lowerBound")
            maximum: 1
            minimum: 0
            sliderValue: 1.0
            width: root.width - sliderLowerBound.textBlockWidth - 10
        }

        Utility.LabelSlider{
            id: sliderUpperBound

            text: qsTr("upperBound")
            maximum: 1
            minimum: 0
            sliderValue: 0
            width: sliderLowerBound.width
        }

        Utility.ReturnButton{
            id: defaultButton

            width: sliderUpperBound.textBlockWidth + sliderUpperBound.width
            theText: qsTr("default")

            MouseArea{
                anchors.fill: parent

                onClicked: {
                    sliderLowerBound.sliderValue = 1.0
                    sliderUpperBound.sliderValue = 0
                }
            }
        }
    }

    Behavior on opacity{ NumberAnimation{ duration: 500} }
}
