import QtQuick 2.0

import "../utility" as Utility
import "../../../js/style.js" as Style

Item {
    id: root

    width: parent.width
    height: Style.effectButtonHeight * 3

    property real rippleAmplitude: sliderAmplitude.sliderValue
    property real rippleFrequency: sliderFrequency.sliderValue

    signal click(string state)
    signal hide()

    Column{
        anchors.top: parent.bottom

        spacing: 10

        Utility.LabelSlider{
            id: sliderAmplitude

            text: qsTr("amplitude")
            maximum: 1
            minimum: 0
            sliderValue: 0
            width: root.width - sliderAmplitude.textBlockWidth - 10
        }

        Utility.LabelSlider{
            id: sliderFrequency

            text: qsTr("fequency")
            maximum: 10
            minimum: 0
            sliderValue: 0
            textWidth: sliderAmplitude.textWidth
            width: sliderAmplitude.width
        }

        Utility.ReturnButton{
            id: defaultButton

            width: sliderAmplitude.textBlockWidth + sliderAmplitude.width
            theText: qsTr("default")

            MouseArea{
                anchors.fill: parent

                onClicked: {
                    sliderAmplitude.sliderValue = 0
                    sliderFrequency.sliderValue = 0
                }
            }
        }
    }

    Behavior on opacity{ NumberAnimation{ duration: 500} }
}
