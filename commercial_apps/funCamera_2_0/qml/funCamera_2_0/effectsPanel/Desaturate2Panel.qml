import QtQuick 2.0
import "../utility" as Utility
import "../../../js/style.js" as Style

//UI for setting the parameters of FastBlur
Item {
    id: root

    width: parent.width
    height: Style.effectButtonHeight * 3

    property real desatureSaturation: sliderSaturation.sliderValue

    signal click(string state)
    signal hide()

    Column{
        anchors.fill: parent

        spacing: 10

        Utility.LabelSlider{
            id: sliderSaturation

            text: "saturation"
            maximum: 1
            minimum: 0
            width: root.width - sliderSaturation.textBlockWidth - 10
        }

        Utility.ReturnButton{
            id: defaultButton

            width: sliderSaturation.textBlockWidth + sliderSaturation.width
            theText: "default"

            MouseArea{
                anchors.fill: parent

                onClicked: {
                    sliderSaturation.sliderValue = 0
                }
            }
        }
    }

    Behavior on opacity{ NumberAnimation{ duration: 500} }
}
