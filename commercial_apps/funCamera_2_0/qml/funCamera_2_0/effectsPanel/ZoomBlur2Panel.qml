import QtQuick 2.0
import "../utility" as Utility
import "../../../js/style.js" as Style

//UI for setting the parameters of ZoomBlur
Item {
    id: root

    width: parent.width
    height: Style.effectButtonHeight * 3

    property real zoomBlurLength: sliderLength.sliderValue

    signal click(string state)
    signal hide()

    Column{
        anchors.fill: parent

        spacing: 10

        Utility.LabelSlider{
            id: sliderLength

            text: "length"
            maximum: 200
            width: root.width - 10 - sliderLength.textBlockWidth
        }

        Utility.ReturnButton{
            id: defaultButton

            width: sliderLength.textBlockWidth + sliderLength.width
            theText: "default"

            MouseArea{
                anchors.fill: parent

                onClicked: {
                    sliderLength.sliderValue = 0
                }
            }
        }
    }

    Behavior on opacity{ NumberAnimation{ duration: 500} }
}


