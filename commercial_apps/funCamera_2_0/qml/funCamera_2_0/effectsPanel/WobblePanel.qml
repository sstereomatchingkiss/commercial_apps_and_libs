import QtQuick 2.0

import "../utility" as Utility
import "../../../js/style.js" as Style

Item {
    id: root

    width: parent.width
    height: Style.effectButtonHeight * 3

    property real wobbleAmplitude: sliderAmplitude.sliderValue * 0.03
    property real wobbleFrequency: sliderFrequency.sliderValue

    signal click(string state)
    signal hide()

    Column{
        anchors.top: parent.bottom

        spacing: 10

        Utility.LabelSlider{
            id: sliderAmplitude

            text: qsTr("amplitude")
            textWidth: sliderFrequency.textWidth
            maximum: 5
            minimum: 0
            width: root.width - sliderFrequency.textBlockWidth - 10
        }

        Utility.LabelSlider{
            id: sliderFrequency

            text: qsTr("frequency")
            maximum: 5
            minimum: 0
            width: sliderAmplitude.width
        }

        Utility.ReturnButton{
            id: defaultButton

            width: sliderFrequency.textBlockWidth + sliderFrequency.width
            theText: qsTr("default")

            MouseArea{
                anchors.fill: parent

                onClicked: {
                    sliderAmplitude.sliderValue = 0
                    sliderFrequency.sliderValue = 0
                }
            }
        }
    }

    Behavior on opacity{ NumberAnimation{ duration: 500} }
}
