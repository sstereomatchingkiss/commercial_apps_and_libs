import QtQuick 2.0
import "../utility" as Utility
import "../../../js/style.js" as Style

Item {
    id: root

    width: parent.width
    height: Style.effectButtonHeight * 3

    property real brightnessConstrastBright: sliderBrightness.sliderValue - 1
    property real brightnessConstrastContrast: sliderContrast.sliderValue - 1

    signal click(string state)
    signal hide()

    Column{
        anchors.fill: parent

        spacing: 10

        Utility.LabelSlider{
            id: sliderBrightness

            text: "brightness"
            maximum: 2
            minimum: 0
            sliderValue: maximum / 2
            textBlockWidth: 100
            width: root.width - sliderBrightness.textBlockWidth - 10
        }

        Utility.LabelSlider{
            id: sliderContrast

            text: "contrast"
            textWidth: sliderBrightness.textWidth
            maximum: 2
            minimum: 0
            sliderValue: maximum / 2
            textBlockWidth: 100
            width: sliderBrightness.width
        }

        Utility.ReturnButton{
            id: defaultButton

            width: sliderBrightness.textBlockWidth + sliderBrightness.width
            theText: "default"

            MouseArea{
                anchors.fill: parent

                onClicked: {
                    sliderBrightness.sliderValue = sliderBrightness.maximum / 2
                    sliderContrast.sliderValue = sliderContrast.maximum / 2
                }
            }
        }
    }

    Behavior on opacity{ NumberAnimation{ duration: 500} }
}
