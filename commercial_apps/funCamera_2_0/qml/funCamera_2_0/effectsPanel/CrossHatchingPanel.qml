import QtQuick 2.0

import "../utility" as Utility
import "../../../js/style.js" as Style

Item {
    id: root

    width: parent.width
    height: Style.effectButtonHeight * 3

    property real crossHatchingLuminance: sliderLuminance.sliderValue

    signal click(string state)
    signal hide()

    Column{
        anchors.top: parent.bottom

        spacing: 10

        Utility.LabelSlider{
            id: sliderLuminance

            text: qsTr("luminance")
            maximum: 1.0
            minimum: 0
            sliderValue: 0.5
            width: root.width - sliderLuminance.textBlockWidth - 10
        }

        Utility.ReturnButton{
            id: defaultButton

            width: sliderLuminance.textBlockWidth + sliderLuminance.width
            theText: qsTr("default")

            MouseArea{
                anchors.fill: parent

                onClicked: {
                    sliderLuminance.sliderValue = 0.5
                }
            }
        }
    }

    Behavior on opacity{ NumberAnimation{ duration: 500} }
}
