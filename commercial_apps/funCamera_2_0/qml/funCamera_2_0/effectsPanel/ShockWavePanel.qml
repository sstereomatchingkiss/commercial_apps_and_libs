import QtQuick 2.0

import "../utility" as Utility
import "../../../js/style.js" as Style

Item {
    id: root

    width: parent.width
    height: Style.effectButtonHeight * 3

    property real shockWaveWeight: sliderWeight.sliderValue

    signal click(string state)
    signal hide()

    Column{
        anchors.top: parent.bottom

        spacing: 10

        Utility.LabelSlider{
            id: sliderWeight

            text: qsTr("amplitude")
            maximum: 1
            minimum: 0
            sliderValue: 0
            width: root.width - sliderWeight.textBlockWidth - 10
        }

        Utility.ReturnButton{
            id: defaultButton

            width: sliderWeight.textBlockWidth + sliderWeight.width
            theText: qsTr("default")

            MouseArea{
                anchors.fill: parent

                onClicked: {
                    sliderWeight.sliderValue = 0

                }
            }
        }
    }

    Behavior on opacity{ NumberAnimation{ duration: 500} }
}
