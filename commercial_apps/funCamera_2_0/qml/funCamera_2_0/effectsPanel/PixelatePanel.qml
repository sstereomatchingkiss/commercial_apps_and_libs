import QtQuick 2.0

import "../utility" as Utility
import "../../../js/style.js" as Style

Item {
    id: root

    width: parent.width
    height: Style.effectButtonHeight * 3

    property real pixelateGranularity: sliderGranularity.sliderValue

    signal click(string state)
    signal hide()

    Column{
        anchors.top: parent.bottom

        spacing: 10

        Utility.LabelSlider{
            id: sliderGranularity

            text: qsTr("granularity")
            maximum: 10
            minimum: 0
            sliderValue: 0
            width: root.width - sliderGranularity.textBlockWidth - 10
        }

        Utility.ReturnButton{
            id: defaultButton

            width: sliderGranularity.textBlockWidth + sliderGranularity.width
            theText: qsTr("default")

            MouseArea{
                anchors.fill: parent

                onClicked: {
                    sliderGranularity.sliderValue = 0
                }
            }
        }
    }

    Behavior on opacity{ NumberAnimation{ duration: 500} }
}
