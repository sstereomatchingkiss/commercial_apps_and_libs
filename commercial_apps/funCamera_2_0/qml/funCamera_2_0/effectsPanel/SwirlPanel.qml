import QtQuick 2.0

import "../utility" as Utility
import "../../../js/style.js" as Style

Item {
    id: root

    width: parent.width
    height: Style.effectButtonHeight * 3

    property real swirlAngle: sliderAngle.sliderValue
    property real swirlRadius: sliderRadius.sliderValue

    signal click(string state)
    signal hide()

    Column{
        anchors.top: parent.bottom

        spacing: 10

        Utility.LabelSlider{
            id: sliderAngle

            text: qsTr("angle")
            textWidth: sliderRadius.textWidth
            maximum: 1
            minimum: 0
            width: root.width - sliderAngle.textBlockWidth - 10
        }

        Utility.LabelSlider{
            id: sliderRadius

            text: qsTr("radius")
            maximum: 1
            minimum: 0
            width: sliderAngle.width
        }

        Utility.ReturnButton{
            id: defaultButton

            width: sliderRadius.textBlockWidth + sliderRadius.width
            theText: qsTr("default")

            MouseArea{
                anchors.fill: parent

                onClicked: {
                    sliderAngle.sliderValue = 0
                    sliderRadius.sliderValue = 0
                }
            }
        }
    }

    Behavior on opacity{ NumberAnimation{ duration: 500} }
}
