import QtQuick 2.0
import "../utility" as Utility
import "../../../js/style.js" as Style

//UI for setting the parameters of FastBlur
Item {
    id: root

    width: parent.width
    height: Style.effectButtonHeight * 3

    property real fastBlurRadius: sliderRadius.sliderValue

    signal click(string state)
    signal hide()

    Column{
        anchors.top: parent.bottom

        spacing: 10

        Utility.LabelSlider{
            id: sliderRadius

            text: "radius"
            maximum: 64
            minimum: 0
            width: root.width - sliderRadius.textBlockWidth - 10
        }

        Utility.ReturnButton{
            id: defaultButton

            width: sliderRadius.textBlockWidth + sliderRadius.width
            theText: "default"

            MouseArea{
                anchors.fill: parent

                onClicked: {
                    sliderRadius.sliderValue = 0
                }
            }
        }        
    }

    Behavior on opacity{ NumberAnimation{ duration: 500} }
}
