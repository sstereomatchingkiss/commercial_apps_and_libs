import QtQuick 2.0
import "../utility" as Utility
import "../../../js/style.js" as Style

Item {
    id: root

    width: parent.width
    height: Style.effectButtonHeight * 3

    property real colorOverlayAlpha: sliderAlpha.sliderValue
    property real colorOverlayBlue: sliderBlue.sliderValue
    property real colorOverlayGreen: sliderGreen.sliderValue
    property real colorOverlayRed: sliderRed.sliderValue

    signal click(string state)
    signal hide()

    Column{
        anchors.fill: parent

        spacing: 10

        Utility.LabelSlider{
            id: sliderAlpha

            text: "alpha"
            maximum: 255
            minimum: 0
            sliderValue: Style.colorOverlayInitialValue
            step: 1
            width: root.width - sliderAlpha.textBlockWidth - 10
        }

        Utility.LabelSlider{
            id: sliderRed

            text: "red"
            textWidth: sliderAlpha.textWidth
            maximum: 255
            minimum: 0
            sliderValue: Style.colorOverlayInitialValue
            step: 1
            width: sliderAlpha.width
        }

        Utility.LabelSlider{
            id: sliderGreen

            text: "green"
            textWidth: sliderAlpha.textWidth
            maximum: 255
            minimum: 0
            sliderValue: Style.colorOverlayInitialValue
            step: 1
            width: sliderAlpha.width
        }

        Utility.LabelSlider{
            id: sliderBlue

            text: "blue"
            textWidth: sliderAlpha.textWidth
            maximum: 255
            minimum: 0
            sliderValue: Style.colorOverlayInitialValue
            step: 1
            width: sliderAlpha.width
        }

        Utility.ReturnButton{
            id: defaultButton

            width: sliderBlue.textBlockWidth + sliderBlue.width
            theText: "default"

            MouseArea{
                anchors.fill: parent

                onClicked: {
                    sliderAlpha.sliderValue = Style.colorOverlayInitialValue
                    sliderBlue.sliderValue = Style.colorOverlayInitialValue
                    sliderGreen.sliderValue = Style.colorOverlayInitialValue
                    sliderRed.sliderValue = Style.colorOverlayInitialValue
                }
            }
        }
    }

    Behavior on opacity{ NumberAnimation{ duration: 500} }
}
