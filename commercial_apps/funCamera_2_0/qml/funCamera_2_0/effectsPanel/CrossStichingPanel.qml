import QtQuick 2.0

import "../utility" as Utility
import "../../../js/style.js" as Style

Item {
    id: root

    width: parent.width
    height: Style.effectButtonHeight * 3

    signal click(string state)
    signal hide()

    Behavior on opacity{ NumberAnimation{ duration: 500} }
}
