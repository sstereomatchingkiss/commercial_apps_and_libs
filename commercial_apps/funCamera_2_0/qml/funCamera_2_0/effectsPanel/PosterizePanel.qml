import QtQuick 2.0

import "../utility" as Utility
import "../../../js/style.js" as Style

Item {
    id: root

    width: parent.width
    height: Style.effectButtonHeight * 3

    property real posterizeGamma: sliderGamma.sliderValue
    property real posterizeNumColors: sliderNumColors.sliderValue

    signal click(string state)
    signal hide()

    Column{
        anchors.top: parent.bottom

        spacing: 10

        Utility.LabelSlider{
            id: sliderGamma

            text: qsTr("gamma")
            maximum: 1
            minimum: 0
            sliderValue: 0.5
            width: root.width - sliderGamma.textBlockWidth - 10
        }

        Utility.LabelSlider{
            id: sliderNumColors

            text: qsTr("color")
            maximum: 30
            minimum: 0
            sliderValue: 10
            width: sliderGamma.width
            textWidth: sliderGamma.textWidth
        }

        Utility.ReturnButton{
            id: defaultButton

            width: sliderGamma.textBlockWidth + sliderGamma.width
            theText: qsTr("default")

            MouseArea{
                anchors.fill: parent

                onClicked: {
                    sliderGamma.sliderValue = 0.5
                    sliderNumColors.sliderValue = 10
                }
            }
        }
    }

    Behavior on opacity{ NumberAnimation{ duration: 500} }
}
