import QtQuick 2.0
import "../utility" as Utility
import "../../../js/style.js" as Style

Item {
    id: root

    width: parent.width
    height: Style.effectButtonHeight * 3

    property real colorizeHue: sliderHue.sliderValue
    property real colorizeSaturation: sliderSaturation.sliderValue
    property real colorizeIntensity: sliderIntensity.sliderValue - 1

    signal click(string state)
    signal hide()

    Column{
        anchors.fill: parent

        spacing: 10

        Utility.LabelSlider{
            id: sliderHue

            text: "hue"
            textWidth: sliderSaturation.textWidth
            maximum: 1
            minimum: 0
            width: sliderSaturation.width
        }

        Utility.LabelSlider{
            id: sliderSaturation

            text: "saturation"
            maximum: 1
            minimum: 0
            sliderValue: 1
            width: root.width - sliderSaturation.textBlockWidth - 10
        }

        Utility.LabelSlider{
            id: sliderIntensity

            text: "lightness"
            textWidth: sliderSaturation.textWidth
            maximum: 2
            minimum: 0
            sliderValue: maximum / 2
            width: sliderSaturation.width
        }

        Utility.ReturnButton{
            id: defaultButton

            width: sliderIntensity.textBlockWidth + sliderIntensity.width
            theText: "default"

            MouseArea{
                anchors.fill: parent

                onClicked: {
                    sliderHue.sliderValue = 0
                    sliderIntensity.sliderValue = sliderIntensity.maximum / 2
                    sliderSaturation.sliderValue = 1
                }
            }
        }
    }

    Behavior on opacity{ NumberAnimation{ duration: 500} }
}
