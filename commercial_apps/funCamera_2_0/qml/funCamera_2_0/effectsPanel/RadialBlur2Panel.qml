import QtQuick 2.0
import "../utility" as Utility
import "../../../js/style.js" as Style

//UI for setting the parameters of RadialBlur
Item {
    id: root

    width: parent.width
    height: Style.effectButtonHeight * 3

    property real radialBlurAngle: sliderAngle.sliderValue

    signal click(string state)
    signal hide()

    Column{
        anchors.fill: parent

        spacing: 10

        Utility.LabelSlider{
            id: sliderAngle

            text: "angle"
            maximum: 360
            minimum: 0
            width: root.width - sliderAngle.textBlockWidth - 10
        }

        Utility.ReturnButton{
            id: defaultButton

            width: sliderAngle.textBlockWidth + sliderAngle.width
            theText: "default"

            MouseArea{
                anchors.fill: parent

                onClicked: {
                    sliderAngle.sliderValue = 0
                }
            }
        }
    }

    Behavior on opacity{ NumberAnimation{ duration: 500} }
}

