import QtQuick 2.0

import "../utility" as Utility
import "../../../js/style.js" as Style

Item {
    id: root

    width: parent.width
    height: Style.effectButtonHeight * 3

    property real blackAndWhiteThreshold: sliderThreshold.sliderValue

    signal click(string state)
    signal hide()

    Column{
        anchors.top: parent.bottom

        spacing: 10

        Utility.LabelSlider{
            id: sliderThreshold

            text: qsTr("threshold")
            maximum: 1
            minimum: 0
            sliderValue: 0.5
            width: root.width - sliderThreshold.textBlockWidth - 10
        }

        Utility.ReturnButton{
            id: defaultButton

            width: sliderThreshold.textBlockWidth + sliderThreshold.width
            theText: qsTr("default")

            MouseArea{
                anchors.fill: parent

                onClicked: {
                    sliderThreshold.sliderValue = 0.5
                }
            }
        }
    }

    Behavior on opacity{ NumberAnimation{ duration: 500} }
}
