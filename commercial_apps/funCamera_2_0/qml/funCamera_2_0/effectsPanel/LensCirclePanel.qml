import QtQuick 2.0

import "../utility" as Utility
import "../../../js/style.js" as Style

Item {
    id: root

    width: parent.width
    height: Style.effectButtonHeight * 3

    //property real lensCircleRadiusX: sliderRadiusX.sliderValue > sliderRadiusY.sliderValue ? sliderRadiusY.sliderValue : sliderRadiusX.sliderValue
    //property real lensCircleRadiusY: sliderRadiusY.sliderValue < sliderRadiusX.sliderValue ? sliderRadiusX.sliderValue : sliderRadiusY.sliderValue

    property real lensCircleRadiusX: sliderRadiusX.sliderValue
    property real lensCircleRadiusY: sliderRadiusY.sliderValue

    signal click(string state)
    signal hide()

    Column{
        anchors.top: parent.bottom

        spacing: 10

        Utility.LabelSlider{
            id: sliderRadiusX

            text: qsTr("radiusX")
            maximum: 1
            minimum: sliderRadiusY.sliderValue + 0.01
            sliderValue: 0.5
            width: root.width - sliderRadiusX.textBlockWidth - 10
        }

        Utility.LabelSlider{
            id: sliderRadiusY

            text: qsTr("radiusY")
            maximum: sliderRadiusX.sliderValue - 0.01
            minimum: 0
            sliderValue: 0.3
            width: root.width - sliderRadiusX.textBlockWidth - 10
        }

        Utility.ReturnButton{
            id: defaultButton

            width: sliderRadiusX.textBlockWidth + sliderRadiusX.width
            theText: qsTr("default")

            MouseArea{
                anchors.fill: parent

                onClicked: {
                    sliderRadiusX.sliderValue = 0.5
                    sliderRadiusY.sliderValue = 0.3
                }
            }
        }
    }

    Behavior on opacity{ NumberAnimation{ duration: 500} }
}
