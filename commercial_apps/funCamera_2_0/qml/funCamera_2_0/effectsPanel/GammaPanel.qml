import QtQuick 2.0
import "../utility" as Utility
import "../../../js/style.js" as Style


//UI for setting the parameters of FastBlur
Item {
    id: root

    width: parent.width
    height: Style.effectButtonHeight * 3

    property real gammaAdjustGamma: sliderGamma.sliderValue

    signal click(string state)
    signal hide()

    Column{
        anchors.fill: parent

        spacing: 10

        Utility.LabelSlider{
            id: sliderGamma

            text: "gamma"
            maximum: 10
            minimum: 0
            sliderValue: 1
            width: root.width - sliderGamma.textBlockWidth - 10
        }

        Utility.ReturnButton{
            id: defaultButton

            width: sliderGamma.textBlockWidth + sliderGamma.width
            theText: "default"

            MouseArea{
                anchors.fill: parent

                onClicked: {
                    sliderGamma.sliderValue = 1
                }
            }
        }
    }

    Behavior on opacity{ NumberAnimation{ duration: 500} }
}
