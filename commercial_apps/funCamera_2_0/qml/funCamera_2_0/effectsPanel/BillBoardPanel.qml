import QtQuick 2.0

import "../utility" as Utility
import "../../../js/style.js" as Style

Item {
    id: root

    width: parent.width
    height: Style.effectButtonHeight * 3

    property real billBoardGrid: sliderGrid.sliderValue

    signal click(string state)
    signal hide()

    Column{
        anchors.top: parent.bottom
        //anchors.centerIn: parent

        spacing: 10

        Utility.LabelSlider{
            id: sliderGrid

            text: qsTr("grid")
            maximum: 32
            minimum: 0
            sliderValue: 10

            width: root.width - sliderGrid.textBlockWidth - 10
        }

        Utility.ReturnButton{
            id: defaultButton

            width: sliderGrid.textBlockWidth + sliderGrid.width
            theText: qsTr("default")

            MouseArea{
                anchors.fill: parent

                onClicked: {
                    sliderGrid.sliderValue = 10
                }
            }
        }        
    }

    Behavior on opacity{ NumberAnimation{ duration: 500} }
}
