//#include <QtGui/QGuiApplication>
#include <QApplication>
#include "qtquick2applicationviewer.h"

#include <QQmlContext>
//#include <QQmlEngine>
#include <QQuickPaintedItem>
#include <QQuickView>

#include <QGuiApplication>

#include <file/fileReader.hpp>

#include "systemInformation.hpp"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    qmlRegisterType<FileReader>("File", 1, 0, "FileReader");
    qmlRegisterType<systemInformation>("SystemInformation", 1, 0, "SysInfo");

    QtQuick2ApplicationViewer viewer;
    viewer.setSource(QStringLiteral("qrc:///qml/funCamera_2_0/main.qml"));
    //viewer.setMainQmlFile(QStringLiteral("qrc:///qml/funCamera_2_0/main.qml"));
    viewer.showExpanded();

    return app.exec();
}
