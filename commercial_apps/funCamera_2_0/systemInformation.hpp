#ifndef SYSTEMINFORMATION_HPP
#define SYSTEMINFORMATION_HPP

#include <QDesktopWidget>
#include <QObject>

class systemInformation : public QObject
{
    Q_OBJECT
public:
    explicit systemInformation(QObject *parent = nullptr);
    systemInformation& operator=(systemInformation const&) = delete;
    systemInformation(systemInformation const&) = delete;

    Q_INVOKABLE int get_height() const;
    Q_INVOKABLE int get_width() const;

private:
    QDesktopWidget info_;
};

#endif // SYSTEMINFORMATION_HPP
