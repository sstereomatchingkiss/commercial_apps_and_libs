# Add more folders to ship with the application, here
folder_01.source = qml/funCamera_2_0
folder_01.target = qml
DEPLOYMENTFOLDERS = folder_01

# Additional import path used to resolve QML modules in Creator's code model
QML_IMPORT_PATH =

CONFIG += c++11

INCLUDEPATH += ../../commercial_libs/qmlHelper
INCLUDEPATH += ../../commercial_libs/QFileAndQString

QT += quick qml multimedia widgets

# The .cpp file which was generated for your project. Feel free to hack it.
SOURCES += main.cpp \
    ../../commercial_libs/qmlHelper/file/fileReader.cpp \
    systemInformation.cpp \
    ../../commercial_libs/qmlHelper/shared/screenCapture.cpp \
    ../../commercial_libs/QFileAndQString/fileAuxiliary.cpp \
    ../../commercial_libs/QFileAndQString/qstringAlgo.cpp

# Installation path
# target.path =

# Please do not modify the following two lines. Required for deployment.
include(qtquick2applicationviewer/qtquick2applicationviewer.pri)
qtcAddDeployment()

RESOURCES += \
    qml.qrc \
    ../../commercial_libs/qmlHelper/shared/shared.qrc \
    ../../commercial_libs/shaderEffect/shader.qrc \
    js.qrc

OTHER_FILES += \
    ../../commercial_libs/qmlHelper/shared/SimpleLauncherDelegate.qml \
    ../../commercial_libs/qmlHelper/shared/LauncherList.qml \
    ../../commercial_libs/shaderEffect/crossHatchingFragment.fsh \
    ../../commercial_libs/shaderEffect/lensCircleFragment.fsh \
    ../../commercial_libs/shaderEffect/tiltShiftFragment.fsh \
    ../../commercial_libs/shaderEffect/sketchFragment.fsh \
    ../../commercial_libs/shaderEffect/rippleFragment.fsh \
    ../../commercial_libs/shaderEffect/shockWaveFragment.fsh \
    ../../commercial_libs/shaderEffect/oilPaintingFragment.fsh \
    ../../commercial_libs/shaderEffect/pixelateFragment.fsh \
    ../../commercial_libs/shaderEffect/blackAndWhiteFragment.fsh \
    ../../commercial_libs/shaderEffect/swirlFragment.fsh \
    ../../commercial_libs/shaderEffect/crossStitchingFragment.fsh \
    ../../commercial_libs/shaderEffect/wobbleFragment.fsh \
    ../../commercial_libs/shaderEffect/warholFragment.fsh \
    ../../commercial_libs/shaderEffect/vignetteFragment.fsh \
    ../../commercial_libs/shaderEffect/toonFragment.fsh \
    ../../commercial_libs/shaderEffect/sharpenFragment.fsh \
    ../../commercial_libs/shaderEffect/billBoardFragment.fsh \
    ../../commercial_libs/shaderEffect/sepiaFragment.fsh \
    ../../commercial_libs/shaderEffect/posterizeFragment.fsh \
    ../../commercial_libs/shaderEffect/pageCurlFragment.fsh \
    ../../commercial_libs/shaderEffect/glowFragment.fsh \
    ../../commercial_libs/shaderEffect/embossFragment.fsh \
    ../../commercial_libs/qmlHelper/shared/ImageEffect.qml \
    js/style.js \
    js/logic.js \
    qml/funCamera_2_0/effects/BrightnessContrast2.qml \
    qml/funCamera_2_0/effects/ColorOverlay2.qml \
    qml/funCamera_2_0/effects/Desaturate2.qml \
    qml/funCamera_2_0/effects/DirectionalBlur2.qml \
    qml/funCamera_2_0/effects/FastBlur2.qml \
    qml/funCamera_2_0/effects/RadialBlur2.qml \
    qml/funCamera_2_0/effects/ZoomBlur2.qml \
    qml/funCamera_2_0/effectsPanel/ColorOverlay2Panel.qml \
    qml/funCamera_2_0/effectsPanel/Desaturate2Panel.qml \
    qml/funCamera_2_0/effectsPanel/DirectionalBlur2Panel.qml \
    qml/funCamera_2_0/effectsPanel/FastBlur2Panel.qml \
    qml/funCamera_2_0/effectsPanel/GaussianBlur2Panel.qml \
    qml/funCamera_2_0/effectsPanel/RadialBlur2Panel.qml \
    qml/funCamera_2_0/effectsPanel/ZoomBlur2Panel.qml \
    qml/funCamera_2_0/effects/GaussianBlur2.qml \
    qml/funCamera_2_0/effectsPanel/BrightnessContrast2Panel.qml \
    ../../commercial_libs/shaderEffect/colorFilterFragment.fsh

HEADERS += \
    ../../commercial_libs/qmlHelper/file/fileReader.hpp \
    systemInformation.hpp \
    ../../commercial_libs/qmlHelper/shared/screenCapture.hpp \
    ../../commercial_libs/QFileAndQString/fileAuxiliary.hpp \
    ../../commercial_libs/QFileAndQString/qstringAlgo.hpp
