#ifndef COLORCORRECTION_HPP
#define COLORCORRECTION_HPP

/*
 * implement a window for color correction, includes
 * RGB, HSV and gamma correction
 */

#include <opencv2/core/core.hpp>

#include <QDialog>
#include <QImage>

class QDoubleSpinBox;
class QGroupBox;
class QLabel;
class QPushButton;
class QSlider;
class QSpinBox;

class colorCorrection : public QDialog
{
    Q_OBJECT
public:
    explicit colorCorrection(QWidget *parent = 0);
    colorCorrection(colorCorrection const&) = delete;
    colorCorrection& operator=(colorCorrection const&) = delete;

    QImage const get_image() const;

    void set_image(QImage const &image);

signals:
    void apply_to_origin_signal();
    void ok();
    
private slots:
    void apply_to_origin();

    void bgr_b_changed(int value);
    void bgr_g_changed(int value);
    void bgr_r_changed(int value);

    void hsv_h_changed(int value);
    void hsv_s_changed(int value);
    void hsv_v_changed(int value);

    void value_gamma_changed(int value);
    void value_gamma_changed(double value);

    void set_default_value();
    void set_new_image(cv::Mat &mat);

private:
    void add_bgr_value(cv::Mat &src);
    void add_hsv_value(cv::Mat &bgr, cv::Mat &dst);

    void block_and_set_bgr_channel(int value, int channel);
    void block_and_set_gamma_value(int value);
    void block_and_set_hsv_channel(int value, int channel);

    void color_change(cv::Mat &src);
    void create_connection();
    void create_layout();
    void create_widget();

    void value_bgr_changed(int value, int channel);
    void value_hsv_changed(int value, int channel);

private:    
    QPushButton         *button_apply_to_origin_;
    QPushButton         *button_ok_;
    QPushButton         *button_set_default_value_;

    QGroupBox           *groupbox_color_balance_;    
    QGroupBox           *groupbox_gamma_;

    QImage               image_new_bgr_scale_;    //save the bgr image afer scale
    QImage               image_origin_;           //origin image without scale
    QImage               image_origin_bgr_scale_; //save the bgr image after scale
    QImage               image_result_;           //result without scale

    cv::Mat              mat_new_bgr_scale_;    //reference to image_new_bgr_
    cv::Mat              mat_new_hsv_scale_;    //reference to image_new_hsv_
    cv::Mat              mat_origin_bgr_scale_; //reference to image_origin_bgr_

    QLabel              *label_bgr_[3];
    QLabel              *label_gamma_;
    QLabel              *label_hsv_[3];
    QLabel              *label_new_image_;
    QLabel              *label_new_image_content_;
    QLabel              *label_origin_image_;
    QLabel              *label_origin_image_content_;       

    QSlider             *slider_bgr_[3];
    QSlider             *slider_gamma_;
    QSlider             *slider_hsv_[3];

    QSpinBox            *spinbox_bgr_[3];
    QDoubleSpinBox      *spinbox_gamma_;
    QSpinBox            *spinbox_hsv_[3];
};

#endif // COLORCORRECTION_HPP
