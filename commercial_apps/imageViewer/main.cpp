#include <QApplication>

#include "imageViewer.hpp"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    imageViewer viewer;
    viewer.show();
    
    return a.exec();
}
