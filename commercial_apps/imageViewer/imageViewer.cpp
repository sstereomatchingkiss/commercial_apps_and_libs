#ifdef DEBUG_OK
#include <QDebug>
#endif

#include <initializer_list>

#include "opencv2/core/core.hpp"
//#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#include <QList>
#include <QStringList>

#include <QAction>
#include <QApplication>
#include <QFileDialog>
#include <QGraphicsPixmapItem>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QIcon>
#include <QMenu>
#include <QMenuBar>
#include <QMessageBox>
#include <QPixmap>
#include <QTextCodec>
#include <QToolBar>

#include "colorCorrection.hpp"
#include "imageAlgorithm.hpp"
#include "imageViewer.hpp"
#include "openCVToQt.hpp"
#include "widgetGlobalHelper.hpp"
#include "waterMark.hpp"

imageViewer::imageViewer(QWidget *parent) :
    QMainWindow(parent), angle_(0), fit_to_window_(false), scale_option_(ScaleOption::In)
{
    QTextCodec *codec = QTextCodec::codecForName("UTF-8");
    QTextCodec::setCodecForLocale(codec);

    create_action();
    create_menu_add_toolbar();
    create_widget();
    create_connection();
}

QImage imageViewer::get_image() const
{
    return graph_pixmap_->pixmap().toImage();
}

void imageViewer::save_image()
{
    QImage image = graph_pixmap_->pixmap().toImage();
    if(!image.isNull()){
        auto const image_name = QFileDialog::getSaveFileName
                (
                    this, tr("Save Image"), QDir::currentPath(),
                    "Images (*.bmp *.jpg *.JPEG *.png *.ppm *.tiff);;All files (*)"
                    );

        if(!image_name.isEmpty()){
#ifdef DEBUG_OK
            add_water_mark(image, "Trial Version");
#endif
            if(!image.save(image_name, 0, save_quality_)){
                QMessageBox::warning(this, tr("Warning"), tr("Can't save image %1.\n%2\n%3").arg(image_name).arg(__FUNCTION__).arg(__FILE__));
            }
        }
    }
}

/*
 * make the action become visible or invisible by command
 *
 *@param
 * command : the action you want to manipulate
 * is_visible : false is invisible; true is visible
 */
void imageViewer::set_action_visible(std::initializer_list<Action> const command, bool is_visible)
{
    for(auto const cmd : command){
        action_array_[static_cast<int>(cmd)]->setVisible(is_visible);
    }
}

/*
 * set the image by name
 */
void imageViewer::set_image(QString const &image_name)
{        
    open_image_impl(image_name);
}

/*
 * set the image by QImage, the name is optional, but it is recommended
 * to give it a name
 */
void imageViewer::set_image(QImage const &image, QString const &image_name)
{
    open_image_impl(image, image_name);
}

/*
 * make the menu become visible or invisible
 *
 *@param
 * is_visible : false is invisible; true is visible
 */
void imageViewer::set_menu_visible(bool is_visible)
{    
    menuBar()->setVisible(is_visible);
}

/*
 * make the toolbar become visible or invisible by command
 *
 *@param
 * command : the toolbar you want to manipulate
 * is_visible : false is invisible; true is visible
 */
void imageViewer::set_toolbar_visible(std::initializer_list<Toolbar> const command, bool is_visible)
{
    for(auto const cmd : command){
        toolbar_array_[static_cast<int>(cmd)]->setVisible(is_visible);
    }
}

void imageViewer::set_undo_image(QImage const &src)
{
    if(!src.isNull()){
        image_undo_ = src;
        action_undo_->setEnabled(true);
    }
}

/**********************************************************
 ****************** implementation ************************
 **********************************************************/

/*
 * scale the image size to fit in the viewport
 */
void imageViewer::aspect_ratio()
{
    if(!graph_view_is_empty()){
        QImage image = graph_pixmap_->pixmap().toImage();
        action_aspect_ratio_->setChecked(!action_aspect_ratio_->isChecked());
        graph_pixmap_->setPixmap(QPixmap::fromImage(image));
        graph_view_->fitInView(graph_pixmap_, Qt::KeepAspectRatio);

        if(fit_to_window_){
            fit_to_window();
        }
    }
}

void imageViewer::create_action()
{
    action_about_ = new QAction(tr("About"), this);

    action_aspect_ratio_ = new QAction(QIcon(":/icons/aspect_ratio.png"), tr("Aspect ratio"), this);
    action_aspect_ratio_->setCheckable(true);
    action_aspect_ratio_->setShortcut(tr("Shift+A"));

    action_color_balance_    = new QAction(tr("HSV/RGB/Gamma"), this);
    action_color_correction_ = new QAction(QIcon(":/icons/color.png"), tr("Color correction"), this);

    action_exit_ = new QAction(QIcon(":/icons/close.png"), tr("Exit"), this);
    action_exit_->setShortcut(tr("Ctrl+Q"));

    action_fit_to_window_ = new QAction(tr("Fit to window"), this);
    action_fit_to_window_->setCheckable(true);
    action_fit_to_window_->setShortcut(tr("Ctrl+F"));

    action_flip_horizontal_ = new QAction(QIcon(":/icons/flip_horizontal.png"), tr("Flip horizontal"), this);
    action_flip_horizontal_->setShortcut(tr("Ctrl+H"));

    action_flip_vertical_ = new QAction(QIcon(":/icons/flip_vertical.png"), tr("Flip vertical"), this);
    action_flip_vertical_->setShortcut(tr("Ctrl+V"));

    action_full_screen_ = new QAction(QIcon(":/icons/full_screen.png"), tr("Full screen"), this);
    action_full_screen_->setShortcut(tr("F11"));

    action_normal_size_ = new QAction(QIcon(":/icons/zoom_1_1.png"), tr("1:1"), this);
    action_normal_size_->setShortcut(tr("Ctrl+N"));

    action_histogram_equal_ = new QAction(tr("Histogram equalization"), this);

    action_open_ = new QAction(QIcon(":/icons/open.png"), tr("Open"), this);
    action_open_->setShortcut(tr("Ctrl+O"));

    action_qt_ = new QAction(tr("About Qt"), this);

    action_rotate_90_ = new QAction(QIcon(":/icons/rotate_90.png"), tr("Rotate clockwise"), this);
    action_rotate_90n_ = new QAction(QIcon(":/icons/rotate_90n.png"), tr("Rotate counter clockwise"), this);

    action_save_image_ = new QAction(QIcon(":/icons/save.png"), tr("Save"), this);

    action_to_gray_image_ = new QAction(QIcon(":/icons/gray.png"), tr("Gray image"), this);

    //I do not name the image as "undo.png" since there are some weird bug if you do that
    //should be solve in the future
    action_undo_ = new QAction(QIcon(":/icons/revert.png"), tr("Undo"), this);
    action_undo_->setEnabled(false);

    action_zoom_in_ = new QAction(QIcon(":/icons/zoom_in.png"), tr("Zoom in"), this);
    action_zoom_in_->setShortcut(tr("Ctrl++"));

    action_zoom_out_ = new QAction(QIcon(":/icons/zoom_out.png"), tr("Zoom out"), this);
    action_zoom_out_->setShortcut(tr("Ctrl+-"));

    QAction *temp[] = {action_about_,           action_aspect_ratio_,  action_color_correction_,
                       action_qt_,              action_exit_,          action_fit_to_window_,
                       action_flip_horizontal_, action_flip_vertical_, action_full_screen_,
                       action_normal_size_,     action_open_,          action_rotate_90_,
                       action_rotate_90n_,      action_save_image_,    action_undo_,
                       action_zoom_in_,         action_zoom_out_};
    action_array_.insert(std::end(action_array_), std::begin(temp), std::end(temp));
}

void imageViewer::create_connection()
{
    connect(action_aspect_ratio_,    SIGNAL(triggered()), this, SLOT(aspect_ratio()));
    connect(action_color_balance_,   SIGNAL(triggered()), this, SLOT(exec_color_correction()));
    connect(action_exit_,            SIGNAL(triggered()), this, SLOT(close()));
    connect(action_fit_to_window_,   SIGNAL(triggered()), this, SLOT(fit_to_window()) );
    connect(action_flip_horizontal_, SIGNAL(triggered()), this, SLOT(flip_horizontal()) );
    connect(action_flip_vertical_,   SIGNAL(triggered()), this, SLOT(flip_vertical()) );
    connect(action_histogram_equal_, SIGNAL(triggered()), this, SLOT(histogram_equalization()));
    connect(action_normal_size_,     SIGNAL(triggered()), this, SLOT(normal_size()) );
    connect(action_open_,            SIGNAL(triggered()), this, SLOT(open_image()) );
    connect(action_qt_,              SIGNAL(triggered()), qApp, SLOT(aboutQt()) );
    connect(action_rotate_90_,       SIGNAL(triggered()), this, SLOT(rotate_image_clockwise()));
    connect(action_rotate_90n_,      SIGNAL(triggered()), this, SLOT(rotate_image_counter_clockwise()));
    connect(action_save_image_,      SIGNAL(triggered()), this, SLOT(save_image()));
    connect(action_to_gray_image_,   SIGNAL(triggered()), this, SLOT(to_gray_image()));
    connect(action_undo_,            SIGNAL(triggered()), this, SLOT(undo()));
    connect(action_zoom_in_,         SIGNAL(triggered()), this, SLOT(zoom_in()) );
    connect(action_zoom_out_,        SIGNAL(triggered()), this, SLOT(zoom_out()) );

    connect(color_correction_, SIGNAL(apply_to_origin_signal()), this, SLOT(set_image_after_color_balance()));
    connect(color_correction_, SIGNAL(ok()), this, SLOT(set_image_after_color_balance_and_close()));
}

void imageViewer::create_menu_add_toolbar()
{
    QMenu *color_correction_menu = new QMenu(tr("Color correction menu"), this);
    color_correction_menu->addAction(action_color_balance_);
    color_correction_menu->addAction(action_histogram_equal_);
    action_color_correction_->setMenu(color_correction_menu);

    menu_file_ = menuBar()->addMenu(tr("File"));
    toolbar_file_ = addToolBar(tr("File"));
    populate_menu_and_toolbar(menu_file_, toolbar_file_,
    {
                                  action_open_, action_exit_, action_save_image_
                              });

    menu_view_ = menuBar()->addMenu(tr("View"));
    QAction *const null_action = nullptr;
    populate_menu(menu_view_,
    {
                      action_full_screen_,   action_normal_size_, action_zoom_in_,
                      action_zoom_out_,      null_action,         action_aspect_ratio_,
                      action_fit_to_window_
                  });

    toolbar_image_ = addToolBar(tr("View"));
    populate_toolbar(toolbar_image_,
    {
                         action_undo_,          action_aspect_ratio_,  action_full_screen_,
                         action_normal_size_,   action_zoom_in_,       action_zoom_out_,
                         action_rotate_90n_,    action_rotate_90_,     action_flip_horizontal_,
                         action_flip_vertical_, action_to_gray_image_, action_color_correction_
                     });

    menu_about_ = menuBar()->addMenu(tr("About"));
    populate_menu(menu_about_, {action_about_, action_qt_});

    QToolBar *toolbar_temp[] = {toolbar_file_, toolbar_image_};
    toolbar_array_.insert(std::begin(toolbar_array_), std::begin(toolbar_temp), std::end(toolbar_temp));
}

void imageViewer::create_widget()
{        
    color_correction_ = new colorCorrection(this);

    graph_scene_  = new QGraphicsScene(this);
    graph_scene_->addItem(graph_pixmap_ = new QGraphicsPixmapItem);
    graph_view_   = new QGraphicsView(graph_scene_, this);

    graph_view_->setDragMode(QGraphicsView::ScrollHandDrag);

    setCentralWidget(graph_view_);
}

void imageViewer::disable_undo()
{
    image_undo_ = QImage();
    action_undo_->setEnabled(false);
}

void imageViewer::exec_color_correction()
{
    if(!graph_pixmap_->pixmap().isNull()){
        color_correction_->set_image(graph_pixmap_->pixmap().toImage());
        color_correction_->exec();
    }
}

void imageViewer::fit_to_window()
{
    fit_to_window_ = !fit_to_window_;
    update_fit_to_action(fit_to_window_);
}

void imageViewer::flip_horizontal()
{
    flip_image_impl(true);
}

/*
 * true : flip horizontal; false: flip vertical
 */
void imageViewer::flip_image_impl(bool flip)
{
    if(!graph_view_is_empty()){
        QImage img = get_image();
        if(!img.isNull()){
            img = img.mirrored(flip, !flip);
            graph_pixmap_->setPixmap(QPixmap::fromImage(img));
        }
    }
}

void imageViewer::flip_vertical()
{
    flip_image_impl(false);
}

bool imageViewer::graph_view_is_empty() const
{
    return graph_view_->items().isEmpty() ? true : false;
}

void imageViewer::histogram_equalization()
{
    QImage src = get_image();
    if(!src.isNull()){
        cv::Mat mat = qimage_to_mat_ref(src);
        if(mat.data){
            if(histogram_equalize(mat, mat)){
                src = mat_to_qimage_cpy(mat);
                if(!src.isNull()){
                    set_undo_image(get_image());
                    graph_pixmap_->setPixmap(QPixmap::fromImage(src));
                }
            }
        }
    }
}

void imageViewer::normal_size()
{    
    if(!graph_view_is_empty()){
        scale_option_ = ScaleOption::Normal;
        scale_image();
    }
}

void imageViewer::open_image()
{
    image_name_ = QFileDialog::getOpenFileName(this, tr("Open File"), QDir::currentPath());
    open_image_impl(image_name_);
}

void imageViewer::open_image_impl(QString const &image_name)
{
    if (!image_name.isEmpty()){
        open_image_impl(QImage(image_name), image_name);
    }
}

void imageViewer::open_image_impl(QImage const &image, QString const &image_name)
{
    if (image.isNull()) {
        QMessageBox::information(this, tr("Image Viewer"), tr("Cannot load %1.\n%2\n%3").arg(image_name).arg(__FUNCTION__).arg(__FILE__));
        return;
    }

    graph_pixmap_->setPixmap(QPixmap::fromImage(image));
    //set the scene rect to the specific image rect since the sceneRect wouldn't shrink automatically
    graph_scene_->setSceneRect(image.rect());
    //reset the matrix because fitInView may change the internal matrix value, it
    //may cause weird effect when showing image
    graph_view_->resetMatrix();

    disable_undo();

    angle_ = 0;
    image_name_ = image_name;
}

/*
 * implementation of image rotation
 *
 *@param
 * angle : the angle of rotation, will not exceed std::abs(360) degrees.
 *         The angle should be -360, -270, -180, -90, 0, 90, 180, 270, 360
 */
void imageViewer::rotate_image_implement(double angle)
{    
    if(!graph_view_is_empty()){
        QImage img = get_image();
        if(!img.isNull()){
            cv::Mat mat = qimage_to_mat_ref(img);
            if(!mat.empty()){
                rotate_image_90n(mat, mat, angle);
                img = mat_to_qimage_cpy(mat, img.format());

                if(!img.isNull()){
                    graph_pixmap_->setPixmap(QPixmap::fromImage(img));
                    graph_scene_->setSceneRect(img.rect());
                }
            }else{
                QMessageBox::information(this, tr("Image Viewer"),
                                         tr("do not support rotation function on this type of image yet.\n%1\n%2").arg(__FUNCTION__).arg(__FILE__));
            }
        }
    }
}

/*
 * rotate the image clockwise(90)
 */
void imageViewer::rotate_image_clockwise()
{
    angle_ += 90;
    rotate_image_implement(90);
}

/*
 * rotate the image counter clockwise(-90)
 */
void imageViewer::rotate_image_counter_clockwise()
{
    angle_ -= 90;
    rotate_image_implement(-90);
}

void imageViewer::scale_image()
{                      
    if(!graph_view_is_empty()){
        try{
            QImage src = get_image();
            if(!src.isNull()){

                double const scale_factor = scale_option_ == ScaleOption::In ? 1.25 :
                                                             ScaleOption::Out ? 0.8 : 1.0;
                if(scale_option_ != ScaleOption::Normal){
                    src = src.scaled(src.width() * scale_factor, src.height() * scale_factor, Qt::KeepAspectRatio, Qt::SmoothTransformation);
                }else{
                    src = QImage(image_name_);
                }

                if(!src.isNull()){
                    graph_pixmap_->setPixmap(QPixmap::fromImage(src));
                    graph_scene_->setSceneRect(src.rect());
                    //reset the matrix because fitInView may change the internal matrix value, it
                    //may cause weird effect when showing image
                    graph_view_->resetMatrix();
                }else{
                    QMessageBox::warning(this, tr("Warning"),
                                         tr("Can't scale image %1.\n%2\n%3").arg(image_name_).arg(__FUNCTION__).arg(__FILE__));
                }
            }else{
                QMessageBox::warning(this, tr("Warning"),
                                     tr("Can't find the image %1.\n%2\n%3").arg(image_name_).arg(__FUNCTION__).arg(__FILE__));
            }
        }catch(std::exception const &exp){
            QMessageBox::warning(this, tr("Warning"), QString(exp.what()) + "\n" + __FUNCTION__ + "\n" + __FILE__);
        }
    }
}

void imageViewer::set_image_after_color_balance()
{   
    QImage const original_image = get_image();
    set_image(color_correction_->get_image());
    set_undo_image(original_image);
}

void imageViewer::set_image_after_color_balance_and_close()
{        
    QImage const original_image = get_image();
    set_image(color_correction_->get_image());
    color_correction_->close();
    set_undo_image(original_image);
}

/*
 * convert the image to gray image
 */
void imageViewer::to_gray_image()
{
    if(!graph_view_is_empty()){
        QImage img = get_image();
        if(!img.isNull()){
            cv::Mat mat = qimage_to_mat_ref(img);
            if(!mat.empty()){
                cv::Mat dst;
                cvt_to_gray(mat, dst);
                if(!dst.empty()){
                    set_undo_image(get_image());
                    graph_pixmap_->setPixmap(QPixmap::fromImage(mat_to_qimage_cpy(dst)));
                }else{
                    QMessageBox::warning(this, tr("Warning"),
                                         tr("Can't convert image %1 to gray image.\n%2\n%3").arg(image_name_).arg(__FUNCTION__).arg(__FILE__));
                }
            }
        }
    }
}

/*
 * revert the display image to previous state
 */
void imageViewer::undo()
{
    if(!image_undo_.isNull()){
        graph_pixmap_->setPixmap(QPixmap::fromImage(image_undo_));
        graph_scene_->setSceneRect(image_undo_.rect());
        disable_undo();
        emit undo_image();
    }
}

/*
 * when user enable or disable fit to action, setting
 * some behaviors(under development)
 */
void imageViewer::update_fit_to_action(bool flag)
{    
    graph_view_->fitInView(graph_pixmap_, Qt::IgnoreAspectRatio);

    action_fit_to_window_->setChecked(flag);

    action_full_screen_->setCheckable(!flag);
    action_normal_size_->setEnabled(!flag);
    action_zoom_in_->setEnabled(!flag);
    action_zoom_out_->setEnabled(!flag);
}

void imageViewer::zoom_in()
{    
    if(!graph_view_is_empty()){
        scale_option_ = ScaleOption::In;
        scale_image();
    }
}

void imageViewer::zoom_out()
{
    if(!graph_view_is_empty()){
        scale_option_ = ScaleOption::Out;
        scale_image();
    }
}
