#ifndef IMAGEVIEWER_HPP
#define IMAGEVIEWER_HPP

#include <memory>
#include <vector>

#include <QString>

#include <QImage>
#include <QMainWindow>

#include "imageViewerGlobal.hpp"

namespace cv
{
  class Mat;
}

class colorCorrection;
class QAction;
class QGraphicsPixmapItem;
class QGraphicsScene;
class QGraphicsView;
class QImage;
class QMenu;
class QStringList;
class QToolBar;

class imageViewer : public QMainWindow
{
    /*
     * These types are used for disable or enable action, menu and toolbar
     */
    typedef std::vector<QAction*>  ActionArr;
    typedef std::vector<QMenu*>    MenuArr;
    typedef std::vector<QToolBar*> ToolBarArr;

    enum ScaleOption
    {
        In = -1, Normal = 0, Out = 1
    };

    Q_OBJECT
public:    

    /*
     *design to enable or disable the actions, menu or toolbar of the
     *imageViewer
     */
    enum class Action  : int{About,      ASpectRatio, ColorCorrection, Qt,
                             Exit,       FitToWindow, FlipHorizontal,  FlipVertical,
                             FullScreen, NormalSize,  Open,            Rotate90,
                             Rotate90n,  SaveImage,   Undo,            ZoomIn,
                             ZoomOut,    ActionSize = ZoomOut + 1};
    enum class Toolbar : int{File, Image, ToolBarSize = Image + 1};

    explicit imageViewer(QWidget *parent = 0);
    imageViewer(imageViewer const&) = delete;
    imageViewer& operator=(imageViewer const&) = delete;

    QImage get_image() const;

    void set_action_visible(std::initializer_list<Action> const command, bool is_visible);
    void set_image(QString const &image_name);
    void set_image(QImage const &image, QString const &image_name = "");
    void set_menu_visible(bool is_visible);    
    void set_toolbar_visible(std::initializer_list<Toolbar> const command, bool is_visible);
    void set_undo_image(QImage const &src);

public slots:    
    void set_save_quality(int quality) { save_quality_ = quality; }

signals:
    void undo_image();
    
private slots:
    void aspect_ratio();

    void disable_undo();

    void exec_color_correction();

    void fit_to_window();
    void flip_horizontal();
    void flip_vertical();

    void histogram_equalization();

    void normal_size();

    void open_image();

    void rotate_image_clockwise();
    void rotate_image_counter_clockwise();

    void save_image();
    void set_image_after_color_balance();
    void set_image_after_color_balance_and_close();

    void to_gray_image();

    void undo();

    void zoom_in();
    void zoom_out();

private:
    void create_action();
    void create_connection();
    void create_menu_add_toolbar();
    void create_widget();

    void flip_image_impl(bool flip);

    bool graph_view_is_empty() const;    

    void open_image_impl(QString const &image_name);
    void open_image_impl(QImage const &image, QString const &image_name = "");

    void rotate_image_implement(double angle);

    void scale_image();

    void update_fit_to_action(bool flag);    

private:    
    QAction             *action_about_;
    ActionArr            action_array_;
    QAction             *action_aspect_ratio_;    
    QAction             *action_color_balance_;
    QAction             *action_color_correction_;
    QAction             *action_exit_;
    QAction             *action_fit_to_window_;
    QAction             *action_flip_horizontal_;
    QAction             *action_flip_vertical_;
    QAction             *action_full_screen_;
    QAction             *action_histogram_equal_;
    QAction             *action_normal_size_;
    QAction             *action_open_;
    QAction             *action_qt_;    
    QAction             *action_rotate_90_; //rotate clockwise(180)
    QAction             *action_rotate_90n_; //rotate counter clockwise(-180)
    QAction             *action_save_image_;
    QAction             *action_to_gray_image_;
    QAction             *action_undo_;
    QAction             *action_zoom_in_;
    QAction             *action_zoom_out_;
    double               angle_;

    colorCorrection     *color_correction_; //in charge of the works of color correction

    bool                 fit_to_window_;

    QGraphicsPixmapItem *graph_pixmap_;
    QGraphicsScene      *graph_scene_;
    QGraphicsView       *graph_view_;

    QString              image_name_;
    QImage               image_undo_; //image designed for undo function

    QMenu                *menu_about_;
    QMenu                *menu_file_;
    QMenu                *menu_view_;

    int                   save_quality_;
    int                   scale_option_; //-1 : in; 0 : normal; 1 : out

    ToolBarArr            toolbar_array_;
    QToolBar             *toolbar_file_;
    QToolBar             *toolbar_image_;
};

#endif // IMAGEVIEWER_HPP
