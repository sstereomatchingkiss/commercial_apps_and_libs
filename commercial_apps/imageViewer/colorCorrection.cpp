#include <memory>

#ifdef DEBUG_OK
#include <QDebug>
//#include <opencv2/highgui/highgui.hpp>
#endif

#include <QDoubleSpinBox>
#include <QGridLayout>
#include <QGroupBox>
#include <QLabel>
#include <QPixmap>
#include <QPushButton>
#include <QSlider>
#include <QSpinBox>
#include <QWidget>

#include "basicImageAlgo.hpp"
#include "blockSignal.hpp"
#include "colorCorrection.hpp"
#include "imageAlgorithm.hpp"
#include "openCVToQt.hpp"

namespace{

template<typename T>
inline void initialize_range(T *data[])
{
    data[0]->setRange(0, 180);
    data[1]->setRange(0, 255);
    data[2]->setRange(0, 255);
}

void set_zero() {}

template<typename T, typename...Args>
void set_zero(T *data[], Args...args)
{
    for(int i = 0; i != 3; ++i){
        data[i]->setValue(0);
    }
    set_zero(args...);
}

}

colorCorrection::colorCorrection(QWidget *parent) :
    QDialog(parent)
{    
    create_widget();
    create_layout();
    create_connection();
}

/*
 * under development
 */
QImage const colorCorrection::get_image() const
{
    return image_result_;
}

/*
 * set the image you want to apply color balance
 */
void colorCorrection::set_image(QImage const &image)
{
    image_origin_     = image;
    image_result_     = image;
    double const size = 100;
    int const width   = (size / image.width()) * image.width();
    int const height  = (size / image.height()) * image.height();
    image_origin_bgr_scale_ = image.width() > size && image.height() > size ?
                              image.scaled(width, height, Qt::KeepAspectRatio, Qt::SmoothTransformation)
                              : image.copy();
    image_new_bgr_scale_    = image_origin_bgr_scale_.copy();
    mat_new_bgr_scale_      = qimage_to_mat_cpy(image_new_bgr_scale_);
    mat_origin_bgr_scale_   = qimage_to_mat_ref(image_origin_bgr_scale_);

    #ifdef DEBUG_OK
    qDebug() << "set_image : " << mat_origin_bgr_scale_.channels();
    #endif

    label_new_image_content_->setPixmap(QPixmap::fromImage(image_new_bgr_scale_));
    label_origin_image_content_->setPixmap(QPixmap::fromImage(image_origin_bgr_scale_));

    set_default_value();
}

/**********************************************************
 ****************** implementation ************************
 **********************************************************/

/*
 * add the bgr value to new image
 *
 *@param :
 * src : the image you want to add value
 *
 */
void colorCorrection::add_bgr_value(cv::Mat &src)
{
    for(int i = 0; i != 3; ++i){
        add_channel(src, slider_bgr_[i]->value(), i);
    }
}

/*
 * add the hsv value to new image
 *
 *@param :
 * bgr : the bgr color space need to transform to hsv and add value
 * hsv : the value after adding hsv value
 */
void colorCorrection::add_hsv_value(cv::Mat &bgr, cv::Mat &hsv)
{
    cv::cvtColor(bgr, hsv, CV_BGR2HSV);
    for(int i = 0; i !=3; ++i){
        add_channel(hsv, slider_hsv_[i]->value(), i);
    }
    cv::cvtColor(hsv, bgr, CV_HSV2BGR);
}

/*
 * apply the color balance result to the origin picture without scale,
 * will emit signal "apply_to_origin_signal"
 */
void colorCorrection::apply_to_origin()
{
    cv::Mat temp = qimage_to_mat_cpy(image_origin_);
    add_bgr_value(temp);
    add_hsv_value(temp, mat_new_hsv_scale_);
    gamma_correction_lazy(temp, temp, static_cast<float>(spinbox_gamma_->value()));

    image_result_ = mat_to_qimage_cpy(temp);    
}

/*
 *block the signal of spinbox and slider and set the value which
 *associative to bgr color space
 *
 *@param :
 * channel : the channel need to be locked, it is same as the
 * slider or spinbox which value be changed
 *
 * value : value of the channel
 */
void colorCorrection::block_and_set_bgr_channel(int value, int channel)
{
    blockQSignal block_slider_bgr(slider_bgr_[channel]);
    blockQSignal block_spinbox_bgr(spinbox_bgr_[channel]);
    slider_bgr_[channel]->setValue(value);
    spinbox_bgr_[channel]->setValue(value);
}

void colorCorrection::block_and_set_gamma_value(int value)
{
    blockQSignal block_slider(slider_gamma_);
    blockQSignal block_spinbox(spinbox_gamma_);

    slider_gamma_->setValue(value);
    spinbox_gamma_->setValue(static_cast<double>(value) / 100);
}

/*
 *block the signal of spinbox and slider and set the value which
 *associative to hsv color channel
 *
 *@param :
 * channel : the channel need to be locked, it is same as the
 * slider or spinbox which value be changed
 *
 * value : value of the channel
 */
void colorCorrection::block_and_set_hsv_channel(int value, int channel)
{
    blockQSignal block_slider_lab(slider_hsv_[channel]);
    blockQSignal block_spinbox_lab(spinbox_hsv_[channel]);
    slider_hsv_[channel]->setValue(value);
    spinbox_hsv_[channel]->setValue(value);
}

/*
 * apply bgr, hsv color balance and gamma correction
 * on the input image
 *
 *@param :
 * src : input image
 */
void colorCorrection::color_change(cv::Mat &src)
{
    add_bgr_value(src);
    add_hsv_value(src, mat_new_hsv_scale_);
    gamma_correction_lazy(src, src, static_cast<float>(spinbox_gamma_->value()));
    set_new_image(src);
}

void colorCorrection::create_connection()
{
    connect(button_apply_to_origin_,   SIGNAL(clicked()), this, SLOT(apply_to_origin()));
    connect(button_apply_to_origin_,   SIGNAL(clicked()), this, SIGNAL(apply_to_origin_signal()));
    connect(button_ok_,                SIGNAL(clicked()), this, SLOT(apply_to_origin()));
    connect(button_ok_,                SIGNAL(clicked()), this, SIGNAL(ok()));
    connect(button_set_default_value_, SIGNAL(clicked()), this, SLOT(set_default_value()));

    connect(slider_bgr_[0], SIGNAL(valueChanged(int)), this, SLOT(bgr_b_changed(int)));
    connect(slider_bgr_[1], SIGNAL(valueChanged(int)), this, SLOT(bgr_g_changed(int)));
    connect(slider_bgr_[2], SIGNAL(valueChanged(int)), this, SLOT(bgr_r_changed(int)));
    connect(slider_gamma_,  SIGNAL(valueChanged(int)), this, SLOT(value_gamma_changed(int)));
    connect(slider_hsv_[0], SIGNAL(valueChanged(int)), this, SLOT(hsv_h_changed(int)));
    connect(slider_hsv_[1], SIGNAL(valueChanged(int)), this, SLOT(hsv_s_changed(int)));
    connect(slider_hsv_[2], SIGNAL(valueChanged(int)), this, SLOT(hsv_v_changed(int)));

    connect(spinbox_bgr_[0], SIGNAL(valueChanged(int)),    this, SLOT(bgr_b_changed(int)));
    connect(spinbox_bgr_[1], SIGNAL(valueChanged(int)),    this, SLOT(bgr_g_changed(int)));
    connect(spinbox_bgr_[2], SIGNAL(valueChanged(int)),    this, SLOT(bgr_r_changed(int)));
    connect(spinbox_gamma_,  SIGNAL(valueChanged(double)), this, SLOT(value_gamma_changed(double)));
    connect(spinbox_hsv_[0], SIGNAL(valueChanged(int)),    this, SLOT(hsv_h_changed(int)));
    connect(spinbox_hsv_[1], SIGNAL(valueChanged(int)),    this, SLOT(hsv_s_changed(int)));
    connect(spinbox_hsv_[2], SIGNAL(valueChanged(int)),    this, SLOT(hsv_v_changed(int)));
}

void colorCorrection::create_layout()
{
    auto *color_balance_layout = new QGridLayout(groupbox_color_balance_);
    color_balance_layout->addWidget(label_bgr_[2], 0, 0, 1, 1);
    color_balance_layout->addWidget(slider_bgr_[2], 0, 1, 1, 1);
    color_balance_layout->addWidget(spinbox_bgr_[2], 0, 2, 1, 1);
    color_balance_layout->addWidget(label_bgr_[1], 1, 0, 1, 1);
    color_balance_layout->addWidget(slider_bgr_[1], 1, 1, 1, 1);
    color_balance_layout->addWidget(spinbox_bgr_[1], 1, 2, 1, 1);
    color_balance_layout->addWidget(label_bgr_[0], 2, 0, 1, 1);
    color_balance_layout->addWidget(slider_bgr_[0], 2, 1, 1, 1);
    color_balance_layout->addWidget(spinbox_bgr_[0], 2, 2, 1, 1);
    color_balance_layout->addWidget(label_hsv_[0], 3, 0, 1, 1);
    color_balance_layout->addWidget(slider_hsv_[0], 3, 1, 1, 1);
    color_balance_layout->addWidget(spinbox_hsv_[0], 3, 2, 1, 1);
    color_balance_layout->addWidget(label_hsv_[1], 4, 0, 1, 1);
    color_balance_layout->addWidget(slider_hsv_[1], 4, 1, 1, 1);
    color_balance_layout->addWidget(spinbox_hsv_[1], 4, 2, 1, 1);
    color_balance_layout->addWidget(label_hsv_[2], 5, 0, 1, 1);
    color_balance_layout->addWidget(slider_hsv_[2], 5, 1, 1, 1);
    color_balance_layout->addWidget(spinbox_hsv_[2], 5, 2, 1, 1);

    auto *gamma_layout = new QGridLayout(groupbox_gamma_);
    gamma_layout->addWidget(label_gamma_, 0, 0, 1, 1);
    gamma_layout->addWidget(slider_gamma_, 0, 1, 1, 1);
    gamma_layout->addWidget(spinbox_gamma_, 0, 2, 1, 1);
    color_balance_layout->addWidget(groupbox_gamma_, 0, 3, 1, 1);

    std::unique_ptr<QGridLayout> main_layout(new QGridLayout);
    main_layout->addWidget(label_origin_image_, 0, 0, 1, 1);
    main_layout->addWidget(label_origin_image_content_, 1, 0, 1, 1);
    main_layout->addWidget(label_new_image_, 0, 1, 1, 1);
    main_layout->addWidget(label_new_image_content_, 1, 1, 1, 1);
    main_layout->addWidget(groupbox_color_balance_, 2, 0, 1, 1);
    main_layout->addWidget(button_apply_to_origin_, 3, 0, 1, 1);
    main_layout->addWidget(button_ok_, 3, 1, 1, 1);
    main_layout->addWidget(button_set_default_value_, 4, 0, 1, 1);

    setLayout(main_layout.release());
}

void colorCorrection::create_widget()
{
    button_apply_to_origin_ = new QPushButton(tr("Apply to original"), this);
    button_ok_ = new QPushButton(tr("OK"), this);
    button_set_default_value_ = new QPushButton(tr("Set default values"), this);

    groupbox_color_balance_ = new QGroupBox(tr("Color balance"), this);
    groupbox_gamma_         = new QGroupBox(tr("Gamma"), this);

    label_bgr_[0]               = new QLabel("B", this);
    label_bgr_[1]               = new QLabel("G", this);
    label_bgr_[2]               = new QLabel("R", this);
    label_hsv_[0]               = new QLabel("Hue", this);
    label_hsv_[1]               = new QLabel(tr("Saturation"), this);
    label_hsv_[2]               = new QLabel(tr("Intensity"), this);
    label_gamma_                = new QLabel(tr("Gamma"), this);
    label_new_image_            = new QLabel(tr("New image"), this);
    label_new_image_content_    = new QLabel(this);
    label_origin_image_         = new QLabel(tr("Original image"), this);
    label_origin_image_content_ = new QLabel(this);

    slider_gamma_ = new QSlider(Qt::Horizontal, this);
    spinbox_gamma_ = new QDoubleSpinBox(this);
    slider_gamma_->setRange(0, 699);
    slider_gamma_->setValue(100);
    spinbox_gamma_->setRange(0, 7);
    spinbox_gamma_->setValue(1);
    spinbox_gamma_->setSingleStep(0.01);
    for(int i = 0; i != 3; ++i){
        slider_bgr_[i]  = new QSlider(Qt::Horizontal, this);
        spinbox_bgr_[i] = new QSpinBox(this);
        slider_bgr_[i]->setRange(-255, 255);
        spinbox_bgr_[i]->setRange(-255, 255);
        slider_hsv_[i]  = new QSlider(Qt::Horizontal, this);
        spinbox_hsv_[i] = new QSpinBox(this);
    }
    initialize_range(slider_hsv_);
    initialize_range(spinbox_hsv_);
}

void colorCorrection::bgr_b_changed(int value)
{
    value_bgr_changed(value, 0);
}

void colorCorrection::bgr_g_changed(int value)
{
    value_bgr_changed(value, 1);
}

void colorCorrection::bgr_r_changed(int value)
{
    value_bgr_changed(value, 2);
}

void colorCorrection::hsv_h_changed(int value)
{
    value_hsv_changed(value, 0);
}

void colorCorrection::hsv_s_changed(int value)
{
    value_hsv_changed(value, 1);
}

void colorCorrection::hsv_v_changed(int value)
{
    value_hsv_changed(value, 2);
}

/*
 * reset the values of slider and spinbox to default values
 */
void colorCorrection::set_default_value()
{
    blockQSignal block_slider_gamma_(slider_gamma_);
    blockQSignal block_spinbox_gamma_(spinbox_gamma_);
    slider_gamma_->setValue(100);
    spinbox_gamma_->setValue(1);

    size_t const array_size = 3;
    typedef blockQSignals<array_size> blockIt;
    blockIt block_slider_bgr_(slider_bgr_);
    blockIt block_slider_hsv_(slider_hsv_);
    blockIt block_spinbox_bgr_(spinbox_bgr_);
    blockIt block_spinbox_hsv_(spinbox_hsv_);
    set_zero(slider_bgr_, slider_hsv_, spinbox_bgr_, spinbox_hsv_);

    set_new_image(mat_origin_bgr_scale_);
}

void colorCorrection::set_new_image(cv::Mat &mat)
{
    image_new_bgr_scale_ = mat_to_qimage_ref(mat);
    label_new_image_content_->setPixmap(QPixmap::fromImage(image_new_bgr_scale_));
}

/*
 * implementation of changing bgr value
 */
void colorCorrection::value_bgr_changed(int value, int channel)
{
    if(spinbox_bgr_[channel]->value() != slider_bgr_[channel]->value()){
        mat_origin_bgr_scale_.copyTo(mat_new_bgr_scale_);
        block_and_set_bgr_channel(value, channel);
        color_change(mat_new_bgr_scale_);
    }
}

void colorCorrection::value_gamma_changed(int value)
{
    if(static_cast<int>(spinbox_gamma_->value() * 100) != slider_gamma_->value()){
        mat_origin_bgr_scale_.copyTo(mat_new_bgr_scale_);
        block_and_set_gamma_value(value);
        color_change(mat_new_bgr_scale_);
    }
}

void colorCorrection::value_gamma_changed(double value)
{
    value_gamma_changed(static_cast<int>(value * 100));
}

/*
 * implementation of changing lab value
 */
void colorCorrection::value_hsv_changed(int value, int channel)
{
    if(spinbox_hsv_[channel]->value() != slider_hsv_[channel]->value()){
        mat_origin_bgr_scale_.copyTo(mat_new_bgr_scale_);
        block_and_set_hsv_channel(value, channel);
        color_change(mat_new_bgr_scale_);
    }
}
