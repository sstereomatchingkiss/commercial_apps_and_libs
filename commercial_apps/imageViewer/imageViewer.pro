#-------------------------------------------------
#
# Project created by QtCreator 2012-11-05T14:36:30
#
#-------------------------------------------------

QT += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = imageViewer
TEMPLATE = lib

#CONFIG += console
CONFIG += debug_and_release
CONFIG += warn_on
CONFIG += lib
CONFIG  += c++11

DEFINES += DEBUG_OK

win32{
INCLUDEPATH += ../../3rdLibs/openCV/OpenCV-2.4.5/build/include

LIBS += -L../../3rdLibs/openCV/OpenCV-2.4.5/build/x86/mingw/lib/ -lopencv_core245 -lopencv_highgui245 -lopencv_imgproc245 -lopencv_photo245
}
mac{
INCLUDEPATH += /usr/local/include

LIBS += -L/usr/local/lib/ -lopencv_core.2.4.5 -lopencv_imgproc.2.4.5
#LIBS += -L/usr/local/lib/ -lopencv_highgui.2.4.5
}

QMAKE_CXXFLAGS += -Woverloaded-virtual

CONFIG(debug, debug | release){
  DESTDIR += ../../commercial_apps/imageInterpolation/debug
}else{
  DESTDIR += ../../commercial_apps/imageInterpolation/release
}

INCLUDEPATH += ../../commercial_libs/algorithms
INCLUDEPATH += ../../commercial_libs/debugHelper
INCLUDEPATH += ../../commercial_libs/imageAlgorithm
INCLUDEPATH += ../../commercial_libs/OpenCVAndQt
INCLUDEPATH += ../../commercial_libs/widgetHelper2

SOURCES += main.cpp \
    imageViewer.cpp \
    ../../commercial_libs/OpenCVAndQt/openCVToQt.cpp \
    ../../commercial_libs/imageAlgorithm/imageAlgorithm.cpp \
    ../../commercial_libs/imageAlgorithm/waterMark.cpp \
    colorCorrection.cpp \
    ../../commercial_libs/widgetHelper2/widgetGlobalHelper.cpp \   
    ../../commercial_libs/imageAlgorithm/basicImageAlgo.cpp

HEADERS  += \
    imageViewer.hpp \
    ../../commercial_libs/OpenCVAndQt/openCVToQt.hpp \
    imageViewerGlobal.hpp \    
    ../../commercial_libs/imageAlgorithm/imageAlgorithm.hpp \
    ../../commercial_libs/imageAlgorithm/waterMark.hpp \
    colorCorrection.hpp \
    ../../commercial_libs/widgetHelper2/widgetGlobalHelper.hpp \    
    ../../commercial_libs/algorithms/metaProgrammingHelper.hpp \
    ../../commercial_libs/widgetHelper2/blockSignal.hpp \
    ../../commercial_libs/imageAlgorithm/basicImageAlgo.hpp

RESOURCES += \
    icons2.qrc
